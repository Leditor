#include "lqt_common.hpp"
#include <QGraphicsScene>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QGraphicsScene > : public QGraphicsScene {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__render (lua_State *L);
  static int __LuaWrapCall__addPixmap (lua_State *L);
  static int __LuaWrapCall__focusItem (lua_State *L);
  static int __LuaWrapCall__clearSelection (lua_State *L);
  static int __LuaWrapCall__update__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__update__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__update (lua_State *L);
  static int __LuaWrapCall__sceneRect (lua_State *L);
  static int __LuaWrapCall__addLine__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__addLine__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__addLine (lua_State *L);
  static int __LuaWrapCall__itemsBoundingRect (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__trUtf8 (lua_State *L);
  static int __LuaWrapCall__addRect__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__addRect__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__addRect (lua_State *L);
  static int __LuaWrapCall__advance (lua_State *L);
  static int __LuaWrapCall__selectionArea (lua_State *L);
  static int __LuaWrapCall__setSelectionArea__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__setSelectionArea__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__setSelectionArea (lua_State *L);
  static int __LuaWrapCall__setItemIndexMethod (lua_State *L);
  static int __LuaWrapCall__hasFocus (lua_State *L);
  static int __LuaWrapCall__bspTreeDepth (lua_State *L);
  static int __LuaWrapCall__collidingItems (lua_State *L);
  static int __LuaWrapCall__height (lua_State *L);
  static int __LuaWrapCall__addEllipse__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__addEllipse__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__addEllipse (lua_State *L);
  static int __LuaWrapCall__addSimpleText (lua_State *L);
  static int __LuaWrapCall__setSceneRect__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__setSceneRect__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__setSceneRect (lua_State *L);
  static int __LuaWrapCall__destroyItemGroup (lua_State *L);
  static int __LuaWrapCall__createItemGroup (lua_State *L);
  static int __LuaWrapCall__invalidate__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__invalidate__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__invalidate (lua_State *L);
  static int __LuaWrapCall__addItem (lua_State *L);
  static int __LuaWrapCall__setFocusItem (lua_State *L);
  static int __LuaWrapCall__setFocus (lua_State *L);
  static int __LuaWrapCall__items__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__items__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__items__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__items__OverloadedVersion__4 (lua_State *L);
  static int __LuaWrapCall__items__OverloadedVersion__5 (lua_State *L);
  static int __LuaWrapCall__items__OverloadedVersion__6 (lua_State *L);
  static int __LuaWrapCall__items (lua_State *L);
  static int __LuaWrapCall__addText (lua_State *L);
  static int __LuaWrapCall__metaObject (lua_State *L);
  static int __LuaWrapCall__itemIndexMethod (lua_State *L);
  static int __LuaWrapCall__backgroundBrush (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__4 (lua_State *L);
  static int __LuaWrapCall__new (lua_State *L);
  static int __LuaWrapCall__inputMethodQuery (lua_State *L);
  static int __LuaWrapCall__delete (lua_State *L);
  static int __LuaWrapCall__addPath (lua_State *L);
  static int __LuaWrapCall__foregroundBrush (lua_State *L);
  static int __LuaWrapCall__selectedItems (lua_State *L);
  static int __LuaWrapCall__mouseGrabberItem (lua_State *L);
  static int __LuaWrapCall__setBspTreeDepth (lua_State *L);
  static int __LuaWrapCall__width (lua_State *L);
  static int __LuaWrapCall__setForegroundBrush (lua_State *L);
  static int __LuaWrapCall__itemAt__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__itemAt__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__itemAt (lua_State *L);
  static int __LuaWrapCall__setBackgroundBrush (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__tr (lua_State *L);
  static int __LuaWrapCall__views (lua_State *L);
  static int __LuaWrapCall__removeItem (lua_State *L);
  static int __LuaWrapCall__addPolygon (lua_State *L);
  static int __LuaWrapCall__clearFocus (lua_State *L);
protected:
  void focusInEvent (QFocusEvent * arg1);
public:
protected:
  void keyPressEvent (QKeyEvent * arg1);
public:
protected:
  void contextMenuEvent (QGraphicsSceneContextMenuEvent * arg1);
public:
protected:
  void focusOutEvent (QFocusEvent * arg1);
public:
protected:
  void drawBackground (QPainter * arg1, const QRectF& arg2);
public:
protected:
  void mouseDoubleClickEvent (QGraphicsSceneMouseEvent * arg1);
public:
protected:
  void childEvent (QChildEvent * arg1);
public:
protected:
  void disconnectNotify (const char * arg1);
public:
  QVariant inputMethodQuery (Qt::InputMethodQuery arg1) const;
protected:
  void connectNotify (const char * arg1);
public:
protected:
  void drawForeground (QPainter * arg1, const QRectF& arg2);
public:
protected:
  void dragLeaveEvent (QGraphicsSceneDragDropEvent * arg1);
public:
protected:
  void mousePressEvent (QGraphicsSceneMouseEvent * arg1);
public:
protected:
  void timerEvent (QTimerEvent * arg1);
public:
  bool eventFilter (QObject * arg1, QEvent * arg2);
protected:
  void inputMethodEvent (QInputMethodEvent * arg1);
public:
protected:
  void dragMoveEvent (QGraphicsSceneDragDropEvent * arg1);
public:
protected:
  void drawItems (QPainter * arg1, int arg2, QGraphicsItem * * arg3, const QStyleOptionGraphicsItem * arg4, QWidget * arg5 = 0);
public:
protected:
  void wheelEvent (QGraphicsSceneWheelEvent * arg1);
public:
protected:
  void mouseReleaseEvent (QGraphicsSceneMouseEvent * arg1);
public:
protected:
  bool event (QEvent * arg1);
public:
protected:
  void dragEnterEvent (QGraphicsSceneDragDropEvent * arg1);
public:
protected:
  void mouseMoveEvent (QGraphicsSceneMouseEvent * arg1);
public:
protected:
  void helpEvent (QGraphicsSceneHelpEvent * arg1);
public:
  const QMetaObject * metaObject () const;
protected:
  void keyReleaseEvent (QKeyEvent * arg1);
public:
protected:
  void dropEvent (QGraphicsSceneDragDropEvent * arg1);
public:
protected:
  void customEvent (QEvent * arg1);
public:
  ~LuaBinder< QGraphicsScene > ();
  static int lqt_pushenum_ItemIndexMethod (lua_State *L);
  static int lqt_pushenum_ItemIndexMethod_QFLAGS_CREATOR (lua_State *L);
  static int lqt_pushenum_SceneLayer (lua_State *L);
  static int lqt_pushenum_SceneLayer_QFLAGS_CREATOR (lua_State *L);
  LuaBinder< QGraphicsScene > (lua_State *l, QObject * arg1):QGraphicsScene(arg1), L(l) {}
  LuaBinder< QGraphicsScene > (lua_State *l, const QRectF& arg1, QObject * arg2):QGraphicsScene(arg1, arg2), L(l) {}
  LuaBinder< QGraphicsScene > (lua_State *l, double arg1, double arg2, double arg3, double arg4, QObject * arg5):QGraphicsScene(arg1, arg2, arg3, arg4, arg5), L(l) {}
};

extern "C" int luaopen_QGraphicsScene (lua_State *L);
