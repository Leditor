#include "lqt_bind_QBoxLayout.hpp"

int LuaBinder< QBoxLayout >::__LuaWrapCall__insertLayout (lua_State *L) {
  QBoxLayout *& __lua__obj = *static_cast<QBoxLayout**>(lqtL_checkudata(L, 1, "QBoxLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  QLayout * arg2 = *static_cast<QLayout**>(lqtL_checkudata(L, 3, "QLayout*"));
  int arg3 = lua_isnumber(L, 4)?lua_tointeger(L, 4):static_cast< int >(0);
  __lua__obj->QBoxLayout::insertLayout(arg1, arg2, arg3);
  return 0;
}
int LuaBinder< QBoxLayout >::__LuaWrapCall__takeAt (lua_State *L) {
  QBoxLayout *& __lua__obj = *static_cast<QBoxLayout**>(lqtL_checkudata(L, 1, "QBoxLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  QLayoutItem * ret = __lua__obj->QBoxLayout::takeAt(arg1);
  lqtL_pushudata(L, ret, "QLayoutItem*");
  return 1;
}
int LuaBinder< QBoxLayout >::__LuaWrapCall__delete (lua_State *L) {
  QBoxLayout *& __lua__obj = *static_cast<QBoxLayout**>(lqtL_checkudata(L, 1, "QBoxLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  delete __lua__obj;
  __lua__obj = 0;
  return 0;
}
int LuaBinder< QBoxLayout >::__LuaWrapCall__sizeHint (lua_State *L) {
  QBoxLayout *& __lua__obj = *static_cast<QBoxLayout**>(lqtL_checkudata(L, 1, "QBoxLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QSize ret = __lua__obj->QBoxLayout::sizeHint();
  lqtL_passudata(L, new QSize(ret), "QSize*");
  return 1;
}
int LuaBinder< QBoxLayout >::__LuaWrapCall__setSpacing (lua_State *L) {
  QBoxLayout *& __lua__obj = *static_cast<QBoxLayout**>(lqtL_checkudata(L, 1, "QBoxLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QBoxLayout::setSpacing(arg1);
  return 0;
}
int LuaBinder< QBoxLayout >::__LuaWrapCall__minimumSize (lua_State *L) {
  QBoxLayout *& __lua__obj = *static_cast<QBoxLayout**>(lqtL_checkudata(L, 1, "QBoxLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QSize ret = __lua__obj->QBoxLayout::minimumSize();
  lqtL_passudata(L, new QSize(ret), "QSize*");
  return 1;
}
int LuaBinder< QBoxLayout >::__LuaWrapCall__invalidate (lua_State *L) {
  QBoxLayout *& __lua__obj = *static_cast<QBoxLayout**>(lqtL_checkudata(L, 1, "QBoxLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QBoxLayout::invalidate();
  return 0;
}
int LuaBinder< QBoxLayout >::__LuaWrapCall__direction (lua_State *L) {
  QBoxLayout *& __lua__obj = *static_cast<QBoxLayout**>(lqtL_checkudata(L, 1, "QBoxLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QBoxLayout::Direction ret = __lua__obj->QBoxLayout::direction();
  lqtL_pushenum(L, ret, "QBoxLayout::Direction");
  return 1;
}
int LuaBinder< QBoxLayout >::__LuaWrapCall__spacing (lua_State *L) {
  QBoxLayout *& __lua__obj = *static_cast<QBoxLayout**>(lqtL_checkudata(L, 1, "QBoxLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QBoxLayout::spacing();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QBoxLayout >::__LuaWrapCall__addSpacing (lua_State *L) {
  QBoxLayout *& __lua__obj = *static_cast<QBoxLayout**>(lqtL_checkudata(L, 1, "QBoxLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QBoxLayout::addSpacing(arg1);
  return 0;
}
int LuaBinder< QBoxLayout >::__LuaWrapCall__insertWidget (lua_State *L) {
  QBoxLayout *& __lua__obj = *static_cast<QBoxLayout**>(lqtL_checkudata(L, 1, "QBoxLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  QWidget * arg2 = *static_cast<QWidget**>(lqtL_checkudata(L, 3, "QWidget*"));
  int arg3 = lua_isnumber(L, 4)?lua_tointeger(L, 4):static_cast< int >(0);
  QFlags<Qt::AlignmentFlag> arg4 = lqtL_testudata(L, 5, "QFlags<Qt::AlignmentFlag>*")?**static_cast<QFlags<Qt::AlignmentFlag>**>(lqtL_checkudata(L, 5, "QFlags<Qt::AlignmentFlag>*")):static_cast< QFlags<Qt::AlignmentFlag> >(0);
  __lua__obj->QBoxLayout::insertWidget(arg1, arg2, arg3, arg4);
  return 0;
}
int LuaBinder< QBoxLayout >::__LuaWrapCall__addStretch (lua_State *L) {
  QBoxLayout *& __lua__obj = *static_cast<QBoxLayout**>(lqtL_checkudata(L, 1, "QBoxLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_isnumber(L, 2)?lua_tointeger(L, 2):static_cast< int >(0);
  __lua__obj->QBoxLayout::addStretch(arg1);
  return 0;
}
int LuaBinder< QBoxLayout >::__LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QBoxLayout::trUtf8(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QBoxLayout >::__LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QBoxLayout::trUtf8(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QBoxLayout >::__LuaWrapCall__trUtf8 (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x10dbb90;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x10db6f0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x10dd980;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x10dd0b0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x10dde10;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__trUtf8__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__trUtf8__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__trUtf8 matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QBoxLayout >::__LuaWrapCall__insertStretch (lua_State *L) {
  QBoxLayout *& __lua__obj = *static_cast<QBoxLayout**>(lqtL_checkudata(L, 1, "QBoxLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int arg2 = lua_isnumber(L, 3)?lua_tointeger(L, 3):static_cast< int >(0);
  __lua__obj->QBoxLayout::insertStretch(arg1, arg2);
  return 0;
}
int LuaBinder< QBoxLayout >::__LuaWrapCall__metaObject (lua_State *L) {
  QBoxLayout *& __lua__obj = *static_cast<QBoxLayout**>(lqtL_checkudata(L, 1, "QBoxLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QMetaObject * ret = __lua__obj->QBoxLayout::metaObject();
  lqtL_pushudata(L, ret, "QMetaObject*");
  return 1;
}
int LuaBinder< QBoxLayout >::__LuaWrapCall__new (lua_State *L) {
  QBoxLayout::Direction arg1 = static_cast<QBoxLayout::Direction>(lqtL_toenum(L, 1, "QBoxLayout::Direction"));
  QWidget * arg2 = lqtL_testudata(L, 2, "QWidget*")?*static_cast<QWidget**>(lqtL_checkudata(L, 2, "QWidget*")):static_cast< QWidget * >(0);
  QBoxLayout * ret = new LuaBinder< QBoxLayout >(L, arg1, arg2);
  lqtL_passudata(L, ret, "QBoxLayout*");
  return 1;
}
int LuaBinder< QBoxLayout >::__LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QBoxLayout::tr(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QBoxLayout >::__LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QBoxLayout::tr(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QBoxLayout >::__LuaWrapCall__tr (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x10dae30;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x10dab90;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x10dc870;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x10dcd60;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x10dd110;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__tr__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__tr__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__tr matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QBoxLayout >::__LuaWrapCall__addStrut (lua_State *L) {
  QBoxLayout *& __lua__obj = *static_cast<QBoxLayout**>(lqtL_checkudata(L, 1, "QBoxLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QBoxLayout::addStrut(arg1);
  return 0;
}
int LuaBinder< QBoxLayout >::__LuaWrapCall__minimumHeightForWidth (lua_State *L) {
  QBoxLayout *& __lua__obj = *static_cast<QBoxLayout**>(lqtL_checkudata(L, 1, "QBoxLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int ret = __lua__obj->QBoxLayout::minimumHeightForWidth(arg1);
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QBoxLayout >::__LuaWrapCall__setDirection (lua_State *L) {
  QBoxLayout *& __lua__obj = *static_cast<QBoxLayout**>(lqtL_checkudata(L, 1, "QBoxLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QBoxLayout::Direction arg1 = static_cast<QBoxLayout::Direction>(lqtL_toenum(L, 2, "QBoxLayout::Direction"));
  __lua__obj->QBoxLayout::setDirection(arg1);
  return 0;
}
int LuaBinder< QBoxLayout >::__LuaWrapCall__heightForWidth (lua_State *L) {
  QBoxLayout *& __lua__obj = *static_cast<QBoxLayout**>(lqtL_checkudata(L, 1, "QBoxLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int ret = __lua__obj->QBoxLayout::heightForWidth(arg1);
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QBoxLayout >::__LuaWrapCall__addItem (lua_State *L) {
  QBoxLayout *& __lua__obj = *static_cast<QBoxLayout**>(lqtL_checkudata(L, 1, "QBoxLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QLayoutItem * arg1 = *static_cast<QLayoutItem**>(lqtL_checkudata(L, 2, "QLayoutItem*"));
  __lua__obj->QBoxLayout::addItem(arg1);
  return 0;
}
int LuaBinder< QBoxLayout >::__LuaWrapCall__setStretchFactor__OverloadedVersion__1 (lua_State *L) {
  QBoxLayout *& __lua__obj = *static_cast<QBoxLayout**>(lqtL_checkudata(L, 1, "QBoxLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 2, "QWidget*"));
  int arg2 = lua_tointeger(L, 3);
  bool ret = __lua__obj->QBoxLayout::setStretchFactor(arg1, arg2);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QBoxLayout >::__LuaWrapCall__setStretchFactor__OverloadedVersion__2 (lua_State *L) {
  QBoxLayout *& __lua__obj = *static_cast<QBoxLayout**>(lqtL_checkudata(L, 1, "QBoxLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QLayout * arg1 = *static_cast<QLayout**>(lqtL_checkudata(L, 2, "QLayout*"));
  int arg2 = lua_tointeger(L, 3);
  bool ret = __lua__obj->QBoxLayout::setStretchFactor(arg1, arg2);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QBoxLayout >::__LuaWrapCall__setStretchFactor (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QBoxLayout*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QWidget*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x10ed010;
  } else {
    score[1] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x10eca40;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QBoxLayout*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QLayout*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x10ede50;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x10ed8f0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__setStretchFactor__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__setStretchFactor__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__setStretchFactor matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QBoxLayout >::__LuaWrapCall__expandingDirections (lua_State *L) {
  QBoxLayout *& __lua__obj = *static_cast<QBoxLayout**>(lqtL_checkudata(L, 1, "QBoxLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<Qt::Orientation> ret = __lua__obj->QBoxLayout::expandingDirections();
  lqtL_passudata(L, new QFlags<Qt::Orientation>(ret), "QFlags<Qt::Orientation>*");
  return 1;
}
int LuaBinder< QBoxLayout >::__LuaWrapCall__insertSpacing (lua_State *L) {
  QBoxLayout *& __lua__obj = *static_cast<QBoxLayout**>(lqtL_checkudata(L, 1, "QBoxLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int arg2 = lua_tointeger(L, 3);
  __lua__obj->QBoxLayout::insertSpacing(arg1, arg2);
  return 0;
}
int LuaBinder< QBoxLayout >::__LuaWrapCall__addLayout (lua_State *L) {
  QBoxLayout *& __lua__obj = *static_cast<QBoxLayout**>(lqtL_checkudata(L, 1, "QBoxLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QLayout * arg1 = *static_cast<QLayout**>(lqtL_checkudata(L, 2, "QLayout*"));
  int arg2 = lua_isnumber(L, 3)?lua_tointeger(L, 3):static_cast< int >(0);
  __lua__obj->QBoxLayout::addLayout(arg1, arg2);
  return 0;
}
int LuaBinder< QBoxLayout >::__LuaWrapCall__count (lua_State *L) {
  QBoxLayout *& __lua__obj = *static_cast<QBoxLayout**>(lqtL_checkudata(L, 1, "QBoxLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QBoxLayout::count();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QBoxLayout >::__LuaWrapCall__addWidget (lua_State *L) {
  QBoxLayout *& __lua__obj = *static_cast<QBoxLayout**>(lqtL_checkudata(L, 1, "QBoxLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 2, "QWidget*"));
  int arg2 = lua_isnumber(L, 3)?lua_tointeger(L, 3):static_cast< int >(0);
  QFlags<Qt::AlignmentFlag> arg3 = lqtL_testudata(L, 4, "QFlags<Qt::AlignmentFlag>*")?**static_cast<QFlags<Qt::AlignmentFlag>**>(lqtL_checkudata(L, 4, "QFlags<Qt::AlignmentFlag>*")):static_cast< QFlags<Qt::AlignmentFlag> >(0);
  __lua__obj->QBoxLayout::addWidget(arg1, arg2, arg3);
  return 0;
}
int LuaBinder< QBoxLayout >::__LuaWrapCall__maximumSize (lua_State *L) {
  QBoxLayout *& __lua__obj = *static_cast<QBoxLayout**>(lqtL_checkudata(L, 1, "QBoxLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QSize ret = __lua__obj->QBoxLayout::maximumSize();
  lqtL_passudata(L, new QSize(ret), "QSize*");
  return 1;
}
int LuaBinder< QBoxLayout >::__LuaWrapCall__setGeometry (lua_State *L) {
  QBoxLayout *& __lua__obj = *static_cast<QBoxLayout**>(lqtL_checkudata(L, 1, "QBoxLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QRect& arg1 = **static_cast<QRect**>(lqtL_checkudata(L, 2, "QRect*"));
  __lua__obj->QBoxLayout::setGeometry(arg1);
  return 0;
}
int LuaBinder< QBoxLayout >::__LuaWrapCall__itemAt (lua_State *L) {
  QBoxLayout *& __lua__obj = *static_cast<QBoxLayout**>(lqtL_checkudata(L, 1, "QBoxLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  QLayoutItem * ret = __lua__obj->QBoxLayout::itemAt(arg1);
  lqtL_pushudata(L, ret, "QLayoutItem*");
  return 1;
}
int LuaBinder< QBoxLayout >::__LuaWrapCall__hasHeightForWidth (lua_State *L) {
  QBoxLayout *& __lua__obj = *static_cast<QBoxLayout**>(lqtL_checkudata(L, 1, "QBoxLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QBoxLayout::hasHeightForWidth();
  lua_pushboolean(L, ret);
  return 1;
}
QLayout * LuaBinder< QBoxLayout >::layout () {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QBoxLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "layout");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QLayout::layout();
  }
  QLayout * ret = *static_cast<QLayout**>(lqtL_checkudata(L, -1, "QLayout*"));
  lua_settop(L, oldtop);
  return ret;
}
QLayoutItem * LuaBinder< QBoxLayout >::takeAt (int arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QBoxLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "takeAt");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushinteger(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QBoxLayout::takeAt(arg1);
  }
  QLayoutItem * ret = *static_cast<QLayoutItem**>(lqtL_checkudata(L, -1, "QLayoutItem*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QBoxLayout >::setGeometry (const QRect& arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QBoxLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "setGeometry");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QRect*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QBoxLayout::setGeometry(arg1);
  }
  lua_settop(L, oldtop);
}
QLayoutItem * LuaBinder< QBoxLayout >::itemAt (int arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QBoxLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "itemAt");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushinteger(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QBoxLayout::itemAt(arg1);
  }
  QLayoutItem * ret = *static_cast<QLayoutItem**>(lqtL_checkudata(L, -1, "QLayoutItem*"));
  lua_settop(L, oldtop);
  return ret;
}
int LuaBinder< QBoxLayout >::heightForWidth (int arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QBoxLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "heightForWidth");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushinteger(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QBoxLayout::heightForWidth(arg1);
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
QSize LuaBinder< QBoxLayout >::minimumSize () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QBoxLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "minimumSize");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QBoxLayout::minimumSize();
  }
  QSize ret = **static_cast<QSize**>(lqtL_checkudata(L, -1, "QSize*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QBoxLayout >::invalidate () {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QBoxLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "invalidate");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QBoxLayout::invalidate();
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QBoxLayout >::connectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QBoxLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "connectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::connectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QBoxLayout >::timerEvent (QTimerEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QBoxLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "timerEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QTimerEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::timerEvent(arg1);
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QBoxLayout >::minimumHeightForWidth (int arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QBoxLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "minimumHeightForWidth");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushinteger(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QBoxLayout::minimumHeightForWidth(arg1);
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
QSize LuaBinder< QBoxLayout >::maximumSize () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QBoxLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "maximumSize");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QBoxLayout::maximumSize();
  }
  QSize ret = **static_cast<QSize**>(lqtL_checkudata(L, -1, "QSize*"));
  lua_settop(L, oldtop);
  return ret;
}
bool LuaBinder< QBoxLayout >::hasHeightForWidth () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QBoxLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "hasHeightForWidth");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QBoxLayout::hasHeightForWidth();
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QBoxLayout >::addItem (QLayoutItem * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QBoxLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "addItem");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QLayoutItem*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QBoxLayout::addItem(arg1);
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QBoxLayout >::eventFilter (QObject * arg1, QEvent * arg2) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QBoxLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "eventFilter");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QObject*");
  lqtL_pushudata(L, arg2, "QEvent*");
  if (lua_isfunction(L, -2-2)) {
    lua_pcall(L, 2+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QObject::eventFilter(arg1, arg2);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
QSpacerItem * LuaBinder< QBoxLayout >::spacerItem () {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QBoxLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "spacerItem");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QLayoutItem::spacerItem();
  }
  QSpacerItem * ret = *static_cast<QSpacerItem**>(lqtL_checkudata(L, -1, "QSpacerItem*"));
  lua_settop(L, oldtop);
  return ret;
}
QSize LuaBinder< QBoxLayout >::sizeHint () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QBoxLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "sizeHint");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QBoxLayout::sizeHint();
  }
  QSize ret = **static_cast<QSize**>(lqtL_checkudata(L, -1, "QSize*"));
  lua_settop(L, oldtop);
  return ret;
}
int LuaBinder< QBoxLayout >::indexOf (QWidget * arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QBoxLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "indexOf");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QWidget*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QLayout::indexOf(arg1);
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
bool LuaBinder< QBoxLayout >::isEmpty () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QBoxLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "isEmpty");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QLayout::isEmpty();
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QBoxLayout >::disconnectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QBoxLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "disconnectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::disconnectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QBoxLayout >::event (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QBoxLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "event");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QObject::event(arg1);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
QWidget * LuaBinder< QBoxLayout >::widget () {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QBoxLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "widget");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QLayoutItem::widget();
  }
  QWidget * ret = *static_cast<QWidget**>(lqtL_checkudata(L, -1, "QWidget*"));
  lua_settop(L, oldtop);
  return ret;
}
int LuaBinder< QBoxLayout >::count () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QBoxLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "count");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QBoxLayout::count();
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
QRect LuaBinder< QBoxLayout >::geometry () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QBoxLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "geometry");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QLayout::geometry();
  }
  QRect ret = **static_cast<QRect**>(lqtL_checkudata(L, -1, "QRect*"));
  lua_settop(L, oldtop);
  return ret;
}
QFlags<Qt::Orientation> LuaBinder< QBoxLayout >::expandingDirections () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QBoxLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "expandingDirections");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QBoxLayout::expandingDirections();
  }
  QFlags<Qt::Orientation> ret = **static_cast<QFlags<Qt::Orientation>**>(lqtL_checkudata(L, -1, "QFlags<Qt::Orientation>*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QBoxLayout >::childEvent (QChildEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QBoxLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "childEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QChildEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QLayout::childEvent(arg1);
  }
  lua_settop(L, oldtop);
}
const QMetaObject * LuaBinder< QBoxLayout >::metaObject () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QBoxLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "metaObject");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QBoxLayout::metaObject();
  }
  const QMetaObject * ret = *static_cast<QMetaObject**>(lqtL_checkudata(L, -1, "QMetaObject*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QBoxLayout >::customEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QBoxLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "customEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::customEvent(arg1);
  }
  lua_settop(L, oldtop);
}
LuaBinder< QBoxLayout >::  ~LuaBinder< QBoxLayout > () {
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QBoxLayout*");
  lua_getfield(L, -1, "~QBoxLayout");

  if (lua_isfunction(L, -1)) {
    lua_insert(L, -2);
    lua_pcall(L, 1, 1, 0);
  } else {
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QBoxLayout >::lqt_pushenum_Direction (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "LeftToRight");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "LeftToRight");
  lua_pushstring(L, "RightToLeft");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "RightToLeft");
  lua_pushstring(L, "TopToBottom");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "TopToBottom");
  lua_pushstring(L, "BottomToTop");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "BottomToTop");
  lua_pushstring(L, "Down");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "Down");
  lua_pushstring(L, "Up");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "Up");
  lua_pushcfunction(L, LuaBinder< QBoxLayout >::lqt_pushenum_Direction_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QBoxLayout::Direction");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QBoxLayout >::lqt_pushenum_Direction_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QBoxLayout::Direction>*) + sizeof(QFlags<QBoxLayout::Direction>));
  QFlags<QBoxLayout::Direction> *fl = static_cast<QFlags<QBoxLayout::Direction>*>( static_cast<void*>(&static_cast<QFlags<QBoxLayout::Direction>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QBoxLayout::Direction>(lqtL_toenum(L, i, "QBoxLayout::Direction"));
	}
	if (luaL_newmetatable(L, "QFlags<QBoxLayout::Direction>*")) {
		lua_pushstring(L, "QFlags<QBoxLayout::Direction>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int luaopen_QBoxLayout (lua_State *L) {
  if (luaL_newmetatable(L, "QBoxLayout*")) {
    lua_pushcfunction(L, LuaBinder< QBoxLayout >::__LuaWrapCall__insertLayout);
    lua_setfield(L, -2, "insertLayout");
    lua_pushcfunction(L, LuaBinder< QBoxLayout >::__LuaWrapCall__takeAt);
    lua_setfield(L, -2, "takeAt");
    lua_pushcfunction(L, LuaBinder< QBoxLayout >::__LuaWrapCall__delete);
    lua_setfield(L, -2, "delete");
    lua_pushcfunction(L, LuaBinder< QBoxLayout >::__LuaWrapCall__sizeHint);
    lua_setfield(L, -2, "sizeHint");
    lua_pushcfunction(L, LuaBinder< QBoxLayout >::__LuaWrapCall__setSpacing);
    lua_setfield(L, -2, "setSpacing");
    lua_pushcfunction(L, LuaBinder< QBoxLayout >::__LuaWrapCall__minimumSize);
    lua_setfield(L, -2, "minimumSize");
    lua_pushcfunction(L, LuaBinder< QBoxLayout >::__LuaWrapCall__invalidate);
    lua_setfield(L, -2, "invalidate");
    lua_pushcfunction(L, LuaBinder< QBoxLayout >::__LuaWrapCall__direction);
    lua_setfield(L, -2, "direction");
    lua_pushcfunction(L, LuaBinder< QBoxLayout >::__LuaWrapCall__spacing);
    lua_setfield(L, -2, "spacing");
    lua_pushcfunction(L, LuaBinder< QBoxLayout >::__LuaWrapCall__addSpacing);
    lua_setfield(L, -2, "addSpacing");
    lua_pushcfunction(L, LuaBinder< QBoxLayout >::__LuaWrapCall__insertWidget);
    lua_setfield(L, -2, "insertWidget");
    lua_pushcfunction(L, LuaBinder< QBoxLayout >::__LuaWrapCall__addStretch);
    lua_setfield(L, -2, "addStretch");
    lua_pushcfunction(L, LuaBinder< QBoxLayout >::__LuaWrapCall__trUtf8);
    lua_setfield(L, -2, "trUtf8");
    lua_pushcfunction(L, LuaBinder< QBoxLayout >::__LuaWrapCall__insertStretch);
    lua_setfield(L, -2, "insertStretch");
    lua_pushcfunction(L, LuaBinder< QBoxLayout >::__LuaWrapCall__metaObject);
    lua_setfield(L, -2, "metaObject");
    lua_pushcfunction(L, LuaBinder< QBoxLayout >::__LuaWrapCall__new);
    lua_setfield(L, -2, "new");
    lua_pushcfunction(L, LuaBinder< QBoxLayout >::__LuaWrapCall__tr);
    lua_setfield(L, -2, "tr");
    lua_pushcfunction(L, LuaBinder< QBoxLayout >::__LuaWrapCall__addStrut);
    lua_setfield(L, -2, "addStrut");
    lua_pushcfunction(L, LuaBinder< QBoxLayout >::__LuaWrapCall__minimumHeightForWidth);
    lua_setfield(L, -2, "minimumHeightForWidth");
    lua_pushcfunction(L, LuaBinder< QBoxLayout >::__LuaWrapCall__setDirection);
    lua_setfield(L, -2, "setDirection");
    lua_pushcfunction(L, LuaBinder< QBoxLayout >::__LuaWrapCall__heightForWidth);
    lua_setfield(L, -2, "heightForWidth");
    lua_pushcfunction(L, LuaBinder< QBoxLayout >::__LuaWrapCall__addItem);
    lua_setfield(L, -2, "addItem");
    lua_pushcfunction(L, LuaBinder< QBoxLayout >::__LuaWrapCall__setStretchFactor);
    lua_setfield(L, -2, "setStretchFactor");
    lua_pushcfunction(L, LuaBinder< QBoxLayout >::__LuaWrapCall__expandingDirections);
    lua_setfield(L, -2, "expandingDirections");
    lua_pushcfunction(L, LuaBinder< QBoxLayout >::__LuaWrapCall__insertSpacing);
    lua_setfield(L, -2, "insertSpacing");
    lua_pushcfunction(L, LuaBinder< QBoxLayout >::__LuaWrapCall__addLayout);
    lua_setfield(L, -2, "addLayout");
    lua_pushcfunction(L, LuaBinder< QBoxLayout >::__LuaWrapCall__count);
    lua_setfield(L, -2, "count");
    lua_pushcfunction(L, LuaBinder< QBoxLayout >::__LuaWrapCall__addWidget);
    lua_setfield(L, -2, "addWidget");
    lua_pushcfunction(L, LuaBinder< QBoxLayout >::__LuaWrapCall__maximumSize);
    lua_setfield(L, -2, "maximumSize");
    lua_pushcfunction(L, LuaBinder< QBoxLayout >::__LuaWrapCall__setGeometry);
    lua_setfield(L, -2, "setGeometry");
    lua_pushcfunction(L, LuaBinder< QBoxLayout >::__LuaWrapCall__itemAt);
    lua_setfield(L, -2, "itemAt");
    lua_pushcfunction(L, LuaBinder< QBoxLayout >::__LuaWrapCall__hasHeightForWidth);
    lua_setfield(L, -2, "hasHeightForWidth");
    LuaBinder< QBoxLayout >::lqt_pushenum_Direction(L);
    lua_setfield(L, -2, "Direction");
    lua_newtable(L);
    lua_pushboolean(L, 1);
    lua_setfield(L, -2, "QLayout*");
    lua_pushboolean(L, 0);
    lua_setfield(L, -2, "QObject*");
    lua_pushboolean(L, 0);
    lua_setfield(L, -2, "QLayoutItem*");
    lua_setfield(L, -2, "__base");
    lua_pushcfunction(L, lqtL_newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, lqtL_index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, lqtL_gc);
    lua_setfield(L, -2, "__gc");
    lua_pushstring(L, "QBoxLayout");
    lua_setfield(L, -2, "__qtype");
		lua_setglobal(L, "QBoxLayout");
  } else {
		lua_pop(L, 1);
	}
  return 0;
}
