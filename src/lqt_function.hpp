#ifndef __LQT_FUNCTION
#define __LQT_FUNCTION

#include "lqt_common.hpp"

#include <QObject>
//#include <QDebug>

#define LUA_FUNCTION_REGISTRY "Registry Function"
//#ifndef SEE_STACK
//# define SEE_STACK(L, j) for (int j=1;j<=lua_gettop(L);j++) { qDebug() << j << '=' << luaL_typename(L, j) << '@' << lua_topointer (L, j); }
//#endif

class LuaFunction: public QObject {
  Q_OBJECT

  public:
  LuaFunction(lua_State *state);
  virtual ~LuaFunction();

  private:
    lua_State *L;
    static int __gc (lua_State *L);
  protected:
  public:
  public slots:
  void function ();
  void function (double arg1);
  void function (int arg1);
  void function (const char * arg1);
  void function (bool arg1);
  void function (int arg1, const char * arg2);
  void function (bool arg1, double arg2);
  void function (bool arg1, int arg2);
  void function (int arg1, int arg2);
  void function (bool arg1, bool arg2);
  void function (bool arg1, const char * arg2);
  void function (int arg1, double arg2);
  void function (double arg1, int arg2);
  void function (const char * arg1, bool arg2);
  void function (int arg1, bool arg2);
  void function (double arg1, double arg2);
  void function (const char * arg1, double arg2);
  void function (const char * arg1, int arg2);
  void function (double arg1, bool arg2);
  void function (double arg1, const char * arg2);
  void function (const char * arg1, const char * arg2);
};


#endif // __LQT_FUNCTION
