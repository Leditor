#include "lqt_common.hpp"
#include <QMainWindow>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QMainWindow > : public QMainWindow {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__splitDockWidget (lua_State *L);
  static int __LuaWrapCall__toolBarBreak (lua_State *L);
  static int __LuaWrapCall__saveState (lua_State *L);
  static int __LuaWrapCall__addToolBarBreak (lua_State *L);
  static int __LuaWrapCall__createPopupMenu (lua_State *L);
  static int __LuaWrapCall__insertToolBar (lua_State *L);
  static int __LuaWrapCall__removeToolBarBreak (lua_State *L);
  static int __LuaWrapCall__setAnimated (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__trUtf8 (lua_State *L);
  static int __LuaWrapCall__setUnifiedTitleAndToolBarOnMac (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__tr (lua_State *L);
  static int __LuaWrapCall__menuWidget (lua_State *L);
  static int __LuaWrapCall__removeDockWidget (lua_State *L);
  static int __LuaWrapCall__setCentralWidget (lua_State *L);
  static int __LuaWrapCall__setDockOptions (lua_State *L);
  static int __LuaWrapCall__setStatusBar (lua_State *L);
  static int __LuaWrapCall__insertToolBarBreak (lua_State *L);
  static int __LuaWrapCall__setMenuWidget (lua_State *L);
  static int __LuaWrapCall__setMenuBar (lua_State *L);
  static int __LuaWrapCall__isAnimated (lua_State *L);
  static int __LuaWrapCall__statusBar (lua_State *L);
  static int __LuaWrapCall__menuBar (lua_State *L);
  static int __LuaWrapCall__setDockNestingEnabled (lua_State *L);
  static int __LuaWrapCall__setIconSize (lua_State *L);
  static int __LuaWrapCall__iconSize (lua_State *L);
  static int __LuaWrapCall__dockWidgetArea (lua_State *L);
  static int __LuaWrapCall__metaObject (lua_State *L);
  static int __LuaWrapCall__new (lua_State *L);
  static int __LuaWrapCall__unifiedTitleAndToolBarOnMac (lua_State *L);
  static int __LuaWrapCall__delete (lua_State *L);
  static int __LuaWrapCall__setCorner (lua_State *L);
  static int __LuaWrapCall__corner (lua_State *L);
  static int __LuaWrapCall__addToolBar__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__addToolBar__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__addToolBar__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__addToolBar (lua_State *L);
  static int __LuaWrapCall__removeToolBar (lua_State *L);
  static int __LuaWrapCall__dockOptions (lua_State *L);
  static int __LuaWrapCall__restoreState (lua_State *L);
  static int __LuaWrapCall__toolButtonStyle (lua_State *L);
  static int __LuaWrapCall__tabifyDockWidget (lua_State *L);
  static int __LuaWrapCall__isDockNestingEnabled (lua_State *L);
  static int __LuaWrapCall__isSeparator (lua_State *L);
  static int __LuaWrapCall__addDockWidget__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__addDockWidget__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__addDockWidget (lua_State *L);
  static int __LuaWrapCall__setToolButtonStyle (lua_State *L);
  static int __LuaWrapCall__toolBarArea (lua_State *L);
  static int __LuaWrapCall__centralWidget (lua_State *L);
protected:
  void styleChange (QStyle& arg1);
public:
protected:
  void focusInEvent (QFocusEvent * arg1);
public:
protected:
  void keyPressEvent (QKeyEvent * arg1);
public:
  const QMetaObject * metaObject () const;
  int devType () const;
  void setVisible (bool arg1);
  QVariant inputMethodQuery (Qt::InputMethodQuery arg1) const;
  QPaintEngine * paintEngine () const;
protected:
  void dragLeaveEvent (QDragLeaveEvent * arg1);
public:
protected:
  void mousePressEvent (QMouseEvent * arg1);
public:
  QSize sizeHint () const;
protected:
  void moveEvent (QMoveEvent * arg1);
public:
protected:
  void tabletEvent (QTabletEvent * arg1);
public:
protected:
  void enterEvent (QEvent * arg1);
public:
protected:
  void closeEvent (QCloseEvent * arg1);
public:
protected:
  void dropEvent (QDropEvent * arg1);
public:
  QSize minimumSizeHint () const;
  QMenu * createPopupMenu ();
protected:
  void actionEvent (QActionEvent * arg1);
public:
protected:
  void showEvent (QShowEvent * arg1);
public:
protected:
  void dragEnterEvent (QDragEnterEvent * arg1);
public:
protected:
  void paintEvent (QPaintEvent * arg1);
public:
protected:
  void dragMoveEvent (QDragMoveEvent * arg1);
public:
protected:
  void contextMenuEvent (QContextMenuEvent * arg1);
public:
protected:
  void enabledChange (bool arg1);
public:
protected:
  void disconnectNotify (const char * arg1);
public:
  int heightForWidth (int arg1) const;
protected:
  void fontChange (const QFont& arg1);
public:
protected:
  bool focusNextPrevChild (bool arg1);
public:
protected:
  void timerEvent (QTimerEvent * arg1);
public:
protected:
  void mouseMoveEvent (QMouseEvent * arg1);
public:
protected:
  void focusOutEvent (QFocusEvent * arg1);
public:
protected:
  void childEvent (QChildEvent * arg1);
public:
protected:
  void connectNotify (const char * arg1);
public:
protected:
  void mouseReleaseEvent (QMouseEvent * arg1);
public:
protected:
  int metric (QPaintDevice::PaintDeviceMetric arg1) const;
public:
  bool eventFilter (QObject * arg1, QEvent * arg2);
protected:
  void inputMethodEvent (QInputMethodEvent * arg1);
public:
protected:
  void wheelEvent (QWheelEvent * arg1);
public:
protected:
  void resizeEvent (QResizeEvent * arg1);
public:
protected:
  void paletteChange (const QPalette& arg1);
public:
protected:
  void languageChange ();
public:
protected:
  bool event (QEvent * arg1);
public:
protected:
  void hideEvent (QHideEvent * arg1);
public:
protected:
  void mouseDoubleClickEvent (QMouseEvent * arg1);
public:
protected:
  void keyReleaseEvent (QKeyEvent * arg1);
public:
protected:
  void windowActivationChange (bool arg1);
public:
protected:
  void leaveEvent (QEvent * arg1);
public:
protected:
  void changeEvent (QEvent * arg1);
public:
protected:
  void customEvent (QEvent * arg1);
public:
  ~LuaBinder< QMainWindow > ();
  static int lqt_pushenum_DockOption (lua_State *L);
  static int lqt_pushenum_DockOption_QFLAGS_CREATOR (lua_State *L);
  LuaBinder< QMainWindow > (lua_State *l, QWidget * arg1, QFlags<Qt::WindowType> arg2):QMainWindow(arg1, arg2), L(l) {}
};

extern "C" int luaopen_QMainWindow (lua_State *L);
