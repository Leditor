#include "lqt_bind_QFont.hpp"

int LuaBinder< QFont >::__LuaWrapCall__setStyleHint (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFont::StyleHint arg1 = static_cast<QFont::StyleHint>(lqtL_toenum(L, 2, "QFont::StyleHint"));
  QFont::StyleStrategy arg2 = lqtL_isenum(L, 3, "QFont::StyleStrategy")?static_cast<QFont::StyleStrategy>(lqtL_toenum(L, 3, "QFont::StyleStrategy")):QFont::PreferDefault;
  __lua__obj->QFont::setStyleHint(arg1, arg2);
  return 0;
}
int LuaBinder< QFont >::__LuaWrapCall__removeSubstitution (lua_State *L) {
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 1), lua_objlen(L, 1));
  QFont::removeSubstitution(arg1);
  return 0;
}
int LuaBinder< QFont >::__LuaWrapCall__setFamily (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QFont::setFamily(arg1);
  return 0;
}
int LuaBinder< QFont >::__LuaWrapCall__underline (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QFont::underline();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QFont >::__LuaWrapCall__setWeight (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QFont::setWeight(arg1);
  return 0;
}
int LuaBinder< QFont >::__LuaWrapCall__italic (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QFont::italic();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QFont >::__LuaWrapCall__key (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QFont::key();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QFont >::__LuaWrapCall__weight (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QFont::weight();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QFont >::__LuaWrapCall__handle (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  long unsigned int ret = __lua__obj->QFont::handle();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QFont >::__LuaWrapCall__setStyle (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFont::Style arg1 = static_cast<QFont::Style>(lqtL_toenum(L, 2, "QFont::Style"));
  __lua__obj->QFont::setStyle(arg1);
  return 0;
}
int LuaBinder< QFont >::__LuaWrapCall__freetypeFace (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  FT_FaceRec_ * ret = __lua__obj->QFont::freetypeFace();
  lqtL_pushudata(L, ret, "FT_FaceRec_*");
  return 1;
}
int LuaBinder< QFont >::__LuaWrapCall__cleanup (lua_State *L) {
  QFont::cleanup();
  return 0;
}
int LuaBinder< QFont >::__LuaWrapCall__setStretch (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QFont::setStretch(arg1);
  return 0;
}
int LuaBinder< QFont >::__LuaWrapCall__setPointSize (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QFont::setPointSize(arg1);
  return 0;
}
int LuaBinder< QFont >::__LuaWrapCall__setKerning (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QFont::setKerning(arg1);
  return 0;
}
int LuaBinder< QFont >::__LuaWrapCall__isCopyOf (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QFont& arg1 = **static_cast<QFont**>(lqtL_checkudata(L, 2, "QFont*"));
  bool ret = __lua__obj->QFont::isCopyOf(arg1);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QFont >::__LuaWrapCall__strikeOut (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QFont::strikeOut();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QFont >::__LuaWrapCall__toString (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QFont::toString();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QFont >::__LuaWrapCall__overline (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QFont::overline();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QFont >::__LuaWrapCall__setStrikeOut (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QFont::setStrikeOut(arg1);
  return 0;
}
int LuaBinder< QFont >::__LuaWrapCall__style (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFont::Style ret = __lua__obj->QFont::style();
  lqtL_pushenum(L, ret, "QFont::Style");
  return 1;
}
int LuaBinder< QFont >::__LuaWrapCall__styleStrategy (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFont::StyleStrategy ret = __lua__obj->QFont::styleStrategy();
  lqtL_pushenum(L, ret, "QFont::StyleStrategy");
  return 1;
}
int LuaBinder< QFont >::__LuaWrapCall__kerning (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QFont::kerning();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QFont >::__LuaWrapCall__resolve__OverloadedVersion__1 (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QFont& arg1 = **static_cast<QFont**>(lqtL_checkudata(L, 2, "QFont*"));
  QFont ret = __lua__obj->QFont::resolve(arg1);
  lqtL_passudata(L, new QFont(ret), "QFont*");
  return 1;
}
int LuaBinder< QFont >::__LuaWrapCall__resolve__OverloadedVersion__2 (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  unsigned int ret = __lua__obj->QFont::resolve();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QFont >::__LuaWrapCall__resolve__OverloadedVersion__3 (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  unsigned int arg1 = lua_tointeger(L, 2);
  __lua__obj->QFont::resolve(arg1);
  return 0;
}
int LuaBinder< QFont >::__LuaWrapCall__resolve (lua_State *L) {
  int score[4];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QFont*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QFont*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1dc8f50;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QFont*")?premium:-premium*premium;
  score[3] = 0;
  score[3] += lqtL_testudata(L, 1, "QFont*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1dc9fa0;
  } else {
    score[3] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=3;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__resolve__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__resolve__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__resolve__OverloadedVersion__3(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__resolve matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QFont >::__LuaWrapCall__setStyleStrategy (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFont::StyleStrategy arg1 = static_cast<QFont::StyleStrategy>(lqtL_toenum(L, 2, "QFont::StyleStrategy"));
  __lua__obj->QFont::setStyleStrategy(arg1);
  return 0;
}
int LuaBinder< QFont >::__LuaWrapCall__pointSize (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QFont::pointSize();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QFont >::__LuaWrapCall__setFixedPitch (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QFont::setFixedPitch(arg1);
  return 0;
}
int LuaBinder< QFont >::__LuaWrapCall__fromString (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  bool ret = __lua__obj->QFont::fromString(arg1);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QFont >::__LuaWrapCall__setPointSizeF (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  __lua__obj->QFont::setPointSizeF(arg1);
  return 0;
}
int LuaBinder< QFont >::__LuaWrapCall__setPixelSize (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QFont::setPixelSize(arg1);
  return 0;
}
int LuaBinder< QFont >::__LuaWrapCall__pointSizeF (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double ret = __lua__obj->QFont::pointSizeF();
  lua_pushnumber(L, ret);
  return 1;
}
int LuaBinder< QFont >::__LuaWrapCall__stretch (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QFont::stretch();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QFont >::__LuaWrapCall__styleHint (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFont::StyleHint ret = __lua__obj->QFont::styleHint();
  lqtL_pushenum(L, ret, "QFont::StyleHint");
  return 1;
}
int LuaBinder< QFont >::__LuaWrapCall__lastResortFont (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QFont::lastResortFont();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QFont >::__LuaWrapCall__substitute (lua_State *L) {
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 1), lua_objlen(L, 1));
  QString ret = QFont::substitute(arg1);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QFont >::__LuaWrapCall__lastResortFamily (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QFont::lastResortFamily();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QFont >::__LuaWrapCall__insertSubstitutions (lua_State *L) {
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 1), lua_objlen(L, 1));
  const QStringList& arg2 = **static_cast<QStringList**>(lqtL_checkudata(L, 2, "QStringList*"));
  QFont::insertSubstitutions(arg1, arg2);
  return 0;
}
int LuaBinder< QFont >::__LuaWrapCall__setOverline (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QFont::setOverline(arg1);
  return 0;
}
int LuaBinder< QFont >::__LuaWrapCall__bold (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QFont::bold();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QFont >::__LuaWrapCall__substitutions (lua_State *L) {
  QStringList ret = QFont::substitutions();
  lqtL_passudata(L, new QStringList(ret), "QStringList*");
  return 1;
}
int LuaBinder< QFont >::__LuaWrapCall__setItalic (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QFont::setItalic(arg1);
  return 0;
}
int LuaBinder< QFont >::__LuaWrapCall__new__OverloadedVersion__1 (lua_State *L) {
  QFont * ret = new LuaBinder< QFont >(L);
  lqtL_passudata(L, ret, "QFont*");
  return 1;
}
int LuaBinder< QFont >::__LuaWrapCall__new__OverloadedVersion__2 (lua_State *L) {
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 1), lua_objlen(L, 1));
  int arg2 = lua_isnumber(L, 2)?lua_tointeger(L, 2):static_cast< int >(-1);
  int arg3 = lua_isnumber(L, 3)?lua_tointeger(L, 3):static_cast< int >(-1);
  bool arg4 = lua_isboolean(L, 4)?(bool)lua_toboolean(L, 4):false;
  QFont * ret = new LuaBinder< QFont >(L, arg1, arg2, arg3, arg4);
  lqtL_passudata(L, ret, "QFont*");
  return 1;
}
int LuaBinder< QFont >::__LuaWrapCall__new__OverloadedVersion__3 (lua_State *L) {
  const QFont& arg1 = **static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
  QPaintDevice * arg2 = *static_cast<QPaintDevice**>(lqtL_checkudata(L, 2, "QPaintDevice*"));
  QFont * ret = new LuaBinder< QFont >(L, arg1, arg2);
  lqtL_passudata(L, ret, "QFont*");
  return 1;
}
int LuaBinder< QFont >::__LuaWrapCall__new__OverloadedVersion__4 (lua_State *L) {
  const QFont& arg1 = **static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
  QFont * ret = new LuaBinder< QFont >(L, arg1);
  lqtL_passudata(L, ret, "QFont*");
  return 1;
}
int LuaBinder< QFont >::__LuaWrapCall__new (lua_State *L) {
  int score[5];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1da57e0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 2)) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x1da5cf0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x1da5c90;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isboolean(L, 4)) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x1da64f0;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  if (lqtL_testudata(L, 1, "QFont*")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1da6dc0;
  } else {
    score[3] -= premium*premium;
  }
  if (lqtL_testudata(L, 2, "QPaintDevice*")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1da6af0;
  } else {
    score[3] -= premium*premium;
  }
  score[4] = 0;
  if (lqtL_testudata(L, 1, "QFont*")) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x1da7b20;
  } else {
    score[4] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=4;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__new__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__new__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__new__OverloadedVersion__3(L); break;
    case 4: return __LuaWrapCall__new__OverloadedVersion__4(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__new matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QFont >::__LuaWrapCall__insertSubstitution (lua_State *L) {
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 1), lua_objlen(L, 1));
  const QString& arg2 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  QFont::insertSubstitution(arg1, arg2);
  return 0;
}
int LuaBinder< QFont >::__LuaWrapCall__delete (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  delete __lua__obj;
  __lua__obj = 0;
  return 0;
}
int LuaBinder< QFont >::__LuaWrapCall__setRawMode (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QFont::setRawMode(arg1);
  return 0;
}
int LuaBinder< QFont >::__LuaWrapCall__setUnderline (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QFont::setUnderline(arg1);
  return 0;
}
int LuaBinder< QFont >::__LuaWrapCall__rawMode (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QFont::rawMode();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QFont >::__LuaWrapCall__pixelSize (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QFont::pixelSize();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QFont >::__LuaWrapCall__exactMatch (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QFont::exactMatch();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QFont >::__LuaWrapCall__substitutes (lua_State *L) {
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 1), lua_objlen(L, 1));
  QStringList ret = QFont::substitutes(arg1);
  lqtL_passudata(L, new QStringList(ret), "QStringList*");
  return 1;
}
int LuaBinder< QFont >::__LuaWrapCall__rawName (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QFont::rawName();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QFont >::__LuaWrapCall__setRawName (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QFont::setRawName(arg1);
  return 0;
}
int LuaBinder< QFont >::__LuaWrapCall__setBold (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QFont::setBold(arg1);
  return 0;
}
int LuaBinder< QFont >::__LuaWrapCall__initialize (lua_State *L) {
  QFont::initialize();
  return 0;
}
int LuaBinder< QFont >::__LuaWrapCall__family (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QFont::family();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QFont >::__LuaWrapCall__defaultFamily (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QFont::defaultFamily();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QFont >::__LuaWrapCall__fixedPitch (lua_State *L) {
  QFont *& __lua__obj = *static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QFont::fixedPitch();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QFont >::__LuaWrapCall__cacheStatistics (lua_State *L) {
  QFont::cacheStatistics();
  return 0;
}
int LuaBinder< QFont >::lqt_pushenum_StyleHint (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "Helvetica");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "Helvetica");
  lua_pushstring(L, "SansSerif");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "SansSerif");
  lua_pushstring(L, "Times");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "Times");
  lua_pushstring(L, "Serif");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "Serif");
  lua_pushstring(L, "Courier");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "Courier");
  lua_pushstring(L, "TypeWriter");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "TypeWriter");
  lua_pushstring(L, "OldEnglish");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "OldEnglish");
  lua_pushstring(L, "Decorative");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "Decorative");
  lua_pushstring(L, "System");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "System");
  lua_pushstring(L, "AnyStyle");
  lua_rawseti(L, enum_table, 5);
  lua_pushinteger(L, 5);
  lua_setfield(L, enum_table, "AnyStyle");
  lua_pushcfunction(L, LuaBinder< QFont >::lqt_pushenum_StyleHint_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QFont::StyleHint");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QFont >::lqt_pushenum_StyleHint_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QFont::StyleHint>*) + sizeof(QFlags<QFont::StyleHint>));
  QFlags<QFont::StyleHint> *fl = static_cast<QFlags<QFont::StyleHint>*>( static_cast<void*>(&static_cast<QFlags<QFont::StyleHint>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QFont::StyleHint>(lqtL_toenum(L, i, "QFont::StyleHint"));
	}
	if (luaL_newmetatable(L, "QFlags<QFont::StyleHint>*")) {
		lua_pushstring(L, "QFlags<QFont::StyleHint>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinder< QFont >::lqt_pushenum_StyleStrategy (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "PreferDefault");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "PreferDefault");
  lua_pushstring(L, "PreferBitmap");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "PreferBitmap");
  lua_pushstring(L, "PreferDevice");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "PreferDevice");
  lua_pushstring(L, "PreferOutline");
  lua_rawseti(L, enum_table, 8);
  lua_pushinteger(L, 8);
  lua_setfield(L, enum_table, "PreferOutline");
  lua_pushstring(L, "ForceOutline");
  lua_rawseti(L, enum_table, 16);
  lua_pushinteger(L, 16);
  lua_setfield(L, enum_table, "ForceOutline");
  lua_pushstring(L, "PreferMatch");
  lua_rawseti(L, enum_table, 32);
  lua_pushinteger(L, 32);
  lua_setfield(L, enum_table, "PreferMatch");
  lua_pushstring(L, "PreferQuality");
  lua_rawseti(L, enum_table, 64);
  lua_pushinteger(L, 64);
  lua_setfield(L, enum_table, "PreferQuality");
  lua_pushstring(L, "PreferAntialias");
  lua_rawseti(L, enum_table, 128);
  lua_pushinteger(L, 128);
  lua_setfield(L, enum_table, "PreferAntialias");
  lua_pushstring(L, "NoAntialias");
  lua_rawseti(L, enum_table, 256);
  lua_pushinteger(L, 256);
  lua_setfield(L, enum_table, "NoAntialias");
  lua_pushstring(L, "OpenGLCompatible");
  lua_rawseti(L, enum_table, 512);
  lua_pushinteger(L, 512);
  lua_setfield(L, enum_table, "OpenGLCompatible");
  lua_pushstring(L, "NoFontMerging");
  lua_rawseti(L, enum_table, 32768);
  lua_pushinteger(L, 32768);
  lua_setfield(L, enum_table, "NoFontMerging");
  lua_pushcfunction(L, LuaBinder< QFont >::lqt_pushenum_StyleStrategy_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QFont::StyleStrategy");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QFont >::lqt_pushenum_StyleStrategy_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QFont::StyleStrategy>*) + sizeof(QFlags<QFont::StyleStrategy>));
  QFlags<QFont::StyleStrategy> *fl = static_cast<QFlags<QFont::StyleStrategy>*>( static_cast<void*>(&static_cast<QFlags<QFont::StyleStrategy>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QFont::StyleStrategy>(lqtL_toenum(L, i, "QFont::StyleStrategy"));
	}
	if (luaL_newmetatable(L, "QFlags<QFont::StyleStrategy>*")) {
		lua_pushstring(L, "QFlags<QFont::StyleStrategy>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinder< QFont >::lqt_pushenum_Weight (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "Light");
  lua_rawseti(L, enum_table, 25);
  lua_pushinteger(L, 25);
  lua_setfield(L, enum_table, "Light");
  lua_pushstring(L, "Normal");
  lua_rawseti(L, enum_table, 50);
  lua_pushinteger(L, 50);
  lua_setfield(L, enum_table, "Normal");
  lua_pushstring(L, "DemiBold");
  lua_rawseti(L, enum_table, 63);
  lua_pushinteger(L, 63);
  lua_setfield(L, enum_table, "DemiBold");
  lua_pushstring(L, "Bold");
  lua_rawseti(L, enum_table, 75);
  lua_pushinteger(L, 75);
  lua_setfield(L, enum_table, "Bold");
  lua_pushstring(L, "Black");
  lua_rawseti(L, enum_table, 87);
  lua_pushinteger(L, 87);
  lua_setfield(L, enum_table, "Black");
  lua_pushcfunction(L, LuaBinder< QFont >::lqt_pushenum_Weight_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QFont::Weight");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QFont >::lqt_pushenum_Weight_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QFont::Weight>*) + sizeof(QFlags<QFont::Weight>));
  QFlags<QFont::Weight> *fl = static_cast<QFlags<QFont::Weight>*>( static_cast<void*>(&static_cast<QFlags<QFont::Weight>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QFont::Weight>(lqtL_toenum(L, i, "QFont::Weight"));
	}
	if (luaL_newmetatable(L, "QFlags<QFont::Weight>*")) {
		lua_pushstring(L, "QFlags<QFont::Weight>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinder< QFont >::lqt_pushenum_Style (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "StyleNormal");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "StyleNormal");
  lua_pushstring(L, "StyleItalic");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "StyleItalic");
  lua_pushstring(L, "StyleOblique");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "StyleOblique");
  lua_pushcfunction(L, LuaBinder< QFont >::lqt_pushenum_Style_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QFont::Style");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QFont >::lqt_pushenum_Style_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QFont::Style>*) + sizeof(QFlags<QFont::Style>));
  QFlags<QFont::Style> *fl = static_cast<QFlags<QFont::Style>*>( static_cast<void*>(&static_cast<QFlags<QFont::Style>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QFont::Style>(lqtL_toenum(L, i, "QFont::Style"));
	}
	if (luaL_newmetatable(L, "QFlags<QFont::Style>*")) {
		lua_pushstring(L, "QFlags<QFont::Style>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinder< QFont >::lqt_pushenum_Stretch (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "UltraCondensed");
  lua_rawseti(L, enum_table, 50);
  lua_pushinteger(L, 50);
  lua_setfield(L, enum_table, "UltraCondensed");
  lua_pushstring(L, "ExtraCondensed");
  lua_rawseti(L, enum_table, 62);
  lua_pushinteger(L, 62);
  lua_setfield(L, enum_table, "ExtraCondensed");
  lua_pushstring(L, "Condensed");
  lua_rawseti(L, enum_table, 75);
  lua_pushinteger(L, 75);
  lua_setfield(L, enum_table, "Condensed");
  lua_pushstring(L, "SemiCondensed");
  lua_rawseti(L, enum_table, 87);
  lua_pushinteger(L, 87);
  lua_setfield(L, enum_table, "SemiCondensed");
  lua_pushstring(L, "Unstretched");
  lua_rawseti(L, enum_table, 100);
  lua_pushinteger(L, 100);
  lua_setfield(L, enum_table, "Unstretched");
  lua_pushstring(L, "SemiExpanded");
  lua_rawseti(L, enum_table, 112);
  lua_pushinteger(L, 112);
  lua_setfield(L, enum_table, "SemiExpanded");
  lua_pushstring(L, "Expanded");
  lua_rawseti(L, enum_table, 125);
  lua_pushinteger(L, 125);
  lua_setfield(L, enum_table, "Expanded");
  lua_pushstring(L, "ExtraExpanded");
  lua_rawseti(L, enum_table, 150);
  lua_pushinteger(L, 150);
  lua_setfield(L, enum_table, "ExtraExpanded");
  lua_pushstring(L, "UltraExpanded");
  lua_rawseti(L, enum_table, 200);
  lua_pushinteger(L, 200);
  lua_setfield(L, enum_table, "UltraExpanded");
  lua_pushcfunction(L, LuaBinder< QFont >::lqt_pushenum_Stretch_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QFont::Stretch");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QFont >::lqt_pushenum_Stretch_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QFont::Stretch>*) + sizeof(QFlags<QFont::Stretch>));
  QFlags<QFont::Stretch> *fl = static_cast<QFlags<QFont::Stretch>*>( static_cast<void*>(&static_cast<QFlags<QFont::Stretch>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QFont::Stretch>(lqtL_toenum(L, i, "QFont::Stretch"));
	}
	if (luaL_newmetatable(L, "QFlags<QFont::Stretch>*")) {
		lua_pushstring(L, "QFlags<QFont::Stretch>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int luaopen_QFont (lua_State *L) {
  if (luaL_newmetatable(L, "QFont*")) {
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__setStyleHint);
    lua_setfield(L, -2, "setStyleHint");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__removeSubstitution);
    lua_setfield(L, -2, "removeSubstitution");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__setFamily);
    lua_setfield(L, -2, "setFamily");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__underline);
    lua_setfield(L, -2, "underline");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__setWeight);
    lua_setfield(L, -2, "setWeight");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__italic);
    lua_setfield(L, -2, "italic");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__key);
    lua_setfield(L, -2, "key");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__weight);
    lua_setfield(L, -2, "weight");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__handle);
    lua_setfield(L, -2, "handle");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__setStyle);
    lua_setfield(L, -2, "setStyle");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__freetypeFace);
    lua_setfield(L, -2, "freetypeFace");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__cleanup);
    lua_setfield(L, -2, "cleanup");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__setStretch);
    lua_setfield(L, -2, "setStretch");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__setPointSize);
    lua_setfield(L, -2, "setPointSize");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__setKerning);
    lua_setfield(L, -2, "setKerning");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__isCopyOf);
    lua_setfield(L, -2, "isCopyOf");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__strikeOut);
    lua_setfield(L, -2, "strikeOut");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__toString);
    lua_setfield(L, -2, "toString");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__overline);
    lua_setfield(L, -2, "overline");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__setStrikeOut);
    lua_setfield(L, -2, "setStrikeOut");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__style);
    lua_setfield(L, -2, "style");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__styleStrategy);
    lua_setfield(L, -2, "styleStrategy");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__kerning);
    lua_setfield(L, -2, "kerning");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__resolve);
    lua_setfield(L, -2, "resolve");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__setStyleStrategy);
    lua_setfield(L, -2, "setStyleStrategy");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__pointSize);
    lua_setfield(L, -2, "pointSize");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__setFixedPitch);
    lua_setfield(L, -2, "setFixedPitch");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__fromString);
    lua_setfield(L, -2, "fromString");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__setPointSizeF);
    lua_setfield(L, -2, "setPointSizeF");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__setPixelSize);
    lua_setfield(L, -2, "setPixelSize");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__pointSizeF);
    lua_setfield(L, -2, "pointSizeF");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__stretch);
    lua_setfield(L, -2, "stretch");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__styleHint);
    lua_setfield(L, -2, "styleHint");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__lastResortFont);
    lua_setfield(L, -2, "lastResortFont");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__substitute);
    lua_setfield(L, -2, "substitute");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__lastResortFamily);
    lua_setfield(L, -2, "lastResortFamily");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__insertSubstitutions);
    lua_setfield(L, -2, "insertSubstitutions");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__setOverline);
    lua_setfield(L, -2, "setOverline");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__bold);
    lua_setfield(L, -2, "bold");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__substitutions);
    lua_setfield(L, -2, "substitutions");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__setItalic);
    lua_setfield(L, -2, "setItalic");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__new);
    lua_setfield(L, -2, "new");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__insertSubstitution);
    lua_setfield(L, -2, "insertSubstitution");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__delete);
    lua_setfield(L, -2, "delete");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__setRawMode);
    lua_setfield(L, -2, "setRawMode");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__setUnderline);
    lua_setfield(L, -2, "setUnderline");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__rawMode);
    lua_setfield(L, -2, "rawMode");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__pixelSize);
    lua_setfield(L, -2, "pixelSize");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__exactMatch);
    lua_setfield(L, -2, "exactMatch");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__substitutes);
    lua_setfield(L, -2, "substitutes");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__rawName);
    lua_setfield(L, -2, "rawName");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__setRawName);
    lua_setfield(L, -2, "setRawName");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__setBold);
    lua_setfield(L, -2, "setBold");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__initialize);
    lua_setfield(L, -2, "initialize");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__family);
    lua_setfield(L, -2, "family");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__defaultFamily);
    lua_setfield(L, -2, "defaultFamily");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__fixedPitch);
    lua_setfield(L, -2, "fixedPitch");
    lua_pushcfunction(L, LuaBinder< QFont >::__LuaWrapCall__cacheStatistics);
    lua_setfield(L, -2, "cacheStatistics");
    LuaBinder< QFont >::lqt_pushenum_StyleHint(L);
    lua_setfield(L, -2, "StyleHint");
    LuaBinder< QFont >::lqt_pushenum_StyleStrategy(L);
    lua_setfield(L, -2, "StyleStrategy");
    LuaBinder< QFont >::lqt_pushenum_Weight(L);
    lua_setfield(L, -2, "Weight");
    LuaBinder< QFont >::lqt_pushenum_Style(L);
    lua_setfield(L, -2, "Style");
    LuaBinder< QFont >::lqt_pushenum_Stretch(L);
    lua_setfield(L, -2, "Stretch");
    lua_newtable(L);
    lua_setfield(L, -2, "__base");
    lua_pushcfunction(L, lqtL_newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, lqtL_index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, lqtL_gc);
    lua_setfield(L, -2, "__gc");
    lua_pushstring(L, "QFont");
    lua_setfield(L, -2, "__qtype");
		lua_setglobal(L, "QFont");
  } else {
		lua_pop(L, 1);
	}
  return 0;
}
