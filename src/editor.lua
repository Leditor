#!/usr/bin/lua

if not QObject then
	require'qt'
	require'QAbstractScrollArea'
	require'QAction'
	require'QApplication'
	require'QBoxLayout'
	require'QColor'
	require'QCoreApplication'
	require'QEvent'
	require'QFileDialog'
	require'QFont'
	require'QFrame'
	require'QGraphicsItem'
	require'QGraphicsScene'
	require'QGraphicsView'
	require'QHBoxLayout'
	require'QIcon'
	require'QInputEvent'
	require'QKeyEvent'
	require'QKeySequence'
	require'QLabel'
	require'QLayout'
	require'QLayoutItem'
	require'QLineEdit'
	require'QMainWindow'
	require'QMenuBar'
	require'QMenu'
	require'QMessageBox'
	require'QObject'
	require'QPaintDevice'
	require'QShortcut'
	require'QSize'
	require'QSyntaxHighlighter'
	require'Qt'
	require'QTextBlockUserData'
	require'QTextCharFormat'
	require'QTextCursor'
	require'QTextDocument'
	require'QTextEdit'
	require'QToolBar'
	require'QVBoxLayout'
	require'QWidget'
else
	qt = { signal = signal, derive = derive,
	pass = function(...) return ... end, pick = function(...) return ... end,
	slot = function(a)
		if type(a)=='string' then return '1'..a end
		if type(a)=='function' then return slot_from_function(a) end
	end,
	}
end


local sc
local args = { "editor", "-sync" }
--print(#args)
app = qt.pass(QApplication.new(#args, args))

mw = QMainWindow.new()
mw:setWindowTitle'Leditor'
mw:show()
mw:resize(800, 600)



te = QTextEdit.new()
te:setFont(QFont.new'Monospace')
te:setAcceptRichText(false)
--te:setTabStopWidth(20)
--te.starting_document = te:document()
do
  local sd = te.setDocument
  local olddocs = {}
  function te:setDocument (d)
    local olddoc = self:document()
    table.insert(olddocs, olddoc)
    --print(d, olddoc)
    sd(self, d)
    ls:setDocument(d)
    d:connect(qt.signal'destroyed()', qt.slot(
      function ()
        d:disconnect(qt.signal'destroyed()')
        -- shouldn't need more than this. title and filename will be erased later
        if QTextDocument.openDocuments[d] then
          QTextDocument.openDocuments[d] = nil
        end
        local back_doc = table.remove(olddocs)
        while back_doc and not QTextDocument.openDocuments[back_doc] do back_doc = table.remove(olddocs) end
        self:setDocument(back_doc or QTextDocument.new())
      end
    ))
    local title = d:title() or d:fileName() or '<Untitled>'
    mw:setWindowTitle('Leditor: '..title)
  end
end

le = QLineEdit.new()
le:setFont(QFont.new'Monospace')
le.m_history = {}
le.m_history_index = 0
function le:addHistoryLine(t)
  if t=='' then return false end
  table.insert(self.m_history, t)
  self.m_history_index = table.maxn(self.m_history) + 1
end
function le:adjustHistory()
  if type(self.m_history[self.m_history_index])=='string' then
    self:setText(self.m_history[self.m_history_index])
  else
    self.m_history_index = table.maxn(self.m_history) + 1
    self:setText(self.m_current_line or '')
  end
end
function le:goToHistoryIndex(i)
  self.m_history_index = tonumber(i)
  self:adjustHistory()
end
function le:historyBack()
  self.m_history_index = self.m_history_index - 1
  self:adjustHistory()
end
function le:historyForward()
  self.m_history_index = self.m_history_index + 1
  self:adjustHistory()
end
sc = QShortcut.new(QKeySequence.new(Qt.Key['Key_Up']), le) qt.pass(sc)
sc:setContext'WidgetShortcut'
sc:connect(qt.signal'activated()', qt.slot(
function()
  le:historyBack()
end
))
sc = QShortcut.new(QKeySequence.new(Qt.Key['Key_Down']), le) qt.pass(sc)
sc:setContext'WidgetShortcut'
sc:connect(qt.signal'activated()', qt.slot(
function()
  le:historyForward()
end
))

-- OK

el = QVBoxLayout.new()
editor = QWidget.new()
editor:setLayout(qt.pass(el))
el:addWidget(qt.pass(te))
el:addWidget(qt.pass(le))

mw:setCentralWidget(editor)

ls = QSyntaxHighlighter.new(editor)
qt.derive(ls)

local keywords = {
  ['for'] = true,
  ['in'] = true,
  ['do'] = true,
  ['end'] = true,
  ['function'] = true,
  ['while'] = true,
  ['local'] = true,
  ['if'] = true,
  ['then'] = true,
  ['break'] = true,
  ['return'] = true,
}
function ls:highlightBlock(text)
  --print(text)
  for m, f, l in string.gmatch(text, '(()[%w_]+())') do
    --print(f, l)
    --print(QFont.Weight.Bold)
    if keywords[m] then
      self:setFormat(f-1, l-f, QFont.new('Monospace', 10, QFont.Weight.Bold))
    end
  end
end
ls:setDocument(te:document())

-- CONFIG

--table.foreach( getmetatable(QMessageBox.StandardButton.QFlags(QMessageBox.StandardButton.Discard, QMessageBox.StandardButton.Cancel)), print )
--print( QMessageBox.StandardButton.QFlags(QMessageBox.StandardButton.Discard, QMessageBox.StandardButton.Cancel) )
--print( QMessageBox.StandardButton.QFlags(QMessageBox.StandardButton.Discard, QMessageBox.StandardButton.Cancel).__qtype )
--print(QMessageBox.StandardButton.Discard)

--[[
function QTextDocument:discard(sure)
  if self:isModified() and not sure then
    local bpress = QMessageBox.question(mw, "Discard File?", "There are unsaved changes too this file. Proceeding will discard these changes", QMessageBox.StandardButton.QFlags('Discard', 'Cancel'))
    if bpress=='Discard' then
    elseif bpress=='Cancel' then
      error'Action Canceled'
    end
  end
  te.documents[self] = { files={} }
  self:clear()
  --te.documents[self].files = {}
end
function QTextDocument:load(f)
  self:discard()
  f = f or QFileDialog.getOpenFileName(nil, "Open Document")
  self:setMetaInformation(QTextDocument.MetaInformation.DocumentTitle, f)
  local file, err = io.open(f, "r")
  if not file then error(err) end
  self:setPlainText(file:read'*a')
  file:close()
end
--]]

QTextDocument.titles = {}
QTextDocument.filenames = {}
QTextDocument.openDocuments = {}

function QTextDocument:fileName ()
  return QTextDocument.filenames[self]
end
function QTextDocument:title ()
  return QTextDocument.titles[self]
end

function QTextDocument:setFileName (fn)
  QTextDocument.filenames[self] = fn
end
function QTextDocument:setTitle (t)
  QTextDocument.titles[self] = t
end

do
  local oldnew = QTextDocument.new
  local olddelete = QTextDocument.delete
  QTextDocument.new = function(...)
    local ret = oldnew(...)
    ret.openDocuments[ret] = true
    return ret
  end
  QTextDocument.delete = function(doc, ...)
    doc.openDocuments[doc] = nil
    QTextDocument.filenames[doc] = nil
    QTextDocument.titles[doc] = nil
    return olddelete(doc, ...)
  end
end


function QTextDocument:close ()
  if not self.openDocuments[self] then return end
  if self:fileName() and self:isModified() then
    local bpress = QMessageBox.question(mw, "Discard File?", "There are unsaved changes too this file. Proceeding will discard these changes", QMessageBox.StandardButton.QFlags('Discard', 'Save', 'Cancel'))
    if bpress=='Discard' then
      print'Discarding'
    elseif bpress=='Save' then
      self:save()
    else
      error'Action Canceled'
    end
  end
  self:delete()
end

function QTextDocument:save (f)
  local text = self:toPlainText()
  f = f or self:fileName() or ''
  -- particularly proud of the next line as it uses the difference between a?b:c and (a and b) or c
  f = f~='' and f or QFileDialog.getSaveFileName(nil, "Save Document As")
  if type(f)=='string' then
    self:setFileName(f)
  end
  local file, err = io.open(f, "w")
  if not file then error(err) end
  file:write(text)
  file:close()
  self:setFileName(f)
  --self:setModified(false)
end

function te:save(...)
  return te:document():save(...)
end
function te:closeDocument(...)
  return te:document():close(...)
end

function te:loadfile(fn)
  local doc = nil
  for d, n in pairs(QTextDocument.filenames) do
    if n==fn then
      --print'reused'
      return self:setDocument(d)
    end
  end
  -- FIXME: reusing starting document seems broken
  --[[
  if nil and not doc and self.starting_document then
    doc = self.starting_document
    self.starting_document = nil
  end
  --]]
  local text, filename = '', nil
  fn = fn~='' and fn or QFileDialog.getOpenFileName(nil, "Open Document")
  if fn~='' then
    local file, err = io.open(fn, "r")
    if not file then error('Cannot open file '..fn..': '..err) end
    text = file:read'*a'
    file:close()
    filename = fn
    if not doc then doc = QTextDocument.new() end
    if not doc then error'Cannot spawn a new document' end
    doc:setPlainText(text)
    if filename then doc:setFileName(filename) end
    self:setDocument(doc)
  end
end
function te:loaddoc(d)
  if type(obj)=='userdata' and obj.__qtype=='QTextDocument' then
    return self:setDocument(d)
  else
    return self:setDocument(QTextDocument.new())
  end
end
function te:load(obj)
  if type(obj)=='string' then
    te:loadfile(obj)
  elseif type(obj)=='userdata' and obj.__qtype=='QTextDocument' then
    te:loaddoc(obj)
  elseif obj==nil then
		te:setDocument(QTextDocument.new())
  --elseif type(obj)=='number' and QTextDocument.openDocuments[obj] then
    --te:loaddoc(QTextDocument.openDocuments[obj])
  end
end
function te:next(obj)
  local init = self:document()
  init = init.openDocuments[init] and init or nil
  local newdoc = next(init.openDocuments, init)
  if not newdoc then newdoc = next(init.openDocuments, nil) end
  self:setDocument(newdoc)
end


--[[
function preprocess (widget)
end

qt.connect(le, qt.signal'textChanged(const QString&)', qt.slot(
function(...)
  --print'textChanged'
  preprocess(le)
end
))
-- ]]

--[[
for k, v in pairs(Qt.Key) do
  if k~=Qt.Key[v] then print(k, v, Qt.Key[v]) end
end
-- ]]

sc = QShortcut.new(QKeySequence.new(Qt.Key['Key_Less']+Qt.Modifier['CTRL']), mw)

sc:connect(qt.signal'activated()', qt.slot(
function()
if not le:hasFocus() then
  le:setFocus()
else
  te:setFocus()
end
end))

function vim_std_command(text)
  local cmd, args = string.match(text, '^:(%w+)%s*(.*)$')
  args = string.match(args, '^(.*[^%s]*)%s*$')
  --print(text, cmd, args)
  if string.match('quit', cmd) then
    mw:close()
  elseif string.match('edit', cmd) then
    --print('edit', args)
    te:load(args~='' and args or nil)
  elseif string.match('write', cmd) then
    te:save(args~='' and args or nil)
  elseif string.match('tetris', cmd) then
		if tetris then
			tetris:show()
		else
			tetris = dofile'tetris.lua'
			tetris:show()
		end
  elseif string.match('substitute', cmd) then
		local pat, sub, opt = string.match(args, '/(.*)/(.*)/([%dgc]*)')
		opt = opt or ''
		local cursor = te:textCursor()
		if not cursor:hasSelection() then
      cursor:select'LineUnderCursor'
		end
		local txt = cursor:selectedText()
		local n = 1
		if string.match(opt, 'g') then
			n=nil
		elseif string.match(opt, '%d+') and tonumber(string.match(opt, '%d+'))>0 then
			n = tonumber(string.match(opt, '%d*'))
		end
		local ret = string.gsub(txt, pat, sub, n)
    cursor:insertText(ret)
  elseif string.match('close', cmd) then
    te:closeDocument()
  end
end

function vim_search_command(text)
  local dir, args = string.match(text, '^([/?])(.*)$')
  --print(text, dir, args)
  local pos = te:textCursor():position()
  local found = te:find(args, dir=='?' and QTextDocument.FindFlag.QFlags'FindBackward' or nil)
  --print(found)
end


interpreters = {
  [":"] = vim_std_command,
  ["/"] = vim_search_command,
  ["?"] = vim_search_command,
}
function process (text)
  --print('processing', text)
  local init = string.sub(text, 1, 1)
  if interpreters[init] then
    local st, err = pcall(interpreters[init], text)
    if not st then
      error(err)
    else
      return err
    end
  else
    local st, err = loadstring(text)
    --print(st, err)
    if st then st, err = pcall(st) end
    --print(st, err)
    if not st then error(err) end
  end
end
QObject.connect(
le, qt.signal'returnPressed()', qt.slot(
function()
  --print'returnPressed'
  local text = le:text()
  local st, err = pcall(process, text)
  if not st then
    QMessageBox.critical(mw, "Error", 'Error while processing: "'..text..'":\n'..err)
  end
  le:addHistoryLine(text)
	--[[
  print('===========', text)
  see_doc(te:document())
  all_docs()
	--]]
  le:clear()
  --te:setFocus()
end
))

function see_doc(k,v) print(k, k:title(), k:fileName()) end
function all_docs()
  table.foreach(QTextDocument.openDocuments, function(k) print(k, k:title(), k:fileName()) end)
end

mb = QMenuBar.new()
mw:setMenuBar(qt.pass(mb))
tb = QMenu.new'File'
mb:addMenu(qt.pass(tb))
sw = tb:addAction'New' sw:connect(qt.signal'triggered(bool)', qt.slot(function(b) te:setDocument(QTextDocument.new()) end), qt.slot'function(bool)')
tb:addSeparator()
sl = tb:addAction'Load' sl:connect(qt.signal'triggered(bool)', qt.slot(function(b) te:loadfile() end), qt.slot'function(bool)')
tb:addSeparator()
sv = tb:addAction'Save' sv:connect(qt.signal'triggered(bool)', qt.slot(function(b) te:save() end), qt.slot'function(bool)')
sa = tb:addAction'Save As' sa:connect(qt.signal'triggered(bool)', qt.slot(function(b) te:save'' end), qt.slot'function(bool)')
tb:addSeparator()
sn = tb:addAction'Next' sn:connect(qt.signal'triggered(bool)', qt.slot(function(b) te:next() end), qt.slot'function(bool)')
sc = tb:addAction'Close' sc:connect(qt.signal'triggered(bool)', qt.slot(function(b) te:closeDocument() end), qt.slot'function(bool)')

--[[
qt.pass(sw)
qt.pass(sl)
qt.pass(sv)
qt.pass(sa)
qt.pass(sn)
qt.pass(sc)
]]

sw:setIcon(QIcon.new'/usr/kde/3.5/share/icons/crystalsvg/32x32/actions/filenew.png')
sl:setIcon(QIcon.new'/usr/kde/3.5/share/icons/crystalsvg/32x32/actions/fileopen.png')
sv:setIcon(QIcon.new'/usr/kde/3.5/share/icons/crystalsvg/32x32/actions/filesave.png')
sa:setIcon(QIcon.new'/usr/kde/3.5/share/icons/crystalsvg/32x32/actions/filesaveas.png')
sc:setIcon(QIcon.new'/usr/kde/3.5/share/icons/crystalsvg/32x32/actions/fileclose.png')
sn:setIcon(QIcon.new'/usr/kde/3.5/share/icons/crystalsvg/32x32/actions/next.png')


st = app:exec()
app:delete()
return st

