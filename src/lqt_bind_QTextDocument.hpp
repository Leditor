#include "lqt_common.hpp"
#include <QTextDocument>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QTextDocument > : public QTextDocument {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__objectForFormat (lua_State *L);
  static int __LuaWrapCall__defaultFont (lua_State *L);
  static int __LuaWrapCall__isRedoAvailable (lua_State *L);
  static int __LuaWrapCall__useDesignMetrics (lua_State *L);
  static int __LuaWrapCall__print (lua_State *L);
  static int __LuaWrapCall__delete (lua_State *L);
  static int __LuaWrapCall__setUseDesignMetrics (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__trUtf8 (lua_State *L);
  static int __LuaWrapCall__object (lua_State *L);
  static int __LuaWrapCall__setUndoRedoEnabled (lua_State *L);
  static int __LuaWrapCall__size (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__tr (lua_State *L);
  static int __LuaWrapCall__rootFrame (lua_State *L);
  static int __LuaWrapCall__addResource (lua_State *L);
  static int __LuaWrapCall__setPlainText (lua_State *L);
  static int __LuaWrapCall__setDefaultStyleSheet (lua_State *L);
  static int __LuaWrapCall__isEmpty (lua_State *L);
  static int __LuaWrapCall__find__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__find__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__find__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__find__OverloadedVersion__4 (lua_State *L);
  static int __LuaWrapCall__find (lua_State *L);
  static int __LuaWrapCall__isUndoRedoEnabled (lua_State *L);
  static int __LuaWrapCall__begin (lua_State *L);
  static int __LuaWrapCall__isUndoAvailable (lua_State *L);
  static int __LuaWrapCall__adjustSize (lua_State *L);
  static int __LuaWrapCall__markContentsDirty (lua_State *L);
  static int __LuaWrapCall__clone (lua_State *L);
  static int __LuaWrapCall__defaultStyleSheet (lua_State *L);
  static int __LuaWrapCall__toPlainText (lua_State *L);
  static int __LuaWrapCall__setModified (lua_State *L);
  static int __LuaWrapCall__maximumBlockCount (lua_State *L);
  static int __LuaWrapCall__appendUndoItem (lua_State *L);
  static int __LuaWrapCall__pageCount (lua_State *L);
  static int __LuaWrapCall__documentLayout (lua_State *L);
  static int __LuaWrapCall__allFormats (lua_State *L);
  static int __LuaWrapCall__setMaximumBlockCount (lua_State *L);
  static int __LuaWrapCall__setDefaultTextOption (lua_State *L);
  static int __LuaWrapCall__defaultTextOption (lua_State *L);
  static int __LuaWrapCall__isModified (lua_State *L);
  static int __LuaWrapCall__metaInformation (lua_State *L);
  static int __LuaWrapCall__resource (lua_State *L);
  static int __LuaWrapCall__end (lua_State *L);
  static int __LuaWrapCall__setDefaultFont (lua_State *L);
  static int __LuaWrapCall__toHtml (lua_State *L);
  static int __LuaWrapCall__metaObject (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__new (lua_State *L);
  static int __LuaWrapCall__clear (lua_State *L);
  static int __LuaWrapCall__setDocumentLayout (lua_State *L);
  static int __LuaWrapCall__redo__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__redo__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__redo (lua_State *L);
  static int __LuaWrapCall__findBlock (lua_State *L);
  static int __LuaWrapCall__setTextWidth (lua_State *L);
  static int __LuaWrapCall__textWidth (lua_State *L);
  static int __LuaWrapCall__idealWidth (lua_State *L);
  static int __LuaWrapCall__setHtml (lua_State *L);
  static int __LuaWrapCall__blockCount (lua_State *L);
  static int __LuaWrapCall__setMetaInformation (lua_State *L);
  static int __LuaWrapCall__frameAt (lua_State *L);
  static int __LuaWrapCall__drawContents (lua_State *L);
  static int __LuaWrapCall__undo__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__undo__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__undo (lua_State *L);
  static int __LuaWrapCall__docHandle (lua_State *L);
  static int __LuaWrapCall__pageSize (lua_State *L);
  static int __LuaWrapCall__setPageSize (lua_State *L);
  bool eventFilter (QObject * arg1, QEvent * arg2);
  const QMetaObject * metaObject () const;
protected:
  void connectNotify (const char * arg1);
public:
  bool event (QEvent * arg1);
  void clear ();
protected:
  void timerEvent (QTimerEvent * arg1);
public:
protected:
  void disconnectNotify (const char * arg1);
public:
protected:
  QVariant loadResource (int arg1, const QUrl& arg2);
public:
protected:
  QTextObject * createObject (const QTextFormat& arg1);
public:
protected:
  void childEvent (QChildEvent * arg1);
public:
protected:
  void customEvent (QEvent * arg1);
public:
  ~LuaBinder< QTextDocument > ();
  static int lqt_pushenum_MetaInformation (lua_State *L);
  static int lqt_pushenum_MetaInformation_QFLAGS_CREATOR (lua_State *L);
  static int lqt_pushenum_FindFlag (lua_State *L);
  static int lqt_pushenum_FindFlag_QFLAGS_CREATOR (lua_State *L);
  static int lqt_pushenum_ResourceType (lua_State *L);
  static int lqt_pushenum_ResourceType_QFLAGS_CREATOR (lua_State *L);
  LuaBinder< QTextDocument > (lua_State *l, QObject * arg1):QTextDocument(arg1), L(l) {}
  LuaBinder< QTextDocument > (lua_State *l, const QString& arg1, QObject * arg2):QTextDocument(arg1, arg2), L(l) {}
};

extern "C" int luaopen_QTextDocument (lua_State *L);
