#include "lqt_common.hpp"
#include <QAction>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QAction > : public QAction {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__parentWidget (lua_State *L);
  static int __LuaWrapCall__setChecked (lua_State *L);
  static int __LuaWrapCall__statusTip (lua_State *L);
  static int __LuaWrapCall__setVisible (lua_State *L);
  static int __LuaWrapCall__setIcon (lua_State *L);
  static int __LuaWrapCall__isSeparator (lua_State *L);
  static int __LuaWrapCall__toggle (lua_State *L);
  static int __LuaWrapCall__icon (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__tr (lua_State *L);
  static int __LuaWrapCall__isVisible (lua_State *L);
  static int __LuaWrapCall__whatsThis (lua_State *L);
  static int __LuaWrapCall__toolTip (lua_State *L);
  static int __LuaWrapCall__setIconText (lua_State *L);
  static int __LuaWrapCall__shortcut (lua_State *L);
  static int __LuaWrapCall__iconText (lua_State *L);
  static int __LuaWrapCall__shortcutContext (lua_State *L);
  static int __LuaWrapCall__setToolTip (lua_State *L);
  static int __LuaWrapCall__setMenu (lua_State *L);
  static int __LuaWrapCall__showStatusText (lua_State *L);
  static int __LuaWrapCall__setAutoRepeat (lua_State *L);
  static int __LuaWrapCall__setCheckable (lua_State *L);
  static int __LuaWrapCall__setSeparator (lua_State *L);
  static int __LuaWrapCall__activate (lua_State *L);
  static int __LuaWrapCall__trigger (lua_State *L);
  static int __LuaWrapCall__shortcuts (lua_State *L);
  static int __LuaWrapCall__associatedWidgets (lua_State *L);
  static int __LuaWrapCall__setText (lua_State *L);
  static int __LuaWrapCall__data (lua_State *L);
  static int __LuaWrapCall__isCheckable (lua_State *L);
  static int __LuaWrapCall__setStatusTip (lua_State *L);
  static int __LuaWrapCall__setDisabled (lua_State *L);
  static int __LuaWrapCall__setShortcutContext (lua_State *L);
  static int __LuaWrapCall__setActionGroup (lua_State *L);
  static int __LuaWrapCall__autoRepeat (lua_State *L);
  static int __LuaWrapCall__setFont (lua_State *L);
  static int __LuaWrapCall__metaObject (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__new (lua_State *L);
  static int __LuaWrapCall__hover (lua_State *L);
  static int __LuaWrapCall__delete (lua_State *L);
  static int __LuaWrapCall__font (lua_State *L);
  static int __LuaWrapCall__actionGroup (lua_State *L);
  static int __LuaWrapCall__setShortcut (lua_State *L);
  static int __LuaWrapCall__setShortcuts__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__setShortcuts__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__setShortcuts (lua_State *L);
  static int __LuaWrapCall__isChecked (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__trUtf8 (lua_State *L);
  static int __LuaWrapCall__setMenuRole (lua_State *L);
  static int __LuaWrapCall__menuRole (lua_State *L);
  static int __LuaWrapCall__menu (lua_State *L);
  static int __LuaWrapCall__setWhatsThis (lua_State *L);
  static int __LuaWrapCall__isEnabled (lua_State *L);
  static int __LuaWrapCall__setData (lua_State *L);
  static int __LuaWrapCall__setEnabled (lua_State *L);
  static int __LuaWrapCall__text (lua_State *L);
protected:
  bool event (QEvent * arg1);
public:
protected:
  void connectNotify (const char * arg1);
public:
  bool eventFilter (QObject * arg1, QEvent * arg2);
protected:
  void disconnectNotify (const char * arg1);
public:
protected:
  void childEvent (QChildEvent * arg1);
public:
protected:
  void timerEvent (QTimerEvent * arg1);
public:
  const QMetaObject * metaObject () const;
protected:
  void customEvent (QEvent * arg1);
public:
  ~LuaBinder< QAction > ();
  static int lqt_pushenum_MenuRole (lua_State *L);
  static int lqt_pushenum_MenuRole_QFLAGS_CREATOR (lua_State *L);
  static int lqt_pushenum_ActionEvent (lua_State *L);
  static int lqt_pushenum_ActionEvent_QFLAGS_CREATOR (lua_State *L);
  LuaBinder< QAction > (lua_State *l, QObject * arg1):QAction(arg1), L(l) {}
  LuaBinder< QAction > (lua_State *l, const QString& arg1, QObject * arg2):QAction(arg1, arg2), L(l) {}
  LuaBinder< QAction > (lua_State *l, const QIcon& arg1, const QString& arg2, QObject * arg3):QAction(arg1, arg2, arg3), L(l) {}
};

extern "C" int luaopen_QAction (lua_State *L);
