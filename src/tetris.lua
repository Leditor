#!/usr/bin/lua

---[[

require'QTimer'

--[[
local tetris = QGraphicsView.new()
local game = QGraphicsScene.new()
tetris:setScene(qt.pass(game))


tetris:setViewportUpdateMode'NoViewportUpdate'
--]]

local tetris = QTextEdit.new()
tetris:setFont(QFont.new('Monospace', 20))
qt.derive(tetris)
tetris:setReadOnly(true)
tetris:setFixedSize(200,400)
tetris:show()

tetris.lines = { }
tetris.piece = { }

local pieces = {
  --X1 = { cw='X2', ccw='X2', desc = {}, },
  --X2 = { cw='X1', ccw='X1', desc = {}, },
  H1 = { cw='H2', ccw='H2', desc = {}, },
  H2 = { cw='H1', ccw='H1', desc = {}, },
  Z1 = { cw='Z2', ccw='Z2', desc = {}, },
  Z2 = { cw='Z1', ccw='Z1', desc = {}, },
  S1 = { cw='S2', ccw='S2', desc = {}, },
  S2 = { cw='S1', ccw='S1', desc = {}, },
  L1 = { cw='L2', ccw='L4', desc = {}, },
  L2 = { cw='L3', ccw='L1', desc = {}, },
  L3 = { cw='L4', ccw='L2', desc = {}, },
  L4 = { cw='L1', ccw='L3', desc = {}, },
  T1 = { cw='T2', ccw='T4', desc = {}, },
  T2 = { cw='T3', ccw='T1', desc = {}, },
  T3 = { cw='T4', ccw='T2', desc = {}, },
  T4 = { cw='T1', ccw='T3', desc = {}, },
  B = { desc = {}, },
}

local function put_box(t, i, j)
	if not t.desc[i] then t.desc[i] = {} end
	t.desc[i][j] = true
end

--put_box(pieces.X1, 0, 0)
--put_box(pieces.X1, 0, 1)
--put_box(pieces.X2, 0, 0)
--put_box(pieces.X2, 1, 0)

put_box(pieces.H1, 0, -1)
put_box(pieces.H1, 0, 0)
put_box(pieces.H1, 0, 1)
put_box(pieces.H1, 0, 2)
put_box(pieces.H2, -1, 0)
put_box(pieces.H2, 0, 0)
put_box(pieces.H2, 1, 0)
put_box(pieces.H2, 2, 0)

put_box(pieces.Z1, 0, -1)
put_box(pieces.Z1, 0, 0)
put_box(pieces.Z1, 1, 0)
put_box(pieces.Z1, 1, 1)
put_box(pieces.Z2, -1, 0)
put_box(pieces.Z2, 0, 0)
put_box(pieces.Z2, 0, -1)
put_box(pieces.Z2, 1, -1)

put_box(pieces.S1, 0, 1)
put_box(pieces.S1, 0, 0)
put_box(pieces.S1, 1, 0)
put_box(pieces.S1, 1, -1)
put_box(pieces.S2, -1, 0)
put_box(pieces.S2, 0, 0)
put_box(pieces.S2, 0, 1)
put_box(pieces.S2, 1, 1)

put_box(pieces.L1, 0, -1)
put_box(pieces.L1, 0, 0)
put_box(pieces.L1, 0, 1)
put_box(pieces.L1, -1, 1)
put_box(pieces.L2, -1, 0)
put_box(pieces.L2, 0, 0)
put_box(pieces.L2, 1, 0)
put_box(pieces.L2, 1, 1)
put_box(pieces.L3, 0, -1)
put_box(pieces.L3, 0, 0)
put_box(pieces.L3, 0, 1)
put_box(pieces.L3, 1, -1)
put_box(pieces.L4, -1, 0)
put_box(pieces.L4, 0, 0)
put_box(pieces.L4, 1, 0)
put_box(pieces.L4, -1, -1)

put_box(pieces.T1, 0, -1)
put_box(pieces.T1, 0, 0)
put_box(pieces.T1, 0, 1)
put_box(pieces.T1, 1, 1)
put_box(pieces.T2, -1, 0)
put_box(pieces.T2, 0, 0)
put_box(pieces.T2, 1, 0)
put_box(pieces.T2, 1, -1)
put_box(pieces.T3, 0, -1)
put_box(pieces.T3, 0, 0)
put_box(pieces.T3, 0, 1)
put_box(pieces.T3, -1, -1)
put_box(pieces.T4, -1, 0)
put_box(pieces.T4, 0, 0)
put_box(pieces.T4, 1, 0)
put_box(pieces.T4, -1, 1)

put_box(pieces.B, 0, 0)
put_box(pieces.B, 0, 1)
put_box(pieces.B, 1, 0)
put_box(pieces.B, 1, 1)



local null_piece = function(t, k) return {} end
for _, t in pairs(pieces) do
setmetatable(t.desc, {__index = null_piece })
end


setmetatable(tetris.piece, {
	__index = function(t, i)
		local ret = {}
		setmetatable(ret, {
			__index = function(s, j)
				return pieces[t.piece_type].desc[i-t.position.y][j-t.position.x] and string.sub(t.piece_type, 1, 1) or nil
			end
		})
		return ret
	end
})



local function drawGame()
  local t = {}
	for i = 1,10 do for j = 1,10 do
		t[i] = (t[i] or '|')..(tetris.lines[i][j] or tetris.piece[i][j] or ' ')
	end end
	tetris:setText(table.concat(t, '|\n')..'|\n\\==========/')
	--print(tetris:toPlainText())
end

local function isLegal()
	for i = 0,11 do
		for j = 0,11 do
      if (tetris.piece[i][j]) and ((i>10) or (j>10) or (j<1)) then return false end
      if tetris.piece[i][j] and tetris.lines[i][j] then return false end
		end
	end
	return true
end

local function checkLines()
	for i = 1,10 do
		local completed = true
		for j = 1,10 do
			if not tetris.lines[i][j] then completed=nil end
		end
		if completed then
			--print('completed line', i)
			for k = i,2,-1 do tetris.lines[k] = tetris.lines[k-1] end
			tetris.lines[1] = {}
		end
	end
	drawGame()
end

local function placePiece()
	local t = nil
	local n = math.random(15)

	while not t do
		for _=1,n do t = next(pieces, t) end
	end
	tetris.piece.piece_type = t
	tetris.piece.position = { x=5, y=1 }
end

local function pieceDown()
	--print'piece down'

	tetris.piece.position.y = tetris.piece.position.y+1
	local cannot = not isLegal()
	if cannot then
		--print'hit'
		tetris.piece.position.y = tetris.piece.position.y-1
		for i = 1,10 do for j = 1,10 do
			tetris.lines[i][j] = tetris.lines[i][j] or tetris.piece[i][j]
		end end
		checkLines()
		placePiece()
	end
	drawGame()
end

function tetris.reset()
	for i = 0,10 do
		tetris.lines[i] = {}
	end
	placePiece()
end
tetris.reset()


local sc
sc = QShortcut.new(QKeySequence.new(Qt.Key['Key_Right']), tetris) qt.pass(sc)
sc:connect(qt.signal'activated()', qt.slot(
function()
	--print'right'
	tetris.piece.position.x = tetris.piece.position.x+1
	local cannot = not isLegal()
	if cannot then
		tetris.piece.position.x = tetris.piece.position.x-1
	end
	drawGame()
end
))
sc = QShortcut.new(QKeySequence.new(Qt.Key['Key_Left']), tetris) qt.pass(sc)
sc:connect(qt.signal'activated()', qt.slot(
function()
	--print'left'
	tetris.piece.position.x = tetris.piece.position.x-1
	local cannot = not isLegal()
	if cannot then
		tetris.piece.position.x = tetris.piece.position.x+1
	end
	drawGame()
end
))

sc = QShortcut.new(QKeySequence.new(Qt.Key['Key_Down']), tetris) qt.pass(sc)
sc:connect(qt.signal'activated()', qt.slot(pieceDown))
sc = QShortcut.new(QKeySequence.new(Qt.Key['Key_R']), tetris) qt.pass(sc)
sc:connect(qt.signal'activated()', qt.slot(tetris.reset))

sc = QShortcut.new(QKeySequence.new(Qt.Key['Key_A']), tetris) qt.pass(sc)
sc:connect(qt.signal'activated()', qt.slot(
function()
	local old = tetris.piece.piece_type
	if not pieces[old].ccw then return end
	tetris.piece.piece_type = pieces[old].ccw
	local cannot = not isLegal()
	if cannot then
		tetris.piece.piece_type = old
	end
	drawGame()
end
))
sc = QShortcut.new(QKeySequence.new(Qt.Key['Key_S']), tetris) qt.pass(sc)
sc:connect(qt.signal'activated()', qt.slot(
function()
	local old = tetris.piece.piece_type
	if not pieces[old].cw then return end
	tetris.piece.piece_type = pieces[old].cw
	local cannot = not isLegal()
	if cannot then
		tetris.piece.piece_type = old
	end
	drawGame()
end
))

timer = qt.pass(QTimer.new(tetris))
tetris.timer = timer
timer:connect(qt.signal'timeout()', qt.slot(pieceDown))
timer:start(1000)
tetris:show()
placePiece()
drawGame()

--tetris:delete()

return tetris

--]]
