#include "lqt_common.hpp"
#include <QCoreApplication>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QCoreApplication > : public QCoreApplication {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__setApplicationName (lua_State *L);
  static int __LuaWrapCall__postEvent__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__postEvent__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__postEvent (lua_State *L);
  static int __LuaWrapCall__organizationDomain (lua_State *L);
  static int __LuaWrapCall__addLibraryPath (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__trUtf8 (lua_State *L);
  static int __LuaWrapCall__setAttribute (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__tr (lua_State *L);
  static int __LuaWrapCall__applicationFilePath (lua_State *L);
  static int __LuaWrapCall__instance (lua_State *L);
  static int __LuaWrapCall__argv (lua_State *L);
  static int __LuaWrapCall__setOrganizationName (lua_State *L);
  static int __LuaWrapCall__organizationName (lua_State *L);
  static int __LuaWrapCall__applicationDirPath (lua_State *L);
  static int __LuaWrapCall__hasPendingEvents (lua_State *L);
  static int __LuaWrapCall__testAttribute (lua_State *L);
  static int __LuaWrapCall__delete (lua_State *L);
  static int __LuaWrapCall__libraryPaths (lua_State *L);
  static int __LuaWrapCall__quit (lua_State *L);
  static int __LuaWrapCall__removeLibraryPath (lua_State *L);
  static int __LuaWrapCall__argc (lua_State *L);
  static int __LuaWrapCall__translate__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__translate__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__translate (lua_State *L);
  static int __LuaWrapCall__arguments (lua_State *L);
  static int __LuaWrapCall__removePostedEvents__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__removePostedEvents__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__removePostedEvents (lua_State *L);
  static int __LuaWrapCall__metaObject (lua_State *L);
  static int __LuaWrapCall__closingDown (lua_State *L);
  static int __LuaWrapCall__sendPostedEvents__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__sendPostedEvents__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__sendPostedEvents (lua_State *L);
  static int __LuaWrapCall__applicationName (lua_State *L);
  static int __LuaWrapCall__startingUp (lua_State *L);
  static int __LuaWrapCall__watchUnixSignal (lua_State *L);
  static int __LuaWrapCall__flush (lua_State *L);
  static int __LuaWrapCall__setOrganizationDomain (lua_State *L);
  static int __LuaWrapCall__exit (lua_State *L);
  static int __LuaWrapCall__removeTranslator (lua_State *L);
  static int __LuaWrapCall__sendEvent (lua_State *L);
  static int __LuaWrapCall__notify (lua_State *L);
  static int __LuaWrapCall__setLibraryPaths (lua_State *L);
  static int __LuaWrapCall__filterEvent (lua_State *L);
  static int __LuaWrapCall__exec (lua_State *L);
  static int __LuaWrapCall__installTranslator (lua_State *L);
  static int __LuaWrapCall__processEvents__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__processEvents__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__processEvents (lua_State *L);
  bool notify (QObject * arg1, QEvent * arg2);
  bool eventFilter (QObject * arg1, QEvent * arg2);
  const QMetaObject * metaObject () const;
protected:
  bool event (QEvent * arg1);
public:
protected:
  void connectNotify (const char * arg1);
public:
protected:
  void timerEvent (QTimerEvent * arg1);
public:
protected:
  bool compressEvent (QEvent * arg1, QObject * arg2, QPostEventList * arg3);
public:
protected:
  void disconnectNotify (const char * arg1);
public:
protected:
  void childEvent (QChildEvent * arg1);
public:
protected:
  void customEvent (QEvent * arg1);
public:
  ~LuaBinder< QCoreApplication > ();
  static int lqt_pushenum_Encoding (lua_State *L);
  static int lqt_pushenum_Encoding_QFLAGS_CREATOR (lua_State *L);
  LuaBinder< QCoreApplication > (lua_State *l, int& arg1, char * * arg2):QCoreApplication(arg1, arg2), L(l) {}
};

extern "C" int luaopen_QCoreApplication (lua_State *L);
