#include "lqt_bind_QLayout.hpp"

int LuaBinder< QLayout >::__LuaWrapCall__indexOf (lua_State *L) {
  QLayout *& __lua__obj = *static_cast<QLayout**>(lqtL_checkudata(L, 1, "QLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 2, "QWidget*"));
  int ret = __lua__obj->QLayout::indexOf(arg1);
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QLayout >::__LuaWrapCall__parentWidget (lua_State *L) {
  QLayout *& __lua__obj = *static_cast<QLayout**>(lqtL_checkudata(L, 1, "QLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * ret = __lua__obj->QLayout::parentWidget();
  lqtL_pushudata(L, ret, "QWidget*");
  return 1;
}
int LuaBinder< QLayout >::__LuaWrapCall__minimumSize (lua_State *L) {
  QLayout *& __lua__obj = *static_cast<QLayout**>(lqtL_checkudata(L, 1, "QLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QSize ret = __lua__obj->QLayout::minimumSize();
  lqtL_passudata(L, new QSize(ret), "QSize*");
  return 1;
}
int LuaBinder< QLayout >::__LuaWrapCall__update (lua_State *L) {
  QLayout *& __lua__obj = *static_cast<QLayout**>(lqtL_checkudata(L, 1, "QLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QLayout::update();
  return 0;
}
int LuaBinder< QLayout >::__LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QLayout::trUtf8(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QLayout >::__LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QLayout::trUtf8(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QLayout >::__LuaWrapCall__trUtf8 (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1468a40;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x14685a0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x146a820;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1469f50;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x146acb0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__trUtf8__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__trUtf8__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__trUtf8 matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QLayout >::__LuaWrapCall__totalMaximumSize (lua_State *L) {
  QLayout *& __lua__obj = *static_cast<QLayout**>(lqtL_checkudata(L, 1, "QLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QSize ret = __lua__obj->QLayout::totalMaximumSize();
  lqtL_passudata(L, new QSize(ret), "QSize*");
  return 1;
}
int LuaBinder< QLayout >::__LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QLayout::tr(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QLayout >::__LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QLayout::tr(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QLayout >::__LuaWrapCall__tr (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1467d00;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x1467a70;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1469710;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1469c00;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1469fb0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__tr__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__tr__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__tr matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QLayout >::__LuaWrapCall__isEmpty (lua_State *L) {
  QLayout *& __lua__obj = *static_cast<QLayout**>(lqtL_checkudata(L, 1, "QLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QLayout::isEmpty();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QLayout >::__LuaWrapCall__setAlignment__OverloadedVersion__1 (lua_State *L) {
  QLayout *& __lua__obj = *static_cast<QLayout**>(lqtL_checkudata(L, 1, "QLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 2, "QWidget*"));
  QFlags<Qt::AlignmentFlag> arg2 = **static_cast<QFlags<Qt::AlignmentFlag>**>(lqtL_checkudata(L, 3, "QFlags<Qt::AlignmentFlag>*"));
  bool ret = __lua__obj->QLayout::setAlignment(arg1, arg2);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QLayout >::__LuaWrapCall__setAlignment__OverloadedVersion__2 (lua_State *L) {
  QLayout *& __lua__obj = *static_cast<QLayout**>(lqtL_checkudata(L, 1, "QLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QLayout * arg1 = *static_cast<QLayout**>(lqtL_checkudata(L, 2, "QLayout*"));
  QFlags<Qt::AlignmentFlag> arg2 = **static_cast<QFlags<Qt::AlignmentFlag>**>(lqtL_checkudata(L, 3, "QFlags<Qt::AlignmentFlag>*"));
  bool ret = __lua__obj->QLayout::setAlignment(arg1, arg2);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QLayout >::__LuaWrapCall__setAlignment (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QLayout*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QWidget*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1474330;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_testudata(L, 3, "QFlags<Qt::AlignmentFlag>*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1474040;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QLayout*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QLayout*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1475200;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_testudata(L, 3, "QFlags<Qt::AlignmentFlag>*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1474c60;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__setAlignment__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__setAlignment__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__setAlignment matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QLayout >::__LuaWrapCall__expandingDirections (lua_State *L) {
  QLayout *& __lua__obj = *static_cast<QLayout**>(lqtL_checkudata(L, 1, "QLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<Qt::Orientation> ret = __lua__obj->QLayout::expandingDirections();
  lqtL_passudata(L, new QFlags<Qt::Orientation>(ret), "QFlags<Qt::Orientation>*");
  return 1;
}
int LuaBinder< QLayout >::__LuaWrapCall__addWidget (lua_State *L) {
  QLayout *& __lua__obj = *static_cast<QLayout**>(lqtL_checkudata(L, 1, "QLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 2, "QWidget*"));
  __lua__obj->QLayout::addWidget(arg1);
  return 0;
}
int LuaBinder< QLayout >::__LuaWrapCall__removeWidget (lua_State *L) {
  QLayout *& __lua__obj = *static_cast<QLayout**>(lqtL_checkudata(L, 1, "QLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 2, "QWidget*"));
  __lua__obj->QLayout::removeWidget(arg1);
  return 0;
}
int LuaBinder< QLayout >::__LuaWrapCall__activate (lua_State *L) {
  QLayout *& __lua__obj = *static_cast<QLayout**>(lqtL_checkudata(L, 1, "QLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QLayout::activate();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QLayout >::__LuaWrapCall__setMargin (lua_State *L) {
  QLayout *& __lua__obj = *static_cast<QLayout**>(lqtL_checkudata(L, 1, "QLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QLayout::setMargin(arg1);
  return 0;
}
int LuaBinder< QLayout >::__LuaWrapCall__delete (lua_State *L) {
  QLayout *& __lua__obj = *static_cast<QLayout**>(lqtL_checkudata(L, 1, "QLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  delete __lua__obj;
  __lua__obj = 0;
  return 0;
}
int LuaBinder< QLayout >::__LuaWrapCall__totalMinimumSize (lua_State *L) {
  QLayout *& __lua__obj = *static_cast<QLayout**>(lqtL_checkudata(L, 1, "QLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QSize ret = __lua__obj->QLayout::totalMinimumSize();
  lqtL_passudata(L, new QSize(ret), "QSize*");
  return 1;
}
int LuaBinder< QLayout >::__LuaWrapCall__layout (lua_State *L) {
  QLayout *& __lua__obj = *static_cast<QLayout**>(lqtL_checkudata(L, 1, "QLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QLayout * ret = __lua__obj->QLayout::layout();
  lqtL_pushudata(L, ret, "QLayout*");
  return 1;
}
int LuaBinder< QLayout >::__LuaWrapCall__margin (lua_State *L) {
  QLayout *& __lua__obj = *static_cast<QLayout**>(lqtL_checkudata(L, 1, "QLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QLayout::margin();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QLayout >::__LuaWrapCall__menuBar (lua_State *L) {
  QLayout *& __lua__obj = *static_cast<QLayout**>(lqtL_checkudata(L, 1, "QLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * ret = __lua__obj->QLayout::menuBar();
  lqtL_pushudata(L, ret, "QWidget*");
  return 1;
}
int LuaBinder< QLayout >::__LuaWrapCall__geometry (lua_State *L) {
  QLayout *& __lua__obj = *static_cast<QLayout**>(lqtL_checkudata(L, 1, "QLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QRect ret = __lua__obj->QLayout::geometry();
  lqtL_passudata(L, new QRect(ret), "QRect*");
  return 1;
}
int LuaBinder< QLayout >::__LuaWrapCall__spacing (lua_State *L) {
  QLayout *& __lua__obj = *static_cast<QLayout**>(lqtL_checkudata(L, 1, "QLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QLayout::spacing();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QLayout >::__LuaWrapCall__sizeConstraint (lua_State *L) {
  QLayout *& __lua__obj = *static_cast<QLayout**>(lqtL_checkudata(L, 1, "QLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QLayout::SizeConstraint ret = __lua__obj->QLayout::sizeConstraint();
  lqtL_pushenum(L, ret, "QLayout::SizeConstraint");
  return 1;
}
int LuaBinder< QLayout >::__LuaWrapCall__setContentsMargins (lua_State *L) {
  QLayout *& __lua__obj = *static_cast<QLayout**>(lqtL_checkudata(L, 1, "QLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int arg2 = lua_tointeger(L, 3);
  int arg3 = lua_tointeger(L, 4);
  int arg4 = lua_tointeger(L, 5);
  __lua__obj->QLayout::setContentsMargins(arg1, arg2, arg3, arg4);
  return 0;
}
int LuaBinder< QLayout >::__LuaWrapCall__metaObject (lua_State *L) {
  QLayout *& __lua__obj = *static_cast<QLayout**>(lqtL_checkudata(L, 1, "QLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QMetaObject * ret = __lua__obj->QLayout::metaObject();
  lqtL_pushudata(L, ret, "QMetaObject*");
  return 1;
}
int LuaBinder< QLayout >::__LuaWrapCall__new__OverloadedVersion__1 (lua_State *L) {
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
  QLayout * ret = new LuaBinder< QLayout >(L, arg1);
  lqtL_passudata(L, ret, "QLayout*");
  return 1;
}
int LuaBinder< QLayout >::__LuaWrapCall__new__OverloadedVersion__2 (lua_State *L) {
  QLayout * ret = new LuaBinder< QLayout >(L);
  lqtL_passudata(L, ret, "QLayout*");
  return 1;
}
int LuaBinder< QLayout >::__LuaWrapCall__new (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if (lqtL_testudata(L, 1, "QWidget*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x146d6c0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__new__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__new__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__new matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QLayout >::__LuaWrapCall__contentsRect (lua_State *L) {
  QLayout *& __lua__obj = *static_cast<QLayout**>(lqtL_checkudata(L, 1, "QLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QRect ret = __lua__obj->QLayout::contentsRect();
  lqtL_passudata(L, new QRect(ret), "QRect*");
  return 1;
}
int LuaBinder< QLayout >::__LuaWrapCall__getContentsMargins (lua_State *L) {
  QLayout *& __lua__obj = *static_cast<QLayout**>(lqtL_checkudata(L, 1, "QLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int * arg1 = static_cast<int *>(lua_touserdata(L, 2));
  int * arg2 = static_cast<int *>(lua_touserdata(L, 3));
  int * arg3 = static_cast<int *>(lua_touserdata(L, 4));
  int * arg4 = static_cast<int *>(lua_touserdata(L, 5));
  __lua__obj->QLayout::getContentsMargins(arg1, arg2, arg3, arg4);
  return 0;
}
int LuaBinder< QLayout >::__LuaWrapCall__invalidate (lua_State *L) {
  QLayout *& __lua__obj = *static_cast<QLayout**>(lqtL_checkudata(L, 1, "QLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QLayout::invalidate();
  return 0;
}
int LuaBinder< QLayout >::__LuaWrapCall__maximumSize (lua_State *L) {
  QLayout *& __lua__obj = *static_cast<QLayout**>(lqtL_checkudata(L, 1, "QLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QSize ret = __lua__obj->QLayout::maximumSize();
  lqtL_passudata(L, new QSize(ret), "QSize*");
  return 1;
}
int LuaBinder< QLayout >::__LuaWrapCall__setMenuBar (lua_State *L) {
  QLayout *& __lua__obj = *static_cast<QLayout**>(lqtL_checkudata(L, 1, "QLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 2, "QWidget*"));
  __lua__obj->QLayout::setMenuBar(arg1);
  return 0;
}
int LuaBinder< QLayout >::__LuaWrapCall__totalSizeHint (lua_State *L) {
  QLayout *& __lua__obj = *static_cast<QLayout**>(lqtL_checkudata(L, 1, "QLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QSize ret = __lua__obj->QLayout::totalSizeHint();
  lqtL_passudata(L, new QSize(ret), "QSize*");
  return 1;
}
int LuaBinder< QLayout >::__LuaWrapCall__setSpacing (lua_State *L) {
  QLayout *& __lua__obj = *static_cast<QLayout**>(lqtL_checkudata(L, 1, "QLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QLayout::setSpacing(arg1);
  return 0;
}
int LuaBinder< QLayout >::__LuaWrapCall__closestAcceptableSize (lua_State *L) {
  const QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
  const QSize& arg2 = **static_cast<QSize**>(lqtL_checkudata(L, 2, "QSize*"));
  QSize ret = QLayout::closestAcceptableSize(arg1, arg2);
  lqtL_passudata(L, new QSize(ret), "QSize*");
  return 1;
}
int LuaBinder< QLayout >::__LuaWrapCall__setSizeConstraint (lua_State *L) {
  QLayout *& __lua__obj = *static_cast<QLayout**>(lqtL_checkudata(L, 1, "QLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QLayout::SizeConstraint arg1 = static_cast<QLayout::SizeConstraint>(lqtL_toenum(L, 2, "QLayout::SizeConstraint"));
  __lua__obj->QLayout::setSizeConstraint(arg1);
  return 0;
}
int LuaBinder< QLayout >::__LuaWrapCall__isEnabled (lua_State *L) {
  QLayout *& __lua__obj = *static_cast<QLayout**>(lqtL_checkudata(L, 1, "QLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QLayout::isEnabled();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QLayout >::__LuaWrapCall__removeItem (lua_State *L) {
  QLayout *& __lua__obj = *static_cast<QLayout**>(lqtL_checkudata(L, 1, "QLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QLayoutItem * arg1 = *static_cast<QLayoutItem**>(lqtL_checkudata(L, 2, "QLayoutItem*"));
  __lua__obj->QLayout::removeItem(arg1);
  return 0;
}
int LuaBinder< QLayout >::__LuaWrapCall__setEnabled (lua_State *L) {
  QLayout *& __lua__obj = *static_cast<QLayout**>(lqtL_checkudata(L, 1, "QLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QLayout::setEnabled(arg1);
  return 0;
}
int LuaBinder< QLayout >::__LuaWrapCall__totalHeightForWidth (lua_State *L) {
  QLayout *& __lua__obj = *static_cast<QLayout**>(lqtL_checkudata(L, 1, "QLayout*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int ret = __lua__obj->QLayout::totalHeightForWidth(arg1);
  lua_pushinteger(L, ret);
  return 1;
}
QLayout * LuaBinder< QLayout >::layout () {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "layout");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QLayout::layout();
  }
  QLayout * ret = *static_cast<QLayout**>(lqtL_checkudata(L, -1, "QLayout*"));
  lua_settop(L, oldtop);
  return ret;
}
QLayoutItem * LuaBinder< QLayout >::takeAt (int arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "takeAt");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushinteger(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
      }
  QLayoutItem * ret = *static_cast<QLayoutItem**>(lqtL_checkudata(L, -1, "QLayoutItem*"));
  lua_settop(L, oldtop);
  return ret;
}
const QMetaObject * LuaBinder< QLayout >::metaObject () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "metaObject");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QLayout::metaObject();
  }
  const QMetaObject * ret = *static_cast<QMetaObject**>(lqtL_checkudata(L, -1, "QMetaObject*"));
  lua_settop(L, oldtop);
  return ret;
}
QLayoutItem * LuaBinder< QLayout >::itemAt (int arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "itemAt");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushinteger(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
      }
  QLayoutItem * ret = *static_cast<QLayoutItem**>(lqtL_checkudata(L, -1, "QLayoutItem*"));
  lua_settop(L, oldtop);
  return ret;
}
int LuaBinder< QLayout >::heightForWidth (int arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "heightForWidth");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushinteger(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QLayoutItem::heightForWidth(arg1);
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
int LuaBinder< QLayout >::minimumHeightForWidth (int arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "minimumHeightForWidth");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushinteger(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QLayoutItem::minimumHeightForWidth(arg1);
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QLayout >::invalidate () {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "invalidate");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QLayout::invalidate();
  }
  lua_settop(L, oldtop);
}
QWidget * LuaBinder< QLayout >::widget () {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "widget");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QLayoutItem::widget();
  }
  QWidget * ret = *static_cast<QWidget**>(lqtL_checkudata(L, -1, "QWidget*"));
  lua_settop(L, oldtop);
  return ret;
}
bool LuaBinder< QLayout >::hasHeightForWidth () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "hasHeightForWidth");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QLayoutItem::hasHeightForWidth();
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QLayout >::childEvent (QChildEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "childEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QChildEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QLayout::childEvent(arg1);
  }
  lua_settop(L, oldtop);
}
QSize LuaBinder< QLayout >::maximumSize () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "maximumSize");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QLayout::maximumSize();
  }
  QSize ret = **static_cast<QSize**>(lqtL_checkudata(L, -1, "QSize*"));
  lua_settop(L, oldtop);
  return ret;
}
int LuaBinder< QLayout >::indexOf (QWidget * arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "indexOf");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QWidget*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QLayout::indexOf(arg1);
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QLayout >::addItem (QLayoutItem * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "addItem");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QLayoutItem*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
      }
  lua_settop(L, oldtop);
}
bool LuaBinder< QLayout >::eventFilter (QObject * arg1, QEvent * arg2) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "eventFilter");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QObject*");
  lqtL_pushudata(L, arg2, "QEvent*");
  if (lua_isfunction(L, -2-2)) {
    lua_pcall(L, 2+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QObject::eventFilter(arg1, arg2);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
QSpacerItem * LuaBinder< QLayout >::spacerItem () {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "spacerItem");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QLayoutItem::spacerItem();
  }
  QSpacerItem * ret = *static_cast<QSpacerItem**>(lqtL_checkudata(L, -1, "QSpacerItem*"));
  lua_settop(L, oldtop);
  return ret;
}
QSize LuaBinder< QLayout >::sizeHint () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "sizeHint");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
      }
  QSize ret = **static_cast<QSize**>(lqtL_checkudata(L, -1, "QSize*"));
  lua_settop(L, oldtop);
  return ret;
}
QSize LuaBinder< QLayout >::minimumSize () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "minimumSize");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QLayout::minimumSize();
  }
  QSize ret = **static_cast<QSize**>(lqtL_checkudata(L, -1, "QSize*"));
  lua_settop(L, oldtop);
  return ret;
}
bool LuaBinder< QLayout >::isEmpty () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "isEmpty");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QLayout::isEmpty();
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QLayout >::connectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "connectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::connectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QLayout >::event (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "event");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QObject::event(arg1);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QLayout >::timerEvent (QTimerEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "timerEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QTimerEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::timerEvent(arg1);
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QLayout >::count () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "count");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
      }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QLayout >::disconnectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "disconnectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::disconnectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
QFlags<Qt::Orientation> LuaBinder< QLayout >::expandingDirections () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "expandingDirections");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QLayout::expandingDirections();
  }
  QFlags<Qt::Orientation> ret = **static_cast<QFlags<Qt::Orientation>**>(lqtL_checkudata(L, -1, "QFlags<Qt::Orientation>*"));
  lua_settop(L, oldtop);
  return ret;
}
QRect LuaBinder< QLayout >::geometry () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "geometry");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QLayout::geometry();
  }
  QRect ret = **static_cast<QRect**>(lqtL_checkudata(L, -1, "QRect*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QLayout >::setGeometry (const QRect& arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "setGeometry");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QRect*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
      }
  lua_settop(L, oldtop);
}
void LuaBinder< QLayout >::customEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayout*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "customEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::customEvent(arg1);
  }
  lua_settop(L, oldtop);
}
LuaBinder< QLayout >::  ~LuaBinder< QLayout > () {
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayout*");
  lua_getfield(L, -1, "~QLayout");

  if (lua_isfunction(L, -1)) {
    lua_insert(L, -2);
    lua_pcall(L, 1, 1, 0);
  } else {
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QLayout >::lqt_pushenum_SizeConstraint (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "SetDefaultConstraint");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "SetDefaultConstraint");
  lua_pushstring(L, "SetNoConstraint");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "SetNoConstraint");
  lua_pushstring(L, "SetMinimumSize");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "SetMinimumSize");
  lua_pushstring(L, "SetFixedSize");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "SetFixedSize");
  lua_pushstring(L, "SetMaximumSize");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "SetMaximumSize");
  lua_pushstring(L, "SetMinAndMaxSize");
  lua_rawseti(L, enum_table, 5);
  lua_pushinteger(L, 5);
  lua_setfield(L, enum_table, "SetMinAndMaxSize");
  lua_pushcfunction(L, LuaBinder< QLayout >::lqt_pushenum_SizeConstraint_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QLayout::SizeConstraint");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QLayout >::lqt_pushenum_SizeConstraint_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QLayout::SizeConstraint>*) + sizeof(QFlags<QLayout::SizeConstraint>));
  QFlags<QLayout::SizeConstraint> *fl = static_cast<QFlags<QLayout::SizeConstraint>*>( static_cast<void*>(&static_cast<QFlags<QLayout::SizeConstraint>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QLayout::SizeConstraint>(lqtL_toenum(L, i, "QLayout::SizeConstraint"));
	}
	if (luaL_newmetatable(L, "QFlags<QLayout::SizeConstraint>*")) {
		lua_pushstring(L, "QFlags<QLayout::SizeConstraint>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int luaopen_QLayout (lua_State *L) {
  if (luaL_newmetatable(L, "QLayout*")) {
    lua_pushcfunction(L, LuaBinder< QLayout >::__LuaWrapCall__indexOf);
    lua_setfield(L, -2, "indexOf");
    lua_pushcfunction(L, LuaBinder< QLayout >::__LuaWrapCall__parentWidget);
    lua_setfield(L, -2, "parentWidget");
    lua_pushcfunction(L, LuaBinder< QLayout >::__LuaWrapCall__minimumSize);
    lua_setfield(L, -2, "minimumSize");
    lua_pushcfunction(L, LuaBinder< QLayout >::__LuaWrapCall__update);
    lua_setfield(L, -2, "update");
    lua_pushcfunction(L, LuaBinder< QLayout >::__LuaWrapCall__trUtf8);
    lua_setfield(L, -2, "trUtf8");
    lua_pushcfunction(L, LuaBinder< QLayout >::__LuaWrapCall__totalMaximumSize);
    lua_setfield(L, -2, "totalMaximumSize");
    lua_pushcfunction(L, LuaBinder< QLayout >::__LuaWrapCall__tr);
    lua_setfield(L, -2, "tr");
    lua_pushcfunction(L, LuaBinder< QLayout >::__LuaWrapCall__isEmpty);
    lua_setfield(L, -2, "isEmpty");
    lua_pushcfunction(L, LuaBinder< QLayout >::__LuaWrapCall__setAlignment);
    lua_setfield(L, -2, "setAlignment");
    lua_pushcfunction(L, LuaBinder< QLayout >::__LuaWrapCall__expandingDirections);
    lua_setfield(L, -2, "expandingDirections");
    lua_pushcfunction(L, LuaBinder< QLayout >::__LuaWrapCall__addWidget);
    lua_setfield(L, -2, "addWidget");
    lua_pushcfunction(L, LuaBinder< QLayout >::__LuaWrapCall__removeWidget);
    lua_setfield(L, -2, "removeWidget");
    lua_pushcfunction(L, LuaBinder< QLayout >::__LuaWrapCall__activate);
    lua_setfield(L, -2, "activate");
    lua_pushcfunction(L, LuaBinder< QLayout >::__LuaWrapCall__setMargin);
    lua_setfield(L, -2, "setMargin");
    lua_pushcfunction(L, LuaBinder< QLayout >::__LuaWrapCall__delete);
    lua_setfield(L, -2, "delete");
    lua_pushcfunction(L, LuaBinder< QLayout >::__LuaWrapCall__totalMinimumSize);
    lua_setfield(L, -2, "totalMinimumSize");
    lua_pushcfunction(L, LuaBinder< QLayout >::__LuaWrapCall__layout);
    lua_setfield(L, -2, "layout");
    lua_pushcfunction(L, LuaBinder< QLayout >::__LuaWrapCall__margin);
    lua_setfield(L, -2, "margin");
    lua_pushcfunction(L, LuaBinder< QLayout >::__LuaWrapCall__menuBar);
    lua_setfield(L, -2, "menuBar");
    lua_pushcfunction(L, LuaBinder< QLayout >::__LuaWrapCall__geometry);
    lua_setfield(L, -2, "geometry");
    lua_pushcfunction(L, LuaBinder< QLayout >::__LuaWrapCall__spacing);
    lua_setfield(L, -2, "spacing");
    lua_pushcfunction(L, LuaBinder< QLayout >::__LuaWrapCall__sizeConstraint);
    lua_setfield(L, -2, "sizeConstraint");
    lua_pushcfunction(L, LuaBinder< QLayout >::__LuaWrapCall__setContentsMargins);
    lua_setfield(L, -2, "setContentsMargins");
    lua_pushcfunction(L, LuaBinder< QLayout >::__LuaWrapCall__metaObject);
    lua_setfield(L, -2, "metaObject");
    lua_pushcfunction(L, LuaBinder< QLayout >::__LuaWrapCall__new);
    lua_setfield(L, -2, "new");
    lua_pushcfunction(L, LuaBinder< QLayout >::__LuaWrapCall__contentsRect);
    lua_setfield(L, -2, "contentsRect");
    lua_pushcfunction(L, LuaBinder< QLayout >::__LuaWrapCall__getContentsMargins);
    lua_setfield(L, -2, "getContentsMargins");
    lua_pushcfunction(L, LuaBinder< QLayout >::__LuaWrapCall__invalidate);
    lua_setfield(L, -2, "invalidate");
    lua_pushcfunction(L, LuaBinder< QLayout >::__LuaWrapCall__maximumSize);
    lua_setfield(L, -2, "maximumSize");
    lua_pushcfunction(L, LuaBinder< QLayout >::__LuaWrapCall__setMenuBar);
    lua_setfield(L, -2, "setMenuBar");
    lua_pushcfunction(L, LuaBinder< QLayout >::__LuaWrapCall__totalSizeHint);
    lua_setfield(L, -2, "totalSizeHint");
    lua_pushcfunction(L, LuaBinder< QLayout >::__LuaWrapCall__setSpacing);
    lua_setfield(L, -2, "setSpacing");
    lua_pushcfunction(L, LuaBinder< QLayout >::__LuaWrapCall__closestAcceptableSize);
    lua_setfield(L, -2, "closestAcceptableSize");
    lua_pushcfunction(L, LuaBinder< QLayout >::__LuaWrapCall__setSizeConstraint);
    lua_setfield(L, -2, "setSizeConstraint");
    lua_pushcfunction(L, LuaBinder< QLayout >::__LuaWrapCall__isEnabled);
    lua_setfield(L, -2, "isEnabled");
    lua_pushcfunction(L, LuaBinder< QLayout >::__LuaWrapCall__removeItem);
    lua_setfield(L, -2, "removeItem");
    lua_pushcfunction(L, LuaBinder< QLayout >::__LuaWrapCall__setEnabled);
    lua_setfield(L, -2, "setEnabled");
    lua_pushcfunction(L, LuaBinder< QLayout >::__LuaWrapCall__totalHeightForWidth);
    lua_setfield(L, -2, "totalHeightForWidth");
    LuaBinder< QLayout >::lqt_pushenum_SizeConstraint(L);
    lua_setfield(L, -2, "SizeConstraint");
    lua_newtable(L);
    lua_pushboolean(L, 1);
    lua_setfield(L, -2, "QObject*");
    lua_pushboolean(L, 1);
    lua_setfield(L, -2, "QLayoutItem*");
    lua_setfield(L, -2, "__base");
    lua_pushcfunction(L, lqtL_newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, lqtL_index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, lqtL_gc);
    lua_setfield(L, -2, "__gc");
    lua_pushstring(L, "QLayout");
    lua_setfield(L, -2, "__qtype");
		lua_setglobal(L, "QLayout");
  } else {
		lua_pop(L, 1);
	}
  return 0;
}
