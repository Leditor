#include "lqt_common.hpp"
#include <QPaintDevice>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QPaintDevice > : public QPaintDevice {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__delete (lua_State *L);
  static int __LuaWrapCall__logicalDpiX (lua_State *L);
  static int __LuaWrapCall__width (lua_State *L);
  static int __LuaWrapCall__widthMM (lua_State *L);
  static int __LuaWrapCall__height (lua_State *L);
  static int __LuaWrapCall__numColors (lua_State *L);
  static int __LuaWrapCall__physicalDpiX (lua_State *L);
  static int __LuaWrapCall__physicalDpiY (lua_State *L);
  static int __LuaWrapCall__devType (lua_State *L);
  static int __LuaWrapCall__logicalDpiY (lua_State *L);
  static int __LuaWrapCall__paintingActive (lua_State *L);
  static int __LuaWrapCall__depth (lua_State *L);
  static int __LuaWrapCall__heightMM (lua_State *L);
  QPaintEngine * paintEngine () const;
protected:
  int metric (QPaintDevice::PaintDeviceMetric arg1) const;
public:
  int devType () const;
  ~LuaBinder< QPaintDevice > ();
  static int lqt_pushenum_PaintDeviceMetric (lua_State *L);
  static int lqt_pushenum_PaintDeviceMetric_QFLAGS_CREATOR (lua_State *L);
};

extern "C" int luaopen_QPaintDevice (lua_State *L);
