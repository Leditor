#include "lqt_common.hpp"
#include <QInputEvent>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QInputEvent > : public QInputEvent {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__delete (lua_State *L);
  static int __LuaWrapCall__modifiers (lua_State *L);
  ~LuaBinder< QInputEvent > ();
  LuaBinder< QInputEvent > (lua_State *l, QEvent::Type arg1, QFlags<Qt::KeyboardModifier> arg2):QInputEvent(arg1, arg2), L(l) {}
};

extern "C" int luaopen_QInputEvent (lua_State *L);
