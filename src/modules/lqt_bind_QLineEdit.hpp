#include "lqt_common.hpp"
#include <QLineEdit>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QLineEdit > : public QLineEdit {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__setValidator (lua_State *L);
  static int __LuaWrapCall__inputMask (lua_State *L);
  static int __LuaWrapCall__createStandardContextMenu (lua_State *L);
  static int __LuaWrapCall__sizeHint (lua_State *L);
  static int __LuaWrapCall__validator (lua_State *L);
  static int __LuaWrapCall__cursorWordBackward (lua_State *L);
  static int __LuaWrapCall__cut (lua_State *L);
  static int __LuaWrapCall__hasSelectedText (lua_State *L);
  static int __LuaWrapCall__copy (lua_State *L);
  static int __LuaWrapCall__home (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__trUtf8 (lua_State *L);
  static int __LuaWrapCall__cursorWordForward (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__tr (lua_State *L);
  static int __LuaWrapCall__cursorPositionAt (lua_State *L);
  static int __LuaWrapCall__isReadOnly (lua_State *L);
  static int __LuaWrapCall__insert (lua_State *L);
  static int __LuaWrapCall__setMaxLength (lua_State *L);
  static int __LuaWrapCall__text (lua_State *L);
  static int __LuaWrapCall__event (lua_State *L);
  static int __LuaWrapCall__isModified (lua_State *L);
  static int __LuaWrapCall__hasFrame (lua_State *L);
  static int __LuaWrapCall__setCursorPosition (lua_State *L);
  static int __LuaWrapCall__echoMode (lua_State *L);
  static int __LuaWrapCall__isUndoAvailable (lua_State *L);
  static int __LuaWrapCall__cursorBackward (lua_State *L);
  static int __LuaWrapCall__setSelection (lua_State *L);
  static int __LuaWrapCall__setReadOnly (lua_State *L);
  static int __LuaWrapCall__setModified (lua_State *L);
  static int __LuaWrapCall__setDragEnabled (lua_State *L);
  static int __LuaWrapCall__cursorPosition (lua_State *L);
  static int __LuaWrapCall__isRedoAvailable (lua_State *L);
  static int __LuaWrapCall__del (lua_State *L);
  static int __LuaWrapCall__displayText (lua_State *L);
  static int __LuaWrapCall__setText (lua_State *L);
  static int __LuaWrapCall__minimumSizeHint (lua_State *L);
  static int __LuaWrapCall__hasAcceptableInput (lua_State *L);
  static int __LuaWrapCall__selectionStart (lua_State *L);
  static int __LuaWrapCall__backspace (lua_State *L);
  static int __LuaWrapCall__setEchoMode (lua_State *L);
  static int __LuaWrapCall__end (lua_State *L);
  static int __LuaWrapCall__selectedText (lua_State *L);
  static int __LuaWrapCall__setCompleter (lua_State *L);
  static int __LuaWrapCall__metaObject (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__new (lua_State *L);
  static int __LuaWrapCall__inputMethodQuery (lua_State *L);
  static int __LuaWrapCall__delete (lua_State *L);
  static int __LuaWrapCall__redo (lua_State *L);
  static int __LuaWrapCall__deselect (lua_State *L);
  static int __LuaWrapCall__completer (lua_State *L);
  static int __LuaWrapCall__setInputMask (lua_State *L);
  static int __LuaWrapCall__clear (lua_State *L);
  static int __LuaWrapCall__maxLength (lua_State *L);
  static int __LuaWrapCall__setFrame (lua_State *L);
  static int __LuaWrapCall__selectAll (lua_State *L);
  static int __LuaWrapCall__alignment (lua_State *L);
  static int __LuaWrapCall__cursorForward (lua_State *L);
  static int __LuaWrapCall__undo (lua_State *L);
  static int __LuaWrapCall__paste (lua_State *L);
  static int __LuaWrapCall__dragEnabled (lua_State *L);
  static int __LuaWrapCall__setAlignment (lua_State *L);
protected:
  void styleChange (QStyle& arg1);
public:
protected:
  void mouseReleaseEvent (QMouseEvent * arg1);
public:
protected:
  void keyPressEvent (QKeyEvent * arg1);
public:
protected:
  void contextMenuEvent (QContextMenuEvent * arg1);
public:
  int devType () const;
  void setVisible (bool arg1);
  QVariant inputMethodQuery (Qt::InputMethodQuery arg1) const;
  QPaintEngine * paintEngine () const;
protected:
  void dragLeaveEvent (QDragLeaveEvent * arg1);
public:
protected:
  void mousePressEvent (QMouseEvent * arg1);
public:
  QSize sizeHint () const;
protected:
  void changeEvent (QEvent * arg1);
public:
protected:
  void tabletEvent (QTabletEvent * arg1);
public:
protected:
  void enterEvent (QEvent * arg1);
public:
protected:
  void closeEvent (QCloseEvent * arg1);
public:
  QSize minimumSizeHint () const;
protected:
  void dragEnterEvent (QDragEnterEvent * arg1);
public:
protected:
  void actionEvent (QActionEvent * arg1);
public:
protected:
  void showEvent (QShowEvent * arg1);
public:
  const QMetaObject * metaObject () const;
protected:
  void paintEvent (QPaintEvent * arg1);
public:
protected:
  void dragMoveEvent (QDragMoveEvent * arg1);
public:
protected:
  void focusInEvent (QFocusEvent * arg1);
public:
protected:
  void enabledChange (bool arg1);
public:
protected:
  void windowActivationChange (bool arg1);
public:
  int heightForWidth (int arg1) const;
protected:
  void mouseDoubleClickEvent (QMouseEvent * arg1);
public:
protected:
  void fontChange (const QFont& arg1);
public:
protected:
  void timerEvent (QTimerEvent * arg1);
public:
protected:
  void mouseMoveEvent (QMouseEvent * arg1);
public:
protected:
  void disconnectNotify (const char * arg1);
public:
protected:
  void childEvent (QChildEvent * arg1);
public:
protected:
  void focusOutEvent (QFocusEvent * arg1);
public:
protected:
  int metric (QPaintDevice::PaintDeviceMetric arg1) const;
public:
protected:
  bool focusNextPrevChild (bool arg1);
public:
  bool eventFilter (QObject * arg1, QEvent * arg2);
protected:
  void inputMethodEvent (QInputMethodEvent * arg1);
public:
protected:
  void wheelEvent (QWheelEvent * arg1);
public:
protected:
  void resizeEvent (QResizeEvent * arg1);
public:
protected:
  void paletteChange (const QPalette& arg1);
public:
protected:
  void languageChange ();
public:
  bool event (QEvent * arg1);
protected:
  void hideEvent (QHideEvent * arg1);
public:
protected:
  void connectNotify (const char * arg1);
public:
protected:
  void moveEvent (QMoveEvent * arg1);
public:
protected:
  void keyReleaseEvent (QKeyEvent * arg1);
public:
protected:
  void leaveEvent (QEvent * arg1);
public:
protected:
  void dropEvent (QDropEvent * arg1);
public:
protected:
  void customEvent (QEvent * arg1);
public:
  ~LuaBinder< QLineEdit > ();
  static int lqt_pushenum_EchoMode (lua_State *L);
  static int lqt_pushenum_EchoMode_QFLAGS_CREATOR (lua_State *L);
  LuaBinder< QLineEdit > (lua_State *l, QWidget * arg1):QLineEdit(arg1), L(l) {}
  LuaBinder< QLineEdit > (lua_State *l, const QString& arg1, QWidget * arg2):QLineEdit(arg1, arg2), L(l) {}
};

extern "C" int luaopen_QLineEdit (lua_State *L);
