#include "lqt_bind_QCoreApplication.hpp"

int LuaBinder< QCoreApplication >::__LuaWrapCall__setApplicationName (lua_State *L) {
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 1), lua_objlen(L, 1));
  QCoreApplication::setApplicationName(arg1);
  return 0;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__postEvent__OverloadedVersion__1 (lua_State *L) {
  QObject * arg1 = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
  QEvent * arg2 = *static_cast<QEvent**>(lqtL_checkudata(L, 2, "QEvent*"));
  QCoreApplication::postEvent(arg1, arg2);
  return 0;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__postEvent__OverloadedVersion__2 (lua_State *L) {
  QObject * arg1 = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
  QEvent * arg2 = *static_cast<QEvent**>(lqtL_checkudata(L, 2, "QEvent*"));
  int arg3 = lua_tointeger(L, 3);
  QCoreApplication::postEvent(arg1, arg2, arg3);
  return 0;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__postEvent (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if (lqtL_testudata(L, 1, "QObject*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1626390;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_testudata(L, 2, "QEvent*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1625d80;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if (lqtL_testudata(L, 1, "QObject*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1627200;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_testudata(L, 2, "QEvent*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1626c60;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x16276b0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__postEvent__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__postEvent__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__postEvent matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__organizationDomain (lua_State *L) {
  QString ret = QCoreApplication::organizationDomain();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__addLibraryPath (lua_State *L) {
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 1), lua_objlen(L, 1));
  QCoreApplication::addLibraryPath(arg1);
  return 0;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QCoreApplication::trUtf8(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QCoreApplication::trUtf8(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__trUtf8 (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1615390;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x1614ed0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1617190;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x16168a0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1617610;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__trUtf8__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__trUtf8__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__trUtf8 matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__setAttribute (lua_State *L) {
  Qt::ApplicationAttribute arg1 = static_cast<Qt::ApplicationAttribute>(lqtL_toenum(L, 1, "Qt::ApplicationAttribute"));
  bool arg2 = lua_isboolean(L, 2)?(bool)lua_toboolean(L, 2):true;
  QCoreApplication::setAttribute(arg1, arg2);
  return 0;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QCoreApplication::tr(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QCoreApplication::tr(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__tr (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1614610;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x1614370;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1616070;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1616550;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1616900;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__tr__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__tr__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__tr matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__applicationFilePath (lua_State *L) {
  QString ret = QCoreApplication::applicationFilePath();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__instance (lua_State *L) {
  QCoreApplication * ret = QCoreApplication::instance();
  lqtL_pushudata(L, ret, "QCoreApplication*");
  return 1;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__argv (lua_State *L) {
  char * * ret = QCoreApplication::argv();
  lqtL_pusharguments(L, ret);
  return 1;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__setOrganizationName (lua_State *L) {
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 1), lua_objlen(L, 1));
  QCoreApplication::setOrganizationName(arg1);
  return 0;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__organizationName (lua_State *L) {
  QString ret = QCoreApplication::organizationName();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__applicationDirPath (lua_State *L) {
  QString ret = QCoreApplication::applicationDirPath();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__hasPendingEvents (lua_State *L) {
  bool ret = QCoreApplication::hasPendingEvents();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__testAttribute (lua_State *L) {
  Qt::ApplicationAttribute arg1 = static_cast<Qt::ApplicationAttribute>(lqtL_toenum(L, 1, "Qt::ApplicationAttribute"));
  bool ret = QCoreApplication::testAttribute(arg1);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__delete (lua_State *L) {
  QCoreApplication *& __lua__obj = *static_cast<QCoreApplication**>(lqtL_toudata(L, 1, "QCoreApplication"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  delete __lua__obj;
  __lua__obj = 0;
  return 0;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__libraryPaths (lua_State *L) {
  QStringList ret = QCoreApplication::libraryPaths();
  lqtL_passudata(L, new QStringList(ret), "QStringList*");
  return 1;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__quit (lua_State *L) {
  QCoreApplication::quit();
  return 0;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__removeLibraryPath (lua_State *L) {
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 1), lua_objlen(L, 1));
  QCoreApplication::removeLibraryPath(arg1);
  return 0;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__argc (lua_State *L) {
  int ret = QCoreApplication::argc();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__translate__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  const char * arg3 = (lua_type(L, 3)==LUA_TSTRING)?lua_tostring(L, 3):static_cast< const char * >(0);
  QCoreApplication::Encoding arg4 = lqtL_isenum(L, 4, "QCoreApplication::Encoding")?static_cast<QCoreApplication::Encoding>(lqtL_toenum(L, 4, "QCoreApplication::Encoding")):QCoreApplication::CodecForTr;
  QString ret = QCoreApplication::translate(arg1, arg2, arg3, arg4);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__translate__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  const char * arg3 = lua_tostring(L, 3);
  QCoreApplication::Encoding arg4 = static_cast<QCoreApplication::Encoding>(lqtL_toenum(L, 4, "QCoreApplication::Encoding"));
  int arg5 = lua_tointeger(L, 5);
  QString ret = QCoreApplication::translate(arg1, arg2, arg3, arg4, arg5);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__translate (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1632560;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1631ff0;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 3)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x1632a10;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_isenum(L, 4, "QCoreApplication::Encoding")) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x16332e0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1633c80;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1634190;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 3)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1634130;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_isenum(L, 4, "QCoreApplication::Encoding")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1634980;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 5)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1634d20;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__translate__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__translate__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__translate matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__arguments (lua_State *L) {
  QStringList ret = QCoreApplication::arguments();
  lqtL_passudata(L, new QStringList(ret), "QStringList*");
  return 1;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__removePostedEvents__OverloadedVersion__1 (lua_State *L) {
  QObject * arg1 = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
  QCoreApplication::removePostedEvents(arg1);
  return 0;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__removePostedEvents__OverloadedVersion__2 (lua_State *L) {
  QObject * arg1 = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
  int arg2 = lua_tointeger(L, 2);
  QCoreApplication::removePostedEvents(arg1, arg2);
  return 0;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__removePostedEvents (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if (lqtL_testudata(L, 1, "QObject*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1629bc0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if (lqtL_testudata(L, 1, "QObject*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x162a650;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 2)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x162a0d0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__removePostedEvents__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__removePostedEvents__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__removePostedEvents matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__metaObject (lua_State *L) {
  QCoreApplication *& __lua__obj = *static_cast<QCoreApplication**>(lqtL_toudata(L, 1, "QCoreApplication"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QMetaObject * ret = __lua__obj->QCoreApplication::metaObject();
  lqtL_pushudata(L, ret, "QMetaObject*");
  return 1;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__closingDown (lua_State *L) {
  bool ret = QCoreApplication::closingDown();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__sendPostedEvents__OverloadedVersion__1 (lua_State *L) {
  QObject * arg1 = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
  int arg2 = lua_tointeger(L, 2);
  QCoreApplication::sendPostedEvents(arg1, arg2);
  return 0;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__sendPostedEvents__OverloadedVersion__2 (lua_State *L) {
  QCoreApplication::sendPostedEvents();
  return 0;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__sendPostedEvents (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if (lqtL_testudata(L, 1, "QObject*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1628530;
  } else {
    score[1] -= premium*premium;
  }
  if (lua_isnumber(L, 2)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1627a70;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__sendPostedEvents__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__sendPostedEvents__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__sendPostedEvents matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__applicationName (lua_State *L) {
  QString ret = QCoreApplication::applicationName();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__startingUp (lua_State *L) {
  bool ret = QCoreApplication::startingUp();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__watchUnixSignal (lua_State *L) {
  int arg1 = lua_tointeger(L, 1);
  bool arg2 = (bool)lua_toboolean(L, 2);
  QCoreApplication::watchUnixSignal(arg1, arg2);
  return 0;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__flush (lua_State *L) {
  QCoreApplication::flush();
  return 0;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__setOrganizationDomain (lua_State *L) {
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 1), lua_objlen(L, 1));
  QCoreApplication::setOrganizationDomain(arg1);
  return 0;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__exit (lua_State *L) {
  int arg1 = lua_isnumber(L, 1)?lua_tointeger(L, 1):static_cast< int >(0);
  QCoreApplication::exit(arg1);
  return 0;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__removeTranslator (lua_State *L) {
  QTranslator * arg1 = *static_cast<QTranslator**>(lqtL_checkudata(L, 1, "QTranslator*"));
  QCoreApplication::removeTranslator(arg1);
  return 0;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__sendEvent (lua_State *L) {
  QObject * arg1 = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
  QEvent * arg2 = *static_cast<QEvent**>(lqtL_checkudata(L, 2, "QEvent*"));
  bool ret = QCoreApplication::sendEvent(arg1, arg2);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__notify (lua_State *L) {
  QCoreApplication *& __lua__obj = *static_cast<QCoreApplication**>(lqtL_toudata(L, 1, "QCoreApplication"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QObject * arg1 = *static_cast<QObject**>(lqtL_checkudata(L, 2, "QObject*"));
  QEvent * arg2 = *static_cast<QEvent**>(lqtL_checkudata(L, 3, "QEvent*"));
  bool ret = __lua__obj->QCoreApplication::notify(arg1, arg2);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__setLibraryPaths (lua_State *L) {
  const QStringList& arg1 = **static_cast<QStringList**>(lqtL_checkudata(L, 1, "QStringList*"));
  QCoreApplication::setLibraryPaths(arg1);
  return 0;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__filterEvent (lua_State *L) {
  QCoreApplication *& __lua__obj = *static_cast<QCoreApplication**>(lqtL_toudata(L, 1, "QCoreApplication"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  void * arg1 = lua_touserdata(L, 2);
  long int * arg2 = static_cast<long int *>(lua_touserdata(L, 3));
  bool ret = __lua__obj->QCoreApplication::filterEvent(arg1, arg2);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__exec (lua_State *L) {
  int ret = QCoreApplication::exec();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__installTranslator (lua_State *L) {
  QTranslator * arg1 = *static_cast<QTranslator**>(lqtL_checkudata(L, 1, "QTranslator*"));
  QCoreApplication::installTranslator(arg1);
  return 0;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__processEvents__OverloadedVersion__1 (lua_State *L) {
  QFlags<QEventLoop::ProcessEventsFlag> arg1 = lqtL_testudata(L, 1, "QFlags<QEventLoop::ProcessEventsFlag>*")?**static_cast<QFlags<QEventLoop::ProcessEventsFlag>**>(lqtL_checkudata(L, 1, "QFlags<QEventLoop::ProcessEventsFlag>*")):QEventLoop::AllEvents;
  QCoreApplication::processEvents(arg1);
  return 0;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__processEvents__OverloadedVersion__2 (lua_State *L) {
  QFlags<QEventLoop::ProcessEventsFlag> arg1 = **static_cast<QFlags<QEventLoop::ProcessEventsFlag>**>(lqtL_checkudata(L, 1, "QFlags<QEventLoop::ProcessEventsFlag>*"));
  int arg2 = lua_tointeger(L, 2);
  QCoreApplication::processEvents(arg1, arg2);
  return 0;
}
int LuaBinder< QCoreApplication >::__LuaWrapCall__processEvents (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if (lqtL_testudata(L, 1, "QFlags<QEventLoop::ProcessEventsFlag>*")) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x1622ee0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if (lqtL_testudata(L, 1, "QFlags<QEventLoop::ProcessEventsFlag>*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1623a40;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 2)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1623740;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__processEvents__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__processEvents__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__processEvents matching arguments");
	lua_error(L);
	return 0;
}
bool LuaBinder< QCoreApplication >::notify (QObject * arg1, QEvent * arg2) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QCoreApplication*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "notify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QObject*");
  lqtL_pushudata(L, arg2, "QEvent*");
  if (lua_isfunction(L, -2-2)) {
    lua_pcall(L, 2+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QCoreApplication::notify(arg1, arg2);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
bool LuaBinder< QCoreApplication >::eventFilter (QObject * arg1, QEvent * arg2) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QCoreApplication*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "eventFilter");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QObject*");
  lqtL_pushudata(L, arg2, "QEvent*");
  if (lua_isfunction(L, -2-2)) {
    lua_pcall(L, 2+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QObject::eventFilter(arg1, arg2);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
const QMetaObject * LuaBinder< QCoreApplication >::metaObject () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QCoreApplication*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "metaObject");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QCoreApplication::metaObject();
  }
  const QMetaObject * ret = *static_cast<QMetaObject**>(lqtL_checkudata(L, -1, "QMetaObject*"));
  lua_settop(L, oldtop);
  return ret;
}
bool LuaBinder< QCoreApplication >::event (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QCoreApplication*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "event");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QCoreApplication::event(arg1);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QCoreApplication >::connectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QCoreApplication*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "connectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::connectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QCoreApplication >::timerEvent (QTimerEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QCoreApplication*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "timerEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QTimerEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::timerEvent(arg1);
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QCoreApplication >::compressEvent (QEvent * arg1, QObject * arg2, QPostEventList * arg3) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QCoreApplication*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "compressEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  lqtL_pushudata(L, arg2, "QObject*");
  lqtL_pushudata(L, arg3, "QPostEventList*");
  if (lua_isfunction(L, -3-2)) {
    lua_pcall(L, 3+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QCoreApplication::compressEvent(arg1, arg2, arg3);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QCoreApplication >::disconnectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QCoreApplication*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "disconnectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::disconnectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QCoreApplication >::childEvent (QChildEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QCoreApplication*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "childEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QChildEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::childEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QCoreApplication >::customEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QCoreApplication*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "customEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::customEvent(arg1);
  }
  lua_settop(L, oldtop);
}
LuaBinder< QCoreApplication >::  ~LuaBinder< QCoreApplication > () {
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QCoreApplication*");
  lua_getfield(L, -1, "~QCoreApplication");

  if (lua_isfunction(L, -1)) {
    lua_insert(L, -2);
    lua_pcall(L, 1, 1, 0);
  } else {
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QCoreApplication >::lqt_pushenum_Encoding (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "CodecForTr");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "CodecForTr");
  lua_pushstring(L, "UnicodeUTF8");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "UnicodeUTF8");
  lua_pushstring(L, "DefaultCodec");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "DefaultCodec");
  lua_pushcfunction(L, LuaBinder< QCoreApplication >::lqt_pushenum_Encoding_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QCoreApplication::Encoding");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QCoreApplication >::lqt_pushenum_Encoding_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QCoreApplication::Encoding>*) + sizeof(QFlags<QCoreApplication::Encoding>));
  QFlags<QCoreApplication::Encoding> *fl = static_cast<QFlags<QCoreApplication::Encoding>*>( static_cast<void*>(&static_cast<QFlags<QCoreApplication::Encoding>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QCoreApplication::Encoding>(lqtL_toenum(L, i, "QCoreApplication::Encoding"));
	}
	if (luaL_newmetatable(L, "QFlags<QCoreApplication::Encoding>*")) {
		lua_pushstring(L, "QFlags<QCoreApplication::Encoding>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int luaopen_QCoreApplication (lua_State *L) {
  if (luaL_newmetatable(L, "QCoreApplication*")) {
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__setApplicationName);
    lua_setfield(L, -2, "setApplicationName");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__postEvent);
    lua_setfield(L, -2, "postEvent");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__organizationDomain);
    lua_setfield(L, -2, "organizationDomain");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__addLibraryPath);
    lua_setfield(L, -2, "addLibraryPath");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__trUtf8);
    lua_setfield(L, -2, "trUtf8");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__setAttribute);
    lua_setfield(L, -2, "setAttribute");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__tr);
    lua_setfield(L, -2, "tr");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__applicationFilePath);
    lua_setfield(L, -2, "applicationFilePath");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__instance);
    lua_setfield(L, -2, "instance");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__argv);
    lua_setfield(L, -2, "argv");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__setOrganizationName);
    lua_setfield(L, -2, "setOrganizationName");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__organizationName);
    lua_setfield(L, -2, "organizationName");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__applicationDirPath);
    lua_setfield(L, -2, "applicationDirPath");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__hasPendingEvents);
    lua_setfield(L, -2, "hasPendingEvents");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__testAttribute);
    lua_setfield(L, -2, "testAttribute");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__delete);
    lua_setfield(L, -2, "delete");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__libraryPaths);
    lua_setfield(L, -2, "libraryPaths");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__quit);
    lua_setfield(L, -2, "quit");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__removeLibraryPath);
    lua_setfield(L, -2, "removeLibraryPath");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__argc);
    lua_setfield(L, -2, "argc");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__translate);
    lua_setfield(L, -2, "translate");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__arguments);
    lua_setfield(L, -2, "arguments");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__removePostedEvents);
    lua_setfield(L, -2, "removePostedEvents");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__metaObject);
    lua_setfield(L, -2, "metaObject");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__closingDown);
    lua_setfield(L, -2, "closingDown");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__sendPostedEvents);
    lua_setfield(L, -2, "sendPostedEvents");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__applicationName);
    lua_setfield(L, -2, "applicationName");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__startingUp);
    lua_setfield(L, -2, "startingUp");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__watchUnixSignal);
    lua_setfield(L, -2, "watchUnixSignal");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__flush);
    lua_setfield(L, -2, "flush");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__setOrganizationDomain);
    lua_setfield(L, -2, "setOrganizationDomain");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__exit);
    lua_setfield(L, -2, "exit");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__removeTranslator);
    lua_setfield(L, -2, "removeTranslator");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__sendEvent);
    lua_setfield(L, -2, "sendEvent");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__notify);
    lua_setfield(L, -2, "notify");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__setLibraryPaths);
    lua_setfield(L, -2, "setLibraryPaths");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__filterEvent);
    lua_setfield(L, -2, "filterEvent");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__exec);
    lua_setfield(L, -2, "exec");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__installTranslator);
    lua_setfield(L, -2, "installTranslator");
    lua_pushcfunction(L, LuaBinder< QCoreApplication >::__LuaWrapCall__processEvents);
    lua_setfield(L, -2, "processEvents");
    LuaBinder< QCoreApplication >::lqt_pushenum_Encoding(L);
    lua_setfield(L, -2, "Encoding");
    lua_newtable(L);
    lua_pushboolean(L, 1);
    lua_setfield(L, -2, "QObject*");
    lua_setfield(L, -2, "__base");
    lua_pushcfunction(L, lqtL_newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, lqtL_index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, lqtL_gc);
    lua_setfield(L, -2, "__gc");
    lua_pushstring(L, "QCoreApplication");
    lua_setfield(L, -2, "__qtype");
		lua_setglobal(L, "QCoreApplication");
  } else {
		lua_pop(L, 1);
	}
  return 0;
}
