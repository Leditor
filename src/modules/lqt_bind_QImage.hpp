#include "lqt_common.hpp"
#include <QImage>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QImage > : public QImage {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__trueMatrix__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__trueMatrix__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__trueMatrix (lua_State *L);
  static int __LuaWrapCall__delete (lua_State *L);
  static int __LuaWrapCall__textList (lua_State *L);
  static int __LuaWrapCall__allGray (lua_State *L);
  static int __LuaWrapCall__save__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__save__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__save (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__8 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__9 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__10 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__11 (lua_State *L);
  static int __LuaWrapCall__new (lua_State *L);
  static int __LuaWrapCall__createMaskFromColor (lua_State *L);
  static int __LuaWrapCall__depth (lua_State *L);
  static int __LuaWrapCall__copy__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__copy__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__copy (lua_State *L);
  static int __LuaWrapCall__devType (lua_State *L);
  static int __LuaWrapCall__textKeys (lua_State *L);
  static int __LuaWrapCall__isDetached (lua_State *L);
  static int __LuaWrapCall__format (lua_State *L);
  static int __LuaWrapCall__scaledToWidth (lua_State *L);
  static int __LuaWrapCall__size (lua_State *L);
  static int __LuaWrapCall__valid__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__valid__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__valid (lua_State *L);
  static int __LuaWrapCall__convertToFormat__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__convertToFormat__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__convertToFormat (lua_State *L);
  static int __LuaWrapCall__serialNumber (lua_State *L);
  static int __LuaWrapCall__setColorTable (lua_State *L);
  static int __LuaWrapCall__bytesPerLine (lua_State *L);
  static int __LuaWrapCall__text__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__text__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__text__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__text (lua_State *L);
  static int __LuaWrapCall__rgbSwapped (lua_State *L);
  static int __LuaWrapCall__numColors (lua_State *L);
  static int __LuaWrapCall__scaled__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__scaled__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__scaled (lua_State *L);
  static int __LuaWrapCall__rect (lua_State *L);
  static int __LuaWrapCall__detach (lua_State *L);
  static int __LuaWrapCall__offset (lua_State *L);
  static int __LuaWrapCall__height (lua_State *L);
  static int __LuaWrapCall__textLanguages (lua_State *L);
  static int __LuaWrapCall__pixel__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__pixel__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__pixel (lua_State *L);
  static int __LuaWrapCall__setColor (lua_State *L);
  static int __LuaWrapCall__setPixel__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__setPixel__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__setPixel (lua_State *L);
  static int __LuaWrapCall__bits__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__bits__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__bits (lua_State *L);
  static int __LuaWrapCall__color (lua_State *L);
  static int __LuaWrapCall__load__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__load__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__load (lua_State *L);
  static int __LuaWrapCall__fill (lua_State *L);
  static int __LuaWrapCall__setText__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__setText__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__setText (lua_State *L);
  static int __LuaWrapCall__colorTable (lua_State *L);
  static int __LuaWrapCall__paintEngine (lua_State *L);
  static int __LuaWrapCall__setAlphaChannel (lua_State *L);
  static int __LuaWrapCall__pixelIndex__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__pixelIndex__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__pixelIndex (lua_State *L);
  static int __LuaWrapCall__hasAlphaChannel (lua_State *L);
  static int __LuaWrapCall__numBytes (lua_State *L);
  static int __LuaWrapCall__dotsPerMeterX (lua_State *L);
  static int __LuaWrapCall__setOffset (lua_State *L);
  static int __LuaWrapCall__setDotsPerMeterX (lua_State *L);
  static int __LuaWrapCall__invertPixels (lua_State *L);
  static int __LuaWrapCall__createAlphaMask (lua_State *L);
  static int __LuaWrapCall__dotsPerMeterY (lua_State *L);
  static int __LuaWrapCall__isGrayscale (lua_State *L);
  static int __LuaWrapCall__mirrored (lua_State *L);
  static int __LuaWrapCall__isNull (lua_State *L);
  static int __LuaWrapCall__width (lua_State *L);
  static int __LuaWrapCall__createHeuristicMask (lua_State *L);
  static int __LuaWrapCall__setNumColors (lua_State *L);
  static int __LuaWrapCall__scaledToHeight (lua_State *L);
  static int __LuaWrapCall__scanLine__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__scanLine__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__scanLine (lua_State *L);
  static int __LuaWrapCall__cacheKey (lua_State *L);
  static int __LuaWrapCall__setDotsPerMeterY (lua_State *L);
  static int __LuaWrapCall__alphaChannel (lua_State *L);
  static int __LuaWrapCall__transformed__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__transformed__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__transformed (lua_State *L);
  QPaintEngine * paintEngine () const;
protected:
  int metric (QPaintDevice::PaintDeviceMetric arg1) const;
public:
  int devType () const;
  ~LuaBinder< QImage > ();
  static int lqt_pushenum_InvertMode (lua_State *L);
  static int lqt_pushenum_InvertMode_QFLAGS_CREATOR (lua_State *L);
  static int lqt_pushenum_Format (lua_State *L);
  static int lqt_pushenum_Format_QFLAGS_CREATOR (lua_State *L);
  LuaBinder< QImage > (lua_State *l):QImage(), L(l) {}
  LuaBinder< QImage > (lua_State *l, const QSize& arg1, QImage::Format arg2):QImage(arg1, arg2), L(l) {}
  LuaBinder< QImage > (lua_State *l, int arg1, int arg2, QImage::Format arg3):QImage(arg1, arg2, arg3), L(l) {}
  LuaBinder< QImage > (lua_State *l, unsigned char * arg1, int arg2, int arg3, QImage::Format arg4):QImage(arg1, arg2, arg3, arg4), L(l) {}
  LuaBinder< QImage > (lua_State *l, const unsigned char * arg1, int arg2, int arg3, QImage::Format arg4):QImage(arg1, arg2, arg3, arg4), L(l) {}
  LuaBinder< QImage > (lua_State *l, unsigned char * arg1, int arg2, int arg3, int arg4, QImage::Format arg5):QImage(arg1, arg2, arg3, arg4, arg5), L(l) {}
  LuaBinder< QImage > (lua_State *l, const unsigned char * arg1, int arg2, int arg3, int arg4, QImage::Format arg5):QImage(arg1, arg2, arg3, arg4, arg5), L(l) {}
  LuaBinder< QImage > (lua_State *l, const char * const * arg1):QImage(arg1), L(l) {}
  LuaBinder< QImage > (lua_State *l, const QString& arg1, const char * arg2):QImage(arg1, arg2), L(l) {}
  LuaBinder< QImage > (lua_State *l, const char * arg1, const char * arg2):QImage(arg1, arg2), L(l) {}
  LuaBinder< QImage > (lua_State *l, const QImage& arg1):QImage(arg1), L(l) {}
};

extern "C" int luaopen_QImage (lua_State *L);
