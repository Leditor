#include "lqt_bind_QMessageBox.hpp"

int LuaBinder< QMessageBox >::__LuaWrapCall__standardButtons (lua_State *L) {
  QMessageBox *& __lua__obj = *static_cast<QMessageBox**>(lqtL_checkudata(L, 1, "QMessageBox*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<QMessageBox::StandardButton> ret = __lua__obj->QMessageBox::standardButtons();
  lqtL_passudata(L, new QFlags<QMessageBox::StandardButton>(ret), "QFlags<QMessageBox::StandardButton>*");
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__setWindowModality (lua_State *L) {
  QMessageBox *& __lua__obj = *static_cast<QMessageBox**>(lqtL_checkudata(L, 1, "QMessageBox*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::WindowModality arg1 = static_cast<Qt::WindowModality>(lqtL_toenum(L, 2, "Qt::WindowModality"));
  __lua__obj->QMessageBox::setWindowModality(arg1);
  return 0;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__sizeHint (lua_State *L) {
  QMessageBox *& __lua__obj = *static_cast<QMessageBox**>(lqtL_checkudata(L, 1, "QMessageBox*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QSize ret = __lua__obj->QMessageBox::sizeHint();
  lqtL_passudata(L, new QSize(ret), "QSize*");
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__defaultButton (lua_State *L) {
  QMessageBox *& __lua__obj = *static_cast<QMessageBox**>(lqtL_checkudata(L, 1, "QMessageBox*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QPushButton * ret = __lua__obj->QMessageBox::defaultButton();
  lqtL_pushudata(L, ret, "QPushButton*");
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__setDefaultButton__OverloadedVersion__1 (lua_State *L) {
  QMessageBox *& __lua__obj = *static_cast<QMessageBox**>(lqtL_checkudata(L, 1, "QMessageBox*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QPushButton * arg1 = *static_cast<QPushButton**>(lqtL_checkudata(L, 2, "QPushButton*"));
  __lua__obj->QMessageBox::setDefaultButton(arg1);
  return 0;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__setDefaultButton__OverloadedVersion__2 (lua_State *L) {
  QMessageBox *& __lua__obj = *static_cast<QMessageBox**>(lqtL_checkudata(L, 1, "QMessageBox*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QMessageBox::StandardButton arg1 = static_cast<QMessageBox::StandardButton>(lqtL_toenum(L, 2, "QMessageBox::StandardButton"));
  __lua__obj->QMessageBox::setDefaultButton(arg1);
  return 0;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__setDefaultButton (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QMessageBox*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPushButton*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1337170;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QMessageBox*")?premium:-premium*premium;
  if (lqtL_isenum(L, 2, "QMessageBox::StandardButton")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1337c20;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__setDefaultButton__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__setDefaultButton__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__setDefaultButton matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__setIcon (lua_State *L) {
  QMessageBox *& __lua__obj = *static_cast<QMessageBox**>(lqtL_checkudata(L, 1, "QMessageBox*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QMessageBox::Icon arg1 = static_cast<QMessageBox::Icon>(lqtL_toenum(L, 2, "QMessageBox::Icon"));
  __lua__obj->QMessageBox::setIcon(arg1);
  return 0;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__setStandardButtons (lua_State *L) {
  QMessageBox *& __lua__obj = *static_cast<QMessageBox**>(lqtL_checkudata(L, 1, "QMessageBox*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<QMessageBox::StandardButton> arg1 = **static_cast<QFlags<QMessageBox::StandardButton>**>(lqtL_checkudata(L, 2, "QFlags<QMessageBox::StandardButton>*"));
  __lua__obj->QMessageBox::setStandardButtons(arg1);
  return 0;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__button (lua_State *L) {
  QMessageBox *& __lua__obj = *static_cast<QMessageBox**>(lqtL_checkudata(L, 1, "QMessageBox*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QMessageBox::StandardButton arg1 = static_cast<QMessageBox::StandardButton>(lqtL_toenum(L, 2, "QMessageBox::StandardButton"));
  QAbstractButton * ret = __lua__obj->QMessageBox::button(arg1);
  lqtL_pushudata(L, ret, "QAbstractButton*");
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__standardIcon (lua_State *L) {
  QMessageBox::Icon arg1 = static_cast<QMessageBox::Icon>(lqtL_toenum(L, 1, "QMessageBox::Icon"));
  QPixmap ret = QMessageBox::standardIcon(arg1);
  lqtL_passudata(L, new QPixmap(ret), "QPixmap*");
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QMessageBox::tr(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QMessageBox::tr(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__tr (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1328e70;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x1328bd0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x132a8b0;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x132ada0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x132b150;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__tr__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__tr__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__tr matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__textFormat (lua_State *L) {
  QMessageBox *& __lua__obj = *static_cast<QMessageBox**>(lqtL_checkudata(L, 1, "QMessageBox*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::TextFormat ret = __lua__obj->QMessageBox::textFormat();
  lqtL_pushenum(L, ret, "Qt::TextFormat");
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__informativeText (lua_State *L) {
  QMessageBox *& __lua__obj = *static_cast<QMessageBox**>(lqtL_checkudata(L, 1, "QMessageBox*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QMessageBox::informativeText();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__removeButton (lua_State *L) {
  QMessageBox *& __lua__obj = *static_cast<QMessageBox**>(lqtL_checkudata(L, 1, "QMessageBox*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QAbstractButton * arg1 = *static_cast<QAbstractButton**>(lqtL_checkudata(L, 2, "QAbstractButton*"));
  __lua__obj->QMessageBox::removeButton(arg1);
  return 0;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__buttonText (lua_State *L) {
  QMessageBox *& __lua__obj = *static_cast<QMessageBox**>(lqtL_checkudata(L, 1, "QMessageBox*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  QString ret = __lua__obj->QMessageBox::buttonText(arg1);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__critical__OverloadedVersion__1 (lua_State *L) {
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
  const QString& arg2 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  const QString& arg3 = QString::fromAscii(lua_tostring(L, 3), lua_objlen(L, 3));
  QFlags<QMessageBox::StandardButton> arg4 = lqtL_testudata(L, 4, "QFlags<QMessageBox::StandardButton>*")?**static_cast<QFlags<QMessageBox::StandardButton>**>(lqtL_checkudata(L, 4, "QFlags<QMessageBox::StandardButton>*")):Ok;
  QMessageBox::StandardButton arg5 = lqtL_isenum(L, 5, "QMessageBox::StandardButton")?static_cast<QMessageBox::StandardButton>(lqtL_toenum(L, 5, "QMessageBox::StandardButton")):QMessageBox::NoButton;
  QMessageBox::StandardButton ret = QMessageBox::critical(arg1, arg2, arg3, arg4, arg5);
  lqtL_pushenum(L, ret, "QMessageBox::StandardButton");
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__critical__OverloadedVersion__2 (lua_State *L) {
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
  const QString& arg2 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  const QString& arg3 = QString::fromAscii(lua_tostring(L, 3), lua_objlen(L, 3));
  int arg4 = lua_tointeger(L, 4);
  int arg5 = lua_tointeger(L, 5);
  int arg6 = lua_isnumber(L, 6)?lua_tointeger(L, 6):static_cast< int >(0);
  int ret = QMessageBox::critical(arg1, arg2, arg3, arg4, arg5, arg6);
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__critical__OverloadedVersion__3 (lua_State *L) {
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
  const QString& arg2 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  const QString& arg3 = QString::fromAscii(lua_tostring(L, 3), lua_objlen(L, 3));
  const QString& arg4 = QString::fromAscii(lua_tostring(L, 4), lua_objlen(L, 4));
  const QString& arg5 = (lua_type(L, 5)==LUA_TSTRING)?QString::fromAscii(lua_tostring(L, 5), lua_objlen(L, 5)):QString();
  const QString& arg6 = (lua_type(L, 6)==LUA_TSTRING)?QString::fromAscii(lua_tostring(L, 6), lua_objlen(L, 6)):QString();
  int arg7 = lua_isnumber(L, 7)?lua_tointeger(L, 7):static_cast< int >(0);
  int arg8 = lua_isnumber(L, 8)?lua_tointeger(L, 8):static_cast< int >(-1);
  int ret = QMessageBox::critical(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__critical__OverloadedVersion__4 (lua_State *L) {
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
  const QString& arg2 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  const QString& arg3 = QString::fromAscii(lua_tostring(L, 3), lua_objlen(L, 3));
  QMessageBox::StandardButton arg4 = static_cast<QMessageBox::StandardButton>(lqtL_toenum(L, 4, "QMessageBox::StandardButton"));
  QMessageBox::StandardButton arg5 = static_cast<QMessageBox::StandardButton>(lqtL_toenum(L, 5, "QMessageBox::StandardButton"));
  int ret = QMessageBox::critical(arg1, arg2, arg3, arg4, arg5);
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__critical (lua_State *L) {
  int score[5];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if (lqtL_testudata(L, 1, "QWidget*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x13442d0;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x13434f0;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 3)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x13447f0;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_testudata(L, 4, "QFlags<QMessageBox::StandardButton>*")) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x1345050;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_isenum(L, 5, "QMessageBox::StandardButton")) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x1345420;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if (lqtL_testudata(L, 1, "QWidget*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x135ca40;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x135bcd0;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 3)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x135cef0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x135d750;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 5)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x135db00;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 6)) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x135d6a0;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  if (lqtL_testudata(L, 1, "QWidget*")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x135e850;
  } else {
    score[3] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x135e520;
  } else {
    score[3] -= premium*premium;
  }
  if ((lua_type(L, 3)==LUA_TSTRING)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x135ed00;
  } else {
    score[3] -= premium*premium;
  }
  if ((lua_type(L, 4)==LUA_TSTRING)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x135f560;
  } else {
    score[3] -= premium*premium;
  }
  if ((lua_type(L, 5)==LUA_TSTRING)) {
    score[3] += premium;
  } else if (true) {
    score[3] += premium-1; // table: 0x135f920;
  } else {
    score[3] -= premium*premium;
  }
  if ((lua_type(L, 6)==LUA_TSTRING)) {
    score[3] += premium;
  } else if (true) {
    score[3] += premium-1; // table: 0x135f4b0;
  } else {
    score[3] -= premium*premium;
  }
  if (lua_isnumber(L, 7)) {
    score[3] += premium;
  } else if (true) {
    score[3] += premium-1; // table: 0x1360110;
  } else {
    score[3] -= premium*premium;
  }
  if (lua_isnumber(L, 8)) {
    score[3] += premium;
  } else if (true) {
    score[3] += premium-1; // table: 0x13604d0;
  } else {
    score[3] -= premium*premium;
  }
  score[4] = 0;
  if (lqtL_testudata(L, 1, "QWidget*")) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x1360e00;
  } else {
    score[4] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x1360ac0;
  } else {
    score[4] -= premium*premium;
  }
  if ((lua_type(L, 3)==LUA_TSTRING)) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x13612f0;
  } else {
    score[4] -= premium*premium;
  }
  if (lqtL_isenum(L, 4, "QMessageBox::StandardButton")) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x1361b50;
  } else {
    score[4] -= premium*premium;
  }
  if (lqtL_isenum(L, 5, "QMessageBox::StandardButton")) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x1361f00;
  } else {
    score[4] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=4;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__critical__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__critical__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__critical__OverloadedVersion__3(L); break;
    case 4: return __LuaWrapCall__critical__OverloadedVersion__4(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__critical matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__addButton__OverloadedVersion__1 (lua_State *L) {
  QMessageBox *& __lua__obj = *static_cast<QMessageBox**>(lqtL_checkudata(L, 1, "QMessageBox*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QAbstractButton * arg1 = *static_cast<QAbstractButton**>(lqtL_checkudata(L, 2, "QAbstractButton*"));
  QMessageBox::ButtonRole arg2 = static_cast<QMessageBox::ButtonRole>(lqtL_toenum(L, 3, "QMessageBox::ButtonRole"));
  __lua__obj->QMessageBox::addButton(arg1, arg2);
  return 0;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__addButton__OverloadedVersion__2 (lua_State *L) {
  QMessageBox *& __lua__obj = *static_cast<QMessageBox**>(lqtL_checkudata(L, 1, "QMessageBox*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  QMessageBox::ButtonRole arg2 = static_cast<QMessageBox::ButtonRole>(lqtL_toenum(L, 3, "QMessageBox::ButtonRole"));
  QPushButton * ret = __lua__obj->QMessageBox::addButton(arg1, arg2);
  lqtL_pushudata(L, ret, "QPushButton*");
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__addButton__OverloadedVersion__3 (lua_State *L) {
  QMessageBox *& __lua__obj = *static_cast<QMessageBox**>(lqtL_checkudata(L, 1, "QMessageBox*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QMessageBox::StandardButton arg1 = static_cast<QMessageBox::StandardButton>(lqtL_toenum(L, 2, "QMessageBox::StandardButton"));
  QPushButton * ret = __lua__obj->QMessageBox::addButton(arg1);
  lqtL_pushudata(L, ret, "QPushButton*");
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__addButton (lua_State *L) {
  int score[4];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QMessageBox*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QAbstractButton*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1330cf0;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_isenum(L, 3, "QMessageBox::ButtonRole")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1330960;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QMessageBox*")?premium:-premium*premium;
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1331cd0;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_isenum(L, 3, "QMessageBox::ButtonRole")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1332250;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  score[3] += lqtL_testudata(L, 1, "QMessageBox*")?premium:-premium*premium;
  if (lqtL_isenum(L, 2, "QMessageBox::StandardButton")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1332b70;
  } else {
    score[3] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=3;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__addButton__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__addButton__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__addButton__OverloadedVersion__3(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__addButton matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__setDetailedText (lua_State *L) {
  QMessageBox *& __lua__obj = *static_cast<QMessageBox**>(lqtL_checkudata(L, 1, "QMessageBox*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QMessageBox::setDetailedText(arg1);
  return 0;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__about (lua_State *L) {
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
  const QString& arg2 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  const QString& arg3 = QString::fromAscii(lua_tostring(L, 3), lua_objlen(L, 3));
  QMessageBox::about(arg1, arg2, arg3);
  return 0;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__setText (lua_State *L) {
  QMessageBox *& __lua__obj = *static_cast<QMessageBox**>(lqtL_checkudata(L, 1, "QMessageBox*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QMessageBox::setText(arg1);
  return 0;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__aboutQt (lua_State *L) {
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
  const QString& arg2 = (lua_type(L, 2)==LUA_TSTRING)?QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2)):QString();
  QMessageBox::aboutQt(arg1, arg2);
  return 0;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__iconPixmap (lua_State *L) {
  QMessageBox *& __lua__obj = *static_cast<QMessageBox**>(lqtL_checkudata(L, 1, "QMessageBox*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QPixmap ret = __lua__obj->QMessageBox::iconPixmap();
  lqtL_passudata(L, new QPixmap(ret), "QPixmap*");
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__setEscapeButton__OverloadedVersion__1 (lua_State *L) {
  QMessageBox *& __lua__obj = *static_cast<QMessageBox**>(lqtL_checkudata(L, 1, "QMessageBox*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QAbstractButton * arg1 = *static_cast<QAbstractButton**>(lqtL_checkudata(L, 2, "QAbstractButton*"));
  __lua__obj->QMessageBox::setEscapeButton(arg1);
  return 0;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__setEscapeButton__OverloadedVersion__2 (lua_State *L) {
  QMessageBox *& __lua__obj = *static_cast<QMessageBox**>(lqtL_checkudata(L, 1, "QMessageBox*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QMessageBox::StandardButton arg1 = static_cast<QMessageBox::StandardButton>(lqtL_toenum(L, 2, "QMessageBox::StandardButton"));
  __lua__obj->QMessageBox::setEscapeButton(arg1);
  return 0;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__setEscapeButton (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QMessageBox*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QAbstractButton*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1338e60;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QMessageBox*")?premium:-premium*premium;
  if (lqtL_isenum(L, 2, "QMessageBox::StandardButton")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x13398e0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__setEscapeButton__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__setEscapeButton__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__setEscapeButton matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__standardButton (lua_State *L) {
  QMessageBox *& __lua__obj = *static_cast<QMessageBox**>(lqtL_checkudata(L, 1, "QMessageBox*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QAbstractButton * arg1 = *static_cast<QAbstractButton**>(lqtL_checkudata(L, 2, "QAbstractButton*"));
  QMessageBox::StandardButton ret = __lua__obj->QMessageBox::standardButton(arg1);
  lqtL_pushenum(L, ret, "QMessageBox::StandardButton");
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__metaObject (lua_State *L) {
  QMessageBox *& __lua__obj = *static_cast<QMessageBox**>(lqtL_checkudata(L, 1, "QMessageBox*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QMetaObject * ret = __lua__obj->QMessageBox::metaObject();
  lqtL_pushudata(L, ret, "QMetaObject*");
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__new__OverloadedVersion__1 (lua_State *L) {
  QWidget * arg1 = lqtL_testudata(L, 1, "QWidget*")?*static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*")):static_cast< QWidget * >(0);
  QMessageBox * ret = new LuaBinder< QMessageBox >(L, arg1);
  lqtL_passudata(L, ret, "QMessageBox*");
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__new__OverloadedVersion__2 (lua_State *L) {
  QMessageBox::Icon arg1 = static_cast<QMessageBox::Icon>(lqtL_toenum(L, 1, "QMessageBox::Icon"));
  const QString& arg2 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  const QString& arg3 = QString::fromAscii(lua_tostring(L, 3), lua_objlen(L, 3));
  QFlags<QMessageBox::StandardButton> arg4 = lqtL_testudata(L, 4, "QFlags<QMessageBox::StandardButton>*")?**static_cast<QFlags<QMessageBox::StandardButton>**>(lqtL_checkudata(L, 4, "QFlags<QMessageBox::StandardButton>*")):NoButton;
  QWidget * arg5 = lqtL_testudata(L, 5, "QWidget*")?*static_cast<QWidget**>(lqtL_checkudata(L, 5, "QWidget*")):static_cast< QWidget * >(0);
  QFlags<Qt::WindowType> arg6 = lqtL_testudata(L, 6, "QFlags<Qt::WindowType>*")?**static_cast<QFlags<Qt::WindowType>**>(lqtL_checkudata(L, 6, "QFlags<Qt::WindowType>*")):Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint;
  QMessageBox * ret = new LuaBinder< QMessageBox >(L, arg1, arg2, arg3, arg4, arg5, arg6);
  lqtL_passudata(L, ret, "QMessageBox*");
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__new__OverloadedVersion__3 (lua_State *L) {
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 1), lua_objlen(L, 1));
  const QString& arg2 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  QMessageBox::Icon arg3 = static_cast<QMessageBox::Icon>(lqtL_toenum(L, 3, "QMessageBox::Icon"));
  int arg4 = lua_tointeger(L, 4);
  int arg5 = lua_tointeger(L, 5);
  int arg6 = lua_tointeger(L, 6);
  QWidget * arg7 = lqtL_testudata(L, 7, "QWidget*")?*static_cast<QWidget**>(lqtL_checkudata(L, 7, "QWidget*")):static_cast< QWidget * >(0);
  QFlags<Qt::WindowType> arg8 = lqtL_testudata(L, 8, "QFlags<Qt::WindowType>*")?**static_cast<QFlags<Qt::WindowType>**>(lqtL_checkudata(L, 8, "QFlags<Qt::WindowType>*")):Qt::Dialog | Qt::MSWindowsFixedSizeDialogHint;
  QMessageBox * ret = new LuaBinder< QMessageBox >(L, arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
  lqtL_passudata(L, ret, "QMessageBox*");
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__new (lua_State *L) {
  int score[4];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if (lqtL_testudata(L, 1, "QWidget*")) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x132db10;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if (lqtL_isenum(L, 1, "QMessageBox::Icon")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x132e590;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x132e020;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 3)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x132eaf0;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_testudata(L, 4, "QFlags<QMessageBox::StandardButton>*")) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x132f380;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_testudata(L, 5, "QWidget*")) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x132f790;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_testudata(L, 6, "QFlags<Qt::WindowType>*")) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x132f2d0;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1348610;
  } else {
    score[3] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x13482e0;
  } else {
    score[3] -= premium*premium;
  }
  if (lqtL_isenum(L, 3, "QMessageBox::Icon")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1348b00;
  } else {
    score[3] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1349360;
  } else {
    score[3] -= premium*premium;
  }
  if (lua_isnumber(L, 5)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1349770;
  } else {
    score[3] -= premium*premium;
  }
  if (lua_isnumber(L, 6)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x13492b0;
  } else {
    score[3] -= premium*premium;
  }
  if (lqtL_testudata(L, 7, "QWidget*")) {
    score[3] += premium;
  } else if (true) {
    score[3] += premium-1; // table: 0x134a010;
  } else {
    score[3] -= premium*premium;
  }
  if (lqtL_testudata(L, 8, "QFlags<Qt::WindowType>*")) {
    score[3] += premium;
  } else if (true) {
    score[3] += premium-1; // table: 0x134a410;
  } else {
    score[3] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=3;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__new__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__new__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__new__OverloadedVersion__3(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__new matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__warning__OverloadedVersion__1 (lua_State *L) {
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
  const QString& arg2 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  const QString& arg3 = QString::fromAscii(lua_tostring(L, 3), lua_objlen(L, 3));
  QFlags<QMessageBox::StandardButton> arg4 = lqtL_testudata(L, 4, "QFlags<QMessageBox::StandardButton>*")?**static_cast<QFlags<QMessageBox::StandardButton>**>(lqtL_checkudata(L, 4, "QFlags<QMessageBox::StandardButton>*")):Ok;
  QMessageBox::StandardButton arg5 = lqtL_isenum(L, 5, "QMessageBox::StandardButton")?static_cast<QMessageBox::StandardButton>(lqtL_toenum(L, 5, "QMessageBox::StandardButton")):QMessageBox::NoButton;
  QMessageBox::StandardButton ret = QMessageBox::warning(arg1, arg2, arg3, arg4, arg5);
  lqtL_pushenum(L, ret, "QMessageBox::StandardButton");
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__warning__OverloadedVersion__2 (lua_State *L) {
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
  const QString& arg2 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  const QString& arg3 = QString::fromAscii(lua_tostring(L, 3), lua_objlen(L, 3));
  int arg4 = lua_tointeger(L, 4);
  int arg5 = lua_tointeger(L, 5);
  int arg6 = lua_isnumber(L, 6)?lua_tointeger(L, 6):static_cast< int >(0);
  int ret = QMessageBox::warning(arg1, arg2, arg3, arg4, arg5, arg6);
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__warning__OverloadedVersion__3 (lua_State *L) {
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
  const QString& arg2 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  const QString& arg3 = QString::fromAscii(lua_tostring(L, 3), lua_objlen(L, 3));
  const QString& arg4 = QString::fromAscii(lua_tostring(L, 4), lua_objlen(L, 4));
  const QString& arg5 = (lua_type(L, 5)==LUA_TSTRING)?QString::fromAscii(lua_tostring(L, 5), lua_objlen(L, 5)):QString();
  const QString& arg6 = (lua_type(L, 6)==LUA_TSTRING)?QString::fromAscii(lua_tostring(L, 6), lua_objlen(L, 6)):QString();
  int arg7 = lua_isnumber(L, 7)?lua_tointeger(L, 7):static_cast< int >(0);
  int arg8 = lua_isnumber(L, 8)?lua_tointeger(L, 8):static_cast< int >(-1);
  int ret = QMessageBox::warning(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__warning__OverloadedVersion__4 (lua_State *L) {
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
  const QString& arg2 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  const QString& arg3 = QString::fromAscii(lua_tostring(L, 3), lua_objlen(L, 3));
  QMessageBox::StandardButton arg4 = static_cast<QMessageBox::StandardButton>(lqtL_toenum(L, 4, "QMessageBox::StandardButton"));
  QMessageBox::StandardButton arg5 = static_cast<QMessageBox::StandardButton>(lqtL_toenum(L, 5, "QMessageBox::StandardButton"));
  int ret = QMessageBox::warning(arg1, arg2, arg3, arg4, arg5);
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__warning (lua_State *L) {
  int score[5];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if (lqtL_testudata(L, 1, "QWidget*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x13427d0;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1341a10;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 3)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1342cf0;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_testudata(L, 4, "QFlags<QMessageBox::StandardButton>*")) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x1343550;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_isenum(L, 5, "QMessageBox::StandardButton")) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x1343920;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if (lqtL_testudata(L, 1, "QWidget*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1356c20;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1355e70;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 3)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x13570d0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1357930;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 5)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1357ce0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 6)) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x1357880;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  if (lqtL_testudata(L, 1, "QWidget*")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1358a30;
  } else {
    score[3] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1358700;
  } else {
    score[3] -= premium*premium;
  }
  if ((lua_type(L, 3)==LUA_TSTRING)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1358ee0;
  } else {
    score[3] -= premium*premium;
  }
  if ((lua_type(L, 4)==LUA_TSTRING)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1359740;
  } else {
    score[3] -= premium*premium;
  }
  if ((lua_type(L, 5)==LUA_TSTRING)) {
    score[3] += premium;
  } else if (true) {
    score[3] += premium-1; // table: 0x1359b00;
  } else {
    score[3] -= premium*premium;
  }
  if ((lua_type(L, 6)==LUA_TSTRING)) {
    score[3] += premium;
  } else if (true) {
    score[3] += premium-1; // table: 0x1359690;
  } else {
    score[3] -= premium*premium;
  }
  if (lua_isnumber(L, 7)) {
    score[3] += premium;
  } else if (true) {
    score[3] += premium-1; // table: 0x135a2f0;
  } else {
    score[3] -= premium*premium;
  }
  if (lua_isnumber(L, 8)) {
    score[3] += premium;
  } else if (true) {
    score[3] += premium-1; // table: 0x135a6b0;
  } else {
    score[3] -= premium*premium;
  }
  score[4] = 0;
  if (lqtL_testudata(L, 1, "QWidget*")) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x135afe0;
  } else {
    score[4] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x135aca0;
  } else {
    score[4] -= premium*premium;
  }
  if ((lua_type(L, 3)==LUA_TSTRING)) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x135b4d0;
  } else {
    score[4] -= premium*premium;
  }
  if (lqtL_isenum(L, 4, "QMessageBox::StandardButton")) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x135bd30;
  } else {
    score[4] -= premium*premium;
  }
  if (lqtL_isenum(L, 5, "QMessageBox::StandardButton")) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x135c0e0;
  } else {
    score[4] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=4;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__warning__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__warning__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__warning__OverloadedVersion__3(L); break;
    case 4: return __LuaWrapCall__warning__OverloadedVersion__4(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__warning matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__setWindowTitle (lua_State *L) {
  QMessageBox *& __lua__obj = *static_cast<QMessageBox**>(lqtL_checkudata(L, 1, "QMessageBox*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QMessageBox::setWindowTitle(arg1);
  return 0;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__delete (lua_State *L) {
  QMessageBox *& __lua__obj = *static_cast<QMessageBox**>(lqtL_checkudata(L, 1, "QMessageBox*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  delete __lua__obj;
  __lua__obj = 0;
  return 0;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__escapeButton (lua_State *L) {
  QMessageBox *& __lua__obj = *static_cast<QMessageBox**>(lqtL_checkudata(L, 1, "QMessageBox*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QAbstractButton * ret = __lua__obj->QMessageBox::escapeButton();
  lqtL_pushudata(L, ret, "QAbstractButton*");
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__clickedButton (lua_State *L) {
  QMessageBox *& __lua__obj = *static_cast<QMessageBox**>(lqtL_checkudata(L, 1, "QMessageBox*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QAbstractButton * ret = __lua__obj->QMessageBox::clickedButton();
  lqtL_pushudata(L, ret, "QAbstractButton*");
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__detailedText (lua_State *L) {
  QMessageBox *& __lua__obj = *static_cast<QMessageBox**>(lqtL_checkudata(L, 1, "QMessageBox*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QMessageBox::detailedText();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__text (lua_State *L) {
  QMessageBox *& __lua__obj = *static_cast<QMessageBox**>(lqtL_checkudata(L, 1, "QMessageBox*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QMessageBox::text();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__setTextFormat (lua_State *L) {
  QMessageBox *& __lua__obj = *static_cast<QMessageBox**>(lqtL_checkudata(L, 1, "QMessageBox*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::TextFormat arg1 = static_cast<Qt::TextFormat>(lqtL_toenum(L, 2, "Qt::TextFormat"));
  __lua__obj->QMessageBox::setTextFormat(arg1);
  return 0;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QMessageBox::trUtf8(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QMessageBox::trUtf8(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__trUtf8 (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1329bd0;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x1329730;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x132b9c0;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x132b0f0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x132be50;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__trUtf8__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__trUtf8__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__trUtf8 matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__setButtonText (lua_State *L) {
  QMessageBox *& __lua__obj = *static_cast<QMessageBox**>(lqtL_checkudata(L, 1, "QMessageBox*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  const QString& arg2 = QString::fromAscii(lua_tostring(L, 3), lua_objlen(L, 3));
  __lua__obj->QMessageBox::setButtonText(arg1, arg2);
  return 0;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__question__OverloadedVersion__1 (lua_State *L) {
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
  const QString& arg2 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  const QString& arg3 = QString::fromAscii(lua_tostring(L, 3), lua_objlen(L, 3));
  QFlags<QMessageBox::StandardButton> arg4 = lqtL_testudata(L, 4, "QFlags<QMessageBox::StandardButton>*")?**static_cast<QFlags<QMessageBox::StandardButton>**>(lqtL_checkudata(L, 4, "QFlags<QMessageBox::StandardButton>*")):Ok;
  QMessageBox::StandardButton arg5 = lqtL_isenum(L, 5, "QMessageBox::StandardButton")?static_cast<QMessageBox::StandardButton>(lqtL_toenum(L, 5, "QMessageBox::StandardButton")):QMessageBox::NoButton;
  QMessageBox::StandardButton ret = QMessageBox::question(arg1, arg2, arg3, arg4, arg5);
  lqtL_pushenum(L, ret, "QMessageBox::StandardButton");
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__question__OverloadedVersion__2 (lua_State *L) {
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
  const QString& arg2 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  const QString& arg3 = QString::fromAscii(lua_tostring(L, 3), lua_objlen(L, 3));
  int arg4 = lua_tointeger(L, 4);
  int arg5 = lua_isnumber(L, 5)?lua_tointeger(L, 5):static_cast< int >(0);
  int arg6 = lua_isnumber(L, 6)?lua_tointeger(L, 6):static_cast< int >(0);
  int ret = QMessageBox::question(arg1, arg2, arg3, arg4, arg5, arg6);
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__question__OverloadedVersion__3 (lua_State *L) {
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
  const QString& arg2 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  const QString& arg3 = QString::fromAscii(lua_tostring(L, 3), lua_objlen(L, 3));
  const QString& arg4 = QString::fromAscii(lua_tostring(L, 4), lua_objlen(L, 4));
  const QString& arg5 = (lua_type(L, 5)==LUA_TSTRING)?QString::fromAscii(lua_tostring(L, 5), lua_objlen(L, 5)):QString();
  const QString& arg6 = (lua_type(L, 6)==LUA_TSTRING)?QString::fromAscii(lua_tostring(L, 6), lua_objlen(L, 6)):QString();
  int arg7 = lua_isnumber(L, 7)?lua_tointeger(L, 7):static_cast< int >(0);
  int arg8 = lua_isnumber(L, 8)?lua_tointeger(L, 8):static_cast< int >(-1);
  int ret = QMessageBox::question(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__question__OverloadedVersion__4 (lua_State *L) {
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
  const QString& arg2 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  const QString& arg3 = QString::fromAscii(lua_tostring(L, 3), lua_objlen(L, 3));
  QMessageBox::StandardButton arg4 = static_cast<QMessageBox::StandardButton>(lqtL_toenum(L, 4, "QMessageBox::StandardButton"));
  QMessageBox::StandardButton arg5 = static_cast<QMessageBox::StandardButton>(lqtL_toenum(L, 5, "QMessageBox::StandardButton"));
  int ret = QMessageBox::question(arg1, arg2, arg3, arg4, arg5);
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__question (lua_State *L) {
  int score[5];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if (lqtL_testudata(L, 1, "QWidget*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1340cf0;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x133fee0;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 3)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1341210;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_testudata(L, 4, "QFlags<QMessageBox::StandardButton>*")) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x1341a70;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_isenum(L, 5, "QMessageBox::StandardButton")) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x1341e40;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if (lqtL_testudata(L, 1, "QWidget*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1350de0;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1350060;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 3)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1351290;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1351af0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 5)) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x1351eb0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 6)) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x1351a40;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  if (lqtL_testudata(L, 1, "QWidget*")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1352c00;
  } else {
    score[3] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x13528d0;
  } else {
    score[3] -= premium*premium;
  }
  if ((lua_type(L, 3)==LUA_TSTRING)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x13530b0;
  } else {
    score[3] -= premium*premium;
  }
  if ((lua_type(L, 4)==LUA_TSTRING)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1353910;
  } else {
    score[3] -= premium*premium;
  }
  if ((lua_type(L, 5)==LUA_TSTRING)) {
    score[3] += premium;
  } else if (true) {
    score[3] += premium-1; // table: 0x1353cd0;
  } else {
    score[3] -= premium*premium;
  }
  if ((lua_type(L, 6)==LUA_TSTRING)) {
    score[3] += premium;
  } else if (true) {
    score[3] += premium-1; // table: 0x1353860;
  } else {
    score[3] -= premium*premium;
  }
  if (lua_isnumber(L, 7)) {
    score[3] += premium;
  } else if (true) {
    score[3] += premium-1; // table: 0x13544c0;
  } else {
    score[3] -= premium*premium;
  }
  if (lua_isnumber(L, 8)) {
    score[3] += premium;
  } else if (true) {
    score[3] += premium-1; // table: 0x1354880;
  } else {
    score[3] -= premium*premium;
  }
  score[4] = 0;
  if (lqtL_testudata(L, 1, "QWidget*")) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x13551b0;
  } else {
    score[4] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x1354e70;
  } else {
    score[4] -= premium*premium;
  }
  if ((lua_type(L, 3)==LUA_TSTRING)) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x13556a0;
  } else {
    score[4] -= premium*premium;
  }
  if (lqtL_isenum(L, 4, "QMessageBox::StandardButton")) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x1355a60;
  } else {
    score[4] -= premium*premium;
  }
  if (lqtL_isenum(L, 5, "QMessageBox::StandardButton")) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x13562c0;
  } else {
    score[4] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=4;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__question__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__question__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__question__OverloadedVersion__3(L); break;
    case 4: return __LuaWrapCall__question__OverloadedVersion__4(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__question matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__setInformativeText (lua_State *L) {
  QMessageBox *& __lua__obj = *static_cast<QMessageBox**>(lqtL_checkudata(L, 1, "QMessageBox*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QMessageBox::setInformativeText(arg1);
  return 0;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__setIconPixmap (lua_State *L) {
  QMessageBox *& __lua__obj = *static_cast<QMessageBox**>(lqtL_checkudata(L, 1, "QMessageBox*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPixmap& arg1 = **static_cast<QPixmap**>(lqtL_checkudata(L, 2, "QPixmap*"));
  __lua__obj->QMessageBox::setIconPixmap(arg1);
  return 0;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__information__OverloadedVersion__1 (lua_State *L) {
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
  const QString& arg2 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  const QString& arg3 = QString::fromAscii(lua_tostring(L, 3), lua_objlen(L, 3));
  QFlags<QMessageBox::StandardButton> arg4 = lqtL_testudata(L, 4, "QFlags<QMessageBox::StandardButton>*")?**static_cast<QFlags<QMessageBox::StandardButton>**>(lqtL_checkudata(L, 4, "QFlags<QMessageBox::StandardButton>*")):Ok;
  QMessageBox::StandardButton arg5 = lqtL_isenum(L, 5, "QMessageBox::StandardButton")?static_cast<QMessageBox::StandardButton>(lqtL_toenum(L, 5, "QMessageBox::StandardButton")):QMessageBox::NoButton;
  QMessageBox::StandardButton ret = QMessageBox::information(arg1, arg2, arg3, arg4, arg5);
  lqtL_pushenum(L, ret, "QMessageBox::StandardButton");
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__information__OverloadedVersion__2 (lua_State *L) {
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
  const QString& arg2 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  const QString& arg3 = QString::fromAscii(lua_tostring(L, 3), lua_objlen(L, 3));
  int arg4 = lua_tointeger(L, 4);
  int arg5 = lua_isnumber(L, 5)?lua_tointeger(L, 5):static_cast< int >(0);
  int arg6 = lua_isnumber(L, 6)?lua_tointeger(L, 6):static_cast< int >(0);
  int ret = QMessageBox::information(arg1, arg2, arg3, arg4, arg5, arg6);
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__information__OverloadedVersion__3 (lua_State *L) {
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
  const QString& arg2 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  const QString& arg3 = QString::fromAscii(lua_tostring(L, 3), lua_objlen(L, 3));
  const QString& arg4 = QString::fromAscii(lua_tostring(L, 4), lua_objlen(L, 4));
  const QString& arg5 = (lua_type(L, 5)==LUA_TSTRING)?QString::fromAscii(lua_tostring(L, 5), lua_objlen(L, 5)):QString();
  const QString& arg6 = (lua_type(L, 6)==LUA_TSTRING)?QString::fromAscii(lua_tostring(L, 6), lua_objlen(L, 6)):QString();
  int arg7 = lua_isnumber(L, 7)?lua_tointeger(L, 7):static_cast< int >(0);
  int arg8 = lua_isnumber(L, 8)?lua_tointeger(L, 8):static_cast< int >(-1);
  int ret = QMessageBox::information(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__information__OverloadedVersion__4 (lua_State *L) {
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
  const QString& arg2 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  const QString& arg3 = QString::fromAscii(lua_tostring(L, 3), lua_objlen(L, 3));
  QMessageBox::StandardButton arg4 = static_cast<QMessageBox::StandardButton>(lqtL_toenum(L, 4, "QMessageBox::StandardButton"));
  QMessageBox::StandardButton arg5 = lqtL_isenum(L, 5, "QMessageBox::StandardButton")?static_cast<QMessageBox::StandardButton>(lqtL_toenum(L, 5, "QMessageBox::StandardButton")):QMessageBox::NoButton;
  QMessageBox::StandardButton ret = QMessageBox::information(arg1, arg2, arg3, arg4, arg5);
  lqtL_pushenum(L, ret, "QMessageBox::StandardButton");
  return 1;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__information (lua_State *L) {
  int score[5];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if (lqtL_testudata(L, 1, "QWidget*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x133f1a0;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x133eba0;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 3)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x133f6e0;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_testudata(L, 4, "QFlags<QMessageBox::StandardButton>*")) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x133ff40;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_isenum(L, 5, "QMessageBox::StandardButton")) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x1340310;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if (lqtL_testudata(L, 1, "QWidget*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x134ad30;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x134aa00;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 3)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x134b1e0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x134ba40;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 5)) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x134be00;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 6)) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x134b990;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  if (lqtL_testudata(L, 1, "QWidget*")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x134cb50;
  } else {
    score[3] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x134c820;
  } else {
    score[3] -= premium*premium;
  }
  if ((lua_type(L, 3)==LUA_TSTRING)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x134d000;
  } else {
    score[3] -= premium*premium;
  }
  if ((lua_type(L, 4)==LUA_TSTRING)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x134d860;
  } else {
    score[3] -= premium*premium;
  }
  if ((lua_type(L, 5)==LUA_TSTRING)) {
    score[3] += premium;
  } else if (true) {
    score[3] += premium-1; // table: 0x134dc90;
  } else {
    score[3] -= premium*premium;
  }
  if ((lua_type(L, 6)==LUA_TSTRING)) {
    score[3] += premium;
  } else if (true) {
    score[3] += premium-1; // table: 0x134d7b0;
  } else {
    score[3] -= premium*premium;
  }
  if (lua_isnumber(L, 7)) {
    score[3] += premium;
  } else if (true) {
    score[3] += premium-1; // table: 0x134e560;
  } else {
    score[3] -= premium*premium;
  }
  if (lua_isnumber(L, 8)) {
    score[3] += premium;
  } else if (true) {
    score[3] += premium-1; // table: 0x134e9a0;
  } else {
    score[3] -= premium*premium;
  }
  score[4] = 0;
  if (lqtL_testudata(L, 1, "QWidget*")) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x134f370;
  } else {
    score[4] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x134edb0;
  } else {
    score[4] -= premium*premium;
  }
  if ((lua_type(L, 3)==LUA_TSTRING)) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x134f860;
  } else {
    score[4] -= premium*premium;
  }
  if (lqtL_isenum(L, 4, "QMessageBox::StandardButton")) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x13500c0;
  } else {
    score[4] -= premium*premium;
  }
  if (lqtL_isenum(L, 5, "QMessageBox::StandardButton")) {
    score[4] += premium;
  } else if (true) {
    score[4] += premium-1; // table: 0x1350480;
  } else {
    score[4] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=4;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__information__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__information__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__information__OverloadedVersion__3(L); break;
    case 4: return __LuaWrapCall__information__OverloadedVersion__4(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__information matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QMessageBox >::__LuaWrapCall__icon (lua_State *L) {
  QMessageBox *& __lua__obj = *static_cast<QMessageBox**>(lqtL_checkudata(L, 1, "QMessageBox*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QMessageBox::Icon ret = __lua__obj->QMessageBox::icon();
  lqtL_pushenum(L, ret, "QMessageBox::Icon");
  return 1;
}
void LuaBinder< QMessageBox >::styleChange (QStyle& arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "styleChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QStyle*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::styleChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMessageBox >::focusInEvent (QFocusEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "focusInEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QFocusEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::focusInEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMessageBox >::keyPressEvent (QKeyEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "keyPressEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QKeyEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QMessageBox::keyPressEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMessageBox >::contextMenuEvent (QContextMenuEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "contextMenuEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QContextMenuEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QDialog::contextMenuEvent(arg1);
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QMessageBox >::devType () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "devType");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::devType();
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QMessageBox >::setVisible (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "setVisible");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QDialog::setVisible(arg1);
  }
  lua_settop(L, oldtop);
}
QVariant LuaBinder< QMessageBox >::inputMethodQuery (Qt::InputMethodQuery arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "inputMethodQuery");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushenum(L, arg1, "Qt::InputMethodQuery");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::inputMethodQuery(arg1);
  }
  QVariant ret = **static_cast<QVariant**>(lqtL_checkudata(L, -1, "QVariant*"));
  lua_settop(L, oldtop);
  return ret;
}
QPaintEngine * LuaBinder< QMessageBox >::paintEngine () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "paintEngine");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::paintEngine();
  }
  QPaintEngine * ret = *static_cast<QPaintEngine**>(lqtL_checkudata(L, -1, "QPaintEngine*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QMessageBox >::dragLeaveEvent (QDragLeaveEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dragLeaveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDragLeaveEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::dragLeaveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMessageBox >::mousePressEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mousePressEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::mousePressEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMessageBox >::dragEnterEvent (QDragEnterEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dragEnterEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDragEnterEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::dragEnterEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMessageBox >::closeEvent (QCloseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "closeEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QCloseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QMessageBox::closeEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMessageBox >::reject () {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "reject");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QDialog::reject();
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMessageBox >::accept () {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "accept");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QDialog::accept();
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMessageBox >::tabletEvent (QTabletEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "tabletEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QTabletEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::tabletEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMessageBox >::customEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "customEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::customEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMessageBox >::moveEvent (QMoveEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "moveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMoveEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::moveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMessageBox >::keyReleaseEvent (QKeyEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "keyReleaseEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QKeyEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::keyReleaseEvent(arg1);
  }
  lua_settop(L, oldtop);
}
QSize LuaBinder< QMessageBox >::minimumSizeHint () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "minimumSizeHint");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QDialog::minimumSizeHint();
  }
  QSize ret = **static_cast<QSize**>(lqtL_checkudata(L, -1, "QSize*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QMessageBox >::dropEvent (QDropEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dropEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDropEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::dropEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMessageBox >::windowActivationChange (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "windowActivationChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::windowActivationChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMessageBox >::showEvent (QShowEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "showEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QShowEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QMessageBox::showEvent(arg1);
  }
  lua_settop(L, oldtop);
}
const QMetaObject * LuaBinder< QMessageBox >::metaObject () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "metaObject");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QMessageBox::metaObject();
  }
  const QMetaObject * ret = *static_cast<QMetaObject**>(lqtL_checkudata(L, -1, "QMetaObject*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QMessageBox >::paintEvent (QPaintEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "paintEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QPaintEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::paintEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMessageBox >::done (int arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "done");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushinteger(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QDialog::done(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMessageBox >::mouseDoubleClickEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mouseDoubleClickEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::mouseDoubleClickEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMessageBox >::enabledChange (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "enabledChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::enabledChange(arg1);
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QMessageBox >::focusNextPrevChild (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "focusNextPrevChild");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::focusNextPrevChild(arg1);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
int LuaBinder< QMessageBox >::heightForWidth (int arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "heightForWidth");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushinteger(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::heightForWidth(arg1);
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QMessageBox >::fontChange (const QFont& arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "fontChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QFont*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::fontChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMessageBox >::timerEvent (QTimerEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "timerEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QTimerEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::timerEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMessageBox >::disconnectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "disconnectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::disconnectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMessageBox >::mouseMoveEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mouseMoveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::mouseMoveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMessageBox >::focusOutEvent (QFocusEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "focusOutEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QFocusEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::focusOutEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMessageBox >::childEvent (QChildEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "childEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QChildEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::childEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMessageBox >::connectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "connectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::connectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMessageBox >::mouseReleaseEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mouseReleaseEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::mouseReleaseEvent(arg1);
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QMessageBox >::metric (QPaintDevice::PaintDeviceMetric arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "metric");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushenum(L, arg1, "QPaintDevice::PaintDeviceMetric");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::metric(arg1);
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
bool LuaBinder< QMessageBox >::eventFilter (QObject * arg1, QEvent * arg2) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "eventFilter");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QObject*");
  lqtL_pushudata(L, arg2, "QEvent*");
  if (lua_isfunction(L, -2-2)) {
    lua_pcall(L, 2+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QDialog::eventFilter(arg1, arg2);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QMessageBox >::inputMethodEvent (QInputMethodEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "inputMethodEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QInputMethodEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::inputMethodEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMessageBox >::wheelEvent (QWheelEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "wheelEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QWheelEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::wheelEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMessageBox >::actionEvent (QActionEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "actionEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QActionEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::actionEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMessageBox >::paletteChange (const QPalette& arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "paletteChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QPalette*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::paletteChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMessageBox >::languageChange () {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "languageChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::languageChange();
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QMessageBox >::event (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "event");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QMessageBox::event(arg1);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QMessageBox >::hideEvent (QHideEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "hideEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QHideEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::hideEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMessageBox >::dragMoveEvent (QDragMoveEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dragMoveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDragMoveEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::dragMoveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
QSize LuaBinder< QMessageBox >::sizeHint () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "sizeHint");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QMessageBox::sizeHint();
  }
  QSize ret = **static_cast<QSize**>(lqtL_checkudata(L, -1, "QSize*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QMessageBox >::resizeEvent (QResizeEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "resizeEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QResizeEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QMessageBox::resizeEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMessageBox >::leaveEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "leaveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::leaveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMessageBox >::changeEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "changeEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QMessageBox::changeEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMessageBox >::enterEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "enterEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::enterEvent(arg1);
  }
  lua_settop(L, oldtop);
}
LuaBinder< QMessageBox >::  ~LuaBinder< QMessageBox > () {
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMessageBox*");
  lua_getfield(L, -1, "~QMessageBox");

  if (lua_isfunction(L, -1)) {
    lua_insert(L, -2);
    lua_pcall(L, 1, 1, 0);
  } else {
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QMessageBox >::lqt_pushenum_Icon (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "NoIcon");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "NoIcon");
  lua_pushstring(L, "Information");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "Information");
  lua_pushstring(L, "Warning");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "Warning");
  lua_pushstring(L, "Critical");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "Critical");
  lua_pushstring(L, "Question");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "Question");
  lua_pushcfunction(L, LuaBinder< QMessageBox >::lqt_pushenum_Icon_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QMessageBox::Icon");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QMessageBox >::lqt_pushenum_Icon_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QMessageBox::Icon>*) + sizeof(QFlags<QMessageBox::Icon>));
  QFlags<QMessageBox::Icon> *fl = static_cast<QFlags<QMessageBox::Icon>*>( static_cast<void*>(&static_cast<QFlags<QMessageBox::Icon>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QMessageBox::Icon>(lqtL_toenum(L, i, "QMessageBox::Icon"));
	}
	if (luaL_newmetatable(L, "QFlags<QMessageBox::Icon>*")) {
		lua_pushstring(L, "QFlags<QMessageBox::Icon>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinder< QMessageBox >::lqt_pushenum_ButtonRole (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "InvalidRole");
  lua_rawseti(L, enum_table, -1);
  lua_pushinteger(L, -1);
  lua_setfield(L, enum_table, "InvalidRole");
  lua_pushstring(L, "AcceptRole");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "AcceptRole");
  lua_pushstring(L, "RejectRole");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "RejectRole");
  lua_pushstring(L, "DestructiveRole");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "DestructiveRole");
  lua_pushstring(L, "ActionRole");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "ActionRole");
  lua_pushstring(L, "HelpRole");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "HelpRole");
  lua_pushstring(L, "YesRole");
  lua_rawseti(L, enum_table, 5);
  lua_pushinteger(L, 5);
  lua_setfield(L, enum_table, "YesRole");
  lua_pushstring(L, "NoRole");
  lua_rawseti(L, enum_table, 6);
  lua_pushinteger(L, 6);
  lua_setfield(L, enum_table, "NoRole");
  lua_pushstring(L, "ResetRole");
  lua_rawseti(L, enum_table, 7);
  lua_pushinteger(L, 7);
  lua_setfield(L, enum_table, "ResetRole");
  lua_pushstring(L, "ApplyRole");
  lua_rawseti(L, enum_table, 8);
  lua_pushinteger(L, 8);
  lua_setfield(L, enum_table, "ApplyRole");
  lua_pushstring(L, "NRoles");
  lua_rawseti(L, enum_table, 9);
  lua_pushinteger(L, 9);
  lua_setfield(L, enum_table, "NRoles");
  lua_pushcfunction(L, LuaBinder< QMessageBox >::lqt_pushenum_ButtonRole_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QMessageBox::ButtonRole");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QMessageBox >::lqt_pushenum_ButtonRole_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QMessageBox::ButtonRole>*) + sizeof(QFlags<QMessageBox::ButtonRole>));
  QFlags<QMessageBox::ButtonRole> *fl = static_cast<QFlags<QMessageBox::ButtonRole>*>( static_cast<void*>(&static_cast<QFlags<QMessageBox::ButtonRole>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QMessageBox::ButtonRole>(lqtL_toenum(L, i, "QMessageBox::ButtonRole"));
	}
	if (luaL_newmetatable(L, "QFlags<QMessageBox::ButtonRole>*")) {
		lua_pushstring(L, "QFlags<QMessageBox::ButtonRole>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinder< QMessageBox >::lqt_pushenum_StandardButton (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "NoButton");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "NoButton");
  lua_pushstring(L, "Ok");
  lua_rawseti(L, enum_table, 1024);
  lua_pushinteger(L, 1024);
  lua_setfield(L, enum_table, "Ok");
  lua_pushstring(L, "Save");
  lua_rawseti(L, enum_table, 2048);
  lua_pushinteger(L, 2048);
  lua_setfield(L, enum_table, "Save");
  lua_pushstring(L, "SaveAll");
  lua_rawseti(L, enum_table, 4096);
  lua_pushinteger(L, 4096);
  lua_setfield(L, enum_table, "SaveAll");
  lua_pushstring(L, "Open");
  lua_rawseti(L, enum_table, 8192);
  lua_pushinteger(L, 8192);
  lua_setfield(L, enum_table, "Open");
  lua_pushstring(L, "Yes");
  lua_rawseti(L, enum_table, 16384);
  lua_pushinteger(L, 16384);
  lua_setfield(L, enum_table, "Yes");
  lua_pushstring(L, "YesToAll");
  lua_rawseti(L, enum_table, 32768);
  lua_pushinteger(L, 32768);
  lua_setfield(L, enum_table, "YesToAll");
  lua_pushstring(L, "No");
  lua_rawseti(L, enum_table, 65536);
  lua_pushinteger(L, 65536);
  lua_setfield(L, enum_table, "No");
  lua_pushstring(L, "NoToAll");
  lua_rawseti(L, enum_table, 131072);
  lua_pushinteger(L, 131072);
  lua_setfield(L, enum_table, "NoToAll");
  lua_pushstring(L, "Abort");
  lua_rawseti(L, enum_table, 262144);
  lua_pushinteger(L, 262144);
  lua_setfield(L, enum_table, "Abort");
  lua_pushstring(L, "Retry");
  lua_rawseti(L, enum_table, 524288);
  lua_pushinteger(L, 524288);
  lua_setfield(L, enum_table, "Retry");
  lua_pushstring(L, "Ignore");
  lua_rawseti(L, enum_table, 1048576);
  lua_pushinteger(L, 1048576);
  lua_setfield(L, enum_table, "Ignore");
  lua_pushstring(L, "Close");
  lua_rawseti(L, enum_table, 2097152);
  lua_pushinteger(L, 2097152);
  lua_setfield(L, enum_table, "Close");
  lua_pushstring(L, "Cancel");
  lua_rawseti(L, enum_table, 4194304);
  lua_pushinteger(L, 4194304);
  lua_setfield(L, enum_table, "Cancel");
  lua_pushstring(L, "Discard");
  lua_rawseti(L, enum_table, 8388608);
  lua_pushinteger(L, 8388608);
  lua_setfield(L, enum_table, "Discard");
  lua_pushstring(L, "Help");
  lua_rawseti(L, enum_table, 16777216);
  lua_pushinteger(L, 16777216);
  lua_setfield(L, enum_table, "Help");
  lua_pushstring(L, "Apply");
  lua_rawseti(L, enum_table, 33554432);
  lua_pushinteger(L, 33554432);
  lua_setfield(L, enum_table, "Apply");
  lua_pushstring(L, "Reset");
  lua_rawseti(L, enum_table, 67108864);
  lua_pushinteger(L, 67108864);
  lua_setfield(L, enum_table, "Reset");
  lua_pushstring(L, "RestoreDefaults");
  lua_rawseti(L, enum_table, 134217728);
  lua_pushinteger(L, 134217728);
  lua_setfield(L, enum_table, "RestoreDefaults");
  lua_pushstring(L, "FirstButton");
  lua_rawseti(L, enum_table, 1024);
  lua_pushinteger(L, 1024);
  lua_setfield(L, enum_table, "FirstButton");
  lua_pushstring(L, "LastButton");
  lua_rawseti(L, enum_table, 134217728);
  lua_pushinteger(L, 134217728);
  lua_setfield(L, enum_table, "LastButton");
  lua_pushstring(L, "YesAll");
  lua_rawseti(L, enum_table, 32768);
  lua_pushinteger(L, 32768);
  lua_setfield(L, enum_table, "YesAll");
  lua_pushstring(L, "NoAll");
  lua_rawseti(L, enum_table, 131072);
  lua_pushinteger(L, 131072);
  lua_setfield(L, enum_table, "NoAll");
  lua_pushstring(L, "Default");
  lua_rawseti(L, enum_table, 256);
  lua_pushinteger(L, 256);
  lua_setfield(L, enum_table, "Default");
  lua_pushstring(L, "Escape");
  lua_rawseti(L, enum_table, 512);
  lua_pushinteger(L, 512);
  lua_setfield(L, enum_table, "Escape");
  lua_pushstring(L, "FlagMask");
  lua_rawseti(L, enum_table, 768);
  lua_pushinteger(L, 768);
  lua_setfield(L, enum_table, "FlagMask");
  lua_pushstring(L, "ButtonMask");
  lua_rawseti(L, enum_table, -769);
  lua_pushinteger(L, -769);
  lua_setfield(L, enum_table, "ButtonMask");
  lua_pushcfunction(L, LuaBinder< QMessageBox >::lqt_pushenum_StandardButton_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QMessageBox::StandardButton");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QMessageBox >::lqt_pushenum_StandardButton_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QMessageBox::StandardButton>*) + sizeof(QFlags<QMessageBox::StandardButton>));
  QFlags<QMessageBox::StandardButton> *fl = static_cast<QFlags<QMessageBox::StandardButton>*>( static_cast<void*>(&static_cast<QFlags<QMessageBox::StandardButton>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QMessageBox::StandardButton>(lqtL_toenum(L, i, "QMessageBox::StandardButton"));
	}
	if (luaL_newmetatable(L, "QFlags<QMessageBox::StandardButton>*")) {
		lua_pushstring(L, "QFlags<QMessageBox::StandardButton>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int luaopen_QMessageBox (lua_State *L) {
  if (luaL_newmetatable(L, "QMessageBox*")) {
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__standardButtons);
    lua_setfield(L, -2, "standardButtons");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__setWindowModality);
    lua_setfield(L, -2, "setWindowModality");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__sizeHint);
    lua_setfield(L, -2, "sizeHint");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__defaultButton);
    lua_setfield(L, -2, "defaultButton");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__setDefaultButton);
    lua_setfield(L, -2, "setDefaultButton");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__setIcon);
    lua_setfield(L, -2, "setIcon");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__setStandardButtons);
    lua_setfield(L, -2, "setStandardButtons");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__button);
    lua_setfield(L, -2, "button");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__standardIcon);
    lua_setfield(L, -2, "standardIcon");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__tr);
    lua_setfield(L, -2, "tr");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__textFormat);
    lua_setfield(L, -2, "textFormat");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__informativeText);
    lua_setfield(L, -2, "informativeText");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__removeButton);
    lua_setfield(L, -2, "removeButton");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__buttonText);
    lua_setfield(L, -2, "buttonText");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__critical);
    lua_setfield(L, -2, "critical");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__addButton);
    lua_setfield(L, -2, "addButton");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__setDetailedText);
    lua_setfield(L, -2, "setDetailedText");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__about);
    lua_setfield(L, -2, "about");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__setText);
    lua_setfield(L, -2, "setText");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__aboutQt);
    lua_setfield(L, -2, "aboutQt");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__iconPixmap);
    lua_setfield(L, -2, "iconPixmap");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__setEscapeButton);
    lua_setfield(L, -2, "setEscapeButton");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__standardButton);
    lua_setfield(L, -2, "standardButton");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__metaObject);
    lua_setfield(L, -2, "metaObject");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__new);
    lua_setfield(L, -2, "new");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__warning);
    lua_setfield(L, -2, "warning");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__setWindowTitle);
    lua_setfield(L, -2, "setWindowTitle");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__delete);
    lua_setfield(L, -2, "delete");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__escapeButton);
    lua_setfield(L, -2, "escapeButton");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__clickedButton);
    lua_setfield(L, -2, "clickedButton");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__detailedText);
    lua_setfield(L, -2, "detailedText");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__text);
    lua_setfield(L, -2, "text");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__setTextFormat);
    lua_setfield(L, -2, "setTextFormat");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__trUtf8);
    lua_setfield(L, -2, "trUtf8");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__setButtonText);
    lua_setfield(L, -2, "setButtonText");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__question);
    lua_setfield(L, -2, "question");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__setInformativeText);
    lua_setfield(L, -2, "setInformativeText");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__setIconPixmap);
    lua_setfield(L, -2, "setIconPixmap");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__information);
    lua_setfield(L, -2, "information");
    lua_pushcfunction(L, LuaBinder< QMessageBox >::__LuaWrapCall__icon);
    lua_setfield(L, -2, "icon");
    LuaBinder< QMessageBox >::lqt_pushenum_Icon(L);
    lua_setfield(L, -2, "Icon");
    LuaBinder< QMessageBox >::lqt_pushenum_ButtonRole(L);
    lua_setfield(L, -2, "ButtonRole");
    LuaBinder< QMessageBox >::lqt_pushenum_StandardButton(L);
    lua_setfield(L, -2, "StandardButton");
    lua_newtable(L);
    lua_pushboolean(L, 1);
    lua_setfield(L, -2, "QDialog*");
    lua_pushboolean(L, 0);
    lua_setfield(L, -2, "QObject*");
    lua_pushboolean(L, 0);
    lua_setfield(L, -2, "QPaintDevice*");
    lua_pushboolean(L, 0);
    lua_setfield(L, -2, "QWidget*");
    lua_setfield(L, -2, "__base");
    lua_pushcfunction(L, lqtL_newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, lqtL_index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, lqtL_gc);
    lua_setfield(L, -2, "__gc");
    lua_pushstring(L, "QMessageBox");
    lua_setfield(L, -2, "__qtype");
		lua_setglobal(L, "QMessageBox");
  } else {
		lua_pop(L, 1);
	}
  return 0;
}
