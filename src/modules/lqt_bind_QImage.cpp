#include "lqt_bind_QImage.hpp"

int LuaBinder< QImage >::__LuaWrapCall__trueMatrix__OverloadedVersion__1 (lua_State *L) {
  const QMatrix& arg1 = **static_cast<QMatrix**>(lqtL_checkudata(L, 1, "QMatrix*"));
  int arg2 = lua_tointeger(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QMatrix ret = QImage::trueMatrix(arg1, arg2, arg3);
  lqtL_passudata(L, new QMatrix(ret), "QMatrix*");
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__trueMatrix__OverloadedVersion__2 (lua_State *L) {
  const QTransform& arg1 = **static_cast<QTransform**>(lqtL_checkudata(L, 1, "QTransform*"));
  int arg2 = lua_tointeger(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QTransform ret = QImage::trueMatrix(arg1, arg2, arg3);
  lqtL_passudata(L, new QTransform(ret), "QTransform*");
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__trueMatrix (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if (lqtL_testudata(L, 1, "QMatrix*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1ce5570;
  } else {
    score[1] -= premium*premium;
  }
  if (lua_isnumber(L, 2)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1ce5b40;
  } else {
    score[1] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1ce5d90;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if (lqtL_testudata(L, 1, "QTransform*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1ce75a0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 2)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1ce7b70;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1ce7dc0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__trueMatrix__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__trueMatrix__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__trueMatrix matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QImage >::__LuaWrapCall__delete (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  delete __lua__obj;
  __lua__obj = 0;
  return 0;
}
int LuaBinder< QImage >::__LuaWrapCall__textList (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QList<QImageTextKeyLang> ret = __lua__obj->QImage::textList();
  lqtL_passudata(L, new QList<QImageTextKeyLang>(ret), "QList<QImageTextKeyLang>*");
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__allGray (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QImage::allGray();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__save__OverloadedVersion__1 (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  const char * arg2 = (lua_type(L, 3)==LUA_TSTRING)?lua_tostring(L, 3):static_cast< const char * >(0);
  int arg3 = lua_isnumber(L, 4)?lua_tointeger(L, 4):static_cast< int >(-1);
  bool ret = __lua__obj->QImage::save(arg1, arg2, arg3);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__save__OverloadedVersion__2 (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QIODevice * arg1 = *static_cast<QIODevice**>(lqtL_checkudata(L, 2, "QIODevice*"));
  const char * arg2 = (lua_type(L, 3)==LUA_TSTRING)?lua_tostring(L, 3):static_cast< const char * >(0);
  int arg3 = lua_isnumber(L, 4)?lua_tointeger(L, 4):static_cast< int >(-1);
  bool ret = __lua__obj->QImage::save(arg1, arg2, arg3);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__save (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QImage*")?premium:-premium*premium;
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1cee6d0;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 3)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x1cee190;
  } else {
    score[1] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x1ceeba0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QImage*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QIODevice*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1cef980;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 3)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x1ceef70;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x1cefe40;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__save__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__save__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__save matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QImage >::__LuaWrapCall__new__OverloadedVersion__1 (lua_State *L) {
  QImage * ret = new LuaBinder< QImage >(L);
  lqtL_passudata(L, ret, "QImage*");
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__new__OverloadedVersion__2 (lua_State *L) {
  const QSize& arg1 = **static_cast<QSize**>(lqtL_checkudata(L, 1, "QSize*"));
  QImage::Format arg2 = static_cast<QImage::Format>(lqtL_toenum(L, 2, "QImage::Format"));
  QImage * ret = new LuaBinder< QImage >(L, arg1, arg2);
  lqtL_passudata(L, ret, "QImage*");
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__new__OverloadedVersion__3 (lua_State *L) {
  int arg1 = lua_tointeger(L, 1);
  int arg2 = lua_tointeger(L, 2);
  QImage::Format arg3 = static_cast<QImage::Format>(lqtL_toenum(L, 3, "QImage::Format"));
  QImage * ret = new LuaBinder< QImage >(L, arg1, arg2, arg3);
  lqtL_passudata(L, ret, "QImage*");
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__new__OverloadedVersion__8 (lua_State *L) {
  const char * const * arg1 = static_cast<const char * const *>(lua_touserdata(L, 1));
  QImage * ret = new LuaBinder< QImage >(L, arg1);
  lqtL_passudata(L, ret, "QImage*");
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__new__OverloadedVersion__9 (lua_State *L) {
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 1), lua_objlen(L, 1));
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QImage * ret = new LuaBinder< QImage >(L, arg1, arg2);
  lqtL_passudata(L, ret, "QImage*");
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__new__OverloadedVersion__10 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QImage * ret = new LuaBinder< QImage >(L, arg1, arg2);
  lqtL_passudata(L, ret, "QImage*");
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__new__OverloadedVersion__11 (lua_State *L) {
  const QImage& arg1 = **static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
  QImage * ret = new LuaBinder< QImage >(L, arg1);
  lqtL_passudata(L, ret, "QImage*");
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__new (lua_State *L) {
  int score[12];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[2] = 0;
  if (lqtL_testudata(L, 1, "QSize*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1cb6b60;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_isenum(L, 2, "QImage::Format")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1cb6590;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  if (lua_isnumber(L, 1)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1cb79e0;
  } else {
    score[3] -= premium*premium;
  }
  if (lua_isnumber(L, 2)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1cb7ef0;
  } else {
    score[3] -= premium*premium;
  }
  if (lqtL_isenum(L, 3, "QImage::Format")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1cb7e90;
  } else {
    score[3] -= premium*premium;
  }
  score[8] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[8] += premium;
  } else if (false) {
    score[8] += premium-1; // table: 0x1cbec20;
  } else {
    score[8] -= premium*premium;
  }
  score[9] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[9] += premium;
  } else if (false) {
    score[9] += premium-1; // table: 0x1cbf740;
  } else {
    score[9] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[9] += premium;
  } else if (true) {
    score[9] += premium-1; // table: 0x1cbf1e0;
  } else {
    score[9] -= premium*premium;
  }
  score[10] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[10] += premium;
  } else if (false) {
    score[10] += premium-1; // table: 0x1cc0560;
  } else {
    score[10] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[10] += premium;
  } else if (true) {
    score[10] += premium-1; // table: 0x1cc0020;
  } else {
    score[10] -= premium*premium;
  }
  score[11] = 0;
  if (lqtL_testudata(L, 1, "QImage*")) {
    score[11] += premium;
  } else if (false) {
    score[11] += premium-1; // table: 0x1cc1380;
  } else {
    score[11] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=11;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__new__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__new__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__new__OverloadedVersion__3(L); break;
    case 8: return __LuaWrapCall__new__OverloadedVersion__8(L); break;
    case 9: return __LuaWrapCall__new__OverloadedVersion__9(L); break;
    case 10: return __LuaWrapCall__new__OverloadedVersion__10(L); break;
    case 11: return __LuaWrapCall__new__OverloadedVersion__11(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__new matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QImage >::__LuaWrapCall__createMaskFromColor (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  unsigned int arg1 = lua_tointeger(L, 2);
  Qt::MaskMode arg2 = lqtL_isenum(L, 3, "Qt::MaskMode")?static_cast<Qt::MaskMode>(lqtL_toenum(L, 3, "Qt::MaskMode")):Qt::MaskInColor;
  QImage ret = __lua__obj->QImage::createMaskFromColor(arg1, arg2);
  lqtL_passudata(L, new QImage(ret), "QImage*");
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__depth (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QImage::depth();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__copy__OverloadedVersion__1 (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QRect& arg1 = lqtL_testudata(L, 2, "QRect*")?**static_cast<QRect**>(lqtL_checkudata(L, 2, "QRect*")):QRect();
  QImage ret = __lua__obj->QImage::copy(arg1);
  lqtL_passudata(L, new QImage(ret), "QImage*");
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__copy__OverloadedVersion__2 (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int arg2 = lua_tointeger(L, 3);
  int arg3 = lua_tointeger(L, 4);
  int arg4 = lua_tointeger(L, 5);
  QImage ret = __lua__obj->QImage::copy(arg1, arg2, arg3, arg4);
  lqtL_passudata(L, new QImage(ret), "QImage*");
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__copy (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QImage*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QRect*")) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x1cc61e0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QImage*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1cc6bf0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1cc7100;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1cc74b0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 5)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1cc7450;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__copy__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__copy__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__copy matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QImage >::__LuaWrapCall__devType (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QImage::devType();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__textKeys (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QStringList ret = __lua__obj->QImage::textKeys();
  lqtL_passudata(L, new QStringList(ret), "QStringList*");
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__isDetached (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QImage::isDetached();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__format (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QImage::Format ret = __lua__obj->QImage::format();
  lqtL_pushenum(L, ret, "QImage::Format");
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__scaledToWidth (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  Qt::TransformationMode arg2 = lqtL_isenum(L, 3, "Qt::TransformationMode")?static_cast<Qt::TransformationMode>(lqtL_toenum(L, 3, "Qt::TransformationMode")):Qt::FastTransformation;
  QImage ret = __lua__obj->QImage::scaledToWidth(arg1, arg2);
  lqtL_passudata(L, new QImage(ret), "QImage*");
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__size (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QSize ret = __lua__obj->QImage::size();
  lqtL_passudata(L, new QSize(ret), "QSize*");
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__valid__OverloadedVersion__1 (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int arg2 = lua_tointeger(L, 3);
  bool ret = __lua__obj->QImage::valid(arg1, arg2);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__valid__OverloadedVersion__2 (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPoint& arg1 = **static_cast<QPoint**>(lqtL_checkudata(L, 2, "QPoint*"));
  bool ret = __lua__obj->QImage::valid(arg1);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__valid (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QImage*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1cd34b0;
  } else {
    score[1] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1cd39d0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QImage*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPoint*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1cd4370;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__valid__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__valid__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__valid matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QImage >::__LuaWrapCall__convertToFormat__OverloadedVersion__1 (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QImage::Format arg1 = static_cast<QImage::Format>(lqtL_toenum(L, 2, "QImage::Format"));
  QFlags<Qt::ImageConversionFlag> arg2 = lqtL_testudata(L, 3, "QFlags<Qt::ImageConversionFlag>*")?**static_cast<QFlags<Qt::ImageConversionFlag>**>(lqtL_checkudata(L, 3, "QFlags<Qt::ImageConversionFlag>*")):Qt::AutoColor;
  QImage ret = __lua__obj->QImage::convertToFormat(arg1, arg2);
  lqtL_passudata(L, new QImage(ret), "QImage*");
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__convertToFormat__OverloadedVersion__2 (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QImage::Format arg1 = static_cast<QImage::Format>(lqtL_toenum(L, 2, "QImage::Format"));
  const QVector<unsigned int>& arg2 = **static_cast<QVector<unsigned int>**>(lqtL_checkudata(L, 3, "QVector<unsigned int>*"));
  QFlags<Qt::ImageConversionFlag> arg3 = lqtL_testudata(L, 4, "QFlags<Qt::ImageConversionFlag>*")?**static_cast<QFlags<Qt::ImageConversionFlag>**>(lqtL_checkudata(L, 4, "QFlags<Qt::ImageConversionFlag>*")):Qt::AutoColor;
  QImage ret = __lua__obj->QImage::convertToFormat(arg1, arg2, arg3);
  lqtL_passudata(L, new QImage(ret), "QImage*");
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__convertToFormat (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QImage*")?premium:-premium*premium;
  if (lqtL_isenum(L, 2, "QImage::Format")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1cc88c0;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_testudata(L, 3, "QFlags<Qt::ImageConversionFlag>*")) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x1cc8560;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QImage*")?premium:-premium*premium;
  if (lqtL_isenum(L, 2, "QImage::Format")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1cc98c0;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_testudata(L, 3, "QVector<unsigned int>*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1cc9570;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_testudata(L, 4, "QFlags<Qt::ImageConversionFlag>*")) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x1cc9dc0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__convertToFormat__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__convertToFormat__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__convertToFormat matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QImage >::__LuaWrapCall__serialNumber (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QImage::serialNumber();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__setColorTable (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QVector<unsigned int> arg1 = **static_cast<QVector<unsigned int>**>(lqtL_checkudata(L, 2, "QVector<unsigned int>*"));
  __lua__obj->QImage::setColorTable(arg1);
  return 0;
}
int LuaBinder< QImage >::__LuaWrapCall__bytesPerLine (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QImage::bytesPerLine();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__text__OverloadedVersion__1 (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = (lua_type(L, 2)==LUA_TSTRING)?QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2)):QString();
  QString ret = __lua__obj->QImage::text(arg1);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__text__OverloadedVersion__2 (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const char * arg1 = lua_tostring(L, 2);
  const char * arg2 = (lua_type(L, 3)==LUA_TSTRING)?lua_tostring(L, 3):static_cast< const char * >(0);
  QString ret = __lua__obj->QImage::text(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__text__OverloadedVersion__3 (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QImageTextKeyLang& arg1 = **static_cast<QImageTextKeyLang**>(lqtL_checkudata(L, 2, "QImageTextKeyLang*"));
  QString ret = __lua__obj->QImage::text(arg1);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__text (lua_State *L) {
  int score[4];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QImage*")?premium:-premium*premium;
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x1cf7c30;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QImage*")?premium:-premium*premium;
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1cf9490;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 3)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x1cf8f50;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  score[3] += lqtL_testudata(L, 1, "QImage*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QImageTextKeyLang*")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1cfb140;
  } else {
    score[3] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=3;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__text__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__text__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__text__OverloadedVersion__3(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__text matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QImage >::__LuaWrapCall__rgbSwapped (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QImage ret = __lua__obj->QImage::rgbSwapped();
  lqtL_passudata(L, new QImage(ret), "QImage*");
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__numColors (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QImage::numColors();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__scaled__OverloadedVersion__1 (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int arg2 = lua_tointeger(L, 3);
  Qt::AspectRatioMode arg3 = lqtL_isenum(L, 4, "Qt::AspectRatioMode")?static_cast<Qt::AspectRatioMode>(lqtL_toenum(L, 4, "Qt::AspectRatioMode")):Qt::IgnoreAspectRatio;
  Qt::TransformationMode arg4 = lqtL_isenum(L, 5, "Qt::TransformationMode")?static_cast<Qt::TransformationMode>(lqtL_toenum(L, 5, "Qt::TransformationMode")):Qt::FastTransformation;
  QImage ret = __lua__obj->QImage::scaled(arg1, arg2, arg3, arg4);
  lqtL_passudata(L, new QImage(ret), "QImage*");
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__scaled__OverloadedVersion__2 (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QSize& arg1 = **static_cast<QSize**>(lqtL_checkudata(L, 2, "QSize*"));
  Qt::AspectRatioMode arg2 = lqtL_isenum(L, 3, "Qt::AspectRatioMode")?static_cast<Qt::AspectRatioMode>(lqtL_toenum(L, 3, "Qt::AspectRatioMode")):Qt::IgnoreAspectRatio;
  Qt::TransformationMode arg3 = lqtL_isenum(L, 4, "Qt::TransformationMode")?static_cast<Qt::TransformationMode>(lqtL_toenum(L, 4, "Qt::TransformationMode")):Qt::FastTransformation;
  QImage ret = __lua__obj->QImage::scaled(arg1, arg2, arg3);
  lqtL_passudata(L, new QImage(ret), "QImage*");
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__scaled (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QImage*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1cdfd20;
  } else {
    score[1] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1ce0280;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_isenum(L, 4, "Qt::AspectRatioMode")) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x1ce0220;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_isenum(L, 5, "Qt::TransformationMode")) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x1ce0b20;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QImage*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QSize*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1ce14a0;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_isenum(L, 3, "Qt::AspectRatioMode")) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x1ce11e0;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_isenum(L, 4, "Qt::TransformationMode")) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x1ce19a0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__scaled__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__scaled__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__scaled matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QImage >::__LuaWrapCall__rect (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QRect ret = __lua__obj->QImage::rect();
  lqtL_passudata(L, new QRect(ret), "QRect*");
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__detach (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QImage::detach();
  return 0;
}
int LuaBinder< QImage >::__LuaWrapCall__offset (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QPoint ret = __lua__obj->QImage::offset();
  lqtL_passudata(L, new QPoint(ret), "QPoint*");
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__height (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QImage::height();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__textLanguages (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QStringList ret = __lua__obj->QImage::textLanguages();
  lqtL_passudata(L, new QStringList(ret), "QStringList*");
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__pixel__OverloadedVersion__1 (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int arg2 = lua_tointeger(L, 3);
  unsigned int ret = __lua__obj->QImage::pixel(arg1, arg2);
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__pixel__OverloadedVersion__2 (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPoint& arg1 = **static_cast<QPoint**>(lqtL_checkudata(L, 2, "QPoint*"));
  unsigned int ret = __lua__obj->QImage::pixel(arg1);
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__pixel (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QImage*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1cd66e0;
  } else {
    score[1] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1cd6c00;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QImage*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPoint*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1cd7520;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__pixel__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__pixel__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__pixel matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QImage >::__LuaWrapCall__setColor (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  unsigned int arg2 = lua_tointeger(L, 3);
  __lua__obj->QImage::setColor(arg1, arg2);
  return 0;
}
int LuaBinder< QImage >::__LuaWrapCall__setPixel__OverloadedVersion__1 (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int arg2 = lua_tointeger(L, 3);
  unsigned int arg3 = lua_tointeger(L, 4);
  __lua__obj->QImage::setPixel(arg1, arg2, arg3);
  return 0;
}
int LuaBinder< QImage >::__LuaWrapCall__setPixel__OverloadedVersion__2 (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPoint& arg1 = **static_cast<QPoint**>(lqtL_checkudata(L, 2, "QPoint*"));
  unsigned int arg2 = lua_tointeger(L, 3);
  __lua__obj->QImage::setPixel(arg1, arg2);
  return 0;
}
int LuaBinder< QImage >::__LuaWrapCall__setPixel (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QImage*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1cd7fc0;
  } else {
    score[1] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1cd84e0;
  } else {
    score[1] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1cd8480;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QImage*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPoint*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1cd9250;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1cd8830;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__setPixel__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__setPixel__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__setPixel matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QImage >::__LuaWrapCall__bits__OverloadedVersion__1 (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  unsigned char * ret = __lua__obj->QImage::bits();
  lqtL_pushudata(L, ret, "unsigned char*");
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__bits__OverloadedVersion__2 (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const unsigned char * ret = __lua__obj->QImage::bits();
  lqtL_pushudata(L, ret, "unsigned char*");
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__bits (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QImage*")?premium:-premium*premium;
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QImage*")?premium:-premium*premium;
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__bits__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__bits__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__bits matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QImage >::__LuaWrapCall__color (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  unsigned int ret = __lua__obj->QImage::color(arg1);
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__load__OverloadedVersion__1 (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QIODevice * arg1 = *static_cast<QIODevice**>(lqtL_checkudata(L, 2, "QIODevice*"));
  const char * arg2 = lua_tostring(L, 3);
  bool ret = __lua__obj->QImage::load(arg1, arg2);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__load__OverloadedVersion__2 (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  const char * arg2 = (lua_type(L, 3)==LUA_TSTRING)?lua_tostring(L, 3):static_cast< const char * >(0);
  bool ret = __lua__obj->QImage::load(arg1, arg2);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__load (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QImage*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QIODevice*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1cea8f0;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 3)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1ceae50;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QImage*")?premium:-premium*premium;
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1ceb770;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 3)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x1ceb230;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__load__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__load__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__load matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QImage >::__LuaWrapCall__fill (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  unsigned int arg1 = lua_tointeger(L, 2);
  __lua__obj->QImage::fill(arg1);
  return 0;
}
int LuaBinder< QImage >::__LuaWrapCall__setText__OverloadedVersion__1 (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  const QString& arg2 = QString::fromAscii(lua_tostring(L, 3), lua_objlen(L, 3));
  __lua__obj->QImage::setText(arg1, arg2);
  return 0;
}
int LuaBinder< QImage >::__LuaWrapCall__setText__OverloadedVersion__2 (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const char * arg1 = lua_tostring(L, 2);
  const char * arg2 = lua_tostring(L, 3);
  const QString& arg3 = QString::fromAscii(lua_tostring(L, 4), lua_objlen(L, 4));
  __lua__obj->QImage::setText(arg1, arg2, arg3);
  return 0;
}
int LuaBinder< QImage >::__LuaWrapCall__setText (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QImage*")?premium:-premium*premium;
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1cf8670;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 3)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1cf8150;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QImage*")?premium:-premium*premium;
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1cfbae0;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 3)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1cfb7e0;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 4)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1cfc3c0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__setText__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__setText__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__setText matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QImage >::__LuaWrapCall__colorTable (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QVector<unsigned int> ret = __lua__obj->QImage::colorTable();
  lqtL_passudata(L, new QVector<unsigned int>(ret), "QVector<unsigned int>*");
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__paintEngine (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QPaintEngine * ret = __lua__obj->QImage::paintEngine();
  lqtL_pushudata(L, ret, "QPaintEngine*");
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__setAlphaChannel (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QImage& arg1 = **static_cast<QImage**>(lqtL_checkudata(L, 2, "QImage*"));
  __lua__obj->QImage::setAlphaChannel(arg1);
  return 0;
}
int LuaBinder< QImage >::__LuaWrapCall__pixelIndex__OverloadedVersion__1 (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int arg2 = lua_tointeger(L, 3);
  int ret = __lua__obj->QImage::pixelIndex(arg1, arg2);
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__pixelIndex__OverloadedVersion__2 (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPoint& arg1 = **static_cast<QPoint**>(lqtL_checkudata(L, 2, "QPoint*"));
  int ret = __lua__obj->QImage::pixelIndex(arg1);
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__pixelIndex (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QImage*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1cd4e10;
  } else {
    score[1] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1cd5330;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QImage*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPoint*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1cd5c70;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__pixelIndex__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__pixelIndex__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__pixelIndex matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QImage >::__LuaWrapCall__hasAlphaChannel (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QImage::hasAlphaChannel();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__numBytes (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QImage::numBytes();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__dotsPerMeterX (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QImage::dotsPerMeterX();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__setOffset (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPoint& arg1 = **static_cast<QPoint**>(lqtL_checkudata(L, 2, "QPoint*"));
  __lua__obj->QImage::setOffset(arg1);
  return 0;
}
int LuaBinder< QImage >::__LuaWrapCall__setDotsPerMeterX (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QImage::setDotsPerMeterX(arg1);
  return 0;
}
int LuaBinder< QImage >::__LuaWrapCall__invertPixels (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QImage::InvertMode arg1 = lqtL_isenum(L, 2, "QImage::InvertMode")?static_cast<QImage::InvertMode>(lqtL_toenum(L, 2, "QImage::InvertMode")):QImage::InvertRgb;
  __lua__obj->QImage::invertPixels(arg1);
  return 0;
}
int LuaBinder< QImage >::__LuaWrapCall__createAlphaMask (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<Qt::ImageConversionFlag> arg1 = lqtL_testudata(L, 2, "QFlags<Qt::ImageConversionFlag>*")?**static_cast<QFlags<Qt::ImageConversionFlag>**>(lqtL_checkudata(L, 2, "QFlags<Qt::ImageConversionFlag>*")):Qt::AutoColor;
  QImage ret = __lua__obj->QImage::createAlphaMask(arg1);
  lqtL_passudata(L, new QImage(ret), "QImage*");
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__dotsPerMeterY (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QImage::dotsPerMeterY();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__isGrayscale (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QImage::isGrayscale();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__mirrored (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = lua_isboolean(L, 2)?(bool)lua_toboolean(L, 2):false;
  bool arg2 = lua_isboolean(L, 3)?(bool)lua_toboolean(L, 3):true;
  QImage ret = __lua__obj->QImage::mirrored(arg1, arg2);
  lqtL_passudata(L, new QImage(ret), "QImage*");
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__isNull (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QImage::isNull();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__width (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QImage::width();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__createHeuristicMask (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = lua_isboolean(L, 2)?(bool)lua_toboolean(L, 2):true;
  QImage ret = __lua__obj->QImage::createHeuristicMask(arg1);
  lqtL_passudata(L, new QImage(ret), "QImage*");
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__setNumColors (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QImage::setNumColors(arg1);
  return 0;
}
int LuaBinder< QImage >::__LuaWrapCall__scaledToHeight (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  Qt::TransformationMode arg2 = lqtL_isenum(L, 3, "Qt::TransformationMode")?static_cast<Qt::TransformationMode>(lqtL_toenum(L, 3, "Qt::TransformationMode")):Qt::FastTransformation;
  QImage ret = __lua__obj->QImage::scaledToHeight(arg1, arg2);
  lqtL_passudata(L, new QImage(ret), "QImage*");
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__scanLine__OverloadedVersion__1 (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  unsigned char * ret = __lua__obj->QImage::scanLine(arg1);
  lqtL_pushudata(L, ret, "unsigned char*");
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__scanLine__OverloadedVersion__2 (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  const unsigned char * ret = __lua__obj->QImage::scanLine(arg1);
  lqtL_pushudata(L, ret, "unsigned char*");
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__scanLine (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QImage*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1cd1aa0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QImage*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1cd2420;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__scanLine__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__scanLine__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__scanLine matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QImage >::__LuaWrapCall__cacheKey (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  long long int ret = __lua__obj->QImage::cacheKey();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__setDotsPerMeterY (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QImage::setDotsPerMeterY(arg1);
  return 0;
}
int LuaBinder< QImage >::__LuaWrapCall__alphaChannel (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QImage ret = __lua__obj->QImage::alphaChannel();
  lqtL_passudata(L, new QImage(ret), "QImage*");
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__transformed__OverloadedVersion__1 (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QMatrix& arg1 = **static_cast<QMatrix**>(lqtL_checkudata(L, 2, "QMatrix*"));
  Qt::TransformationMode arg2 = lqtL_isenum(L, 3, "Qt::TransformationMode")?static_cast<Qt::TransformationMode>(lqtL_toenum(L, 3, "Qt::TransformationMode")):Qt::FastTransformation;
  QImage ret = __lua__obj->QImage::transformed(arg1, arg2);
  lqtL_passudata(L, new QImage(ret), "QImage*");
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__transformed__OverloadedVersion__2 (lua_State *L) {
  QImage *& __lua__obj = *static_cast<QImage**>(lqtL_checkudata(L, 1, "QImage*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QTransform& arg1 = **static_cast<QTransform**>(lqtL_checkudata(L, 2, "QTransform*"));
  Qt::TransformationMode arg2 = lqtL_isenum(L, 3, "Qt::TransformationMode")?static_cast<Qt::TransformationMode>(lqtL_toenum(L, 3, "Qt::TransformationMode")):Qt::FastTransformation;
  QImage ret = __lua__obj->QImage::transformed(arg1, arg2);
  lqtL_passudata(L, new QImage(ret), "QImage*");
  return 1;
}
int LuaBinder< QImage >::__LuaWrapCall__transformed (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QImage*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QMatrix*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1ce4690;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_isenum(L, 3, "Qt::TransformationMode")) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x1ce4070;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QImage*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QTransform*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1ce66e0;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_isenum(L, 3, "Qt::TransformationMode")) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x1ce5d30;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__transformed__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__transformed__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__transformed matching arguments");
	lua_error(L);
	return 0;
}
QPaintEngine * LuaBinder< QImage >::paintEngine () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QImage*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "paintEngine");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QImage::paintEngine();
  }
  QPaintEngine * ret = *static_cast<QPaintEngine**>(lqtL_checkudata(L, -1, "QPaintEngine*"));
  lua_settop(L, oldtop);
  return ret;
}
int LuaBinder< QImage >::metric (QPaintDevice::PaintDeviceMetric arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QImage*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "metric");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushenum(L, arg1, "QPaintDevice::PaintDeviceMetric");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QImage::metric(arg1);
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
int LuaBinder< QImage >::devType () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QImage*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "devType");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QImage::devType();
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
LuaBinder< QImage >::  ~LuaBinder< QImage > () {
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QImage*");
  lua_getfield(L, -1, "~QImage");

  if (lua_isfunction(L, -1)) {
    lua_insert(L, -2);
    lua_pcall(L, 1, 1, 0);
  } else {
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QImage >::lqt_pushenum_InvertMode (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "InvertRgb");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "InvertRgb");
  lua_pushstring(L, "InvertRgba");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "InvertRgba");
  lua_pushcfunction(L, LuaBinder< QImage >::lqt_pushenum_InvertMode_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QImage::InvertMode");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QImage >::lqt_pushenum_InvertMode_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QImage::InvertMode>*) + sizeof(QFlags<QImage::InvertMode>));
  QFlags<QImage::InvertMode> *fl = static_cast<QFlags<QImage::InvertMode>*>( static_cast<void*>(&static_cast<QFlags<QImage::InvertMode>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QImage::InvertMode>(lqtL_toenum(L, i, "QImage::InvertMode"));
	}
	if (luaL_newmetatable(L, "QFlags<QImage::InvertMode>*")) {
		lua_pushstring(L, "QFlags<QImage::InvertMode>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinder< QImage >::lqt_pushenum_Format (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "Format_Invalid");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "Format_Invalid");
  lua_pushstring(L, "Format_Mono");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "Format_Mono");
  lua_pushstring(L, "Format_MonoLSB");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "Format_MonoLSB");
  lua_pushstring(L, "Format_Indexed8");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "Format_Indexed8");
  lua_pushstring(L, "Format_RGB32");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "Format_RGB32");
  lua_pushstring(L, "Format_ARGB32");
  lua_rawseti(L, enum_table, 5);
  lua_pushinteger(L, 5);
  lua_setfield(L, enum_table, "Format_ARGB32");
  lua_pushstring(L, "Format_ARGB32_Premultiplied");
  lua_rawseti(L, enum_table, 6);
  lua_pushinteger(L, 6);
  lua_setfield(L, enum_table, "Format_ARGB32_Premultiplied");
  lua_pushstring(L, "Format_RGB16");
  lua_rawseti(L, enum_table, 7);
  lua_pushinteger(L, 7);
  lua_setfield(L, enum_table, "Format_RGB16");
  lua_pushstring(L, "NImageFormats");
  lua_rawseti(L, enum_table, 8);
  lua_pushinteger(L, 8);
  lua_setfield(L, enum_table, "NImageFormats");
  lua_pushcfunction(L, LuaBinder< QImage >::lqt_pushenum_Format_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QImage::Format");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QImage >::lqt_pushenum_Format_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QImage::Format>*) + sizeof(QFlags<QImage::Format>));
  QFlags<QImage::Format> *fl = static_cast<QFlags<QImage::Format>*>( static_cast<void*>(&static_cast<QFlags<QImage::Format>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QImage::Format>(lqtL_toenum(L, i, "QImage::Format"));
	}
	if (luaL_newmetatable(L, "QFlags<QImage::Format>*")) {
		lua_pushstring(L, "QFlags<QImage::Format>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int luaopen_QImage (lua_State *L) {
  if (luaL_newmetatable(L, "QImage*")) {
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__trueMatrix);
    lua_setfield(L, -2, "trueMatrix");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__delete);
    lua_setfield(L, -2, "delete");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__textList);
    lua_setfield(L, -2, "textList");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__allGray);
    lua_setfield(L, -2, "allGray");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__save);
    lua_setfield(L, -2, "save");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__new);
    lua_setfield(L, -2, "new");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__createMaskFromColor);
    lua_setfield(L, -2, "createMaskFromColor");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__depth);
    lua_setfield(L, -2, "depth");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__copy);
    lua_setfield(L, -2, "copy");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__devType);
    lua_setfield(L, -2, "devType");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__textKeys);
    lua_setfield(L, -2, "textKeys");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__isDetached);
    lua_setfield(L, -2, "isDetached");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__format);
    lua_setfield(L, -2, "format");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__scaledToWidth);
    lua_setfield(L, -2, "scaledToWidth");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__size);
    lua_setfield(L, -2, "size");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__valid);
    lua_setfield(L, -2, "valid");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__convertToFormat);
    lua_setfield(L, -2, "convertToFormat");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__serialNumber);
    lua_setfield(L, -2, "serialNumber");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__setColorTable);
    lua_setfield(L, -2, "setColorTable");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__bytesPerLine);
    lua_setfield(L, -2, "bytesPerLine");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__text);
    lua_setfield(L, -2, "text");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__rgbSwapped);
    lua_setfield(L, -2, "rgbSwapped");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__numColors);
    lua_setfield(L, -2, "numColors");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__scaled);
    lua_setfield(L, -2, "scaled");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__rect);
    lua_setfield(L, -2, "rect");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__detach);
    lua_setfield(L, -2, "detach");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__offset);
    lua_setfield(L, -2, "offset");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__height);
    lua_setfield(L, -2, "height");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__textLanguages);
    lua_setfield(L, -2, "textLanguages");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__pixel);
    lua_setfield(L, -2, "pixel");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__setColor);
    lua_setfield(L, -2, "setColor");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__setPixel);
    lua_setfield(L, -2, "setPixel");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__bits);
    lua_setfield(L, -2, "bits");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__color);
    lua_setfield(L, -2, "color");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__load);
    lua_setfield(L, -2, "load");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__fill);
    lua_setfield(L, -2, "fill");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__setText);
    lua_setfield(L, -2, "setText");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__colorTable);
    lua_setfield(L, -2, "colorTable");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__paintEngine);
    lua_setfield(L, -2, "paintEngine");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__setAlphaChannel);
    lua_setfield(L, -2, "setAlphaChannel");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__pixelIndex);
    lua_setfield(L, -2, "pixelIndex");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__hasAlphaChannel);
    lua_setfield(L, -2, "hasAlphaChannel");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__numBytes);
    lua_setfield(L, -2, "numBytes");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__dotsPerMeterX);
    lua_setfield(L, -2, "dotsPerMeterX");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__setOffset);
    lua_setfield(L, -2, "setOffset");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__setDotsPerMeterX);
    lua_setfield(L, -2, "setDotsPerMeterX");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__invertPixels);
    lua_setfield(L, -2, "invertPixels");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__createAlphaMask);
    lua_setfield(L, -2, "createAlphaMask");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__dotsPerMeterY);
    lua_setfield(L, -2, "dotsPerMeterY");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__isGrayscale);
    lua_setfield(L, -2, "isGrayscale");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__mirrored);
    lua_setfield(L, -2, "mirrored");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__isNull);
    lua_setfield(L, -2, "isNull");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__width);
    lua_setfield(L, -2, "width");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__createHeuristicMask);
    lua_setfield(L, -2, "createHeuristicMask");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__setNumColors);
    lua_setfield(L, -2, "setNumColors");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__scaledToHeight);
    lua_setfield(L, -2, "scaledToHeight");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__scanLine);
    lua_setfield(L, -2, "scanLine");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__cacheKey);
    lua_setfield(L, -2, "cacheKey");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__setDotsPerMeterY);
    lua_setfield(L, -2, "setDotsPerMeterY");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__alphaChannel);
    lua_setfield(L, -2, "alphaChannel");
    lua_pushcfunction(L, LuaBinder< QImage >::__LuaWrapCall__transformed);
    lua_setfield(L, -2, "transformed");
    LuaBinder< QImage >::lqt_pushenum_InvertMode(L);
    lua_setfield(L, -2, "InvertMode");
    LuaBinder< QImage >::lqt_pushenum_Format(L);
    lua_setfield(L, -2, "Format");
    lua_newtable(L);
    lua_pushboolean(L, 1);
    lua_setfield(L, -2, "QPaintDevice*");
    lua_setfield(L, -2, "__base");
    lua_pushcfunction(L, lqtL_newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, lqtL_index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, lqtL_gc);
    lua_setfield(L, -2, "__gc");
    lua_pushstring(L, "QImage");
    lua_setfield(L, -2, "__qtype");
		lua_setglobal(L, "QImage");
  } else {
		lua_pop(L, 1);
	}
  return 0;
}
