#include "lqt_bind_QDialog.hpp"

int LuaBinder< QDialog >::__LuaWrapCall__setModal (lua_State *L) {
  QDialog *& __lua__obj = *static_cast<QDialog**>(lqtL_checkudata(L, 1, "QDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QDialog::setModal(arg1);
  return 0;
}
int LuaBinder< QDialog >::__LuaWrapCall__setExtension (lua_State *L) {
  QDialog *& __lua__obj = *static_cast<QDialog**>(lqtL_checkudata(L, 1, "QDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 2, "QWidget*"));
  __lua__obj->QDialog::setExtension(arg1);
  return 0;
}
int LuaBinder< QDialog >::__LuaWrapCall__sizeHint (lua_State *L) {
  QDialog *& __lua__obj = *static_cast<QDialog**>(lqtL_checkudata(L, 1, "QDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QSize ret = __lua__obj->QDialog::sizeHint();
  lqtL_passudata(L, new QSize(ret), "QSize*");
  return 1;
}
int LuaBinder< QDialog >::__LuaWrapCall__minimumSizeHint (lua_State *L) {
  QDialog *& __lua__obj = *static_cast<QDialog**>(lqtL_checkudata(L, 1, "QDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QSize ret = __lua__obj->QDialog::minimumSizeHint();
  lqtL_passudata(L, new QSize(ret), "QSize*");
  return 1;
}
int LuaBinder< QDialog >::__LuaWrapCall__setVisible (lua_State *L) {
  QDialog *& __lua__obj = *static_cast<QDialog**>(lqtL_checkudata(L, 1, "QDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QDialog::setVisible(arg1);
  return 0;
}
int LuaBinder< QDialog >::__LuaWrapCall__setOrientation (lua_State *L) {
  QDialog *& __lua__obj = *static_cast<QDialog**>(lqtL_checkudata(L, 1, "QDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::Orientation arg1 = static_cast<Qt::Orientation>(lqtL_toenum(L, 2, "Qt::Orientation"));
  __lua__obj->QDialog::setOrientation(arg1);
  return 0;
}
int LuaBinder< QDialog >::__LuaWrapCall__isSizeGripEnabled (lua_State *L) {
  QDialog *& __lua__obj = *static_cast<QDialog**>(lqtL_checkudata(L, 1, "QDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QDialog::isSizeGripEnabled();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QDialog >::__LuaWrapCall__metaObject (lua_State *L) {
  QDialog *& __lua__obj = *static_cast<QDialog**>(lqtL_checkudata(L, 1, "QDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QMetaObject * ret = __lua__obj->QDialog::metaObject();
  lqtL_pushudata(L, ret, "QMetaObject*");
  return 1;
}
int LuaBinder< QDialog >::__LuaWrapCall__new (lua_State *L) {
  QWidget * arg1 = lqtL_testudata(L, 1, "QWidget*")?*static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*")):static_cast< QWidget * >(0);
  QFlags<Qt::WindowType> arg2 = lqtL_testudata(L, 2, "QFlags<Qt::WindowType>*")?**static_cast<QFlags<Qt::WindowType>**>(lqtL_checkudata(L, 2, "QFlags<Qt::WindowType>*")):static_cast< QFlags<Qt::WindowType> >(0);
  QDialog * ret = new LuaBinder< QDialog >(L, arg1, arg2);
  lqtL_passudata(L, ret, "QDialog*");
  return 1;
}
int LuaBinder< QDialog >::__LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QDialog::tr(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QDialog >::__LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QDialog::tr(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QDialog >::__LuaWrapCall__tr (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1f78980;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x1f786f0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1f7a3a0;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1f7a890;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1f7ac40;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__tr__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__tr__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__tr matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QDialog >::__LuaWrapCall__delete (lua_State *L) {
  QDialog *& __lua__obj = *static_cast<QDialog**>(lqtL_checkudata(L, 1, "QDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  delete __lua__obj;
  __lua__obj = 0;
  return 0;
}
int LuaBinder< QDialog >::__LuaWrapCall__extension (lua_State *L) {
  QDialog *& __lua__obj = *static_cast<QDialog**>(lqtL_checkudata(L, 1, "QDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * ret = __lua__obj->QDialog::extension();
  lqtL_pushudata(L, ret, "QWidget*");
  return 1;
}
int LuaBinder< QDialog >::__LuaWrapCall__accept (lua_State *L) {
  QDialog *& __lua__obj = *static_cast<QDialog**>(lqtL_checkudata(L, 1, "QDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QDialog::accept();
  return 0;
}
int LuaBinder< QDialog >::__LuaWrapCall__done (lua_State *L) {
  QDialog *& __lua__obj = *static_cast<QDialog**>(lqtL_checkudata(L, 1, "QDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QDialog::done(arg1);
  return 0;
}
int LuaBinder< QDialog >::__LuaWrapCall__result (lua_State *L) {
  QDialog *& __lua__obj = *static_cast<QDialog**>(lqtL_checkudata(L, 1, "QDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QDialog::result();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QDialog >::__LuaWrapCall__reject (lua_State *L) {
  QDialog *& __lua__obj = *static_cast<QDialog**>(lqtL_checkudata(L, 1, "QDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QDialog::reject();
  return 0;
}
int LuaBinder< QDialog >::__LuaWrapCall__setSizeGripEnabled (lua_State *L) {
  QDialog *& __lua__obj = *static_cast<QDialog**>(lqtL_checkudata(L, 1, "QDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QDialog::setSizeGripEnabled(arg1);
  return 0;
}
int LuaBinder< QDialog >::__LuaWrapCall__setResult (lua_State *L) {
  QDialog *& __lua__obj = *static_cast<QDialog**>(lqtL_checkudata(L, 1, "QDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QDialog::setResult(arg1);
  return 0;
}
int LuaBinder< QDialog >::__LuaWrapCall__orientation (lua_State *L) {
  QDialog *& __lua__obj = *static_cast<QDialog**>(lqtL_checkudata(L, 1, "QDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::Orientation ret = __lua__obj->QDialog::orientation();
  lqtL_pushenum(L, ret, "Qt::Orientation");
  return 1;
}
int LuaBinder< QDialog >::__LuaWrapCall__exec (lua_State *L) {
  QDialog *& __lua__obj = *static_cast<QDialog**>(lqtL_checkudata(L, 1, "QDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QDialog::exec();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QDialog >::__LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QDialog::trUtf8(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QDialog >::__LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QDialog::trUtf8(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QDialog >::__LuaWrapCall__trUtf8 (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1f796c0;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x1f79220;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1f7b4b0;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1f7abe0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1f7b940;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__trUtf8__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__trUtf8__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__trUtf8 matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QDialog >::__LuaWrapCall__showExtension (lua_State *L) {
  QDialog *& __lua__obj = *static_cast<QDialog**>(lqtL_checkudata(L, 1, "QDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QDialog::showExtension(arg1);
  return 0;
}
void LuaBinder< QDialog >::styleChange (QStyle& arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "styleChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QStyle*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::styleChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QDialog >::focusInEvent (QFocusEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "focusInEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QFocusEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::focusInEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QDialog >::keyPressEvent (QKeyEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "keyPressEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QKeyEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QDialog::keyPressEvent(arg1);
  }
  lua_settop(L, oldtop);
}
const QMetaObject * LuaBinder< QDialog >::metaObject () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "metaObject");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QDialog::metaObject();
  }
  const QMetaObject * ret = *static_cast<QMetaObject**>(lqtL_checkudata(L, -1, "QMetaObject*"));
  lua_settop(L, oldtop);
  return ret;
}
int LuaBinder< QDialog >::devType () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "devType");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::devType();
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QDialog >::setVisible (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "setVisible");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QDialog::setVisible(arg1);
  }
  lua_settop(L, oldtop);
}
QVariant LuaBinder< QDialog >::inputMethodQuery (Qt::InputMethodQuery arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "inputMethodQuery");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushenum(L, arg1, "Qt::InputMethodQuery");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::inputMethodQuery(arg1);
  }
  QVariant ret = **static_cast<QVariant**>(lqtL_checkudata(L, -1, "QVariant*"));
  lua_settop(L, oldtop);
  return ret;
}
QPaintEngine * LuaBinder< QDialog >::paintEngine () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "paintEngine");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::paintEngine();
  }
  QPaintEngine * ret = *static_cast<QPaintEngine**>(lqtL_checkudata(L, -1, "QPaintEngine*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QDialog >::dragLeaveEvent (QDragLeaveEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dragLeaveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDragLeaveEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::dragLeaveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QDialog >::mousePressEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mousePressEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::mousePressEvent(arg1);
  }
  lua_settop(L, oldtop);
}
QSize LuaBinder< QDialog >::sizeHint () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "sizeHint");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QDialog::sizeHint();
  }
  QSize ret = **static_cast<QSize**>(lqtL_checkudata(L, -1, "QSize*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QDialog >::moveEvent (QMoveEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "moveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMoveEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::moveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QDialog >::enterEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "enterEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::enterEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QDialog >::accept () {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "accept");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QDialog::accept();
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QDialog >::tabletEvent (QTabletEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "tabletEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QTabletEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::tabletEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QDialog >::closeEvent (QCloseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "closeEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QCloseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QDialog::closeEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QDialog >::actionEvent (QActionEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "actionEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QActionEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::actionEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QDialog >::dragEnterEvent (QDragEnterEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dragEnterEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDragEnterEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::dragEnterEvent(arg1);
  }
  lua_settop(L, oldtop);
}
QSize LuaBinder< QDialog >::minimumSizeHint () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "minimumSizeHint");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QDialog::minimumSizeHint();
  }
  QSize ret = **static_cast<QSize**>(lqtL_checkudata(L, -1, "QSize*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QDialog >::dragMoveEvent (QDragMoveEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dragMoveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDragMoveEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::dragMoveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QDialog >::dropEvent (QDropEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dropEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDropEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::dropEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QDialog >::showEvent (QShowEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "showEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QShowEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QDialog::showEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QDialog >::contextMenuEvent (QContextMenuEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "contextMenuEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QContextMenuEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QDialog::contextMenuEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QDialog >::paintEvent (QPaintEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "paintEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QPaintEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::paintEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QDialog >::windowActivationChange (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "windowActivationChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::windowActivationChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QDialog >::fontChange (const QFont& arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "fontChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QFont*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::fontChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QDialog >::enabledChange (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "enabledChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::enabledChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QDialog >::disconnectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "disconnectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::disconnectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QDialog >::heightForWidth (int arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "heightForWidth");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushinteger(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::heightForWidth(arg1);
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QDialog >::mouseDoubleClickEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mouseDoubleClickEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::mouseDoubleClickEvent(arg1);
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QDialog >::focusNextPrevChild (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "focusNextPrevChild");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::focusNextPrevChild(arg1);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QDialog >::timerEvent (QTimerEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "timerEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QTimerEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::timerEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QDialog >::mouseMoveEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mouseMoveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::mouseMoveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QDialog >::metric (QPaintDevice::PaintDeviceMetric arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "metric");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushenum(L, arg1, "QPaintDevice::PaintDeviceMetric");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::metric(arg1);
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QDialog >::childEvent (QChildEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "childEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QChildEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::childEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QDialog >::connectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "connectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::connectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QDialog >::mouseReleaseEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mouseReleaseEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::mouseReleaseEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QDialog >::focusOutEvent (QFocusEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "focusOutEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QFocusEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::focusOutEvent(arg1);
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QDialog >::eventFilter (QObject * arg1, QEvent * arg2) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "eventFilter");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QObject*");
  lqtL_pushudata(L, arg2, "QEvent*");
  if (lua_isfunction(L, -2-2)) {
    lua_pcall(L, 2+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QDialog::eventFilter(arg1, arg2);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QDialog >::inputMethodEvent (QInputMethodEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "inputMethodEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QInputMethodEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::inputMethodEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QDialog >::wheelEvent (QWheelEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "wheelEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QWheelEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::wheelEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QDialog >::done (int arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "done");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushinteger(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QDialog::done(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QDialog >::paletteChange (const QPalette& arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "paletteChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QPalette*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::paletteChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QDialog >::languageChange () {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "languageChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::languageChange();
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QDialog >::event (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "event");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::event(arg1);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QDialog >::hideEvent (QHideEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "hideEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QHideEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::hideEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QDialog >::keyReleaseEvent (QKeyEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "keyReleaseEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QKeyEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::keyReleaseEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QDialog >::changeEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "changeEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::changeEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QDialog >::resizeEvent (QResizeEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "resizeEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QResizeEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QDialog::resizeEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QDialog >::leaveEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "leaveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::leaveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QDialog >::customEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "customEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::customEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QDialog >::reject () {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "reject");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QDialog::reject();
  }
  lua_settop(L, oldtop);
}
LuaBinder< QDialog >::  ~LuaBinder< QDialog > () {
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QDialog*");
  lua_getfield(L, -1, "~QDialog");

  if (lua_isfunction(L, -1)) {
    lua_insert(L, -2);
    lua_pcall(L, 1, 1, 0);
  } else {
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QDialog >::lqt_pushenum_DialogCode (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "Rejected");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "Rejected");
  lua_pushstring(L, "Accepted");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "Accepted");
  lua_pushcfunction(L, LuaBinder< QDialog >::lqt_pushenum_DialogCode_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QDialog::DialogCode");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QDialog >::lqt_pushenum_DialogCode_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QDialog::DialogCode>*) + sizeof(QFlags<QDialog::DialogCode>));
  QFlags<QDialog::DialogCode> *fl = static_cast<QFlags<QDialog::DialogCode>*>( static_cast<void*>(&static_cast<QFlags<QDialog::DialogCode>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QDialog::DialogCode>(lqtL_toenum(L, i, "QDialog::DialogCode"));
	}
	if (luaL_newmetatable(L, "QFlags<QDialog::DialogCode>*")) {
		lua_pushstring(L, "QFlags<QDialog::DialogCode>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int luaopen_QDialog (lua_State *L) {
  if (luaL_newmetatable(L, "QDialog*")) {
    lua_pushcfunction(L, LuaBinder< QDialog >::__LuaWrapCall__setModal);
    lua_setfield(L, -2, "setModal");
    lua_pushcfunction(L, LuaBinder< QDialog >::__LuaWrapCall__setExtension);
    lua_setfield(L, -2, "setExtension");
    lua_pushcfunction(L, LuaBinder< QDialog >::__LuaWrapCall__sizeHint);
    lua_setfield(L, -2, "sizeHint");
    lua_pushcfunction(L, LuaBinder< QDialog >::__LuaWrapCall__minimumSizeHint);
    lua_setfield(L, -2, "minimumSizeHint");
    lua_pushcfunction(L, LuaBinder< QDialog >::__LuaWrapCall__setVisible);
    lua_setfield(L, -2, "setVisible");
    lua_pushcfunction(L, LuaBinder< QDialog >::__LuaWrapCall__setOrientation);
    lua_setfield(L, -2, "setOrientation");
    lua_pushcfunction(L, LuaBinder< QDialog >::__LuaWrapCall__isSizeGripEnabled);
    lua_setfield(L, -2, "isSizeGripEnabled");
    lua_pushcfunction(L, LuaBinder< QDialog >::__LuaWrapCall__metaObject);
    lua_setfield(L, -2, "metaObject");
    lua_pushcfunction(L, LuaBinder< QDialog >::__LuaWrapCall__new);
    lua_setfield(L, -2, "new");
    lua_pushcfunction(L, LuaBinder< QDialog >::__LuaWrapCall__tr);
    lua_setfield(L, -2, "tr");
    lua_pushcfunction(L, LuaBinder< QDialog >::__LuaWrapCall__delete);
    lua_setfield(L, -2, "delete");
    lua_pushcfunction(L, LuaBinder< QDialog >::__LuaWrapCall__extension);
    lua_setfield(L, -2, "extension");
    lua_pushcfunction(L, LuaBinder< QDialog >::__LuaWrapCall__accept);
    lua_setfield(L, -2, "accept");
    lua_pushcfunction(L, LuaBinder< QDialog >::__LuaWrapCall__done);
    lua_setfield(L, -2, "done");
    lua_pushcfunction(L, LuaBinder< QDialog >::__LuaWrapCall__result);
    lua_setfield(L, -2, "result");
    lua_pushcfunction(L, LuaBinder< QDialog >::__LuaWrapCall__reject);
    lua_setfield(L, -2, "reject");
    lua_pushcfunction(L, LuaBinder< QDialog >::__LuaWrapCall__setSizeGripEnabled);
    lua_setfield(L, -2, "setSizeGripEnabled");
    lua_pushcfunction(L, LuaBinder< QDialog >::__LuaWrapCall__setResult);
    lua_setfield(L, -2, "setResult");
    lua_pushcfunction(L, LuaBinder< QDialog >::__LuaWrapCall__orientation);
    lua_setfield(L, -2, "orientation");
    lua_pushcfunction(L, LuaBinder< QDialog >::__LuaWrapCall__exec);
    lua_setfield(L, -2, "exec");
    lua_pushcfunction(L, LuaBinder< QDialog >::__LuaWrapCall__trUtf8);
    lua_setfield(L, -2, "trUtf8");
    lua_pushcfunction(L, LuaBinder< QDialog >::__LuaWrapCall__showExtension);
    lua_setfield(L, -2, "showExtension");
    LuaBinder< QDialog >::lqt_pushenum_DialogCode(L);
    lua_setfield(L, -2, "DialogCode");
    lua_newtable(L);
    lua_pushboolean(L, 1);
    lua_setfield(L, -2, "QWidget*");
    lua_pushboolean(L, 0);
    lua_setfield(L, -2, "QObject*");
    lua_pushboolean(L, 0);
    lua_setfield(L, -2, "QPaintDevice*");
    lua_setfield(L, -2, "__base");
    lua_pushcfunction(L, lqtL_newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, lqtL_index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, lqtL_gc);
    lua_setfield(L, -2, "__gc");
    lua_pushstring(L, "QDialog");
    lua_setfield(L, -2, "__qtype");
		lua_setglobal(L, "QDialog");
  } else {
		lua_pop(L, 1);
	}
  return 0;
}
