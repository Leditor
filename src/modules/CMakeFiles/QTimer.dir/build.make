# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.4

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canoncical targets will work.
.SUFFIXES:

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# The program to use to edit the cache.
CMAKE_EDIT_COMMAND = /usr/bin/ccmake

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/iax/projects/editor/src/src

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/iax/projects/editor/src/src

# Include any dependencies generated for this target.
include CMakeFiles/QTimer.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/QTimer.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/QTimer.dir/flags.make

CMakeFiles/QTimer.dir/depend.make.mark: CMakeFiles/QTimer.dir/flags.make
CMakeFiles/QTimer.dir/depend.make.mark: lqt_bind_QTimer.cpp

CMakeFiles/QTimer.dir/lqt_bind_QTimer.o: CMakeFiles/QTimer.dir/flags.make
CMakeFiles/QTimer.dir/lqt_bind_QTimer.o: lqt_bind_QTimer.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/iax/projects/editor/src/src/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/QTimer.dir/lqt_bind_QTimer.o"
	/usr/bin/c++   $(CXX_FLAGS) -ggdb -o CMakeFiles/QTimer.dir/lqt_bind_QTimer.o -c /home/iax/projects/editor/src/src/lqt_bind_QTimer.cpp

CMakeFiles/QTimer.dir/lqt_bind_QTimer.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/QTimer.dir/lqt_bind_QTimer.i"
	/usr/bin/c++  $(CXX_FLAGS) -ggdb -E /home/iax/projects/editor/src/src/lqt_bind_QTimer.cpp > CMakeFiles/QTimer.dir/lqt_bind_QTimer.i

CMakeFiles/QTimer.dir/lqt_bind_QTimer.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/QTimer.dir/lqt_bind_QTimer.s"
	/usr/bin/c++  $(CXX_FLAGS) -ggdb -S /home/iax/projects/editor/src/src/lqt_bind_QTimer.cpp -o CMakeFiles/QTimer.dir/lqt_bind_QTimer.s

CMakeFiles/QTimer.dir/lqt_bind_QTimer.o.requires:

CMakeFiles/QTimer.dir/lqt_bind_QTimer.o.provides: CMakeFiles/QTimer.dir/lqt_bind_QTimer.o.requires
	$(MAKE) -f CMakeFiles/QTimer.dir/build.make CMakeFiles/QTimer.dir/lqt_bind_QTimer.o.provides.build

CMakeFiles/QTimer.dir/lqt_bind_QTimer.o.provides.build: CMakeFiles/QTimer.dir/lqt_bind_QTimer.o

CMakeFiles/QTimer.dir/depend: CMakeFiles/QTimer.dir/depend.make.mark

CMakeFiles/QTimer.dir/depend.make.mark:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --magenta --bold "Scanning dependencies of target QTimer"
	cd /home/iax/projects/editor/src/src && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/iax/projects/editor/src/src /home/iax/projects/editor/src/src /home/iax/projects/editor/src/src /home/iax/projects/editor/src/src /home/iax/projects/editor/src/src/CMakeFiles/QTimer.dir/DependInfo.cmake

# Object files for target QTimer
QTimer_OBJECTS = \
"CMakeFiles/QTimer.dir/lqt_bind_QTimer.o"

# External object files for target QTimer
QTimer_EXTERNAL_OBJECTS =

libQTimer.so: CMakeFiles/QTimer.dir/lqt_bind_QTimer.o
libQTimer.so: /opt/qt4/lib/libQtGui.so
libQTimer.so: /usr/lib/libpng.so
libQTimer.so: /usr/lib/libSM.so
libQTimer.so: /usr/lib/libICE.so
libQTimer.so: /usr/lib/libXi.so
libQTimer.so: /usr/lib/libXrender.so
libQTimer.so: /usr/lib/libXrandr.so
libQTimer.so: /usr/lib/libXcursor.so
libQTimer.so: /usr/lib/libXinerama.so
libQTimer.so: /usr/lib/libfreetype.so
libQTimer.so: /usr/lib/libfontconfig.so
libQTimer.so: /usr/lib/libXext.so
libQTimer.so: /usr/lib/libX11.so
libQTimer.so: /usr/lib/libm.so
libQTimer.so: /opt/qt4/lib/libQtCore.so
libQTimer.so: /usr/lib/libz.so
libQTimer.so: CMakeFiles/QTimer.dir/build.make
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX shared library libQTimer.so"
	$(CMAKE_COMMAND) -P CMakeFiles/QTimer.dir/cmake_clean_target.cmake
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/QTimer.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/QTimer.dir/build: libQTimer.so

CMakeFiles/QTimer.dir/requires: CMakeFiles/QTimer.dir/lqt_bind_QTimer.o.requires

CMakeFiles/QTimer.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/QTimer.dir/cmake_clean.cmake

