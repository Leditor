# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.4

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canoncical targets will work.
.SUFFIXES:

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# The program to use to edit the cache.
CMAKE_EDIT_COMMAND = /usr/bin/ccmake

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/iax/projects/editor/src/src

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/iax/projects/editor/src/src

# Include any dependencies generated for this target.
include CMakeFiles/QTextCursor.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/QTextCursor.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/QTextCursor.dir/flags.make

CMakeFiles/QTextCursor.dir/depend.make.mark: CMakeFiles/QTextCursor.dir/flags.make
CMakeFiles/QTextCursor.dir/depend.make.mark: lqt_bind_QTextCursor.cpp

CMakeFiles/QTextCursor.dir/lqt_bind_QTextCursor.o: CMakeFiles/QTextCursor.dir/flags.make
CMakeFiles/QTextCursor.dir/lqt_bind_QTextCursor.o: lqt_bind_QTextCursor.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/iax/projects/editor/src/src/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/QTextCursor.dir/lqt_bind_QTextCursor.o"
	/usr/bin/c++   $(CXX_FLAGS) -ggdb -o CMakeFiles/QTextCursor.dir/lqt_bind_QTextCursor.o -c /home/iax/projects/editor/src/src/lqt_bind_QTextCursor.cpp

CMakeFiles/QTextCursor.dir/lqt_bind_QTextCursor.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/QTextCursor.dir/lqt_bind_QTextCursor.i"
	/usr/bin/c++  $(CXX_FLAGS) -ggdb -E /home/iax/projects/editor/src/src/lqt_bind_QTextCursor.cpp > CMakeFiles/QTextCursor.dir/lqt_bind_QTextCursor.i

CMakeFiles/QTextCursor.dir/lqt_bind_QTextCursor.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/QTextCursor.dir/lqt_bind_QTextCursor.s"
	/usr/bin/c++  $(CXX_FLAGS) -ggdb -S /home/iax/projects/editor/src/src/lqt_bind_QTextCursor.cpp -o CMakeFiles/QTextCursor.dir/lqt_bind_QTextCursor.s

CMakeFiles/QTextCursor.dir/lqt_bind_QTextCursor.o.requires:

CMakeFiles/QTextCursor.dir/lqt_bind_QTextCursor.o.provides: CMakeFiles/QTextCursor.dir/lqt_bind_QTextCursor.o.requires
	$(MAKE) -f CMakeFiles/QTextCursor.dir/build.make CMakeFiles/QTextCursor.dir/lqt_bind_QTextCursor.o.provides.build

CMakeFiles/QTextCursor.dir/lqt_bind_QTextCursor.o.provides.build: CMakeFiles/QTextCursor.dir/lqt_bind_QTextCursor.o

CMakeFiles/QTextCursor.dir/depend: CMakeFiles/QTextCursor.dir/depend.make.mark

CMakeFiles/QTextCursor.dir/depend.make.mark:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --magenta --bold "Scanning dependencies of target QTextCursor"
	cd /home/iax/projects/editor/src/src && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/iax/projects/editor/src/src /home/iax/projects/editor/src/src /home/iax/projects/editor/src/src /home/iax/projects/editor/src/src /home/iax/projects/editor/src/src/CMakeFiles/QTextCursor.dir/DependInfo.cmake

# Object files for target QTextCursor
QTextCursor_OBJECTS = \
"CMakeFiles/QTextCursor.dir/lqt_bind_QTextCursor.o"

# External object files for target QTextCursor
QTextCursor_EXTERNAL_OBJECTS =

libQTextCursor.so: CMakeFiles/QTextCursor.dir/lqt_bind_QTextCursor.o
libQTextCursor.so: /opt/qt4/lib/libQtGui.so
libQTextCursor.so: /usr/lib/libpng.so
libQTextCursor.so: /usr/lib/libSM.so
libQTextCursor.so: /usr/lib/libICE.so
libQTextCursor.so: /usr/lib/libXi.so
libQTextCursor.so: /usr/lib/libXrender.so
libQTextCursor.so: /usr/lib/libXrandr.so
libQTextCursor.so: /usr/lib/libXcursor.so
libQTextCursor.so: /usr/lib/libXinerama.so
libQTextCursor.so: /usr/lib/libfreetype.so
libQTextCursor.so: /usr/lib/libfontconfig.so
libQTextCursor.so: /usr/lib/libXext.so
libQTextCursor.so: /usr/lib/libX11.so
libQTextCursor.so: /usr/lib/libm.so
libQTextCursor.so: /opt/qt4/lib/libQtCore.so
libQTextCursor.so: /usr/lib/libz.so
libQTextCursor.so: CMakeFiles/QTextCursor.dir/build.make
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX shared library libQTextCursor.so"
	$(CMAKE_COMMAND) -P CMakeFiles/QTextCursor.dir/cmake_clean_target.cmake
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/QTextCursor.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/QTextCursor.dir/build: libQTextCursor.so

CMakeFiles/QTextCursor.dir/requires: CMakeFiles/QTextCursor.dir/lqt_bind_QTextCursor.o.requires

CMakeFiles/QTextCursor.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/QTextCursor.dir/cmake_clean.cmake

