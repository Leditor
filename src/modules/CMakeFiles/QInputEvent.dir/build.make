# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.4

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canoncical targets will work.
.SUFFIXES:

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# The program to use to edit the cache.
CMAKE_EDIT_COMMAND = /usr/bin/ccmake

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/iax/projects/editor/src/src

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/iax/projects/editor/src/src

# Include any dependencies generated for this target.
include CMakeFiles/QInputEvent.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/QInputEvent.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/QInputEvent.dir/flags.make

CMakeFiles/QInputEvent.dir/depend.make.mark: CMakeFiles/QInputEvent.dir/flags.make
CMakeFiles/QInputEvent.dir/depend.make.mark: lqt_bind_QInputEvent.cpp

CMakeFiles/QInputEvent.dir/lqt_bind_QInputEvent.o: CMakeFiles/QInputEvent.dir/flags.make
CMakeFiles/QInputEvent.dir/lqt_bind_QInputEvent.o: lqt_bind_QInputEvent.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/iax/projects/editor/src/src/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/QInputEvent.dir/lqt_bind_QInputEvent.o"
	/usr/bin/c++   $(CXX_FLAGS) -ggdb -o CMakeFiles/QInputEvent.dir/lqt_bind_QInputEvent.o -c /home/iax/projects/editor/src/src/lqt_bind_QInputEvent.cpp

CMakeFiles/QInputEvent.dir/lqt_bind_QInputEvent.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/QInputEvent.dir/lqt_bind_QInputEvent.i"
	/usr/bin/c++  $(CXX_FLAGS) -ggdb -E /home/iax/projects/editor/src/src/lqt_bind_QInputEvent.cpp > CMakeFiles/QInputEvent.dir/lqt_bind_QInputEvent.i

CMakeFiles/QInputEvent.dir/lqt_bind_QInputEvent.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/QInputEvent.dir/lqt_bind_QInputEvent.s"
	/usr/bin/c++  $(CXX_FLAGS) -ggdb -S /home/iax/projects/editor/src/src/lqt_bind_QInputEvent.cpp -o CMakeFiles/QInputEvent.dir/lqt_bind_QInputEvent.s

CMakeFiles/QInputEvent.dir/lqt_bind_QInputEvent.o.requires:

CMakeFiles/QInputEvent.dir/lqt_bind_QInputEvent.o.provides: CMakeFiles/QInputEvent.dir/lqt_bind_QInputEvent.o.requires
	$(MAKE) -f CMakeFiles/QInputEvent.dir/build.make CMakeFiles/QInputEvent.dir/lqt_bind_QInputEvent.o.provides.build

CMakeFiles/QInputEvent.dir/lqt_bind_QInputEvent.o.provides.build: CMakeFiles/QInputEvent.dir/lqt_bind_QInputEvent.o

CMakeFiles/QInputEvent.dir/depend: CMakeFiles/QInputEvent.dir/depend.make.mark

CMakeFiles/QInputEvent.dir/depend.make.mark:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --magenta --bold "Scanning dependencies of target QInputEvent"
	cd /home/iax/projects/editor/src/src && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/iax/projects/editor/src/src /home/iax/projects/editor/src/src /home/iax/projects/editor/src/src /home/iax/projects/editor/src/src /home/iax/projects/editor/src/src/CMakeFiles/QInputEvent.dir/DependInfo.cmake

# Object files for target QInputEvent
QInputEvent_OBJECTS = \
"CMakeFiles/QInputEvent.dir/lqt_bind_QInputEvent.o"

# External object files for target QInputEvent
QInputEvent_EXTERNAL_OBJECTS =

libQInputEvent.so: CMakeFiles/QInputEvent.dir/lqt_bind_QInputEvent.o
libQInputEvent.so: /opt/qt4/lib/libQtGui.so
libQInputEvent.so: /usr/lib/libpng.so
libQInputEvent.so: /usr/lib/libSM.so
libQInputEvent.so: /usr/lib/libICE.so
libQInputEvent.so: /usr/lib/libXi.so
libQInputEvent.so: /usr/lib/libXrender.so
libQInputEvent.so: /usr/lib/libXrandr.so
libQInputEvent.so: /usr/lib/libXcursor.so
libQInputEvent.so: /usr/lib/libXinerama.so
libQInputEvent.so: /usr/lib/libfreetype.so
libQInputEvent.so: /usr/lib/libfontconfig.so
libQInputEvent.so: /usr/lib/libXext.so
libQInputEvent.so: /usr/lib/libX11.so
libQInputEvent.so: /usr/lib/libm.so
libQInputEvent.so: /opt/qt4/lib/libQtCore.so
libQInputEvent.so: /usr/lib/libz.so
libQInputEvent.so: CMakeFiles/QInputEvent.dir/build.make
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX shared library libQInputEvent.so"
	$(CMAKE_COMMAND) -P CMakeFiles/QInputEvent.dir/cmake_clean_target.cmake
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/QInputEvent.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/QInputEvent.dir/build: libQInputEvent.so

CMakeFiles/QInputEvent.dir/requires: CMakeFiles/QInputEvent.dir/lqt_bind_QInputEvent.o.requires

CMakeFiles/QInputEvent.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/QInputEvent.dir/cmake_clean.cmake

