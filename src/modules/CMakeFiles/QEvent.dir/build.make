# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.4

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canoncical targets will work.
.SUFFIXES:

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# The program to use to edit the cache.
CMAKE_EDIT_COMMAND = /usr/bin/ccmake

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/iax/projects/editor/src/src

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/iax/projects/editor/src/src

# Include any dependencies generated for this target.
include CMakeFiles/QEvent.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/QEvent.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/QEvent.dir/flags.make

CMakeFiles/QEvent.dir/depend.make.mark: CMakeFiles/QEvent.dir/flags.make
CMakeFiles/QEvent.dir/depend.make.mark: lqt_bind_QEvent.cpp

CMakeFiles/QEvent.dir/lqt_bind_QEvent.o: CMakeFiles/QEvent.dir/flags.make
CMakeFiles/QEvent.dir/lqt_bind_QEvent.o: lqt_bind_QEvent.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/iax/projects/editor/src/src/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/QEvent.dir/lqt_bind_QEvent.o"
	/usr/bin/c++   $(CXX_FLAGS) -ggdb -o CMakeFiles/QEvent.dir/lqt_bind_QEvent.o -c /home/iax/projects/editor/src/src/lqt_bind_QEvent.cpp

CMakeFiles/QEvent.dir/lqt_bind_QEvent.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/QEvent.dir/lqt_bind_QEvent.i"
	/usr/bin/c++  $(CXX_FLAGS) -ggdb -E /home/iax/projects/editor/src/src/lqt_bind_QEvent.cpp > CMakeFiles/QEvent.dir/lqt_bind_QEvent.i

CMakeFiles/QEvent.dir/lqt_bind_QEvent.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/QEvent.dir/lqt_bind_QEvent.s"
	/usr/bin/c++  $(CXX_FLAGS) -ggdb -S /home/iax/projects/editor/src/src/lqt_bind_QEvent.cpp -o CMakeFiles/QEvent.dir/lqt_bind_QEvent.s

CMakeFiles/QEvent.dir/lqt_bind_QEvent.o.requires:

CMakeFiles/QEvent.dir/lqt_bind_QEvent.o.provides: CMakeFiles/QEvent.dir/lqt_bind_QEvent.o.requires
	$(MAKE) -f CMakeFiles/QEvent.dir/build.make CMakeFiles/QEvent.dir/lqt_bind_QEvent.o.provides.build

CMakeFiles/QEvent.dir/lqt_bind_QEvent.o.provides.build: CMakeFiles/QEvent.dir/lqt_bind_QEvent.o

CMakeFiles/QEvent.dir/depend: CMakeFiles/QEvent.dir/depend.make.mark

CMakeFiles/QEvent.dir/depend.make.mark:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --magenta --bold "Scanning dependencies of target QEvent"
	cd /home/iax/projects/editor/src/src && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/iax/projects/editor/src/src /home/iax/projects/editor/src/src /home/iax/projects/editor/src/src /home/iax/projects/editor/src/src /home/iax/projects/editor/src/src/CMakeFiles/QEvent.dir/DependInfo.cmake

# Object files for target QEvent
QEvent_OBJECTS = \
"CMakeFiles/QEvent.dir/lqt_bind_QEvent.o"

# External object files for target QEvent
QEvent_EXTERNAL_OBJECTS =

libQEvent.so: CMakeFiles/QEvent.dir/lqt_bind_QEvent.o
libQEvent.so: /opt/qt4/lib/libQtGui.so
libQEvent.so: /usr/lib/libpng.so
libQEvent.so: /usr/lib/libSM.so
libQEvent.so: /usr/lib/libICE.so
libQEvent.so: /usr/lib/libXi.so
libQEvent.so: /usr/lib/libXrender.so
libQEvent.so: /usr/lib/libXrandr.so
libQEvent.so: /usr/lib/libXcursor.so
libQEvent.so: /usr/lib/libXinerama.so
libQEvent.so: /usr/lib/libfreetype.so
libQEvent.so: /usr/lib/libfontconfig.so
libQEvent.so: /usr/lib/libXext.so
libQEvent.so: /usr/lib/libX11.so
libQEvent.so: /usr/lib/libm.so
libQEvent.so: /opt/qt4/lib/libQtCore.so
libQEvent.so: /usr/lib/libz.so
libQEvent.so: CMakeFiles/QEvent.dir/build.make
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX shared library libQEvent.so"
	$(CMAKE_COMMAND) -P CMakeFiles/QEvent.dir/cmake_clean_target.cmake
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/QEvent.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/QEvent.dir/build: libQEvent.so

CMakeFiles/QEvent.dir/requires: CMakeFiles/QEvent.dir/lqt_bind_QEvent.o.requires

CMakeFiles/QEvent.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/QEvent.dir/cmake_clean.cmake

