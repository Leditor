# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 2.4

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canoncical targets will work.
.SUFFIXES:

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# The program to use to edit the cache.
CMAKE_EDIT_COMMAND = /usr/bin/ccmake

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/iax/projects/editor/src/src

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/iax/projects/editor/src/src

# Include any dependencies generated for this target.
include CMakeFiles/QTextBlockUserData.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/QTextBlockUserData.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/QTextBlockUserData.dir/flags.make

CMakeFiles/QTextBlockUserData.dir/depend.make.mark: CMakeFiles/QTextBlockUserData.dir/flags.make
CMakeFiles/QTextBlockUserData.dir/depend.make.mark: lqt_bind_QTextBlockUserData.cpp

CMakeFiles/QTextBlockUserData.dir/lqt_bind_QTextBlockUserData.o: CMakeFiles/QTextBlockUserData.dir/flags.make
CMakeFiles/QTextBlockUserData.dir/lqt_bind_QTextBlockUserData.o: lqt_bind_QTextBlockUserData.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/iax/projects/editor/src/src/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object CMakeFiles/QTextBlockUserData.dir/lqt_bind_QTextBlockUserData.o"
	/usr/bin/c++   $(CXX_FLAGS) -ggdb -o CMakeFiles/QTextBlockUserData.dir/lqt_bind_QTextBlockUserData.o -c /home/iax/projects/editor/src/src/lqt_bind_QTextBlockUserData.cpp

CMakeFiles/QTextBlockUserData.dir/lqt_bind_QTextBlockUserData.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/QTextBlockUserData.dir/lqt_bind_QTextBlockUserData.i"
	/usr/bin/c++  $(CXX_FLAGS) -ggdb -E /home/iax/projects/editor/src/src/lqt_bind_QTextBlockUserData.cpp > CMakeFiles/QTextBlockUserData.dir/lqt_bind_QTextBlockUserData.i

CMakeFiles/QTextBlockUserData.dir/lqt_bind_QTextBlockUserData.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/QTextBlockUserData.dir/lqt_bind_QTextBlockUserData.s"
	/usr/bin/c++  $(CXX_FLAGS) -ggdb -S /home/iax/projects/editor/src/src/lqt_bind_QTextBlockUserData.cpp -o CMakeFiles/QTextBlockUserData.dir/lqt_bind_QTextBlockUserData.s

CMakeFiles/QTextBlockUserData.dir/lqt_bind_QTextBlockUserData.o.requires:

CMakeFiles/QTextBlockUserData.dir/lqt_bind_QTextBlockUserData.o.provides: CMakeFiles/QTextBlockUserData.dir/lqt_bind_QTextBlockUserData.o.requires
	$(MAKE) -f CMakeFiles/QTextBlockUserData.dir/build.make CMakeFiles/QTextBlockUserData.dir/lqt_bind_QTextBlockUserData.o.provides.build

CMakeFiles/QTextBlockUserData.dir/lqt_bind_QTextBlockUserData.o.provides.build: CMakeFiles/QTextBlockUserData.dir/lqt_bind_QTextBlockUserData.o

CMakeFiles/QTextBlockUserData.dir/depend: CMakeFiles/QTextBlockUserData.dir/depend.make.mark

CMakeFiles/QTextBlockUserData.dir/depend.make.mark:
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --magenta --bold "Scanning dependencies of target QTextBlockUserData"
	cd /home/iax/projects/editor/src/src && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/iax/projects/editor/src/src /home/iax/projects/editor/src/src /home/iax/projects/editor/src/src /home/iax/projects/editor/src/src /home/iax/projects/editor/src/src/CMakeFiles/QTextBlockUserData.dir/DependInfo.cmake

# Object files for target QTextBlockUserData
QTextBlockUserData_OBJECTS = \
"CMakeFiles/QTextBlockUserData.dir/lqt_bind_QTextBlockUserData.o"

# External object files for target QTextBlockUserData
QTextBlockUserData_EXTERNAL_OBJECTS =

libQTextBlockUserData.so: CMakeFiles/QTextBlockUserData.dir/lqt_bind_QTextBlockUserData.o
libQTextBlockUserData.so: /opt/qt4/lib/libQtGui.so
libQTextBlockUserData.so: /usr/lib/libpng.so
libQTextBlockUserData.so: /usr/lib/libSM.so
libQTextBlockUserData.so: /usr/lib/libICE.so
libQTextBlockUserData.so: /usr/lib/libXi.so
libQTextBlockUserData.so: /usr/lib/libXrender.so
libQTextBlockUserData.so: /usr/lib/libXrandr.so
libQTextBlockUserData.so: /usr/lib/libXcursor.so
libQTextBlockUserData.so: /usr/lib/libXinerama.so
libQTextBlockUserData.so: /usr/lib/libfreetype.so
libQTextBlockUserData.so: /usr/lib/libfontconfig.so
libQTextBlockUserData.so: /usr/lib/libXext.so
libQTextBlockUserData.so: /usr/lib/libX11.so
libQTextBlockUserData.so: /usr/lib/libm.so
libQTextBlockUserData.so: /opt/qt4/lib/libQtCore.so
libQTextBlockUserData.so: /usr/lib/libz.so
libQTextBlockUserData.so: CMakeFiles/QTextBlockUserData.dir/build.make
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX shared library libQTextBlockUserData.so"
	$(CMAKE_COMMAND) -P CMakeFiles/QTextBlockUserData.dir/cmake_clean_target.cmake
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/QTextBlockUserData.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/QTextBlockUserData.dir/build: libQTextBlockUserData.so

CMakeFiles/QTextBlockUserData.dir/requires: CMakeFiles/QTextBlockUserData.dir/lqt_bind_QTextBlockUserData.o.requires

CMakeFiles/QTextBlockUserData.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/QTextBlockUserData.dir/cmake_clean.cmake

