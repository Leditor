#include "lqt_common.hpp"
#include <QGraphicsView>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QGraphicsView > : public QGraphicsView {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__render (lua_State *L);
  static int __LuaWrapCall__setOptimizationFlag (lua_State *L);
  static int __LuaWrapCall__sizeHint (lua_State *L);
  static int __LuaWrapCall__setMatrix (lua_State *L);
  static int __LuaWrapCall__setInteractive (lua_State *L);
  static int __LuaWrapCall__sceneRect (lua_State *L);
  static int __LuaWrapCall__viewportTransform (lua_State *L);
  static int __LuaWrapCall__setOptimizationFlags (lua_State *L);
  static int __LuaWrapCall__setTransformationAnchor (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__trUtf8 (lua_State *L);
  static int __LuaWrapCall__optimizationFlags (lua_State *L);
  static int __LuaWrapCall__rubberBandSelectionMode (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__tr (lua_State *L);
  static int __LuaWrapCall__renderHints (lua_State *L);
  static int __LuaWrapCall__setCacheMode (lua_State *L);
  static int __LuaWrapCall__delete (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__new (lua_State *L);
  static int __LuaWrapCall__setRenderHints (lua_State *L);
  static int __LuaWrapCall__updateScene (lua_State *L);
  static int __LuaWrapCall__setRenderHint (lua_State *L);
  static int __LuaWrapCall__items__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__items__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__items__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__items__OverloadedVersion__4 (lua_State *L);
  static int __LuaWrapCall__items__OverloadedVersion__5 (lua_State *L);
  static int __LuaWrapCall__items__OverloadedVersion__6 (lua_State *L);
  static int __LuaWrapCall__items__OverloadedVersion__7 (lua_State *L);
  static int __LuaWrapCall__items (lua_State *L);
  static int __LuaWrapCall__invalidateScene (lua_State *L);
  static int __LuaWrapCall__matrix (lua_State *L);
  static int __LuaWrapCall__transformationAnchor (lua_State *L);
  static int __LuaWrapCall__centerOn__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__centerOn__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__centerOn__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__centerOn (lua_State *L);
  static int __LuaWrapCall__rotate (lua_State *L);
  static int __LuaWrapCall__setScene (lua_State *L);
  static int __LuaWrapCall__transform (lua_State *L);
  static int __LuaWrapCall__resetMatrix (lua_State *L);
  static int __LuaWrapCall__setRubberBandSelectionMode (lua_State *L);
  static int __LuaWrapCall__setTransform (lua_State *L);
  static int __LuaWrapCall__setSceneRect__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__setSceneRect__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__setSceneRect (lua_State *L);
  static int __LuaWrapCall__scale (lua_State *L);
  static int __LuaWrapCall__resizeAnchor (lua_State *L);
  static int __LuaWrapCall__dragMode (lua_State *L);
  static int __LuaWrapCall__shear (lua_State *L);
  static int __LuaWrapCall__fitInView__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__fitInView__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__fitInView__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__fitInView (lua_State *L);
  static int __LuaWrapCall__isInteractive (lua_State *L);
  static int __LuaWrapCall__setResizeAnchor (lua_State *L);
  static int __LuaWrapCall__translate (lua_State *L);
  static int __LuaWrapCall__ensureVisible__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__ensureVisible__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__ensureVisible__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__ensureVisible (lua_State *L);
  static int __LuaWrapCall__updateSceneRect (lua_State *L);
  static int __LuaWrapCall__backgroundBrush (lua_State *L);
  static int __LuaWrapCall__cacheMode (lua_State *L);
  static int __LuaWrapCall__inputMethodQuery (lua_State *L);
  static int __LuaWrapCall__setViewportUpdateMode (lua_State *L);
  static int __LuaWrapCall__mapToScene__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__mapToScene__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__mapToScene__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__mapToScene__OverloadedVersion__4 (lua_State *L);
  static int __LuaWrapCall__mapToScene__OverloadedVersion__5 (lua_State *L);
  static int __LuaWrapCall__mapToScene__OverloadedVersion__6 (lua_State *L);
  static int __LuaWrapCall__mapToScene (lua_State *L);
  static int __LuaWrapCall__foregroundBrush (lua_State *L);
  static int __LuaWrapCall__resetTransform (lua_State *L);
  static int __LuaWrapCall__scene (lua_State *L);
  static int __LuaWrapCall__metaObject (lua_State *L);
  static int __LuaWrapCall__resetCachedContent (lua_State *L);
  static int __LuaWrapCall__setForegroundBrush (lua_State *L);
  static int __LuaWrapCall__itemAt__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__itemAt__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__itemAt (lua_State *L);
  static int __LuaWrapCall__alignment (lua_State *L);
  static int __LuaWrapCall__setDragMode (lua_State *L);
  static int __LuaWrapCall__viewportUpdateMode (lua_State *L);
  static int __LuaWrapCall__setBackgroundBrush (lua_State *L);
  static int __LuaWrapCall__mapFromScene__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__mapFromScene__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__mapFromScene__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__mapFromScene__OverloadedVersion__4 (lua_State *L);
  static int __LuaWrapCall__mapFromScene__OverloadedVersion__5 (lua_State *L);
  static int __LuaWrapCall__mapFromScene__OverloadedVersion__6 (lua_State *L);
  static int __LuaWrapCall__mapFromScene (lua_State *L);
  static int __LuaWrapCall__setAlignment (lua_State *L);
protected:
  void styleChange (QStyle& arg1);
public:
protected:
  void mouseReleaseEvent (QMouseEvent * arg1);
public:
protected:
  void keyPressEvent (QKeyEvent * arg1);
public:
protected:
  void contextMenuEvent (QContextMenuEvent * arg1);
public:
  int devType () const;
protected:
  void drawBackground (QPainter * arg1, const QRectF& arg2);
public:
  void setVisible (bool arg1);
protected:
  void keyReleaseEvent (QKeyEvent * arg1);
public:
protected:
  void scrollContentsBy (int arg1, int arg2);
public:
protected:
  void dragLeaveEvent (QDragLeaveEvent * arg1);
public:
protected:
  void mousePressEvent (QMouseEvent * arg1);
public:
  QSize sizeHint () const;
protected:
  void customEvent (QEvent * arg1);
public:
protected:
  void closeEvent (QCloseEvent * arg1);
public:
protected:
  void wheelEvent (QWheelEvent * arg1);
public:
  QPaintEngine * paintEngine () const;
protected:
  void tabletEvent (QTabletEvent * arg1);
public:
protected:
  void focusInEvent (QFocusEvent * arg1);
public:
protected:
  void dragMoveEvent (QDragMoveEvent * arg1);
public:
protected:
  void moveEvent (QMoveEvent * arg1);
public:
protected:
  void dropEvent (QDropEvent * arg1);
public:
protected:
  void fontChange (const QFont& arg1);
public:
protected:
  void timerEvent (QTimerEvent * arg1);
public:
protected:
  void showEvent (QShowEvent * arg1);
public:
protected:
  void connectNotify (const char * arg1);
public:
protected:
  void paintEvent (QPaintEvent * arg1);
public:
protected:
  void childEvent (QChildEvent * arg1);
public:
protected:
  void windowActivationChange (bool arg1);
public:
protected:
  void enabledChange (bool arg1);
public:
protected:
  int metric (QPaintDevice::PaintDeviceMetric arg1) const;
public:
  int heightForWidth (int arg1) const;
protected:
  void mouseDoubleClickEvent (QMouseEvent * arg1);
public:
protected:
  void actionEvent (QActionEvent * arg1);
public:
protected:
  void disconnectNotify (const char * arg1);
public:
protected:
  void mouseMoveEvent (QMouseEvent * arg1);
public:
protected:
  void changeEvent (QEvent * arg1);
public:
protected:
  void drawForeground (QPainter * arg1, const QRectF& arg2);
public:
protected:
  void focusOutEvent (QFocusEvent * arg1);
public:
protected:
  void drawItems (QPainter * arg1, int arg2, QGraphicsItem * * arg3, const QStyleOptionGraphicsItem * arg4);
public:
  QSize minimumSizeHint () const;
  bool eventFilter (QObject * arg1, QEvent * arg2);
protected:
  void inputMethodEvent (QInputMethodEvent * arg1);
public:
protected:
  bool viewportEvent (QEvent * arg1);
public:
protected:
  void resizeEvent (QResizeEvent * arg1);
public:
protected:
  void paletteChange (const QPalette& arg1);
public:
protected:
  void languageChange ();
public:
protected:
  bool event (QEvent * arg1);
public:
protected:
  void hideEvent (QHideEvent * arg1);
public:
protected:
  bool focusNextPrevChild (bool arg1);
public:
  const QMetaObject * metaObject () const;
  QVariant inputMethodQuery (Qt::InputMethodQuery arg1) const;
protected:
  void leaveEvent (QEvent * arg1);
public:
protected:
  void dragEnterEvent (QDragEnterEvent * arg1);
public:
protected:
  void enterEvent (QEvent * arg1);
public:
  ~LuaBinder< QGraphicsView > ();
  static int lqt_pushenum_ViewportAnchor (lua_State *L);
  static int lqt_pushenum_ViewportAnchor_QFLAGS_CREATOR (lua_State *L);
  static int lqt_pushenum_CacheModeFlag (lua_State *L);
  static int lqt_pushenum_CacheModeFlag_QFLAGS_CREATOR (lua_State *L);
  static int lqt_pushenum_DragMode (lua_State *L);
  static int lqt_pushenum_DragMode_QFLAGS_CREATOR (lua_State *L);
  static int lqt_pushenum_ViewportUpdateMode (lua_State *L);
  static int lqt_pushenum_ViewportUpdateMode_QFLAGS_CREATOR (lua_State *L);
  static int lqt_pushenum_OptimizationFlag (lua_State *L);
  static int lqt_pushenum_OptimizationFlag_QFLAGS_CREATOR (lua_State *L);
  LuaBinder< QGraphicsView > (lua_State *l, QWidget * arg1):QGraphicsView(arg1), L(l) {}
  LuaBinder< QGraphicsView > (lua_State *l, QGraphicsScene * arg1, QWidget * arg2):QGraphicsView(arg1, arg2), L(l) {}
};

extern "C" int luaopen_QGraphicsView (lua_State *L);
