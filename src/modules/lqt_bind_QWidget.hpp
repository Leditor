#include "lqt_common.hpp"
#include <QWidget>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QWidget > : public QWidget {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__windowFlags (lua_State *L);
  static int __LuaWrapCall__setShown (lua_State *L);
  static int __LuaWrapCall__minimumSize (lua_State *L);
  static int __LuaWrapCall__showMinimized (lua_State *L);
  static int __LuaWrapCall__statusTip (lua_State *L);
  static int __LuaWrapCall__styleSheet (lua_State *L);
  static int __LuaWrapCall__childrenRegion (lua_State *L);
  static int __LuaWrapCall__mapTo (lua_State *L);
  static int __LuaWrapCall__setStyle (lua_State *L);
  static int __LuaWrapCall__size (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__tr (lua_State *L);
  static int __LuaWrapCall__isVisible (lua_State *L);
  static int __LuaWrapCall__whatsThis (lua_State *L);
  static int __LuaWrapCall__layout (lua_State *L);
  static int __LuaWrapCall__palette (lua_State *L);
  static int __LuaWrapCall__font (lua_State *L);
  static int __LuaWrapCall__adjustSize (lua_State *L);
  static int __LuaWrapCall__setAcceptDrops (lua_State *L);
  static int __LuaWrapCall__unsetLocale (lua_State *L);
  static int __LuaWrapCall__pos (lua_State *L);
  static int __LuaWrapCall__setGeometry__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__setGeometry__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__setGeometry (lua_State *L);
  static int __LuaWrapCall__windowIconText (lua_State *L);
  static int __LuaWrapCall__isRightToLeft (lua_State *L);
  static int __LuaWrapCall__mapFrom (lua_State *L);
  static int __LuaWrapCall__inputContext (lua_State *L);
  static int __LuaWrapCall__windowState (lua_State *L);
  static int __LuaWrapCall__minimumWidth (lua_State *L);
  static int __LuaWrapCall__addAction (lua_State *L);
  static int __LuaWrapCall__isHidden (lua_State *L);
  static int __LuaWrapCall__delete (lua_State *L);
  static int __LuaWrapCall__setWindowTitle (lua_State *L);
  static int __LuaWrapCall__setStyleSheet (lua_State *L);
  static int __LuaWrapCall__releaseShortcut (lua_State *L);
  static int __LuaWrapCall__setWhatsThis (lua_State *L);
  static int __LuaWrapCall__isEnabled (lua_State *L);
  static int __LuaWrapCall__addActions (lua_State *L);
  static int __LuaWrapCall__setWindowModified (lua_State *L);
  static int __LuaWrapCall__setWindowIconText (lua_State *L);
  static int __LuaWrapCall__setWindowRole (lua_State *L);
  static int __LuaWrapCall__scroll__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__scroll__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__scroll (lua_State *L);
  static int __LuaWrapCall__cursor (lua_State *L);
  static int __LuaWrapCall__setFixedHeight (lua_State *L);
  static int __LuaWrapCall__setShortcutAutoRepeat (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__trUtf8 (lua_State *L);
  static int __LuaWrapCall__foregroundRole (lua_State *L);
  static int __LuaWrapCall__mapToGlobal (lua_State *L);
  static int __LuaWrapCall__releaseMouse (lua_State *L);
  static int __LuaWrapCall__isEnabledTo (lua_State *L);
  static int __LuaWrapCall__isLeftToRight (lua_State *L);
  static int __LuaWrapCall__contextMenuPolicy (lua_State *L);
  static int __LuaWrapCall__updatesEnabled (lua_State *L);
  static int __LuaWrapCall__isAncestorOf (lua_State *L);
  static int __LuaWrapCall__normalGeometry (lua_State *L);
  static int __LuaWrapCall__nextInFocusChain (lua_State *L);
  static int __LuaWrapCall__sizePolicy (lua_State *L);
  static int __LuaWrapCall__overrideWindowState (lua_State *L);
  static int __LuaWrapCall__setMinimumSize__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__setMinimumSize__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__setMinimumSize (lua_State *L);
  static int __LuaWrapCall__testAttribute (lua_State *L);
  static int __LuaWrapCall__stackUnder (lua_State *L);
  static int __LuaWrapCall__grabShortcut (lua_State *L);
  static int __LuaWrapCall__setMaximumSize__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__setMaximumSize__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__setMaximumSize (lua_State *L);
  static int __LuaWrapCall__setBackgroundRole (lua_State *L);
  static int __LuaWrapCall__locale (lua_State *L);
  static int __LuaWrapCall__mouseGrabber (lua_State *L);
  static int __LuaWrapCall__frameSize (lua_State *L);
  static int __LuaWrapCall__setInputContext (lua_State *L);
  static int __LuaWrapCall__repaint__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__repaint__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__repaint__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__repaint__OverloadedVersion__4 (lua_State *L);
  static int __LuaWrapCall__repaint (lua_State *L);
  static int __LuaWrapCall__unsetCursor (lua_State *L);
  static int __LuaWrapCall__winId (lua_State *L);
  static int __LuaWrapCall__isWindowModified (lua_State *L);
  static int __LuaWrapCall__acceptDrops (lua_State *L);
  static int __LuaWrapCall__setFont (lua_State *L);
  static int __LuaWrapCall__setWindowIcon (lua_State *L);
  static int __LuaWrapCall__setParent__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__setParent__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__setParent (lua_State *L);
  static int __LuaWrapCall__frameGeometry (lua_State *L);
  static int __LuaWrapCall__setLayoutDirection (lua_State *L);
  static int __LuaWrapCall__setContentsMargins (lua_State *L);
  static int __LuaWrapCall__new (lua_State *L);
  static int __LuaWrapCall__mapToParent (lua_State *L);
  static int __LuaWrapCall__render (lua_State *L);
  static int __LuaWrapCall__baseSize (lua_State *L);
  static int __LuaWrapCall__windowType (lua_State *L);
  static int __LuaWrapCall__setBaseSize__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__setBaseSize__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__setBaseSize (lua_State *L);
  static int __LuaWrapCall__find (lua_State *L);
  static int __LuaWrapCall__mask (lua_State *L);
  static int __LuaWrapCall__isModal (lua_State *L);
  static int __LuaWrapCall__maximumWidth (lua_State *L);
  static int __LuaWrapCall__windowModality (lua_State *L);
  static int __LuaWrapCall__setVisible (lua_State *L);
  static int __LuaWrapCall__setEnabled (lua_State *L);
  static int __LuaWrapCall__topLevelWidget (lua_State *L);
  static int __LuaWrapCall__resize__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__resize__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__resize (lua_State *L);
  static int __LuaWrapCall__hasMouseTracking (lua_State *L);
  static int __LuaWrapCall__actions (lua_State *L);
  static int __LuaWrapCall__isEnabledToTLW (lua_State *L);
  static int __LuaWrapCall__setFixedSize__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__setFixedSize__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__setFixedSize (lua_State *L);
  static int __LuaWrapCall__backgroundRole (lua_State *L);
  static int __LuaWrapCall__setContextMenuPolicy (lua_State *L);
  static int __LuaWrapCall__createWinId (lua_State *L);
  static int __LuaWrapCall__windowSurface (lua_State *L);
  static int __LuaWrapCall__maximumSize (lua_State *L);
  static int __LuaWrapCall__isMaximized (lua_State *L);
  static int __LuaWrapCall__releaseKeyboard (lua_State *L);
  static int __LuaWrapCall__windowOpacity (lua_State *L);
  static int __LuaWrapCall__isVisibleTo (lua_State *L);
  static int __LuaWrapCall__style (lua_State *L);
  static int __LuaWrapCall__contentsRect (lua_State *L);
  static int __LuaWrapCall__clearFocus (lua_State *L);
  static int __LuaWrapCall__insertActions (lua_State *L);
  static int __LuaWrapCall__setShortcutEnabled (lua_State *L);
  static int __LuaWrapCall__childrenRect (lua_State *L);
  static int __LuaWrapCall__hide (lua_State *L);
  static int __LuaWrapCall__grabKeyboard (lua_State *L);
  static int __LuaWrapCall__setWindowState (lua_State *L);
  static int __LuaWrapCall__setMaximumHeight (lua_State *L);
  static int __LuaWrapCall__clearMask (lua_State *L);
  static int __LuaWrapCall__setFocusPolicy (lua_State *L);
  static int __LuaWrapCall__hasFocus (lua_State *L);
  static int __LuaWrapCall__removeAction (lua_State *L);
  static int __LuaWrapCall__setStatusTip (lua_State *L);
  static int __LuaWrapCall__setFocus__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__setFocus__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__setFocus (lua_State *L);
  static int __LuaWrapCall__fontMetrics (lua_State *L);
  static int __LuaWrapCall__setTabOrder (lua_State *L);
  static int __LuaWrapCall__windowRole (lua_State *L);
  static int __LuaWrapCall__underMouse (lua_State *L);
  static int __LuaWrapCall__metaObject (lua_State *L);
  static int __LuaWrapCall__sizeHint (lua_State *L);
  static int __LuaWrapCall__inputMethodQuery (lua_State *L);
  static int __LuaWrapCall__grabMouse__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__grabMouse__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__grabMouse (lua_State *L);
  static int __LuaWrapCall__setMaximumWidth (lua_State *L);
  static int __LuaWrapCall__updateGeometry (lua_State *L);
  static int __LuaWrapCall__setMinimumWidth (lua_State *L);
  static int __LuaWrapCall__setToolTip (lua_State *L);
  static int __LuaWrapCall__setSizeIncrement__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__setSizeIncrement__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__setSizeIncrement (lua_State *L);
  static int __LuaWrapCall__setFocusProxy (lua_State *L);
  static int __LuaWrapCall__show (lua_State *L);
  static int __LuaWrapCall__y (lua_State *L);
  static int __LuaWrapCall__focusWidget (lua_State *L);
  static int __LuaWrapCall__focusPolicy (lua_State *L);
  static int __LuaWrapCall__focusProxy (lua_State *L);
  static int __LuaWrapCall__childAt__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__childAt__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__childAt (lua_State *L);
  static int __LuaWrapCall__geometry (lua_State *L);
  static int __LuaWrapCall__setLayout (lua_State *L);
  static int __LuaWrapCall__setAutoFillBackground (lua_State *L);
  static int __LuaWrapCall__setFixedWidth (lua_State *L);
  static int __LuaWrapCall__close (lua_State *L);
  static int __LuaWrapCall__parentWidget (lua_State *L);
  static int __LuaWrapCall__isTopLevel (lua_State *L);
  static int __LuaWrapCall__setWindowSurface (lua_State *L);
  static int __LuaWrapCall__update__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__update__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__update__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__update__OverloadedVersion__4 (lua_State *L);
  static int __LuaWrapCall__update (lua_State *L);
  static int __LuaWrapCall__isWindow (lua_State *L);
  static int __LuaWrapCall__internalWinId (lua_State *L);
  static int __LuaWrapCall__setCursor (lua_State *L);
  static int __LuaWrapCall__keyboardGrabber (lua_State *L);
  static int __LuaWrapCall__x (lua_State *L);
  static int __LuaWrapCall__ensurePolished (lua_State *L);
  static int __LuaWrapCall__setMouseTracking (lua_State *L);
  static int __LuaWrapCall__setAttribute (lua_State *L);
  static int __LuaWrapCall__window (lua_State *L);
  static int __LuaWrapCall__setForegroundRole (lua_State *L);
  static int __LuaWrapCall__setDisabled (lua_State *L);
  static int __LuaWrapCall__mapFromParent (lua_State *L);
  static int __LuaWrapCall__setHidden (lua_State *L);
  static int __LuaWrapCall__setPalette (lua_State *L);
  static int __LuaWrapCall__setUpdatesEnabled (lua_State *L);
  static int __LuaWrapCall__mapFromGlobal (lua_State *L);
  static int __LuaWrapCall__isFullScreen (lua_State *L);
  static int __LuaWrapCall__layoutDirection (lua_State *L);
  static int __LuaWrapCall__rect (lua_State *L);
  static int __LuaWrapCall__activateWindow (lua_State *L);
  static int __LuaWrapCall__showMaximized (lua_State *L);
  static int __LuaWrapCall__height (lua_State *L);
  static int __LuaWrapCall__showFullScreen (lua_State *L);
  static int __LuaWrapCall__minimumHeight (lua_State *L);
  static int __LuaWrapCall__insertAction (lua_State *L);
  static int __LuaWrapCall__devType (lua_State *L);
  static int __LuaWrapCall__setMask__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__setMask__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__setMask (lua_State *L);
  static int __LuaWrapCall__setWindowOpacity (lua_State *L);
  static int __LuaWrapCall__move__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__move__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__move (lua_State *L);
  static int __LuaWrapCall__restoreGeometry (lua_State *L);
  static int __LuaWrapCall__maximumHeight (lua_State *L);
  static int __LuaWrapCall__setWindowFlags (lua_State *L);
  static int __LuaWrapCall__paintEngine (lua_State *L);
  static int __LuaWrapCall__isMinimized (lua_State *L);
  static int __LuaWrapCall__setMinimumHeight (lua_State *L);
  static int __LuaWrapCall__setSizePolicy__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__setSizePolicy__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__setSizePolicy (lua_State *L);
  static int __LuaWrapCall__heightForWidth (lua_State *L);
  static int __LuaWrapCall__visibleRegion (lua_State *L);
  static int __LuaWrapCall__getContentsMargins (lua_State *L);
  static int __LuaWrapCall__setWindowModality (lua_State *L);
  static int __LuaWrapCall__lower (lua_State *L);
  static int __LuaWrapCall__windowTitle (lua_State *L);
  static int __LuaWrapCall__sizeIncrement (lua_State *L);
  static int __LuaWrapCall__showNormal (lua_State *L);
  static int __LuaWrapCall__unsetLayoutDirection (lua_State *L);
  static int __LuaWrapCall__autoFillBackground (lua_State *L);
  static int __LuaWrapCall__isActiveWindow (lua_State *L);
  static int __LuaWrapCall__minimumSizeHint (lua_State *L);
  static int __LuaWrapCall__width (lua_State *L);
  static int __LuaWrapCall__setLocale (lua_State *L);
  static int __LuaWrapCall__saveGeometry (lua_State *L);
  static int __LuaWrapCall__overrideWindowFlags (lua_State *L);
  static int __LuaWrapCall__toolTip (lua_State *L);
  static int __LuaWrapCall__raise (lua_State *L);
  static int __LuaWrapCall__windowIcon (lua_State *L);
  static int __LuaWrapCall__fontInfo (lua_State *L);
  static int __LuaWrapCall__handle (lua_State *L);
protected:
  void styleChange (QStyle& arg1);
public:
protected:
  void focusInEvent (QFocusEvent * arg1);
public:
protected:
  void keyPressEvent (QKeyEvent * arg1);
public:
protected:
  void contextMenuEvent (QContextMenuEvent * arg1);
public:
  int devType () const;
  void setVisible (bool arg1);
protected:
  void keyReleaseEvent (QKeyEvent * arg1);
public:
  QPaintEngine * paintEngine () const;
protected:
  void dragLeaveEvent (QDragLeaveEvent * arg1);
public:
protected:
  void mousePressEvent (QMouseEvent * arg1);
public:
  QSize sizeHint () const;
protected:
  void moveEvent (QMoveEvent * arg1);
public:
protected:
  void tabletEvent (QTabletEvent * arg1);
public:
protected:
  void connectNotify (const char * arg1);
public:
  QVariant inputMethodQuery (Qt::InputMethodQuery arg1) const;
  QSize minimumSizeHint () const;
protected:
  void timerEvent (QTimerEvent * arg1);
public:
protected:
  void dropEvent (QDropEvent * arg1);
public:
protected:
  void showEvent (QShowEvent * arg1);
public:
protected:
  void changeEvent (QEvent * arg1);
public:
protected:
  void paintEvent (QPaintEvent * arg1);
public:
protected:
  void customEvent (QEvent * arg1);
public:
  const QMetaObject * metaObject () const;
protected:
  void enabledChange (bool arg1);
public:
protected:
  void windowActivationChange (bool arg1);
public:
  int heightForWidth (int arg1) const;
protected:
  void mouseDoubleClickEvent (QMouseEvent * arg1);
public:
protected:
  void fontChange (const QFont& arg1);
public:
protected:
  void disconnectNotify (const char * arg1);
public:
protected:
  void mouseMoveEvent (QMouseEvent * arg1);
public:
protected:
  bool focusNextPrevChild (bool arg1);
public:
protected:
  void childEvent (QChildEvent * arg1);
public:
protected:
  void focusOutEvent (QFocusEvent * arg1);
public:
protected:
  void mouseReleaseEvent (QMouseEvent * arg1);
public:
protected:
  int metric (QPaintDevice::PaintDeviceMetric arg1) const;
public:
  bool eventFilter (QObject * arg1, QEvent * arg2);
protected:
  void inputMethodEvent (QInputMethodEvent * arg1);
public:
protected:
  void wheelEvent (QWheelEvent * arg1);
public:
protected:
  void resizeEvent (QResizeEvent * arg1);
public:
protected:
  void paletteChange (const QPalette& arg1);
public:
protected:
  void languageChange ();
public:
protected:
  bool event (QEvent * arg1);
public:
protected:
  void hideEvent (QHideEvent * arg1);
public:
protected:
  void dragMoveEvent (QDragMoveEvent * arg1);
public:
protected:
  void dragEnterEvent (QDragEnterEvent * arg1);
public:
protected:
  void actionEvent (QActionEvent * arg1);
public:
protected:
  void leaveEvent (QEvent * arg1);
public:
protected:
  void closeEvent (QCloseEvent * arg1);
public:
protected:
  void enterEvent (QEvent * arg1);
public:
  ~LuaBinder< QWidget > ();
  static int lqt_pushenum_RenderFlag (lua_State *L);
  static int lqt_pushenum_RenderFlag_QFLAGS_CREATOR (lua_State *L);
  LuaBinder< QWidget > (lua_State *l, QWidget * arg1, QFlags<Qt::WindowType> arg2):QWidget(arg1, arg2), L(l) {}
};

extern "C" int luaopen_QWidget (lua_State *L);
