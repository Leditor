#include "lqt_bind_QKeySequence.hpp"

int LuaBinder< QKeySequence >::__LuaWrapCall__fromString (lua_State *L) {
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 1), lua_objlen(L, 1));
  QKeySequence::SequenceFormat arg2 = lqtL_isenum(L, 2, "QKeySequence::SequenceFormat")?static_cast<QKeySequence::SequenceFormat>(lqtL_toenum(L, 2, "QKeySequence::SequenceFormat")):QKeySequence::PortableText;
  QKeySequence ret = QKeySequence::fromString(arg1, arg2);
  lqtL_passudata(L, new QKeySequence(ret), "QKeySequence*");
  return 1;
}
int LuaBinder< QKeySequence >::__LuaWrapCall__isDetached (lua_State *L) {
  QKeySequence *& __lua__obj = *static_cast<QKeySequence**>(lqtL_checkudata(L, 1, "QKeySequence*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QKeySequence::isDetached();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QKeySequence >::__LuaWrapCall__toString (lua_State *L) {
  QKeySequence *& __lua__obj = *static_cast<QKeySequence**>(lqtL_checkudata(L, 1, "QKeySequence*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QKeySequence::SequenceFormat arg1 = lqtL_isenum(L, 2, "QKeySequence::SequenceFormat")?static_cast<QKeySequence::SequenceFormat>(lqtL_toenum(L, 2, "QKeySequence::SequenceFormat")):QKeySequence::PortableText;
  QString ret = __lua__obj->QKeySequence::toString(arg1);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QKeySequence >::__LuaWrapCall__delete (lua_State *L) {
  QKeySequence *& __lua__obj = *static_cast<QKeySequence**>(lqtL_checkudata(L, 1, "QKeySequence*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  delete __lua__obj;
  __lua__obj = 0;
  return 0;
}
int LuaBinder< QKeySequence >::__LuaWrapCall__count (lua_State *L) {
  QKeySequence *& __lua__obj = *static_cast<QKeySequence**>(lqtL_checkudata(L, 1, "QKeySequence*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  unsigned int ret = __lua__obj->QKeySequence::count();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QKeySequence >::__LuaWrapCall__new__OverloadedVersion__1 (lua_State *L) {
  QKeySequence * ret = new LuaBinder< QKeySequence >(L);
  lqtL_passudata(L, ret, "QKeySequence*");
  return 1;
}
int LuaBinder< QKeySequence >::__LuaWrapCall__new__OverloadedVersion__2 (lua_State *L) {
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 1), lua_objlen(L, 1));
  QKeySequence * ret = new LuaBinder< QKeySequence >(L, arg1);
  lqtL_passudata(L, ret, "QKeySequence*");
  return 1;
}
int LuaBinder< QKeySequence >::__LuaWrapCall__new__OverloadedVersion__3 (lua_State *L) {
  int arg1 = lua_tointeger(L, 1);
  int arg2 = lua_isnumber(L, 2)?lua_tointeger(L, 2):static_cast< int >(0);
  int arg3 = lua_isnumber(L, 3)?lua_tointeger(L, 3):static_cast< int >(0);
  int arg4 = lua_isnumber(L, 4)?lua_tointeger(L, 4):static_cast< int >(0);
  QKeySequence * ret = new LuaBinder< QKeySequence >(L, arg1, arg2, arg3, arg4);
  lqtL_passudata(L, ret, "QKeySequence*");
  return 1;
}
int LuaBinder< QKeySequence >::__LuaWrapCall__new__OverloadedVersion__4 (lua_State *L) {
  const QKeySequence& arg1 = **static_cast<QKeySequence**>(lqtL_checkudata(L, 1, "QKeySequence*"));
  QKeySequence * ret = new LuaBinder< QKeySequence >(L, arg1);
  lqtL_passudata(L, ret, "QKeySequence*");
  return 1;
}
int LuaBinder< QKeySequence >::__LuaWrapCall__new__OverloadedVersion__5 (lua_State *L) {
  QKeySequence::StandardKey arg1 = static_cast<QKeySequence::StandardKey>(lqtL_toenum(L, 1, "QKeySequence::StandardKey"));
  QKeySequence * ret = new LuaBinder< QKeySequence >(L, arg1);
  lqtL_passudata(L, ret, "QKeySequence*");
  return 1;
}
int LuaBinder< QKeySequence >::__LuaWrapCall__new (lua_State *L) {
  int score[6];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x14a1c40;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  if (lua_isnumber(L, 1)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x14a2670;
  } else {
    score[3] -= premium*premium;
  }
  if (lua_isnumber(L, 2)) {
    score[3] += premium;
  } else if (true) {
    score[3] += premium-1; // table: 0x14a2150;
  } else {
    score[3] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[3] += premium;
  } else if (true) {
    score[3] += premium-1; // table: 0x14a2b90;
  } else {
    score[3] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[3] += premium;
  } else if (true) {
    score[3] += premium-1; // table: 0x14a34b0;
  } else {
    score[3] -= premium*premium;
  }
  score[4] = 0;
  if (lqtL_testudata(L, 1, "QKeySequence*")) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x14a3dd0;
  } else {
    score[4] -= premium*premium;
  }
  score[5] = 0;
  if (lqtL_isenum(L, 1, "QKeySequence::StandardKey")) {
    score[5] += premium;
  } else if (false) {
    score[5] += premium-1; // table: 0x14a4860;
  } else {
    score[5] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=5;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__new__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__new__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__new__OverloadedVersion__3(L); break;
    case 4: return __LuaWrapCall__new__OverloadedVersion__4(L); break;
    case 5: return __LuaWrapCall__new__OverloadedVersion__5(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__new matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QKeySequence >::__LuaWrapCall__isEmpty (lua_State *L) {
  QKeySequence *& __lua__obj = *static_cast<QKeySequence**>(lqtL_checkudata(L, 1, "QKeySequence*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QKeySequence::isEmpty();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QKeySequence >::__LuaWrapCall__matches (lua_State *L) {
  QKeySequence *& __lua__obj = *static_cast<QKeySequence**>(lqtL_checkudata(L, 1, "QKeySequence*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QKeySequence& arg1 = **static_cast<QKeySequence**>(lqtL_checkudata(L, 2, "QKeySequence*"));
  QKeySequence::SequenceMatch ret = __lua__obj->QKeySequence::matches(arg1);
  lqtL_pushenum(L, ret, "QKeySequence::SequenceMatch");
  return 1;
}
int LuaBinder< QKeySequence >::__LuaWrapCall__mnemonic (lua_State *L) {
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 1), lua_objlen(L, 1));
  QKeySequence ret = QKeySequence::mnemonic(arg1);
  lqtL_passudata(L, new QKeySequence(ret), "QKeySequence*");
  return 1;
}
int LuaBinder< QKeySequence >::__LuaWrapCall__keyBindings (lua_State *L) {
  QKeySequence::StandardKey arg1 = static_cast<QKeySequence::StandardKey>(lqtL_toenum(L, 1, "QKeySequence::StandardKey"));
  QList<QKeySequence> ret = QKeySequence::keyBindings(arg1);
  lqtL_passudata(L, new QList<QKeySequence>(ret), "QList<QKeySequence>*");
  return 1;
}
int LuaBinder< QKeySequence >::lqt_pushenum_StandardKey (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "UnknownKey");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "UnknownKey");
  lua_pushstring(L, "HelpContents");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "HelpContents");
  lua_pushstring(L, "WhatsThis");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "WhatsThis");
  lua_pushstring(L, "Open");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "Open");
  lua_pushstring(L, "Close");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "Close");
  lua_pushstring(L, "Save");
  lua_rawseti(L, enum_table, 5);
  lua_pushinteger(L, 5);
  lua_setfield(L, enum_table, "Save");
  lua_pushstring(L, "New");
  lua_rawseti(L, enum_table, 6);
  lua_pushinteger(L, 6);
  lua_setfield(L, enum_table, "New");
  lua_pushstring(L, "Delete");
  lua_rawseti(L, enum_table, 7);
  lua_pushinteger(L, 7);
  lua_setfield(L, enum_table, "Delete");
  lua_pushstring(L, "Cut");
  lua_rawseti(L, enum_table, 8);
  lua_pushinteger(L, 8);
  lua_setfield(L, enum_table, "Cut");
  lua_pushstring(L, "Copy");
  lua_rawseti(L, enum_table, 9);
  lua_pushinteger(L, 9);
  lua_setfield(L, enum_table, "Copy");
  lua_pushstring(L, "Paste");
  lua_rawseti(L, enum_table, 10);
  lua_pushinteger(L, 10);
  lua_setfield(L, enum_table, "Paste");
  lua_pushstring(L, "Undo");
  lua_rawseti(L, enum_table, 11);
  lua_pushinteger(L, 11);
  lua_setfield(L, enum_table, "Undo");
  lua_pushstring(L, "Redo");
  lua_rawseti(L, enum_table, 12);
  lua_pushinteger(L, 12);
  lua_setfield(L, enum_table, "Redo");
  lua_pushstring(L, "Back");
  lua_rawseti(L, enum_table, 13);
  lua_pushinteger(L, 13);
  lua_setfield(L, enum_table, "Back");
  lua_pushstring(L, "Forward");
  lua_rawseti(L, enum_table, 14);
  lua_pushinteger(L, 14);
  lua_setfield(L, enum_table, "Forward");
  lua_pushstring(L, "Refresh");
  lua_rawseti(L, enum_table, 15);
  lua_pushinteger(L, 15);
  lua_setfield(L, enum_table, "Refresh");
  lua_pushstring(L, "ZoomIn");
  lua_rawseti(L, enum_table, 16);
  lua_pushinteger(L, 16);
  lua_setfield(L, enum_table, "ZoomIn");
  lua_pushstring(L, "ZoomOut");
  lua_rawseti(L, enum_table, 17);
  lua_pushinteger(L, 17);
  lua_setfield(L, enum_table, "ZoomOut");
  lua_pushstring(L, "Print");
  lua_rawseti(L, enum_table, 18);
  lua_pushinteger(L, 18);
  lua_setfield(L, enum_table, "Print");
  lua_pushstring(L, "AddTab");
  lua_rawseti(L, enum_table, 19);
  lua_pushinteger(L, 19);
  lua_setfield(L, enum_table, "AddTab");
  lua_pushstring(L, "NextChild");
  lua_rawseti(L, enum_table, 20);
  lua_pushinteger(L, 20);
  lua_setfield(L, enum_table, "NextChild");
  lua_pushstring(L, "PreviousChild");
  lua_rawseti(L, enum_table, 21);
  lua_pushinteger(L, 21);
  lua_setfield(L, enum_table, "PreviousChild");
  lua_pushstring(L, "Find");
  lua_rawseti(L, enum_table, 22);
  lua_pushinteger(L, 22);
  lua_setfield(L, enum_table, "Find");
  lua_pushstring(L, "FindNext");
  lua_rawseti(L, enum_table, 23);
  lua_pushinteger(L, 23);
  lua_setfield(L, enum_table, "FindNext");
  lua_pushstring(L, "FindPrevious");
  lua_rawseti(L, enum_table, 24);
  lua_pushinteger(L, 24);
  lua_setfield(L, enum_table, "FindPrevious");
  lua_pushstring(L, "Replace");
  lua_rawseti(L, enum_table, 25);
  lua_pushinteger(L, 25);
  lua_setfield(L, enum_table, "Replace");
  lua_pushstring(L, "SelectAll");
  lua_rawseti(L, enum_table, 26);
  lua_pushinteger(L, 26);
  lua_setfield(L, enum_table, "SelectAll");
  lua_pushstring(L, "Bold");
  lua_rawseti(L, enum_table, 27);
  lua_pushinteger(L, 27);
  lua_setfield(L, enum_table, "Bold");
  lua_pushstring(L, "Italic");
  lua_rawseti(L, enum_table, 28);
  lua_pushinteger(L, 28);
  lua_setfield(L, enum_table, "Italic");
  lua_pushstring(L, "Underline");
  lua_rawseti(L, enum_table, 29);
  lua_pushinteger(L, 29);
  lua_setfield(L, enum_table, "Underline");
  lua_pushstring(L, "MoveToNextChar");
  lua_rawseti(L, enum_table, 30);
  lua_pushinteger(L, 30);
  lua_setfield(L, enum_table, "MoveToNextChar");
  lua_pushstring(L, "MoveToPreviousChar");
  lua_rawseti(L, enum_table, 31);
  lua_pushinteger(L, 31);
  lua_setfield(L, enum_table, "MoveToPreviousChar");
  lua_pushstring(L, "MoveToNextWord");
  lua_rawseti(L, enum_table, 32);
  lua_pushinteger(L, 32);
  lua_setfield(L, enum_table, "MoveToNextWord");
  lua_pushstring(L, "MoveToPreviousWord");
  lua_rawseti(L, enum_table, 33);
  lua_pushinteger(L, 33);
  lua_setfield(L, enum_table, "MoveToPreviousWord");
  lua_pushstring(L, "MoveToNextLine");
  lua_rawseti(L, enum_table, 34);
  lua_pushinteger(L, 34);
  lua_setfield(L, enum_table, "MoveToNextLine");
  lua_pushstring(L, "MoveToPreviousLine");
  lua_rawseti(L, enum_table, 35);
  lua_pushinteger(L, 35);
  lua_setfield(L, enum_table, "MoveToPreviousLine");
  lua_pushstring(L, "MoveToNextPage");
  lua_rawseti(L, enum_table, 36);
  lua_pushinteger(L, 36);
  lua_setfield(L, enum_table, "MoveToNextPage");
  lua_pushstring(L, "MoveToPreviousPage");
  lua_rawseti(L, enum_table, 37);
  lua_pushinteger(L, 37);
  lua_setfield(L, enum_table, "MoveToPreviousPage");
  lua_pushstring(L, "MoveToStartOfLine");
  lua_rawseti(L, enum_table, 38);
  lua_pushinteger(L, 38);
  lua_setfield(L, enum_table, "MoveToStartOfLine");
  lua_pushstring(L, "MoveToEndOfLine");
  lua_rawseti(L, enum_table, 39);
  lua_pushinteger(L, 39);
  lua_setfield(L, enum_table, "MoveToEndOfLine");
  lua_pushstring(L, "MoveToStartOfBlock");
  lua_rawseti(L, enum_table, 40);
  lua_pushinteger(L, 40);
  lua_setfield(L, enum_table, "MoveToStartOfBlock");
  lua_pushstring(L, "MoveToEndOfBlock");
  lua_rawseti(L, enum_table, 41);
  lua_pushinteger(L, 41);
  lua_setfield(L, enum_table, "MoveToEndOfBlock");
  lua_pushstring(L, "MoveToStartOfDocument");
  lua_rawseti(L, enum_table, 42);
  lua_pushinteger(L, 42);
  lua_setfield(L, enum_table, "MoveToStartOfDocument");
  lua_pushstring(L, "MoveToEndOfDocument");
  lua_rawseti(L, enum_table, 43);
  lua_pushinteger(L, 43);
  lua_setfield(L, enum_table, "MoveToEndOfDocument");
  lua_pushstring(L, "SelectNextChar");
  lua_rawseti(L, enum_table, 44);
  lua_pushinteger(L, 44);
  lua_setfield(L, enum_table, "SelectNextChar");
  lua_pushstring(L, "SelectPreviousChar");
  lua_rawseti(L, enum_table, 45);
  lua_pushinteger(L, 45);
  lua_setfield(L, enum_table, "SelectPreviousChar");
  lua_pushstring(L, "SelectNextWord");
  lua_rawseti(L, enum_table, 46);
  lua_pushinteger(L, 46);
  lua_setfield(L, enum_table, "SelectNextWord");
  lua_pushstring(L, "SelectPreviousWord");
  lua_rawseti(L, enum_table, 47);
  lua_pushinteger(L, 47);
  lua_setfield(L, enum_table, "SelectPreviousWord");
  lua_pushstring(L, "SelectNextLine");
  lua_rawseti(L, enum_table, 48);
  lua_pushinteger(L, 48);
  lua_setfield(L, enum_table, "SelectNextLine");
  lua_pushstring(L, "SelectPreviousLine");
  lua_rawseti(L, enum_table, 49);
  lua_pushinteger(L, 49);
  lua_setfield(L, enum_table, "SelectPreviousLine");
  lua_pushstring(L, "SelectNextPage");
  lua_rawseti(L, enum_table, 50);
  lua_pushinteger(L, 50);
  lua_setfield(L, enum_table, "SelectNextPage");
  lua_pushstring(L, "SelectPreviousPage");
  lua_rawseti(L, enum_table, 51);
  lua_pushinteger(L, 51);
  lua_setfield(L, enum_table, "SelectPreviousPage");
  lua_pushstring(L, "SelectStartOfLine");
  lua_rawseti(L, enum_table, 52);
  lua_pushinteger(L, 52);
  lua_setfield(L, enum_table, "SelectStartOfLine");
  lua_pushstring(L, "SelectEndOfLine");
  lua_rawseti(L, enum_table, 53);
  lua_pushinteger(L, 53);
  lua_setfield(L, enum_table, "SelectEndOfLine");
  lua_pushstring(L, "SelectStartOfBlock");
  lua_rawseti(L, enum_table, 54);
  lua_pushinteger(L, 54);
  lua_setfield(L, enum_table, "SelectStartOfBlock");
  lua_pushstring(L, "SelectEndOfBlock");
  lua_rawseti(L, enum_table, 55);
  lua_pushinteger(L, 55);
  lua_setfield(L, enum_table, "SelectEndOfBlock");
  lua_pushstring(L, "SelectStartOfDocument");
  lua_rawseti(L, enum_table, 56);
  lua_pushinteger(L, 56);
  lua_setfield(L, enum_table, "SelectStartOfDocument");
  lua_pushstring(L, "SelectEndOfDocument");
  lua_rawseti(L, enum_table, 57);
  lua_pushinteger(L, 57);
  lua_setfield(L, enum_table, "SelectEndOfDocument");
  lua_pushstring(L, "DeleteStartOfWord");
  lua_rawseti(L, enum_table, 58);
  lua_pushinteger(L, 58);
  lua_setfield(L, enum_table, "DeleteStartOfWord");
  lua_pushstring(L, "DeleteEndOfWord");
  lua_rawseti(L, enum_table, 59);
  lua_pushinteger(L, 59);
  lua_setfield(L, enum_table, "DeleteEndOfWord");
  lua_pushstring(L, "DeleteEndOfLine");
  lua_rawseti(L, enum_table, 60);
  lua_pushinteger(L, 60);
  lua_setfield(L, enum_table, "DeleteEndOfLine");
  lua_pushcfunction(L, LuaBinder< QKeySequence >::lqt_pushenum_StandardKey_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QKeySequence::StandardKey");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QKeySequence >::lqt_pushenum_StandardKey_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QKeySequence::StandardKey>*) + sizeof(QFlags<QKeySequence::StandardKey>));
  QFlags<QKeySequence::StandardKey> *fl = static_cast<QFlags<QKeySequence::StandardKey>*>( static_cast<void*>(&static_cast<QFlags<QKeySequence::StandardKey>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QKeySequence::StandardKey>(lqtL_toenum(L, i, "QKeySequence::StandardKey"));
	}
	if (luaL_newmetatable(L, "QFlags<QKeySequence::StandardKey>*")) {
		lua_pushstring(L, "QFlags<QKeySequence::StandardKey>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinder< QKeySequence >::lqt_pushenum_SequenceMatch (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "NoMatch");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "NoMatch");
  lua_pushstring(L, "PartialMatch");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "PartialMatch");
  lua_pushstring(L, "ExactMatch");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "ExactMatch");
  lua_pushcfunction(L, LuaBinder< QKeySequence >::lqt_pushenum_SequenceMatch_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QKeySequence::SequenceMatch");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QKeySequence >::lqt_pushenum_SequenceMatch_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QKeySequence::SequenceMatch>*) + sizeof(QFlags<QKeySequence::SequenceMatch>));
  QFlags<QKeySequence::SequenceMatch> *fl = static_cast<QFlags<QKeySequence::SequenceMatch>*>( static_cast<void*>(&static_cast<QFlags<QKeySequence::SequenceMatch>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QKeySequence::SequenceMatch>(lqtL_toenum(L, i, "QKeySequence::SequenceMatch"));
	}
	if (luaL_newmetatable(L, "QFlags<QKeySequence::SequenceMatch>*")) {
		lua_pushstring(L, "QFlags<QKeySequence::SequenceMatch>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinder< QKeySequence >::lqt_pushenum_SequenceFormat (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "NativeText");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "NativeText");
  lua_pushstring(L, "PortableText");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "PortableText");
  lua_pushcfunction(L, LuaBinder< QKeySequence >::lqt_pushenum_SequenceFormat_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QKeySequence::SequenceFormat");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QKeySequence >::lqt_pushenum_SequenceFormat_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QKeySequence::SequenceFormat>*) + sizeof(QFlags<QKeySequence::SequenceFormat>));
  QFlags<QKeySequence::SequenceFormat> *fl = static_cast<QFlags<QKeySequence::SequenceFormat>*>( static_cast<void*>(&static_cast<QFlags<QKeySequence::SequenceFormat>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QKeySequence::SequenceFormat>(lqtL_toenum(L, i, "QKeySequence::SequenceFormat"));
	}
	if (luaL_newmetatable(L, "QFlags<QKeySequence::SequenceFormat>*")) {
		lua_pushstring(L, "QFlags<QKeySequence::SequenceFormat>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int luaopen_QKeySequence (lua_State *L) {
  if (luaL_newmetatable(L, "QKeySequence*")) {
    lua_pushcfunction(L, LuaBinder< QKeySequence >::__LuaWrapCall__fromString);
    lua_setfield(L, -2, "fromString");
    lua_pushcfunction(L, LuaBinder< QKeySequence >::__LuaWrapCall__isDetached);
    lua_setfield(L, -2, "isDetached");
    lua_pushcfunction(L, LuaBinder< QKeySequence >::__LuaWrapCall__toString);
    lua_setfield(L, -2, "toString");
    lua_pushcfunction(L, LuaBinder< QKeySequence >::__LuaWrapCall__delete);
    lua_setfield(L, -2, "delete");
    lua_pushcfunction(L, LuaBinder< QKeySequence >::__LuaWrapCall__count);
    lua_setfield(L, -2, "count");
    lua_pushcfunction(L, LuaBinder< QKeySequence >::__LuaWrapCall__new);
    lua_setfield(L, -2, "new");
    lua_pushcfunction(L, LuaBinder< QKeySequence >::__LuaWrapCall__isEmpty);
    lua_setfield(L, -2, "isEmpty");
    lua_pushcfunction(L, LuaBinder< QKeySequence >::__LuaWrapCall__matches);
    lua_setfield(L, -2, "matches");
    lua_pushcfunction(L, LuaBinder< QKeySequence >::__LuaWrapCall__mnemonic);
    lua_setfield(L, -2, "mnemonic");
    lua_pushcfunction(L, LuaBinder< QKeySequence >::__LuaWrapCall__keyBindings);
    lua_setfield(L, -2, "keyBindings");
    LuaBinder< QKeySequence >::lqt_pushenum_StandardKey(L);
    lua_setfield(L, -2, "StandardKey");
    LuaBinder< QKeySequence >::lqt_pushenum_SequenceMatch(L);
    lua_setfield(L, -2, "SequenceMatch");
    LuaBinder< QKeySequence >::lqt_pushenum_SequenceFormat(L);
    lua_setfield(L, -2, "SequenceFormat");
    lua_newtable(L);
    lua_setfield(L, -2, "__base");
    lua_pushcfunction(L, lqtL_newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, lqtL_index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, lqtL_gc);
    lua_setfield(L, -2, "__gc");
    lua_pushstring(L, "QKeySequence");
    lua_setfield(L, -2, "__qtype");
		lua_setglobal(L, "QKeySequence");
  } else {
		lua_pop(L, 1);
	}
  return 0;
}
