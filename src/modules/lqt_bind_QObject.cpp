#include "lqt_bind_QObject.hpp"

int LuaBinder< QObject >::__LuaWrapCall__parent (lua_State *L) {
  QObject *& __lua__obj = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QObject * ret = __lua__obj->QObject::parent();
  lqtL_pushudata(L, ret, "QObject*");
  return 1;
}
int LuaBinder< QObject >::__LuaWrapCall__signalsBlocked (lua_State *L) {
  QObject *& __lua__obj = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QObject::signalsBlocked();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QObject >::__LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QObject::trUtf8(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QObject >::__LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QObject::trUtf8(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QObject >::__LuaWrapCall__trUtf8 (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0xd13c40;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0xd137a0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xd15a30;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xd15160;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xd15ec0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__trUtf8__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__trUtf8__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__trUtf8 matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QObject >::__LuaWrapCall__killTimer (lua_State *L) {
  QObject *& __lua__obj = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QObject::killTimer(arg1);
  return 0;
}
int LuaBinder< QObject >::__LuaWrapCall__dumpObjectTree (lua_State *L) {
  QObject *& __lua__obj = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QObject::dumpObjectTree();
  return 0;
}
int LuaBinder< QObject >::__LuaWrapCall__dumpObjectInfo (lua_State *L) {
  QObject *& __lua__obj = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QObject::dumpObjectInfo();
  return 0;
}
int LuaBinder< QObject >::__LuaWrapCall__event (lua_State *L) {
  QObject *& __lua__obj = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QEvent * arg1 = *static_cast<QEvent**>(lqtL_checkudata(L, 2, "QEvent*"));
  bool ret = __lua__obj->QObject::event(arg1);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QObject >::__LuaWrapCall__installEventFilter (lua_State *L) {
  QObject *& __lua__obj = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QObject * arg1 = *static_cast<QObject**>(lqtL_checkudata(L, 2, "QObject*"));
  __lua__obj->QObject::installEventFilter(arg1);
  return 0;
}
int LuaBinder< QObject >::__LuaWrapCall__inherits (lua_State *L) {
  QObject *& __lua__obj = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const char * arg1 = lua_tostring(L, 2);
  bool ret = __lua__obj->QObject::inherits(arg1);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QObject >::__LuaWrapCall__registerUserData (lua_State *L) {
  unsigned int ret = QObject::registerUserData();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QObject >::__LuaWrapCall__dynamicPropertyNames (lua_State *L) {
  QObject *& __lua__obj = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QList<QByteArray> ret = __lua__obj->QObject::dynamicPropertyNames();
  lqtL_passudata(L, new QList<QByteArray>(ret), "QList<QByteArray>*");
  return 1;
}
int LuaBinder< QObject >::__LuaWrapCall__connect__OverloadedVersion__1 (lua_State *L) {
  const QObject * arg1 = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
  const char * arg2 = lua_tostring(L, 2);
  const QObject * arg3 = *static_cast<QObject**>(lqtL_checkudata(L, 3, "QObject*"));
  const char * arg4 = lua_tostring(L, 4);
  Qt::ConnectionType arg5 = lqtL_isenum(L, 5, "Qt::ConnectionType")?static_cast<Qt::ConnectionType>(lqtL_toenum(L, 5, "Qt::ConnectionType")):Qt::AutoConnection;
  bool ret = QObject::connect(arg1, arg2, arg3, arg4, arg5);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QObject >::__LuaWrapCall__connect__OverloadedVersion__2 (lua_State *L) {
  QObject *& __lua__obj = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QObject * arg1 = *static_cast<QObject**>(lqtL_checkudata(L, 2, "QObject*"));
  const char * arg2 = lua_tostring(L, 3);
  const char * arg3 = lua_tostring(L, 4);
  Qt::ConnectionType arg4 = lqtL_isenum(L, 5, "Qt::ConnectionType")?static_cast<Qt::ConnectionType>(lqtL_toenum(L, 5, "Qt::ConnectionType")):Qt::AutoConnection;
  bool ret = __lua__obj->QObject::connect(arg1, arg2, arg3, arg4);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QObject >::__LuaWrapCall__connect (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if (lqtL_testudata(L, 1, "QObject*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0xd22980;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0xd22650;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_testudata(L, 3, "QObject*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0xd22e30;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 4)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0xd23690;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_isenum(L, 5, "Qt::ConnectionType")) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0xd23a40;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QObject*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QObject*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xd24460;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 3)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xd23630;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 4)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xd24970;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_isenum(L, 5, "Qt::ConnectionType")) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0xd25290;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__connect__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__connect__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__connect matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QObject >::__LuaWrapCall__setObjectName (lua_State *L) {
  QObject *& __lua__obj = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QObject::setObjectName(arg1);
  return 0;
}
int LuaBinder< QObject >::__LuaWrapCall__isWidgetType (lua_State *L) {
  QObject *& __lua__obj = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QObject::isWidgetType();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QObject >::__LuaWrapCall__property (lua_State *L) {
  QObject *& __lua__obj = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const char * arg1 = lua_tostring(L, 2);
  QVariant ret = __lua__obj->QObject::property(arg1);
  lqtL_passudata(L, new QVariant(ret), "QVariant*");
  return 1;
}
int LuaBinder< QObject >::__LuaWrapCall__children (lua_State *L) {
  QObject *& __lua__obj = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QList<QObject*>& ret = __lua__obj->QObject::children();
  lqtL_pushudata(L, &(ret), "QList<QObject*>*");
  return 1;
}
int LuaBinder< QObject >::__LuaWrapCall__new (lua_State *L) {
  QObject * arg1 = lqtL_testudata(L, 1, "QObject*")?*static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*")):static_cast< QObject * >(0);
  QObject * ret = new LuaBinder< QObject >(L, arg1);
  lqtL_passudata(L, ret, "QObject*");
  return 1;
}
int LuaBinder< QObject >::__LuaWrapCall__delete (lua_State *L) {
  QObject *& __lua__obj = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  delete __lua__obj;
  __lua__obj = 0;
  return 0;
}
int LuaBinder< QObject >::__LuaWrapCall__setUserData (lua_State *L) {
  QObject *& __lua__obj = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  unsigned int arg1 = lua_tointeger(L, 2);
  QObjectUserData * arg2 = *static_cast<QObjectUserData**>(lqtL_checkudata(L, 3, "QObjectUserData*"));
  __lua__obj->QObject::setUserData(arg1, arg2);
  return 0;
}
int LuaBinder< QObject >::__LuaWrapCall__eventFilter (lua_State *L) {
  QObject *& __lua__obj = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QObject * arg1 = *static_cast<QObject**>(lqtL_checkudata(L, 2, "QObject*"));
  QEvent * arg2 = *static_cast<QEvent**>(lqtL_checkudata(L, 3, "QEvent*"));
  bool ret = __lua__obj->QObject::eventFilter(arg1, arg2);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QObject >::__LuaWrapCall__userData (lua_State *L) {
  QObject *& __lua__obj = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  unsigned int arg1 = lua_tointeger(L, 2);
  QObjectUserData * ret = __lua__obj->QObject::userData(arg1);
  lqtL_pushudata(L, ret, "QObjectUserData*");
  return 1;
}
int LuaBinder< QObject >::__LuaWrapCall__startTimer (lua_State *L) {
  QObject *& __lua__obj = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int ret = __lua__obj->QObject::startTimer(arg1);
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QObject >::__LuaWrapCall__moveToThread (lua_State *L) {
  QObject *& __lua__obj = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QThread * arg1 = *static_cast<QThread**>(lqtL_checkudata(L, 2, "QThread*"));
  __lua__obj->QObject::moveToThread(arg1);
  return 0;
}
int LuaBinder< QObject >::__LuaWrapCall__thread (lua_State *L) {
  QObject *& __lua__obj = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QThread * ret = __lua__obj->QObject::thread();
  lqtL_pushudata(L, ret, "QThread*");
  return 1;
}
int LuaBinder< QObject >::__LuaWrapCall__blockSignals (lua_State *L) {
  QObject *& __lua__obj = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  bool ret = __lua__obj->QObject::blockSignals(arg1);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QObject >::__LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QObject::tr(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QObject >::__LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QObject::tr(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QObject >::__LuaWrapCall__tr (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0xd12f00;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0xd12c70;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xd14920;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xd14e10;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xd151c0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__tr__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__tr__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__tr matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QObject >::__LuaWrapCall__setParent (lua_State *L) {
  QObject *& __lua__obj = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QObject * arg1 = *static_cast<QObject**>(lqtL_checkudata(L, 2, "QObject*"));
  __lua__obj->QObject::setParent(arg1);
  return 0;
}
int LuaBinder< QObject >::__LuaWrapCall__disconnect__OverloadedVersion__1 (lua_State *L) {
  const QObject * arg1 = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
  const char * arg2 = lua_tostring(L, 2);
  const QObject * arg3 = *static_cast<QObject**>(lqtL_checkudata(L, 3, "QObject*"));
  const char * arg4 = lua_tostring(L, 4);
  bool ret = QObject::disconnect(arg1, arg2, arg3, arg4);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QObject >::__LuaWrapCall__disconnect__OverloadedVersion__2 (lua_State *L) {
  QObject *& __lua__obj = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const char * arg1 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  const QObject * arg2 = lqtL_testudata(L, 3, "QObject*")?*static_cast<QObject**>(lqtL_checkudata(L, 3, "QObject*")):static_cast< const QObject * >(0);
  const char * arg3 = (lua_type(L, 4)==LUA_TSTRING)?lua_tostring(L, 4):static_cast< const char * >(0);
  bool ret = __lua__obj->QObject::disconnect(arg1, arg2, arg3);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QObject >::__LuaWrapCall__disconnect__OverloadedVersion__3 (lua_State *L) {
  QObject *& __lua__obj = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QObject * arg1 = *static_cast<QObject**>(lqtL_checkudata(L, 2, "QObject*"));
  const char * arg2 = (lua_type(L, 3)==LUA_TSTRING)?lua_tostring(L, 3):static_cast< const char * >(0);
  bool ret = __lua__obj->QObject::disconnect(arg1, arg2);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QObject >::__LuaWrapCall__disconnect (lua_State *L) {
  int score[4];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if (lqtL_testudata(L, 1, "QObject*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0xd25bc0;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0xd258f0;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_testudata(L, 3, "QObject*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0xd26080;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 4)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0xd268e0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QObject*")?premium:-premium*premium;
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0xd271d0;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_testudata(L, 3, "QObject*")) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0xd26ed0;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 4)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0xd27680;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  score[3] += lqtL_testudata(L, 1, "QObject*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QObject*")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0xd28440;
  } else {
    score[3] -= premium*premium;
  }
  if ((lua_type(L, 3)==LUA_TSTRING)) {
    score[3] += premium;
  } else if (true) {
    score[3] += premium-1; // table: 0xd27a50;
  } else {
    score[3] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=3;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__disconnect__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__disconnect__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__disconnect__OverloadedVersion__3(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__disconnect matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QObject >::__LuaWrapCall__removeEventFilter (lua_State *L) {
  QObject *& __lua__obj = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QObject * arg1 = *static_cast<QObject**>(lqtL_checkudata(L, 2, "QObject*"));
  __lua__obj->QObject::removeEventFilter(arg1);
  return 0;
}
int LuaBinder< QObject >::__LuaWrapCall__deleteLater (lua_State *L) {
  QObject *& __lua__obj = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QObject::deleteLater();
  return 0;
}
int LuaBinder< QObject >::__LuaWrapCall__metaObject (lua_State *L) {
  QObject *& __lua__obj = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QMetaObject * ret = __lua__obj->QObject::metaObject();
  lqtL_pushudata(L, ret, "QMetaObject*");
  return 1;
}
int LuaBinder< QObject >::__LuaWrapCall__setProperty (lua_State *L) {
  QObject *& __lua__obj = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const char * arg1 = lua_tostring(L, 2);
  const QVariant& arg2 = **static_cast<QVariant**>(lqtL_checkudata(L, 3, "QVariant*"));
  bool ret = __lua__obj->QObject::setProperty(arg1, arg2);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QObject >::__LuaWrapCall__objectName (lua_State *L) {
  QObject *& __lua__obj = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QObject::objectName();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
bool LuaBinder< QObject >::event (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QObject*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "event");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QObject::event(arg1);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QObject >::disconnectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QObject*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "disconnectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::disconnectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QObject >::eventFilter (QObject * arg1, QEvent * arg2) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QObject*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "eventFilter");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QObject*");
  lqtL_pushudata(L, arg2, "QEvent*");
  if (lua_isfunction(L, -2-2)) {
    lua_pcall(L, 2+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QObject::eventFilter(arg1, arg2);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QObject >::timerEvent (QTimerEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QObject*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "timerEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QTimerEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::timerEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QObject >::connectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QObject*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "connectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::connectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QObject >::childEvent (QChildEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QObject*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "childEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QChildEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::childEvent(arg1);
  }
  lua_settop(L, oldtop);
}
const QMetaObject * LuaBinder< QObject >::metaObject () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QObject*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "metaObject");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QObject::metaObject();
  }
  const QMetaObject * ret = *static_cast<QMetaObject**>(lqtL_checkudata(L, -1, "QMetaObject*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QObject >::customEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QObject*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "customEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::customEvent(arg1);
  }
  lua_settop(L, oldtop);
}
LuaBinder< QObject >::  ~LuaBinder< QObject > () {
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QObject*");
  lua_getfield(L, -1, "~QObject");

  if (lua_isfunction(L, -1)) {
    lua_insert(L, -2);
    lua_pcall(L, 1, 1, 0);
  } else {
  }
  lua_settop(L, oldtop);
}
int luaopen_QObject (lua_State *L) {
  if (luaL_newmetatable(L, "QObject*")) {
    lua_pushcfunction(L, LuaBinder< QObject >::__LuaWrapCall__parent);
    lua_setfield(L, -2, "parent");
    lua_pushcfunction(L, LuaBinder< QObject >::__LuaWrapCall__signalsBlocked);
    lua_setfield(L, -2, "signalsBlocked");
    lua_pushcfunction(L, LuaBinder< QObject >::__LuaWrapCall__trUtf8);
    lua_setfield(L, -2, "trUtf8");
    lua_pushcfunction(L, LuaBinder< QObject >::__LuaWrapCall__killTimer);
    lua_setfield(L, -2, "killTimer");
    lua_pushcfunction(L, LuaBinder< QObject >::__LuaWrapCall__dumpObjectTree);
    lua_setfield(L, -2, "dumpObjectTree");
    lua_pushcfunction(L, LuaBinder< QObject >::__LuaWrapCall__dumpObjectInfo);
    lua_setfield(L, -2, "dumpObjectInfo");
    lua_pushcfunction(L, LuaBinder< QObject >::__LuaWrapCall__event);
    lua_setfield(L, -2, "event");
    lua_pushcfunction(L, LuaBinder< QObject >::__LuaWrapCall__installEventFilter);
    lua_setfield(L, -2, "installEventFilter");
    lua_pushcfunction(L, LuaBinder< QObject >::__LuaWrapCall__inherits);
    lua_setfield(L, -2, "inherits");
    lua_pushcfunction(L, LuaBinder< QObject >::__LuaWrapCall__registerUserData);
    lua_setfield(L, -2, "registerUserData");
    lua_pushcfunction(L, LuaBinder< QObject >::__LuaWrapCall__dynamicPropertyNames);
    lua_setfield(L, -2, "dynamicPropertyNames");
    lua_pushcfunction(L, LuaBinder< QObject >::__LuaWrapCall__connect);
    lua_setfield(L, -2, "connect");
    lua_pushcfunction(L, LuaBinder< QObject >::__LuaWrapCall__setObjectName);
    lua_setfield(L, -2, "setObjectName");
    lua_pushcfunction(L, LuaBinder< QObject >::__LuaWrapCall__isWidgetType);
    lua_setfield(L, -2, "isWidgetType");
    lua_pushcfunction(L, LuaBinder< QObject >::__LuaWrapCall__property);
    lua_setfield(L, -2, "property");
    lua_pushcfunction(L, LuaBinder< QObject >::__LuaWrapCall__children);
    lua_setfield(L, -2, "children");
    lua_pushcfunction(L, LuaBinder< QObject >::__LuaWrapCall__new);
    lua_setfield(L, -2, "new");
    lua_pushcfunction(L, LuaBinder< QObject >::__LuaWrapCall__delete);
    lua_setfield(L, -2, "delete");
    lua_pushcfunction(L, LuaBinder< QObject >::__LuaWrapCall__setUserData);
    lua_setfield(L, -2, "setUserData");
    lua_pushcfunction(L, LuaBinder< QObject >::__LuaWrapCall__eventFilter);
    lua_setfield(L, -2, "eventFilter");
    lua_pushcfunction(L, LuaBinder< QObject >::__LuaWrapCall__userData);
    lua_setfield(L, -2, "userData");
    lua_pushcfunction(L, LuaBinder< QObject >::__LuaWrapCall__startTimer);
    lua_setfield(L, -2, "startTimer");
    lua_pushcfunction(L, LuaBinder< QObject >::__LuaWrapCall__moveToThread);
    lua_setfield(L, -2, "moveToThread");
    lua_pushcfunction(L, LuaBinder< QObject >::__LuaWrapCall__thread);
    lua_setfield(L, -2, "thread");
    lua_pushcfunction(L, LuaBinder< QObject >::__LuaWrapCall__blockSignals);
    lua_setfield(L, -2, "blockSignals");
    lua_pushcfunction(L, LuaBinder< QObject >::__LuaWrapCall__tr);
    lua_setfield(L, -2, "tr");
    lua_pushcfunction(L, LuaBinder< QObject >::__LuaWrapCall__setParent);
    lua_setfield(L, -2, "setParent");
    lua_pushcfunction(L, LuaBinder< QObject >::__LuaWrapCall__disconnect);
    lua_setfield(L, -2, "disconnect");
    lua_pushcfunction(L, LuaBinder< QObject >::__LuaWrapCall__removeEventFilter);
    lua_setfield(L, -2, "removeEventFilter");
    lua_pushcfunction(L, LuaBinder< QObject >::__LuaWrapCall__deleteLater);
    lua_setfield(L, -2, "deleteLater");
    lua_pushcfunction(L, LuaBinder< QObject >::__LuaWrapCall__metaObject);
    lua_setfield(L, -2, "metaObject");
    lua_pushcfunction(L, LuaBinder< QObject >::__LuaWrapCall__setProperty);
    lua_setfield(L, -2, "setProperty");
    lua_pushcfunction(L, LuaBinder< QObject >::__LuaWrapCall__objectName);
    lua_setfield(L, -2, "objectName");
    lua_newtable(L);
    lua_setfield(L, -2, "__base");
    lua_pushcfunction(L, lqtL_newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, lqtL_index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, lqtL_gc);
    lua_setfield(L, -2, "__gc");
    lua_pushstring(L, "QObject");
    lua_setfield(L, -2, "__qtype");
		lua_setglobal(L, "QObject");
  } else {
		lua_pop(L, 1);
	}
  return 0;
}
