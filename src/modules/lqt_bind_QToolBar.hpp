#include "lqt_common.hpp"
#include <QToolBar>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QToolBar > : public QToolBar {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__isFloating (lua_State *L);
  static int __LuaWrapCall__isMovable (lua_State *L);
  static int __LuaWrapCall__addSeparator (lua_State *L);
  static int __LuaWrapCall__delete (lua_State *L);
  static int __LuaWrapCall__insertSeparator (lua_State *L);
  static int __LuaWrapCall__setOrientation (lua_State *L);
  static int __LuaWrapCall__setToolButtonStyle (lua_State *L);
  static int __LuaWrapCall__insertWidget (lua_State *L);
  static int __LuaWrapCall__setIconSize (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__trUtf8 (lua_State *L);
  static int __LuaWrapCall__actionAt__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__actionAt__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__actionAt (lua_State *L);
  static int __LuaWrapCall__metaObject (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__new (lua_State *L);
  static int __LuaWrapCall__clear (lua_State *L);
  static int __LuaWrapCall__isAreaAllowed (lua_State *L);
  static int __LuaWrapCall__widgetForAction (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__tr (lua_State *L);
  static int __LuaWrapCall__setAllowedAreas (lua_State *L);
  static int __LuaWrapCall__setMovable (lua_State *L);
  static int __LuaWrapCall__allowedAreas (lua_State *L);
  static int __LuaWrapCall__isFloatable (lua_State *L);
  static int __LuaWrapCall__toolButtonStyle (lua_State *L);
  static int __LuaWrapCall__addAction__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__addAction__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__addAction__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__addAction__OverloadedVersion__4 (lua_State *L);
  static int __LuaWrapCall__addAction (lua_State *L);
  static int __LuaWrapCall__iconSize (lua_State *L);
  static int __LuaWrapCall__addWidget (lua_State *L);
  static int __LuaWrapCall__orientation (lua_State *L);
  static int __LuaWrapCall__toggleViewAction (lua_State *L);
  static int __LuaWrapCall__actionGeometry (lua_State *L);
  static int __LuaWrapCall__setFloatable (lua_State *L);
protected:
  void styleChange (QStyle& arg1);
public:
protected:
  void focusInEvent (QFocusEvent * arg1);
public:
protected:
  void keyPressEvent (QKeyEvent * arg1);
public:
  const QMetaObject * metaObject () const;
  int devType () const;
  void setVisible (bool arg1);
  QVariant inputMethodQuery (Qt::InputMethodQuery arg1) const;
  QPaintEngine * paintEngine () const;
protected:
  void dragLeaveEvent (QDragLeaveEvent * arg1);
public:
protected:
  void mousePressEvent (QMouseEvent * arg1);
public:
  QSize sizeHint () const;
protected:
  void moveEvent (QMoveEvent * arg1);
public:
protected:
  void tabletEvent (QTabletEvent * arg1);
public:
protected:
  void enterEvent (QEvent * arg1);
public:
protected:
  void closeEvent (QCloseEvent * arg1);
public:
  QSize minimumSizeHint () const;
protected:
  void actionEvent (QActionEvent * arg1);
public:
protected:
  void dragEnterEvent (QDragEnterEvent * arg1);
public:
protected:
  void showEvent (QShowEvent * arg1);
public:
protected:
  void dragMoveEvent (QDragMoveEvent * arg1);
public:
protected:
  void paintEvent (QPaintEvent * arg1);
public:
protected:
  void windowActivationChange (bool arg1);
public:
protected:
  void fontChange (const QFont& arg1);
public:
protected:
  void enabledChange (bool arg1);
public:
protected:
  void disconnectNotify (const char * arg1);
public:
  int heightForWidth (int arg1) const;
protected:
  void mouseDoubleClickEvent (QMouseEvent * arg1);
public:
protected:
  bool focusNextPrevChild (bool arg1);
public:
protected:
  void timerEvent (QTimerEvent * arg1);
public:
protected:
  void mouseMoveEvent (QMouseEvent * arg1);
public:
protected:
  void focusOutEvent (QFocusEvent * arg1);
public:
protected:
  void childEvent (QChildEvent * arg1);
public:
protected:
  void connectNotify (const char * arg1);
public:
protected:
  void mouseReleaseEvent (QMouseEvent * arg1);
public:
protected:
  int metric (QPaintDevice::PaintDeviceMetric arg1) const;
public:
  bool eventFilter (QObject * arg1, QEvent * arg2);
protected:
  void inputMethodEvent (QInputMethodEvent * arg1);
public:
protected:
  void wheelEvent (QWheelEvent * arg1);
public:
protected:
  void resizeEvent (QResizeEvent * arg1);
public:
protected:
  void paletteChange (const QPalette& arg1);
public:
protected:
  void languageChange ();
public:
protected:
  bool event (QEvent * arg1);
public:
protected:
  void hideEvent (QHideEvent * arg1);
public:
protected:
  void contextMenuEvent (QContextMenuEvent * arg1);
public:
protected:
  void keyReleaseEvent (QKeyEvent * arg1);
public:
protected:
  void dropEvent (QDropEvent * arg1);
public:
protected:
  void leaveEvent (QEvent * arg1);
public:
protected:
  void changeEvent (QEvent * arg1);
public:
protected:
  void customEvent (QEvent * arg1);
public:
  ~LuaBinder< QToolBar > ();
  LuaBinder< QToolBar > (lua_State *l, const QString& arg1, QWidget * arg2):QToolBar(arg1, arg2), L(l) {}
  LuaBinder< QToolBar > (lua_State *l, QWidget * arg1):QToolBar(arg1), L(l) {}
};

extern "C" int luaopen_QToolBar (lua_State *L);
