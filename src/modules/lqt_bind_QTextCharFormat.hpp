#include "lqt_common.hpp"
#include <QTextCharFormat>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QTextCharFormat > : public QTextCharFormat {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__setFontFamily (lua_State *L);
  static int __LuaWrapCall__setFontItalic (lua_State *L);
  static int __LuaWrapCall__setUnderlineColor (lua_State *L);
  static int __LuaWrapCall__fontFamily (lua_State *L);
  static int __LuaWrapCall__fontItalic (lua_State *L);
  static int __LuaWrapCall__underlineColor (lua_State *L);
  static int __LuaWrapCall__tableCellRowSpan (lua_State *L);
  static int __LuaWrapCall__fontWeight (lua_State *L);
  static int __LuaWrapCall__isValid (lua_State *L);
  static int __LuaWrapCall__fontOverline (lua_State *L);
  static int __LuaWrapCall__fontPointSize (lua_State *L);
  static int __LuaWrapCall__setTableCellRowSpan (lua_State *L);
  static int __LuaWrapCall__font (lua_State *L);
  static int __LuaWrapCall__setTableCellColumnSpan (lua_State *L);
  static int __LuaWrapCall__setAnchorHref (lua_State *L);
  static int __LuaWrapCall__anchorHref (lua_State *L);
  static int __LuaWrapCall__fontFixedPitch (lua_State *L);
  static int __LuaWrapCall__setFontPointSize (lua_State *L);
  static int __LuaWrapCall__verticalAlignment (lua_State *L);
  static int __LuaWrapCall__setTextOutline (lua_State *L);
  static int __LuaWrapCall__underlineStyle (lua_State *L);
  static int __LuaWrapCall__setFontUnderline (lua_State *L);
  static int __LuaWrapCall__isAnchor (lua_State *L);
  static int __LuaWrapCall__textOutline (lua_State *L);
  static int __LuaWrapCall__setFontStrikeOut (lua_State *L);
  static int __LuaWrapCall__setFontWeight (lua_State *L);
  static int __LuaWrapCall__setFontFixedPitch (lua_State *L);
  static int __LuaWrapCall__delete (lua_State *L);
  static int __LuaWrapCall__anchorName (lua_State *L);
  static int __LuaWrapCall__setAnchor (lua_State *L);
  static int __LuaWrapCall__setUnderlineStyle (lua_State *L);
  static int __LuaWrapCall__setToolTip (lua_State *L);
  static int __LuaWrapCall__setAnchorName (lua_State *L);
  static int __LuaWrapCall__setFontOverline (lua_State *L);
  static int __LuaWrapCall__tableCellColumnSpan (lua_State *L);
  static int __LuaWrapCall__setVerticalAlignment (lua_State *L);
  static int __LuaWrapCall__fontStrikeOut (lua_State *L);
  static int __LuaWrapCall__setAnchorNames (lua_State *L);
  static int __LuaWrapCall__toolTip (lua_State *L);
  static int __LuaWrapCall__setFont (lua_State *L);
  static int __LuaWrapCall__fontUnderline (lua_State *L);
  static int __LuaWrapCall__anchorNames (lua_State *L);
  static int lqt_pushenum_VerticalAlignment (lua_State *L);
  static int lqt_pushenum_VerticalAlignment_QFLAGS_CREATOR (lua_State *L);
  static int lqt_pushenum_UnderlineStyle (lua_State *L);
  static int lqt_pushenum_UnderlineStyle_QFLAGS_CREATOR (lua_State *L);
  LuaBinder< QTextCharFormat > (lua_State *l):QTextCharFormat(), L(l) {}
};

extern "C" int luaopen_QTextCharFormat (lua_State *L);
