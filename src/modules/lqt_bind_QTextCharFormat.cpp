#include "lqt_bind_QTextCharFormat.hpp"

int LuaBinder< QTextCharFormat >::__LuaWrapCall__setFontFamily (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QTextCharFormat::setFontFamily(arg1);
  return 0;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__setFontItalic (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QTextCharFormat::setFontItalic(arg1);
  return 0;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__setUnderlineColor (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QColor& arg1 = **static_cast<QColor**>(lqtL_checkudata(L, 2, "QColor*"));
  __lua__obj->QTextCharFormat::setUnderlineColor(arg1);
  return 0;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__fontFamily (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QTextCharFormat::fontFamily();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__fontItalic (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QTextCharFormat::fontItalic();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__underlineColor (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QColor ret = __lua__obj->QTextCharFormat::underlineColor();
  lqtL_passudata(L, new QColor(ret), "QColor*");
  return 1;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__tableCellRowSpan (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QTextCharFormat::tableCellRowSpan();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__fontWeight (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QTextCharFormat::fontWeight();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__isValid (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QTextCharFormat::isValid();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__fontOverline (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QTextCharFormat::fontOverline();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__fontPointSize (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double ret = __lua__obj->QTextCharFormat::fontPointSize();
  lua_pushnumber(L, ret);
  return 1;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__setTableCellRowSpan (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QTextCharFormat::setTableCellRowSpan(arg1);
  return 0;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__font (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFont ret = __lua__obj->QTextCharFormat::font();
  lqtL_passudata(L, new QFont(ret), "QFont*");
  return 1;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__setTableCellColumnSpan (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QTextCharFormat::setTableCellColumnSpan(arg1);
  return 0;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__setAnchorHref (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QTextCharFormat::setAnchorHref(arg1);
  return 0;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__anchorHref (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QTextCharFormat::anchorHref();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__fontFixedPitch (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QTextCharFormat::fontFixedPitch();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__setFontPointSize (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  __lua__obj->QTextCharFormat::setFontPointSize(arg1);
  return 0;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__verticalAlignment (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextCharFormat::VerticalAlignment ret = __lua__obj->QTextCharFormat::verticalAlignment();
  lqtL_pushenum(L, ret, "QTextCharFormat::VerticalAlignment");
  return 1;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__setTextOutline (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPen& arg1 = **static_cast<QPen**>(lqtL_checkudata(L, 2, "QPen*"));
  __lua__obj->QTextCharFormat::setTextOutline(arg1);
  return 0;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__underlineStyle (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextCharFormat::UnderlineStyle ret = __lua__obj->QTextCharFormat::underlineStyle();
  lqtL_pushenum(L, ret, "QTextCharFormat::UnderlineStyle");
  return 1;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__setFontUnderline (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QTextCharFormat::setFontUnderline(arg1);
  return 0;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__isAnchor (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QTextCharFormat::isAnchor();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__textOutline (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QPen ret = __lua__obj->QTextCharFormat::textOutline();
  lqtL_passudata(L, new QPen(ret), "QPen*");
  return 1;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__setFontStrikeOut (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QTextCharFormat::setFontStrikeOut(arg1);
  return 0;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__setFontWeight (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QTextCharFormat::setFontWeight(arg1);
  return 0;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__setFontFixedPitch (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QTextCharFormat::setFontFixedPitch(arg1);
  return 0;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__delete (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  delete __lua__obj;
  __lua__obj = 0;
  return 0;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__anchorName (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QTextCharFormat::anchorName();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__setAnchor (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QTextCharFormat::setAnchor(arg1);
  return 0;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__setUnderlineStyle (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextCharFormat::UnderlineStyle arg1 = static_cast<QTextCharFormat::UnderlineStyle>(lqtL_toenum(L, 2, "QTextCharFormat::UnderlineStyle"));
  __lua__obj->QTextCharFormat::setUnderlineStyle(arg1);
  return 0;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__setToolTip (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QTextCharFormat::setToolTip(arg1);
  return 0;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__setAnchorName (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QTextCharFormat::setAnchorName(arg1);
  return 0;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__setFontOverline (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QTextCharFormat::setFontOverline(arg1);
  return 0;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__tableCellColumnSpan (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QTextCharFormat::tableCellColumnSpan();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__setVerticalAlignment (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextCharFormat::VerticalAlignment arg1 = static_cast<QTextCharFormat::VerticalAlignment>(lqtL_toenum(L, 2, "QTextCharFormat::VerticalAlignment"));
  __lua__obj->QTextCharFormat::setVerticalAlignment(arg1);
  return 0;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__fontStrikeOut (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QTextCharFormat::fontStrikeOut();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__setAnchorNames (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QStringList& arg1 = **static_cast<QStringList**>(lqtL_checkudata(L, 2, "QStringList*"));
  __lua__obj->QTextCharFormat::setAnchorNames(arg1);
  return 0;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__toolTip (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QTextCharFormat::toolTip();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__setFont (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QFont& arg1 = **static_cast<QFont**>(lqtL_checkudata(L, 2, "QFont*"));
  __lua__obj->QTextCharFormat::setFont(arg1);
  return 0;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__fontUnderline (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QTextCharFormat::fontUnderline();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextCharFormat >::__LuaWrapCall__anchorNames (lua_State *L) {
  QTextCharFormat *& __lua__obj = *static_cast<QTextCharFormat**>(lqtL_checkudata(L, 1, "QTextCharFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QStringList ret = __lua__obj->QTextCharFormat::anchorNames();
  lqtL_passudata(L, new QStringList(ret), "QStringList*");
  return 1;
}
int LuaBinder< QTextCharFormat >::lqt_pushenum_VerticalAlignment (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "AlignNormal");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "AlignNormal");
  lua_pushstring(L, "AlignSuperScript");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "AlignSuperScript");
  lua_pushstring(L, "AlignSubScript");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "AlignSubScript");
  lua_pushstring(L, "AlignMiddle");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "AlignMiddle");
  lua_pushstring(L, "AlignTop");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "AlignTop");
  lua_pushstring(L, "AlignBottom");
  lua_rawseti(L, enum_table, 5);
  lua_pushinteger(L, 5);
  lua_setfield(L, enum_table, "AlignBottom");
  lua_pushcfunction(L, LuaBinder< QTextCharFormat >::lqt_pushenum_VerticalAlignment_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QTextCharFormat::VerticalAlignment");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QTextCharFormat >::lqt_pushenum_VerticalAlignment_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QTextCharFormat::VerticalAlignment>*) + sizeof(QFlags<QTextCharFormat::VerticalAlignment>));
  QFlags<QTextCharFormat::VerticalAlignment> *fl = static_cast<QFlags<QTextCharFormat::VerticalAlignment>*>( static_cast<void*>(&static_cast<QFlags<QTextCharFormat::VerticalAlignment>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QTextCharFormat::VerticalAlignment>(lqtL_toenum(L, i, "QTextCharFormat::VerticalAlignment"));
	}
	if (luaL_newmetatable(L, "QFlags<QTextCharFormat::VerticalAlignment>*")) {
		lua_pushstring(L, "QFlags<QTextCharFormat::VerticalAlignment>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinder< QTextCharFormat >::lqt_pushenum_UnderlineStyle (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "NoUnderline");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "NoUnderline");
  lua_pushstring(L, "SingleUnderline");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "SingleUnderline");
  lua_pushstring(L, "DashUnderline");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "DashUnderline");
  lua_pushstring(L, "DotLine");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "DotLine");
  lua_pushstring(L, "DashDotLine");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "DashDotLine");
  lua_pushstring(L, "DashDotDotLine");
  lua_rawseti(L, enum_table, 5);
  lua_pushinteger(L, 5);
  lua_setfield(L, enum_table, "DashDotDotLine");
  lua_pushstring(L, "WaveUnderline");
  lua_rawseti(L, enum_table, 6);
  lua_pushinteger(L, 6);
  lua_setfield(L, enum_table, "WaveUnderline");
  lua_pushstring(L, "SpellCheckUnderline");
  lua_rawseti(L, enum_table, 7);
  lua_pushinteger(L, 7);
  lua_setfield(L, enum_table, "SpellCheckUnderline");
  lua_pushcfunction(L, LuaBinder< QTextCharFormat >::lqt_pushenum_UnderlineStyle_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QTextCharFormat::UnderlineStyle");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QTextCharFormat >::lqt_pushenum_UnderlineStyle_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QTextCharFormat::UnderlineStyle>*) + sizeof(QFlags<QTextCharFormat::UnderlineStyle>));
  QFlags<QTextCharFormat::UnderlineStyle> *fl = static_cast<QFlags<QTextCharFormat::UnderlineStyle>*>( static_cast<void*>(&static_cast<QFlags<QTextCharFormat::UnderlineStyle>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QTextCharFormat::UnderlineStyle>(lqtL_toenum(L, i, "QTextCharFormat::UnderlineStyle"));
	}
	if (luaL_newmetatable(L, "QFlags<QTextCharFormat::UnderlineStyle>*")) {
		lua_pushstring(L, "QFlags<QTextCharFormat::UnderlineStyle>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int luaopen_QTextCharFormat (lua_State *L) {
  if (luaL_newmetatable(L, "QTextCharFormat*")) {
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__setFontFamily);
    lua_setfield(L, -2, "setFontFamily");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__setFontItalic);
    lua_setfield(L, -2, "setFontItalic");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__setUnderlineColor);
    lua_setfield(L, -2, "setUnderlineColor");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__fontFamily);
    lua_setfield(L, -2, "fontFamily");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__fontItalic);
    lua_setfield(L, -2, "fontItalic");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__underlineColor);
    lua_setfield(L, -2, "underlineColor");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__tableCellRowSpan);
    lua_setfield(L, -2, "tableCellRowSpan");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__fontWeight);
    lua_setfield(L, -2, "fontWeight");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__isValid);
    lua_setfield(L, -2, "isValid");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__fontOverline);
    lua_setfield(L, -2, "fontOverline");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__fontPointSize);
    lua_setfield(L, -2, "fontPointSize");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__setTableCellRowSpan);
    lua_setfield(L, -2, "setTableCellRowSpan");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__font);
    lua_setfield(L, -2, "font");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__setTableCellColumnSpan);
    lua_setfield(L, -2, "setTableCellColumnSpan");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__setAnchorHref);
    lua_setfield(L, -2, "setAnchorHref");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__anchorHref);
    lua_setfield(L, -2, "anchorHref");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__fontFixedPitch);
    lua_setfield(L, -2, "fontFixedPitch");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__setFontPointSize);
    lua_setfield(L, -2, "setFontPointSize");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__verticalAlignment);
    lua_setfield(L, -2, "verticalAlignment");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__setTextOutline);
    lua_setfield(L, -2, "setTextOutline");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__underlineStyle);
    lua_setfield(L, -2, "underlineStyle");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__setFontUnderline);
    lua_setfield(L, -2, "setFontUnderline");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__isAnchor);
    lua_setfield(L, -2, "isAnchor");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__textOutline);
    lua_setfield(L, -2, "textOutline");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__setFontStrikeOut);
    lua_setfield(L, -2, "setFontStrikeOut");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__setFontWeight);
    lua_setfield(L, -2, "setFontWeight");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__setFontFixedPitch);
    lua_setfield(L, -2, "setFontFixedPitch");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__delete);
    lua_setfield(L, -2, "delete");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__anchorName);
    lua_setfield(L, -2, "anchorName");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__setAnchor);
    lua_setfield(L, -2, "setAnchor");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__setUnderlineStyle);
    lua_setfield(L, -2, "setUnderlineStyle");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__setToolTip);
    lua_setfield(L, -2, "setToolTip");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__setAnchorName);
    lua_setfield(L, -2, "setAnchorName");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__setFontOverline);
    lua_setfield(L, -2, "setFontOverline");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__tableCellColumnSpan);
    lua_setfield(L, -2, "tableCellColumnSpan");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__setVerticalAlignment);
    lua_setfield(L, -2, "setVerticalAlignment");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__fontStrikeOut);
    lua_setfield(L, -2, "fontStrikeOut");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__setAnchorNames);
    lua_setfield(L, -2, "setAnchorNames");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__toolTip);
    lua_setfield(L, -2, "toolTip");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__setFont);
    lua_setfield(L, -2, "setFont");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__fontUnderline);
    lua_setfield(L, -2, "fontUnderline");
    lua_pushcfunction(L, LuaBinder< QTextCharFormat >::__LuaWrapCall__anchorNames);
    lua_setfield(L, -2, "anchorNames");
    LuaBinder< QTextCharFormat >::lqt_pushenum_VerticalAlignment(L);
    lua_setfield(L, -2, "VerticalAlignment");
    LuaBinder< QTextCharFormat >::lqt_pushenum_UnderlineStyle(L);
    lua_setfield(L, -2, "UnderlineStyle");
    lua_newtable(L);
    lua_pushboolean(L, 1);
    lua_setfield(L, -2, "QTextFormat*");
    lua_setfield(L, -2, "__base");
    lua_pushcfunction(L, lqtL_newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, lqtL_index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, lqtL_gc);
    lua_setfield(L, -2, "__gc");
    lua_pushstring(L, "QTextCharFormat");
    lua_setfield(L, -2, "__qtype");
		lua_setglobal(L, "QTextCharFormat");
  } else {
		lua_pop(L, 1);
	}
  return 0;
}
