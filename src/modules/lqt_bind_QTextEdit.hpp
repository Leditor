#include "lqt_common.hpp"
#include <QTextEdit>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QTextEdit > : public QTextEdit {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__lineWrapColumnOrWidth (lua_State *L);
  static int __LuaWrapCall__lineWrapMode (lua_State *L);
  static int __LuaWrapCall__ensureCursorVisible (lua_State *L);
  static int __LuaWrapCall__cut (lua_State *L);
  static int __LuaWrapCall__selectAll (lua_State *L);
  static int __LuaWrapCall__copy (lua_State *L);
  static int __LuaWrapCall__fontFamily (lua_State *L);
  static int __LuaWrapCall__insertHtml (lua_State *L);
  static int __LuaWrapCall__fontWeight (lua_State *L);
  static int __LuaWrapCall__setLineWrapMode (lua_State *L);
  static int __LuaWrapCall__setAlignment (lua_State *L);
  static int __LuaWrapCall__isUndoRedoEnabled (lua_State *L);
  static int __LuaWrapCall__setReadOnly (lua_State *L);
  static int __LuaWrapCall__document (lua_State *L);
  static int __LuaWrapCall__setDocumentTitle (lua_State *L);
  static int __LuaWrapCall__documentTitle (lua_State *L);
  static int __LuaWrapCall__setTextCursor (lua_State *L);
  static int __LuaWrapCall__setFontPointSize (lua_State *L);
  static int __LuaWrapCall__tabChangesFocus (lua_State *L);
  static int __LuaWrapCall__setWordWrapMode (lua_State *L);
  static int __LuaWrapCall__metaObject (lua_State *L);
  static int __LuaWrapCall__cursorWidth (lua_State *L);
  static int __LuaWrapCall__setDocument (lua_State *L);
  static int __LuaWrapCall__delete (lua_State *L);
  static int __LuaWrapCall__isReadOnly (lua_State *L);
  static int __LuaWrapCall__textCursor (lua_State *L);
  static int __LuaWrapCall__setTextInteractionFlags (lua_State *L);
  static int __LuaWrapCall__zoomIn (lua_State *L);
  static int __LuaWrapCall__undo (lua_State *L);
  static int __LuaWrapCall__paste (lua_State *L);
  static int __LuaWrapCall__fontUnderline (lua_State *L);
  static int __LuaWrapCall__textColor (lua_State *L);
  static int __LuaWrapCall__setFontFamily (lua_State *L);
  static int __LuaWrapCall__createStandardContextMenu (lua_State *L);
  static int __LuaWrapCall__setFontItalic (lua_State *L);
  static int __LuaWrapCall__print (lua_State *L);
  static int __LuaWrapCall__cursorForPosition (lua_State *L);
  static int __LuaWrapCall__autoFormatting (lua_State *L);
  static int __LuaWrapCall__fontItalic (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__trUtf8 (lua_State *L);
  static int __LuaWrapCall__setTabStopWidth (lua_State *L);
  static int __LuaWrapCall__setUndoRedoEnabled (lua_State *L);
  static int __LuaWrapCall__currentCharFormat (lua_State *L);
  static int __LuaWrapCall__find (lua_State *L);
  static int __LuaWrapCall__textInteractionFlags (lua_State *L);
  static int __LuaWrapCall__setCurrentFont (lua_State *L);
  static int __LuaWrapCall__mergeCurrentCharFormat (lua_State *L);
  static int __LuaWrapCall__scrollToAnchor (lua_State *L);
  static int __LuaWrapCall__append (lua_State *L);
  static int __LuaWrapCall__tabStopWidth (lua_State *L);
  static int __LuaWrapCall__loadResource (lua_State *L);
  static int __LuaWrapCall__setLineWrapColumnOrWidth (lua_State *L);
  static int __LuaWrapCall__clear (lua_State *L);
  static int __LuaWrapCall__setTabChangesFocus (lua_State *L);
  static int __LuaWrapCall__redo (lua_State *L);
  static int __LuaWrapCall__extraSelections (lua_State *L);
  static int __LuaWrapCall__acceptRichText (lua_State *L);
  static int __LuaWrapCall__toPlainText (lua_State *L);
  static int __LuaWrapCall__setExtraSelections (lua_State *L);
  static int __LuaWrapCall__currentFont (lua_State *L);
  static int __LuaWrapCall__setAcceptRichText (lua_State *L);
  static int __LuaWrapCall__wordWrapMode (lua_State *L);
  static int __LuaWrapCall__setAutoFormatting (lua_State *L);
  static int __LuaWrapCall__setCurrentCharFormat (lua_State *L);
  static int __LuaWrapCall__setPlainText (lua_State *L);
  static int __LuaWrapCall__setTextColor (lua_State *L);
  static int __LuaWrapCall__fontPointSize (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__new (lua_State *L);
  static int __LuaWrapCall__setFontUnderline (lua_State *L);
  static int __LuaWrapCall__canPaste (lua_State *L);
  static int __LuaWrapCall__anchorAt (lua_State *L);
  static int __LuaWrapCall__toHtml (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__tr (lua_State *L);
  static int __LuaWrapCall__setFontWeight (lua_State *L);
  static int __LuaWrapCall__zoomOut (lua_State *L);
  static int __LuaWrapCall__setHtml (lua_State *L);
  static int __LuaWrapCall__setCursorWidth (lua_State *L);
  static int __LuaWrapCall__overwriteMode (lua_State *L);
  static int __LuaWrapCall__alignment (lua_State *L);
  static int __LuaWrapCall__setText (lua_State *L);
  static int __LuaWrapCall__setOverwriteMode (lua_State *L);
  static int __LuaWrapCall__cursorRect__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__cursorRect__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__cursorRect (lua_State *L);
  static int __LuaWrapCall__insertPlainText (lua_State *L);
  static int __LuaWrapCall__moveCursor (lua_State *L);
protected:
  void styleChange (QStyle& arg1);
public:
protected:
  void focusInEvent (QFocusEvent * arg1);
public:
protected:
  void keyPressEvent (QKeyEvent * arg1);
public:
  const QMetaObject * metaObject () const;
  int devType () const;
  void setVisible (bool arg1);
protected:
  void keyReleaseEvent (QKeyEvent * arg1);
public:
protected:
  void scrollContentsBy (int arg1, int arg2);
public:
protected:
  void dragLeaveEvent (QDragLeaveEvent * arg1);
public:
protected:
  void mousePressEvent (QMouseEvent * arg1);
public:
protected:
  void customEvent (QEvent * arg1);
public:
protected:
  void dragMoveEvent (QDragMoveEvent * arg1);
public:
  QSize sizeHint () const;
protected:
  bool canInsertFromMimeData (const QMimeData * arg1) const;
public:
protected:
  void changeEvent (QEvent * arg1);
public:
protected:
  bool viewportEvent (QEvent * arg1);
public:
protected:
  void mouseReleaseEvent (QMouseEvent * arg1);
public:
protected:
  void tabletEvent (QTabletEvent * arg1);
public:
  QPaintEngine * paintEngine () const;
protected:
  void closeEvent (QCloseEvent * arg1);
public:
protected:
  bool focusNextPrevChild (bool arg1);
public:
protected:
  void dropEvent (QDropEvent * arg1);
public:
protected:
  void fontChange (const QFont& arg1);
public:
protected:
  void insertFromMimeData (const QMimeData * arg1);
public:
protected:
  void showEvent (QShowEvent * arg1);
public:
protected:
  void connectNotify (const char * arg1);
public:
protected:
  void paintEvent (QPaintEvent * arg1);
public:
protected:
  void windowActivationChange (bool arg1);
public:
protected:
  void disconnectNotify (const char * arg1);
public:
protected:
  QMimeData * createMimeDataFromSelection () const;
public:
protected:
  int metric (QPaintDevice::PaintDeviceMetric arg1) const;
public:
  int heightForWidth (int arg1) const;
protected:
  void mouseDoubleClickEvent (QMouseEvent * arg1);
public:
protected:
  void enabledChange (bool arg1);
public:
protected:
  void timerEvent (QTimerEvent * arg1);
public:
  QVariant loadResource (int arg1, const QUrl& arg2);
protected:
  void actionEvent (QActionEvent * arg1);
public:
protected:
  void childEvent (QChildEvent * arg1);
public:
protected:
  void focusOutEvent (QFocusEvent * arg1);
public:
  QSize minimumSizeHint () const;
protected:
  void moveEvent (QMoveEvent * arg1);
public:
  bool eventFilter (QObject * arg1, QEvent * arg2);
protected:
  void inputMethodEvent (QInputMethodEvent * arg1);
public:
protected:
  void wheelEvent (QWheelEvent * arg1);
public:
protected:
  void resizeEvent (QResizeEvent * arg1);
public:
protected:
  void paletteChange (const QPalette& arg1);
public:
protected:
  void languageChange ();
public:
protected:
  bool event (QEvent * arg1);
public:
protected:
  void hideEvent (QHideEvent * arg1);
public:
protected:
  void contextMenuEvent (QContextMenuEvent * arg1);
public:
protected:
  QVariant inputMethodQuery (Qt::InputMethodQuery arg1) const;
public:
protected:
  void mouseMoveEvent (QMouseEvent * arg1);
public:
protected:
  void leaveEvent (QEvent * arg1);
public:
protected:
  void dragEnterEvent (QDragEnterEvent * arg1);
public:
protected:
  void enterEvent (QEvent * arg1);
public:
  ~LuaBinder< QTextEdit > ();
  static int lqt_pushenum_LineWrapMode (lua_State *L);
  static int lqt_pushenum_LineWrapMode_QFLAGS_CREATOR (lua_State *L);
  static int lqt_pushenum_AutoFormattingFlag (lua_State *L);
  static int lqt_pushenum_AutoFormattingFlag_QFLAGS_CREATOR (lua_State *L);
  LuaBinder< QTextEdit > (lua_State *l, QWidget * arg1):QTextEdit(arg1), L(l) {}
  LuaBinder< QTextEdit > (lua_State *l, const QString& arg1, QWidget * arg2):QTextEdit(arg1, arg2), L(l) {}
};

extern "C" int luaopen_QTextEdit (lua_State *L);
