#include "lqt_bind_Qt.hpp"

int LuaBinderQt::lqt_pushenum_WhiteSpaceMode (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "WhiteSpaceNormal");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "WhiteSpaceNormal");
  lua_pushstring(L, "WhiteSpacePre");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "WhiteSpacePre");
  lua_pushstring(L, "WhiteSpaceNoWrap");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "WhiteSpaceNoWrap");
  lua_pushstring(L, "WhiteSpaceModeUndefined");
  lua_rawseti(L, enum_table, -1);
  lua_pushinteger(L, -1);
  lua_setfield(L, enum_table, "WhiteSpaceModeUndefined");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_WhiteSpaceMode_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::WhiteSpaceMode");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_WhiteSpaceMode_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::WhiteSpaceMode>*) + sizeof(QFlags<Qt::WhiteSpaceMode>));
  QFlags<Qt::WhiteSpaceMode> *fl = static_cast<QFlags<Qt::WhiteSpaceMode>*>( static_cast<void*>(&static_cast<QFlags<Qt::WhiteSpaceMode>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::WhiteSpaceMode>(lqtL_toenum(L, i, "Qt::WhiteSpaceMode"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::WhiteSpaceMode>*")) {
		lua_pushstring(L, "QFlags<Qt::WhiteSpaceMode>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_HitTestAccuracy (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "ExactHit");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "ExactHit");
  lua_pushstring(L, "FuzzyHit");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "FuzzyHit");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_HitTestAccuracy_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::HitTestAccuracy");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_HitTestAccuracy_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::HitTestAccuracy>*) + sizeof(QFlags<Qt::HitTestAccuracy>));
  QFlags<Qt::HitTestAccuracy> *fl = static_cast<QFlags<Qt::HitTestAccuracy>*>( static_cast<void*>(&static_cast<QFlags<Qt::HitTestAccuracy>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::HitTestAccuracy>(lqtL_toenum(L, i, "Qt::HitTestAccuracy"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::HitTestAccuracy>*")) {
		lua_pushstring(L, "QFlags<Qt::HitTestAccuracy>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_EventPriority (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "HighEventPriority");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "HighEventPriority");
  lua_pushstring(L, "NormalEventPriority");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "NormalEventPriority");
  lua_pushstring(L, "LowEventPriority");
  lua_rawseti(L, enum_table, -1);
  lua_pushinteger(L, -1);
  lua_setfield(L, enum_table, "LowEventPriority");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_EventPriority_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::EventPriority");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_EventPriority_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::EventPriority>*) + sizeof(QFlags<Qt::EventPriority>));
  QFlags<Qt::EventPriority> *fl = static_cast<QFlags<Qt::EventPriority>*>( static_cast<void*>(&static_cast<QFlags<Qt::EventPriority>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::EventPriority>(lqtL_toenum(L, i, "Qt::EventPriority"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::EventPriority>*")) {
		lua_pushstring(L, "QFlags<Qt::EventPriority>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_TextInteractionFlag (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "NoTextInteraction");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "NoTextInteraction");
  lua_pushstring(L, "TextSelectableByMouse");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "TextSelectableByMouse");
  lua_pushstring(L, "TextSelectableByKeyboard");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "TextSelectableByKeyboard");
  lua_pushstring(L, "LinksAccessibleByMouse");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "LinksAccessibleByMouse");
  lua_pushstring(L, "LinksAccessibleByKeyboard");
  lua_rawseti(L, enum_table, 8);
  lua_pushinteger(L, 8);
  lua_setfield(L, enum_table, "LinksAccessibleByKeyboard");
  lua_pushstring(L, "TextEditable");
  lua_rawseti(L, enum_table, 16);
  lua_pushinteger(L, 16);
  lua_setfield(L, enum_table, "TextEditable");
  lua_pushstring(L, "TextEditorInteraction");
  lua_rawseti(L, enum_table, 19);
  lua_pushinteger(L, 19);
  lua_setfield(L, enum_table, "TextEditorInteraction");
  lua_pushstring(L, "TextBrowserInteraction");
  lua_rawseti(L, enum_table, 13);
  lua_pushinteger(L, 13);
  lua_setfield(L, enum_table, "TextBrowserInteraction");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_TextInteractionFlag_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::TextInteractionFlag");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_TextInteractionFlag_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::TextInteractionFlag>*) + sizeof(QFlags<Qt::TextInteractionFlag>));
  QFlags<Qt::TextInteractionFlag> *fl = static_cast<QFlags<Qt::TextInteractionFlag>*>( static_cast<void*>(&static_cast<QFlags<Qt::TextInteractionFlag>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::TextInteractionFlag>(lqtL_toenum(L, i, "Qt::TextInteractionFlag"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::TextInteractionFlag>*")) {
		lua_pushstring(L, "QFlags<Qt::TextInteractionFlag>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_WindowModality (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "NonModal");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "NonModal");
  lua_pushstring(L, "WindowModal");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "WindowModal");
  lua_pushstring(L, "ApplicationModal");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "ApplicationModal");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_WindowModality_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::WindowModality");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_WindowModality_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::WindowModality>*) + sizeof(QFlags<Qt::WindowModality>));
  QFlags<Qt::WindowModality> *fl = static_cast<QFlags<Qt::WindowModality>*>( static_cast<void*>(&static_cast<QFlags<Qt::WindowModality>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::WindowModality>(lqtL_toenum(L, i, "Qt::WindowModality"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::WindowModality>*")) {
		lua_pushstring(L, "QFlags<Qt::WindowModality>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_MatchFlag (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "MatchExactly");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "MatchExactly");
  lua_pushstring(L, "MatchContains");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "MatchContains");
  lua_pushstring(L, "MatchStartsWith");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "MatchStartsWith");
  lua_pushstring(L, "MatchEndsWith");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "MatchEndsWith");
  lua_pushstring(L, "MatchRegExp");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "MatchRegExp");
  lua_pushstring(L, "MatchWildcard");
  lua_rawseti(L, enum_table, 5);
  lua_pushinteger(L, 5);
  lua_setfield(L, enum_table, "MatchWildcard");
  lua_pushstring(L, "MatchFixedString");
  lua_rawseti(L, enum_table, 8);
  lua_pushinteger(L, 8);
  lua_setfield(L, enum_table, "MatchFixedString");
  lua_pushstring(L, "MatchCaseSensitive");
  lua_rawseti(L, enum_table, 16);
  lua_pushinteger(L, 16);
  lua_setfield(L, enum_table, "MatchCaseSensitive");
  lua_pushstring(L, "MatchWrap");
  lua_rawseti(L, enum_table, 32);
  lua_pushinteger(L, 32);
  lua_setfield(L, enum_table, "MatchWrap");
  lua_pushstring(L, "MatchRecursive");
  lua_rawseti(L, enum_table, 64);
  lua_pushinteger(L, 64);
  lua_setfield(L, enum_table, "MatchRecursive");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_MatchFlag_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::MatchFlag");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_MatchFlag_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::MatchFlag>*) + sizeof(QFlags<Qt::MatchFlag>));
  QFlags<Qt::MatchFlag> *fl = static_cast<QFlags<Qt::MatchFlag>*>( static_cast<void*>(&static_cast<QFlags<Qt::MatchFlag>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::MatchFlag>(lqtL_toenum(L, i, "Qt::MatchFlag"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::MatchFlag>*")) {
		lua_pushstring(L, "QFlags<Qt::MatchFlag>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_ItemFlag (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "ItemIsSelectable");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "ItemIsSelectable");
  lua_pushstring(L, "ItemIsEditable");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "ItemIsEditable");
  lua_pushstring(L, "ItemIsDragEnabled");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "ItemIsDragEnabled");
  lua_pushstring(L, "ItemIsDropEnabled");
  lua_rawseti(L, enum_table, 8);
  lua_pushinteger(L, 8);
  lua_setfield(L, enum_table, "ItemIsDropEnabled");
  lua_pushstring(L, "ItemIsUserCheckable");
  lua_rawseti(L, enum_table, 16);
  lua_pushinteger(L, 16);
  lua_setfield(L, enum_table, "ItemIsUserCheckable");
  lua_pushstring(L, "ItemIsEnabled");
  lua_rawseti(L, enum_table, 32);
  lua_pushinteger(L, 32);
  lua_setfield(L, enum_table, "ItemIsEnabled");
  lua_pushstring(L, "ItemIsTristate");
  lua_rawseti(L, enum_table, 64);
  lua_pushinteger(L, 64);
  lua_setfield(L, enum_table, "ItemIsTristate");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_ItemFlag_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::ItemFlag");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_ItemFlag_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::ItemFlag>*) + sizeof(QFlags<Qt::ItemFlag>));
  QFlags<Qt::ItemFlag> *fl = static_cast<QFlags<Qt::ItemFlag>*>( static_cast<void*>(&static_cast<QFlags<Qt::ItemFlag>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::ItemFlag>(lqtL_toenum(L, i, "Qt::ItemFlag"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::ItemFlag>*")) {
		lua_pushstring(L, "QFlags<Qt::ItemFlag>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_ItemDataRole (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "DisplayRole");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "DisplayRole");
  lua_pushstring(L, "DecorationRole");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "DecorationRole");
  lua_pushstring(L, "EditRole");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "EditRole");
  lua_pushstring(L, "ToolTipRole");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "ToolTipRole");
  lua_pushstring(L, "StatusTipRole");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "StatusTipRole");
  lua_pushstring(L, "WhatsThisRole");
  lua_rawseti(L, enum_table, 5);
  lua_pushinteger(L, 5);
  lua_setfield(L, enum_table, "WhatsThisRole");
  lua_pushstring(L, "FontRole");
  lua_rawseti(L, enum_table, 6);
  lua_pushinteger(L, 6);
  lua_setfield(L, enum_table, "FontRole");
  lua_pushstring(L, "TextAlignmentRole");
  lua_rawseti(L, enum_table, 7);
  lua_pushinteger(L, 7);
  lua_setfield(L, enum_table, "TextAlignmentRole");
  lua_pushstring(L, "BackgroundColorRole");
  lua_rawseti(L, enum_table, 8);
  lua_pushinteger(L, 8);
  lua_setfield(L, enum_table, "BackgroundColorRole");
  lua_pushstring(L, "BackgroundRole");
  lua_rawseti(L, enum_table, 8);
  lua_pushinteger(L, 8);
  lua_setfield(L, enum_table, "BackgroundRole");
  lua_pushstring(L, "TextColorRole");
  lua_rawseti(L, enum_table, 9);
  lua_pushinteger(L, 9);
  lua_setfield(L, enum_table, "TextColorRole");
  lua_pushstring(L, "ForegroundRole");
  lua_rawseti(L, enum_table, 9);
  lua_pushinteger(L, 9);
  lua_setfield(L, enum_table, "ForegroundRole");
  lua_pushstring(L, "CheckStateRole");
  lua_rawseti(L, enum_table, 10);
  lua_pushinteger(L, 10);
  lua_setfield(L, enum_table, "CheckStateRole");
  lua_pushstring(L, "AccessibleTextRole");
  lua_rawseti(L, enum_table, 11);
  lua_pushinteger(L, 11);
  lua_setfield(L, enum_table, "AccessibleTextRole");
  lua_pushstring(L, "AccessibleDescriptionRole");
  lua_rawseti(L, enum_table, 12);
  lua_pushinteger(L, 12);
  lua_setfield(L, enum_table, "AccessibleDescriptionRole");
  lua_pushstring(L, "SizeHintRole");
  lua_rawseti(L, enum_table, 13);
  lua_pushinteger(L, 13);
  lua_setfield(L, enum_table, "SizeHintRole");
  lua_pushstring(L, "UserRole");
  lua_rawseti(L, enum_table, 32);
  lua_pushinteger(L, 32);
  lua_setfield(L, enum_table, "UserRole");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_ItemDataRole_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::ItemDataRole");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_ItemDataRole_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::ItemDataRole>*) + sizeof(QFlags<Qt::ItemDataRole>));
  QFlags<Qt::ItemDataRole> *fl = static_cast<QFlags<Qt::ItemDataRole>*>( static_cast<void*>(&static_cast<QFlags<Qt::ItemDataRole>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::ItemDataRole>(lqtL_toenum(L, i, "Qt::ItemDataRole"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::ItemDataRole>*")) {
		lua_pushstring(L, "QFlags<Qt::ItemDataRole>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_CheckState (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "Unchecked");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "Unchecked");
  lua_pushstring(L, "PartiallyChecked");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "PartiallyChecked");
  lua_pushstring(L, "Checked");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "Checked");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_CheckState_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::CheckState");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_CheckState_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::CheckState>*) + sizeof(QFlags<Qt::CheckState>));
  QFlags<Qt::CheckState> *fl = static_cast<QFlags<Qt::CheckState>*>( static_cast<void*>(&static_cast<QFlags<Qt::CheckState>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::CheckState>(lqtL_toenum(L, i, "Qt::CheckState"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::CheckState>*")) {
		lua_pushstring(L, "QFlags<Qt::CheckState>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_DropAction (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "CopyAction");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "CopyAction");
  lua_pushstring(L, "MoveAction");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "MoveAction");
  lua_pushstring(L, "LinkAction");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "LinkAction");
  lua_pushstring(L, "ActionMask");
  lua_rawseti(L, enum_table, 255);
  lua_pushinteger(L, 255);
  lua_setfield(L, enum_table, "ActionMask");
  lua_pushstring(L, "TargetMoveAction");
  lua_rawseti(L, enum_table, 32770);
  lua_pushinteger(L, 32770);
  lua_setfield(L, enum_table, "TargetMoveAction");
  lua_pushstring(L, "IgnoreAction");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "IgnoreAction");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_DropAction_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::DropAction");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_DropAction_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::DropAction>*) + sizeof(QFlags<Qt::DropAction>));
  QFlags<Qt::DropAction> *fl = static_cast<QFlags<Qt::DropAction>*>( static_cast<void*>(&static_cast<QFlags<Qt::DropAction>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::DropAction>(lqtL_toenum(L, i, "Qt::DropAction"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::DropAction>*")) {
		lua_pushstring(L, "QFlags<Qt::DropAction>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_LayoutDirection (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "LeftToRight");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "LeftToRight");
  lua_pushstring(L, "RightToLeft");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "RightToLeft");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_LayoutDirection_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::LayoutDirection");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_LayoutDirection_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::LayoutDirection>*) + sizeof(QFlags<Qt::LayoutDirection>));
  QFlags<Qt::LayoutDirection> *fl = static_cast<QFlags<Qt::LayoutDirection>*>( static_cast<void*>(&static_cast<QFlags<Qt::LayoutDirection>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::LayoutDirection>(lqtL_toenum(L, i, "Qt::LayoutDirection"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::LayoutDirection>*")) {
		lua_pushstring(L, "QFlags<Qt::LayoutDirection>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_ToolButtonStyle (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "ToolButtonIconOnly");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "ToolButtonIconOnly");
  lua_pushstring(L, "ToolButtonTextOnly");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "ToolButtonTextOnly");
  lua_pushstring(L, "ToolButtonTextBesideIcon");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "ToolButtonTextBesideIcon");
  lua_pushstring(L, "ToolButtonTextUnderIcon");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "ToolButtonTextUnderIcon");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_ToolButtonStyle_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::ToolButtonStyle");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_ToolButtonStyle_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::ToolButtonStyle>*) + sizeof(QFlags<Qt::ToolButtonStyle>));
  QFlags<Qt::ToolButtonStyle> *fl = static_cast<QFlags<Qt::ToolButtonStyle>*>( static_cast<void*>(&static_cast<QFlags<Qt::ToolButtonStyle>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::ToolButtonStyle>(lqtL_toenum(L, i, "Qt::ToolButtonStyle"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::ToolButtonStyle>*")) {
		lua_pushstring(L, "QFlags<Qt::ToolButtonStyle>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_InputMethodQuery (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "ImMicroFocus");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "ImMicroFocus");
  lua_pushstring(L, "ImFont");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "ImFont");
  lua_pushstring(L, "ImCursorPosition");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "ImCursorPosition");
  lua_pushstring(L, "ImSurroundingText");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "ImSurroundingText");
  lua_pushstring(L, "ImCurrentSelection");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "ImCurrentSelection");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_InputMethodQuery_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::InputMethodQuery");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_InputMethodQuery_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::InputMethodQuery>*) + sizeof(QFlags<Qt::InputMethodQuery>));
  QFlags<Qt::InputMethodQuery> *fl = static_cast<QFlags<Qt::InputMethodQuery>*>( static_cast<void*>(&static_cast<QFlags<Qt::InputMethodQuery>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::InputMethodQuery>(lqtL_toenum(L, i, "Qt::InputMethodQuery"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::InputMethodQuery>*")) {
		lua_pushstring(L, "QFlags<Qt::InputMethodQuery>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_ContextMenuPolicy (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "NoContextMenu");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "NoContextMenu");
  lua_pushstring(L, "DefaultContextMenu");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "DefaultContextMenu");
  lua_pushstring(L, "ActionsContextMenu");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "ActionsContextMenu");
  lua_pushstring(L, "CustomContextMenu");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "CustomContextMenu");
  lua_pushstring(L, "PreventContextMenu");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "PreventContextMenu");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_ContextMenuPolicy_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::ContextMenuPolicy");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_ContextMenuPolicy_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::ContextMenuPolicy>*) + sizeof(QFlags<Qt::ContextMenuPolicy>));
  QFlags<Qt::ContextMenuPolicy> *fl = static_cast<QFlags<Qt::ContextMenuPolicy>*>( static_cast<void*>(&static_cast<QFlags<Qt::ContextMenuPolicy>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::ContextMenuPolicy>(lqtL_toenum(L, i, "Qt::ContextMenuPolicy"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::ContextMenuPolicy>*")) {
		lua_pushstring(L, "QFlags<Qt::ContextMenuPolicy>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_FocusReason (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "MouseFocusReason");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "MouseFocusReason");
  lua_pushstring(L, "TabFocusReason");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "TabFocusReason");
  lua_pushstring(L, "BacktabFocusReason");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "BacktabFocusReason");
  lua_pushstring(L, "ActiveWindowFocusReason");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "ActiveWindowFocusReason");
  lua_pushstring(L, "PopupFocusReason");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "PopupFocusReason");
  lua_pushstring(L, "ShortcutFocusReason");
  lua_rawseti(L, enum_table, 5);
  lua_pushinteger(L, 5);
  lua_setfield(L, enum_table, "ShortcutFocusReason");
  lua_pushstring(L, "MenuBarFocusReason");
  lua_rawseti(L, enum_table, 6);
  lua_pushinteger(L, 6);
  lua_setfield(L, enum_table, "MenuBarFocusReason");
  lua_pushstring(L, "OtherFocusReason");
  lua_rawseti(L, enum_table, 7);
  lua_pushinteger(L, 7);
  lua_setfield(L, enum_table, "OtherFocusReason");
  lua_pushstring(L, "NoFocusReason");
  lua_rawseti(L, enum_table, 8);
  lua_pushinteger(L, 8);
  lua_setfield(L, enum_table, "NoFocusReason");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_FocusReason_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::FocusReason");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_FocusReason_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::FocusReason>*) + sizeof(QFlags<Qt::FocusReason>));
  QFlags<Qt::FocusReason> *fl = static_cast<QFlags<Qt::FocusReason>*>( static_cast<void*>(&static_cast<QFlags<Qt::FocusReason>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::FocusReason>(lqtL_toenum(L, i, "Qt::FocusReason"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::FocusReason>*")) {
		lua_pushstring(L, "QFlags<Qt::FocusReason>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_Axis (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "XAxis");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "XAxis");
  lua_pushstring(L, "YAxis");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "YAxis");
  lua_pushstring(L, "ZAxis");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "ZAxis");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_Axis_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::Axis");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_Axis_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::Axis>*) + sizeof(QFlags<Qt::Axis>));
  QFlags<Qt::Axis> *fl = static_cast<QFlags<Qt::Axis>*>( static_cast<void*>(&static_cast<QFlags<Qt::Axis>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::Axis>(lqtL_toenum(L, i, "Qt::Axis"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::Axis>*")) {
		lua_pushstring(L, "QFlags<Qt::Axis>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_TransformationMode (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "FastTransformation");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "FastTransformation");
  lua_pushstring(L, "SmoothTransformation");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "SmoothTransformation");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_TransformationMode_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::TransformationMode");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_TransformationMode_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::TransformationMode>*) + sizeof(QFlags<Qt::TransformationMode>));
  QFlags<Qt::TransformationMode> *fl = static_cast<QFlags<Qt::TransformationMode>*>( static_cast<void*>(&static_cast<QFlags<Qt::TransformationMode>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::TransformationMode>(lqtL_toenum(L, i, "Qt::TransformationMode"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::TransformationMode>*")) {
		lua_pushstring(L, "QFlags<Qt::TransformationMode>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_ItemSelectionMode (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "ContainsItemShape");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "ContainsItemShape");
  lua_pushstring(L, "IntersectsItemShape");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "IntersectsItemShape");
  lua_pushstring(L, "ContainsItemBoundingRect");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "ContainsItemBoundingRect");
  lua_pushstring(L, "IntersectsItemBoundingRect");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "IntersectsItemBoundingRect");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_ItemSelectionMode_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::ItemSelectionMode");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_ItemSelectionMode_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::ItemSelectionMode>*) + sizeof(QFlags<Qt::ItemSelectionMode>));
  QFlags<Qt::ItemSelectionMode> *fl = static_cast<QFlags<Qt::ItemSelectionMode>*>( static_cast<void*>(&static_cast<QFlags<Qt::ItemSelectionMode>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::ItemSelectionMode>(lqtL_toenum(L, i, "Qt::ItemSelectionMode"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::ItemSelectionMode>*")) {
		lua_pushstring(L, "QFlags<Qt::ItemSelectionMode>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_ClipOperation (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "NoClip");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "NoClip");
  lua_pushstring(L, "ReplaceClip");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "ReplaceClip");
  lua_pushstring(L, "IntersectClip");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "IntersectClip");
  lua_pushstring(L, "UniteClip");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "UniteClip");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_ClipOperation_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::ClipOperation");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_ClipOperation_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::ClipOperation>*) + sizeof(QFlags<Qt::ClipOperation>));
  QFlags<Qt::ClipOperation> *fl = static_cast<QFlags<Qt::ClipOperation>*>( static_cast<void*>(&static_cast<QFlags<Qt::ClipOperation>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::ClipOperation>(lqtL_toenum(L, i, "Qt::ClipOperation"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::ClipOperation>*")) {
		lua_pushstring(L, "QFlags<Qt::ClipOperation>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_MaskMode (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "MaskInColor");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "MaskInColor");
  lua_pushstring(L, "MaskOutColor");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "MaskOutColor");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_MaskMode_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::MaskMode");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_MaskMode_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::MaskMode>*) + sizeof(QFlags<Qt::MaskMode>));
  QFlags<Qt::MaskMode> *fl = static_cast<QFlags<Qt::MaskMode>*>( static_cast<void*>(&static_cast<QFlags<Qt::MaskMode>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::MaskMode>(lqtL_toenum(L, i, "Qt::MaskMode"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::MaskMode>*")) {
		lua_pushstring(L, "QFlags<Qt::MaskMode>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_FillRule (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "OddEvenFill");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "OddEvenFill");
  lua_pushstring(L, "WindingFill");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "WindingFill");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_FillRule_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::FillRule");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_FillRule_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::FillRule>*) + sizeof(QFlags<Qt::FillRule>));
  QFlags<Qt::FillRule> *fl = static_cast<QFlags<Qt::FillRule>*>( static_cast<void*>(&static_cast<QFlags<Qt::FillRule>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::FillRule>(lqtL_toenum(L, i, "Qt::FillRule"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::FillRule>*")) {
		lua_pushstring(L, "QFlags<Qt::FillRule>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_ShortcutContext (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "WidgetShortcut");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "WidgetShortcut");
  lua_pushstring(L, "WindowShortcut");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "WindowShortcut");
  lua_pushstring(L, "ApplicationShortcut");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "ApplicationShortcut");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_ShortcutContext_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::ShortcutContext");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_ShortcutContext_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::ShortcutContext>*) + sizeof(QFlags<Qt::ShortcutContext>));
  QFlags<Qt::ShortcutContext> *fl = static_cast<QFlags<Qt::ShortcutContext>*>( static_cast<void*>(&static_cast<QFlags<Qt::ShortcutContext>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::ShortcutContext>(lqtL_toenum(L, i, "Qt::ShortcutContext"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::ShortcutContext>*")) {
		lua_pushstring(L, "QFlags<Qt::ShortcutContext>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_ConnectionType (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "AutoConnection");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "AutoConnection");
  lua_pushstring(L, "DirectConnection");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "DirectConnection");
  lua_pushstring(L, "QueuedConnection");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "QueuedConnection");
  lua_pushstring(L, "AutoCompatConnection");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "AutoCompatConnection");
  lua_pushstring(L, "BlockingQueuedConnection");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "BlockingQueuedConnection");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_ConnectionType_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::ConnectionType");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_ConnectionType_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::ConnectionType>*) + sizeof(QFlags<Qt::ConnectionType>));
  QFlags<Qt::ConnectionType> *fl = static_cast<QFlags<Qt::ConnectionType>*>( static_cast<void*>(&static_cast<QFlags<Qt::ConnectionType>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::ConnectionType>(lqtL_toenum(L, i, "Qt::ConnectionType"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::ConnectionType>*")) {
		lua_pushstring(L, "QFlags<Qt::ConnectionType>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_Corner (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "TopLeftCorner");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "TopLeftCorner");
  lua_pushstring(L, "TopRightCorner");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "TopRightCorner");
  lua_pushstring(L, "BottomLeftCorner");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "BottomLeftCorner");
  lua_pushstring(L, "BottomRightCorner");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "BottomRightCorner");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_Corner_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::Corner");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_Corner_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::Corner>*) + sizeof(QFlags<Qt::Corner>));
  QFlags<Qt::Corner> *fl = static_cast<QFlags<Qt::Corner>*>( static_cast<void*>(&static_cast<QFlags<Qt::Corner>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::Corner>(lqtL_toenum(L, i, "Qt::Corner"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::Corner>*")) {
		lua_pushstring(L, "QFlags<Qt::Corner>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_CaseSensitivity (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "CaseInsensitive");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "CaseInsensitive");
  lua_pushstring(L, "CaseSensitive");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "CaseSensitive");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_CaseSensitivity_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::CaseSensitivity");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_CaseSensitivity_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::CaseSensitivity>*) + sizeof(QFlags<Qt::CaseSensitivity>));
  QFlags<Qt::CaseSensitivity> *fl = static_cast<QFlags<Qt::CaseSensitivity>*>( static_cast<void*>(&static_cast<QFlags<Qt::CaseSensitivity>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::CaseSensitivity>(lqtL_toenum(L, i, "Qt::CaseSensitivity"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::CaseSensitivity>*")) {
		lua_pushstring(L, "QFlags<Qt::CaseSensitivity>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_ScrollBarPolicy (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "ScrollBarAsNeeded");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "ScrollBarAsNeeded");
  lua_pushstring(L, "ScrollBarAlwaysOff");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "ScrollBarAlwaysOff");
  lua_pushstring(L, "ScrollBarAlwaysOn");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "ScrollBarAlwaysOn");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_ScrollBarPolicy_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::ScrollBarPolicy");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_ScrollBarPolicy_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::ScrollBarPolicy>*) + sizeof(QFlags<Qt::ScrollBarPolicy>));
  QFlags<Qt::ScrollBarPolicy> *fl = static_cast<QFlags<Qt::ScrollBarPolicy>*>( static_cast<void*>(&static_cast<QFlags<Qt::ScrollBarPolicy>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::ScrollBarPolicy>(lqtL_toenum(L, i, "Qt::ScrollBarPolicy"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::ScrollBarPolicy>*")) {
		lua_pushstring(L, "QFlags<Qt::ScrollBarPolicy>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_DayOfWeek (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "Monday");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "Monday");
  lua_pushstring(L, "Tuesday");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "Tuesday");
  lua_pushstring(L, "Wednesday");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "Wednesday");
  lua_pushstring(L, "Thursday");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "Thursday");
  lua_pushstring(L, "Friday");
  lua_rawseti(L, enum_table, 5);
  lua_pushinteger(L, 5);
  lua_setfield(L, enum_table, "Friday");
  lua_pushstring(L, "Saturday");
  lua_rawseti(L, enum_table, 6);
  lua_pushinteger(L, 6);
  lua_setfield(L, enum_table, "Saturday");
  lua_pushstring(L, "Sunday");
  lua_rawseti(L, enum_table, 7);
  lua_pushinteger(L, 7);
  lua_setfield(L, enum_table, "Sunday");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_DayOfWeek_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::DayOfWeek");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_DayOfWeek_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::DayOfWeek>*) + sizeof(QFlags<Qt::DayOfWeek>));
  QFlags<Qt::DayOfWeek> *fl = static_cast<QFlags<Qt::DayOfWeek>*>( static_cast<void*>(&static_cast<QFlags<Qt::DayOfWeek>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::DayOfWeek>(lqtL_toenum(L, i, "Qt::DayOfWeek"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::DayOfWeek>*")) {
		lua_pushstring(L, "QFlags<Qt::DayOfWeek>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_TimeSpec (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "LocalTime");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "LocalTime");
  lua_pushstring(L, "UTC");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "UTC");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_TimeSpec_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::TimeSpec");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_TimeSpec_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::TimeSpec>*) + sizeof(QFlags<Qt::TimeSpec>));
  QFlags<Qt::TimeSpec> *fl = static_cast<QFlags<Qt::TimeSpec>*>( static_cast<void*>(&static_cast<QFlags<Qt::TimeSpec>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::TimeSpec>(lqtL_toenum(L, i, "Qt::TimeSpec"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::TimeSpec>*")) {
		lua_pushstring(L, "QFlags<Qt::TimeSpec>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_DateFormat (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "TextDate");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "TextDate");
  lua_pushstring(L, "ISODate");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "ISODate");
  lua_pushstring(L, "SystemLocaleDate");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "SystemLocaleDate");
  lua_pushstring(L, "LocalDate");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "LocalDate");
  lua_pushstring(L, "LocaleDate");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "LocaleDate");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_DateFormat_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::DateFormat");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_DateFormat_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::DateFormat>*) + sizeof(QFlags<Qt::DateFormat>));
  QFlags<Qt::DateFormat> *fl = static_cast<QFlags<Qt::DateFormat>*>( static_cast<void*>(&static_cast<QFlags<Qt::DateFormat>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::DateFormat>(lqtL_toenum(L, i, "Qt::DateFormat"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::DateFormat>*")) {
		lua_pushstring(L, "QFlags<Qt::DateFormat>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_ToolBarAreaSizes (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "NToolBarAreas");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "NToolBarAreas");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_ToolBarAreaSizes_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::ToolBarAreaSizes");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_ToolBarAreaSizes_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::ToolBarAreaSizes>*) + sizeof(QFlags<Qt::ToolBarAreaSizes>));
  QFlags<Qt::ToolBarAreaSizes> *fl = static_cast<QFlags<Qt::ToolBarAreaSizes>*>( static_cast<void*>(&static_cast<QFlags<Qt::ToolBarAreaSizes>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::ToolBarAreaSizes>(lqtL_toenum(L, i, "Qt::ToolBarAreaSizes"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::ToolBarAreaSizes>*")) {
		lua_pushstring(L, "QFlags<Qt::ToolBarAreaSizes>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_ToolBarArea (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "LeftToolBarArea");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "LeftToolBarArea");
  lua_pushstring(L, "RightToolBarArea");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "RightToolBarArea");
  lua_pushstring(L, "TopToolBarArea");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "TopToolBarArea");
  lua_pushstring(L, "BottomToolBarArea");
  lua_rawseti(L, enum_table, 8);
  lua_pushinteger(L, 8);
  lua_setfield(L, enum_table, "BottomToolBarArea");
  lua_pushstring(L, "ToolBarArea_Mask");
  lua_rawseti(L, enum_table, 15);
  lua_pushinteger(L, 15);
  lua_setfield(L, enum_table, "ToolBarArea_Mask");
  lua_pushstring(L, "AllToolBarAreas");
  lua_rawseti(L, enum_table, 15);
  lua_pushinteger(L, 15);
  lua_setfield(L, enum_table, "AllToolBarAreas");
  lua_pushstring(L, "NoToolBarArea");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "NoToolBarArea");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_ToolBarArea_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::ToolBarArea");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_ToolBarArea_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::ToolBarArea>*) + sizeof(QFlags<Qt::ToolBarArea>));
  QFlags<Qt::ToolBarArea> *fl = static_cast<QFlags<Qt::ToolBarArea>*>( static_cast<void*>(&static_cast<QFlags<Qt::ToolBarArea>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::ToolBarArea>(lqtL_toenum(L, i, "Qt::ToolBarArea"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::ToolBarArea>*")) {
		lua_pushstring(L, "QFlags<Qt::ToolBarArea>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_DockWidgetAreaSizes (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "NDockWidgetAreas");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "NDockWidgetAreas");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_DockWidgetAreaSizes_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::DockWidgetAreaSizes");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_DockWidgetAreaSizes_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::DockWidgetAreaSizes>*) + sizeof(QFlags<Qt::DockWidgetAreaSizes>));
  QFlags<Qt::DockWidgetAreaSizes> *fl = static_cast<QFlags<Qt::DockWidgetAreaSizes>*>( static_cast<void*>(&static_cast<QFlags<Qt::DockWidgetAreaSizes>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::DockWidgetAreaSizes>(lqtL_toenum(L, i, "Qt::DockWidgetAreaSizes"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::DockWidgetAreaSizes>*")) {
		lua_pushstring(L, "QFlags<Qt::DockWidgetAreaSizes>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_DockWidgetArea (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "LeftDockWidgetArea");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "LeftDockWidgetArea");
  lua_pushstring(L, "RightDockWidgetArea");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "RightDockWidgetArea");
  lua_pushstring(L, "TopDockWidgetArea");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "TopDockWidgetArea");
  lua_pushstring(L, "BottomDockWidgetArea");
  lua_rawseti(L, enum_table, 8);
  lua_pushinteger(L, 8);
  lua_setfield(L, enum_table, "BottomDockWidgetArea");
  lua_pushstring(L, "DockWidgetArea_Mask");
  lua_rawseti(L, enum_table, 15);
  lua_pushinteger(L, 15);
  lua_setfield(L, enum_table, "DockWidgetArea_Mask");
  lua_pushstring(L, "AllDockWidgetAreas");
  lua_rawseti(L, enum_table, 15);
  lua_pushinteger(L, 15);
  lua_setfield(L, enum_table, "AllDockWidgetAreas");
  lua_pushstring(L, "NoDockWidgetArea");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "NoDockWidgetArea");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_DockWidgetArea_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::DockWidgetArea");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_DockWidgetArea_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::DockWidgetArea>*) + sizeof(QFlags<Qt::DockWidgetArea>));
  QFlags<Qt::DockWidgetArea> *fl = static_cast<QFlags<Qt::DockWidgetArea>*>( static_cast<void*>(&static_cast<QFlags<Qt::DockWidgetArea>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::DockWidgetArea>(lqtL_toenum(L, i, "Qt::DockWidgetArea"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::DockWidgetArea>*")) {
		lua_pushstring(L, "QFlags<Qt::DockWidgetArea>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_AnchorAttribute (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "AnchorName");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "AnchorName");
  lua_pushstring(L, "AnchorHref");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "AnchorHref");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_AnchorAttribute_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::AnchorAttribute");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_AnchorAttribute_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::AnchorAttribute>*) + sizeof(QFlags<Qt::AnchorAttribute>));
  QFlags<Qt::AnchorAttribute> *fl = static_cast<QFlags<Qt::AnchorAttribute>*>( static_cast<void*>(&static_cast<QFlags<Qt::AnchorAttribute>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::AnchorAttribute>(lqtL_toenum(L, i, "Qt::AnchorAttribute"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::AnchorAttribute>*")) {
		lua_pushstring(L, "QFlags<Qt::AnchorAttribute>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_AspectRatioMode (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "IgnoreAspectRatio");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "IgnoreAspectRatio");
  lua_pushstring(L, "KeepAspectRatio");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "KeepAspectRatio");
  lua_pushstring(L, "KeepAspectRatioByExpanding");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "KeepAspectRatioByExpanding");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_AspectRatioMode_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::AspectRatioMode");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_AspectRatioMode_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::AspectRatioMode>*) + sizeof(QFlags<Qt::AspectRatioMode>));
  QFlags<Qt::AspectRatioMode> *fl = static_cast<QFlags<Qt::AspectRatioMode>*>( static_cast<void*>(&static_cast<QFlags<Qt::AspectRatioMode>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::AspectRatioMode>(lqtL_toenum(L, i, "Qt::AspectRatioMode"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::AspectRatioMode>*")) {
		lua_pushstring(L, "QFlags<Qt::AspectRatioMode>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_TextFormat (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "PlainText");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "PlainText");
  lua_pushstring(L, "RichText");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "RichText");
  lua_pushstring(L, "AutoText");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "AutoText");
  lua_pushstring(L, "LogText");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "LogText");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_TextFormat_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::TextFormat");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_TextFormat_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::TextFormat>*) + sizeof(QFlags<Qt::TextFormat>));
  QFlags<Qt::TextFormat> *fl = static_cast<QFlags<Qt::TextFormat>*>( static_cast<void*>(&static_cast<QFlags<Qt::TextFormat>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::TextFormat>(lqtL_toenum(L, i, "Qt::TextFormat"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::TextFormat>*")) {
		lua_pushstring(L, "QFlags<Qt::TextFormat>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_CursorShape (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "ArrowCursor");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "ArrowCursor");
  lua_pushstring(L, "UpArrowCursor");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "UpArrowCursor");
  lua_pushstring(L, "CrossCursor");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "CrossCursor");
  lua_pushstring(L, "WaitCursor");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "WaitCursor");
  lua_pushstring(L, "IBeamCursor");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "IBeamCursor");
  lua_pushstring(L, "SizeVerCursor");
  lua_rawseti(L, enum_table, 5);
  lua_pushinteger(L, 5);
  lua_setfield(L, enum_table, "SizeVerCursor");
  lua_pushstring(L, "SizeHorCursor");
  lua_rawseti(L, enum_table, 6);
  lua_pushinteger(L, 6);
  lua_setfield(L, enum_table, "SizeHorCursor");
  lua_pushstring(L, "SizeBDiagCursor");
  lua_rawseti(L, enum_table, 7);
  lua_pushinteger(L, 7);
  lua_setfield(L, enum_table, "SizeBDiagCursor");
  lua_pushstring(L, "SizeFDiagCursor");
  lua_rawseti(L, enum_table, 8);
  lua_pushinteger(L, 8);
  lua_setfield(L, enum_table, "SizeFDiagCursor");
  lua_pushstring(L, "SizeAllCursor");
  lua_rawseti(L, enum_table, 9);
  lua_pushinteger(L, 9);
  lua_setfield(L, enum_table, "SizeAllCursor");
  lua_pushstring(L, "BlankCursor");
  lua_rawseti(L, enum_table, 10);
  lua_pushinteger(L, 10);
  lua_setfield(L, enum_table, "BlankCursor");
  lua_pushstring(L, "SplitVCursor");
  lua_rawseti(L, enum_table, 11);
  lua_pushinteger(L, 11);
  lua_setfield(L, enum_table, "SplitVCursor");
  lua_pushstring(L, "SplitHCursor");
  lua_rawseti(L, enum_table, 12);
  lua_pushinteger(L, 12);
  lua_setfield(L, enum_table, "SplitHCursor");
  lua_pushstring(L, "PointingHandCursor");
  lua_rawseti(L, enum_table, 13);
  lua_pushinteger(L, 13);
  lua_setfield(L, enum_table, "PointingHandCursor");
  lua_pushstring(L, "ForbiddenCursor");
  lua_rawseti(L, enum_table, 14);
  lua_pushinteger(L, 14);
  lua_setfield(L, enum_table, "ForbiddenCursor");
  lua_pushstring(L, "WhatsThisCursor");
  lua_rawseti(L, enum_table, 15);
  lua_pushinteger(L, 15);
  lua_setfield(L, enum_table, "WhatsThisCursor");
  lua_pushstring(L, "BusyCursor");
  lua_rawseti(L, enum_table, 16);
  lua_pushinteger(L, 16);
  lua_setfield(L, enum_table, "BusyCursor");
  lua_pushstring(L, "OpenHandCursor");
  lua_rawseti(L, enum_table, 17);
  lua_pushinteger(L, 17);
  lua_setfield(L, enum_table, "OpenHandCursor");
  lua_pushstring(L, "ClosedHandCursor");
  lua_rawseti(L, enum_table, 18);
  lua_pushinteger(L, 18);
  lua_setfield(L, enum_table, "ClosedHandCursor");
  lua_pushstring(L, "LastCursor");
  lua_rawseti(L, enum_table, 18);
  lua_pushinteger(L, 18);
  lua_setfield(L, enum_table, "LastCursor");
  lua_pushstring(L, "BitmapCursor");
  lua_rawseti(L, enum_table, 24);
  lua_pushinteger(L, 24);
  lua_setfield(L, enum_table, "BitmapCursor");
  lua_pushstring(L, "CustomCursor");
  lua_rawseti(L, enum_table, 25);
  lua_pushinteger(L, 25);
  lua_setfield(L, enum_table, "CustomCursor");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_CursorShape_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::CursorShape");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_CursorShape_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::CursorShape>*) + sizeof(QFlags<Qt::CursorShape>));
  QFlags<Qt::CursorShape> *fl = static_cast<QFlags<Qt::CursorShape>*>( static_cast<void*>(&static_cast<QFlags<Qt::CursorShape>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::CursorShape>(lqtL_toenum(L, i, "Qt::CursorShape"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::CursorShape>*")) {
		lua_pushstring(L, "QFlags<Qt::CursorShape>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_UIEffect (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "UI_General");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "UI_General");
  lua_pushstring(L, "UI_AnimateMenu");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "UI_AnimateMenu");
  lua_pushstring(L, "UI_FadeMenu");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "UI_FadeMenu");
  lua_pushstring(L, "UI_AnimateCombo");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "UI_AnimateCombo");
  lua_pushstring(L, "UI_AnimateTooltip");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "UI_AnimateTooltip");
  lua_pushstring(L, "UI_FadeTooltip");
  lua_rawseti(L, enum_table, 5);
  lua_pushinteger(L, 5);
  lua_setfield(L, enum_table, "UI_FadeTooltip");
  lua_pushstring(L, "UI_AnimateToolBox");
  lua_rawseti(L, enum_table, 6);
  lua_pushinteger(L, 6);
  lua_setfield(L, enum_table, "UI_AnimateToolBox");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_UIEffect_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::UIEffect");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_UIEffect_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::UIEffect>*) + sizeof(QFlags<Qt::UIEffect>));
  QFlags<Qt::UIEffect> *fl = static_cast<QFlags<Qt::UIEffect>*>( static_cast<void*>(&static_cast<QFlags<Qt::UIEffect>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::UIEffect>(lqtL_toenum(L, i, "Qt::UIEffect"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::UIEffect>*")) {
		lua_pushstring(L, "QFlags<Qt::UIEffect>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_BrushStyle (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "NoBrush");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "NoBrush");
  lua_pushstring(L, "SolidPattern");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "SolidPattern");
  lua_pushstring(L, "Dense1Pattern");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "Dense1Pattern");
  lua_pushstring(L, "Dense2Pattern");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "Dense2Pattern");
  lua_pushstring(L, "Dense3Pattern");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "Dense3Pattern");
  lua_pushstring(L, "Dense4Pattern");
  lua_rawseti(L, enum_table, 5);
  lua_pushinteger(L, 5);
  lua_setfield(L, enum_table, "Dense4Pattern");
  lua_pushstring(L, "Dense5Pattern");
  lua_rawseti(L, enum_table, 6);
  lua_pushinteger(L, 6);
  lua_setfield(L, enum_table, "Dense5Pattern");
  lua_pushstring(L, "Dense6Pattern");
  lua_rawseti(L, enum_table, 7);
  lua_pushinteger(L, 7);
  lua_setfield(L, enum_table, "Dense6Pattern");
  lua_pushstring(L, "Dense7Pattern");
  lua_rawseti(L, enum_table, 8);
  lua_pushinteger(L, 8);
  lua_setfield(L, enum_table, "Dense7Pattern");
  lua_pushstring(L, "HorPattern");
  lua_rawseti(L, enum_table, 9);
  lua_pushinteger(L, 9);
  lua_setfield(L, enum_table, "HorPattern");
  lua_pushstring(L, "VerPattern");
  lua_rawseti(L, enum_table, 10);
  lua_pushinteger(L, 10);
  lua_setfield(L, enum_table, "VerPattern");
  lua_pushstring(L, "CrossPattern");
  lua_rawseti(L, enum_table, 11);
  lua_pushinteger(L, 11);
  lua_setfield(L, enum_table, "CrossPattern");
  lua_pushstring(L, "BDiagPattern");
  lua_rawseti(L, enum_table, 12);
  lua_pushinteger(L, 12);
  lua_setfield(L, enum_table, "BDiagPattern");
  lua_pushstring(L, "FDiagPattern");
  lua_rawseti(L, enum_table, 13);
  lua_pushinteger(L, 13);
  lua_setfield(L, enum_table, "FDiagPattern");
  lua_pushstring(L, "DiagCrossPattern");
  lua_rawseti(L, enum_table, 14);
  lua_pushinteger(L, 14);
  lua_setfield(L, enum_table, "DiagCrossPattern");
  lua_pushstring(L, "LinearGradientPattern");
  lua_rawseti(L, enum_table, 15);
  lua_pushinteger(L, 15);
  lua_setfield(L, enum_table, "LinearGradientPattern");
  lua_pushstring(L, "RadialGradientPattern");
  lua_rawseti(L, enum_table, 16);
  lua_pushinteger(L, 16);
  lua_setfield(L, enum_table, "RadialGradientPattern");
  lua_pushstring(L, "ConicalGradientPattern");
  lua_rawseti(L, enum_table, 17);
  lua_pushinteger(L, 17);
  lua_setfield(L, enum_table, "ConicalGradientPattern");
  lua_pushstring(L, "TexturePattern");
  lua_rawseti(L, enum_table, 24);
  lua_pushinteger(L, 24);
  lua_setfield(L, enum_table, "TexturePattern");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_BrushStyle_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::BrushStyle");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_BrushStyle_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::BrushStyle>*) + sizeof(QFlags<Qt::BrushStyle>));
  QFlags<Qt::BrushStyle> *fl = static_cast<QFlags<Qt::BrushStyle>*>( static_cast<void*>(&static_cast<QFlags<Qt::BrushStyle>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::BrushStyle>(lqtL_toenum(L, i, "Qt::BrushStyle"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::BrushStyle>*")) {
		lua_pushstring(L, "QFlags<Qt::BrushStyle>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_PenJoinStyle (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "MiterJoin");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "MiterJoin");
  lua_pushstring(L, "BevelJoin");
  lua_rawseti(L, enum_table, 64);
  lua_pushinteger(L, 64);
  lua_setfield(L, enum_table, "BevelJoin");
  lua_pushstring(L, "RoundJoin");
  lua_rawseti(L, enum_table, 128);
  lua_pushinteger(L, 128);
  lua_setfield(L, enum_table, "RoundJoin");
  lua_pushstring(L, "SvgMiterJoin");
  lua_rawseti(L, enum_table, 256);
  lua_pushinteger(L, 256);
  lua_setfield(L, enum_table, "SvgMiterJoin");
  lua_pushstring(L, "MPenJoinStyle");
  lua_rawseti(L, enum_table, 448);
  lua_pushinteger(L, 448);
  lua_setfield(L, enum_table, "MPenJoinStyle");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_PenJoinStyle_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::PenJoinStyle");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_PenJoinStyle_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::PenJoinStyle>*) + sizeof(QFlags<Qt::PenJoinStyle>));
  QFlags<Qt::PenJoinStyle> *fl = static_cast<QFlags<Qt::PenJoinStyle>*>( static_cast<void*>(&static_cast<QFlags<Qt::PenJoinStyle>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::PenJoinStyle>(lqtL_toenum(L, i, "Qt::PenJoinStyle"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::PenJoinStyle>*")) {
		lua_pushstring(L, "QFlags<Qt::PenJoinStyle>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_PenCapStyle (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "FlatCap");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "FlatCap");
  lua_pushstring(L, "SquareCap");
  lua_rawseti(L, enum_table, 16);
  lua_pushinteger(L, 16);
  lua_setfield(L, enum_table, "SquareCap");
  lua_pushstring(L, "RoundCap");
  lua_rawseti(L, enum_table, 32);
  lua_pushinteger(L, 32);
  lua_setfield(L, enum_table, "RoundCap");
  lua_pushstring(L, "MPenCapStyle");
  lua_rawseti(L, enum_table, 48);
  lua_pushinteger(L, 48);
  lua_setfield(L, enum_table, "MPenCapStyle");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_PenCapStyle_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::PenCapStyle");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_PenCapStyle_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::PenCapStyle>*) + sizeof(QFlags<Qt::PenCapStyle>));
  QFlags<Qt::PenCapStyle> *fl = static_cast<QFlags<Qt::PenCapStyle>*>( static_cast<void*>(&static_cast<QFlags<Qt::PenCapStyle>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::PenCapStyle>(lqtL_toenum(L, i, "Qt::PenCapStyle"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::PenCapStyle>*")) {
		lua_pushstring(L, "QFlags<Qt::PenCapStyle>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_PenStyle (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "NoPen");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "NoPen");
  lua_pushstring(L, "SolidLine");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "SolidLine");
  lua_pushstring(L, "DashLine");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "DashLine");
  lua_pushstring(L, "DotLine");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "DotLine");
  lua_pushstring(L, "DashDotLine");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "DashDotLine");
  lua_pushstring(L, "DashDotDotLine");
  lua_rawseti(L, enum_table, 5);
  lua_pushinteger(L, 5);
  lua_setfield(L, enum_table, "DashDotDotLine");
  lua_pushstring(L, "CustomDashLine");
  lua_rawseti(L, enum_table, 6);
  lua_pushinteger(L, 6);
  lua_setfield(L, enum_table, "CustomDashLine");
  lua_pushstring(L, "MPenStyle");
  lua_rawseti(L, enum_table, 15);
  lua_pushinteger(L, 15);
  lua_setfield(L, enum_table, "MPenStyle");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_PenStyle_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::PenStyle");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_PenStyle_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::PenStyle>*) + sizeof(QFlags<Qt::PenStyle>));
  QFlags<Qt::PenStyle> *fl = static_cast<QFlags<Qt::PenStyle>*>( static_cast<void*>(&static_cast<QFlags<Qt::PenStyle>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::PenStyle>(lqtL_toenum(L, i, "Qt::PenStyle"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::PenStyle>*")) {
		lua_pushstring(L, "QFlags<Qt::PenStyle>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_ArrowType (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "NoArrow");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "NoArrow");
  lua_pushstring(L, "UpArrow");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "UpArrow");
  lua_pushstring(L, "DownArrow");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "DownArrow");
  lua_pushstring(L, "LeftArrow");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "LeftArrow");
  lua_pushstring(L, "RightArrow");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "RightArrow");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_ArrowType_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::ArrowType");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_ArrowType_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::ArrowType>*) + sizeof(QFlags<Qt::ArrowType>));
  QFlags<Qt::ArrowType> *fl = static_cast<QFlags<Qt::ArrowType>*>( static_cast<void*>(&static_cast<QFlags<Qt::ArrowType>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::ArrowType>(lqtL_toenum(L, i, "Qt::ArrowType"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::ArrowType>*")) {
		lua_pushstring(L, "QFlags<Qt::ArrowType>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_Key (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "Key_Escape");
  lua_rawseti(L, enum_table, 16777216);
  lua_pushinteger(L, 16777216);
  lua_setfield(L, enum_table, "Key_Escape");
  lua_pushstring(L, "Key_Tab");
  lua_rawseti(L, enum_table, 16777217);
  lua_pushinteger(L, 16777217);
  lua_setfield(L, enum_table, "Key_Tab");
  lua_pushstring(L, "Key_Backtab");
  lua_rawseti(L, enum_table, 16777218);
  lua_pushinteger(L, 16777218);
  lua_setfield(L, enum_table, "Key_Backtab");
  lua_pushstring(L, "Key_Backspace");
  lua_rawseti(L, enum_table, 16777219);
  lua_pushinteger(L, 16777219);
  lua_setfield(L, enum_table, "Key_Backspace");
  lua_pushstring(L, "Key_Return");
  lua_rawseti(L, enum_table, 16777220);
  lua_pushinteger(L, 16777220);
  lua_setfield(L, enum_table, "Key_Return");
  lua_pushstring(L, "Key_Enter");
  lua_rawseti(L, enum_table, 16777221);
  lua_pushinteger(L, 16777221);
  lua_setfield(L, enum_table, "Key_Enter");
  lua_pushstring(L, "Key_Insert");
  lua_rawseti(L, enum_table, 16777222);
  lua_pushinteger(L, 16777222);
  lua_setfield(L, enum_table, "Key_Insert");
  lua_pushstring(L, "Key_Delete");
  lua_rawseti(L, enum_table, 16777223);
  lua_pushinteger(L, 16777223);
  lua_setfield(L, enum_table, "Key_Delete");
  lua_pushstring(L, "Key_Pause");
  lua_rawseti(L, enum_table, 16777224);
  lua_pushinteger(L, 16777224);
  lua_setfield(L, enum_table, "Key_Pause");
  lua_pushstring(L, "Key_Print");
  lua_rawseti(L, enum_table, 16777225);
  lua_pushinteger(L, 16777225);
  lua_setfield(L, enum_table, "Key_Print");
  lua_pushstring(L, "Key_SysReq");
  lua_rawseti(L, enum_table, 16777226);
  lua_pushinteger(L, 16777226);
  lua_setfield(L, enum_table, "Key_SysReq");
  lua_pushstring(L, "Key_Clear");
  lua_rawseti(L, enum_table, 16777227);
  lua_pushinteger(L, 16777227);
  lua_setfield(L, enum_table, "Key_Clear");
  lua_pushstring(L, "Key_Home");
  lua_rawseti(L, enum_table, 16777232);
  lua_pushinteger(L, 16777232);
  lua_setfield(L, enum_table, "Key_Home");
  lua_pushstring(L, "Key_End");
  lua_rawseti(L, enum_table, 16777233);
  lua_pushinteger(L, 16777233);
  lua_setfield(L, enum_table, "Key_End");
  lua_pushstring(L, "Key_Left");
  lua_rawseti(L, enum_table, 16777234);
  lua_pushinteger(L, 16777234);
  lua_setfield(L, enum_table, "Key_Left");
  lua_pushstring(L, "Key_Up");
  lua_rawseti(L, enum_table, 16777235);
  lua_pushinteger(L, 16777235);
  lua_setfield(L, enum_table, "Key_Up");
  lua_pushstring(L, "Key_Right");
  lua_rawseti(L, enum_table, 16777236);
  lua_pushinteger(L, 16777236);
  lua_setfield(L, enum_table, "Key_Right");
  lua_pushstring(L, "Key_Down");
  lua_rawseti(L, enum_table, 16777237);
  lua_pushinteger(L, 16777237);
  lua_setfield(L, enum_table, "Key_Down");
  lua_pushstring(L, "Key_PageUp");
  lua_rawseti(L, enum_table, 16777238);
  lua_pushinteger(L, 16777238);
  lua_setfield(L, enum_table, "Key_PageUp");
  lua_pushstring(L, "Key_PageDown");
  lua_rawseti(L, enum_table, 16777239);
  lua_pushinteger(L, 16777239);
  lua_setfield(L, enum_table, "Key_PageDown");
  lua_pushstring(L, "Key_Shift");
  lua_rawseti(L, enum_table, 16777248);
  lua_pushinteger(L, 16777248);
  lua_setfield(L, enum_table, "Key_Shift");
  lua_pushstring(L, "Key_Control");
  lua_rawseti(L, enum_table, 16777249);
  lua_pushinteger(L, 16777249);
  lua_setfield(L, enum_table, "Key_Control");
  lua_pushstring(L, "Key_Meta");
  lua_rawseti(L, enum_table, 16777250);
  lua_pushinteger(L, 16777250);
  lua_setfield(L, enum_table, "Key_Meta");
  lua_pushstring(L, "Key_Alt");
  lua_rawseti(L, enum_table, 16777251);
  lua_pushinteger(L, 16777251);
  lua_setfield(L, enum_table, "Key_Alt");
  lua_pushstring(L, "Key_CapsLock");
  lua_rawseti(L, enum_table, 16777252);
  lua_pushinteger(L, 16777252);
  lua_setfield(L, enum_table, "Key_CapsLock");
  lua_pushstring(L, "Key_NumLock");
  lua_rawseti(L, enum_table, 16777253);
  lua_pushinteger(L, 16777253);
  lua_setfield(L, enum_table, "Key_NumLock");
  lua_pushstring(L, "Key_ScrollLock");
  lua_rawseti(L, enum_table, 16777254);
  lua_pushinteger(L, 16777254);
  lua_setfield(L, enum_table, "Key_ScrollLock");
  lua_pushstring(L, "Key_F1");
  lua_rawseti(L, enum_table, 16777264);
  lua_pushinteger(L, 16777264);
  lua_setfield(L, enum_table, "Key_F1");
  lua_pushstring(L, "Key_F2");
  lua_rawseti(L, enum_table, 16777265);
  lua_pushinteger(L, 16777265);
  lua_setfield(L, enum_table, "Key_F2");
  lua_pushstring(L, "Key_F3");
  lua_rawseti(L, enum_table, 16777266);
  lua_pushinteger(L, 16777266);
  lua_setfield(L, enum_table, "Key_F3");
  lua_pushstring(L, "Key_F4");
  lua_rawseti(L, enum_table, 16777267);
  lua_pushinteger(L, 16777267);
  lua_setfield(L, enum_table, "Key_F4");
  lua_pushstring(L, "Key_F5");
  lua_rawseti(L, enum_table, 16777268);
  lua_pushinteger(L, 16777268);
  lua_setfield(L, enum_table, "Key_F5");
  lua_pushstring(L, "Key_F6");
  lua_rawseti(L, enum_table, 16777269);
  lua_pushinteger(L, 16777269);
  lua_setfield(L, enum_table, "Key_F6");
  lua_pushstring(L, "Key_F7");
  lua_rawseti(L, enum_table, 16777270);
  lua_pushinteger(L, 16777270);
  lua_setfield(L, enum_table, "Key_F7");
  lua_pushstring(L, "Key_F8");
  lua_rawseti(L, enum_table, 16777271);
  lua_pushinteger(L, 16777271);
  lua_setfield(L, enum_table, "Key_F8");
  lua_pushstring(L, "Key_F9");
  lua_rawseti(L, enum_table, 16777272);
  lua_pushinteger(L, 16777272);
  lua_setfield(L, enum_table, "Key_F9");
  lua_pushstring(L, "Key_F10");
  lua_rawseti(L, enum_table, 16777273);
  lua_pushinteger(L, 16777273);
  lua_setfield(L, enum_table, "Key_F10");
  lua_pushstring(L, "Key_F11");
  lua_rawseti(L, enum_table, 16777274);
  lua_pushinteger(L, 16777274);
  lua_setfield(L, enum_table, "Key_F11");
  lua_pushstring(L, "Key_F12");
  lua_rawseti(L, enum_table, 16777275);
  lua_pushinteger(L, 16777275);
  lua_setfield(L, enum_table, "Key_F12");
  lua_pushstring(L, "Key_F13");
  lua_rawseti(L, enum_table, 16777276);
  lua_pushinteger(L, 16777276);
  lua_setfield(L, enum_table, "Key_F13");
  lua_pushstring(L, "Key_F14");
  lua_rawseti(L, enum_table, 16777277);
  lua_pushinteger(L, 16777277);
  lua_setfield(L, enum_table, "Key_F14");
  lua_pushstring(L, "Key_F15");
  lua_rawseti(L, enum_table, 16777278);
  lua_pushinteger(L, 16777278);
  lua_setfield(L, enum_table, "Key_F15");
  lua_pushstring(L, "Key_F16");
  lua_rawseti(L, enum_table, 16777279);
  lua_pushinteger(L, 16777279);
  lua_setfield(L, enum_table, "Key_F16");
  lua_pushstring(L, "Key_F17");
  lua_rawseti(L, enum_table, 16777280);
  lua_pushinteger(L, 16777280);
  lua_setfield(L, enum_table, "Key_F17");
  lua_pushstring(L, "Key_F18");
  lua_rawseti(L, enum_table, 16777281);
  lua_pushinteger(L, 16777281);
  lua_setfield(L, enum_table, "Key_F18");
  lua_pushstring(L, "Key_F19");
  lua_rawseti(L, enum_table, 16777282);
  lua_pushinteger(L, 16777282);
  lua_setfield(L, enum_table, "Key_F19");
  lua_pushstring(L, "Key_F20");
  lua_rawseti(L, enum_table, 16777283);
  lua_pushinteger(L, 16777283);
  lua_setfield(L, enum_table, "Key_F20");
  lua_pushstring(L, "Key_F21");
  lua_rawseti(L, enum_table, 16777284);
  lua_pushinteger(L, 16777284);
  lua_setfield(L, enum_table, "Key_F21");
  lua_pushstring(L, "Key_F22");
  lua_rawseti(L, enum_table, 16777285);
  lua_pushinteger(L, 16777285);
  lua_setfield(L, enum_table, "Key_F22");
  lua_pushstring(L, "Key_F23");
  lua_rawseti(L, enum_table, 16777286);
  lua_pushinteger(L, 16777286);
  lua_setfield(L, enum_table, "Key_F23");
  lua_pushstring(L, "Key_F24");
  lua_rawseti(L, enum_table, 16777287);
  lua_pushinteger(L, 16777287);
  lua_setfield(L, enum_table, "Key_F24");
  lua_pushstring(L, "Key_F25");
  lua_rawseti(L, enum_table, 16777288);
  lua_pushinteger(L, 16777288);
  lua_setfield(L, enum_table, "Key_F25");
  lua_pushstring(L, "Key_F26");
  lua_rawseti(L, enum_table, 16777289);
  lua_pushinteger(L, 16777289);
  lua_setfield(L, enum_table, "Key_F26");
  lua_pushstring(L, "Key_F27");
  lua_rawseti(L, enum_table, 16777290);
  lua_pushinteger(L, 16777290);
  lua_setfield(L, enum_table, "Key_F27");
  lua_pushstring(L, "Key_F28");
  lua_rawseti(L, enum_table, 16777291);
  lua_pushinteger(L, 16777291);
  lua_setfield(L, enum_table, "Key_F28");
  lua_pushstring(L, "Key_F29");
  lua_rawseti(L, enum_table, 16777292);
  lua_pushinteger(L, 16777292);
  lua_setfield(L, enum_table, "Key_F29");
  lua_pushstring(L, "Key_F30");
  lua_rawseti(L, enum_table, 16777293);
  lua_pushinteger(L, 16777293);
  lua_setfield(L, enum_table, "Key_F30");
  lua_pushstring(L, "Key_F31");
  lua_rawseti(L, enum_table, 16777294);
  lua_pushinteger(L, 16777294);
  lua_setfield(L, enum_table, "Key_F31");
  lua_pushstring(L, "Key_F32");
  lua_rawseti(L, enum_table, 16777295);
  lua_pushinteger(L, 16777295);
  lua_setfield(L, enum_table, "Key_F32");
  lua_pushstring(L, "Key_F33");
  lua_rawseti(L, enum_table, 16777296);
  lua_pushinteger(L, 16777296);
  lua_setfield(L, enum_table, "Key_F33");
  lua_pushstring(L, "Key_F34");
  lua_rawseti(L, enum_table, 16777297);
  lua_pushinteger(L, 16777297);
  lua_setfield(L, enum_table, "Key_F34");
  lua_pushstring(L, "Key_F35");
  lua_rawseti(L, enum_table, 16777298);
  lua_pushinteger(L, 16777298);
  lua_setfield(L, enum_table, "Key_F35");
  lua_pushstring(L, "Key_Super_L");
  lua_rawseti(L, enum_table, 16777299);
  lua_pushinteger(L, 16777299);
  lua_setfield(L, enum_table, "Key_Super_L");
  lua_pushstring(L, "Key_Super_R");
  lua_rawseti(L, enum_table, 16777300);
  lua_pushinteger(L, 16777300);
  lua_setfield(L, enum_table, "Key_Super_R");
  lua_pushstring(L, "Key_Menu");
  lua_rawseti(L, enum_table, 16777301);
  lua_pushinteger(L, 16777301);
  lua_setfield(L, enum_table, "Key_Menu");
  lua_pushstring(L, "Key_Hyper_L");
  lua_rawseti(L, enum_table, 16777302);
  lua_pushinteger(L, 16777302);
  lua_setfield(L, enum_table, "Key_Hyper_L");
  lua_pushstring(L, "Key_Hyper_R");
  lua_rawseti(L, enum_table, 16777303);
  lua_pushinteger(L, 16777303);
  lua_setfield(L, enum_table, "Key_Hyper_R");
  lua_pushstring(L, "Key_Help");
  lua_rawseti(L, enum_table, 16777304);
  lua_pushinteger(L, 16777304);
  lua_setfield(L, enum_table, "Key_Help");
  lua_pushstring(L, "Key_Direction_L");
  lua_rawseti(L, enum_table, 16777305);
  lua_pushinteger(L, 16777305);
  lua_setfield(L, enum_table, "Key_Direction_L");
  lua_pushstring(L, "Key_Direction_R");
  lua_rawseti(L, enum_table, 16777312);
  lua_pushinteger(L, 16777312);
  lua_setfield(L, enum_table, "Key_Direction_R");
  lua_pushstring(L, "Key_Space");
  lua_rawseti(L, enum_table, 32);
  lua_pushinteger(L, 32);
  lua_setfield(L, enum_table, "Key_Space");
  lua_pushstring(L, "Key_Any");
  lua_rawseti(L, enum_table, 32);
  lua_pushinteger(L, 32);
  lua_setfield(L, enum_table, "Key_Any");
  lua_pushstring(L, "Key_Exclam");
  lua_rawseti(L, enum_table, 33);
  lua_pushinteger(L, 33);
  lua_setfield(L, enum_table, "Key_Exclam");
  lua_pushstring(L, "Key_QuoteDbl");
  lua_rawseti(L, enum_table, 34);
  lua_pushinteger(L, 34);
  lua_setfield(L, enum_table, "Key_QuoteDbl");
  lua_pushstring(L, "Key_NumberSign");
  lua_rawseti(L, enum_table, 35);
  lua_pushinteger(L, 35);
  lua_setfield(L, enum_table, "Key_NumberSign");
  lua_pushstring(L, "Key_Dollar");
  lua_rawseti(L, enum_table, 36);
  lua_pushinteger(L, 36);
  lua_setfield(L, enum_table, "Key_Dollar");
  lua_pushstring(L, "Key_Percent");
  lua_rawseti(L, enum_table, 37);
  lua_pushinteger(L, 37);
  lua_setfield(L, enum_table, "Key_Percent");
  lua_pushstring(L, "Key_Ampersand");
  lua_rawseti(L, enum_table, 38);
  lua_pushinteger(L, 38);
  lua_setfield(L, enum_table, "Key_Ampersand");
  lua_pushstring(L, "Key_Apostrophe");
  lua_rawseti(L, enum_table, 39);
  lua_pushinteger(L, 39);
  lua_setfield(L, enum_table, "Key_Apostrophe");
  lua_pushstring(L, "Key_ParenLeft");
  lua_rawseti(L, enum_table, 40);
  lua_pushinteger(L, 40);
  lua_setfield(L, enum_table, "Key_ParenLeft");
  lua_pushstring(L, "Key_ParenRight");
  lua_rawseti(L, enum_table, 41);
  lua_pushinteger(L, 41);
  lua_setfield(L, enum_table, "Key_ParenRight");
  lua_pushstring(L, "Key_Asterisk");
  lua_rawseti(L, enum_table, 42);
  lua_pushinteger(L, 42);
  lua_setfield(L, enum_table, "Key_Asterisk");
  lua_pushstring(L, "Key_Plus");
  lua_rawseti(L, enum_table, 43);
  lua_pushinteger(L, 43);
  lua_setfield(L, enum_table, "Key_Plus");
  lua_pushstring(L, "Key_Comma");
  lua_rawseti(L, enum_table, 44);
  lua_pushinteger(L, 44);
  lua_setfield(L, enum_table, "Key_Comma");
  lua_pushstring(L, "Key_Minus");
  lua_rawseti(L, enum_table, 45);
  lua_pushinteger(L, 45);
  lua_setfield(L, enum_table, "Key_Minus");
  lua_pushstring(L, "Key_Period");
  lua_rawseti(L, enum_table, 46);
  lua_pushinteger(L, 46);
  lua_setfield(L, enum_table, "Key_Period");
  lua_pushstring(L, "Key_Slash");
  lua_rawseti(L, enum_table, 47);
  lua_pushinteger(L, 47);
  lua_setfield(L, enum_table, "Key_Slash");
  lua_pushstring(L, "Key_0");
  lua_rawseti(L, enum_table, 48);
  lua_pushinteger(L, 48);
  lua_setfield(L, enum_table, "Key_0");
  lua_pushstring(L, "Key_1");
  lua_rawseti(L, enum_table, 49);
  lua_pushinteger(L, 49);
  lua_setfield(L, enum_table, "Key_1");
  lua_pushstring(L, "Key_2");
  lua_rawseti(L, enum_table, 50);
  lua_pushinteger(L, 50);
  lua_setfield(L, enum_table, "Key_2");
  lua_pushstring(L, "Key_3");
  lua_rawseti(L, enum_table, 51);
  lua_pushinteger(L, 51);
  lua_setfield(L, enum_table, "Key_3");
  lua_pushstring(L, "Key_4");
  lua_rawseti(L, enum_table, 52);
  lua_pushinteger(L, 52);
  lua_setfield(L, enum_table, "Key_4");
  lua_pushstring(L, "Key_5");
  lua_rawseti(L, enum_table, 53);
  lua_pushinteger(L, 53);
  lua_setfield(L, enum_table, "Key_5");
  lua_pushstring(L, "Key_6");
  lua_rawseti(L, enum_table, 54);
  lua_pushinteger(L, 54);
  lua_setfield(L, enum_table, "Key_6");
  lua_pushstring(L, "Key_7");
  lua_rawseti(L, enum_table, 55);
  lua_pushinteger(L, 55);
  lua_setfield(L, enum_table, "Key_7");
  lua_pushstring(L, "Key_8");
  lua_rawseti(L, enum_table, 56);
  lua_pushinteger(L, 56);
  lua_setfield(L, enum_table, "Key_8");
  lua_pushstring(L, "Key_9");
  lua_rawseti(L, enum_table, 57);
  lua_pushinteger(L, 57);
  lua_setfield(L, enum_table, "Key_9");
  lua_pushstring(L, "Key_Colon");
  lua_rawseti(L, enum_table, 58);
  lua_pushinteger(L, 58);
  lua_setfield(L, enum_table, "Key_Colon");
  lua_pushstring(L, "Key_Semicolon");
  lua_rawseti(L, enum_table, 59);
  lua_pushinteger(L, 59);
  lua_setfield(L, enum_table, "Key_Semicolon");
  lua_pushstring(L, "Key_Less");
  lua_rawseti(L, enum_table, 60);
  lua_pushinteger(L, 60);
  lua_setfield(L, enum_table, "Key_Less");
  lua_pushstring(L, "Key_Equal");
  lua_rawseti(L, enum_table, 61);
  lua_pushinteger(L, 61);
  lua_setfield(L, enum_table, "Key_Equal");
  lua_pushstring(L, "Key_Greater");
  lua_rawseti(L, enum_table, 62);
  lua_pushinteger(L, 62);
  lua_setfield(L, enum_table, "Key_Greater");
  lua_pushstring(L, "Key_Question");
  lua_rawseti(L, enum_table, 63);
  lua_pushinteger(L, 63);
  lua_setfield(L, enum_table, "Key_Question");
  lua_pushstring(L, "Key_At");
  lua_rawseti(L, enum_table, 64);
  lua_pushinteger(L, 64);
  lua_setfield(L, enum_table, "Key_At");
  lua_pushstring(L, "Key_A");
  lua_rawseti(L, enum_table, 65);
  lua_pushinteger(L, 65);
  lua_setfield(L, enum_table, "Key_A");
  lua_pushstring(L, "Key_B");
  lua_rawseti(L, enum_table, 66);
  lua_pushinteger(L, 66);
  lua_setfield(L, enum_table, "Key_B");
  lua_pushstring(L, "Key_C");
  lua_rawseti(L, enum_table, 67);
  lua_pushinteger(L, 67);
  lua_setfield(L, enum_table, "Key_C");
  lua_pushstring(L, "Key_D");
  lua_rawseti(L, enum_table, 68);
  lua_pushinteger(L, 68);
  lua_setfield(L, enum_table, "Key_D");
  lua_pushstring(L, "Key_E");
  lua_rawseti(L, enum_table, 69);
  lua_pushinteger(L, 69);
  lua_setfield(L, enum_table, "Key_E");
  lua_pushstring(L, "Key_F");
  lua_rawseti(L, enum_table, 70);
  lua_pushinteger(L, 70);
  lua_setfield(L, enum_table, "Key_F");
  lua_pushstring(L, "Key_G");
  lua_rawseti(L, enum_table, 71);
  lua_pushinteger(L, 71);
  lua_setfield(L, enum_table, "Key_G");
  lua_pushstring(L, "Key_H");
  lua_rawseti(L, enum_table, 72);
  lua_pushinteger(L, 72);
  lua_setfield(L, enum_table, "Key_H");
  lua_pushstring(L, "Key_I");
  lua_rawseti(L, enum_table, 73);
  lua_pushinteger(L, 73);
  lua_setfield(L, enum_table, "Key_I");
  lua_pushstring(L, "Key_J");
  lua_rawseti(L, enum_table, 74);
  lua_pushinteger(L, 74);
  lua_setfield(L, enum_table, "Key_J");
  lua_pushstring(L, "Key_K");
  lua_rawseti(L, enum_table, 75);
  lua_pushinteger(L, 75);
  lua_setfield(L, enum_table, "Key_K");
  lua_pushstring(L, "Key_L");
  lua_rawseti(L, enum_table, 76);
  lua_pushinteger(L, 76);
  lua_setfield(L, enum_table, "Key_L");
  lua_pushstring(L, "Key_M");
  lua_rawseti(L, enum_table, 77);
  lua_pushinteger(L, 77);
  lua_setfield(L, enum_table, "Key_M");
  lua_pushstring(L, "Key_N");
  lua_rawseti(L, enum_table, 78);
  lua_pushinteger(L, 78);
  lua_setfield(L, enum_table, "Key_N");
  lua_pushstring(L, "Key_O");
  lua_rawseti(L, enum_table, 79);
  lua_pushinteger(L, 79);
  lua_setfield(L, enum_table, "Key_O");
  lua_pushstring(L, "Key_P");
  lua_rawseti(L, enum_table, 80);
  lua_pushinteger(L, 80);
  lua_setfield(L, enum_table, "Key_P");
  lua_pushstring(L, "Key_Q");
  lua_rawseti(L, enum_table, 81);
  lua_pushinteger(L, 81);
  lua_setfield(L, enum_table, "Key_Q");
  lua_pushstring(L, "Key_R");
  lua_rawseti(L, enum_table, 82);
  lua_pushinteger(L, 82);
  lua_setfield(L, enum_table, "Key_R");
  lua_pushstring(L, "Key_S");
  lua_rawseti(L, enum_table, 83);
  lua_pushinteger(L, 83);
  lua_setfield(L, enum_table, "Key_S");
  lua_pushstring(L, "Key_T");
  lua_rawseti(L, enum_table, 84);
  lua_pushinteger(L, 84);
  lua_setfield(L, enum_table, "Key_T");
  lua_pushstring(L, "Key_U");
  lua_rawseti(L, enum_table, 85);
  lua_pushinteger(L, 85);
  lua_setfield(L, enum_table, "Key_U");
  lua_pushstring(L, "Key_V");
  lua_rawseti(L, enum_table, 86);
  lua_pushinteger(L, 86);
  lua_setfield(L, enum_table, "Key_V");
  lua_pushstring(L, "Key_W");
  lua_rawseti(L, enum_table, 87);
  lua_pushinteger(L, 87);
  lua_setfield(L, enum_table, "Key_W");
  lua_pushstring(L, "Key_X");
  lua_rawseti(L, enum_table, 88);
  lua_pushinteger(L, 88);
  lua_setfield(L, enum_table, "Key_X");
  lua_pushstring(L, "Key_Y");
  lua_rawseti(L, enum_table, 89);
  lua_pushinteger(L, 89);
  lua_setfield(L, enum_table, "Key_Y");
  lua_pushstring(L, "Key_Z");
  lua_rawseti(L, enum_table, 90);
  lua_pushinteger(L, 90);
  lua_setfield(L, enum_table, "Key_Z");
  lua_pushstring(L, "Key_BracketLeft");
  lua_rawseti(L, enum_table, 91);
  lua_pushinteger(L, 91);
  lua_setfield(L, enum_table, "Key_BracketLeft");
  lua_pushstring(L, "Key_Backslash");
  lua_rawseti(L, enum_table, 92);
  lua_pushinteger(L, 92);
  lua_setfield(L, enum_table, "Key_Backslash");
  lua_pushstring(L, "Key_BracketRight");
  lua_rawseti(L, enum_table, 93);
  lua_pushinteger(L, 93);
  lua_setfield(L, enum_table, "Key_BracketRight");
  lua_pushstring(L, "Key_AsciiCircum");
  lua_rawseti(L, enum_table, 94);
  lua_pushinteger(L, 94);
  lua_setfield(L, enum_table, "Key_AsciiCircum");
  lua_pushstring(L, "Key_Underscore");
  lua_rawseti(L, enum_table, 95);
  lua_pushinteger(L, 95);
  lua_setfield(L, enum_table, "Key_Underscore");
  lua_pushstring(L, "Key_QuoteLeft");
  lua_rawseti(L, enum_table, 96);
  lua_pushinteger(L, 96);
  lua_setfield(L, enum_table, "Key_QuoteLeft");
  lua_pushstring(L, "Key_BraceLeft");
  lua_rawseti(L, enum_table, 123);
  lua_pushinteger(L, 123);
  lua_setfield(L, enum_table, "Key_BraceLeft");
  lua_pushstring(L, "Key_Bar");
  lua_rawseti(L, enum_table, 124);
  lua_pushinteger(L, 124);
  lua_setfield(L, enum_table, "Key_Bar");
  lua_pushstring(L, "Key_BraceRight");
  lua_rawseti(L, enum_table, 125);
  lua_pushinteger(L, 125);
  lua_setfield(L, enum_table, "Key_BraceRight");
  lua_pushstring(L, "Key_AsciiTilde");
  lua_rawseti(L, enum_table, 126);
  lua_pushinteger(L, 126);
  lua_setfield(L, enum_table, "Key_AsciiTilde");
  lua_pushstring(L, "Key_nobreakspace");
  lua_rawseti(L, enum_table, 160);
  lua_pushinteger(L, 160);
  lua_setfield(L, enum_table, "Key_nobreakspace");
  lua_pushstring(L, "Key_exclamdown");
  lua_rawseti(L, enum_table, 161);
  lua_pushinteger(L, 161);
  lua_setfield(L, enum_table, "Key_exclamdown");
  lua_pushstring(L, "Key_cent");
  lua_rawseti(L, enum_table, 162);
  lua_pushinteger(L, 162);
  lua_setfield(L, enum_table, "Key_cent");
  lua_pushstring(L, "Key_sterling");
  lua_rawseti(L, enum_table, 163);
  lua_pushinteger(L, 163);
  lua_setfield(L, enum_table, "Key_sterling");
  lua_pushstring(L, "Key_currency");
  lua_rawseti(L, enum_table, 164);
  lua_pushinteger(L, 164);
  lua_setfield(L, enum_table, "Key_currency");
  lua_pushstring(L, "Key_yen");
  lua_rawseti(L, enum_table, 165);
  lua_pushinteger(L, 165);
  lua_setfield(L, enum_table, "Key_yen");
  lua_pushstring(L, "Key_brokenbar");
  lua_rawseti(L, enum_table, 166);
  lua_pushinteger(L, 166);
  lua_setfield(L, enum_table, "Key_brokenbar");
  lua_pushstring(L, "Key_section");
  lua_rawseti(L, enum_table, 167);
  lua_pushinteger(L, 167);
  lua_setfield(L, enum_table, "Key_section");
  lua_pushstring(L, "Key_diaeresis");
  lua_rawseti(L, enum_table, 168);
  lua_pushinteger(L, 168);
  lua_setfield(L, enum_table, "Key_diaeresis");
  lua_pushstring(L, "Key_copyright");
  lua_rawseti(L, enum_table, 169);
  lua_pushinteger(L, 169);
  lua_setfield(L, enum_table, "Key_copyright");
  lua_pushstring(L, "Key_ordfeminine");
  lua_rawseti(L, enum_table, 170);
  lua_pushinteger(L, 170);
  lua_setfield(L, enum_table, "Key_ordfeminine");
  lua_pushstring(L, "Key_guillemotleft");
  lua_rawseti(L, enum_table, 171);
  lua_pushinteger(L, 171);
  lua_setfield(L, enum_table, "Key_guillemotleft");
  lua_pushstring(L, "Key_notsign");
  lua_rawseti(L, enum_table, 172);
  lua_pushinteger(L, 172);
  lua_setfield(L, enum_table, "Key_notsign");
  lua_pushstring(L, "Key_hyphen");
  lua_rawseti(L, enum_table, 173);
  lua_pushinteger(L, 173);
  lua_setfield(L, enum_table, "Key_hyphen");
  lua_pushstring(L, "Key_registered");
  lua_rawseti(L, enum_table, 174);
  lua_pushinteger(L, 174);
  lua_setfield(L, enum_table, "Key_registered");
  lua_pushstring(L, "Key_macron");
  lua_rawseti(L, enum_table, 175);
  lua_pushinteger(L, 175);
  lua_setfield(L, enum_table, "Key_macron");
  lua_pushstring(L, "Key_degree");
  lua_rawseti(L, enum_table, 176);
  lua_pushinteger(L, 176);
  lua_setfield(L, enum_table, "Key_degree");
  lua_pushstring(L, "Key_plusminus");
  lua_rawseti(L, enum_table, 177);
  lua_pushinteger(L, 177);
  lua_setfield(L, enum_table, "Key_plusminus");
  lua_pushstring(L, "Key_twosuperior");
  lua_rawseti(L, enum_table, 178);
  lua_pushinteger(L, 178);
  lua_setfield(L, enum_table, "Key_twosuperior");
  lua_pushstring(L, "Key_threesuperior");
  lua_rawseti(L, enum_table, 179);
  lua_pushinteger(L, 179);
  lua_setfield(L, enum_table, "Key_threesuperior");
  lua_pushstring(L, "Key_acute");
  lua_rawseti(L, enum_table, 180);
  lua_pushinteger(L, 180);
  lua_setfield(L, enum_table, "Key_acute");
  lua_pushstring(L, "Key_mu");
  lua_rawseti(L, enum_table, 181);
  lua_pushinteger(L, 181);
  lua_setfield(L, enum_table, "Key_mu");
  lua_pushstring(L, "Key_paragraph");
  lua_rawseti(L, enum_table, 182);
  lua_pushinteger(L, 182);
  lua_setfield(L, enum_table, "Key_paragraph");
  lua_pushstring(L, "Key_periodcentered");
  lua_rawseti(L, enum_table, 183);
  lua_pushinteger(L, 183);
  lua_setfield(L, enum_table, "Key_periodcentered");
  lua_pushstring(L, "Key_cedilla");
  lua_rawseti(L, enum_table, 184);
  lua_pushinteger(L, 184);
  lua_setfield(L, enum_table, "Key_cedilla");
  lua_pushstring(L, "Key_onesuperior");
  lua_rawseti(L, enum_table, 185);
  lua_pushinteger(L, 185);
  lua_setfield(L, enum_table, "Key_onesuperior");
  lua_pushstring(L, "Key_masculine");
  lua_rawseti(L, enum_table, 186);
  lua_pushinteger(L, 186);
  lua_setfield(L, enum_table, "Key_masculine");
  lua_pushstring(L, "Key_guillemotright");
  lua_rawseti(L, enum_table, 187);
  lua_pushinteger(L, 187);
  lua_setfield(L, enum_table, "Key_guillemotright");
  lua_pushstring(L, "Key_onequarter");
  lua_rawseti(L, enum_table, 188);
  lua_pushinteger(L, 188);
  lua_setfield(L, enum_table, "Key_onequarter");
  lua_pushstring(L, "Key_onehalf");
  lua_rawseti(L, enum_table, 189);
  lua_pushinteger(L, 189);
  lua_setfield(L, enum_table, "Key_onehalf");
  lua_pushstring(L, "Key_threequarters");
  lua_rawseti(L, enum_table, 190);
  lua_pushinteger(L, 190);
  lua_setfield(L, enum_table, "Key_threequarters");
  lua_pushstring(L, "Key_questiondown");
  lua_rawseti(L, enum_table, 191);
  lua_pushinteger(L, 191);
  lua_setfield(L, enum_table, "Key_questiondown");
  lua_pushstring(L, "Key_Agrave");
  lua_rawseti(L, enum_table, 192);
  lua_pushinteger(L, 192);
  lua_setfield(L, enum_table, "Key_Agrave");
  lua_pushstring(L, "Key_Aacute");
  lua_rawseti(L, enum_table, 193);
  lua_pushinteger(L, 193);
  lua_setfield(L, enum_table, "Key_Aacute");
  lua_pushstring(L, "Key_Acircumflex");
  lua_rawseti(L, enum_table, 194);
  lua_pushinteger(L, 194);
  lua_setfield(L, enum_table, "Key_Acircumflex");
  lua_pushstring(L, "Key_Atilde");
  lua_rawseti(L, enum_table, 195);
  lua_pushinteger(L, 195);
  lua_setfield(L, enum_table, "Key_Atilde");
  lua_pushstring(L, "Key_Adiaeresis");
  lua_rawseti(L, enum_table, 196);
  lua_pushinteger(L, 196);
  lua_setfield(L, enum_table, "Key_Adiaeresis");
  lua_pushstring(L, "Key_Aring");
  lua_rawseti(L, enum_table, 197);
  lua_pushinteger(L, 197);
  lua_setfield(L, enum_table, "Key_Aring");
  lua_pushstring(L, "Key_AE");
  lua_rawseti(L, enum_table, 198);
  lua_pushinteger(L, 198);
  lua_setfield(L, enum_table, "Key_AE");
  lua_pushstring(L, "Key_Ccedilla");
  lua_rawseti(L, enum_table, 199);
  lua_pushinteger(L, 199);
  lua_setfield(L, enum_table, "Key_Ccedilla");
  lua_pushstring(L, "Key_Egrave");
  lua_rawseti(L, enum_table, 200);
  lua_pushinteger(L, 200);
  lua_setfield(L, enum_table, "Key_Egrave");
  lua_pushstring(L, "Key_Eacute");
  lua_rawseti(L, enum_table, 201);
  lua_pushinteger(L, 201);
  lua_setfield(L, enum_table, "Key_Eacute");
  lua_pushstring(L, "Key_Ecircumflex");
  lua_rawseti(L, enum_table, 202);
  lua_pushinteger(L, 202);
  lua_setfield(L, enum_table, "Key_Ecircumflex");
  lua_pushstring(L, "Key_Ediaeresis");
  lua_rawseti(L, enum_table, 203);
  lua_pushinteger(L, 203);
  lua_setfield(L, enum_table, "Key_Ediaeresis");
  lua_pushstring(L, "Key_Igrave");
  lua_rawseti(L, enum_table, 204);
  lua_pushinteger(L, 204);
  lua_setfield(L, enum_table, "Key_Igrave");
  lua_pushstring(L, "Key_Iacute");
  lua_rawseti(L, enum_table, 205);
  lua_pushinteger(L, 205);
  lua_setfield(L, enum_table, "Key_Iacute");
  lua_pushstring(L, "Key_Icircumflex");
  lua_rawseti(L, enum_table, 206);
  lua_pushinteger(L, 206);
  lua_setfield(L, enum_table, "Key_Icircumflex");
  lua_pushstring(L, "Key_Idiaeresis");
  lua_rawseti(L, enum_table, 207);
  lua_pushinteger(L, 207);
  lua_setfield(L, enum_table, "Key_Idiaeresis");
  lua_pushstring(L, "Key_ETH");
  lua_rawseti(L, enum_table, 208);
  lua_pushinteger(L, 208);
  lua_setfield(L, enum_table, "Key_ETH");
  lua_pushstring(L, "Key_Ntilde");
  lua_rawseti(L, enum_table, 209);
  lua_pushinteger(L, 209);
  lua_setfield(L, enum_table, "Key_Ntilde");
  lua_pushstring(L, "Key_Ograve");
  lua_rawseti(L, enum_table, 210);
  lua_pushinteger(L, 210);
  lua_setfield(L, enum_table, "Key_Ograve");
  lua_pushstring(L, "Key_Oacute");
  lua_rawseti(L, enum_table, 211);
  lua_pushinteger(L, 211);
  lua_setfield(L, enum_table, "Key_Oacute");
  lua_pushstring(L, "Key_Ocircumflex");
  lua_rawseti(L, enum_table, 212);
  lua_pushinteger(L, 212);
  lua_setfield(L, enum_table, "Key_Ocircumflex");
  lua_pushstring(L, "Key_Otilde");
  lua_rawseti(L, enum_table, 213);
  lua_pushinteger(L, 213);
  lua_setfield(L, enum_table, "Key_Otilde");
  lua_pushstring(L, "Key_Odiaeresis");
  lua_rawseti(L, enum_table, 214);
  lua_pushinteger(L, 214);
  lua_setfield(L, enum_table, "Key_Odiaeresis");
  lua_pushstring(L, "Key_multiply");
  lua_rawseti(L, enum_table, 215);
  lua_pushinteger(L, 215);
  lua_setfield(L, enum_table, "Key_multiply");
  lua_pushstring(L, "Key_Ooblique");
  lua_rawseti(L, enum_table, 216);
  lua_pushinteger(L, 216);
  lua_setfield(L, enum_table, "Key_Ooblique");
  lua_pushstring(L, "Key_Ugrave");
  lua_rawseti(L, enum_table, 217);
  lua_pushinteger(L, 217);
  lua_setfield(L, enum_table, "Key_Ugrave");
  lua_pushstring(L, "Key_Uacute");
  lua_rawseti(L, enum_table, 218);
  lua_pushinteger(L, 218);
  lua_setfield(L, enum_table, "Key_Uacute");
  lua_pushstring(L, "Key_Ucircumflex");
  lua_rawseti(L, enum_table, 219);
  lua_pushinteger(L, 219);
  lua_setfield(L, enum_table, "Key_Ucircumflex");
  lua_pushstring(L, "Key_Udiaeresis");
  lua_rawseti(L, enum_table, 220);
  lua_pushinteger(L, 220);
  lua_setfield(L, enum_table, "Key_Udiaeresis");
  lua_pushstring(L, "Key_Yacute");
  lua_rawseti(L, enum_table, 221);
  lua_pushinteger(L, 221);
  lua_setfield(L, enum_table, "Key_Yacute");
  lua_pushstring(L, "Key_THORN");
  lua_rawseti(L, enum_table, 222);
  lua_pushinteger(L, 222);
  lua_setfield(L, enum_table, "Key_THORN");
  lua_pushstring(L, "Key_ssharp");
  lua_rawseti(L, enum_table, 223);
  lua_pushinteger(L, 223);
  lua_setfield(L, enum_table, "Key_ssharp");
  lua_pushstring(L, "Key_division");
  lua_rawseti(L, enum_table, 247);
  lua_pushinteger(L, 247);
  lua_setfield(L, enum_table, "Key_division");
  lua_pushstring(L, "Key_ydiaeresis");
  lua_rawseti(L, enum_table, 255);
  lua_pushinteger(L, 255);
  lua_setfield(L, enum_table, "Key_ydiaeresis");
  lua_pushstring(L, "Key_AltGr");
  lua_rawseti(L, enum_table, 16781571);
  lua_pushinteger(L, 16781571);
  lua_setfield(L, enum_table, "Key_AltGr");
  lua_pushstring(L, "Key_Multi_key");
  lua_rawseti(L, enum_table, 16781600);
  lua_pushinteger(L, 16781600);
  lua_setfield(L, enum_table, "Key_Multi_key");
  lua_pushstring(L, "Key_Codeinput");
  lua_rawseti(L, enum_table, 16781623);
  lua_pushinteger(L, 16781623);
  lua_setfield(L, enum_table, "Key_Codeinput");
  lua_pushstring(L, "Key_SingleCandidate");
  lua_rawseti(L, enum_table, 16781628);
  lua_pushinteger(L, 16781628);
  lua_setfield(L, enum_table, "Key_SingleCandidate");
  lua_pushstring(L, "Key_MultipleCandidate");
  lua_rawseti(L, enum_table, 16781629);
  lua_pushinteger(L, 16781629);
  lua_setfield(L, enum_table, "Key_MultipleCandidate");
  lua_pushstring(L, "Key_PreviousCandidate");
  lua_rawseti(L, enum_table, 16781630);
  lua_pushinteger(L, 16781630);
  lua_setfield(L, enum_table, "Key_PreviousCandidate");
  lua_pushstring(L, "Key_Mode_switch");
  lua_rawseti(L, enum_table, 16781694);
  lua_pushinteger(L, 16781694);
  lua_setfield(L, enum_table, "Key_Mode_switch");
  lua_pushstring(L, "Key_Kanji");
  lua_rawseti(L, enum_table, 16781601);
  lua_pushinteger(L, 16781601);
  lua_setfield(L, enum_table, "Key_Kanji");
  lua_pushstring(L, "Key_Muhenkan");
  lua_rawseti(L, enum_table, 16781602);
  lua_pushinteger(L, 16781602);
  lua_setfield(L, enum_table, "Key_Muhenkan");
  lua_pushstring(L, "Key_Henkan");
  lua_rawseti(L, enum_table, 16781603);
  lua_pushinteger(L, 16781603);
  lua_setfield(L, enum_table, "Key_Henkan");
  lua_pushstring(L, "Key_Romaji");
  lua_rawseti(L, enum_table, 16781604);
  lua_pushinteger(L, 16781604);
  lua_setfield(L, enum_table, "Key_Romaji");
  lua_pushstring(L, "Key_Hiragana");
  lua_rawseti(L, enum_table, 16781605);
  lua_pushinteger(L, 16781605);
  lua_setfield(L, enum_table, "Key_Hiragana");
  lua_pushstring(L, "Key_Katakana");
  lua_rawseti(L, enum_table, 16781606);
  lua_pushinteger(L, 16781606);
  lua_setfield(L, enum_table, "Key_Katakana");
  lua_pushstring(L, "Key_Hiragana_Katakana");
  lua_rawseti(L, enum_table, 16781607);
  lua_pushinteger(L, 16781607);
  lua_setfield(L, enum_table, "Key_Hiragana_Katakana");
  lua_pushstring(L, "Key_Zenkaku");
  lua_rawseti(L, enum_table, 16781608);
  lua_pushinteger(L, 16781608);
  lua_setfield(L, enum_table, "Key_Zenkaku");
  lua_pushstring(L, "Key_Hankaku");
  lua_rawseti(L, enum_table, 16781609);
  lua_pushinteger(L, 16781609);
  lua_setfield(L, enum_table, "Key_Hankaku");
  lua_pushstring(L, "Key_Zenkaku_Hankaku");
  lua_rawseti(L, enum_table, 16781610);
  lua_pushinteger(L, 16781610);
  lua_setfield(L, enum_table, "Key_Zenkaku_Hankaku");
  lua_pushstring(L, "Key_Touroku");
  lua_rawseti(L, enum_table, 16781611);
  lua_pushinteger(L, 16781611);
  lua_setfield(L, enum_table, "Key_Touroku");
  lua_pushstring(L, "Key_Massyo");
  lua_rawseti(L, enum_table, 16781612);
  lua_pushinteger(L, 16781612);
  lua_setfield(L, enum_table, "Key_Massyo");
  lua_pushstring(L, "Key_Kana_Lock");
  lua_rawseti(L, enum_table, 16781613);
  lua_pushinteger(L, 16781613);
  lua_setfield(L, enum_table, "Key_Kana_Lock");
  lua_pushstring(L, "Key_Kana_Shift");
  lua_rawseti(L, enum_table, 16781614);
  lua_pushinteger(L, 16781614);
  lua_setfield(L, enum_table, "Key_Kana_Shift");
  lua_pushstring(L, "Key_Eisu_Shift");
  lua_rawseti(L, enum_table, 16781615);
  lua_pushinteger(L, 16781615);
  lua_setfield(L, enum_table, "Key_Eisu_Shift");
  lua_pushstring(L, "Key_Eisu_toggle");
  lua_rawseti(L, enum_table, 16781616);
  lua_pushinteger(L, 16781616);
  lua_setfield(L, enum_table, "Key_Eisu_toggle");
  lua_pushstring(L, "Key_Hangul");
  lua_rawseti(L, enum_table, 16781617);
  lua_pushinteger(L, 16781617);
  lua_setfield(L, enum_table, "Key_Hangul");
  lua_pushstring(L, "Key_Hangul_Start");
  lua_rawseti(L, enum_table, 16781618);
  lua_pushinteger(L, 16781618);
  lua_setfield(L, enum_table, "Key_Hangul_Start");
  lua_pushstring(L, "Key_Hangul_End");
  lua_rawseti(L, enum_table, 16781619);
  lua_pushinteger(L, 16781619);
  lua_setfield(L, enum_table, "Key_Hangul_End");
  lua_pushstring(L, "Key_Hangul_Hanja");
  lua_rawseti(L, enum_table, 16781620);
  lua_pushinteger(L, 16781620);
  lua_setfield(L, enum_table, "Key_Hangul_Hanja");
  lua_pushstring(L, "Key_Hangul_Jamo");
  lua_rawseti(L, enum_table, 16781621);
  lua_pushinteger(L, 16781621);
  lua_setfield(L, enum_table, "Key_Hangul_Jamo");
  lua_pushstring(L, "Key_Hangul_Romaja");
  lua_rawseti(L, enum_table, 16781622);
  lua_pushinteger(L, 16781622);
  lua_setfield(L, enum_table, "Key_Hangul_Romaja");
  lua_pushstring(L, "Key_Hangul_Jeonja");
  lua_rawseti(L, enum_table, 16781624);
  lua_pushinteger(L, 16781624);
  lua_setfield(L, enum_table, "Key_Hangul_Jeonja");
  lua_pushstring(L, "Key_Hangul_Banja");
  lua_rawseti(L, enum_table, 16781625);
  lua_pushinteger(L, 16781625);
  lua_setfield(L, enum_table, "Key_Hangul_Banja");
  lua_pushstring(L, "Key_Hangul_PreHanja");
  lua_rawseti(L, enum_table, 16781626);
  lua_pushinteger(L, 16781626);
  lua_setfield(L, enum_table, "Key_Hangul_PreHanja");
  lua_pushstring(L, "Key_Hangul_PostHanja");
  lua_rawseti(L, enum_table, 16781627);
  lua_pushinteger(L, 16781627);
  lua_setfield(L, enum_table, "Key_Hangul_PostHanja");
  lua_pushstring(L, "Key_Hangul_Special");
  lua_rawseti(L, enum_table, 16781631);
  lua_pushinteger(L, 16781631);
  lua_setfield(L, enum_table, "Key_Hangul_Special");
  lua_pushstring(L, "Key_Dead_Grave");
  lua_rawseti(L, enum_table, 16781904);
  lua_pushinteger(L, 16781904);
  lua_setfield(L, enum_table, "Key_Dead_Grave");
  lua_pushstring(L, "Key_Dead_Acute");
  lua_rawseti(L, enum_table, 16781905);
  lua_pushinteger(L, 16781905);
  lua_setfield(L, enum_table, "Key_Dead_Acute");
  lua_pushstring(L, "Key_Dead_Circumflex");
  lua_rawseti(L, enum_table, 16781906);
  lua_pushinteger(L, 16781906);
  lua_setfield(L, enum_table, "Key_Dead_Circumflex");
  lua_pushstring(L, "Key_Dead_Tilde");
  lua_rawseti(L, enum_table, 16781907);
  lua_pushinteger(L, 16781907);
  lua_setfield(L, enum_table, "Key_Dead_Tilde");
  lua_pushstring(L, "Key_Dead_Macron");
  lua_rawseti(L, enum_table, 16781908);
  lua_pushinteger(L, 16781908);
  lua_setfield(L, enum_table, "Key_Dead_Macron");
  lua_pushstring(L, "Key_Dead_Breve");
  lua_rawseti(L, enum_table, 16781909);
  lua_pushinteger(L, 16781909);
  lua_setfield(L, enum_table, "Key_Dead_Breve");
  lua_pushstring(L, "Key_Dead_Abovedot");
  lua_rawseti(L, enum_table, 16781910);
  lua_pushinteger(L, 16781910);
  lua_setfield(L, enum_table, "Key_Dead_Abovedot");
  lua_pushstring(L, "Key_Dead_Diaeresis");
  lua_rawseti(L, enum_table, 16781911);
  lua_pushinteger(L, 16781911);
  lua_setfield(L, enum_table, "Key_Dead_Diaeresis");
  lua_pushstring(L, "Key_Dead_Abovering");
  lua_rawseti(L, enum_table, 16781912);
  lua_pushinteger(L, 16781912);
  lua_setfield(L, enum_table, "Key_Dead_Abovering");
  lua_pushstring(L, "Key_Dead_Doubleacute");
  lua_rawseti(L, enum_table, 16781913);
  lua_pushinteger(L, 16781913);
  lua_setfield(L, enum_table, "Key_Dead_Doubleacute");
  lua_pushstring(L, "Key_Dead_Caron");
  lua_rawseti(L, enum_table, 16781914);
  lua_pushinteger(L, 16781914);
  lua_setfield(L, enum_table, "Key_Dead_Caron");
  lua_pushstring(L, "Key_Dead_Cedilla");
  lua_rawseti(L, enum_table, 16781915);
  lua_pushinteger(L, 16781915);
  lua_setfield(L, enum_table, "Key_Dead_Cedilla");
  lua_pushstring(L, "Key_Dead_Ogonek");
  lua_rawseti(L, enum_table, 16781916);
  lua_pushinteger(L, 16781916);
  lua_setfield(L, enum_table, "Key_Dead_Ogonek");
  lua_pushstring(L, "Key_Dead_Iota");
  lua_rawseti(L, enum_table, 16781917);
  lua_pushinteger(L, 16781917);
  lua_setfield(L, enum_table, "Key_Dead_Iota");
  lua_pushstring(L, "Key_Dead_Voiced_Sound");
  lua_rawseti(L, enum_table, 16781918);
  lua_pushinteger(L, 16781918);
  lua_setfield(L, enum_table, "Key_Dead_Voiced_Sound");
  lua_pushstring(L, "Key_Dead_Semivoiced_Sound");
  lua_rawseti(L, enum_table, 16781919);
  lua_pushinteger(L, 16781919);
  lua_setfield(L, enum_table, "Key_Dead_Semivoiced_Sound");
  lua_pushstring(L, "Key_Dead_Belowdot");
  lua_rawseti(L, enum_table, 16781920);
  lua_pushinteger(L, 16781920);
  lua_setfield(L, enum_table, "Key_Dead_Belowdot");
  lua_pushstring(L, "Key_Dead_Hook");
  lua_rawseti(L, enum_table, 16781921);
  lua_pushinteger(L, 16781921);
  lua_setfield(L, enum_table, "Key_Dead_Hook");
  lua_pushstring(L, "Key_Dead_Horn");
  lua_rawseti(L, enum_table, 16781922);
  lua_pushinteger(L, 16781922);
  lua_setfield(L, enum_table, "Key_Dead_Horn");
  lua_pushstring(L, "Key_Back");
  lua_rawseti(L, enum_table, 16777313);
  lua_pushinteger(L, 16777313);
  lua_setfield(L, enum_table, "Key_Back");
  lua_pushstring(L, "Key_Forward");
  lua_rawseti(L, enum_table, 16777314);
  lua_pushinteger(L, 16777314);
  lua_setfield(L, enum_table, "Key_Forward");
  lua_pushstring(L, "Key_Stop");
  lua_rawseti(L, enum_table, 16777315);
  lua_pushinteger(L, 16777315);
  lua_setfield(L, enum_table, "Key_Stop");
  lua_pushstring(L, "Key_Refresh");
  lua_rawseti(L, enum_table, 16777316);
  lua_pushinteger(L, 16777316);
  lua_setfield(L, enum_table, "Key_Refresh");
  lua_pushstring(L, "Key_VolumeDown");
  lua_rawseti(L, enum_table, 16777328);
  lua_pushinteger(L, 16777328);
  lua_setfield(L, enum_table, "Key_VolumeDown");
  lua_pushstring(L, "Key_VolumeMute");
  lua_rawseti(L, enum_table, 16777329);
  lua_pushinteger(L, 16777329);
  lua_setfield(L, enum_table, "Key_VolumeMute");
  lua_pushstring(L, "Key_VolumeUp");
  lua_rawseti(L, enum_table, 16777330);
  lua_pushinteger(L, 16777330);
  lua_setfield(L, enum_table, "Key_VolumeUp");
  lua_pushstring(L, "Key_BassBoost");
  lua_rawseti(L, enum_table, 16777331);
  lua_pushinteger(L, 16777331);
  lua_setfield(L, enum_table, "Key_BassBoost");
  lua_pushstring(L, "Key_BassUp");
  lua_rawseti(L, enum_table, 16777332);
  lua_pushinteger(L, 16777332);
  lua_setfield(L, enum_table, "Key_BassUp");
  lua_pushstring(L, "Key_BassDown");
  lua_rawseti(L, enum_table, 16777333);
  lua_pushinteger(L, 16777333);
  lua_setfield(L, enum_table, "Key_BassDown");
  lua_pushstring(L, "Key_TrebleUp");
  lua_rawseti(L, enum_table, 16777334);
  lua_pushinteger(L, 16777334);
  lua_setfield(L, enum_table, "Key_TrebleUp");
  lua_pushstring(L, "Key_TrebleDown");
  lua_rawseti(L, enum_table, 16777335);
  lua_pushinteger(L, 16777335);
  lua_setfield(L, enum_table, "Key_TrebleDown");
  lua_pushstring(L, "Key_MediaPlay");
  lua_rawseti(L, enum_table, 16777344);
  lua_pushinteger(L, 16777344);
  lua_setfield(L, enum_table, "Key_MediaPlay");
  lua_pushstring(L, "Key_MediaStop");
  lua_rawseti(L, enum_table, 16777345);
  lua_pushinteger(L, 16777345);
  lua_setfield(L, enum_table, "Key_MediaStop");
  lua_pushstring(L, "Key_MediaPrevious");
  lua_rawseti(L, enum_table, 16777346);
  lua_pushinteger(L, 16777346);
  lua_setfield(L, enum_table, "Key_MediaPrevious");
  lua_pushstring(L, "Key_MediaNext");
  lua_rawseti(L, enum_table, 16777347);
  lua_pushinteger(L, 16777347);
  lua_setfield(L, enum_table, "Key_MediaNext");
  lua_pushstring(L, "Key_MediaRecord");
  lua_rawseti(L, enum_table, 16777348);
  lua_pushinteger(L, 16777348);
  lua_setfield(L, enum_table, "Key_MediaRecord");
  lua_pushstring(L, "Key_HomePage");
  lua_rawseti(L, enum_table, 16777360);
  lua_pushinteger(L, 16777360);
  lua_setfield(L, enum_table, "Key_HomePage");
  lua_pushstring(L, "Key_Favorites");
  lua_rawseti(L, enum_table, 16777361);
  lua_pushinteger(L, 16777361);
  lua_setfield(L, enum_table, "Key_Favorites");
  lua_pushstring(L, "Key_Search");
  lua_rawseti(L, enum_table, 16777362);
  lua_pushinteger(L, 16777362);
  lua_setfield(L, enum_table, "Key_Search");
  lua_pushstring(L, "Key_Standby");
  lua_rawseti(L, enum_table, 16777363);
  lua_pushinteger(L, 16777363);
  lua_setfield(L, enum_table, "Key_Standby");
  lua_pushstring(L, "Key_OpenUrl");
  lua_rawseti(L, enum_table, 16777364);
  lua_pushinteger(L, 16777364);
  lua_setfield(L, enum_table, "Key_OpenUrl");
  lua_pushstring(L, "Key_LaunchMail");
  lua_rawseti(L, enum_table, 16777376);
  lua_pushinteger(L, 16777376);
  lua_setfield(L, enum_table, "Key_LaunchMail");
  lua_pushstring(L, "Key_LaunchMedia");
  lua_rawseti(L, enum_table, 16777377);
  lua_pushinteger(L, 16777377);
  lua_setfield(L, enum_table, "Key_LaunchMedia");
  lua_pushstring(L, "Key_Launch0");
  lua_rawseti(L, enum_table, 16777378);
  lua_pushinteger(L, 16777378);
  lua_setfield(L, enum_table, "Key_Launch0");
  lua_pushstring(L, "Key_Launch1");
  lua_rawseti(L, enum_table, 16777379);
  lua_pushinteger(L, 16777379);
  lua_setfield(L, enum_table, "Key_Launch1");
  lua_pushstring(L, "Key_Launch2");
  lua_rawseti(L, enum_table, 16777380);
  lua_pushinteger(L, 16777380);
  lua_setfield(L, enum_table, "Key_Launch2");
  lua_pushstring(L, "Key_Launch3");
  lua_rawseti(L, enum_table, 16777381);
  lua_pushinteger(L, 16777381);
  lua_setfield(L, enum_table, "Key_Launch3");
  lua_pushstring(L, "Key_Launch4");
  lua_rawseti(L, enum_table, 16777382);
  lua_pushinteger(L, 16777382);
  lua_setfield(L, enum_table, "Key_Launch4");
  lua_pushstring(L, "Key_Launch5");
  lua_rawseti(L, enum_table, 16777383);
  lua_pushinteger(L, 16777383);
  lua_setfield(L, enum_table, "Key_Launch5");
  lua_pushstring(L, "Key_Launch6");
  lua_rawseti(L, enum_table, 16777384);
  lua_pushinteger(L, 16777384);
  lua_setfield(L, enum_table, "Key_Launch6");
  lua_pushstring(L, "Key_Launch7");
  lua_rawseti(L, enum_table, 16777385);
  lua_pushinteger(L, 16777385);
  lua_setfield(L, enum_table, "Key_Launch7");
  lua_pushstring(L, "Key_Launch8");
  lua_rawseti(L, enum_table, 16777386);
  lua_pushinteger(L, 16777386);
  lua_setfield(L, enum_table, "Key_Launch8");
  lua_pushstring(L, "Key_Launch9");
  lua_rawseti(L, enum_table, 16777387);
  lua_pushinteger(L, 16777387);
  lua_setfield(L, enum_table, "Key_Launch9");
  lua_pushstring(L, "Key_LaunchA");
  lua_rawseti(L, enum_table, 16777388);
  lua_pushinteger(L, 16777388);
  lua_setfield(L, enum_table, "Key_LaunchA");
  lua_pushstring(L, "Key_LaunchB");
  lua_rawseti(L, enum_table, 16777389);
  lua_pushinteger(L, 16777389);
  lua_setfield(L, enum_table, "Key_LaunchB");
  lua_pushstring(L, "Key_LaunchC");
  lua_rawseti(L, enum_table, 16777390);
  lua_pushinteger(L, 16777390);
  lua_setfield(L, enum_table, "Key_LaunchC");
  lua_pushstring(L, "Key_LaunchD");
  lua_rawseti(L, enum_table, 16777391);
  lua_pushinteger(L, 16777391);
  lua_setfield(L, enum_table, "Key_LaunchD");
  lua_pushstring(L, "Key_LaunchE");
  lua_rawseti(L, enum_table, 16777392);
  lua_pushinteger(L, 16777392);
  lua_setfield(L, enum_table, "Key_LaunchE");
  lua_pushstring(L, "Key_LaunchF");
  lua_rawseti(L, enum_table, 16777393);
  lua_pushinteger(L, 16777393);
  lua_setfield(L, enum_table, "Key_LaunchF");
  lua_pushstring(L, "Key_MediaLast");
  lua_rawseti(L, enum_table, 16842751);
  lua_pushinteger(L, 16842751);
  lua_setfield(L, enum_table, "Key_MediaLast");
  lua_pushstring(L, "Key_Select");
  lua_rawseti(L, enum_table, 16842752);
  lua_pushinteger(L, 16842752);
  lua_setfield(L, enum_table, "Key_Select");
  lua_pushstring(L, "Key_Yes");
  lua_rawseti(L, enum_table, 16842753);
  lua_pushinteger(L, 16842753);
  lua_setfield(L, enum_table, "Key_Yes");
  lua_pushstring(L, "Key_No");
  lua_rawseti(L, enum_table, 16842754);
  lua_pushinteger(L, 16842754);
  lua_setfield(L, enum_table, "Key_No");
  lua_pushstring(L, "Key_Cancel");
  lua_rawseti(L, enum_table, 16908289);
  lua_pushinteger(L, 16908289);
  lua_setfield(L, enum_table, "Key_Cancel");
  lua_pushstring(L, "Key_Printer");
  lua_rawseti(L, enum_table, 16908290);
  lua_pushinteger(L, 16908290);
  lua_setfield(L, enum_table, "Key_Printer");
  lua_pushstring(L, "Key_Execute");
  lua_rawseti(L, enum_table, 16908291);
  lua_pushinteger(L, 16908291);
  lua_setfield(L, enum_table, "Key_Execute");
  lua_pushstring(L, "Key_Sleep");
  lua_rawseti(L, enum_table, 16908292);
  lua_pushinteger(L, 16908292);
  lua_setfield(L, enum_table, "Key_Sleep");
  lua_pushstring(L, "Key_Play");
  lua_rawseti(L, enum_table, 16908293);
  lua_pushinteger(L, 16908293);
  lua_setfield(L, enum_table, "Key_Play");
  lua_pushstring(L, "Key_Zoom");
  lua_rawseti(L, enum_table, 16908294);
  lua_pushinteger(L, 16908294);
  lua_setfield(L, enum_table, "Key_Zoom");
  lua_pushstring(L, "Key_Context1");
  lua_rawseti(L, enum_table, 17825792);
  lua_pushinteger(L, 17825792);
  lua_setfield(L, enum_table, "Key_Context1");
  lua_pushstring(L, "Key_Context2");
  lua_rawseti(L, enum_table, 17825793);
  lua_pushinteger(L, 17825793);
  lua_setfield(L, enum_table, "Key_Context2");
  lua_pushstring(L, "Key_Context3");
  lua_rawseti(L, enum_table, 17825794);
  lua_pushinteger(L, 17825794);
  lua_setfield(L, enum_table, "Key_Context3");
  lua_pushstring(L, "Key_Context4");
  lua_rawseti(L, enum_table, 17825795);
  lua_pushinteger(L, 17825795);
  lua_setfield(L, enum_table, "Key_Context4");
  lua_pushstring(L, "Key_Call");
  lua_rawseti(L, enum_table, 17825796);
  lua_pushinteger(L, 17825796);
  lua_setfield(L, enum_table, "Key_Call");
  lua_pushstring(L, "Key_Hangup");
  lua_rawseti(L, enum_table, 17825797);
  lua_pushinteger(L, 17825797);
  lua_setfield(L, enum_table, "Key_Hangup");
  lua_pushstring(L, "Key_Flip");
  lua_rawseti(L, enum_table, 17825798);
  lua_pushinteger(L, 17825798);
  lua_setfield(L, enum_table, "Key_Flip");
  lua_pushstring(L, "Key_unknown");
  lua_rawseti(L, enum_table, 33554431);
  lua_pushinteger(L, 33554431);
  lua_setfield(L, enum_table, "Key_unknown");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_Key_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::Key");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_Key_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::Key>*) + sizeof(QFlags<Qt::Key>));
  QFlags<Qt::Key> *fl = static_cast<QFlags<Qt::Key>*>( static_cast<void*>(&static_cast<QFlags<Qt::Key>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::Key>(lqtL_toenum(L, i, "Qt::Key"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::Key>*")) {
		lua_pushstring(L, "QFlags<Qt::Key>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_BGMode (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "TransparentMode");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "TransparentMode");
  lua_pushstring(L, "OpaqueMode");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "OpaqueMode");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_BGMode_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::BGMode");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_BGMode_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::BGMode>*) + sizeof(QFlags<Qt::BGMode>));
  QFlags<Qt::BGMode> *fl = static_cast<QFlags<Qt::BGMode>*>( static_cast<void*>(&static_cast<QFlags<Qt::BGMode>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::BGMode>(lqtL_toenum(L, i, "Qt::BGMode"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::BGMode>*")) {
		lua_pushstring(L, "QFlags<Qt::BGMode>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_ImageConversionFlag (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "ColorMode_Mask");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "ColorMode_Mask");
  lua_pushstring(L, "AutoColor");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "AutoColor");
  lua_pushstring(L, "ColorOnly");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "ColorOnly");
  lua_pushstring(L, "MonoOnly");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "MonoOnly");
  lua_pushstring(L, "AlphaDither_Mask");
  lua_rawseti(L, enum_table, 12);
  lua_pushinteger(L, 12);
  lua_setfield(L, enum_table, "AlphaDither_Mask");
  lua_pushstring(L, "ThresholdAlphaDither");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "ThresholdAlphaDither");
  lua_pushstring(L, "OrderedAlphaDither");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "OrderedAlphaDither");
  lua_pushstring(L, "DiffuseAlphaDither");
  lua_rawseti(L, enum_table, 8);
  lua_pushinteger(L, 8);
  lua_setfield(L, enum_table, "DiffuseAlphaDither");
  lua_pushstring(L, "NoAlpha");
  lua_rawseti(L, enum_table, 12);
  lua_pushinteger(L, 12);
  lua_setfield(L, enum_table, "NoAlpha");
  lua_pushstring(L, "Dither_Mask");
  lua_rawseti(L, enum_table, 48);
  lua_pushinteger(L, 48);
  lua_setfield(L, enum_table, "Dither_Mask");
  lua_pushstring(L, "DiffuseDither");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "DiffuseDither");
  lua_pushstring(L, "OrderedDither");
  lua_rawseti(L, enum_table, 16);
  lua_pushinteger(L, 16);
  lua_setfield(L, enum_table, "OrderedDither");
  lua_pushstring(L, "ThresholdDither");
  lua_rawseti(L, enum_table, 32);
  lua_pushinteger(L, 32);
  lua_setfield(L, enum_table, "ThresholdDither");
  lua_pushstring(L, "DitherMode_Mask");
  lua_rawseti(L, enum_table, 192);
  lua_pushinteger(L, 192);
  lua_setfield(L, enum_table, "DitherMode_Mask");
  lua_pushstring(L, "AutoDither");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "AutoDither");
  lua_pushstring(L, "PreferDither");
  lua_rawseti(L, enum_table, 64);
  lua_pushinteger(L, 64);
  lua_setfield(L, enum_table, "PreferDither");
  lua_pushstring(L, "AvoidDither");
  lua_rawseti(L, enum_table, 128);
  lua_pushinteger(L, 128);
  lua_setfield(L, enum_table, "AvoidDither");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_ImageConversionFlag_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::ImageConversionFlag");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_ImageConversionFlag_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::ImageConversionFlag>*) + sizeof(QFlags<Qt::ImageConversionFlag>));
  QFlags<Qt::ImageConversionFlag> *fl = static_cast<QFlags<Qt::ImageConversionFlag>*>( static_cast<void*>(&static_cast<QFlags<Qt::ImageConversionFlag>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::ImageConversionFlag>(lqtL_toenum(L, i, "Qt::ImageConversionFlag"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::ImageConversionFlag>*")) {
		lua_pushstring(L, "QFlags<Qt::ImageConversionFlag>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_ApplicationAttribute (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "AA_ImmediateWidgetCreation");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "AA_ImmediateWidgetCreation");
  lua_pushstring(L, "AA_MSWindowsUseDirect3DByDefault");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "AA_MSWindowsUseDirect3DByDefault");
  lua_pushstring(L, "AA_AttributeCount");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "AA_AttributeCount");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_ApplicationAttribute_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::ApplicationAttribute");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_ApplicationAttribute_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::ApplicationAttribute>*) + sizeof(QFlags<Qt::ApplicationAttribute>));
  QFlags<Qt::ApplicationAttribute> *fl = static_cast<QFlags<Qt::ApplicationAttribute>*>( static_cast<void*>(&static_cast<QFlags<Qt::ApplicationAttribute>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::ApplicationAttribute>(lqtL_toenum(L, i, "Qt::ApplicationAttribute"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::ApplicationAttribute>*")) {
		lua_pushstring(L, "QFlags<Qt::ApplicationAttribute>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_WidgetAttribute (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "WA_Disabled");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "WA_Disabled");
  lua_pushstring(L, "WA_UnderMouse");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "WA_UnderMouse");
  lua_pushstring(L, "WA_MouseTracking");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "WA_MouseTracking");
  lua_pushstring(L, "WA_ContentsPropagated");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "WA_ContentsPropagated");
  lua_pushstring(L, "WA_OpaquePaintEvent");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "WA_OpaquePaintEvent");
  lua_pushstring(L, "WA_NoBackground");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "WA_NoBackground");
  lua_pushstring(L, "WA_StaticContents");
  lua_rawseti(L, enum_table, 5);
  lua_pushinteger(L, 5);
  lua_setfield(L, enum_table, "WA_StaticContents");
  lua_pushstring(L, "WA_LaidOut");
  lua_rawseti(L, enum_table, 7);
  lua_pushinteger(L, 7);
  lua_setfield(L, enum_table, "WA_LaidOut");
  lua_pushstring(L, "WA_PaintOnScreen");
  lua_rawseti(L, enum_table, 8);
  lua_pushinteger(L, 8);
  lua_setfield(L, enum_table, "WA_PaintOnScreen");
  lua_pushstring(L, "WA_NoSystemBackground");
  lua_rawseti(L, enum_table, 9);
  lua_pushinteger(L, 9);
  lua_setfield(L, enum_table, "WA_NoSystemBackground");
  lua_pushstring(L, "WA_UpdatesDisabled");
  lua_rawseti(L, enum_table, 10);
  lua_pushinteger(L, 10);
  lua_setfield(L, enum_table, "WA_UpdatesDisabled");
  lua_pushstring(L, "WA_Mapped");
  lua_rawseti(L, enum_table, 11);
  lua_pushinteger(L, 11);
  lua_setfield(L, enum_table, "WA_Mapped");
  lua_pushstring(L, "WA_MacNoClickThrough");
  lua_rawseti(L, enum_table, 12);
  lua_pushinteger(L, 12);
  lua_setfield(L, enum_table, "WA_MacNoClickThrough");
  lua_pushstring(L, "WA_PaintOutsidePaintEvent");
  lua_rawseti(L, enum_table, 13);
  lua_pushinteger(L, 13);
  lua_setfield(L, enum_table, "WA_PaintOutsidePaintEvent");
  lua_pushstring(L, "WA_InputMethodEnabled");
  lua_rawseti(L, enum_table, 14);
  lua_pushinteger(L, 14);
  lua_setfield(L, enum_table, "WA_InputMethodEnabled");
  lua_pushstring(L, "WA_WState_Visible");
  lua_rawseti(L, enum_table, 15);
  lua_pushinteger(L, 15);
  lua_setfield(L, enum_table, "WA_WState_Visible");
  lua_pushstring(L, "WA_WState_Hidden");
  lua_rawseti(L, enum_table, 16);
  lua_pushinteger(L, 16);
  lua_setfield(L, enum_table, "WA_WState_Hidden");
  lua_pushstring(L, "WA_ForceDisabled");
  lua_rawseti(L, enum_table, 32);
  lua_pushinteger(L, 32);
  lua_setfield(L, enum_table, "WA_ForceDisabled");
  lua_pushstring(L, "WA_KeyCompression");
  lua_rawseti(L, enum_table, 33);
  lua_pushinteger(L, 33);
  lua_setfield(L, enum_table, "WA_KeyCompression");
  lua_pushstring(L, "WA_PendingMoveEvent");
  lua_rawseti(L, enum_table, 34);
  lua_pushinteger(L, 34);
  lua_setfield(L, enum_table, "WA_PendingMoveEvent");
  lua_pushstring(L, "WA_PendingResizeEvent");
  lua_rawseti(L, enum_table, 35);
  lua_pushinteger(L, 35);
  lua_setfield(L, enum_table, "WA_PendingResizeEvent");
  lua_pushstring(L, "WA_SetPalette");
  lua_rawseti(L, enum_table, 36);
  lua_pushinteger(L, 36);
  lua_setfield(L, enum_table, "WA_SetPalette");
  lua_pushstring(L, "WA_SetFont");
  lua_rawseti(L, enum_table, 37);
  lua_pushinteger(L, 37);
  lua_setfield(L, enum_table, "WA_SetFont");
  lua_pushstring(L, "WA_SetCursor");
  lua_rawseti(L, enum_table, 38);
  lua_pushinteger(L, 38);
  lua_setfield(L, enum_table, "WA_SetCursor");
  lua_pushstring(L, "WA_NoChildEventsFromChildren");
  lua_rawseti(L, enum_table, 39);
  lua_pushinteger(L, 39);
  lua_setfield(L, enum_table, "WA_NoChildEventsFromChildren");
  lua_pushstring(L, "WA_WindowModified");
  lua_rawseti(L, enum_table, 41);
  lua_pushinteger(L, 41);
  lua_setfield(L, enum_table, "WA_WindowModified");
  lua_pushstring(L, "WA_Resized");
  lua_rawseti(L, enum_table, 42);
  lua_pushinteger(L, 42);
  lua_setfield(L, enum_table, "WA_Resized");
  lua_pushstring(L, "WA_Moved");
  lua_rawseti(L, enum_table, 43);
  lua_pushinteger(L, 43);
  lua_setfield(L, enum_table, "WA_Moved");
  lua_pushstring(L, "WA_PendingUpdate");
  lua_rawseti(L, enum_table, 44);
  lua_pushinteger(L, 44);
  lua_setfield(L, enum_table, "WA_PendingUpdate");
  lua_pushstring(L, "WA_InvalidSize");
  lua_rawseti(L, enum_table, 45);
  lua_pushinteger(L, 45);
  lua_setfield(L, enum_table, "WA_InvalidSize");
  lua_pushstring(L, "WA_MacBrushedMetal");
  lua_rawseti(L, enum_table, 46);
  lua_pushinteger(L, 46);
  lua_setfield(L, enum_table, "WA_MacBrushedMetal");
  lua_pushstring(L, "WA_MacMetalStyle");
  lua_rawseti(L, enum_table, 46);
  lua_pushinteger(L, 46);
  lua_setfield(L, enum_table, "WA_MacMetalStyle");
  lua_pushstring(L, "WA_CustomWhatsThis");
  lua_rawseti(L, enum_table, 47);
  lua_pushinteger(L, 47);
  lua_setfield(L, enum_table, "WA_CustomWhatsThis");
  lua_pushstring(L, "WA_LayoutOnEntireRect");
  lua_rawseti(L, enum_table, 48);
  lua_pushinteger(L, 48);
  lua_setfield(L, enum_table, "WA_LayoutOnEntireRect");
  lua_pushstring(L, "WA_OutsideWSRange");
  lua_rawseti(L, enum_table, 49);
  lua_pushinteger(L, 49);
  lua_setfield(L, enum_table, "WA_OutsideWSRange");
  lua_pushstring(L, "WA_GrabbedShortcut");
  lua_rawseti(L, enum_table, 50);
  lua_pushinteger(L, 50);
  lua_setfield(L, enum_table, "WA_GrabbedShortcut");
  lua_pushstring(L, "WA_TransparentForMouseEvents");
  lua_rawseti(L, enum_table, 51);
  lua_pushinteger(L, 51);
  lua_setfield(L, enum_table, "WA_TransparentForMouseEvents");
  lua_pushstring(L, "WA_PaintUnclipped");
  lua_rawseti(L, enum_table, 52);
  lua_pushinteger(L, 52);
  lua_setfield(L, enum_table, "WA_PaintUnclipped");
  lua_pushstring(L, "WA_SetWindowIcon");
  lua_rawseti(L, enum_table, 53);
  lua_pushinteger(L, 53);
  lua_setfield(L, enum_table, "WA_SetWindowIcon");
  lua_pushstring(L, "WA_NoMouseReplay");
  lua_rawseti(L, enum_table, 54);
  lua_pushinteger(L, 54);
  lua_setfield(L, enum_table, "WA_NoMouseReplay");
  lua_pushstring(L, "WA_DeleteOnClose");
  lua_rawseti(L, enum_table, 55);
  lua_pushinteger(L, 55);
  lua_setfield(L, enum_table, "WA_DeleteOnClose");
  lua_pushstring(L, "WA_RightToLeft");
  lua_rawseti(L, enum_table, 56);
  lua_pushinteger(L, 56);
  lua_setfield(L, enum_table, "WA_RightToLeft");
  lua_pushstring(L, "WA_SetLayoutDirection");
  lua_rawseti(L, enum_table, 57);
  lua_pushinteger(L, 57);
  lua_setfield(L, enum_table, "WA_SetLayoutDirection");
  lua_pushstring(L, "WA_NoChildEventsForParent");
  lua_rawseti(L, enum_table, 58);
  lua_pushinteger(L, 58);
  lua_setfield(L, enum_table, "WA_NoChildEventsForParent");
  lua_pushstring(L, "WA_ForceUpdatesDisabled");
  lua_rawseti(L, enum_table, 59);
  lua_pushinteger(L, 59);
  lua_setfield(L, enum_table, "WA_ForceUpdatesDisabled");
  lua_pushstring(L, "WA_WState_Created");
  lua_rawseti(L, enum_table, 60);
  lua_pushinteger(L, 60);
  lua_setfield(L, enum_table, "WA_WState_Created");
  lua_pushstring(L, "WA_WState_CompressKeys");
  lua_rawseti(L, enum_table, 61);
  lua_pushinteger(L, 61);
  lua_setfield(L, enum_table, "WA_WState_CompressKeys");
  lua_pushstring(L, "WA_WState_InPaintEvent");
  lua_rawseti(L, enum_table, 62);
  lua_pushinteger(L, 62);
  lua_setfield(L, enum_table, "WA_WState_InPaintEvent");
  lua_pushstring(L, "WA_WState_Reparented");
  lua_rawseti(L, enum_table, 63);
  lua_pushinteger(L, 63);
  lua_setfield(L, enum_table, "WA_WState_Reparented");
  lua_pushstring(L, "WA_WState_ConfigPending");
  lua_rawseti(L, enum_table, 64);
  lua_pushinteger(L, 64);
  lua_setfield(L, enum_table, "WA_WState_ConfigPending");
  lua_pushstring(L, "WA_WState_Polished");
  lua_rawseti(L, enum_table, 66);
  lua_pushinteger(L, 66);
  lua_setfield(L, enum_table, "WA_WState_Polished");
  lua_pushstring(L, "WA_WState_DND");
  lua_rawseti(L, enum_table, 67);
  lua_pushinteger(L, 67);
  lua_setfield(L, enum_table, "WA_WState_DND");
  lua_pushstring(L, "WA_WState_OwnSizePolicy");
  lua_rawseti(L, enum_table, 68);
  lua_pushinteger(L, 68);
  lua_setfield(L, enum_table, "WA_WState_OwnSizePolicy");
  lua_pushstring(L, "WA_WState_ExplicitShowHide");
  lua_rawseti(L, enum_table, 69);
  lua_pushinteger(L, 69);
  lua_setfield(L, enum_table, "WA_WState_ExplicitShowHide");
  lua_pushstring(L, "WA_ShowModal");
  lua_rawseti(L, enum_table, 70);
  lua_pushinteger(L, 70);
  lua_setfield(L, enum_table, "WA_ShowModal");
  lua_pushstring(L, "WA_MouseNoMask");
  lua_rawseti(L, enum_table, 71);
  lua_pushinteger(L, 71);
  lua_setfield(L, enum_table, "WA_MouseNoMask");
  lua_pushstring(L, "WA_GroupLeader");
  lua_rawseti(L, enum_table, 72);
  lua_pushinteger(L, 72);
  lua_setfield(L, enum_table, "WA_GroupLeader");
  lua_pushstring(L, "WA_NoMousePropagation");
  lua_rawseti(L, enum_table, 73);
  lua_pushinteger(L, 73);
  lua_setfield(L, enum_table, "WA_NoMousePropagation");
  lua_pushstring(L, "WA_Hover");
  lua_rawseti(L, enum_table, 74);
  lua_pushinteger(L, 74);
  lua_setfield(L, enum_table, "WA_Hover");
  lua_pushstring(L, "WA_InputMethodTransparent");
  lua_rawseti(L, enum_table, 75);
  lua_pushinteger(L, 75);
  lua_setfield(L, enum_table, "WA_InputMethodTransparent");
  lua_pushstring(L, "WA_QuitOnClose");
  lua_rawseti(L, enum_table, 76);
  lua_pushinteger(L, 76);
  lua_setfield(L, enum_table, "WA_QuitOnClose");
  lua_pushstring(L, "WA_KeyboardFocusChange");
  lua_rawseti(L, enum_table, 77);
  lua_pushinteger(L, 77);
  lua_setfield(L, enum_table, "WA_KeyboardFocusChange");
  lua_pushstring(L, "WA_AcceptDrops");
  lua_rawseti(L, enum_table, 78);
  lua_pushinteger(L, 78);
  lua_setfield(L, enum_table, "WA_AcceptDrops");
  lua_pushstring(L, "WA_DropSiteRegistered");
  lua_rawseti(L, enum_table, 79);
  lua_pushinteger(L, 79);
  lua_setfield(L, enum_table, "WA_DropSiteRegistered");
  lua_pushstring(L, "WA_ForceAcceptDrops");
  lua_rawseti(L, enum_table, 79);
  lua_pushinteger(L, 79);
  lua_setfield(L, enum_table, "WA_ForceAcceptDrops");
  lua_pushstring(L, "WA_WindowPropagation");
  lua_rawseti(L, enum_table, 80);
  lua_pushinteger(L, 80);
  lua_setfield(L, enum_table, "WA_WindowPropagation");
  lua_pushstring(L, "WA_NoX11EventCompression");
  lua_rawseti(L, enum_table, 81);
  lua_pushinteger(L, 81);
  lua_setfield(L, enum_table, "WA_NoX11EventCompression");
  lua_pushstring(L, "WA_TintedBackground");
  lua_rawseti(L, enum_table, 82);
  lua_pushinteger(L, 82);
  lua_setfield(L, enum_table, "WA_TintedBackground");
  lua_pushstring(L, "WA_X11OpenGLOverlay");
  lua_rawseti(L, enum_table, 83);
  lua_pushinteger(L, 83);
  lua_setfield(L, enum_table, "WA_X11OpenGLOverlay");
  lua_pushstring(L, "WA_AlwaysShowToolTips");
  lua_rawseti(L, enum_table, 84);
  lua_pushinteger(L, 84);
  lua_setfield(L, enum_table, "WA_AlwaysShowToolTips");
  lua_pushstring(L, "WA_MacOpaqueSizeGrip");
  lua_rawseti(L, enum_table, 85);
  lua_pushinteger(L, 85);
  lua_setfield(L, enum_table, "WA_MacOpaqueSizeGrip");
  lua_pushstring(L, "WA_SetStyle");
  lua_rawseti(L, enum_table, 86);
  lua_pushinteger(L, 86);
  lua_setfield(L, enum_table, "WA_SetStyle");
  lua_pushstring(L, "WA_SetLocale");
  lua_rawseti(L, enum_table, 87);
  lua_pushinteger(L, 87);
  lua_setfield(L, enum_table, "WA_SetLocale");
  lua_pushstring(L, "WA_MacShowFocusRect");
  lua_rawseti(L, enum_table, 88);
  lua_pushinteger(L, 88);
  lua_setfield(L, enum_table, "WA_MacShowFocusRect");
  lua_pushstring(L, "WA_MacNormalSize");
  lua_rawseti(L, enum_table, 89);
  lua_pushinteger(L, 89);
  lua_setfield(L, enum_table, "WA_MacNormalSize");
  lua_pushstring(L, "WA_MacSmallSize");
  lua_rawseti(L, enum_table, 90);
  lua_pushinteger(L, 90);
  lua_setfield(L, enum_table, "WA_MacSmallSize");
  lua_pushstring(L, "WA_MacMiniSize");
  lua_rawseti(L, enum_table, 91);
  lua_pushinteger(L, 91);
  lua_setfield(L, enum_table, "WA_MacMiniSize");
  lua_pushstring(L, "WA_LayoutUsesWidgetRect");
  lua_rawseti(L, enum_table, 92);
  lua_pushinteger(L, 92);
  lua_setfield(L, enum_table, "WA_LayoutUsesWidgetRect");
  lua_pushstring(L, "WA_StyledBackground");
  lua_rawseti(L, enum_table, 93);
  lua_pushinteger(L, 93);
  lua_setfield(L, enum_table, "WA_StyledBackground");
  lua_pushstring(L, "WA_MSWindowsUseDirect3D");
  lua_rawseti(L, enum_table, 94);
  lua_pushinteger(L, 94);
  lua_setfield(L, enum_table, "WA_MSWindowsUseDirect3D");
  lua_pushstring(L, "WA_CanHostQMdiSubWindowTitleBar");
  lua_rawseti(L, enum_table, 95);
  lua_pushinteger(L, 95);
  lua_setfield(L, enum_table, "WA_CanHostQMdiSubWindowTitleBar");
  lua_pushstring(L, "WA_MacAlwaysShowToolWindow");
  lua_rawseti(L, enum_table, 96);
  lua_pushinteger(L, 96);
  lua_setfield(L, enum_table, "WA_MacAlwaysShowToolWindow");
  lua_pushstring(L, "WA_StyleSheet");
  lua_rawseti(L, enum_table, 97);
  lua_pushinteger(L, 97);
  lua_setfield(L, enum_table, "WA_StyleSheet");
  lua_pushstring(L, "WA_AttributeCount");
  lua_rawseti(L, enum_table, 98);
  lua_pushinteger(L, 98);
  lua_setfield(L, enum_table, "WA_AttributeCount");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_WidgetAttribute_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::WidgetAttribute");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_WidgetAttribute_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::WidgetAttribute>*) + sizeof(QFlags<Qt::WidgetAttribute>));
  QFlags<Qt::WidgetAttribute> *fl = static_cast<QFlags<Qt::WidgetAttribute>*>( static_cast<void*>(&static_cast<QFlags<Qt::WidgetAttribute>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::WidgetAttribute>(lqtL_toenum(L, i, "Qt::WidgetAttribute"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::WidgetAttribute>*")) {
		lua_pushstring(L, "QFlags<Qt::WidgetAttribute>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_WindowState (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "WindowNoState");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "WindowNoState");
  lua_pushstring(L, "WindowMinimized");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "WindowMinimized");
  lua_pushstring(L, "WindowMaximized");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "WindowMaximized");
  lua_pushstring(L, "WindowFullScreen");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "WindowFullScreen");
  lua_pushstring(L, "WindowActive");
  lua_rawseti(L, enum_table, 8);
  lua_pushinteger(L, 8);
  lua_setfield(L, enum_table, "WindowActive");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_WindowState_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::WindowState");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_WindowState_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::WindowState>*) + sizeof(QFlags<Qt::WindowState>));
  QFlags<Qt::WindowState> *fl = static_cast<QFlags<Qt::WindowState>*>( static_cast<void*>(&static_cast<QFlags<Qt::WindowState>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::WindowState>(lqtL_toenum(L, i, "Qt::WindowState"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::WindowState>*")) {
		lua_pushstring(L, "QFlags<Qt::WindowState>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_WindowType (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "Widget");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "Widget");
  lua_pushstring(L, "Window");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "Window");
  lua_pushstring(L, "Dialog");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "Dialog");
  lua_pushstring(L, "Sheet");
  lua_rawseti(L, enum_table, 5);
  lua_pushinteger(L, 5);
  lua_setfield(L, enum_table, "Sheet");
  lua_pushstring(L, "Drawer");
  lua_rawseti(L, enum_table, 7);
  lua_pushinteger(L, 7);
  lua_setfield(L, enum_table, "Drawer");
  lua_pushstring(L, "Popup");
  lua_rawseti(L, enum_table, 9);
  lua_pushinteger(L, 9);
  lua_setfield(L, enum_table, "Popup");
  lua_pushstring(L, "Tool");
  lua_rawseti(L, enum_table, 11);
  lua_pushinteger(L, 11);
  lua_setfield(L, enum_table, "Tool");
  lua_pushstring(L, "ToolTip");
  lua_rawseti(L, enum_table, 13);
  lua_pushinteger(L, 13);
  lua_setfield(L, enum_table, "ToolTip");
  lua_pushstring(L, "SplashScreen");
  lua_rawseti(L, enum_table, 15);
  lua_pushinteger(L, 15);
  lua_setfield(L, enum_table, "SplashScreen");
  lua_pushstring(L, "Desktop");
  lua_rawseti(L, enum_table, 17);
  lua_pushinteger(L, 17);
  lua_setfield(L, enum_table, "Desktop");
  lua_pushstring(L, "SubWindow");
  lua_rawseti(L, enum_table, 18);
  lua_pushinteger(L, 18);
  lua_setfield(L, enum_table, "SubWindow");
  lua_pushstring(L, "WindowType_Mask");
  lua_rawseti(L, enum_table, 255);
  lua_pushinteger(L, 255);
  lua_setfield(L, enum_table, "WindowType_Mask");
  lua_pushstring(L, "MSWindowsFixedSizeDialogHint");
  lua_rawseti(L, enum_table, 256);
  lua_pushinteger(L, 256);
  lua_setfield(L, enum_table, "MSWindowsFixedSizeDialogHint");
  lua_pushstring(L, "MSWindowsOwnDC");
  lua_rawseti(L, enum_table, 512);
  lua_pushinteger(L, 512);
  lua_setfield(L, enum_table, "MSWindowsOwnDC");
  lua_pushstring(L, "X11BypassWindowManagerHint");
  lua_rawseti(L, enum_table, 1024);
  lua_pushinteger(L, 1024);
  lua_setfield(L, enum_table, "X11BypassWindowManagerHint");
  lua_pushstring(L, "FramelessWindowHint");
  lua_rawseti(L, enum_table, 2048);
  lua_pushinteger(L, 2048);
  lua_setfield(L, enum_table, "FramelessWindowHint");
  lua_pushstring(L, "WindowTitleHint");
  lua_rawseti(L, enum_table, 4096);
  lua_pushinteger(L, 4096);
  lua_setfield(L, enum_table, "WindowTitleHint");
  lua_pushstring(L, "WindowSystemMenuHint");
  lua_rawseti(L, enum_table, 8192);
  lua_pushinteger(L, 8192);
  lua_setfield(L, enum_table, "WindowSystemMenuHint");
  lua_pushstring(L, "WindowMinimizeButtonHint");
  lua_rawseti(L, enum_table, 16384);
  lua_pushinteger(L, 16384);
  lua_setfield(L, enum_table, "WindowMinimizeButtonHint");
  lua_pushstring(L, "WindowMaximizeButtonHint");
  lua_rawseti(L, enum_table, 32768);
  lua_pushinteger(L, 32768);
  lua_setfield(L, enum_table, "WindowMaximizeButtonHint");
  lua_pushstring(L, "WindowMinMaxButtonsHint");
  lua_rawseti(L, enum_table, 49152);
  lua_pushinteger(L, 49152);
  lua_setfield(L, enum_table, "WindowMinMaxButtonsHint");
  lua_pushstring(L, "WindowContextHelpButtonHint");
  lua_rawseti(L, enum_table, 65536);
  lua_pushinteger(L, 65536);
  lua_setfield(L, enum_table, "WindowContextHelpButtonHint");
  lua_pushstring(L, "WindowShadeButtonHint");
  lua_rawseti(L, enum_table, 131072);
  lua_pushinteger(L, 131072);
  lua_setfield(L, enum_table, "WindowShadeButtonHint");
  lua_pushstring(L, "WindowStaysOnTopHint");
  lua_rawseti(L, enum_table, 262144);
  lua_pushinteger(L, 262144);
  lua_setfield(L, enum_table, "WindowStaysOnTopHint");
  lua_pushstring(L, "CustomizeWindowHint");
  lua_rawseti(L, enum_table, 33554432);
  lua_pushinteger(L, 33554432);
  lua_setfield(L, enum_table, "CustomizeWindowHint");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_WindowType_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::WindowType");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_WindowType_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::WindowType>*) + sizeof(QFlags<Qt::WindowType>));
  QFlags<Qt::WindowType> *fl = static_cast<QFlags<Qt::WindowType>*>( static_cast<void*>(&static_cast<QFlags<Qt::WindowType>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::WindowType>(lqtL_toenum(L, i, "Qt::WindowType"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::WindowType>*")) {
		lua_pushstring(L, "QFlags<Qt::WindowType>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_TextElideMode (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "ElideLeft");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "ElideLeft");
  lua_pushstring(L, "ElideRight");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "ElideRight");
  lua_pushstring(L, "ElideMiddle");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "ElideMiddle");
  lua_pushstring(L, "ElideNone");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "ElideNone");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_TextElideMode_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::TextElideMode");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_TextElideMode_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::TextElideMode>*) + sizeof(QFlags<Qt::TextElideMode>));
  QFlags<Qt::TextElideMode> *fl = static_cast<QFlags<Qt::TextElideMode>*>( static_cast<void*>(&static_cast<QFlags<Qt::TextElideMode>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::TextElideMode>(lqtL_toenum(L, i, "Qt::TextElideMode"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::TextElideMode>*")) {
		lua_pushstring(L, "QFlags<Qt::TextElideMode>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_TextFlag (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "TextSingleLine");
  lua_rawseti(L, enum_table, 256);
  lua_pushinteger(L, 256);
  lua_setfield(L, enum_table, "TextSingleLine");
  lua_pushstring(L, "TextDontClip");
  lua_rawseti(L, enum_table, 512);
  lua_pushinteger(L, 512);
  lua_setfield(L, enum_table, "TextDontClip");
  lua_pushstring(L, "TextExpandTabs");
  lua_rawseti(L, enum_table, 1024);
  lua_pushinteger(L, 1024);
  lua_setfield(L, enum_table, "TextExpandTabs");
  lua_pushstring(L, "TextShowMnemonic");
  lua_rawseti(L, enum_table, 2048);
  lua_pushinteger(L, 2048);
  lua_setfield(L, enum_table, "TextShowMnemonic");
  lua_pushstring(L, "TextWordWrap");
  lua_rawseti(L, enum_table, 4096);
  lua_pushinteger(L, 4096);
  lua_setfield(L, enum_table, "TextWordWrap");
  lua_pushstring(L, "TextWrapAnywhere");
  lua_rawseti(L, enum_table, 8192);
  lua_pushinteger(L, 8192);
  lua_setfield(L, enum_table, "TextWrapAnywhere");
  lua_pushstring(L, "TextDontPrint");
  lua_rawseti(L, enum_table, 16384);
  lua_pushinteger(L, 16384);
  lua_setfield(L, enum_table, "TextDontPrint");
  lua_pushstring(L, "TextIncludeTrailingSpaces");
  lua_rawseti(L, enum_table, 134217728);
  lua_pushinteger(L, 134217728);
  lua_setfield(L, enum_table, "TextIncludeTrailingSpaces");
  lua_pushstring(L, "TextHideMnemonic");
  lua_rawseti(L, enum_table, 32768);
  lua_pushinteger(L, 32768);
  lua_setfield(L, enum_table, "TextHideMnemonic");
  lua_pushstring(L, "TextJustificationForced");
  lua_rawseti(L, enum_table, 65536);
  lua_pushinteger(L, 65536);
  lua_setfield(L, enum_table, "TextJustificationForced");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_TextFlag_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::TextFlag");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_TextFlag_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::TextFlag>*) + sizeof(QFlags<Qt::TextFlag>));
  QFlags<Qt::TextFlag> *fl = static_cast<QFlags<Qt::TextFlag>*>( static_cast<void*>(&static_cast<QFlags<Qt::TextFlag>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::TextFlag>(lqtL_toenum(L, i, "Qt::TextFlag"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::TextFlag>*")) {
		lua_pushstring(L, "QFlags<Qt::TextFlag>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_AlignmentFlag (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "AlignLeft");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "AlignLeft");
  lua_pushstring(L, "AlignLeading");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "AlignLeading");
  lua_pushstring(L, "AlignRight");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "AlignRight");
  lua_pushstring(L, "AlignTrailing");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "AlignTrailing");
  lua_pushstring(L, "AlignHCenter");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "AlignHCenter");
  lua_pushstring(L, "AlignJustify");
  lua_rawseti(L, enum_table, 8);
  lua_pushinteger(L, 8);
  lua_setfield(L, enum_table, "AlignJustify");
  lua_pushstring(L, "AlignAbsolute");
  lua_rawseti(L, enum_table, 16);
  lua_pushinteger(L, 16);
  lua_setfield(L, enum_table, "AlignAbsolute");
  lua_pushstring(L, "AlignHorizontal_Mask");
  lua_rawseti(L, enum_table, 31);
  lua_pushinteger(L, 31);
  lua_setfield(L, enum_table, "AlignHorizontal_Mask");
  lua_pushstring(L, "AlignTop");
  lua_rawseti(L, enum_table, 32);
  lua_pushinteger(L, 32);
  lua_setfield(L, enum_table, "AlignTop");
  lua_pushstring(L, "AlignBottom");
  lua_rawseti(L, enum_table, 64);
  lua_pushinteger(L, 64);
  lua_setfield(L, enum_table, "AlignBottom");
  lua_pushstring(L, "AlignVCenter");
  lua_rawseti(L, enum_table, 128);
  lua_pushinteger(L, 128);
  lua_setfield(L, enum_table, "AlignVCenter");
  lua_pushstring(L, "AlignVertical_Mask");
  lua_rawseti(L, enum_table, 224);
  lua_pushinteger(L, 224);
  lua_setfield(L, enum_table, "AlignVertical_Mask");
  lua_pushstring(L, "AlignCenter");
  lua_rawseti(L, enum_table, 132);
  lua_pushinteger(L, 132);
  lua_setfield(L, enum_table, "AlignCenter");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_AlignmentFlag_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::AlignmentFlag");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_AlignmentFlag_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::AlignmentFlag>*) + sizeof(QFlags<Qt::AlignmentFlag>));
  QFlags<Qt::AlignmentFlag> *fl = static_cast<QFlags<Qt::AlignmentFlag>*>( static_cast<void*>(&static_cast<QFlags<Qt::AlignmentFlag>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::AlignmentFlag>(lqtL_toenum(L, i, "Qt::AlignmentFlag"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::AlignmentFlag>*")) {
		lua_pushstring(L, "QFlags<Qt::AlignmentFlag>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_SortOrder (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "AscendingOrder");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "AscendingOrder");
  lua_pushstring(L, "DescendingOrder");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "DescendingOrder");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_SortOrder_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::SortOrder");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_SortOrder_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::SortOrder>*) + sizeof(QFlags<Qt::SortOrder>));
  QFlags<Qt::SortOrder> *fl = static_cast<QFlags<Qt::SortOrder>*>( static_cast<void*>(&static_cast<QFlags<Qt::SortOrder>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::SortOrder>(lqtL_toenum(L, i, "Qt::SortOrder"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::SortOrder>*")) {
		lua_pushstring(L, "QFlags<Qt::SortOrder>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_FocusPolicy (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "NoFocus");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "NoFocus");
  lua_pushstring(L, "TabFocus");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "TabFocus");
  lua_pushstring(L, "ClickFocus");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "ClickFocus");
  lua_pushstring(L, "StrongFocus");
  lua_rawseti(L, enum_table, 11);
  lua_pushinteger(L, 11);
  lua_setfield(L, enum_table, "StrongFocus");
  lua_pushstring(L, "WheelFocus");
  lua_rawseti(L, enum_table, 15);
  lua_pushinteger(L, 15);
  lua_setfield(L, enum_table, "WheelFocus");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_FocusPolicy_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::FocusPolicy");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_FocusPolicy_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::FocusPolicy>*) + sizeof(QFlags<Qt::FocusPolicy>));
  QFlags<Qt::FocusPolicy> *fl = static_cast<QFlags<Qt::FocusPolicy>*>( static_cast<void*>(&static_cast<QFlags<Qt::FocusPolicy>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::FocusPolicy>(lqtL_toenum(L, i, "Qt::FocusPolicy"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::FocusPolicy>*")) {
		lua_pushstring(L, "QFlags<Qt::FocusPolicy>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_Orientation (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "Horizontal");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "Horizontal");
  lua_pushstring(L, "Vertical");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "Vertical");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_Orientation_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::Orientation");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_Orientation_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::Orientation>*) + sizeof(QFlags<Qt::Orientation>));
  QFlags<Qt::Orientation> *fl = static_cast<QFlags<Qt::Orientation>*>( static_cast<void*>(&static_cast<QFlags<Qt::Orientation>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::Orientation>(lqtL_toenum(L, i, "Qt::Orientation"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::Orientation>*")) {
		lua_pushstring(L, "QFlags<Qt::Orientation>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_MouseButton (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "NoButton");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "NoButton");
  lua_pushstring(L, "LeftButton");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "LeftButton");
  lua_pushstring(L, "RightButton");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "RightButton");
  lua_pushstring(L, "MidButton");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "MidButton");
  lua_pushstring(L, "XButton1");
  lua_rawseti(L, enum_table, 8);
  lua_pushinteger(L, 8);
  lua_setfield(L, enum_table, "XButton1");
  lua_pushstring(L, "XButton2");
  lua_rawseti(L, enum_table, 16);
  lua_pushinteger(L, 16);
  lua_setfield(L, enum_table, "XButton2");
  lua_pushstring(L, "MouseButtonMask");
  lua_rawseti(L, enum_table, 255);
  lua_pushinteger(L, 255);
  lua_setfield(L, enum_table, "MouseButtonMask");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_MouseButton_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::MouseButton");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_MouseButton_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::MouseButton>*) + sizeof(QFlags<Qt::MouseButton>));
  QFlags<Qt::MouseButton> *fl = static_cast<QFlags<Qt::MouseButton>*>( static_cast<void*>(&static_cast<QFlags<Qt::MouseButton>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::MouseButton>(lqtL_toenum(L, i, "Qt::MouseButton"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::MouseButton>*")) {
		lua_pushstring(L, "QFlags<Qt::MouseButton>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_Modifier (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "META");
  lua_rawseti(L, enum_table, 268435456);
  lua_pushinteger(L, 268435456);
  lua_setfield(L, enum_table, "META");
  lua_pushstring(L, "SHIFT");
  lua_rawseti(L, enum_table, 33554432);
  lua_pushinteger(L, 33554432);
  lua_setfield(L, enum_table, "SHIFT");
  lua_pushstring(L, "CTRL");
  lua_rawseti(L, enum_table, 67108864);
  lua_pushinteger(L, 67108864);
  lua_setfield(L, enum_table, "CTRL");
  lua_pushstring(L, "ALT");
  lua_rawseti(L, enum_table, 134217728);
  lua_pushinteger(L, 134217728);
  lua_setfield(L, enum_table, "ALT");
  lua_pushstring(L, "MODIFIER_MASK");
  lua_rawseti(L, enum_table, -33554432);
  lua_pushinteger(L, -33554432);
  lua_setfield(L, enum_table, "MODIFIER_MASK");
  lua_pushstring(L, "UNICODE_ACCEL");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "UNICODE_ACCEL");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_Modifier_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::Modifier");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_Modifier_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::Modifier>*) + sizeof(QFlags<Qt::Modifier>));
  QFlags<Qt::Modifier> *fl = static_cast<QFlags<Qt::Modifier>*>( static_cast<void*>(&static_cast<QFlags<Qt::Modifier>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::Modifier>(lqtL_toenum(L, i, "Qt::Modifier"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::Modifier>*")) {
		lua_pushstring(L, "QFlags<Qt::Modifier>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_KeyboardModifier (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "NoModifier");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "NoModifier");
  lua_pushstring(L, "ShiftModifier");
  lua_rawseti(L, enum_table, 33554432);
  lua_pushinteger(L, 33554432);
  lua_setfield(L, enum_table, "ShiftModifier");
  lua_pushstring(L, "ControlModifier");
  lua_rawseti(L, enum_table, 67108864);
  lua_pushinteger(L, 67108864);
  lua_setfield(L, enum_table, "ControlModifier");
  lua_pushstring(L, "AltModifier");
  lua_rawseti(L, enum_table, 134217728);
  lua_pushinteger(L, 134217728);
  lua_setfield(L, enum_table, "AltModifier");
  lua_pushstring(L, "MetaModifier");
  lua_rawseti(L, enum_table, 268435456);
  lua_pushinteger(L, 268435456);
  lua_setfield(L, enum_table, "MetaModifier");
  lua_pushstring(L, "KeypadModifier");
  lua_rawseti(L, enum_table, 536870912);
  lua_pushinteger(L, 536870912);
  lua_setfield(L, enum_table, "KeypadModifier");
  lua_pushstring(L, "GroupSwitchModifier");
  lua_rawseti(L, enum_table, 1073741824);
  lua_pushinteger(L, 1073741824);
  lua_setfield(L, enum_table, "GroupSwitchModifier");
  lua_pushstring(L, "KeyboardModifierMask");
  lua_rawseti(L, enum_table, -33554432);
  lua_pushinteger(L, -33554432);
  lua_setfield(L, enum_table, "KeyboardModifierMask");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_KeyboardModifier_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::KeyboardModifier");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_KeyboardModifier_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::KeyboardModifier>*) + sizeof(QFlags<Qt::KeyboardModifier>));
  QFlags<Qt::KeyboardModifier> *fl = static_cast<QFlags<Qt::KeyboardModifier>*>( static_cast<void*>(&static_cast<QFlags<Qt::KeyboardModifier>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::KeyboardModifier>(lqtL_toenum(L, i, "Qt::KeyboardModifier"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::KeyboardModifier>*")) {
		lua_pushstring(L, "QFlags<Qt::KeyboardModifier>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinderQt::lqt_pushenum_GlobalColor (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "color0");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "color0");
  lua_pushstring(L, "color1");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "color1");
  lua_pushstring(L, "black");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "black");
  lua_pushstring(L, "white");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "white");
  lua_pushstring(L, "darkGray");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "darkGray");
  lua_pushstring(L, "gray");
  lua_rawseti(L, enum_table, 5);
  lua_pushinteger(L, 5);
  lua_setfield(L, enum_table, "gray");
  lua_pushstring(L, "lightGray");
  lua_rawseti(L, enum_table, 6);
  lua_pushinteger(L, 6);
  lua_setfield(L, enum_table, "lightGray");
  lua_pushstring(L, "red");
  lua_rawseti(L, enum_table, 7);
  lua_pushinteger(L, 7);
  lua_setfield(L, enum_table, "red");
  lua_pushstring(L, "green");
  lua_rawseti(L, enum_table, 8);
  lua_pushinteger(L, 8);
  lua_setfield(L, enum_table, "green");
  lua_pushstring(L, "blue");
  lua_rawseti(L, enum_table, 9);
  lua_pushinteger(L, 9);
  lua_setfield(L, enum_table, "blue");
  lua_pushstring(L, "cyan");
  lua_rawseti(L, enum_table, 10);
  lua_pushinteger(L, 10);
  lua_setfield(L, enum_table, "cyan");
  lua_pushstring(L, "magenta");
  lua_rawseti(L, enum_table, 11);
  lua_pushinteger(L, 11);
  lua_setfield(L, enum_table, "magenta");
  lua_pushstring(L, "yellow");
  lua_rawseti(L, enum_table, 12);
  lua_pushinteger(L, 12);
  lua_setfield(L, enum_table, "yellow");
  lua_pushstring(L, "darkRed");
  lua_rawseti(L, enum_table, 13);
  lua_pushinteger(L, 13);
  lua_setfield(L, enum_table, "darkRed");
  lua_pushstring(L, "darkGreen");
  lua_rawseti(L, enum_table, 14);
  lua_pushinteger(L, 14);
  lua_setfield(L, enum_table, "darkGreen");
  lua_pushstring(L, "darkBlue");
  lua_rawseti(L, enum_table, 15);
  lua_pushinteger(L, 15);
  lua_setfield(L, enum_table, "darkBlue");
  lua_pushstring(L, "darkCyan");
  lua_rawseti(L, enum_table, 16);
  lua_pushinteger(L, 16);
  lua_setfield(L, enum_table, "darkCyan");
  lua_pushstring(L, "darkMagenta");
  lua_rawseti(L, enum_table, 17);
  lua_pushinteger(L, 17);
  lua_setfield(L, enum_table, "darkMagenta");
  lua_pushstring(L, "darkYellow");
  lua_rawseti(L, enum_table, 18);
  lua_pushinteger(L, 18);
  lua_setfield(L, enum_table, "darkYellow");
  lua_pushstring(L, "transparent");
  lua_rawseti(L, enum_table, 19);
  lua_pushinteger(L, 19);
  lua_setfield(L, enum_table, "transparent");
  lua_pushcfunction(L, LuaBinderQt::lqt_pushenum_GlobalColor_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "Qt::GlobalColor");
  lua_remove(L, -2);
  return 1;
}
int LuaBinderQt::lqt_pushenum_GlobalColor_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<Qt::GlobalColor>*) + sizeof(QFlags<Qt::GlobalColor>));
  QFlags<Qt::GlobalColor> *fl = static_cast<QFlags<Qt::GlobalColor>*>( static_cast<void*>(&static_cast<QFlags<Qt::GlobalColor>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<Qt::GlobalColor>(lqtL_toenum(L, i, "Qt::GlobalColor"));
	}
	if (luaL_newmetatable(L, "QFlags<Qt::GlobalColor>*")) {
		lua_pushstring(L, "QFlags<Qt::GlobalColor>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int luaopen_Qt (lua_State *L) {
  if (luaL_newmetatable(L, "Qt*")) {
    LuaBinderQt::lqt_pushenum_WhiteSpaceMode(L);
    lua_setfield(L, -2, "WhiteSpaceMode");
    LuaBinderQt::lqt_pushenum_HitTestAccuracy(L);
    lua_setfield(L, -2, "HitTestAccuracy");
    LuaBinderQt::lqt_pushenum_EventPriority(L);
    lua_setfield(L, -2, "EventPriority");
    LuaBinderQt::lqt_pushenum_TextInteractionFlag(L);
    lua_setfield(L, -2, "TextInteractionFlag");
    LuaBinderQt::lqt_pushenum_WindowModality(L);
    lua_setfield(L, -2, "WindowModality");
    LuaBinderQt::lqt_pushenum_MatchFlag(L);
    lua_setfield(L, -2, "MatchFlag");
    LuaBinderQt::lqt_pushenum_ItemFlag(L);
    lua_setfield(L, -2, "ItemFlag");
    LuaBinderQt::lqt_pushenum_ItemDataRole(L);
    lua_setfield(L, -2, "ItemDataRole");
    LuaBinderQt::lqt_pushenum_CheckState(L);
    lua_setfield(L, -2, "CheckState");
    LuaBinderQt::lqt_pushenum_DropAction(L);
    lua_setfield(L, -2, "DropAction");
    LuaBinderQt::lqt_pushenum_LayoutDirection(L);
    lua_setfield(L, -2, "LayoutDirection");
    LuaBinderQt::lqt_pushenum_ToolButtonStyle(L);
    lua_setfield(L, -2, "ToolButtonStyle");
    LuaBinderQt::lqt_pushenum_InputMethodQuery(L);
    lua_setfield(L, -2, "InputMethodQuery");
    LuaBinderQt::lqt_pushenum_ContextMenuPolicy(L);
    lua_setfield(L, -2, "ContextMenuPolicy");
    LuaBinderQt::lqt_pushenum_FocusReason(L);
    lua_setfield(L, -2, "FocusReason");
    LuaBinderQt::lqt_pushenum_Axis(L);
    lua_setfield(L, -2, "Axis");
    LuaBinderQt::lqt_pushenum_TransformationMode(L);
    lua_setfield(L, -2, "TransformationMode");
    LuaBinderQt::lqt_pushenum_ItemSelectionMode(L);
    lua_setfield(L, -2, "ItemSelectionMode");
    LuaBinderQt::lqt_pushenum_ClipOperation(L);
    lua_setfield(L, -2, "ClipOperation");
    LuaBinderQt::lqt_pushenum_MaskMode(L);
    lua_setfield(L, -2, "MaskMode");
    LuaBinderQt::lqt_pushenum_FillRule(L);
    lua_setfield(L, -2, "FillRule");
    LuaBinderQt::lqt_pushenum_ShortcutContext(L);
    lua_setfield(L, -2, "ShortcutContext");
    LuaBinderQt::lqt_pushenum_ConnectionType(L);
    lua_setfield(L, -2, "ConnectionType");
    LuaBinderQt::lqt_pushenum_Corner(L);
    lua_setfield(L, -2, "Corner");
    LuaBinderQt::lqt_pushenum_CaseSensitivity(L);
    lua_setfield(L, -2, "CaseSensitivity");
    LuaBinderQt::lqt_pushenum_ScrollBarPolicy(L);
    lua_setfield(L, -2, "ScrollBarPolicy");
    LuaBinderQt::lqt_pushenum_DayOfWeek(L);
    lua_setfield(L, -2, "DayOfWeek");
    LuaBinderQt::lqt_pushenum_TimeSpec(L);
    lua_setfield(L, -2, "TimeSpec");
    LuaBinderQt::lqt_pushenum_DateFormat(L);
    lua_setfield(L, -2, "DateFormat");
    LuaBinderQt::lqt_pushenum_ToolBarAreaSizes(L);
    lua_setfield(L, -2, "ToolBarAreaSizes");
    LuaBinderQt::lqt_pushenum_ToolBarArea(L);
    lua_setfield(L, -2, "ToolBarArea");
    LuaBinderQt::lqt_pushenum_DockWidgetAreaSizes(L);
    lua_setfield(L, -2, "DockWidgetAreaSizes");
    LuaBinderQt::lqt_pushenum_DockWidgetArea(L);
    lua_setfield(L, -2, "DockWidgetArea");
    LuaBinderQt::lqt_pushenum_AnchorAttribute(L);
    lua_setfield(L, -2, "AnchorAttribute");
    LuaBinderQt::lqt_pushenum_AspectRatioMode(L);
    lua_setfield(L, -2, "AspectRatioMode");
    LuaBinderQt::lqt_pushenum_TextFormat(L);
    lua_setfield(L, -2, "TextFormat");
    LuaBinderQt::lqt_pushenum_CursorShape(L);
    lua_setfield(L, -2, "CursorShape");
    LuaBinderQt::lqt_pushenum_UIEffect(L);
    lua_setfield(L, -2, "UIEffect");
    LuaBinderQt::lqt_pushenum_BrushStyle(L);
    lua_setfield(L, -2, "BrushStyle");
    LuaBinderQt::lqt_pushenum_PenJoinStyle(L);
    lua_setfield(L, -2, "PenJoinStyle");
    LuaBinderQt::lqt_pushenum_PenCapStyle(L);
    lua_setfield(L, -2, "PenCapStyle");
    LuaBinderQt::lqt_pushenum_PenStyle(L);
    lua_setfield(L, -2, "PenStyle");
    LuaBinderQt::lqt_pushenum_ArrowType(L);
    lua_setfield(L, -2, "ArrowType");
    LuaBinderQt::lqt_pushenum_Key(L);
    lua_setfield(L, -2, "Key");
    LuaBinderQt::lqt_pushenum_BGMode(L);
    lua_setfield(L, -2, "BGMode");
    LuaBinderQt::lqt_pushenum_ImageConversionFlag(L);
    lua_setfield(L, -2, "ImageConversionFlag");
    LuaBinderQt::lqt_pushenum_ApplicationAttribute(L);
    lua_setfield(L, -2, "ApplicationAttribute");
    LuaBinderQt::lqt_pushenum_WidgetAttribute(L);
    lua_setfield(L, -2, "WidgetAttribute");
    LuaBinderQt::lqt_pushenum_WindowState(L);
    lua_setfield(L, -2, "WindowState");
    LuaBinderQt::lqt_pushenum_WindowType(L);
    lua_setfield(L, -2, "WindowType");
    LuaBinderQt::lqt_pushenum_TextElideMode(L);
    lua_setfield(L, -2, "TextElideMode");
    LuaBinderQt::lqt_pushenum_TextFlag(L);
    lua_setfield(L, -2, "TextFlag");
    LuaBinderQt::lqt_pushenum_AlignmentFlag(L);
    lua_setfield(L, -2, "AlignmentFlag");
    LuaBinderQt::lqt_pushenum_SortOrder(L);
    lua_setfield(L, -2, "SortOrder");
    LuaBinderQt::lqt_pushenum_FocusPolicy(L);
    lua_setfield(L, -2, "FocusPolicy");
    LuaBinderQt::lqt_pushenum_Orientation(L);
    lua_setfield(L, -2, "Orientation");
    LuaBinderQt::lqt_pushenum_MouseButton(L);
    lua_setfield(L, -2, "MouseButton");
    LuaBinderQt::lqt_pushenum_Modifier(L);
    lua_setfield(L, -2, "Modifier");
    LuaBinderQt::lqt_pushenum_KeyboardModifier(L);
    lua_setfield(L, -2, "KeyboardModifier");
    LuaBinderQt::lqt_pushenum_GlobalColor(L);
    lua_setfield(L, -2, "GlobalColor");
    lua_newtable(L);
    lua_setfield(L, -2, "__base");
    lua_pushcfunction(L, lqtL_newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, lqtL_index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, lqtL_gc);
    lua_setfield(L, -2, "__gc");
    lua_pushstring(L, "Qt");
    lua_setfield(L, -2, "__qtype");
		lua_setglobal(L, "Qt");
  } else {
		lua_pop(L, 1);
	}
  return 0;
}
