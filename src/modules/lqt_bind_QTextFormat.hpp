#include "lqt_common.hpp"
#include <QTextFormat>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QTextFormat > : public QTextFormat {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__colorProperty (lua_State *L);
  static int __LuaWrapCall__clearBackground (lua_State *L);
  static int __LuaWrapCall__lengthVectorProperty (lua_State *L);
  static int __LuaWrapCall__setForeground (lua_State *L);
  static int __LuaWrapCall__background (lua_State *L);
  static int __LuaWrapCall__setProperty__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__setProperty__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__setProperty (lua_State *L);
  static int __LuaWrapCall__isValid (lua_State *L);
  static int __LuaWrapCall__isImageFormat (lua_State *L);
  static int __LuaWrapCall__toListFormat (lua_State *L);
  static int __LuaWrapCall__lengthProperty (lua_State *L);
  static int __LuaWrapCall__setObjectType (lua_State *L);
  static int __LuaWrapCall__merge (lua_State *L);
  static int __LuaWrapCall__delete (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__new (lua_State *L);
  static int __LuaWrapCall__toBlockFormat (lua_State *L);
  static int __LuaWrapCall__toCharFormat (lua_State *L);
  static int __LuaWrapCall__boolProperty (lua_State *L);
  static int __LuaWrapCall__isFrameFormat (lua_State *L);
  static int __LuaWrapCall__isBlockFormat (lua_State *L);
  static int __LuaWrapCall__toTableFormat (lua_State *L);
  static int __LuaWrapCall__penProperty (lua_State *L);
  static int __LuaWrapCall__stringProperty (lua_State *L);
  static int __LuaWrapCall__isListFormat (lua_State *L);
  static int __LuaWrapCall__setLayoutDirection (lua_State *L);
  static int __LuaWrapCall__layoutDirection (lua_State *L);
  static int __LuaWrapCall__property (lua_State *L);
  static int __LuaWrapCall__properties (lua_State *L);
  static int __LuaWrapCall__hasProperty (lua_State *L);
  static int __LuaWrapCall__propertyCount (lua_State *L);
  static int __LuaWrapCall__type (lua_State *L);
  static int __LuaWrapCall__clearProperty (lua_State *L);
  static int __LuaWrapCall__setBackground (lua_State *L);
  static int __LuaWrapCall__setObjectIndex (lua_State *L);
  static int __LuaWrapCall__isCharFormat (lua_State *L);
  static int __LuaWrapCall__intProperty (lua_State *L);
  static int __LuaWrapCall__isTableFormat (lua_State *L);
  static int __LuaWrapCall__toImageFormat (lua_State *L);
  static int __LuaWrapCall__toFrameFormat (lua_State *L);
  static int __LuaWrapCall__brushProperty (lua_State *L);
  static int __LuaWrapCall__clearForeground (lua_State *L);
  static int __LuaWrapCall__foreground (lua_State *L);
  static int __LuaWrapCall__objectIndex (lua_State *L);
  static int __LuaWrapCall__objectType (lua_State *L);
  static int __LuaWrapCall__doubleProperty (lua_State *L);
  static int lqt_pushenum_FormatType (lua_State *L);
  static int lqt_pushenum_FormatType_QFLAGS_CREATOR (lua_State *L);
  static int lqt_pushenum_Property (lua_State *L);
  static int lqt_pushenum_Property_QFLAGS_CREATOR (lua_State *L);
  static int lqt_pushenum_ObjectTypes (lua_State *L);
  static int lqt_pushenum_ObjectTypes_QFLAGS_CREATOR (lua_State *L);
  static int lqt_pushenum_PageBreakFlag (lua_State *L);
  static int lqt_pushenum_PageBreakFlag_QFLAGS_CREATOR (lua_State *L);
  LuaBinder< QTextFormat > (lua_State *l):QTextFormat(), L(l) {}
  LuaBinder< QTextFormat > (lua_State *l, int arg1):QTextFormat(arg1), L(l) {}
  LuaBinder< QTextFormat > (lua_State *l, const QTextFormat& arg1):QTextFormat(arg1), L(l) {}
};

extern "C" int luaopen_QTextFormat (lua_State *L);
