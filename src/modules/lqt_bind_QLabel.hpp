#include "lqt_common.hpp"
#include <QLabel>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QLabel > : public QLabel {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__sizeHint (lua_State *L);
  static int __LuaWrapCall__setBuddy (lua_State *L);
  static int __LuaWrapCall__setNum__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__setNum__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__setNum (lua_State *L);
  static int __LuaWrapCall__buddy (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__trUtf8 (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__tr (lua_State *L);
  static int __LuaWrapCall__textFormat (lua_State *L);
  static int __LuaWrapCall__picture (lua_State *L);
  static int __LuaWrapCall__setPicture (lua_State *L);
  static int __LuaWrapCall__text (lua_State *L);
  static int __LuaWrapCall__setAlignment (lua_State *L);
  static int __LuaWrapCall__hasScaledContents (lua_State *L);
  static int __LuaWrapCall__setIndent (lua_State *L);
  static int __LuaWrapCall__setOpenExternalLinks (lua_State *L);
  static int __LuaWrapCall__movie (lua_State *L);
  static int __LuaWrapCall__setMargin (lua_State *L);
  static int __LuaWrapCall__setText (lua_State *L);
  static int __LuaWrapCall__minimumSizeHint (lua_State *L);
  static int __LuaWrapCall__openExternalLinks (lua_State *L);
  static int __LuaWrapCall__heightForWidth (lua_State *L);
  static int __LuaWrapCall__setWordWrap (lua_State *L);
  static int __LuaWrapCall__margin (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__new (lua_State *L);
  static int __LuaWrapCall__clear (lua_State *L);
  static int __LuaWrapCall__delete (lua_State *L);
  static int __LuaWrapCall__setTextFormat (lua_State *L);
  static int __LuaWrapCall__wordWrap (lua_State *L);
  static int __LuaWrapCall__setTextInteractionFlags (lua_State *L);
  static int __LuaWrapCall__pixmap (lua_State *L);
  static int __LuaWrapCall__indent (lua_State *L);
  static int __LuaWrapCall__alignment (lua_State *L);
  static int __LuaWrapCall__setScaledContents (lua_State *L);
  static int __LuaWrapCall__textInteractionFlags (lua_State *L);
  static int __LuaWrapCall__setPixmap (lua_State *L);
  static int __LuaWrapCall__metaObject (lua_State *L);
  static int __LuaWrapCall__setMovie (lua_State *L);
protected:
  void styleChange (QStyle& arg1);
public:
protected:
  void mouseReleaseEvent (QMouseEvent * arg1);
public:
protected:
  void keyPressEvent (QKeyEvent * arg1);
public:
  const QMetaObject * metaObject () const;
  int devType () const;
  void setVisible (bool arg1);
protected:
  bool focusNextPrevChild (bool arg1);
public:
  QPaintEngine * paintEngine () const;
protected:
  void dragLeaveEvent (QDragLeaveEvent * arg1);
public:
protected:
  void mousePressEvent (QMouseEvent * arg1);
public:
protected:
  void dragEnterEvent (QDragEnterEvent * arg1);
public:
protected:
  void changeEvent (QEvent * arg1);
public:
protected:
  void tabletEvent (QTabletEvent * arg1);
public:
protected:
  void enterEvent (QEvent * arg1);
public:
protected:
  void dropEvent (QDropEvent * arg1);
public:
  QSize minimumSizeHint () const;
  QSize sizeHint () const;
protected:
  void moveEvent (QMoveEvent * arg1);
public:
protected:
  void showEvent (QShowEvent * arg1);
public:
  QVariant inputMethodQuery (Qt::InputMethodQuery arg1) const;
protected:
  void paintEvent (QPaintEvent * arg1);
public:
protected:
  void contextMenuEvent (QContextMenuEvent * arg1);
public:
protected:
  void fontChange (const QFont& arg1);
public:
protected:
  void enabledChange (bool arg1);
public:
protected:
  void focusInEvent (QFocusEvent * arg1);
public:
  int heightForWidth (int arg1) const;
protected:
  void mouseDoubleClickEvent (QMouseEvent * arg1);
public:
protected:
  void actionEvent (QActionEvent * arg1);
public:
protected:
  void disconnectNotify (const char * arg1);
public:
protected:
  void mouseMoveEvent (QMouseEvent * arg1);
public:
protected:
  void timerEvent (QTimerEvent * arg1);
public:
protected:
  void childEvent (QChildEvent * arg1);
public:
protected:
  void focusOutEvent (QFocusEvent * arg1);
public:
protected:
  void connectNotify (const char * arg1);
public:
protected:
  int metric (QPaintDevice::PaintDeviceMetric arg1) const;
public:
  bool eventFilter (QObject * arg1, QEvent * arg2);
protected:
  void inputMethodEvent (QInputMethodEvent * arg1);
public:
protected:
  void wheelEvent (QWheelEvent * arg1);
public:
protected:
  void resizeEvent (QResizeEvent * arg1);
public:
protected:
  void paletteChange (const QPalette& arg1);
public:
protected:
  void languageChange ();
public:
protected:
  bool event (QEvent * arg1);
public:
protected:
  void hideEvent (QHideEvent * arg1);
public:
protected:
  void keyReleaseEvent (QKeyEvent * arg1);
public:
protected:
  void windowActivationChange (bool arg1);
public:
protected:
  void dragMoveEvent (QDragMoveEvent * arg1);
public:
protected:
  void leaveEvent (QEvent * arg1);
public:
protected:
  void closeEvent (QCloseEvent * arg1);
public:
protected:
  void customEvent (QEvent * arg1);
public:
  ~LuaBinder< QLabel > ();
  LuaBinder< QLabel > (lua_State *l, QWidget * arg1, QFlags<Qt::WindowType> arg2):QLabel(arg1, arg2), L(l) {}
  LuaBinder< QLabel > (lua_State *l, const QString& arg1, QWidget * arg2, QFlags<Qt::WindowType> arg3):QLabel(arg1, arg2, arg3), L(l) {}
};

extern "C" int luaopen_QLabel (lua_State *L);
