#include "lqt_bind_QGraphicsItem.hpp"

int LuaBinder< QGraphicsItem >::__LuaWrapCall__scenePos (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QPointF ret = __lua__obj->QGraphicsItem::scenePos();
  lqtL_passudata(L, new QPointF(ret), "QPointF*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__setCursor (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QCursor& arg1 = **static_cast<QCursor**>(lqtL_checkudata(L, 2, "QCursor*"));
  __lua__obj->QGraphicsItem::setCursor(arg1);
  return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__setPos__OverloadedVersion__1 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPointF& arg1 = **static_cast<QPointF**>(lqtL_checkudata(L, 2, "QPointF*"));
  __lua__obj->QGraphicsItem::setPos(arg1);
  return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__setPos__OverloadedVersion__2 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  double arg2 = lua_tonumber(L, 3);
  __lua__obj->QGraphicsItem::setPos(arg1, arg2);
  return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__setPos (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPointF*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1278560;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1278f80;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1279490;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__setPos__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__setPos__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__setPos matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__isVisible (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QGraphicsItem::isVisible();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__setAcceptsHoverEvents (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QGraphicsItem::setAcceptsHoverEvents(arg1);
  return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__hasCursor (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QGraphicsItem::hasCursor();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__topLevelItem (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QGraphicsItem * ret = __lua__obj->QGraphicsItem::topLevelItem();
  lqtL_pushudata(L, ret, "QGraphicsItem*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__y (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double ret = __lua__obj->QGraphicsItem::y();
  lua_pushnumber(L, ret);
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__x (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double ret = __lua__obj->QGraphicsItem::x();
  lua_pushnumber(L, ret);
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__setAcceptDrops (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QGraphicsItem::setAcceptDrops(arg1);
  return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__clearFocus (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QGraphicsItem::clearFocus();
  return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__deviceTransform (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QTransform& arg1 = **static_cast<QTransform**>(lqtL_checkudata(L, 2, "QTransform*"));
  QTransform ret = __lua__obj->QGraphicsItem::deviceTransform(arg1);
  lqtL_passudata(L, new QTransform(ret), "QTransform*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__shape (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QPainterPath ret = __lua__obj->QGraphicsItem::shape();
  lqtL_passudata(L, new QPainterPath(ret), "QPainterPath*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__pos (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QPointF ret = __lua__obj->QGraphicsItem::pos();
  lqtL_passudata(L, new QPointF(ret), "QPointF*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__moveBy (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  double arg2 = lua_tonumber(L, 3);
  __lua__obj->QGraphicsItem::moveBy(arg1, arg2);
  return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__scale (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  double arg2 = lua_tonumber(L, 3);
  __lua__obj->QGraphicsItem::scale(arg1, arg2);
  return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__isSelected (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QGraphicsItem::isSelected();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__setParentItem (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QGraphicsItem * arg1 = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 2, "QGraphicsItem*"));
  __lua__obj->QGraphicsItem::setParentItem(arg1);
  return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__hasFocus (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QGraphicsItem::hasFocus();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__setFocus (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::FocusReason arg1 = lqtL_isenum(L, 2, "Qt::FocusReason")?static_cast<Qt::FocusReason>(lqtL_toenum(L, 2, "Qt::FocusReason")):Qt::OtherFocusReason;
  __lua__obj->QGraphicsItem::setFocus(arg1);
  return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__setFlag (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QGraphicsItem::GraphicsItemFlag arg1 = static_cast<QGraphicsItem::GraphicsItemFlag>(lqtL_toenum(L, 2, "QGraphicsItem::GraphicsItemFlag"));
  bool arg2 = lua_isboolean(L, 3)?(bool)lua_toboolean(L, 3):true;
  __lua__obj->QGraphicsItem::setFlag(arg1, arg2);
  return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__translate (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  double arg2 = lua_tonumber(L, 3);
  __lua__obj->QGraphicsItem::translate(arg1, arg2);
  return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__acceptedMouseButtons (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<Qt::MouseButton> ret = __lua__obj->QGraphicsItem::acceptedMouseButtons();
  lqtL_passudata(L, new QFlags<Qt::MouseButton>(ret), "QFlags<Qt::MouseButton>*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__opaqueArea (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QPainterPath ret = __lua__obj->QGraphicsItem::opaqueArea();
  lqtL_passudata(L, new QPainterPath(ret), "QPainterPath*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__sceneTransform (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTransform ret = __lua__obj->QGraphicsItem::sceneTransform();
  lqtL_passudata(L, new QTransform(ret), "QTransform*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__resetTransform (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QGraphicsItem::resetTransform();
  return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__setToolTip (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QGraphicsItem::setToolTip(arg1);
  return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__parentItem (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QGraphicsItem * ret = __lua__obj->QGraphicsItem::parentItem();
  lqtL_pushudata(L, ret, "QGraphicsItem*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__collidesWithItem (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QGraphicsItem * arg1 = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 2, "QGraphicsItem*"));
  Qt::ItemSelectionMode arg2 = lqtL_isenum(L, 3, "Qt::ItemSelectionMode")?static_cast<Qt::ItemSelectionMode>(lqtL_toenum(L, 3, "Qt::ItemSelectionMode")):Qt::IntersectsItemShape;
  bool ret = __lua__obj->QGraphicsItem::collidesWithItem(arg1, arg2);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__show (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QGraphicsItem::show();
  return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__isEnabled (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QGraphicsItem::isEnabled();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__setFlags (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<QGraphicsItem::GraphicsItemFlag> arg1 = **static_cast<QFlags<QGraphicsItem::GraphicsItemFlag>**>(lqtL_checkudata(L, 2, "QFlags<QGraphicsItem::GraphicsItemFlag>*"));
  __lua__obj->QGraphicsItem::setFlags(arg1);
  return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__setGroup (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QGraphicsItemGroup * arg1 = *static_cast<QGraphicsItemGroup**>(lqtL_checkudata(L, 2, "QGraphicsItemGroup*"));
  __lua__obj->QGraphicsItem::setGroup(arg1);
  return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__transform (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTransform ret = __lua__obj->QGraphicsItem::transform();
  lqtL_passudata(L, new QTransform(ret), "QTransform*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapToItem__OverloadedVersion__1 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QGraphicsItem * arg1 = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 2, "QGraphicsItem*"));
  const QPointF& arg2 = **static_cast<QPointF**>(lqtL_checkudata(L, 3, "QPointF*"));
  QPointF ret = __lua__obj->QGraphicsItem::mapToItem(arg1, arg2);
  lqtL_passudata(L, new QPointF(ret), "QPointF*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapToItem__OverloadedVersion__2 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QGraphicsItem * arg1 = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 2, "QGraphicsItem*"));
  const QRectF& arg2 = **static_cast<QRectF**>(lqtL_checkudata(L, 3, "QRectF*"));
  QPolygonF ret = __lua__obj->QGraphicsItem::mapToItem(arg1, arg2);
  lqtL_passudata(L, new QPolygonF(ret), "QPolygonF*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapToItem__OverloadedVersion__3 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QGraphicsItem * arg1 = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 2, "QGraphicsItem*"));
  const QPolygonF& arg2 = **static_cast<QPolygonF**>(lqtL_checkudata(L, 3, "QPolygonF*"));
  QPolygonF ret = __lua__obj->QGraphicsItem::mapToItem(arg1, arg2);
  lqtL_passudata(L, new QPolygonF(ret), "QPolygonF*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapToItem__OverloadedVersion__4 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QGraphicsItem * arg1 = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 2, "QGraphicsItem*"));
  const QPainterPath& arg2 = **static_cast<QPainterPath**>(lqtL_checkudata(L, 3, "QPainterPath*"));
  QPainterPath ret = __lua__obj->QGraphicsItem::mapToItem(arg1, arg2);
  lqtL_passudata(L, new QPainterPath(ret), "QPainterPath*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapToItem__OverloadedVersion__5 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QGraphicsItem * arg1 = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 2, "QGraphicsItem*"));
  double arg2 = lua_tonumber(L, 3);
  double arg3 = lua_tonumber(L, 4);
  QPointF ret = __lua__obj->QGraphicsItem::mapToItem(arg1, arg2, arg3);
  lqtL_passudata(L, new QPointF(ret), "QPointF*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapToItem__OverloadedVersion__6 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QGraphicsItem * arg1 = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 2, "QGraphicsItem*"));
  double arg2 = lua_tonumber(L, 3);
  double arg3 = lua_tonumber(L, 4);
  double arg4 = lua_tonumber(L, 5);
  double arg5 = lua_tonumber(L, 6);
  QPolygonF ret = __lua__obj->QGraphicsItem::mapToItem(arg1, arg2, arg3, arg4, arg5);
  lqtL_passudata(L, new QPolygonF(ret), "QPolygonF*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapToItem (lua_State *L) {
  int score[7];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QGraphicsItem*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1293ba0;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_testudata(L, 3, "QPointF*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1293830;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QGraphicsItem*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1295f10;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_testudata(L, 3, "QRectF*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x12959d0;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  score[3] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QGraphicsItem*")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x12981f0;
  } else {
    score[3] -= premium*premium;
  }
  if (lqtL_testudata(L, 3, "QPolygonF*")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1297cb0;
  } else {
    score[3] -= premium*premium;
  }
  score[4] = 0;
  score[4] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QGraphicsItem*")) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x129a510;
  } else {
    score[4] -= premium*premium;
  }
  if (lqtL_testudata(L, 3, "QPainterPath*")) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x1299fa0;
  } else {
    score[4] -= premium*premium;
  }
  score[5] = 0;
  score[5] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QGraphicsItem*")) {
    score[5] += premium;
  } else if (false) {
    score[5] += premium-1; // table: 0x12a5650;
  } else {
    score[5] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[5] += premium;
  } else if (false) {
    score[5] += premium-1; // table: 0x12a5b60;
  } else {
    score[5] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[5] += premium;
  } else if (false) {
    score[5] += premium-1; // table: 0x12a5f10;
  } else {
    score[5] -= premium*premium;
  }
  score[6] = 0;
  score[6] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QGraphicsItem*")) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x12a8590;
  } else {
    score[6] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x12a8aa0;
  } else {
    score[6] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x12a8e50;
  } else {
    score[6] -= premium*premium;
  }
  if (lua_isnumber(L, 5)) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x12a8df0;
  } else {
    score[6] -= premium*premium;
  }
  if (lua_isnumber(L, 6)) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x12a9600;
  } else {
    score[6] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=6;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__mapToItem__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__mapToItem__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__mapToItem__OverloadedVersion__3(L); break;
    case 4: return __LuaWrapCall__mapToItem__OverloadedVersion__4(L); break;
    case 5: return __LuaWrapCall__mapToItem__OverloadedVersion__5(L); break;
    case 6: return __LuaWrapCall__mapToItem__OverloadedVersion__6(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__mapToItem matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__ensureVisible__OverloadedVersion__1 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QRectF& arg1 = lqtL_testudata(L, 2, "QRectF*")?**static_cast<QRectF**>(lqtL_checkudata(L, 2, "QRectF*")):QRectF();
  int arg2 = lua_isnumber(L, 3)?lua_tointeger(L, 3):static_cast< int >(50);
  int arg3 = lua_isnumber(L, 4)?lua_tointeger(L, 4):static_cast< int >(50);
  __lua__obj->QGraphicsItem::ensureVisible(arg1, arg2, arg3);
  return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__ensureVisible__OverloadedVersion__2 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  double arg2 = lua_tonumber(L, 3);
  double arg3 = lua_tonumber(L, 4);
  double arg4 = lua_tonumber(L, 5);
  int arg5 = lua_isnumber(L, 6)?lua_tointeger(L, 6):static_cast< int >(50);
  int arg6 = lua_isnumber(L, 7)?lua_tointeger(L, 7):static_cast< int >(50);
  __lua__obj->QGraphicsItem::ensureVisible(arg1, arg2, arg3, arg4, arg5, arg6);
  return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__ensureVisible (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QRectF*")) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x127ac70;
  } else {
    score[1] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x127a6f0;
  } else {
    score[1] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x127b130;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x127bea0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x127c3c0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x127c770;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 5)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x127c710;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 6)) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x127cf30;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 7)) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x127caf0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__ensureVisible__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__ensureVisible__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__ensureVisible matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__update__OverloadedVersion__1 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QRectF& arg1 = lqtL_testudata(L, 2, "QRectF*")?**static_cast<QRectF**>(lqtL_checkudata(L, 2, "QRectF*")):QRectF();
  __lua__obj->QGraphicsItem::update(arg1);
  return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__update__OverloadedVersion__2 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  double arg2 = lua_tonumber(L, 3);
  double arg3 = lua_tonumber(L, 4);
  double arg4 = lua_tonumber(L, 5);
  __lua__obj->QGraphicsItem::update(arg1, arg2, arg3, arg4);
  return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__update (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QRectF*")) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x1291af0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1292540;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1292a50;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x12929f0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 5)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1293240;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__update__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__update__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__update matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__isObscured__OverloadedVersion__1 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QGraphicsItem::isObscured();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__isObscured__OverloadedVersion__2 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QRectF& arg1 = **static_cast<QRectF**>(lqtL_checkudata(L, 2, "QRectF*"));
  bool ret = __lua__obj->QGraphicsItem::isObscured(arg1);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__isObscured__OverloadedVersion__3 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  double arg2 = lua_tonumber(L, 3);
  double arg3 = lua_tonumber(L, 4);
  double arg4 = lua_tonumber(L, 5);
  bool ret = __lua__obj->QGraphicsItem::isObscured(arg1, arg2, arg3, arg4);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__isObscured (lua_State *L) {
  int score[4];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QRectF*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x128d680;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  score[3] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x128e120;
  } else {
    score[3] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x128e650;
  } else {
    score[3] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x128ea00;
  } else {
    score[3] -= premium*premium;
  }
  if (lua_isnumber(L, 5)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x128e9a0;
  } else {
    score[3] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=3;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__isObscured__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__isObscured__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__isObscured__OverloadedVersion__3(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__isObscured matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__setAcceptedMouseButtons (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<Qt::MouseButton> arg1 = **static_cast<QFlags<Qt::MouseButton>**>(lqtL_checkudata(L, 2, "QFlags<Qt::MouseButton>*"));
  __lua__obj->QGraphicsItem::setAcceptedMouseButtons(arg1);
  return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__matrix (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QMatrix ret = __lua__obj->QGraphicsItem::matrix();
  lqtL_passudata(L, new QMatrix(ret), "QMatrix*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__advance (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QGraphicsItem::advance(arg1);
  return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__toolTip (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QGraphicsItem::toolTip();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__delete (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  delete __lua__obj;
  __lua__obj = 0;
  return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__sceneBoundingRect (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QRectF ret = __lua__obj->QGraphicsItem::sceneBoundingRect();
  lqtL_passudata(L, new QRectF(ret), "QRectF*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__flags (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<QGraphicsItem::GraphicsItemFlag> ret = __lua__obj->QGraphicsItem::flags();
  lqtL_passudata(L, new QFlags<QGraphicsItem::GraphicsItemFlag>(ret), "QFlags<QGraphicsItem::GraphicsItemFlag>*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__isAncestorOf (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QGraphicsItem * arg1 = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 2, "QGraphicsItem*"));
  bool ret = __lua__obj->QGraphicsItem::isAncestorOf(arg1);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__collidesWithPath (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPainterPath& arg1 = **static_cast<QPainterPath**>(lqtL_checkudata(L, 2, "QPainterPath*"));
  Qt::ItemSelectionMode arg2 = lqtL_isenum(L, 3, "Qt::ItemSelectionMode")?static_cast<Qt::ItemSelectionMode>(lqtL_toenum(L, 3, "Qt::ItemSelectionMode")):Qt::IntersectsItemShape;
  bool ret = __lua__obj->QGraphicsItem::collidesWithPath(arg1, arg2);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__new (lua_State *L) {
  QGraphicsItem * arg1 = lqtL_testudata(L, 1, "QGraphicsItem*")?*static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*")):static_cast< QGraphicsItem * >(0);
  QGraphicsScene * arg2 = lqtL_testudata(L, 2, "QGraphicsScene*")?*static_cast<QGraphicsScene**>(lqtL_checkudata(L, 2, "QGraphicsScene*")):static_cast< QGraphicsScene * >(0);
  QGraphicsItem * ret = new LuaBinder< QGraphicsItem >(L, arg1, arg2);
  lqtL_passudata(L, ret, "QGraphicsItem*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__scene (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QGraphicsScene * ret = __lua__obj->QGraphicsItem::scene();
  lqtL_pushudata(L, ret, "QGraphicsScene*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__rotate (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  __lua__obj->QGraphicsItem::rotate(arg1);
  return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__setSelected (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QGraphicsItem::setSelected(arg1);
  return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__hide (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QGraphicsItem::hide();
  return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__installSceneEventFilter (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QGraphicsItem * arg1 = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 2, "QGraphicsItem*"));
  __lua__obj->QGraphicsItem::installSceneEventFilter(arg1);
  return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__contains (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPointF& arg1 = **static_cast<QPointF**>(lqtL_checkudata(L, 2, "QPointF*"));
  bool ret = __lua__obj->QGraphicsItem::contains(arg1);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__sceneMatrix (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QMatrix ret = __lua__obj->QGraphicsItem::sceneMatrix();
  lqtL_passudata(L, new QMatrix(ret), "QMatrix*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__setTransform (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QTransform& arg1 = **static_cast<QTransform**>(lqtL_checkudata(L, 2, "QTransform*"));
  bool arg2 = lua_isboolean(L, 3)?(bool)lua_toboolean(L, 3):false;
  __lua__obj->QGraphicsItem::setTransform(arg1, arg2);
  return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__group (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QGraphicsItemGroup * ret = __lua__obj->QGraphicsItem::group();
  lqtL_pushudata(L, ret, "QGraphicsItemGroup*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapFromScene__OverloadedVersion__1 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPointF& arg1 = **static_cast<QPointF**>(lqtL_checkudata(L, 2, "QPointF*"));
  QPointF ret = __lua__obj->QGraphicsItem::mapFromScene(arg1);
  lqtL_passudata(L, new QPointF(ret), "QPointF*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapFromScene__OverloadedVersion__2 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QRectF& arg1 = **static_cast<QRectF**>(lqtL_checkudata(L, 2, "QRectF*"));
  QPolygonF ret = __lua__obj->QGraphicsItem::mapFromScene(arg1);
  lqtL_passudata(L, new QPolygonF(ret), "QPolygonF*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapFromScene__OverloadedVersion__3 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPolygonF& arg1 = **static_cast<QPolygonF**>(lqtL_checkudata(L, 2, "QPolygonF*"));
  QPolygonF ret = __lua__obj->QGraphicsItem::mapFromScene(arg1);
  lqtL_passudata(L, new QPolygonF(ret), "QPolygonF*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapFromScene__OverloadedVersion__4 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPainterPath& arg1 = **static_cast<QPainterPath**>(lqtL_checkudata(L, 2, "QPainterPath*"));
  QPainterPath ret = __lua__obj->QGraphicsItem::mapFromScene(arg1);
  lqtL_passudata(L, new QPainterPath(ret), "QPainterPath*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapFromScene__OverloadedVersion__5 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  double arg2 = lua_tonumber(L, 3);
  QPointF ret = __lua__obj->QGraphicsItem::mapFromScene(arg1, arg2);
  lqtL_passudata(L, new QPointF(ret), "QPointF*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapFromScene__OverloadedVersion__6 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  double arg2 = lua_tonumber(L, 3);
  double arg3 = lua_tonumber(L, 4);
  double arg4 = lua_tonumber(L, 5);
  QPolygonF ret = __lua__obj->QGraphicsItem::mapFromScene(arg1, arg2, arg3, arg4);
  lqtL_passudata(L, new QPolygonF(ret), "QPolygonF*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapFromScene (lua_State *L) {
  int score[7];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPointF*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x129e230;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QRectF*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x12a0530;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  score[3] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPolygonF*")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x12a2860;
  } else {
    score[3] -= premium*premium;
  }
  score[4] = 0;
  score[4] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPainterPath*")) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x12a4bd0;
  } else {
    score[4] -= premium*premium;
  }
  score[5] = 0;
  score[5] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[5] += premium;
  } else if (false) {
    score[5] += premium-1; // table: 0x12aec80;
  } else {
    score[5] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[5] += premium;
  } else if (false) {
    score[5] += premium-1; // table: 0x12af1a0;
  } else {
    score[5] -= premium*premium;
  }
  score[6] = 0;
  score[6] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x12b2af0;
  } else {
    score[6] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x12b3000;
  } else {
    score[6] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x12b33b0;
  } else {
    score[6] -= premium*premium;
  }
  if (lua_isnumber(L, 5)) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x12b3350;
  } else {
    score[6] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=6;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__mapFromScene__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__mapFromScene__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__mapFromScene__OverloadedVersion__3(L); break;
    case 4: return __LuaWrapCall__mapFromScene__OverloadedVersion__4(L); break;
    case 5: return __LuaWrapCall__mapFromScene__OverloadedVersion__5(L); break;
    case 6: return __LuaWrapCall__mapFromScene__OverloadedVersion__6(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__mapFromScene matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__acceptsHoverEvents (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QGraphicsItem::acceptsHoverEvents();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__setZValue (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  __lua__obj->QGraphicsItem::setZValue(arg1);
  return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__setMatrix (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QMatrix& arg1 = **static_cast<QMatrix**>(lqtL_checkudata(L, 2, "QMatrix*"));
  bool arg2 = lua_isboolean(L, 3)?(bool)lua_toboolean(L, 3):false;
  __lua__obj->QGraphicsItem::setMatrix(arg1, arg2);
  return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__setVisible (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QGraphicsItem::setVisible(arg1);
  return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__handlesChildEvents (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QGraphicsItem::handlesChildEvents();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapFromItem__OverloadedVersion__1 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QGraphicsItem * arg1 = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 2, "QGraphicsItem*"));
  const QPointF& arg2 = **static_cast<QPointF**>(lqtL_checkudata(L, 3, "QPointF*"));
  QPointF ret = __lua__obj->QGraphicsItem::mapFromItem(arg1, arg2);
  lqtL_passudata(L, new QPointF(ret), "QPointF*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapFromItem__OverloadedVersion__2 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QGraphicsItem * arg1 = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 2, "QGraphicsItem*"));
  const QRectF& arg2 = **static_cast<QRectF**>(lqtL_checkudata(L, 3, "QRectF*"));
  QPolygonF ret = __lua__obj->QGraphicsItem::mapFromItem(arg1, arg2);
  lqtL_passudata(L, new QPolygonF(ret), "QPolygonF*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapFromItem__OverloadedVersion__3 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QGraphicsItem * arg1 = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 2, "QGraphicsItem*"));
  const QPolygonF& arg2 = **static_cast<QPolygonF**>(lqtL_checkudata(L, 3, "QPolygonF*"));
  QPolygonF ret = __lua__obj->QGraphicsItem::mapFromItem(arg1, arg2);
  lqtL_passudata(L, new QPolygonF(ret), "QPolygonF*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapFromItem__OverloadedVersion__4 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QGraphicsItem * arg1 = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 2, "QGraphicsItem*"));
  const QPainterPath& arg2 = **static_cast<QPainterPath**>(lqtL_checkudata(L, 3, "QPainterPath*"));
  QPainterPath ret = __lua__obj->QGraphicsItem::mapFromItem(arg1, arg2);
  lqtL_passudata(L, new QPainterPath(ret), "QPainterPath*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapFromItem__OverloadedVersion__5 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QGraphicsItem * arg1 = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 2, "QGraphicsItem*"));
  double arg2 = lua_tonumber(L, 3);
  double arg3 = lua_tonumber(L, 4);
  QPointF ret = __lua__obj->QGraphicsItem::mapFromItem(arg1, arg2, arg3);
  lqtL_passudata(L, new QPointF(ret), "QPointF*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapFromItem__OverloadedVersion__6 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QGraphicsItem * arg1 = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 2, "QGraphicsItem*"));
  double arg2 = lua_tonumber(L, 3);
  double arg3 = lua_tonumber(L, 4);
  double arg4 = lua_tonumber(L, 5);
  double arg5 = lua_tonumber(L, 6);
  QPolygonF ret = __lua__obj->QGraphicsItem::mapFromItem(arg1, arg2, arg3, arg4, arg5);
  lqtL_passudata(L, new QPolygonF(ret), "QPolygonF*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapFromItem (lua_State *L) {
  int score[7];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QGraphicsItem*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x129c8f0;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_testudata(L, 3, "QPointF*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x129c310;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QGraphicsItem*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x129ec90;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_testudata(L, 3, "QRectF*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x129e740;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  score[3] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QGraphicsItem*")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x12a0fb0;
  } else {
    score[3] -= premium*premium;
  }
  if (lqtL_testudata(L, 3, "QPolygonF*")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x12a0a40;
  } else {
    score[3] -= premium*premium;
  }
  score[4] = 0;
  score[4] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QGraphicsItem*")) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x12a32e0;
  } else {
    score[4] -= premium*premium;
  }
  if (lqtL_testudata(L, 3, "QPainterPath*")) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x12a2d70;
  } else {
    score[4] -= premium*premium;
  }
  score[5] = 0;
  score[5] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QGraphicsItem*")) {
    score[5] += premium;
  } else if (false) {
    score[5] += premium-1; // table: 0x12acba0;
  } else {
    score[5] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[5] += premium;
  } else if (false) {
    score[5] += premium-1; // table: 0x12ad0d0;
  } else {
    score[5] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[5] += premium;
  } else if (false) {
    score[5] += premium-1; // table: 0x12ad480;
  } else {
    score[5] -= premium*premium;
  }
  score[6] = 0;
  score[6] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QGraphicsItem*")) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x12afb00;
  } else {
    score[6] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x12b0010;
  } else {
    score[6] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x12b03c0;
  } else {
    score[6] -= premium*premium;
  }
  if (lua_isnumber(L, 5)) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x12b0360;
  } else {
    score[6] -= premium*premium;
  }
  if (lua_isnumber(L, 6)) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x12b0b70;
  } else {
    score[6] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=6;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__mapFromItem__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__mapFromItem__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__mapFromItem__OverloadedVersion__3(L); break;
    case 4: return __LuaWrapCall__mapFromItem__OverloadedVersion__4(L); break;
    case 5: return __LuaWrapCall__mapFromItem__OverloadedVersion__5(L); break;
    case 6: return __LuaWrapCall__mapFromItem__OverloadedVersion__6(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__mapFromItem matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__childrenBoundingRect (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QRectF ret = __lua__obj->QGraphicsItem::childrenBoundingRect();
  lqtL_passudata(L, new QRectF(ret), "QRectF*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__isObscuredBy (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QGraphicsItem * arg1 = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 2, "QGraphicsItem*"));
  bool ret = __lua__obj->QGraphicsItem::isObscuredBy(arg1);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__children (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QList<QGraphicsItem*> ret = __lua__obj->QGraphicsItem::children();
  lqtL_passudata(L, new QList<QGraphicsItem*>(ret), "QList<QGraphicsItem*>*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__unsetCursor (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QGraphicsItem::unsetCursor();
  return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__shear (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  double arg2 = lua_tonumber(L, 3);
  __lua__obj->QGraphicsItem::shear(arg1, arg2);
  return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__type (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QGraphicsItem::type();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapToScene__OverloadedVersion__1 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPointF& arg1 = **static_cast<QPointF**>(lqtL_checkudata(L, 2, "QPointF*"));
  QPointF ret = __lua__obj->QGraphicsItem::mapToScene(arg1);
  lqtL_passudata(L, new QPointF(ret), "QPointF*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapToScene__OverloadedVersion__2 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QRectF& arg1 = **static_cast<QRectF**>(lqtL_checkudata(L, 2, "QRectF*"));
  QPolygonF ret = __lua__obj->QGraphicsItem::mapToScene(arg1);
  lqtL_passudata(L, new QPolygonF(ret), "QPolygonF*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapToScene__OverloadedVersion__3 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPolygonF& arg1 = **static_cast<QPolygonF**>(lqtL_checkudata(L, 2, "QPolygonF*"));
  QPolygonF ret = __lua__obj->QGraphicsItem::mapToScene(arg1);
  lqtL_passudata(L, new QPolygonF(ret), "QPolygonF*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapToScene__OverloadedVersion__4 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPainterPath& arg1 = **static_cast<QPainterPath**>(lqtL_checkudata(L, 2, "QPainterPath*"));
  QPainterPath ret = __lua__obj->QGraphicsItem::mapToScene(arg1);
  lqtL_passudata(L, new QPainterPath(ret), "QPainterPath*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapToScene__OverloadedVersion__5 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  double arg2 = lua_tonumber(L, 3);
  QPointF ret = __lua__obj->QGraphicsItem::mapToScene(arg1, arg2);
  lqtL_passudata(L, new QPointF(ret), "QPointF*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapToScene__OverloadedVersion__6 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  double arg2 = lua_tonumber(L, 3);
  double arg3 = lua_tonumber(L, 4);
  double arg4 = lua_tonumber(L, 5);
  QPolygonF ret = __lua__obj->QGraphicsItem::mapToScene(arg1, arg2, arg3, arg4);
  lqtL_passudata(L, new QPolygonF(ret), "QPolygonF*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapToScene (lua_State *L) {
  int score[7];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPointF*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x12954c0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QRectF*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x12977a0;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  score[3] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPolygonF*")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1299a90;
  } else {
    score[3] -= premium*premium;
  }
  score[4] = 0;
  score[4] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPainterPath*")) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x129be00;
  } else {
    score[4] -= premium*premium;
  }
  score[5] = 0;
  score[5] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[5] += premium;
  } else if (false) {
    score[5] += premium-1; // table: 0x12a7700;
  } else {
    score[5] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[5] += premium;
  } else if (false) {
    score[5] += premium-1; // table: 0x12a7c30;
  } else {
    score[5] -= premium*premium;
  }
  score[6] = 0;
  score[6] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x12ab520;
  } else {
    score[6] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x12aba50;
  } else {
    score[6] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x12abe00;
  } else {
    score[6] -= premium*premium;
  }
  if (lua_isnumber(L, 5)) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x12abda0;
  } else {
    score[6] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=6;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__mapToScene__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__mapToScene__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__mapToScene__OverloadedVersion__3(L); break;
    case 4: return __LuaWrapCall__mapToScene__OverloadedVersion__4(L); break;
    case 5: return __LuaWrapCall__mapToScene__OverloadedVersion__5(L); break;
    case 6: return __LuaWrapCall__mapToScene__OverloadedVersion__6(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__mapToScene matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__collidingItems (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::ItemSelectionMode arg1 = lqtL_isenum(L, 2, "Qt::ItemSelectionMode")?static_cast<Qt::ItemSelectionMode>(lqtL_toenum(L, 2, "Qt::ItemSelectionMode")):Qt::IntersectsItemShape;
  QList<QGraphicsItem*> ret = __lua__obj->QGraphicsItem::collidingItems(arg1);
  lqtL_passudata(L, new QList<QGraphicsItem*>(ret), "QList<QGraphicsItem*>*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__setEnabled (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QGraphicsItem::setEnabled(arg1);
  return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__acceptDrops (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QGraphicsItem::acceptDrops();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__zValue (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double ret = __lua__obj->QGraphicsItem::zValue();
  lua_pushnumber(L, ret);
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__resetMatrix (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QGraphicsItem::resetMatrix();
  return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__cursor (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QCursor ret = __lua__obj->QGraphicsItem::cursor();
  lqtL_passudata(L, new QCursor(ret), "QCursor*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapFromParent__OverloadedVersion__1 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPointF& arg1 = **static_cast<QPointF**>(lqtL_checkudata(L, 2, "QPointF*"));
  QPointF ret = __lua__obj->QGraphicsItem::mapFromParent(arg1);
  lqtL_passudata(L, new QPointF(ret), "QPointF*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapFromParent__OverloadedVersion__2 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QRectF& arg1 = **static_cast<QRectF**>(lqtL_checkudata(L, 2, "QRectF*"));
  QPolygonF ret = __lua__obj->QGraphicsItem::mapFromParent(arg1);
  lqtL_passudata(L, new QPolygonF(ret), "QPolygonF*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapFromParent__OverloadedVersion__3 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPolygonF& arg1 = **static_cast<QPolygonF**>(lqtL_checkudata(L, 2, "QPolygonF*"));
  QPolygonF ret = __lua__obj->QGraphicsItem::mapFromParent(arg1);
  lqtL_passudata(L, new QPolygonF(ret), "QPolygonF*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapFromParent__OverloadedVersion__4 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPainterPath& arg1 = **static_cast<QPainterPath**>(lqtL_checkudata(L, 2, "QPainterPath*"));
  QPainterPath ret = __lua__obj->QGraphicsItem::mapFromParent(arg1);
  lqtL_passudata(L, new QPainterPath(ret), "QPainterPath*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapFromParent__OverloadedVersion__5 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  double arg2 = lua_tonumber(L, 3);
  QPointF ret = __lua__obj->QGraphicsItem::mapFromParent(arg1, arg2);
  lqtL_passudata(L, new QPointF(ret), "QPointF*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapFromParent__OverloadedVersion__6 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  double arg2 = lua_tonumber(L, 3);
  double arg3 = lua_tonumber(L, 4);
  double arg4 = lua_tonumber(L, 5);
  QPolygonF ret = __lua__obj->QGraphicsItem::mapFromParent(arg1, arg2, arg3, arg4);
  lqtL_passudata(L, new QPolygonF(ret), "QPolygonF*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapFromParent (lua_State *L) {
  int score[7];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPointF*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x129d7d0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QRectF*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x129fae0;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  score[3] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPolygonF*")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x12a1e00;
  } else {
    score[3] -= premium*premium;
  }
  score[4] = 0;
  score[4] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPainterPath*")) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x12a4150;
  } else {
    score[4] -= premium*premium;
  }
  score[5] = 0;
  score[5] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[5] += premium;
  } else if (false) {
    score[5] += premium-1; // table: 0x12ade10;
  } else {
    score[5] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[5] += premium;
  } else if (false) {
    score[5] += premium-1; // table: 0x12ae340;
  } else {
    score[5] -= premium*premium;
  }
  score[6] = 0;
  score[6] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x12b14f0;
  } else {
    score[6] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x12b1a10;
  } else {
    score[6] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x12b1dc0;
  } else {
    score[6] -= premium*premium;
  }
  if (lua_isnumber(L, 5)) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x12b1d60;
  } else {
    score[6] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=6;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__mapFromParent__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__mapFromParent__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__mapFromParent__OverloadedVersion__3(L); break;
    case 4: return __LuaWrapCall__mapFromParent__OverloadedVersion__4(L); break;
    case 5: return __LuaWrapCall__mapFromParent__OverloadedVersion__5(L); break;
    case 6: return __LuaWrapCall__mapFromParent__OverloadedVersion__6(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__mapFromParent matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapToParent__OverloadedVersion__1 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPointF& arg1 = **static_cast<QPointF**>(lqtL_checkudata(L, 2, "QPointF*"));
  QPointF ret = __lua__obj->QGraphicsItem::mapToParent(arg1);
  lqtL_passudata(L, new QPointF(ret), "QPointF*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapToParent__OverloadedVersion__2 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QRectF& arg1 = **static_cast<QRectF**>(lqtL_checkudata(L, 2, "QRectF*"));
  QPolygonF ret = __lua__obj->QGraphicsItem::mapToParent(arg1);
  lqtL_passudata(L, new QPolygonF(ret), "QPolygonF*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapToParent__OverloadedVersion__3 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPolygonF& arg1 = **static_cast<QPolygonF**>(lqtL_checkudata(L, 2, "QPolygonF*"));
  QPolygonF ret = __lua__obj->QGraphicsItem::mapToParent(arg1);
  lqtL_passudata(L, new QPolygonF(ret), "QPolygonF*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapToParent__OverloadedVersion__4 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPainterPath& arg1 = **static_cast<QPainterPath**>(lqtL_checkudata(L, 2, "QPainterPath*"));
  QPainterPath ret = __lua__obj->QGraphicsItem::mapToParent(arg1);
  lqtL_passudata(L, new QPainterPath(ret), "QPainterPath*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapToParent__OverloadedVersion__5 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  double arg2 = lua_tonumber(L, 3);
  QPointF ret = __lua__obj->QGraphicsItem::mapToParent(arg1, arg2);
  lqtL_passudata(L, new QPointF(ret), "QPointF*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapToParent__OverloadedVersion__6 (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  double arg2 = lua_tonumber(L, 3);
  double arg3 = lua_tonumber(L, 4);
  double arg4 = lua_tonumber(L, 5);
  QPolygonF ret = __lua__obj->QGraphicsItem::mapToParent(arg1, arg2, arg3, arg4);
  lqtL_passudata(L, new QPolygonF(ret), "QPolygonF*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__mapToParent (lua_State *L) {
  int score[7];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPointF*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1294a70;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QRectF*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1296d50;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  score[3] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPolygonF*")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1299040;
  } else {
    score[3] -= premium*premium;
  }
  score[4] = 0;
  score[4] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPainterPath*")) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x129b380;
  } else {
    score[4] -= premium*premium;
  }
  score[5] = 0;
  score[5] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[5] += premium;
  } else if (false) {
    score[5] += premium-1; // table: 0x12a6870;
  } else {
    score[5] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[5] += premium;
  } else if (false) {
    score[5] += premium-1; // table: 0x12a6d90;
  } else {
    score[5] -= premium*premium;
  }
  score[6] = 0;
  score[6] += lqtL_testudata(L, 1, "QGraphicsItem*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x12a9f60;
  } else {
    score[6] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x12aa480;
  } else {
    score[6] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x12aa830;
  } else {
    score[6] -= premium*premium;
  }
  if (lua_isnumber(L, 5)) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x12aa7d0;
  } else {
    score[6] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=6;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__mapToParent__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__mapToParent__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__mapToParent__OverloadedVersion__3(L); break;
    case 4: return __LuaWrapCall__mapToParent__OverloadedVersion__4(L); break;
    case 5: return __LuaWrapCall__mapToParent__OverloadedVersion__5(L); break;
    case 6: return __LuaWrapCall__mapToParent__OverloadedVersion__6(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__mapToParent matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__setHandlesChildEvents (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QGraphicsItem::setHandlesChildEvents(arg1);
  return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__removeSceneEventFilter (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QGraphicsItem * arg1 = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 2, "QGraphicsItem*"));
  __lua__obj->QGraphicsItem::removeSceneEventFilter(arg1);
  return 0;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__data (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  QVariant ret = __lua__obj->QGraphicsItem::data(arg1);
  lqtL_passudata(L, new QVariant(ret), "QVariant*");
  return 1;
}
int LuaBinder< QGraphicsItem >::__LuaWrapCall__setData (lua_State *L) {
  QGraphicsItem *& __lua__obj = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 1, "QGraphicsItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  const QVariant& arg2 = **static_cast<QVariant**>(lqtL_checkudata(L, 3, "QVariant*"));
  __lua__obj->QGraphicsItem::setData(arg1, arg2);
  return 0;
}
void LuaBinder< QGraphicsItem >::advance (int arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "advance");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushinteger(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsItem::advance(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsItem >::focusInEvent (QFocusEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "focusInEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QFocusEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsItem::focusInEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsItem >::keyPressEvent (QKeyEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "keyPressEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QKeyEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsItem::keyPressEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsItem >::contextMenuEvent (QGraphicsSceneContextMenuEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "contextMenuEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QGraphicsSceneContextMenuEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsItem::contextMenuEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsItem >::keyReleaseEvent (QKeyEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "keyReleaseEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QKeyEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsItem::keyReleaseEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsItem >::dragLeaveEvent (QGraphicsSceneDragDropEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dragLeaveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QGraphicsSceneDragDropEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsItem::dragLeaveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsItem >::mousePressEvent (QGraphicsSceneMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mousePressEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QGraphicsSceneMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsItem::mousePressEvent(arg1);
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QGraphicsItem >::collidesWithPath (const QPainterPath& arg1, Qt::ItemSelectionMode arg2) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "collidesWithPath");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QPainterPath*");
  lqtL_pushenum(L, arg2, "Qt::ItemSelectionMode");
  if (lua_isfunction(L, -2-2)) {
    lua_pcall(L, 2+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QGraphicsItem::collidesWithPath(arg1, arg2);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QGraphicsItem >::hoverMoveEvent (QGraphicsSceneHoverEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "hoverMoveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QGraphicsSceneHoverEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsItem::hoverMoveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsItem >::dragMoveEvent (QGraphicsSceneDragDropEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dragMoveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QGraphicsSceneDragDropEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsItem::dragMoveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QGraphicsItem >::isObscuredBy (const QGraphicsItem * arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "isObscuredBy");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QGraphicsItem*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QGraphicsItem::isObscuredBy(arg1);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QGraphicsItem >::hoverLeaveEvent (QGraphicsSceneHoverEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "hoverLeaveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QGraphicsSceneHoverEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsItem::hoverLeaveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QGraphicsItem >::sceneEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "sceneEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QGraphicsItem::sceneEvent(arg1);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
bool LuaBinder< QGraphicsItem >::sceneEventFilter (QGraphicsItem * arg1, QEvent * arg2) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "sceneEventFilter");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QGraphicsItem*");
  lqtL_pushudata(L, arg2, "QEvent*");
  if (lua_isfunction(L, -2-2)) {
    lua_pcall(L, 2+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QGraphicsItem::sceneEventFilter(arg1, arg2);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
QRectF LuaBinder< QGraphicsItem >::boundingRect () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "boundingRect");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
      }
  QRectF ret = **static_cast<QRectF**>(lqtL_checkudata(L, -1, "QRectF*"));
  lua_settop(L, oldtop);
  return ret;
}
QVariant LuaBinder< QGraphicsItem >::itemChange (QGraphicsItem::GraphicsItemChange arg1, const QVariant& arg2) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "itemChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushenum(L, arg1, "QGraphicsItem::GraphicsItemChange");
  lqtL_pushudata(L, &(arg2), "QVariant*");
  if (lua_isfunction(L, -2-2)) {
    lua_pcall(L, 2+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QGraphicsItem::itemChange(arg1, arg2);
  }
  QVariant ret = **static_cast<QVariant**>(lqtL_checkudata(L, -1, "QVariant*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QGraphicsItem >::mouseDoubleClickEvent (QGraphicsSceneMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mouseDoubleClickEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QGraphicsSceneMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsItem::mouseDoubleClickEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsItem >::mouseMoveEvent (QGraphicsSceneMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mouseMoveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QGraphicsSceneMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsItem::mouseMoveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
QPainterPath LuaBinder< QGraphicsItem >::opaqueArea () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "opaqueArea");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QGraphicsItem::opaqueArea();
  }
  QPainterPath ret = **static_cast<QPainterPath**>(lqtL_checkudata(L, -1, "QPainterPath*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QGraphicsItem >::focusOutEvent (QFocusEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "focusOutEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QFocusEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsItem::focusOutEvent(arg1);
  }
  lua_settop(L, oldtop);
}
QVariant LuaBinder< QGraphicsItem >::extension (const QVariant& arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "extension");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QVariant*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QGraphicsItem::extension(arg1);
  }
  QVariant ret = **static_cast<QVariant**>(lqtL_checkudata(L, -1, "QVariant*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QGraphicsItem >::setExtension (QGraphicsItem::Extension arg1, const QVariant& arg2) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "setExtension");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushenum(L, arg1, "QGraphicsItem::Extension");
  lqtL_pushudata(L, &(arg2), "QVariant*");
  if (lua_isfunction(L, -2-2)) {
    lua_pcall(L, 2+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsItem::setExtension(arg1, arg2);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsItem >::inputMethodEvent (QInputMethodEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "inputMethodEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QInputMethodEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsItem::inputMethodEvent(arg1);
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QGraphicsItem >::type () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "type");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QGraphicsItem::type();
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
bool LuaBinder< QGraphicsItem >::supportsExtension (QGraphicsItem::Extension arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "supportsExtension");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushenum(L, arg1, "QGraphicsItem::Extension");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QGraphicsItem::supportsExtension(arg1);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
QVariant LuaBinder< QGraphicsItem >::inputMethodQuery (Qt::InputMethodQuery arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "inputMethodQuery");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushenum(L, arg1, "Qt::InputMethodQuery");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QGraphicsItem::inputMethodQuery(arg1);
  }
  QVariant ret = **static_cast<QVariant**>(lqtL_checkudata(L, -1, "QVariant*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QGraphicsItem >::wheelEvent (QGraphicsSceneWheelEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "wheelEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QGraphicsSceneWheelEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsItem::wheelEvent(arg1);
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QGraphicsItem >::contains (const QPointF& arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "contains");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QPointF*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QGraphicsItem::contains(arg1);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QGraphicsItem >::mouseReleaseEvent (QGraphicsSceneMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mouseReleaseEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QGraphicsSceneMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsItem::mouseReleaseEvent(arg1);
  }
  lua_settop(L, oldtop);
}
QPainterPath LuaBinder< QGraphicsItem >::shape () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "shape");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QGraphicsItem::shape();
  }
  QPainterPath ret = **static_cast<QPainterPath**>(lqtL_checkudata(L, -1, "QPainterPath*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QGraphicsItem >::paint (QPainter * arg1, const QStyleOptionGraphicsItem * arg2, QWidget * arg3) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "paint");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QPainter*");
  lqtL_pushudata(L, arg2, "QStyleOptionGraphicsItem*");
  lqtL_pushudata(L, arg3, "QWidget*");
  if (lua_isfunction(L, -3-2)) {
    lua_pcall(L, 3+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
      }
  lua_settop(L, oldtop);
}
bool LuaBinder< QGraphicsItem >::collidesWithItem (const QGraphicsItem * arg1, Qt::ItemSelectionMode arg2) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "collidesWithItem");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QGraphicsItem*");
  lqtL_pushenum(L, arg2, "Qt::ItemSelectionMode");
  if (lua_isfunction(L, -2-2)) {
    lua_pcall(L, 2+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QGraphicsItem::collidesWithItem(arg1, arg2);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QGraphicsItem >::hoverEnterEvent (QGraphicsSceneHoverEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "hoverEnterEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QGraphicsSceneHoverEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsItem::hoverEnterEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsItem >::dragEnterEvent (QGraphicsSceneDragDropEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dragEnterEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QGraphicsSceneDragDropEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsItem::dragEnterEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsItem >::dropEvent (QGraphicsSceneDragDropEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dropEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QGraphicsSceneDragDropEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsItem::dropEvent(arg1);
  }
  lua_settop(L, oldtop);
}
LuaBinder< QGraphicsItem >::  ~LuaBinder< QGraphicsItem > () {
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsItem*");
  lua_getfield(L, -1, "~QGraphicsItem");

  if (lua_isfunction(L, -1)) {
    lua_insert(L, -2);
    lua_pcall(L, 1, 1, 0);
  } else {
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QGraphicsItem >::lqt_pushenum_GraphicsItemFlag (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "ItemIsMovable");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "ItemIsMovable");
  lua_pushstring(L, "ItemIsSelectable");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "ItemIsSelectable");
  lua_pushstring(L, "ItemIsFocusable");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "ItemIsFocusable");
  lua_pushstring(L, "ItemClipsToShape");
  lua_rawseti(L, enum_table, 8);
  lua_pushinteger(L, 8);
  lua_setfield(L, enum_table, "ItemClipsToShape");
  lua_pushstring(L, "ItemClipsChildrenToShape");
  lua_rawseti(L, enum_table, 16);
  lua_pushinteger(L, 16);
  lua_setfield(L, enum_table, "ItemClipsChildrenToShape");
  lua_pushstring(L, "ItemIgnoresTransformations");
  lua_rawseti(L, enum_table, 32);
  lua_pushinteger(L, 32);
  lua_setfield(L, enum_table, "ItemIgnoresTransformations");
  lua_pushcfunction(L, LuaBinder< QGraphicsItem >::lqt_pushenum_GraphicsItemFlag_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QGraphicsItem::GraphicsItemFlag");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QGraphicsItem >::lqt_pushenum_GraphicsItemFlag_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QGraphicsItem::GraphicsItemFlag>*) + sizeof(QFlags<QGraphicsItem::GraphicsItemFlag>));
  QFlags<QGraphicsItem::GraphicsItemFlag> *fl = static_cast<QFlags<QGraphicsItem::GraphicsItemFlag>*>( static_cast<void*>(&static_cast<QFlags<QGraphicsItem::GraphicsItemFlag>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QGraphicsItem::GraphicsItemFlag>(lqtL_toenum(L, i, "QGraphicsItem::GraphicsItemFlag"));
	}
	if (luaL_newmetatable(L, "QFlags<QGraphicsItem::GraphicsItemFlag>*")) {
		lua_pushstring(L, "QFlags<QGraphicsItem::GraphicsItemFlag>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinder< QGraphicsItem >::lqt_pushenum_GraphicsItemChange (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "ItemPositionChange");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "ItemPositionChange");
  lua_pushstring(L, "ItemMatrixChange");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "ItemMatrixChange");
  lua_pushstring(L, "ItemVisibleChange");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "ItemVisibleChange");
  lua_pushstring(L, "ItemEnabledChange");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "ItemEnabledChange");
  lua_pushstring(L, "ItemSelectedChange");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "ItemSelectedChange");
  lua_pushstring(L, "ItemParentChange");
  lua_rawseti(L, enum_table, 5);
  lua_pushinteger(L, 5);
  lua_setfield(L, enum_table, "ItemParentChange");
  lua_pushstring(L, "ItemChildAddedChange");
  lua_rawseti(L, enum_table, 6);
  lua_pushinteger(L, 6);
  lua_setfield(L, enum_table, "ItemChildAddedChange");
  lua_pushstring(L, "ItemChildRemovedChange");
  lua_rawseti(L, enum_table, 7);
  lua_pushinteger(L, 7);
  lua_setfield(L, enum_table, "ItemChildRemovedChange");
  lua_pushstring(L, "ItemTransformChange");
  lua_rawseti(L, enum_table, 8);
  lua_pushinteger(L, 8);
  lua_setfield(L, enum_table, "ItemTransformChange");
  lua_pushstring(L, "ItemPositionHasChanged");
  lua_rawseti(L, enum_table, 9);
  lua_pushinteger(L, 9);
  lua_setfield(L, enum_table, "ItemPositionHasChanged");
  lua_pushstring(L, "ItemTransformHasChanged");
  lua_rawseti(L, enum_table, 10);
  lua_pushinteger(L, 10);
  lua_setfield(L, enum_table, "ItemTransformHasChanged");
  lua_pushstring(L, "ItemSceneChange");
  lua_rawseti(L, enum_table, 11);
  lua_pushinteger(L, 11);
  lua_setfield(L, enum_table, "ItemSceneChange");
  lua_pushcfunction(L, LuaBinder< QGraphicsItem >::lqt_pushenum_GraphicsItemChange_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QGraphicsItem::GraphicsItemChange");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QGraphicsItem >::lqt_pushenum_GraphicsItemChange_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QGraphicsItem::GraphicsItemChange>*) + sizeof(QFlags<QGraphicsItem::GraphicsItemChange>));
  QFlags<QGraphicsItem::GraphicsItemChange> *fl = static_cast<QFlags<QGraphicsItem::GraphicsItemChange>*>( static_cast<void*>(&static_cast<QFlags<QGraphicsItem::GraphicsItemChange>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QGraphicsItem::GraphicsItemChange>(lqtL_toenum(L, i, "QGraphicsItem::GraphicsItemChange"));
	}
	if (luaL_newmetatable(L, "QFlags<QGraphicsItem::GraphicsItemChange>*")) {
		lua_pushstring(L, "QFlags<QGraphicsItem::GraphicsItemChange>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinder< QGraphicsItem >::lqt_pushenum_Extension (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "UserExtension");
  lua_rawseti(L, enum_table, -2147483648);
  lua_pushinteger(L, -2147483648);
  lua_setfield(L, enum_table, "UserExtension");
  lua_pushcfunction(L, LuaBinder< QGraphicsItem >::lqt_pushenum_Extension_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QGraphicsItem::Extension");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QGraphicsItem >::lqt_pushenum_Extension_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QGraphicsItem::Extension>*) + sizeof(QFlags<QGraphicsItem::Extension>));
  QFlags<QGraphicsItem::Extension> *fl = static_cast<QFlags<QGraphicsItem::Extension>*>( static_cast<void*>(&static_cast<QFlags<QGraphicsItem::Extension>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QGraphicsItem::Extension>(lqtL_toenum(L, i, "QGraphicsItem::Extension"));
	}
	if (luaL_newmetatable(L, "QFlags<QGraphicsItem::Extension>*")) {
		lua_pushstring(L, "QFlags<QGraphicsItem::Extension>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int luaopen_QGraphicsItem (lua_State *L) {
  if (luaL_newmetatable(L, "QGraphicsItem*")) {
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__scenePos);
    lua_setfield(L, -2, "scenePos");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__setCursor);
    lua_setfield(L, -2, "setCursor");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__setPos);
    lua_setfield(L, -2, "setPos");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__isVisible);
    lua_setfield(L, -2, "isVisible");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__setAcceptsHoverEvents);
    lua_setfield(L, -2, "setAcceptsHoverEvents");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__hasCursor);
    lua_setfield(L, -2, "hasCursor");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__topLevelItem);
    lua_setfield(L, -2, "topLevelItem");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__y);
    lua_setfield(L, -2, "y");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__x);
    lua_setfield(L, -2, "x");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__setAcceptDrops);
    lua_setfield(L, -2, "setAcceptDrops");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__clearFocus);
    lua_setfield(L, -2, "clearFocus");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__deviceTransform);
    lua_setfield(L, -2, "deviceTransform");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__shape);
    lua_setfield(L, -2, "shape");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__pos);
    lua_setfield(L, -2, "pos");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__moveBy);
    lua_setfield(L, -2, "moveBy");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__scale);
    lua_setfield(L, -2, "scale");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__isSelected);
    lua_setfield(L, -2, "isSelected");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__setParentItem);
    lua_setfield(L, -2, "setParentItem");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__hasFocus);
    lua_setfield(L, -2, "hasFocus");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__setFocus);
    lua_setfield(L, -2, "setFocus");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__setFlag);
    lua_setfield(L, -2, "setFlag");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__translate);
    lua_setfield(L, -2, "translate");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__acceptedMouseButtons);
    lua_setfield(L, -2, "acceptedMouseButtons");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__opaqueArea);
    lua_setfield(L, -2, "opaqueArea");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__sceneTransform);
    lua_setfield(L, -2, "sceneTransform");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__resetTransform);
    lua_setfield(L, -2, "resetTransform");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__setToolTip);
    lua_setfield(L, -2, "setToolTip");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__parentItem);
    lua_setfield(L, -2, "parentItem");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__collidesWithItem);
    lua_setfield(L, -2, "collidesWithItem");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__show);
    lua_setfield(L, -2, "show");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__isEnabled);
    lua_setfield(L, -2, "isEnabled");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__setFlags);
    lua_setfield(L, -2, "setFlags");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__setGroup);
    lua_setfield(L, -2, "setGroup");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__transform);
    lua_setfield(L, -2, "transform");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__mapToItem);
    lua_setfield(L, -2, "mapToItem");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__ensureVisible);
    lua_setfield(L, -2, "ensureVisible");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__update);
    lua_setfield(L, -2, "update");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__isObscured);
    lua_setfield(L, -2, "isObscured");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__setAcceptedMouseButtons);
    lua_setfield(L, -2, "setAcceptedMouseButtons");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__matrix);
    lua_setfield(L, -2, "matrix");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__advance);
    lua_setfield(L, -2, "advance");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__toolTip);
    lua_setfield(L, -2, "toolTip");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__delete);
    lua_setfield(L, -2, "delete");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__sceneBoundingRect);
    lua_setfield(L, -2, "sceneBoundingRect");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__flags);
    lua_setfield(L, -2, "flags");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__isAncestorOf);
    lua_setfield(L, -2, "isAncestorOf");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__collidesWithPath);
    lua_setfield(L, -2, "collidesWithPath");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__new);
    lua_setfield(L, -2, "new");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__scene);
    lua_setfield(L, -2, "scene");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__rotate);
    lua_setfield(L, -2, "rotate");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__setSelected);
    lua_setfield(L, -2, "setSelected");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__hide);
    lua_setfield(L, -2, "hide");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__installSceneEventFilter);
    lua_setfield(L, -2, "installSceneEventFilter");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__contains);
    lua_setfield(L, -2, "contains");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__sceneMatrix);
    lua_setfield(L, -2, "sceneMatrix");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__setTransform);
    lua_setfield(L, -2, "setTransform");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__group);
    lua_setfield(L, -2, "group");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__mapFromScene);
    lua_setfield(L, -2, "mapFromScene");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__acceptsHoverEvents);
    lua_setfield(L, -2, "acceptsHoverEvents");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__setZValue);
    lua_setfield(L, -2, "setZValue");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__setMatrix);
    lua_setfield(L, -2, "setMatrix");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__setVisible);
    lua_setfield(L, -2, "setVisible");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__handlesChildEvents);
    lua_setfield(L, -2, "handlesChildEvents");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__mapFromItem);
    lua_setfield(L, -2, "mapFromItem");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__childrenBoundingRect);
    lua_setfield(L, -2, "childrenBoundingRect");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__isObscuredBy);
    lua_setfield(L, -2, "isObscuredBy");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__children);
    lua_setfield(L, -2, "children");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__unsetCursor);
    lua_setfield(L, -2, "unsetCursor");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__shear);
    lua_setfield(L, -2, "shear");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__type);
    lua_setfield(L, -2, "type");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__mapToScene);
    lua_setfield(L, -2, "mapToScene");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__collidingItems);
    lua_setfield(L, -2, "collidingItems");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__setEnabled);
    lua_setfield(L, -2, "setEnabled");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__acceptDrops);
    lua_setfield(L, -2, "acceptDrops");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__zValue);
    lua_setfield(L, -2, "zValue");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__resetMatrix);
    lua_setfield(L, -2, "resetMatrix");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__cursor);
    lua_setfield(L, -2, "cursor");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__mapFromParent);
    lua_setfield(L, -2, "mapFromParent");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__mapToParent);
    lua_setfield(L, -2, "mapToParent");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__setHandlesChildEvents);
    lua_setfield(L, -2, "setHandlesChildEvents");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__removeSceneEventFilter);
    lua_setfield(L, -2, "removeSceneEventFilter");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__data);
    lua_setfield(L, -2, "data");
    lua_pushcfunction(L, LuaBinder< QGraphicsItem >::__LuaWrapCall__setData);
    lua_setfield(L, -2, "setData");
    LuaBinder< QGraphicsItem >::lqt_pushenum_GraphicsItemFlag(L);
    lua_setfield(L, -2, "GraphicsItemFlag");
    LuaBinder< QGraphicsItem >::lqt_pushenum_GraphicsItemChange(L);
    lua_setfield(L, -2, "GraphicsItemChange");
    LuaBinder< QGraphicsItem >::lqt_pushenum_Extension(L);
    lua_setfield(L, -2, "Extension");
    lua_newtable(L);
    lua_setfield(L, -2, "__base");
    lua_pushcfunction(L, lqtL_newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, lqtL_index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, lqtL_gc);
    lua_setfield(L, -2, "__gc");
    lua_pushstring(L, "QGraphicsItem");
    lua_setfield(L, -2, "__qtype");
		lua_setglobal(L, "QGraphicsItem");
  } else {
		lua_pop(L, 1);
	}
  return 0;
}
