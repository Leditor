#include "lqt_common.hpp"
#include <QFont>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QFont > : public QFont {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__setStyleHint (lua_State *L);
  static int __LuaWrapCall__removeSubstitution (lua_State *L);
  static int __LuaWrapCall__setFamily (lua_State *L);
  static int __LuaWrapCall__underline (lua_State *L);
  static int __LuaWrapCall__setWeight (lua_State *L);
  static int __LuaWrapCall__italic (lua_State *L);
  static int __LuaWrapCall__key (lua_State *L);
  static int __LuaWrapCall__weight (lua_State *L);
  static int __LuaWrapCall__handle (lua_State *L);
  static int __LuaWrapCall__setStyle (lua_State *L);
  static int __LuaWrapCall__freetypeFace (lua_State *L);
  static int __LuaWrapCall__cleanup (lua_State *L);
  static int __LuaWrapCall__setStretch (lua_State *L);
  static int __LuaWrapCall__setPointSize (lua_State *L);
  static int __LuaWrapCall__setKerning (lua_State *L);
  static int __LuaWrapCall__isCopyOf (lua_State *L);
  static int __LuaWrapCall__strikeOut (lua_State *L);
  static int __LuaWrapCall__toString (lua_State *L);
  static int __LuaWrapCall__overline (lua_State *L);
  static int __LuaWrapCall__setStrikeOut (lua_State *L);
  static int __LuaWrapCall__style (lua_State *L);
  static int __LuaWrapCall__styleStrategy (lua_State *L);
  static int __LuaWrapCall__kerning (lua_State *L);
  static int __LuaWrapCall__resolve__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__resolve__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__resolve__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__resolve (lua_State *L);
  static int __LuaWrapCall__setStyleStrategy (lua_State *L);
  static int __LuaWrapCall__pointSize (lua_State *L);
  static int __LuaWrapCall__setFixedPitch (lua_State *L);
  static int __LuaWrapCall__fromString (lua_State *L);
  static int __LuaWrapCall__setPointSizeF (lua_State *L);
  static int __LuaWrapCall__setPixelSize (lua_State *L);
  static int __LuaWrapCall__pointSizeF (lua_State *L);
  static int __LuaWrapCall__stretch (lua_State *L);
  static int __LuaWrapCall__styleHint (lua_State *L);
  static int __LuaWrapCall__lastResortFont (lua_State *L);
  static int __LuaWrapCall__substitute (lua_State *L);
  static int __LuaWrapCall__lastResortFamily (lua_State *L);
  static int __LuaWrapCall__insertSubstitutions (lua_State *L);
  static int __LuaWrapCall__setOverline (lua_State *L);
  static int __LuaWrapCall__bold (lua_State *L);
  static int __LuaWrapCall__substitutions (lua_State *L);
  static int __LuaWrapCall__setItalic (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__4 (lua_State *L);
  static int __LuaWrapCall__new (lua_State *L);
  static int __LuaWrapCall__insertSubstitution (lua_State *L);
  static int __LuaWrapCall__delete (lua_State *L);
  static int __LuaWrapCall__setRawMode (lua_State *L);
  static int __LuaWrapCall__setUnderline (lua_State *L);
  static int __LuaWrapCall__rawMode (lua_State *L);
  static int __LuaWrapCall__pixelSize (lua_State *L);
  static int __LuaWrapCall__exactMatch (lua_State *L);
  static int __LuaWrapCall__substitutes (lua_State *L);
  static int __LuaWrapCall__rawName (lua_State *L);
  static int __LuaWrapCall__setRawName (lua_State *L);
  static int __LuaWrapCall__setBold (lua_State *L);
  static int __LuaWrapCall__initialize (lua_State *L);
  static int __LuaWrapCall__family (lua_State *L);
  static int __LuaWrapCall__defaultFamily (lua_State *L);
  static int __LuaWrapCall__fixedPitch (lua_State *L);
  static int __LuaWrapCall__cacheStatistics (lua_State *L);
  static int lqt_pushenum_StyleHint (lua_State *L);
  static int lqt_pushenum_StyleHint_QFLAGS_CREATOR (lua_State *L);
  static int lqt_pushenum_StyleStrategy (lua_State *L);
  static int lqt_pushenum_StyleStrategy_QFLAGS_CREATOR (lua_State *L);
  static int lqt_pushenum_Weight (lua_State *L);
  static int lqt_pushenum_Weight_QFLAGS_CREATOR (lua_State *L);
  static int lqt_pushenum_Style (lua_State *L);
  static int lqt_pushenum_Style_QFLAGS_CREATOR (lua_State *L);
  static int lqt_pushenum_Stretch (lua_State *L);
  static int lqt_pushenum_Stretch_QFLAGS_CREATOR (lua_State *L);
  LuaBinder< QFont > (lua_State *l):QFont(), L(l) {}
  LuaBinder< QFont > (lua_State *l, const QString& arg1, int arg2, int arg3, bool arg4):QFont(arg1, arg2, arg3, arg4), L(l) {}
  LuaBinder< QFont > (lua_State *l, const QFont& arg1, QPaintDevice * arg2):QFont(arg1, arg2), L(l) {}
  LuaBinder< QFont > (lua_State *l, const QFont& arg1):QFont(arg1), L(l) {}
};

extern "C" int luaopen_QFont (lua_State *L);
