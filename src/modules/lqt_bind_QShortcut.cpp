#include "lqt_bind_QShortcut.hpp"

int LuaBinder< QShortcut >::__LuaWrapCall__parentWidget (lua_State *L) {
  QShortcut *& __lua__obj = *static_cast<QShortcut**>(lqtL_checkudata(L, 1, "QShortcut*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * ret = __lua__obj->QShortcut::parentWidget();
  lqtL_pushudata(L, ret, "QWidget*");
  return 1;
}
int LuaBinder< QShortcut >::__LuaWrapCall__setContext (lua_State *L) {
  QShortcut *& __lua__obj = *static_cast<QShortcut**>(lqtL_checkudata(L, 1, "QShortcut*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::ShortcutContext arg1 = static_cast<Qt::ShortcutContext>(lqtL_toenum(L, 2, "Qt::ShortcutContext"));
  __lua__obj->QShortcut::setContext(arg1);
  return 0;
}
int LuaBinder< QShortcut >::__LuaWrapCall__key (lua_State *L) {
  QShortcut *& __lua__obj = *static_cast<QShortcut**>(lqtL_checkudata(L, 1, "QShortcut*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QKeySequence ret = __lua__obj->QShortcut::key();
  lqtL_passudata(L, new QKeySequence(ret), "QKeySequence*");
  return 1;
}
int LuaBinder< QShortcut >::__LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QShortcut::trUtf8(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QShortcut >::__LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QShortcut::trUtf8(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QShortcut >::__LuaWrapCall__trUtf8 (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x148f280;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x148ede0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1491070;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x14907a0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1491500;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__trUtf8__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__trUtf8__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__trUtf8 matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QShortcut >::__LuaWrapCall__context (lua_State *L) {
  QShortcut *& __lua__obj = *static_cast<QShortcut**>(lqtL_checkudata(L, 1, "QShortcut*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::ShortcutContext ret = __lua__obj->QShortcut::context();
  lqtL_pushenum(L, ret, "Qt::ShortcutContext");
  return 1;
}
int LuaBinder< QShortcut >::__LuaWrapCall__new__OverloadedVersion__2 (lua_State *L) {
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
  QShortcut * ret = new LuaBinder< QShortcut >(L, arg1);
  lqtL_passudata(L, ret, "QShortcut*");
  return 1;
}
int LuaBinder< QShortcut >::__LuaWrapCall__new__OverloadedVersion__3 (lua_State *L) {
  const QKeySequence& arg1 = **static_cast<QKeySequence**>(lqtL_checkudata(L, 1, "QKeySequence*"));
  QWidget * arg2 = *static_cast<QWidget**>(lqtL_checkudata(L, 2, "QWidget*"));
  const char * arg3 = (lua_type(L, 3)==LUA_TSTRING)?lua_tostring(L, 3):static_cast< const char * >(0);
  const char * arg4 = (lua_type(L, 4)==LUA_TSTRING)?lua_tostring(L, 4):static_cast< const char * >(0);
  Qt::ShortcutContext arg5 = lqtL_isenum(L, 5, "Qt::ShortcutContext")?static_cast<Qt::ShortcutContext>(lqtL_toenum(L, 5, "Qt::ShortcutContext")):Qt::WindowShortcut;
  QShortcut * ret = new LuaBinder< QShortcut >(L, arg1, arg2, arg3, arg4, arg5);
  lqtL_passudata(L, ret, "QShortcut*");
  return 1;
}
int LuaBinder< QShortcut >::__LuaWrapCall__new (lua_State *L) {
  int score[4];
	score[0] = score[1] = score[2] = score[3] = 0;
  const int premium = 11+lua_gettop(L);
  score[2] = 0;
  if (lqtL_testudata(L, 1, "QWidget*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1493f40;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  if (lqtL_testudata(L, 1, "QKeySequence*")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x14949d0;
  } else {
    score[3] -= premium*premium;
  }
  if (lqtL_testudata(L, 2, "QWidget*")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1494460;
  } else {
    score[3] -= premium*premium;
  }
  if ((lua_type(L, 3)==LUA_TSTRING)) {
    score[3] += premium;
  } else if (true) {
    score[3] += premium-1; // table: 0x1494ed0;
  } else {
    score[3] -= premium*premium;
  }
  if ((lua_type(L, 4)==LUA_TSTRING)) {
    score[3] += premium;
  } else if (true) {
    score[3] += premium-1; // table: 0x1495730;
  } else {
    score[3] -= premium*premium;
  }
  if (lqtL_isenum(L, 5, "Qt::ShortcutContext")) {
    score[3] += premium;
  } else if (true) {
    score[3] += premium-1; // table: 0x1495b70;
  } else {
    score[3] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=3;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 2: return __LuaWrapCall__new__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__new__OverloadedVersion__3(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__new matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QShortcut >::__LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QShortcut::tr(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QShortcut >::__LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QShortcut::tr(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QShortcut >::__LuaWrapCall__tr (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x148e520;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x148e280;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x148ff60;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1490450;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1490800;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__tr__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__tr__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__tr matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QShortcut >::__LuaWrapCall__delete (lua_State *L) {
  QShortcut *& __lua__obj = *static_cast<QShortcut**>(lqtL_checkudata(L, 1, "QShortcut*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  delete __lua__obj;
  __lua__obj = 0;
  return 0;
}
int LuaBinder< QShortcut >::__LuaWrapCall__setKey (lua_State *L) {
  QShortcut *& __lua__obj = *static_cast<QShortcut**>(lqtL_checkudata(L, 1, "QShortcut*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QKeySequence& arg1 = **static_cast<QKeySequence**>(lqtL_checkudata(L, 2, "QKeySequence*"));
  __lua__obj->QShortcut::setKey(arg1);
  return 0;
}
int LuaBinder< QShortcut >::__LuaWrapCall__whatsThis (lua_State *L) {
  QShortcut *& __lua__obj = *static_cast<QShortcut**>(lqtL_checkudata(L, 1, "QShortcut*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QShortcut::whatsThis();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QShortcut >::__LuaWrapCall__autoRepeat (lua_State *L) {
  QShortcut *& __lua__obj = *static_cast<QShortcut**>(lqtL_checkudata(L, 1, "QShortcut*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QShortcut::autoRepeat();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QShortcut >::__LuaWrapCall__id (lua_State *L) {
  QShortcut *& __lua__obj = *static_cast<QShortcut**>(lqtL_checkudata(L, 1, "QShortcut*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QShortcut::id();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QShortcut >::__LuaWrapCall__setWhatsThis (lua_State *L) {
  QShortcut *& __lua__obj = *static_cast<QShortcut**>(lqtL_checkudata(L, 1, "QShortcut*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QShortcut::setWhatsThis(arg1);
  return 0;
}
int LuaBinder< QShortcut >::__LuaWrapCall__isEnabled (lua_State *L) {
  QShortcut *& __lua__obj = *static_cast<QShortcut**>(lqtL_checkudata(L, 1, "QShortcut*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QShortcut::isEnabled();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QShortcut >::__LuaWrapCall__setEnabled (lua_State *L) {
  QShortcut *& __lua__obj = *static_cast<QShortcut**>(lqtL_checkudata(L, 1, "QShortcut*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QShortcut::setEnabled(arg1);
  return 0;
}
int LuaBinder< QShortcut >::__LuaWrapCall__setAutoRepeat (lua_State *L) {
  QShortcut *& __lua__obj = *static_cast<QShortcut**>(lqtL_checkudata(L, 1, "QShortcut*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QShortcut::setAutoRepeat(arg1);
  return 0;
}
int LuaBinder< QShortcut >::__LuaWrapCall__metaObject (lua_State *L) {
  QShortcut *& __lua__obj = *static_cast<QShortcut**>(lqtL_checkudata(L, 1, "QShortcut*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QMetaObject * ret = __lua__obj->QShortcut::metaObject();
  lqtL_pushudata(L, ret, "QMetaObject*");
  return 1;
}
bool LuaBinder< QShortcut >::event (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QShortcut*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "event");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QShortcut::event(arg1);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QShortcut >::connectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QShortcut*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "connectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::connectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QShortcut >::eventFilter (QObject * arg1, QEvent * arg2) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QShortcut*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "eventFilter");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QObject*");
  lqtL_pushudata(L, arg2, "QEvent*");
  if (lua_isfunction(L, -2-2)) {
    lua_pcall(L, 2+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QObject::eventFilter(arg1, arg2);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QShortcut >::disconnectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QShortcut*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "disconnectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::disconnectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QShortcut >::childEvent (QChildEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QShortcut*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "childEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QChildEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::childEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QShortcut >::timerEvent (QTimerEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QShortcut*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "timerEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QTimerEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::timerEvent(arg1);
  }
  lua_settop(L, oldtop);
}
const QMetaObject * LuaBinder< QShortcut >::metaObject () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QShortcut*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "metaObject");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QShortcut::metaObject();
  }
  const QMetaObject * ret = *static_cast<QMetaObject**>(lqtL_checkudata(L, -1, "QMetaObject*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QShortcut >::customEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QShortcut*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "customEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::customEvent(arg1);
  }
  lua_settop(L, oldtop);
}
LuaBinder< QShortcut >::  ~LuaBinder< QShortcut > () {
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QShortcut*");
  lua_getfield(L, -1, "~QShortcut");

  if (lua_isfunction(L, -1)) {
    lua_insert(L, -2);
    lua_pcall(L, 1, 1, 0);
  } else {
  }
  lua_settop(L, oldtop);
}
int luaopen_QShortcut (lua_State *L) {
  if (luaL_newmetatable(L, "QShortcut*")) {
    lua_pushcfunction(L, LuaBinder< QShortcut >::__LuaWrapCall__parentWidget);
    lua_setfield(L, -2, "parentWidget");
    lua_pushcfunction(L, LuaBinder< QShortcut >::__LuaWrapCall__setContext);
    lua_setfield(L, -2, "setContext");
    lua_pushcfunction(L, LuaBinder< QShortcut >::__LuaWrapCall__key);
    lua_setfield(L, -2, "key");
    lua_pushcfunction(L, LuaBinder< QShortcut >::__LuaWrapCall__trUtf8);
    lua_setfield(L, -2, "trUtf8");
    lua_pushcfunction(L, LuaBinder< QShortcut >::__LuaWrapCall__context);
    lua_setfield(L, -2, "context");
    lua_pushcfunction(L, LuaBinder< QShortcut >::__LuaWrapCall__new);
    lua_setfield(L, -2, "new");
    lua_pushcfunction(L, LuaBinder< QShortcut >::__LuaWrapCall__tr);
    lua_setfield(L, -2, "tr");
    lua_pushcfunction(L, LuaBinder< QShortcut >::__LuaWrapCall__delete);
    lua_setfield(L, -2, "delete");
    lua_pushcfunction(L, LuaBinder< QShortcut >::__LuaWrapCall__setKey);
    lua_setfield(L, -2, "setKey");
    lua_pushcfunction(L, LuaBinder< QShortcut >::__LuaWrapCall__whatsThis);
    lua_setfield(L, -2, "whatsThis");
    lua_pushcfunction(L, LuaBinder< QShortcut >::__LuaWrapCall__autoRepeat);
    lua_setfield(L, -2, "autoRepeat");
    lua_pushcfunction(L, LuaBinder< QShortcut >::__LuaWrapCall__id);
    lua_setfield(L, -2, "id");
    lua_pushcfunction(L, LuaBinder< QShortcut >::__LuaWrapCall__setWhatsThis);
    lua_setfield(L, -2, "setWhatsThis");
    lua_pushcfunction(L, LuaBinder< QShortcut >::__LuaWrapCall__isEnabled);
    lua_setfield(L, -2, "isEnabled");
    lua_pushcfunction(L, LuaBinder< QShortcut >::__LuaWrapCall__setEnabled);
    lua_setfield(L, -2, "setEnabled");
    lua_pushcfunction(L, LuaBinder< QShortcut >::__LuaWrapCall__setAutoRepeat);
    lua_setfield(L, -2, "setAutoRepeat");
    lua_pushcfunction(L, LuaBinder< QShortcut >::__LuaWrapCall__metaObject);
    lua_setfield(L, -2, "metaObject");
    lua_newtable(L);
    lua_pushboolean(L, 1);
    lua_setfield(L, -2, "QObject*");
    lua_setfield(L, -2, "__base");
    lua_pushcfunction(L, lqtL_newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, lqtL_index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, lqtL_gc);
    lua_setfield(L, -2, "__gc");
    lua_pushstring(L, "QShortcut");
    lua_setfield(L, -2, "__qtype");
		lua_setglobal(L, "QShortcut");
  } else {
		lua_pop(L, 1);
	}
  return 0;
}
