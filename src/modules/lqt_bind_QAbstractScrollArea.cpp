#include "lqt_bind_QAbstractScrollArea.hpp"

int LuaBinder< QAbstractScrollArea >::__LuaWrapCall__setHorizontalScrollBarPolicy (lua_State *L) {
  QAbstractScrollArea *& __lua__obj = *static_cast<QAbstractScrollArea**>(lqtL_checkudata(L, 1, "QAbstractScrollArea*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::ScrollBarPolicy arg1 = static_cast<Qt::ScrollBarPolicy>(lqtL_toenum(L, 2, "Qt::ScrollBarPolicy"));
  __lua__obj->QAbstractScrollArea::setHorizontalScrollBarPolicy(arg1);
  return 0;
}
int LuaBinder< QAbstractScrollArea >::__LuaWrapCall__setCornerWidget (lua_State *L) {
  QAbstractScrollArea *& __lua__obj = *static_cast<QAbstractScrollArea**>(lqtL_checkudata(L, 1, "QAbstractScrollArea*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 2, "QWidget*"));
  __lua__obj->QAbstractScrollArea::setCornerWidget(arg1);
  return 0;
}
int LuaBinder< QAbstractScrollArea >::__LuaWrapCall__sizeHint (lua_State *L) {
  QAbstractScrollArea *& __lua__obj = *static_cast<QAbstractScrollArea**>(lqtL_checkudata(L, 1, "QAbstractScrollArea*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QSize ret = __lua__obj->QAbstractScrollArea::sizeHint();
  lqtL_passudata(L, new QSize(ret), "QSize*");
  return 1;
}
int LuaBinder< QAbstractScrollArea >::__LuaWrapCall__setHorizontalScrollBar (lua_State *L) {
  QAbstractScrollArea *& __lua__obj = *static_cast<QAbstractScrollArea**>(lqtL_checkudata(L, 1, "QAbstractScrollArea*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QScrollBar * arg1 = *static_cast<QScrollBar**>(lqtL_checkudata(L, 2, "QScrollBar*"));
  __lua__obj->QAbstractScrollArea::setHorizontalScrollBar(arg1);
  return 0;
}
int LuaBinder< QAbstractScrollArea >::__LuaWrapCall__minimumSizeHint (lua_State *L) {
  QAbstractScrollArea *& __lua__obj = *static_cast<QAbstractScrollArea**>(lqtL_checkudata(L, 1, "QAbstractScrollArea*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QSize ret = __lua__obj->QAbstractScrollArea::minimumSizeHint();
  lqtL_passudata(L, new QSize(ret), "QSize*");
  return 1;
}
int LuaBinder< QAbstractScrollArea >::__LuaWrapCall__horizontalScrollBar (lua_State *L) {
  QAbstractScrollArea *& __lua__obj = *static_cast<QAbstractScrollArea**>(lqtL_checkudata(L, 1, "QAbstractScrollArea*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QScrollBar * ret = __lua__obj->QAbstractScrollArea::horizontalScrollBar();
  lqtL_pushudata(L, ret, "QScrollBar*");
  return 1;
}
int LuaBinder< QAbstractScrollArea >::__LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QAbstractScrollArea::trUtf8(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QAbstractScrollArea >::__LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QAbstractScrollArea::trUtf8(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QAbstractScrollArea >::__LuaWrapCall__trUtf8 (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x9e87c0;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x5f54f0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x8e2e00;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xa99e90;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x9e5080;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__trUtf8__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__trUtf8__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__trUtf8 matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QAbstractScrollArea >::__LuaWrapCall__maximumViewportSize (lua_State *L) {
  QAbstractScrollArea *& __lua__obj = *static_cast<QAbstractScrollArea**>(lqtL_checkudata(L, 1, "QAbstractScrollArea*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QSize ret = __lua__obj->QAbstractScrollArea::maximumViewportSize();
  lqtL_passudata(L, new QSize(ret), "QSize*");
  return 1;
}
int LuaBinder< QAbstractScrollArea >::__LuaWrapCall__setVerticalScrollBarPolicy (lua_State *L) {
  QAbstractScrollArea *& __lua__obj = *static_cast<QAbstractScrollArea**>(lqtL_checkudata(L, 1, "QAbstractScrollArea*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::ScrollBarPolicy arg1 = static_cast<Qt::ScrollBarPolicy>(lqtL_toenum(L, 2, "Qt::ScrollBarPolicy"));
  __lua__obj->QAbstractScrollArea::setVerticalScrollBarPolicy(arg1);
  return 0;
}
int LuaBinder< QAbstractScrollArea >::__LuaWrapCall__new (lua_State *L) {
  QWidget * arg1 = lqtL_testudata(L, 1, "QWidget*")?*static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*")):static_cast< QWidget * >(0);
  QAbstractScrollArea * ret = new LuaBinder< QAbstractScrollArea >(L, arg1);
  lqtL_passudata(L, ret, "QAbstractScrollArea*");
  return 1;
}
int LuaBinder< QAbstractScrollArea >::__LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QAbstractScrollArea::tr(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QAbstractScrollArea >::__LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QAbstractScrollArea::tr(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QAbstractScrollArea >::__LuaWrapCall__tr (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x5f2600;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0xe469f0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x9e79d0;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xd7f300;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x8ba4b0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__tr__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__tr__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__tr matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QAbstractScrollArea >::__LuaWrapCall__scrollBarWidgets (lua_State *L) {
  QAbstractScrollArea *& __lua__obj = *static_cast<QAbstractScrollArea**>(lqtL_checkudata(L, 1, "QAbstractScrollArea*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<Qt::AlignmentFlag> arg1 = **static_cast<QFlags<Qt::AlignmentFlag>**>(lqtL_checkudata(L, 2, "QFlags<Qt::AlignmentFlag>*"));
  QList<QWidget*> ret = __lua__obj->QAbstractScrollArea::scrollBarWidgets(arg1);
  lqtL_passudata(L, new QList<QWidget*>(ret), "QList<QWidget*>*");
  return 1;
}
int LuaBinder< QAbstractScrollArea >::__LuaWrapCall__cornerWidget (lua_State *L) {
  QAbstractScrollArea *& __lua__obj = *static_cast<QAbstractScrollArea**>(lqtL_checkudata(L, 1, "QAbstractScrollArea*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * ret = __lua__obj->QAbstractScrollArea::cornerWidget();
  lqtL_pushudata(L, ret, "QWidget*");
  return 1;
}
int LuaBinder< QAbstractScrollArea >::__LuaWrapCall__verticalScrollBar (lua_State *L) {
  QAbstractScrollArea *& __lua__obj = *static_cast<QAbstractScrollArea**>(lqtL_checkudata(L, 1, "QAbstractScrollArea*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QScrollBar * ret = __lua__obj->QAbstractScrollArea::verticalScrollBar();
  lqtL_pushudata(L, ret, "QScrollBar*");
  return 1;
}
int LuaBinder< QAbstractScrollArea >::__LuaWrapCall__addScrollBarWidget (lua_State *L) {
  QAbstractScrollArea *& __lua__obj = *static_cast<QAbstractScrollArea**>(lqtL_checkudata(L, 1, "QAbstractScrollArea*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 2, "QWidget*"));
  QFlags<Qt::AlignmentFlag> arg2 = **static_cast<QFlags<Qt::AlignmentFlag>**>(lqtL_checkudata(L, 3, "QFlags<Qt::AlignmentFlag>*"));
  __lua__obj->QAbstractScrollArea::addScrollBarWidget(arg1, arg2);
  return 0;
}
int LuaBinder< QAbstractScrollArea >::__LuaWrapCall__setViewport (lua_State *L) {
  QAbstractScrollArea *& __lua__obj = *static_cast<QAbstractScrollArea**>(lqtL_checkudata(L, 1, "QAbstractScrollArea*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 2, "QWidget*"));
  __lua__obj->QAbstractScrollArea::setViewport(arg1);
  return 0;
}
int LuaBinder< QAbstractScrollArea >::__LuaWrapCall__delete (lua_State *L) {
  QAbstractScrollArea *& __lua__obj = *static_cast<QAbstractScrollArea**>(lqtL_checkudata(L, 1, "QAbstractScrollArea*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  delete __lua__obj;
  __lua__obj = 0;
  return 0;
}
int LuaBinder< QAbstractScrollArea >::__LuaWrapCall__metaObject (lua_State *L) {
  QAbstractScrollArea *& __lua__obj = *static_cast<QAbstractScrollArea**>(lqtL_checkudata(L, 1, "QAbstractScrollArea*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QMetaObject * ret = __lua__obj->QAbstractScrollArea::metaObject();
  lqtL_pushudata(L, ret, "QMetaObject*");
  return 1;
}
int LuaBinder< QAbstractScrollArea >::__LuaWrapCall__viewport (lua_State *L) {
  QAbstractScrollArea *& __lua__obj = *static_cast<QAbstractScrollArea**>(lqtL_checkudata(L, 1, "QAbstractScrollArea*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * ret = __lua__obj->QAbstractScrollArea::viewport();
  lqtL_pushudata(L, ret, "QWidget*");
  return 1;
}
int LuaBinder< QAbstractScrollArea >::__LuaWrapCall__verticalScrollBarPolicy (lua_State *L) {
  QAbstractScrollArea *& __lua__obj = *static_cast<QAbstractScrollArea**>(lqtL_checkudata(L, 1, "QAbstractScrollArea*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::ScrollBarPolicy ret = __lua__obj->QAbstractScrollArea::verticalScrollBarPolicy();
  lqtL_pushenum(L, ret, "Qt::ScrollBarPolicy");
  return 1;
}
int LuaBinder< QAbstractScrollArea >::__LuaWrapCall__horizontalScrollBarPolicy (lua_State *L) {
  QAbstractScrollArea *& __lua__obj = *static_cast<QAbstractScrollArea**>(lqtL_checkudata(L, 1, "QAbstractScrollArea*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::ScrollBarPolicy ret = __lua__obj->QAbstractScrollArea::horizontalScrollBarPolicy();
  lqtL_pushenum(L, ret, "Qt::ScrollBarPolicy");
  return 1;
}
int LuaBinder< QAbstractScrollArea >::__LuaWrapCall__setVerticalScrollBar (lua_State *L) {
  QAbstractScrollArea *& __lua__obj = *static_cast<QAbstractScrollArea**>(lqtL_checkudata(L, 1, "QAbstractScrollArea*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QScrollBar * arg1 = *static_cast<QScrollBar**>(lqtL_checkudata(L, 2, "QScrollBar*"));
  __lua__obj->QAbstractScrollArea::setVerticalScrollBar(arg1);
  return 0;
}
void LuaBinder< QAbstractScrollArea >::styleChange (QStyle& arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "styleChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QStyle*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::styleChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QAbstractScrollArea >::focusInEvent (QFocusEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "focusInEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QFocusEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::focusInEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QAbstractScrollArea >::keyPressEvent (QKeyEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "keyPressEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QKeyEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QAbstractScrollArea::keyPressEvent(arg1);
  }
  lua_settop(L, oldtop);
}
const QMetaObject * LuaBinder< QAbstractScrollArea >::metaObject () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "metaObject");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QAbstractScrollArea::metaObject();
  }
  const QMetaObject * ret = *static_cast<QMetaObject**>(lqtL_checkudata(L, -1, "QMetaObject*"));
  lua_settop(L, oldtop);
  return ret;
}
int LuaBinder< QAbstractScrollArea >::devType () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "devType");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::devType();
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QAbstractScrollArea >::setVisible (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "setVisible");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::setVisible(arg1);
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QAbstractScrollArea >::focusNextPrevChild (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "focusNextPrevChild");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::focusNextPrevChild(arg1);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
QPaintEngine * LuaBinder< QAbstractScrollArea >::paintEngine () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "paintEngine");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::paintEngine();
  }
  QPaintEngine * ret = *static_cast<QPaintEngine**>(lqtL_checkudata(L, -1, "QPaintEngine*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QAbstractScrollArea >::dragLeaveEvent (QDragLeaveEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dragLeaveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDragLeaveEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QAbstractScrollArea::dragLeaveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QAbstractScrollArea >::mousePressEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mousePressEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QAbstractScrollArea::mousePressEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QAbstractScrollArea >::dragMoveEvent (QDragMoveEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dragMoveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDragMoveEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QAbstractScrollArea::dragMoveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QAbstractScrollArea >::closeEvent (QCloseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "closeEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QCloseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::closeEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QAbstractScrollArea >::enterEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "enterEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::enterEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QAbstractScrollArea >::tabletEvent (QTabletEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "tabletEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QTabletEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::tabletEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QAbstractScrollArea >::dropEvent (QDropEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dropEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDropEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QAbstractScrollArea::dropEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QAbstractScrollArea >::moveEvent (QMoveEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "moveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMoveEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::moveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
QVariant LuaBinder< QAbstractScrollArea >::inputMethodQuery (Qt::InputMethodQuery arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "inputMethodQuery");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushenum(L, arg1, "Qt::InputMethodQuery");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::inputMethodQuery(arg1);
  }
  QVariant ret = **static_cast<QVariant**>(lqtL_checkudata(L, -1, "QVariant*"));
  lua_settop(L, oldtop);
  return ret;
}
QSize LuaBinder< QAbstractScrollArea >::minimumSizeHint () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "minimumSizeHint");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QAbstractScrollArea::minimumSizeHint();
  }
  QSize ret = **static_cast<QSize**>(lqtL_checkudata(L, -1, "QSize*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QAbstractScrollArea >::changeEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "changeEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QFrame::changeEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QAbstractScrollArea >::contextMenuEvent (QContextMenuEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "contextMenuEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QContextMenuEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QAbstractScrollArea::contextMenuEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QAbstractScrollArea >::showEvent (QShowEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "showEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QShowEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::showEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QAbstractScrollArea >::windowActivationChange (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "windowActivationChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::windowActivationChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QAbstractScrollArea >::paintEvent (QPaintEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "paintEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QPaintEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QAbstractScrollArea::paintEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QAbstractScrollArea >::actionEvent (QActionEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "actionEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QActionEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::actionEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QAbstractScrollArea >::keyReleaseEvent (QKeyEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "keyReleaseEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QKeyEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::keyReleaseEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QAbstractScrollArea >::enabledChange (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "enabledChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::enabledChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QAbstractScrollArea >::mouseReleaseEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mouseReleaseEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QAbstractScrollArea::mouseReleaseEvent(arg1);
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QAbstractScrollArea >::heightForWidth (int arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "heightForWidth");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushinteger(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::heightForWidth(arg1);
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QAbstractScrollArea >::mouseDoubleClickEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mouseDoubleClickEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QAbstractScrollArea::mouseDoubleClickEvent(arg1);
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QAbstractScrollArea >::metric (QPaintDevice::PaintDeviceMetric arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "metric");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushenum(L, arg1, "QPaintDevice::PaintDeviceMetric");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::metric(arg1);
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QAbstractScrollArea >::disconnectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "disconnectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::disconnectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QAbstractScrollArea >::mouseMoveEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mouseMoveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QAbstractScrollArea::mouseMoveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QAbstractScrollArea >::focusOutEvent (QFocusEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "focusOutEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QFocusEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::focusOutEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QAbstractScrollArea >::childEvent (QChildEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "childEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QChildEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::childEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QAbstractScrollArea >::connectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "connectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::connectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QAbstractScrollArea >::timerEvent (QTimerEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "timerEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QTimerEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::timerEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QAbstractScrollArea >::fontChange (const QFont& arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "fontChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QFont*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::fontChange(arg1);
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QAbstractScrollArea >::eventFilter (QObject * arg1, QEvent * arg2) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "eventFilter");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QObject*");
  lqtL_pushudata(L, arg2, "QEvent*");
  if (lua_isfunction(L, -2-2)) {
    lua_pcall(L, 2+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QObject::eventFilter(arg1, arg2);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QAbstractScrollArea >::inputMethodEvent (QInputMethodEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "inputMethodEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QInputMethodEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::inputMethodEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QAbstractScrollArea >::wheelEvent (QWheelEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "wheelEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QWheelEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QAbstractScrollArea::wheelEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QAbstractScrollArea >::resizeEvent (QResizeEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "resizeEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QResizeEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QAbstractScrollArea::resizeEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QAbstractScrollArea >::paletteChange (const QPalette& arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "paletteChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QPalette*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::paletteChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QAbstractScrollArea >::languageChange () {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "languageChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::languageChange();
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QAbstractScrollArea >::event (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "event");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QAbstractScrollArea::event(arg1);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QAbstractScrollArea >::hideEvent (QHideEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "hideEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QHideEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::hideEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QAbstractScrollArea >::dragEnterEvent (QDragEnterEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dragEnterEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDragEnterEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QAbstractScrollArea::dragEnterEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QAbstractScrollArea >::scrollContentsBy (int arg1, int arg2) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "scrollContentsBy");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushinteger(L, arg1);
  lua_pushinteger(L, arg2);
  if (lua_isfunction(L, -2-2)) {
    lua_pcall(L, 2+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QAbstractScrollArea::scrollContentsBy(arg1, arg2);
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QAbstractScrollArea >::viewportEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "viewportEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QAbstractScrollArea::viewportEvent(arg1);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QAbstractScrollArea >::leaveEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "leaveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::leaveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
QSize LuaBinder< QAbstractScrollArea >::sizeHint () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "sizeHint");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QAbstractScrollArea::sizeHint();
  }
  QSize ret = **static_cast<QSize**>(lqtL_checkudata(L, -1, "QSize*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QAbstractScrollArea >::customEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "customEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::customEvent(arg1);
  }
  lua_settop(L, oldtop);
}
LuaBinder< QAbstractScrollArea >::  ~LuaBinder< QAbstractScrollArea > () {
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAbstractScrollArea*");
  lua_getfield(L, -1, "~QAbstractScrollArea");

  if (lua_isfunction(L, -1)) {
    lua_insert(L, -2);
    lua_pcall(L, 1, 1, 0);
  } else {
  }
  lua_settop(L, oldtop);
}
int luaopen_QAbstractScrollArea (lua_State *L) {
  if (luaL_newmetatable(L, "QAbstractScrollArea*")) {
    lua_pushcfunction(L, LuaBinder< QAbstractScrollArea >::__LuaWrapCall__setHorizontalScrollBarPolicy);
    lua_setfield(L, -2, "setHorizontalScrollBarPolicy");
    lua_pushcfunction(L, LuaBinder< QAbstractScrollArea >::__LuaWrapCall__setCornerWidget);
    lua_setfield(L, -2, "setCornerWidget");
    lua_pushcfunction(L, LuaBinder< QAbstractScrollArea >::__LuaWrapCall__sizeHint);
    lua_setfield(L, -2, "sizeHint");
    lua_pushcfunction(L, LuaBinder< QAbstractScrollArea >::__LuaWrapCall__setHorizontalScrollBar);
    lua_setfield(L, -2, "setHorizontalScrollBar");
    lua_pushcfunction(L, LuaBinder< QAbstractScrollArea >::__LuaWrapCall__minimumSizeHint);
    lua_setfield(L, -2, "minimumSizeHint");
    lua_pushcfunction(L, LuaBinder< QAbstractScrollArea >::__LuaWrapCall__horizontalScrollBar);
    lua_setfield(L, -2, "horizontalScrollBar");
    lua_pushcfunction(L, LuaBinder< QAbstractScrollArea >::__LuaWrapCall__trUtf8);
    lua_setfield(L, -2, "trUtf8");
    lua_pushcfunction(L, LuaBinder< QAbstractScrollArea >::__LuaWrapCall__maximumViewportSize);
    lua_setfield(L, -2, "maximumViewportSize");
    lua_pushcfunction(L, LuaBinder< QAbstractScrollArea >::__LuaWrapCall__setVerticalScrollBarPolicy);
    lua_setfield(L, -2, "setVerticalScrollBarPolicy");
    lua_pushcfunction(L, LuaBinder< QAbstractScrollArea >::__LuaWrapCall__new);
    lua_setfield(L, -2, "new");
    lua_pushcfunction(L, LuaBinder< QAbstractScrollArea >::__LuaWrapCall__tr);
    lua_setfield(L, -2, "tr");
    lua_pushcfunction(L, LuaBinder< QAbstractScrollArea >::__LuaWrapCall__scrollBarWidgets);
    lua_setfield(L, -2, "scrollBarWidgets");
    lua_pushcfunction(L, LuaBinder< QAbstractScrollArea >::__LuaWrapCall__cornerWidget);
    lua_setfield(L, -2, "cornerWidget");
    lua_pushcfunction(L, LuaBinder< QAbstractScrollArea >::__LuaWrapCall__verticalScrollBar);
    lua_setfield(L, -2, "verticalScrollBar");
    lua_pushcfunction(L, LuaBinder< QAbstractScrollArea >::__LuaWrapCall__addScrollBarWidget);
    lua_setfield(L, -2, "addScrollBarWidget");
    lua_pushcfunction(L, LuaBinder< QAbstractScrollArea >::__LuaWrapCall__setViewport);
    lua_setfield(L, -2, "setViewport");
    lua_pushcfunction(L, LuaBinder< QAbstractScrollArea >::__LuaWrapCall__delete);
    lua_setfield(L, -2, "delete");
    lua_pushcfunction(L, LuaBinder< QAbstractScrollArea >::__LuaWrapCall__metaObject);
    lua_setfield(L, -2, "metaObject");
    lua_pushcfunction(L, LuaBinder< QAbstractScrollArea >::__LuaWrapCall__viewport);
    lua_setfield(L, -2, "viewport");
    lua_pushcfunction(L, LuaBinder< QAbstractScrollArea >::__LuaWrapCall__verticalScrollBarPolicy);
    lua_setfield(L, -2, "verticalScrollBarPolicy");
    lua_pushcfunction(L, LuaBinder< QAbstractScrollArea >::__LuaWrapCall__horizontalScrollBarPolicy);
    lua_setfield(L, -2, "horizontalScrollBarPolicy");
    lua_pushcfunction(L, LuaBinder< QAbstractScrollArea >::__LuaWrapCall__setVerticalScrollBar);
    lua_setfield(L, -2, "setVerticalScrollBar");
    lua_newtable(L);
    lua_pushboolean(L, 1);
    lua_setfield(L, -2, "QFrame*");
    lua_pushboolean(L, 0);
    lua_setfield(L, -2, "QObject*");
    lua_pushboolean(L, 0);
    lua_setfield(L, -2, "QPaintDevice*");
    lua_pushboolean(L, 0);
    lua_setfield(L, -2, "QWidget*");
    lua_setfield(L, -2, "__base");
    lua_pushcfunction(L, lqtL_newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, lqtL_index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, lqtL_gc);
    lua_setfield(L, -2, "__gc");
    lua_pushstring(L, "QAbstractScrollArea");
    lua_setfield(L, -2, "__qtype");
		lua_setglobal(L, "QAbstractScrollArea");
  } else {
		lua_pop(L, 1);
	}
  return 0;
}
