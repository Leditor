#include "lqt_bind_QMainWindow.hpp"

int LuaBinder< QMainWindow >::__LuaWrapCall__splitDockWidget (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QDockWidget * arg1 = *static_cast<QDockWidget**>(lqtL_checkudata(L, 2, "QDockWidget*"));
  QDockWidget * arg2 = *static_cast<QDockWidget**>(lqtL_checkudata(L, 3, "QDockWidget*"));
  Qt::Orientation arg3 = static_cast<Qt::Orientation>(lqtL_toenum(L, 4, "Qt::Orientation"));
  __lua__obj->QMainWindow::splitDockWidget(arg1, arg2, arg3);
  return 0;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__toolBarBreak (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QToolBar * arg1 = *static_cast<QToolBar**>(lqtL_checkudata(L, 2, "QToolBar*"));
  bool ret = __lua__obj->QMainWindow::toolBarBreak(arg1);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__saveState (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_isnumber(L, 2)?lua_tointeger(L, 2):static_cast< int >(0);
  QByteArray ret = __lua__obj->QMainWindow::saveState(arg1);
  lua_pushlstring(L, ret.data(), ret.size());
  return 1;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__addToolBarBreak (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::ToolBarArea arg1 = lqtL_isenum(L, 2, "Qt::ToolBarArea")?static_cast<Qt::ToolBarArea>(lqtL_toenum(L, 2, "Qt::ToolBarArea")):Qt::TopToolBarArea;
  __lua__obj->QMainWindow::addToolBarBreak(arg1);
  return 0;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__createPopupMenu (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QMenu * ret = __lua__obj->QMainWindow::createPopupMenu();
  lqtL_pushudata(L, ret, "QMenu*");
  return 1;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__insertToolBar (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QToolBar * arg1 = *static_cast<QToolBar**>(lqtL_checkudata(L, 2, "QToolBar*"));
  QToolBar * arg2 = *static_cast<QToolBar**>(lqtL_checkudata(L, 3, "QToolBar*"));
  __lua__obj->QMainWindow::insertToolBar(arg1, arg2);
  return 0;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__removeToolBarBreak (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QToolBar * arg1 = *static_cast<QToolBar**>(lqtL_checkudata(L, 2, "QToolBar*"));
  __lua__obj->QMainWindow::removeToolBarBreak(arg1);
  return 0;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__setAnimated (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QMainWindow::setAnimated(arg1);
  return 0;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QMainWindow::trUtf8(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QMainWindow::trUtf8(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__trUtf8 (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x12f67a0;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x12f6300;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x12f8590;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x12f7cc0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x12f8a20;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__trUtf8__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__trUtf8__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__trUtf8 matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__setUnifiedTitleAndToolBarOnMac (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QMainWindow::setUnifiedTitleAndToolBarOnMac(arg1);
  return 0;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QMainWindow::tr(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QMainWindow::tr(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__tr (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x12f5a40;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x12f57a0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x12f7480;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x12f7970;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x12f7d20;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__tr__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__tr__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__tr matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__menuWidget (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * ret = __lua__obj->QMainWindow::menuWidget();
  lqtL_pushudata(L, ret, "QWidget*");
  return 1;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__removeDockWidget (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QDockWidget * arg1 = *static_cast<QDockWidget**>(lqtL_checkudata(L, 2, "QDockWidget*"));
  __lua__obj->QMainWindow::removeDockWidget(arg1);
  return 0;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__setCentralWidget (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 2, "QWidget*"));
  __lua__obj->QMainWindow::setCentralWidget(arg1);
  return 0;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__setDockOptions (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<QMainWindow::DockOption> arg1 = **static_cast<QFlags<QMainWindow::DockOption>**>(lqtL_checkudata(L, 2, "QFlags<QMainWindow::DockOption>*"));
  __lua__obj->QMainWindow::setDockOptions(arg1);
  return 0;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__setStatusBar (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QStatusBar * arg1 = *static_cast<QStatusBar**>(lqtL_checkudata(L, 2, "QStatusBar*"));
  __lua__obj->QMainWindow::setStatusBar(arg1);
  return 0;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__insertToolBarBreak (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QToolBar * arg1 = *static_cast<QToolBar**>(lqtL_checkudata(L, 2, "QToolBar*"));
  __lua__obj->QMainWindow::insertToolBarBreak(arg1);
  return 0;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__setMenuWidget (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 2, "QWidget*"));
  __lua__obj->QMainWindow::setMenuWidget(arg1);
  return 0;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__setMenuBar (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QMenuBar * arg1 = *static_cast<QMenuBar**>(lqtL_checkudata(L, 2, "QMenuBar*"));
  __lua__obj->QMainWindow::setMenuBar(arg1);
  return 0;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__isAnimated (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QMainWindow::isAnimated();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__statusBar (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QStatusBar * ret = __lua__obj->QMainWindow::statusBar();
  lqtL_pushudata(L, ret, "QStatusBar*");
  return 1;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__menuBar (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QMenuBar * ret = __lua__obj->QMainWindow::menuBar();
  lqtL_pushudata(L, ret, "QMenuBar*");
  return 1;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__setDockNestingEnabled (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QMainWindow::setDockNestingEnabled(arg1);
  return 0;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__setIconSize (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QSize& arg1 = **static_cast<QSize**>(lqtL_checkudata(L, 2, "QSize*"));
  __lua__obj->QMainWindow::setIconSize(arg1);
  return 0;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__iconSize (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QSize ret = __lua__obj->QMainWindow::iconSize();
  lqtL_passudata(L, new QSize(ret), "QSize*");
  return 1;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__dockWidgetArea (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QDockWidget * arg1 = *static_cast<QDockWidget**>(lqtL_checkudata(L, 2, "QDockWidget*"));
  Qt::DockWidgetArea ret = __lua__obj->QMainWindow::dockWidgetArea(arg1);
  lqtL_pushenum(L, ret, "Qt::DockWidgetArea");
  return 1;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__metaObject (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QMetaObject * ret = __lua__obj->QMainWindow::metaObject();
  lqtL_pushudata(L, ret, "QMetaObject*");
  return 1;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__new (lua_State *L) {
  QWidget * arg1 = lqtL_testudata(L, 1, "QWidget*")?*static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*")):static_cast< QWidget * >(0);
  QFlags<Qt::WindowType> arg2 = lqtL_testudata(L, 2, "QFlags<Qt::WindowType>*")?**static_cast<QFlags<Qt::WindowType>**>(lqtL_checkudata(L, 2, "QFlags<Qt::WindowType>*")):static_cast< QFlags<Qt::WindowType> >(0);
  QMainWindow * ret = new LuaBinder< QMainWindow >(L, arg1, arg2);
  lqtL_passudata(L, ret, "QMainWindow*");
  return 1;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__unifiedTitleAndToolBarOnMac (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QMainWindow::unifiedTitleAndToolBarOnMac();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__delete (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  delete __lua__obj;
  __lua__obj = 0;
  return 0;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__setCorner (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::Corner arg1 = static_cast<Qt::Corner>(lqtL_toenum(L, 2, "Qt::Corner"));
  Qt::DockWidgetArea arg2 = static_cast<Qt::DockWidgetArea>(lqtL_toenum(L, 3, "Qt::DockWidgetArea"));
  __lua__obj->QMainWindow::setCorner(arg1, arg2);
  return 0;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__corner (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::Corner arg1 = static_cast<Qt::Corner>(lqtL_toenum(L, 2, "Qt::Corner"));
  Qt::DockWidgetArea ret = __lua__obj->QMainWindow::corner(arg1);
  lqtL_pushenum(L, ret, "Qt::DockWidgetArea");
  return 1;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__addToolBar__OverloadedVersion__1 (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::ToolBarArea arg1 = static_cast<Qt::ToolBarArea>(lqtL_toenum(L, 2, "Qt::ToolBarArea"));
  QToolBar * arg2 = *static_cast<QToolBar**>(lqtL_checkudata(L, 3, "QToolBar*"));
  __lua__obj->QMainWindow::addToolBar(arg1, arg2);
  return 0;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__addToolBar__OverloadedVersion__2 (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QToolBar * arg1 = *static_cast<QToolBar**>(lqtL_checkudata(L, 2, "QToolBar*"));
  __lua__obj->QMainWindow::addToolBar(arg1);
  return 0;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__addToolBar__OverloadedVersion__3 (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  QToolBar * ret = __lua__obj->QMainWindow::addToolBar(arg1);
  lqtL_pushudata(L, ret, "QToolBar*");
  return 1;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__addToolBar (lua_State *L) {
  int score[4];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QMainWindow*")?premium:-premium*premium;
  if (lqtL_isenum(L, 2, "Qt::ToolBarArea")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1308640;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_testudata(L, 3, "QToolBar*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1308030;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QMainWindow*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QToolBar*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x13094e0;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  score[3] += lqtL_testudata(L, 1, "QMainWindow*")?premium:-premium*premium;
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1309f30;
  } else {
    score[3] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=3;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__addToolBar__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__addToolBar__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__addToolBar__OverloadedVersion__3(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__addToolBar matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__removeToolBar (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QToolBar * arg1 = *static_cast<QToolBar**>(lqtL_checkudata(L, 2, "QToolBar*"));
  __lua__obj->QMainWindow::removeToolBar(arg1);
  return 0;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__dockOptions (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<QMainWindow::DockOption> ret = __lua__obj->QMainWindow::dockOptions();
  lqtL_passudata(L, new QFlags<QMainWindow::DockOption>(ret), "QFlags<QMainWindow::DockOption>*");
  return 1;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__restoreState (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QByteArray& arg1 = QByteArray(lua_tostring(L, 2), lua_objlen(L, 2));
  int arg2 = lua_isnumber(L, 3)?lua_tointeger(L, 3):static_cast< int >(0);
  bool ret = __lua__obj->QMainWindow::restoreState(arg1, arg2);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__toolButtonStyle (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::ToolButtonStyle ret = __lua__obj->QMainWindow::toolButtonStyle();
  lqtL_pushenum(L, ret, "Qt::ToolButtonStyle");
  return 1;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__tabifyDockWidget (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QDockWidget * arg1 = *static_cast<QDockWidget**>(lqtL_checkudata(L, 2, "QDockWidget*"));
  QDockWidget * arg2 = *static_cast<QDockWidget**>(lqtL_checkudata(L, 3, "QDockWidget*"));
  __lua__obj->QMainWindow::tabifyDockWidget(arg1, arg2);
  return 0;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__isDockNestingEnabled (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QMainWindow::isDockNestingEnabled();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__isSeparator (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPoint& arg1 = **static_cast<QPoint**>(lqtL_checkudata(L, 2, "QPoint*"));
  bool ret = __lua__obj->QMainWindow::isSeparator(arg1);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__addDockWidget__OverloadedVersion__1 (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::DockWidgetArea arg1 = static_cast<Qt::DockWidgetArea>(lqtL_toenum(L, 2, "Qt::DockWidgetArea"));
  QDockWidget * arg2 = *static_cast<QDockWidget**>(lqtL_checkudata(L, 3, "QDockWidget*"));
  __lua__obj->QMainWindow::addDockWidget(arg1, arg2);
  return 0;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__addDockWidget__OverloadedVersion__2 (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::DockWidgetArea arg1 = static_cast<Qt::DockWidgetArea>(lqtL_toenum(L, 2, "Qt::DockWidgetArea"));
  QDockWidget * arg2 = *static_cast<QDockWidget**>(lqtL_checkudata(L, 3, "QDockWidget*"));
  Qt::Orientation arg3 = static_cast<Qt::Orientation>(lqtL_toenum(L, 4, "Qt::Orientation"));
  __lua__obj->QMainWindow::addDockWidget(arg1, arg2, arg3);
  return 0;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__addDockWidget (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QMainWindow*")?premium:-premium*premium;
  if (lqtL_isenum(L, 2, "Qt::DockWidgetArea")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x130f820;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_testudata(L, 3, "QDockWidget*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x130f230;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QMainWindow*")?premium:-premium*premium;
  if (lqtL_isenum(L, 2, "Qt::DockWidgetArea")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x13107e0;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_testudata(L, 3, "QDockWidget*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1310d30;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_isenum(L, 4, "Qt::Orientation")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1310cd0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__addDockWidget__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__addDockWidget__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__addDockWidget matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__setToolButtonStyle (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::ToolButtonStyle arg1 = static_cast<Qt::ToolButtonStyle>(lqtL_toenum(L, 2, "Qt::ToolButtonStyle"));
  __lua__obj->QMainWindow::setToolButtonStyle(arg1);
  return 0;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__toolBarArea (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QToolBar * arg1 = *static_cast<QToolBar**>(lqtL_checkudata(L, 2, "QToolBar*"));
  Qt::ToolBarArea ret = __lua__obj->QMainWindow::toolBarArea(arg1);
  lqtL_pushenum(L, ret, "Qt::ToolBarArea");
  return 1;
}
int LuaBinder< QMainWindow >::__LuaWrapCall__centralWidget (lua_State *L) {
  QMainWindow *& __lua__obj = *static_cast<QMainWindow**>(lqtL_checkudata(L, 1, "QMainWindow*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * ret = __lua__obj->QMainWindow::centralWidget();
  lqtL_pushudata(L, ret, "QWidget*");
  return 1;
}
void LuaBinder< QMainWindow >::styleChange (QStyle& arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "styleChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QStyle*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::styleChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMainWindow >::focusInEvent (QFocusEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "focusInEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QFocusEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::focusInEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMainWindow >::keyPressEvent (QKeyEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "keyPressEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QKeyEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::keyPressEvent(arg1);
  }
  lua_settop(L, oldtop);
}
const QMetaObject * LuaBinder< QMainWindow >::metaObject () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "metaObject");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QMainWindow::metaObject();
  }
  const QMetaObject * ret = *static_cast<QMetaObject**>(lqtL_checkudata(L, -1, "QMetaObject*"));
  lua_settop(L, oldtop);
  return ret;
}
int LuaBinder< QMainWindow >::devType () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "devType");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::devType();
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QMainWindow >::setVisible (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "setVisible");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::setVisible(arg1);
  }
  lua_settop(L, oldtop);
}
QVariant LuaBinder< QMainWindow >::inputMethodQuery (Qt::InputMethodQuery arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "inputMethodQuery");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushenum(L, arg1, "Qt::InputMethodQuery");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::inputMethodQuery(arg1);
  }
  QVariant ret = **static_cast<QVariant**>(lqtL_checkudata(L, -1, "QVariant*"));
  lua_settop(L, oldtop);
  return ret;
}
QPaintEngine * LuaBinder< QMainWindow >::paintEngine () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "paintEngine");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::paintEngine();
  }
  QPaintEngine * ret = *static_cast<QPaintEngine**>(lqtL_checkudata(L, -1, "QPaintEngine*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QMainWindow >::dragLeaveEvent (QDragLeaveEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dragLeaveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDragLeaveEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::dragLeaveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMainWindow >::mousePressEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mousePressEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::mousePressEvent(arg1);
  }
  lua_settop(L, oldtop);
}
QSize LuaBinder< QMainWindow >::sizeHint () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "sizeHint");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::sizeHint();
  }
  QSize ret = **static_cast<QSize**>(lqtL_checkudata(L, -1, "QSize*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QMainWindow >::moveEvent (QMoveEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "moveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMoveEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::moveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMainWindow >::tabletEvent (QTabletEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "tabletEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QTabletEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::tabletEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMainWindow >::enterEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "enterEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::enterEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMainWindow >::closeEvent (QCloseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "closeEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QCloseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::closeEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMainWindow >::dropEvent (QDropEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dropEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDropEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::dropEvent(arg1);
  }
  lua_settop(L, oldtop);
}
QSize LuaBinder< QMainWindow >::minimumSizeHint () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "minimumSizeHint");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::minimumSizeHint();
  }
  QSize ret = **static_cast<QSize**>(lqtL_checkudata(L, -1, "QSize*"));
  lua_settop(L, oldtop);
  return ret;
}
QMenu * LuaBinder< QMainWindow >::createPopupMenu () {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "createPopupMenu");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QMainWindow::createPopupMenu();
  }
  QMenu * ret = *static_cast<QMenu**>(lqtL_checkudata(L, -1, "QMenu*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QMainWindow >::actionEvent (QActionEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "actionEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QActionEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::actionEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMainWindow >::showEvent (QShowEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "showEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QShowEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::showEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMainWindow >::dragEnterEvent (QDragEnterEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dragEnterEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDragEnterEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::dragEnterEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMainWindow >::paintEvent (QPaintEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "paintEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QPaintEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::paintEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMainWindow >::dragMoveEvent (QDragMoveEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dragMoveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDragMoveEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::dragMoveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMainWindow >::contextMenuEvent (QContextMenuEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "contextMenuEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QContextMenuEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QMainWindow::contextMenuEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMainWindow >::enabledChange (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "enabledChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::enabledChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMainWindow >::disconnectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "disconnectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::disconnectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QMainWindow >::heightForWidth (int arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "heightForWidth");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushinteger(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::heightForWidth(arg1);
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QMainWindow >::fontChange (const QFont& arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "fontChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QFont*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::fontChange(arg1);
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QMainWindow >::focusNextPrevChild (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "focusNextPrevChild");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::focusNextPrevChild(arg1);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QMainWindow >::timerEvent (QTimerEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "timerEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QTimerEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::timerEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMainWindow >::mouseMoveEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mouseMoveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::mouseMoveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMainWindow >::focusOutEvent (QFocusEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "focusOutEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QFocusEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::focusOutEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMainWindow >::childEvent (QChildEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "childEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QChildEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::childEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMainWindow >::connectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "connectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::connectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMainWindow >::mouseReleaseEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mouseReleaseEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::mouseReleaseEvent(arg1);
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QMainWindow >::metric (QPaintDevice::PaintDeviceMetric arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "metric");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushenum(L, arg1, "QPaintDevice::PaintDeviceMetric");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::metric(arg1);
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
bool LuaBinder< QMainWindow >::eventFilter (QObject * arg1, QEvent * arg2) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "eventFilter");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QObject*");
  lqtL_pushudata(L, arg2, "QEvent*");
  if (lua_isfunction(L, -2-2)) {
    lua_pcall(L, 2+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QObject::eventFilter(arg1, arg2);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QMainWindow >::inputMethodEvent (QInputMethodEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "inputMethodEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QInputMethodEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::inputMethodEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMainWindow >::wheelEvent (QWheelEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "wheelEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QWheelEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::wheelEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMainWindow >::resizeEvent (QResizeEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "resizeEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QResizeEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::resizeEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMainWindow >::paletteChange (const QPalette& arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "paletteChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QPalette*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::paletteChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMainWindow >::languageChange () {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "languageChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::languageChange();
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QMainWindow >::event (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "event");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QMainWindow::event(arg1);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QMainWindow >::hideEvent (QHideEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "hideEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QHideEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::hideEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMainWindow >::mouseDoubleClickEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mouseDoubleClickEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::mouseDoubleClickEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMainWindow >::keyReleaseEvent (QKeyEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "keyReleaseEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QKeyEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::keyReleaseEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMainWindow >::windowActivationChange (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "windowActivationChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::windowActivationChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMainWindow >::leaveEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "leaveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::leaveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMainWindow >::changeEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "changeEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::changeEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMainWindow >::customEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "customEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::customEvent(arg1);
  }
  lua_settop(L, oldtop);
}
LuaBinder< QMainWindow >::  ~LuaBinder< QMainWindow > () {
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMainWindow*");
  lua_getfield(L, -1, "~QMainWindow");

  if (lua_isfunction(L, -1)) {
    lua_insert(L, -2);
    lua_pcall(L, 1, 1, 0);
  } else {
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QMainWindow >::lqt_pushenum_DockOption (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "AnimatedDocks");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "AnimatedDocks");
  lua_pushstring(L, "AllowNestedDocks");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "AllowNestedDocks");
  lua_pushstring(L, "AllowTabbedDocks");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "AllowTabbedDocks");
  lua_pushstring(L, "ForceTabbedDocks");
  lua_rawseti(L, enum_table, 8);
  lua_pushinteger(L, 8);
  lua_setfield(L, enum_table, "ForceTabbedDocks");
  lua_pushstring(L, "VerticalTabs");
  lua_rawseti(L, enum_table, 16);
  lua_pushinteger(L, 16);
  lua_setfield(L, enum_table, "VerticalTabs");
  lua_pushcfunction(L, LuaBinder< QMainWindow >::lqt_pushenum_DockOption_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QMainWindow::DockOption");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QMainWindow >::lqt_pushenum_DockOption_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QMainWindow::DockOption>*) + sizeof(QFlags<QMainWindow::DockOption>));
  QFlags<QMainWindow::DockOption> *fl = static_cast<QFlags<QMainWindow::DockOption>*>( static_cast<void*>(&static_cast<QFlags<QMainWindow::DockOption>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QMainWindow::DockOption>(lqtL_toenum(L, i, "QMainWindow::DockOption"));
	}
	if (luaL_newmetatable(L, "QFlags<QMainWindow::DockOption>*")) {
		lua_pushstring(L, "QFlags<QMainWindow::DockOption>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int luaopen_QMainWindow (lua_State *L) {
  if (luaL_newmetatable(L, "QMainWindow*")) {
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__splitDockWidget);
    lua_setfield(L, -2, "splitDockWidget");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__toolBarBreak);
    lua_setfield(L, -2, "toolBarBreak");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__saveState);
    lua_setfield(L, -2, "saveState");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__addToolBarBreak);
    lua_setfield(L, -2, "addToolBarBreak");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__createPopupMenu);
    lua_setfield(L, -2, "createPopupMenu");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__insertToolBar);
    lua_setfield(L, -2, "insertToolBar");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__removeToolBarBreak);
    lua_setfield(L, -2, "removeToolBarBreak");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__setAnimated);
    lua_setfield(L, -2, "setAnimated");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__trUtf8);
    lua_setfield(L, -2, "trUtf8");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__setUnifiedTitleAndToolBarOnMac);
    lua_setfield(L, -2, "setUnifiedTitleAndToolBarOnMac");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__tr);
    lua_setfield(L, -2, "tr");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__menuWidget);
    lua_setfield(L, -2, "menuWidget");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__removeDockWidget);
    lua_setfield(L, -2, "removeDockWidget");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__setCentralWidget);
    lua_setfield(L, -2, "setCentralWidget");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__setDockOptions);
    lua_setfield(L, -2, "setDockOptions");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__setStatusBar);
    lua_setfield(L, -2, "setStatusBar");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__insertToolBarBreak);
    lua_setfield(L, -2, "insertToolBarBreak");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__setMenuWidget);
    lua_setfield(L, -2, "setMenuWidget");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__setMenuBar);
    lua_setfield(L, -2, "setMenuBar");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__isAnimated);
    lua_setfield(L, -2, "isAnimated");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__statusBar);
    lua_setfield(L, -2, "statusBar");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__menuBar);
    lua_setfield(L, -2, "menuBar");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__setDockNestingEnabled);
    lua_setfield(L, -2, "setDockNestingEnabled");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__setIconSize);
    lua_setfield(L, -2, "setIconSize");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__iconSize);
    lua_setfield(L, -2, "iconSize");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__dockWidgetArea);
    lua_setfield(L, -2, "dockWidgetArea");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__metaObject);
    lua_setfield(L, -2, "metaObject");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__new);
    lua_setfield(L, -2, "new");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__unifiedTitleAndToolBarOnMac);
    lua_setfield(L, -2, "unifiedTitleAndToolBarOnMac");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__delete);
    lua_setfield(L, -2, "delete");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__setCorner);
    lua_setfield(L, -2, "setCorner");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__corner);
    lua_setfield(L, -2, "corner");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__addToolBar);
    lua_setfield(L, -2, "addToolBar");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__removeToolBar);
    lua_setfield(L, -2, "removeToolBar");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__dockOptions);
    lua_setfield(L, -2, "dockOptions");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__restoreState);
    lua_setfield(L, -2, "restoreState");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__toolButtonStyle);
    lua_setfield(L, -2, "toolButtonStyle");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__tabifyDockWidget);
    lua_setfield(L, -2, "tabifyDockWidget");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__isDockNestingEnabled);
    lua_setfield(L, -2, "isDockNestingEnabled");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__isSeparator);
    lua_setfield(L, -2, "isSeparator");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__addDockWidget);
    lua_setfield(L, -2, "addDockWidget");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__setToolButtonStyle);
    lua_setfield(L, -2, "setToolButtonStyle");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__toolBarArea);
    lua_setfield(L, -2, "toolBarArea");
    lua_pushcfunction(L, LuaBinder< QMainWindow >::__LuaWrapCall__centralWidget);
    lua_setfield(L, -2, "centralWidget");
    LuaBinder< QMainWindow >::lqt_pushenum_DockOption(L);
    lua_setfield(L, -2, "DockOption");
    lua_newtable(L);
    lua_pushboolean(L, 1);
    lua_setfield(L, -2, "QWidget*");
    lua_pushboolean(L, 0);
    lua_setfield(L, -2, "QObject*");
    lua_pushboolean(L, 0);
    lua_setfield(L, -2, "QPaintDevice*");
    lua_setfield(L, -2, "__base");
    lua_pushcfunction(L, lqtL_newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, lqtL_index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, lqtL_gc);
    lua_setfield(L, -2, "__gc");
    lua_pushstring(L, "QMainWindow");
    lua_setfield(L, -2, "__qtype");
		lua_setglobal(L, "QMainWindow");
  } else {
		lua_pop(L, 1);
	}
  return 0;
}
