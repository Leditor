#include "lqt_common.hpp"
#include <QSize>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QSize > : public QSize {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__isValid (lua_State *L);
  static int __LuaWrapCall__boundedTo (lua_State *L);
  static int __LuaWrapCall__scale__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__scale__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__scale (lua_State *L);
  static int __LuaWrapCall__setHeight (lua_State *L);
  static int __LuaWrapCall__rheight (lua_State *L);
  static int __LuaWrapCall__width (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__new (lua_State *L);
  static int __LuaWrapCall__rwidth (lua_State *L);
  static int __LuaWrapCall__setWidth (lua_State *L);
  static int __LuaWrapCall__isNull (lua_State *L);
  static int __LuaWrapCall__height (lua_State *L);
  static int __LuaWrapCall__expandedTo (lua_State *L);
  static int __LuaWrapCall__isEmpty (lua_State *L);
  static int __LuaWrapCall__transpose (lua_State *L);
  LuaBinder< QSize > (lua_State *l):QSize(), L(l) {}
  LuaBinder< QSize > (lua_State *l, int arg1, int arg2):QSize(arg1, arg2), L(l) {}
};

extern "C" int luaopen_QSize (lua_State *L);
