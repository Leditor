#include "lqt_bind_QInputEvent.hpp"

int LuaBinder< QInputEvent >::__LuaWrapCall__delete (lua_State *L) {
  QInputEvent *& __lua__obj = *static_cast<QInputEvent**>(lqtL_checkudata(L, 1, "QInputEvent*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  delete __lua__obj;
  __lua__obj = 0;
  return 0;
}
int LuaBinder< QInputEvent >::__LuaWrapCall__modifiers (lua_State *L) {
  QInputEvent *& __lua__obj = *static_cast<QInputEvent**>(lqtL_checkudata(L, 1, "QInputEvent*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<Qt::KeyboardModifier> ret = __lua__obj->QInputEvent::modifiers();
  lqtL_passudata(L, new QFlags<Qt::KeyboardModifier>(ret), "QFlags<Qt::KeyboardModifier>*");
  return 1;
}
LuaBinder< QInputEvent >::  ~LuaBinder< QInputEvent > () {
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QInputEvent*");
  lua_getfield(L, -1, "~QInputEvent");

  if (lua_isfunction(L, -1)) {
    lua_insert(L, -2);
    lua_pcall(L, 1, 1, 0);
  } else {
  }
  lua_settop(L, oldtop);
}
int luaopen_QInputEvent (lua_State *L) {
  if (luaL_newmetatable(L, "QInputEvent*")) {
    lua_pushcfunction(L, LuaBinder< QInputEvent >::__LuaWrapCall__delete);
    lua_setfield(L, -2, "delete");
    lua_pushcfunction(L, LuaBinder< QInputEvent >::__LuaWrapCall__modifiers);
    lua_setfield(L, -2, "modifiers");
    lua_newtable(L);
    lua_pushboolean(L, 1);
    lua_setfield(L, -2, "QEvent*");
    lua_setfield(L, -2, "__base");
    lua_pushcfunction(L, lqtL_newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, lqtL_index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, lqtL_gc);
    lua_setfield(L, -2, "__gc");
    lua_pushstring(L, "QInputEvent");
    lua_setfield(L, -2, "__qtype");
		lua_setglobal(L, "QInputEvent");
  } else {
		lua_pop(L, 1);
	}
  return 0;
}
