#include "lqt_common.hpp"
#include <QDialog>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QDialog > : public QDialog {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__setModal (lua_State *L);
  static int __LuaWrapCall__setExtension (lua_State *L);
  static int __LuaWrapCall__sizeHint (lua_State *L);
  static int __LuaWrapCall__minimumSizeHint (lua_State *L);
  static int __LuaWrapCall__setVisible (lua_State *L);
  static int __LuaWrapCall__setOrientation (lua_State *L);
  static int __LuaWrapCall__isSizeGripEnabled (lua_State *L);
  static int __LuaWrapCall__metaObject (lua_State *L);
  static int __LuaWrapCall__new (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__tr (lua_State *L);
  static int __LuaWrapCall__delete (lua_State *L);
  static int __LuaWrapCall__extension (lua_State *L);
  static int __LuaWrapCall__accept (lua_State *L);
  static int __LuaWrapCall__done (lua_State *L);
  static int __LuaWrapCall__result (lua_State *L);
  static int __LuaWrapCall__reject (lua_State *L);
  static int __LuaWrapCall__setSizeGripEnabled (lua_State *L);
  static int __LuaWrapCall__setResult (lua_State *L);
  static int __LuaWrapCall__orientation (lua_State *L);
  static int __LuaWrapCall__exec (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__trUtf8 (lua_State *L);
  static int __LuaWrapCall__showExtension (lua_State *L);
protected:
  void styleChange (QStyle& arg1);
public:
protected:
  void focusInEvent (QFocusEvent * arg1);
public:
protected:
  void keyPressEvent (QKeyEvent * arg1);
public:
  const QMetaObject * metaObject () const;
  int devType () const;
  void setVisible (bool arg1);
  QVariant inputMethodQuery (Qt::InputMethodQuery arg1) const;
  QPaintEngine * paintEngine () const;
protected:
  void dragLeaveEvent (QDragLeaveEvent * arg1);
public:
protected:
  void mousePressEvent (QMouseEvent * arg1);
public:
  QSize sizeHint () const;
protected:
  void moveEvent (QMoveEvent * arg1);
public:
protected:
  void enterEvent (QEvent * arg1);
public:
  void accept ();
protected:
  void tabletEvent (QTabletEvent * arg1);
public:
protected:
  void closeEvent (QCloseEvent * arg1);
public:
protected:
  void actionEvent (QActionEvent * arg1);
public:
protected:
  void dragEnterEvent (QDragEnterEvent * arg1);
public:
  QSize minimumSizeHint () const;
protected:
  void dragMoveEvent (QDragMoveEvent * arg1);
public:
protected:
  void dropEvent (QDropEvent * arg1);
public:
protected:
  void showEvent (QShowEvent * arg1);
public:
protected:
  void contextMenuEvent (QContextMenuEvent * arg1);
public:
protected:
  void paintEvent (QPaintEvent * arg1);
public:
protected:
  void windowActivationChange (bool arg1);
public:
protected:
  void fontChange (const QFont& arg1);
public:
protected:
  void enabledChange (bool arg1);
public:
protected:
  void disconnectNotify (const char * arg1);
public:
  int heightForWidth (int arg1) const;
protected:
  void mouseDoubleClickEvent (QMouseEvent * arg1);
public:
protected:
  bool focusNextPrevChild (bool arg1);
public:
protected:
  void timerEvent (QTimerEvent * arg1);
public:
protected:
  void mouseMoveEvent (QMouseEvent * arg1);
public:
protected:
  int metric (QPaintDevice::PaintDeviceMetric arg1) const;
public:
protected:
  void childEvent (QChildEvent * arg1);
public:
protected:
  void connectNotify (const char * arg1);
public:
protected:
  void mouseReleaseEvent (QMouseEvent * arg1);
public:
protected:
  void focusOutEvent (QFocusEvent * arg1);
public:
protected:
  bool eventFilter (QObject * arg1, QEvent * arg2);
public:
protected:
  void inputMethodEvent (QInputMethodEvent * arg1);
public:
protected:
  void wheelEvent (QWheelEvent * arg1);
public:
  void done (int arg1);
protected:
  void paletteChange (const QPalette& arg1);
public:
protected:
  void languageChange ();
public:
protected:
  bool event (QEvent * arg1);
public:
protected:
  void hideEvent (QHideEvent * arg1);
public:
protected:
  void keyReleaseEvent (QKeyEvent * arg1);
public:
protected:
  void changeEvent (QEvent * arg1);
public:
protected:
  void resizeEvent (QResizeEvent * arg1);
public:
protected:
  void leaveEvent (QEvent * arg1);
public:
protected:
  void customEvent (QEvent * arg1);
public:
  void reject ();
  ~LuaBinder< QDialog > ();
  static int lqt_pushenum_DialogCode (lua_State *L);
  static int lqt_pushenum_DialogCode_QFLAGS_CREATOR (lua_State *L);
  LuaBinder< QDialog > (lua_State *l, QWidget * arg1, QFlags<Qt::WindowType> arg2):QDialog(arg1, arg2), L(l) {}
};

extern "C" int luaopen_QDialog (lua_State *L);
