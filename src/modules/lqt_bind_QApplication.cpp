#include "lqt_bind_QApplication.hpp"

int LuaBinder< QApplication >::__LuaWrapCall__setWheelScrollLines (lua_State *L) {
  int arg1 = lua_tointeger(L, 1);
  QApplication::setWheelScrollLines(arg1);
  return 0;
}
int LuaBinder< QApplication >::__LuaWrapCall__styleSheet (lua_State *L) {
  QApplication *& __lua__obj = *static_cast<QApplication**>(lqtL_checkudata(L, 1, "QApplication*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QApplication::styleSheet();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__colorSpec (lua_State *L) {
  int ret = QApplication::colorSpec();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__doubleClickInterval (lua_State *L) {
  int ret = QApplication::doubleClickInterval();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__activePopupWidget (lua_State *L) {
  QWidget * ret = QApplication::activePopupWidget();
  lqtL_pushudata(L, ret, "QWidget*");
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__setStyle__OverloadedVersion__1 (lua_State *L) {
  QStyle * arg1 = *static_cast<QStyle**>(lqtL_checkudata(L, 1, "QStyle*"));
  QApplication::setStyle(arg1);
  return 0;
}
int LuaBinder< QApplication >::__LuaWrapCall__setStyle__OverloadedVersion__2 (lua_State *L) {
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 1), lua_objlen(L, 1));
  QStyle * ret = QApplication::setStyle(arg1);
  lqtL_pushudata(L, ret, "QStyle*");
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__setStyle (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if (lqtL_testudata(L, 1, "QStyle*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1c7ddb0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1c7e790;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__setStyle__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__setStyle__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__setStyle matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QApplication >::__LuaWrapCall__keyboardInputDirection (lua_State *L) {
  Qt::LayoutDirection ret = QApplication::keyboardInputDirection();
  lqtL_pushenum(L, ret, "Qt::LayoutDirection");
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QApplication::tr(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QApplication::tr(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__tr (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1c70850;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x1c705b0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1c72290;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1c72780;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1c72b30;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__tr__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__tr__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__tr matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QApplication >::__LuaWrapCall__sessionId (lua_State *L) {
  QApplication *& __lua__obj = *static_cast<QApplication**>(lqtL_checkudata(L, 1, "QApplication*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QApplication::sessionId();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__palette__OverloadedVersion__1 (lua_State *L) {
  QPalette ret = QApplication::palette();
  lqtL_passudata(L, new QPalette(ret), "QPalette*");
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__palette__OverloadedVersion__2 (lua_State *L) {
  const QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
  QPalette ret = QApplication::palette(arg1);
  lqtL_passudata(L, new QPalette(ret), "QPalette*");
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__palette__OverloadedVersion__3 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  QPalette ret = QApplication::palette(arg1);
  lqtL_passudata(L, new QPalette(ret), "QPalette*");
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__palette (lua_State *L) {
  int score[4];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[2] = 0;
  if (lqtL_testudata(L, 1, "QWidget*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1c82e40;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1c837f0;
  } else {
    score[3] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=3;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__palette__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__palette__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__palette__OverloadedVersion__3(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__palette matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QApplication >::__LuaWrapCall__setStartDragDistance (lua_State *L) {
  int arg1 = lua_tointeger(L, 1);
  QApplication::setStartDragDistance(arg1);
  return 0;
}
int LuaBinder< QApplication >::__LuaWrapCall__style (lua_State *L) {
  QStyle * ret = QApplication::style();
  lqtL_pushudata(L, ret, "QStyle*");
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__setGlobalStrut (lua_State *L) {
  const QSize& arg1 = **static_cast<QSize**>(lqtL_checkudata(L, 1, "QSize*"));
  QApplication::setGlobalStrut(arg1);
  return 0;
}
int LuaBinder< QApplication >::__LuaWrapCall__fontMetrics (lua_State *L) {
  QFontMetrics ret = QApplication::fontMetrics();
  lqtL_passudata(L, new QFontMetrics(ret), "QFontMetrics*");
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__setQuitOnLastWindowClosed (lua_State *L) {
  bool arg1 = (bool)lua_toboolean(L, 1);
  QApplication::setQuitOnLastWindowClosed(arg1);
  return 0;
}
int LuaBinder< QApplication >::__LuaWrapCall__keyboardInputInterval (lua_State *L) {
  int ret = QApplication::keyboardInputInterval();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__beep (lua_State *L) {
  QApplication::beep();
  return 0;
}
int LuaBinder< QApplication >::__LuaWrapCall__aboutQt (lua_State *L) {
  QApplication::aboutQt();
  return 0;
}
int LuaBinder< QApplication >::__LuaWrapCall__startDragTime (lua_State *L) {
  int ret = QApplication::startDragTime();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__metaObject (lua_State *L) {
  QApplication *& __lua__obj = *static_cast<QApplication**>(lqtL_checkudata(L, 1, "QApplication*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QMetaObject * ret = __lua__obj->QApplication::metaObject();
  lqtL_pushudata(L, ret, "QMetaObject*");
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__delete (lua_State *L) {
	//return 0;
  QApplication *& __lua__obj = *static_cast<QApplication**>(lqtL_checkudata(L, 1, "QApplication*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  delete __lua__obj;
  __lua__obj = 0;
  return 0;
}
int LuaBinder< QApplication >::__LuaWrapCall__mouseButtons (lua_State *L) {
  QFlags<Qt::MouseButton> ret = QApplication::mouseButtons();
  lqtL_passudata(L, new QFlags<Qt::MouseButton>(ret), "QFlags<Qt::MouseButton>*");
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__keyboardModifiers (lua_State *L) {
  QFlags<Qt::KeyboardModifier> ret = QApplication::keyboardModifiers();
  lqtL_passudata(L, new QFlags<Qt::KeyboardModifier>(ret), "QFlags<Qt::KeyboardModifier>*");
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__setOverrideCursor (lua_State *L) {
  const QCursor& arg1 = **static_cast<QCursor**>(lqtL_checkudata(L, 1, "QCursor*"));
  QApplication::setOverrideCursor(arg1);
  return 0;
}
int LuaBinder< QApplication >::__LuaWrapCall__focusWidget (lua_State *L) {
  QWidget * ret = QApplication::focusWidget();
  lqtL_pushudata(L, ret, "QWidget*");
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__overrideCursor (lua_State *L) {
  QCursor * ret = QApplication::overrideCursor();
  lqtL_pushudata(L, ret, "QCursor*");
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__exec (lua_State *L) {
  int ret = QApplication::exec();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__setEffectEnabled (lua_State *L) {
  Qt::UIEffect arg1 = static_cast<Qt::UIEffect>(lqtL_toenum(L, 1, "Qt::UIEffect"));
  bool arg2 = lua_isboolean(L, 2)?(bool)lua_toboolean(L, 2):true;
  QApplication::setEffectEnabled(arg1, arg2);
  return 0;
}
int LuaBinder< QApplication >::__LuaWrapCall__saveState (lua_State *L) {
  QApplication *& __lua__obj = *static_cast<QApplication**>(lqtL_checkudata(L, 1, "QApplication*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QSessionManager& arg1 = **static_cast<QSessionManager**>(lqtL_checkudata(L, 2, "QSessionManager*"));
  __lua__obj->QApplication::saveState(arg1);
  return 0;
}
int LuaBinder< QApplication >::__LuaWrapCall__setDesktopSettingsAware (lua_State *L) {
  bool arg1 = (bool)lua_toboolean(L, 1);
  QApplication::setDesktopSettingsAware(arg1);
  return 0;
}
int LuaBinder< QApplication >::__LuaWrapCall__sessionKey (lua_State *L) {
  QApplication *& __lua__obj = *static_cast<QApplication**>(lqtL_checkudata(L, 1, "QApplication*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QApplication::sessionKey();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QApplication::trUtf8(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QApplication::trUtf8(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__trUtf8 (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1c715b0;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x1c71110;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1c733b0;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1c72ad0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1c73830;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__trUtf8__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__trUtf8__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__trUtf8 matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QApplication >::__LuaWrapCall__topLevelWidgets (lua_State *L) {
  QList<QWidget*> ret = QApplication::topLevelWidgets();
  lqtL_passudata(L, new QList<QWidget*>(ret), "QList<QWidget*>*");
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__desktop (lua_State *L) {
  QDesktopWidget * ret = QApplication::desktop();
  lqtL_pushudata(L, ret, "QDesktopWidget*");
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__quitOnLastWindowClosed (lua_State *L) {
  bool ret = QApplication::quitOnLastWindowClosed();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__setPalette (lua_State *L) {
  const QPalette& arg1 = **static_cast<QPalette**>(lqtL_checkudata(L, 1, "QPalette*"));
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QApplication::setPalette(arg1, arg2);
  return 0;
}
int LuaBinder< QApplication >::__LuaWrapCall__isLeftToRight (lua_State *L) {
  bool ret = QApplication::isLeftToRight();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__layoutDirection (lua_State *L) {
  Qt::LayoutDirection ret = QApplication::layoutDirection();
  lqtL_pushenum(L, ret, "Qt::LayoutDirection");
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__keyboardInputLocale (lua_State *L) {
  QLocale ret = QApplication::keyboardInputLocale();
  lqtL_passudata(L, new QLocale(ret), "QLocale*");
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__new__OverloadedVersion__1 (lua_State *L) {
  int& arg1 = lqtL_tointref(L, 1);
  char * * arg2 = lqtL_toarguments(L, 2);
  int arg3 = lua_isnumber(L, 3)?lua_tointeger(L, 3):static_cast< int >(262913);
  QApplication * ret = new LuaBinder< QApplication >(L, arg1, arg2, arg3);
  lqtL_passudata(L, ret, "QApplication*");
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__new__OverloadedVersion__2 (lua_State *L) {
  int& arg1 = lqtL_tointref(L, 1);
  char * * arg2 = lqtL_toarguments(L, 2);
  bool arg3 = (bool)lua_toboolean(L, 3);
  int arg4 = lua_isnumber(L, 4)?lua_tointeger(L, 4):static_cast< int >(262913);
  QApplication * ret = new LuaBinder< QApplication >(L, arg1, arg2, arg3, arg4);
  lqtL_passudata(L, ret, "QApplication*");
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__new__OverloadedVersion__3 (lua_State *L) {
  int& arg1 = lqtL_tointref(L, 1);
  char * * arg2 = lqtL_toarguments(L, 2);
  QApplication::Type arg3 = static_cast<QApplication::Type>(lqtL_toenum(L, 3, "QApplication::Type"));
  int arg4 = lua_isnumber(L, 4)?lua_tointeger(L, 4):static_cast< int >(262913);
  QApplication * ret = new LuaBinder< QApplication >(L, arg1, arg2, arg3, arg4);
  lqtL_passudata(L, ret, "QApplication*");
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__new__OverloadedVersion__4 (lua_State *L) {
  _XDisplay * arg1 = *static_cast<_XDisplay**>(lqtL_checkudata(L, 1, "_XDisplay*"));
  long unsigned int arg2 = lua_isnumber(L, 2)?lua_tointeger(L, 2):static_cast< long unsigned int >(0);
  long unsigned int arg3 = lua_isnumber(L, 3)?lua_tointeger(L, 3):static_cast< long unsigned int >(0);
  int arg4 = lua_isnumber(L, 4)?lua_tointeger(L, 4):static_cast< int >(262913);
  QApplication * ret = new LuaBinder< QApplication >(L, arg1, arg2, arg3, arg4);
  lqtL_passudata(L, ret, "QApplication*");
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__new__OverloadedVersion__5 (lua_State *L) {
  _XDisplay * arg1 = *static_cast<_XDisplay**>(lqtL_checkudata(L, 1, "_XDisplay*"));
  int& arg2 = lqtL_tointref(L, 2);
  char * * arg3 = lqtL_toarguments(L, 3);
  long unsigned int arg4 = lua_isnumber(L, 4)?lua_tointeger(L, 4):static_cast< long unsigned int >(0);
  long unsigned int arg5 = lua_isnumber(L, 5)?lua_tointeger(L, 5):static_cast< long unsigned int >(0);
  int arg6 = lua_isnumber(L, 6)?lua_tointeger(L, 6):static_cast< int >(262913);
  QApplication * ret = new LuaBinder< QApplication >(L, arg1, arg2, arg3, arg4, arg5, arg6);
  lqtL_passudata(L, ret, "QApplication*");
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__new (lua_State *L) {
  int score[6];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if (lua_isnumber(L, 1)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1c754e0;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_testarguments(L, 2)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1c751a0;
  } else {
    score[1] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x1c75990;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if (lua_isnumber(L, 1)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1c76790;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_testarguments(L, 2)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1c76480;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isboolean(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1c76c40;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x1c77510;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  if (lua_isnumber(L, 1)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1c77de0;
  } else {
    score[3] -= premium*premium;
  }
  if (lqtL_testarguments(L, 2)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1c77b00;
  } else {
    score[3] -= premium*premium;
  }
  if (lqtL_isenum(L, 3, "QApplication::Type")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1c786b0;
  } else {
    score[3] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[3] += premium;
  } else if (true) {
    score[3] += premium-1; // table: 0x1c78650;
  } else {
    score[3] -= premium*premium;
  }
  score[4] = 0;
  if (lqtL_testudata(L, 1, "_XDisplay*")) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x1c79350;
  } else {
    score[4] -= premium*premium;
  }
  if (lua_isnumber(L, 2)) {
    score[4] += premium;
  } else if (true) {
    score[4] += premium-1; // table: 0x1c79060;
  } else {
    score[4] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[4] += premium;
  } else if (true) {
    score[4] += premium-1; // table: 0x1c79890;
  } else {
    score[4] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[4] += premium;
  } else if (true) {
    score[4] += premium-1; // table: 0x1c7a260;
  } else {
    score[4] -= premium*premium;
  }
  score[5] = 0;
  if (lqtL_testudata(L, 1, "_XDisplay*")) {
    score[5] += premium;
  } else if (false) {
    score[5] += premium-1; // table: 0x1c7ab50;
  } else {
    score[5] -= premium*premium;
  }
  if (lua_isnumber(L, 2)) {
    score[5] += premium;
  } else if (false) {
    score[5] += premium-1; // table: 0x1c7a850;
  } else {
    score[5] -= premium*premium;
  }
  if (lqtL_testarguments(L, 3)) {
    score[5] += premium;
  } else if (false) {
    score[5] += premium-1; // table: 0x1c7b000;
  } else {
    score[5] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[5] += premium;
  } else if (true) {
    score[5] += premium-1; // table: 0x1c7b860;
  } else {
    score[5] -= premium*premium;
  }
  if (lua_isnumber(L, 5)) {
    score[5] += premium;
  } else if (true) {
    score[5] += premium-1; // table: 0x1c7bc20;
  } else {
    score[5] -= premium*premium;
  }
  if (lua_isnumber(L, 6)) {
    score[5] += premium;
  } else if (true) {
    score[5] += premium-1; // table: 0x1c7b7b0;
  } else {
    score[5] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=5;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__new__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__new__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__new__OverloadedVersion__3(L); break;
    case 4: return __LuaWrapCall__new__OverloadedVersion__4(L); break;
    case 5: return __LuaWrapCall__new__OverloadedVersion__5(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__new matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QApplication >::__LuaWrapCall__wheelScrollLines (lua_State *L) {
  int ret = QApplication::wheelScrollLines();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__type (lua_State *L) {
  QApplication::Type ret = QApplication::type();
  lqtL_pushenum(L, ret, "QApplication::Type");
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__syncX (lua_State *L) {
  QApplication::syncX();
  return 0;
}
int LuaBinder< QApplication >::__LuaWrapCall__setDoubleClickInterval (lua_State *L) {
  int arg1 = lua_tointeger(L, 1);
  QApplication::setDoubleClickInterval(arg1);
  return 0;
}
int LuaBinder< QApplication >::__LuaWrapCall__setColorSpec (lua_State *L) {
  int arg1 = lua_tointeger(L, 1);
  QApplication::setColorSpec(arg1);
  return 0;
}
int LuaBinder< QApplication >::__LuaWrapCall__windowIcon (lua_State *L) {
  QIcon ret = QApplication::windowIcon();
  lqtL_passudata(L, new QIcon(ret), "QIcon*");
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__restoreOverrideCursor (lua_State *L) {
  QApplication::restoreOverrideCursor();
  return 0;
}
int LuaBinder< QApplication >::__LuaWrapCall__setKeyboardInputInterval (lua_State *L) {
  int arg1 = lua_tointeger(L, 1);
  QApplication::setKeyboardInputInterval(arg1);
  return 0;
}
int LuaBinder< QApplication >::__LuaWrapCall__setActiveWindow (lua_State *L) {
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
  QApplication::setActiveWindow(arg1);
  return 0;
}
int LuaBinder< QApplication >::__LuaWrapCall__cursorFlashTime (lua_State *L) {
  int ret = QApplication::cursorFlashTime();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__globalStrut (lua_State *L) {
  QSize ret = QApplication::globalStrut();
  lqtL_passudata(L, new QSize(ret), "QSize*");
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__font__OverloadedVersion__1 (lua_State *L) {
  QFont ret = QApplication::font();
  lqtL_passudata(L, new QFont(ret), "QFont*");
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__font__OverloadedVersion__2 (lua_State *L) {
  const QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
  QFont ret = QApplication::font(arg1);
  lqtL_passudata(L, new QFont(ret), "QFont*");
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__font__OverloadedVersion__3 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  QFont ret = QApplication::font(arg1);
  lqtL_passudata(L, new QFont(ret), "QFont*");
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__font (lua_State *L) {
  int score[4];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[2] = 0;
  if (lqtL_testudata(L, 1, "QWidget*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1c857e0;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1c86180;
  } else {
    score[3] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=3;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__font__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__font__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__font__OverloadedVersion__3(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__font matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QApplication >::__LuaWrapCall__desktopSettingsAware (lua_State *L) {
  bool ret = QApplication::desktopSettingsAware();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__topLevelAt__OverloadedVersion__1 (lua_State *L) {
  const QPoint& arg1 = **static_cast<QPoint**>(lqtL_checkudata(L, 1, "QPoint*"));
  QWidget * ret = QApplication::topLevelAt(arg1);
  lqtL_pushudata(L, ret, "QWidget*");
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__topLevelAt__OverloadedVersion__2 (lua_State *L) {
  int arg1 = lua_tointeger(L, 1);
  int arg2 = lua_tointeger(L, 2);
  QWidget * ret = QApplication::topLevelAt(arg1, arg2);
  lqtL_pushudata(L, ret, "QWidget*");
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__topLevelAt (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if (lqtL_testudata(L, 1, "QPoint*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1c8f390;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if (lua_isnumber(L, 1)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1c8fde0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 2)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1c90310;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__topLevelAt__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__topLevelAt__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__topLevelAt matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QApplication >::__LuaWrapCall__setCursorFlashTime (lua_State *L) {
  int arg1 = lua_tointeger(L, 1);
  QApplication::setCursorFlashTime(arg1);
  return 0;
}
int LuaBinder< QApplication >::__LuaWrapCall__setInputContext (lua_State *L) {
  QApplication *& __lua__obj = *static_cast<QApplication**>(lqtL_checkudata(L, 1, "QApplication*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QInputContext * arg1 = *static_cast<QInputContext**>(lqtL_checkudata(L, 2, "QInputContext*"));
  __lua__obj->QApplication::setInputContext(arg1);
  return 0;
}
int LuaBinder< QApplication >::__LuaWrapCall__startDragDistance (lua_State *L) {
  int ret = QApplication::startDragDistance();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__clipboard (lua_State *L) {
  QClipboard * ret = QApplication::clipboard();
  lqtL_pushudata(L, ret, "QClipboard*");
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__activeWindow (lua_State *L) {
  QWidget * ret = QApplication::activeWindow();
  lqtL_pushudata(L, ret, "QWidget*");
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__changeOverrideCursor (lua_State *L) {
  const QCursor& arg1 = **static_cast<QCursor**>(lqtL_checkudata(L, 1, "QCursor*"));
  QApplication::changeOverrideCursor(arg1);
  return 0;
}
int LuaBinder< QApplication >::__LuaWrapCall__isSessionRestored (lua_State *L) {
  QApplication *& __lua__obj = *static_cast<QApplication**>(lqtL_checkudata(L, 1, "QApplication*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QApplication::isSessionRestored();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__activeModalWidget (lua_State *L) {
  QWidget * ret = QApplication::activeModalWidget();
  lqtL_pushudata(L, ret, "QWidget*");
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__isEffectEnabled (lua_State *L) {
  Qt::UIEffect arg1 = static_cast<Qt::UIEffect>(lqtL_toenum(L, 1, "Qt::UIEffect"));
  bool ret = QApplication::isEffectEnabled(arg1);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__inputContext (lua_State *L) {
  QApplication *& __lua__obj = *static_cast<QApplication**>(lqtL_checkudata(L, 1, "QApplication*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QInputContext * ret = __lua__obj->QApplication::inputContext();
  lqtL_pushudata(L, ret, "QInputContext*");
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__widgetAt__OverloadedVersion__1 (lua_State *L) {
  const QPoint& arg1 = **static_cast<QPoint**>(lqtL_checkudata(L, 1, "QPoint*"));
  QWidget * ret = QApplication::widgetAt(arg1);
  lqtL_pushudata(L, ret, "QWidget*");
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__widgetAt__OverloadedVersion__2 (lua_State *L) {
  int arg1 = lua_tointeger(L, 1);
  int arg2 = lua_tointeger(L, 2);
  QWidget * ret = QApplication::widgetAt(arg1, arg2);
  lqtL_pushudata(L, ret, "QWidget*");
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__widgetAt (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if (lqtL_testudata(L, 1, "QPoint*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1c8daa0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if (lua_isnumber(L, 1)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1c8e4e0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 2)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1c8e9f0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__widgetAt__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__widgetAt__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__widgetAt matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QApplication >::__LuaWrapCall__setFont (lua_State *L) {
  const QFont& arg1 = **static_cast<QFont**>(lqtL_checkudata(L, 1, "QFont*"));
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QApplication::setFont(arg1, arg2);
  return 0;
}
int LuaBinder< QApplication >::__LuaWrapCall__setStartDragTime (lua_State *L) {
  int arg1 = lua_tointeger(L, 1);
  QApplication::setStartDragTime(arg1);
  return 0;
}
int LuaBinder< QApplication >::__LuaWrapCall__allWidgets (lua_State *L) {
  QList<QWidget*> ret = QApplication::allWidgets();
  lqtL_passudata(L, new QList<QWidget*>(ret), "QList<QWidget*>*");
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__setWindowIcon (lua_State *L) {
  const QIcon& arg1 = **static_cast<QIcon**>(lqtL_checkudata(L, 1, "QIcon*"));
  QApplication::setWindowIcon(arg1);
  return 0;
}
int LuaBinder< QApplication >::__LuaWrapCall__commitData (lua_State *L) {
  QApplication *& __lua__obj = *static_cast<QApplication**>(lqtL_checkudata(L, 1, "QApplication*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QSessionManager& arg1 = **static_cast<QSessionManager**>(lqtL_checkudata(L, 2, "QSessionManager*"));
  __lua__obj->QApplication::commitData(arg1);
  return 0;
}
int LuaBinder< QApplication >::__LuaWrapCall__isRightToLeft (lua_State *L) {
  bool ret = QApplication::isRightToLeft();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__notify (lua_State *L) {
  QApplication *& __lua__obj = *static_cast<QApplication**>(lqtL_checkudata(L, 1, "QApplication*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QObject * arg1 = *static_cast<QObject**>(lqtL_checkudata(L, 2, "QObject*"));
  QEvent * arg2 = *static_cast<QEvent**>(lqtL_checkudata(L, 3, "QEvent*"));
  bool ret = __lua__obj->QApplication::notify(arg1, arg2);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QApplication >::__LuaWrapCall__setLayoutDirection (lua_State *L) {
  Qt::LayoutDirection arg1 = static_cast<Qt::LayoutDirection>(lqtL_toenum(L, 1, "Qt::LayoutDirection"));
  QApplication::setLayoutDirection(arg1);
  return 0;
}
int LuaBinder< QApplication >::__LuaWrapCall__closeAllWindows (lua_State *L) {
  QApplication::closeAllWindows();
  return 0;
}
int LuaBinder< QApplication >::__LuaWrapCall__alert (lua_State *L) {
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
  int arg2 = lua_isnumber(L, 2)?lua_tointeger(L, 2):static_cast< int >(0);
  QApplication::alert(arg1, arg2);
  return 0;
}
int LuaBinder< QApplication >::__LuaWrapCall__setStyleSheet (lua_State *L) {
  QApplication *& __lua__obj = *static_cast<QApplication**>(lqtL_checkudata(L, 1, "QApplication*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QApplication::setStyleSheet(arg1);
  return 0;
}
void LuaBinder< QApplication >::commitData (QSessionManager& arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QApplication*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "commitData");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QSessionManager*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QApplication::commitData(arg1);
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QApplication >::notify (QObject * arg1, QEvent * arg2) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QApplication*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "notify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QObject*");
  lqtL_pushudata(L, arg2, "QEvent*");
  if (lua_isfunction(L, -2-2)) {
    lua_pcall(L, 2+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QApplication::notify(arg1, arg2);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
bool LuaBinder< QApplication >::eventFilter (QObject * arg1, QEvent * arg2) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QApplication*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "eventFilter");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QObject*");
  lqtL_pushudata(L, arg2, "QEvent*");
  if (lua_isfunction(L, -2-2)) {
    lua_pcall(L, 2+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QObject::eventFilter(arg1, arg2);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
const QMetaObject * LuaBinder< QApplication >::metaObject () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QApplication*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "metaObject");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QApplication::metaObject();
  }
  const QMetaObject * ret = *static_cast<QMetaObject**>(lqtL_checkudata(L, -1, "QMetaObject*"));
  lua_settop(L, oldtop);
  return ret;
}
bool LuaBinder< QApplication >::event (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QApplication*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "event");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QApplication::event(arg1);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QApplication >::connectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QApplication*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "connectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::connectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QApplication >::saveState (QSessionManager& arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QApplication*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "saveState");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QSessionManager*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QApplication::saveState(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QApplication >::timerEvent (QTimerEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QApplication*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "timerEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QTimerEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::timerEvent(arg1);
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QApplication >::compressEvent (QEvent * arg1, QObject * arg2, QPostEventList * arg3) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QApplication*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "compressEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  lqtL_pushudata(L, arg2, "QObject*");
  lqtL_pushudata(L, arg3, "QPostEventList*");
  if (lua_isfunction(L, -3-2)) {
    lua_pcall(L, 3+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QApplication::compressEvent(arg1, arg2, arg3);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QApplication >::disconnectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QApplication*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "disconnectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::disconnectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QApplication >::childEvent (QChildEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QApplication*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "childEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QChildEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::childEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QApplication >::customEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QApplication*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "customEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::customEvent(arg1);
  }
  lua_settop(L, oldtop);
}
LuaBinder< QApplication >::  ~LuaBinder< QApplication > () {
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QApplication*");
  lua_getfield(L, -1, "~QApplication");

  if (lua_isfunction(L, -1)) {
    lua_insert(L, -2);
    lua_pcall(L, 1, 1, 0);
  } else {
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QApplication >::lqt_pushenum_Type (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "Tty");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "Tty");
  lua_pushstring(L, "GuiClient");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "GuiClient");
  lua_pushstring(L, "GuiServer");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "GuiServer");
  lua_pushcfunction(L, LuaBinder< QApplication >::lqt_pushenum_Type_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QApplication::Type");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QApplication >::lqt_pushenum_Type_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QApplication::Type>*) + sizeof(QFlags<QApplication::Type>));
  QFlags<QApplication::Type> *fl = static_cast<QFlags<QApplication::Type>*>( static_cast<void*>(&static_cast<QFlags<QApplication::Type>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QApplication::Type>(lqtL_toenum(L, i, "QApplication::Type"));
	}
	if (luaL_newmetatable(L, "QFlags<QApplication::Type>*")) {
		lua_pushstring(L, "QFlags<QApplication::Type>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinder< QApplication >::lqt_pushenum_ColorSpec (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "NormalColor");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "NormalColor");
  lua_pushstring(L, "CustomColor");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "CustomColor");
  lua_pushstring(L, "ManyColor");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "ManyColor");
  lua_pushcfunction(L, LuaBinder< QApplication >::lqt_pushenum_ColorSpec_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QApplication::ColorSpec");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QApplication >::lqt_pushenum_ColorSpec_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QApplication::ColorSpec>*) + sizeof(QFlags<QApplication::ColorSpec>));
  QFlags<QApplication::ColorSpec> *fl = static_cast<QFlags<QApplication::ColorSpec>*>( static_cast<void*>(&static_cast<QFlags<QApplication::ColorSpec>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QApplication::ColorSpec>(lqtL_toenum(L, i, "QApplication::ColorSpec"));
	}
	if (luaL_newmetatable(L, "QFlags<QApplication::ColorSpec>*")) {
		lua_pushstring(L, "QFlags<QApplication::ColorSpec>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int luaopen_QApplication (lua_State *L) {
  if (luaL_newmetatable(L, "QApplication*")) {
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__setWheelScrollLines);
    lua_setfield(L, -2, "setWheelScrollLines");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__styleSheet);
    lua_setfield(L, -2, "styleSheet");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__colorSpec);
    lua_setfield(L, -2, "colorSpec");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__doubleClickInterval);
    lua_setfield(L, -2, "doubleClickInterval");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__activePopupWidget);
    lua_setfield(L, -2, "activePopupWidget");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__setStyle);
    lua_setfield(L, -2, "setStyle");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__keyboardInputDirection);
    lua_setfield(L, -2, "keyboardInputDirection");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__tr);
    lua_setfield(L, -2, "tr");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__sessionId);
    lua_setfield(L, -2, "sessionId");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__palette);
    lua_setfield(L, -2, "palette");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__setStartDragDistance);
    lua_setfield(L, -2, "setStartDragDistance");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__style);
    lua_setfield(L, -2, "style");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__setGlobalStrut);
    lua_setfield(L, -2, "setGlobalStrut");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__fontMetrics);
    lua_setfield(L, -2, "fontMetrics");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__setQuitOnLastWindowClosed);
    lua_setfield(L, -2, "setQuitOnLastWindowClosed");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__keyboardInputInterval);
    lua_setfield(L, -2, "keyboardInputInterval");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__beep);
    lua_setfield(L, -2, "beep");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__aboutQt);
    lua_setfield(L, -2, "aboutQt");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__startDragTime);
    lua_setfield(L, -2, "startDragTime");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__metaObject);
    lua_setfield(L, -2, "metaObject");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__delete);
    lua_setfield(L, -2, "delete");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__mouseButtons);
    lua_setfield(L, -2, "mouseButtons");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__keyboardModifiers);
    lua_setfield(L, -2, "keyboardModifiers");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__setOverrideCursor);
    lua_setfield(L, -2, "setOverrideCursor");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__focusWidget);
    lua_setfield(L, -2, "focusWidget");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__overrideCursor);
    lua_setfield(L, -2, "overrideCursor");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__exec);
    lua_setfield(L, -2, "exec");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__setEffectEnabled);
    lua_setfield(L, -2, "setEffectEnabled");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__saveState);
    lua_setfield(L, -2, "saveState");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__setDesktopSettingsAware);
    lua_setfield(L, -2, "setDesktopSettingsAware");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__sessionKey);
    lua_setfield(L, -2, "sessionKey");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__trUtf8);
    lua_setfield(L, -2, "trUtf8");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__topLevelWidgets);
    lua_setfield(L, -2, "topLevelWidgets");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__desktop);
    lua_setfield(L, -2, "desktop");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__quitOnLastWindowClosed);
    lua_setfield(L, -2, "quitOnLastWindowClosed");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__setPalette);
    lua_setfield(L, -2, "setPalette");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__isLeftToRight);
    lua_setfield(L, -2, "isLeftToRight");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__layoutDirection);
    lua_setfield(L, -2, "layoutDirection");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__keyboardInputLocale);
    lua_setfield(L, -2, "keyboardInputLocale");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__new);
    lua_setfield(L, -2, "new");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__wheelScrollLines);
    lua_setfield(L, -2, "wheelScrollLines");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__type);
    lua_setfield(L, -2, "type");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__syncX);
    lua_setfield(L, -2, "syncX");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__setDoubleClickInterval);
    lua_setfield(L, -2, "setDoubleClickInterval");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__setColorSpec);
    lua_setfield(L, -2, "setColorSpec");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__windowIcon);
    lua_setfield(L, -2, "windowIcon");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__restoreOverrideCursor);
    lua_setfield(L, -2, "restoreOverrideCursor");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__setKeyboardInputInterval);
    lua_setfield(L, -2, "setKeyboardInputInterval");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__setActiveWindow);
    lua_setfield(L, -2, "setActiveWindow");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__cursorFlashTime);
    lua_setfield(L, -2, "cursorFlashTime");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__globalStrut);
    lua_setfield(L, -2, "globalStrut");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__font);
    lua_setfield(L, -2, "font");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__desktopSettingsAware);
    lua_setfield(L, -2, "desktopSettingsAware");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__topLevelAt);
    lua_setfield(L, -2, "topLevelAt");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__setCursorFlashTime);
    lua_setfield(L, -2, "setCursorFlashTime");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__setInputContext);
    lua_setfield(L, -2, "setInputContext");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__startDragDistance);
    lua_setfield(L, -2, "startDragDistance");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__clipboard);
    lua_setfield(L, -2, "clipboard");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__activeWindow);
    lua_setfield(L, -2, "activeWindow");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__changeOverrideCursor);
    lua_setfield(L, -2, "changeOverrideCursor");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__isSessionRestored);
    lua_setfield(L, -2, "isSessionRestored");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__activeModalWidget);
    lua_setfield(L, -2, "activeModalWidget");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__isEffectEnabled);
    lua_setfield(L, -2, "isEffectEnabled");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__inputContext);
    lua_setfield(L, -2, "inputContext");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__widgetAt);
    lua_setfield(L, -2, "widgetAt");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__setFont);
    lua_setfield(L, -2, "setFont");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__setStartDragTime);
    lua_setfield(L, -2, "setStartDragTime");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__allWidgets);
    lua_setfield(L, -2, "allWidgets");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__setWindowIcon);
    lua_setfield(L, -2, "setWindowIcon");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__commitData);
    lua_setfield(L, -2, "commitData");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__isRightToLeft);
    lua_setfield(L, -2, "isRightToLeft");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__notify);
    lua_setfield(L, -2, "notify");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__setLayoutDirection);
    lua_setfield(L, -2, "setLayoutDirection");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__closeAllWindows);
    lua_setfield(L, -2, "closeAllWindows");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__alert);
    lua_setfield(L, -2, "alert");
    lua_pushcfunction(L, LuaBinder< QApplication >::__LuaWrapCall__setStyleSheet);
    lua_setfield(L, -2, "setStyleSheet");
    LuaBinder< QApplication >::lqt_pushenum_Type(L);
    lua_setfield(L, -2, "Type");
    LuaBinder< QApplication >::lqt_pushenum_ColorSpec(L);
    lua_setfield(L, -2, "ColorSpec");
    lua_newtable(L);
    lua_pushboolean(L, 1);
    lua_setfield(L, -2, "QCoreApplication*");
    lua_pushboolean(L, 0);
    lua_setfield(L, -2, "QObject*");
    lua_setfield(L, -2, "__base");
    lua_pushcfunction(L, lqtL_newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, lqtL_index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, lqtL_gc);
    lua_setfield(L, -2, "__gc");
    lua_pushstring(L, "QApplication");
    lua_setfield(L, -2, "__qtype");
		lua_setglobal(L, "QApplication");
  } else {
		lua_pop(L, 1);
	}
  return 0;
}
