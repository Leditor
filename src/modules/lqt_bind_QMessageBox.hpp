#include "lqt_common.hpp"
#include <QMessageBox>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QMessageBox > : public QMessageBox {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__standardButtons (lua_State *L);
  static int __LuaWrapCall__setWindowModality (lua_State *L);
  static int __LuaWrapCall__sizeHint (lua_State *L);
  static int __LuaWrapCall__defaultButton (lua_State *L);
  static int __LuaWrapCall__setDefaultButton__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__setDefaultButton__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__setDefaultButton (lua_State *L);
  static int __LuaWrapCall__setIcon (lua_State *L);
  static int __LuaWrapCall__setStandardButtons (lua_State *L);
  static int __LuaWrapCall__button (lua_State *L);
  static int __LuaWrapCall__standardIcon (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__tr (lua_State *L);
  static int __LuaWrapCall__textFormat (lua_State *L);
  static int __LuaWrapCall__informativeText (lua_State *L);
  static int __LuaWrapCall__removeButton (lua_State *L);
  static int __LuaWrapCall__buttonText (lua_State *L);
  static int __LuaWrapCall__critical__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__critical__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__critical__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__critical__OverloadedVersion__4 (lua_State *L);
  static int __LuaWrapCall__critical (lua_State *L);
  static int __LuaWrapCall__addButton__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__addButton__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__addButton__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__addButton (lua_State *L);
  static int __LuaWrapCall__setDetailedText (lua_State *L);
  static int __LuaWrapCall__about (lua_State *L);
  static int __LuaWrapCall__setText (lua_State *L);
  static int __LuaWrapCall__aboutQt (lua_State *L);
  static int __LuaWrapCall__iconPixmap (lua_State *L);
  static int __LuaWrapCall__setEscapeButton__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__setEscapeButton__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__setEscapeButton (lua_State *L);
  static int __LuaWrapCall__standardButton (lua_State *L);
  static int __LuaWrapCall__metaObject (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__new (lua_State *L);
  static int __LuaWrapCall__warning__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__warning__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__warning__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__warning__OverloadedVersion__4 (lua_State *L);
  static int __LuaWrapCall__warning (lua_State *L);
  static int __LuaWrapCall__setWindowTitle (lua_State *L);
  static int __LuaWrapCall__delete (lua_State *L);
  static int __LuaWrapCall__escapeButton (lua_State *L);
  static int __LuaWrapCall__clickedButton (lua_State *L);
  static int __LuaWrapCall__detailedText (lua_State *L);
  static int __LuaWrapCall__text (lua_State *L);
  static int __LuaWrapCall__setTextFormat (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__trUtf8 (lua_State *L);
  static int __LuaWrapCall__setButtonText (lua_State *L);
  static int __LuaWrapCall__question__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__question__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__question__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__question__OverloadedVersion__4 (lua_State *L);
  static int __LuaWrapCall__question (lua_State *L);
  static int __LuaWrapCall__setInformativeText (lua_State *L);
  static int __LuaWrapCall__setIconPixmap (lua_State *L);
  static int __LuaWrapCall__information__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__information__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__information__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__information__OverloadedVersion__4 (lua_State *L);
  static int __LuaWrapCall__information (lua_State *L);
  static int __LuaWrapCall__icon (lua_State *L);
protected:
  void styleChange (QStyle& arg1);
public:
protected:
  void focusInEvent (QFocusEvent * arg1);
public:
protected:
  void keyPressEvent (QKeyEvent * arg1);
public:
protected:
  void contextMenuEvent (QContextMenuEvent * arg1);
public:
  int devType () const;
  void setVisible (bool arg1);
  QVariant inputMethodQuery (Qt::InputMethodQuery arg1) const;
  QPaintEngine * paintEngine () const;
protected:
  void dragLeaveEvent (QDragLeaveEvent * arg1);
public:
protected:
  void mousePressEvent (QMouseEvent * arg1);
public:
protected:
  void dragEnterEvent (QDragEnterEvent * arg1);
public:
protected:
  void closeEvent (QCloseEvent * arg1);
public:
  void reject ();
  void accept ();
protected:
  void tabletEvent (QTabletEvent * arg1);
public:
protected:
  void customEvent (QEvent * arg1);
public:
protected:
  void moveEvent (QMoveEvent * arg1);
public:
protected:
  void keyReleaseEvent (QKeyEvent * arg1);
public:
  QSize minimumSizeHint () const;
protected:
  void dropEvent (QDropEvent * arg1);
public:
protected:
  void windowActivationChange (bool arg1);
public:
protected:
  void showEvent (QShowEvent * arg1);
public:
  const QMetaObject * metaObject () const;
protected:
  void paintEvent (QPaintEvent * arg1);
public:
  void done (int arg1);
protected:
  void mouseDoubleClickEvent (QMouseEvent * arg1);
public:
protected:
  void enabledChange (bool arg1);
public:
protected:
  bool focusNextPrevChild (bool arg1);
public:
  int heightForWidth (int arg1) const;
protected:
  void fontChange (const QFont& arg1);
public:
protected:
  void timerEvent (QTimerEvent * arg1);
public:
protected:
  void disconnectNotify (const char * arg1);
public:
protected:
  void mouseMoveEvent (QMouseEvent * arg1);
public:
protected:
  void focusOutEvent (QFocusEvent * arg1);
public:
protected:
  void childEvent (QChildEvent * arg1);
public:
protected:
  void connectNotify (const char * arg1);
public:
protected:
  void mouseReleaseEvent (QMouseEvent * arg1);
public:
protected:
  int metric (QPaintDevice::PaintDeviceMetric arg1) const;
public:
protected:
  bool eventFilter (QObject * arg1, QEvent * arg2);
public:
protected:
  void inputMethodEvent (QInputMethodEvent * arg1);
public:
protected:
  void wheelEvent (QWheelEvent * arg1);
public:
protected:
  void actionEvent (QActionEvent * arg1);
public:
protected:
  void paletteChange (const QPalette& arg1);
public:
protected:
  void languageChange ();
public:
protected:
  bool event (QEvent * arg1);
public:
protected:
  void hideEvent (QHideEvent * arg1);
public:
protected:
  void dragMoveEvent (QDragMoveEvent * arg1);
public:
  QSize sizeHint () const;
protected:
  void resizeEvent (QResizeEvent * arg1);
public:
protected:
  void leaveEvent (QEvent * arg1);
public:
protected:
  void changeEvent (QEvent * arg1);
public:
protected:
  void enterEvent (QEvent * arg1);
public:
  ~LuaBinder< QMessageBox > ();
  static int lqt_pushenum_Icon (lua_State *L);
  static int lqt_pushenum_Icon_QFLAGS_CREATOR (lua_State *L);
  static int lqt_pushenum_ButtonRole (lua_State *L);
  static int lqt_pushenum_ButtonRole_QFLAGS_CREATOR (lua_State *L);
  static int lqt_pushenum_StandardButton (lua_State *L);
  static int lqt_pushenum_StandardButton_QFLAGS_CREATOR (lua_State *L);
  LuaBinder< QMessageBox > (lua_State *l, QWidget * arg1):QMessageBox(arg1), L(l) {}
  LuaBinder< QMessageBox > (lua_State *l, QMessageBox::Icon arg1, const QString& arg2, const QString& arg3, QFlags<QMessageBox::StandardButton> arg4, QWidget * arg5, QFlags<Qt::WindowType> arg6):QMessageBox(arg1, arg2, arg3, arg4, arg5, arg6), L(l) {}
  LuaBinder< QMessageBox > (lua_State *l, const QString& arg1, const QString& arg2, QMessageBox::Icon arg3, int arg4, int arg5, int arg6, QWidget * arg7, QFlags<Qt::WindowType> arg8):QMessageBox(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8), L(l) {}
};

extern "C" int luaopen_QMessageBox (lua_State *L);
