#include "lqt_function.hpp"
 LuaFunction::LuaFunction(lua_State *state):L(state) {
	int functionTable = lua_gettop(L); // not yet but soon
	//qDebug() << "Function" << this << "is born";
	lua_getfield(L, LUA_REGISTRYINDEX, LUA_FUNCTION_REGISTRY);
	if (lua_isnil(L, -1)) {
		lua_pop(L, 1);
		lua_newtable(L);
		lua_pushvalue(L, -1);
		lua_setfield(L, LUA_REGISTRYINDEX, LUA_FUNCTION_REGISTRY);
	}
	lua_insert(L, -2);

	lua_pushvalue(L, -1);
	lua_gettable(L, functionTable);

	if (!lqtL_testudata(L, -1, "QObject*")) {
		//qDebug() << "not QObject* is" << luaL_typename(L, -1);
		lua_pop(L, 1);
		// top of stack is the function I want
		//qDebug() << "to be bound is" << luaL_typename(L, -1);
		lua_pushlightuserdata(L, this);
		lua_pushvalue(L, -2);
		lua_settable(L, functionTable);
		// registry this is associated to this function
		lqtL_passudata(L, this, "QObject*");
		lua_insert(L, -2);
		lua_pushvalue(L, -2);
		lua_settable(L, functionTable);
	} else {
		// leave the qobject on top;
		//qDebug() << "Function" << this << "scheduled for deletion";
		this->deleteLater();
	}
	lua_replace(L, functionTable);
	lua_settop(L, functionTable);
}
LuaFunction::~LuaFunction() {
	//qDebug() << "Function" << this << "is dead";
	lua_getfield(L, LUA_REGISTRYINDEX, LUA_FUNCTION_REGISTRY);
	lua_pushlightuserdata(L, this);
	lua_gettable(L, -2);
	lua_pushnil(L);
	lua_settable(L, -3);
	lua_pushlightuserdata(L, this);
	lua_pushnil(L);
	lua_settable(L, -3);
	lua_pop(L, 1);
}
void LuaFunction::function () {
  int functionTable = lua_gettop(L) + 1;
  lua_getfield(L, LUA_REGISTRYINDEX, LUA_FUNCTION_REGISTRY);
  if (!lua_istable(L, -1)) {
    return;
  }
  lua_pushlightuserdata(L, this);
  lua_gettable(L, functionTable);
  
  lua_call(L,0, 0);
};
void LuaFunction::function (double arg1) {
  int functionTable = lua_gettop(L) + 1;
  lua_getfield(L, LUA_REGISTRYINDEX, LUA_FUNCTION_REGISTRY);
  if (!lua_istable(L, -1)) {
    return;
  }
  lua_pushlightuserdata(L, this);
  lua_gettable(L, functionTable);
  lua_pushnumber(L, arg1);
  lua_call(L,1, 0);
};
void LuaFunction::function (int arg1) {
  int functionTable = lua_gettop(L) + 1;
  lua_getfield(L, LUA_REGISTRYINDEX, LUA_FUNCTION_REGISTRY);
  if (!lua_istable(L, -1)) {
    return;
  }
  lua_pushlightuserdata(L, this);
  lua_gettable(L, functionTable);
  lua_pushinteger(L, arg1);
  lua_call(L,1, 0);
};
void LuaFunction::function (const char * arg1) {
  int functionTable = lua_gettop(L) + 1;
  lua_getfield(L, LUA_REGISTRYINDEX, LUA_FUNCTION_REGISTRY);
  if (!lua_istable(L, -1)) {
    return;
  }
  lua_pushlightuserdata(L, this);
  lua_gettable(L, functionTable);
  lua_pushstring(L, arg1);
  lua_call(L,1, 0);
};
void LuaFunction::function (bool arg1) {
  int functionTable = lua_gettop(L) + 1;
  lua_getfield(L, LUA_REGISTRYINDEX, LUA_FUNCTION_REGISTRY);
  if (!lua_istable(L, -1)) {
    return;
  }
  lua_pushlightuserdata(L, this);
  lua_gettable(L, functionTable);
  lua_pushboolean(L, arg1);
  lua_call(L,1, 0);
};
void LuaFunction::function (int arg1, const char * arg2) {
  int functionTable = lua_gettop(L) + 1;
  lua_getfield(L, LUA_REGISTRYINDEX, LUA_FUNCTION_REGISTRY);
  if (!lua_istable(L, -1)) {
    return;
  }
  lua_pushlightuserdata(L, this);
  lua_gettable(L, functionTable);
  lua_pushinteger(L, arg1);lua_pushstring(L, arg2);
  lua_call(L,2, 0);
};
void LuaFunction::function (bool arg1, double arg2) {
  int functionTable = lua_gettop(L) + 1;
  lua_getfield(L, LUA_REGISTRYINDEX, LUA_FUNCTION_REGISTRY);
  if (!lua_istable(L, -1)) {
    return;
  }
  lua_pushlightuserdata(L, this);
  lua_gettable(L, functionTable);
  lua_pushboolean(L, arg1);lua_pushnumber(L, arg2);
  lua_call(L,2, 0);
};
void LuaFunction::function (bool arg1, int arg2) {
  int functionTable = lua_gettop(L) + 1;
  lua_getfield(L, LUA_REGISTRYINDEX, LUA_FUNCTION_REGISTRY);
  if (!lua_istable(L, -1)) {
    return;
  }
  lua_pushlightuserdata(L, this);
  lua_gettable(L, functionTable);
  lua_pushboolean(L, arg1);lua_pushinteger(L, arg2);
  lua_call(L,2, 0);
};
void LuaFunction::function (int arg1, int arg2) {
  int functionTable = lua_gettop(L) + 1;
  lua_getfield(L, LUA_REGISTRYINDEX, LUA_FUNCTION_REGISTRY);
  if (!lua_istable(L, -1)) {
    return;
  }
  lua_pushlightuserdata(L, this);
  lua_gettable(L, functionTable);
  lua_pushinteger(L, arg1);lua_pushinteger(L, arg2);
  lua_call(L,2, 0);
};
void LuaFunction::function (bool arg1, bool arg2) {
  int functionTable = lua_gettop(L) + 1;
  lua_getfield(L, LUA_REGISTRYINDEX, LUA_FUNCTION_REGISTRY);
  if (!lua_istable(L, -1)) {
    return;
  }
  lua_pushlightuserdata(L, this);
  lua_gettable(L, functionTable);
  lua_pushboolean(L, arg1);lua_pushboolean(L, arg2);
  lua_call(L,2, 0);
};
void LuaFunction::function (bool arg1, const char * arg2) {
  int functionTable = lua_gettop(L) + 1;
  lua_getfield(L, LUA_REGISTRYINDEX, LUA_FUNCTION_REGISTRY);
  if (!lua_istable(L, -1)) {
    return;
  }
  lua_pushlightuserdata(L, this);
  lua_gettable(L, functionTable);
  lua_pushboolean(L, arg1);lua_pushstring(L, arg2);
  lua_call(L,2, 0);
};
void LuaFunction::function (int arg1, double arg2) {
  int functionTable = lua_gettop(L) + 1;
  lua_getfield(L, LUA_REGISTRYINDEX, LUA_FUNCTION_REGISTRY);
  if (!lua_istable(L, -1)) {
    return;
  }
  lua_pushlightuserdata(L, this);
  lua_gettable(L, functionTable);
  lua_pushinteger(L, arg1);lua_pushnumber(L, arg2);
  lua_call(L,2, 0);
};
void LuaFunction::function (double arg1, int arg2) {
  int functionTable = lua_gettop(L) + 1;
  lua_getfield(L, LUA_REGISTRYINDEX, LUA_FUNCTION_REGISTRY);
  if (!lua_istable(L, -1)) {
    return;
  }
  lua_pushlightuserdata(L, this);
  lua_gettable(L, functionTable);
  lua_pushnumber(L, arg1);lua_pushinteger(L, arg2);
  lua_call(L,2, 0);
};
void LuaFunction::function (const char * arg1, bool arg2) {
  int functionTable = lua_gettop(L) + 1;
  lua_getfield(L, LUA_REGISTRYINDEX, LUA_FUNCTION_REGISTRY);
  if (!lua_istable(L, -1)) {
    return;
  }
  lua_pushlightuserdata(L, this);
  lua_gettable(L, functionTable);
  lua_pushstring(L, arg1);lua_pushboolean(L, arg2);
  lua_call(L,2, 0);
};
void LuaFunction::function (int arg1, bool arg2) {
  int functionTable = lua_gettop(L) + 1;
  lua_getfield(L, LUA_REGISTRYINDEX, LUA_FUNCTION_REGISTRY);
  if (!lua_istable(L, -1)) {
    return;
  }
  lua_pushlightuserdata(L, this);
  lua_gettable(L, functionTable);
  lua_pushinteger(L, arg1);lua_pushboolean(L, arg2);
  lua_call(L,2, 0);
};
void LuaFunction::function (double arg1, double arg2) {
  int functionTable = lua_gettop(L) + 1;
  lua_getfield(L, LUA_REGISTRYINDEX, LUA_FUNCTION_REGISTRY);
  if (!lua_istable(L, -1)) {
    return;
  }
  lua_pushlightuserdata(L, this);
  lua_gettable(L, functionTable);
  lua_pushnumber(L, arg1);lua_pushnumber(L, arg2);
  lua_call(L,2, 0);
};
void LuaFunction::function (const char * arg1, double arg2) {
  int functionTable = lua_gettop(L) + 1;
  lua_getfield(L, LUA_REGISTRYINDEX, LUA_FUNCTION_REGISTRY);
  if (!lua_istable(L, -1)) {
    return;
  }
  lua_pushlightuserdata(L, this);
  lua_gettable(L, functionTable);
  lua_pushstring(L, arg1);lua_pushnumber(L, arg2);
  lua_call(L,2, 0);
};
void LuaFunction::function (const char * arg1, int arg2) {
  int functionTable = lua_gettop(L) + 1;
  lua_getfield(L, LUA_REGISTRYINDEX, LUA_FUNCTION_REGISTRY);
  if (!lua_istable(L, -1)) {
    return;
  }
  lua_pushlightuserdata(L, this);
  lua_gettable(L, functionTable);
  lua_pushstring(L, arg1);lua_pushinteger(L, arg2);
  lua_call(L,2, 0);
};
void LuaFunction::function (double arg1, bool arg2) {
  int functionTable = lua_gettop(L) + 1;
  lua_getfield(L, LUA_REGISTRYINDEX, LUA_FUNCTION_REGISTRY);
  if (!lua_istable(L, -1)) {
    return;
  }
  lua_pushlightuserdata(L, this);
  lua_gettable(L, functionTable);
  lua_pushnumber(L, arg1);lua_pushboolean(L, arg2);
  lua_call(L,2, 0);
};
void LuaFunction::function (double arg1, const char * arg2) {
  int functionTable = lua_gettop(L) + 1;
  lua_getfield(L, LUA_REGISTRYINDEX, LUA_FUNCTION_REGISTRY);
  if (!lua_istable(L, -1)) {
    return;
  }
  lua_pushlightuserdata(L, this);
  lua_gettable(L, functionTable);
  lua_pushnumber(L, arg1);lua_pushstring(L, arg2);
  lua_call(L,2, 0);
};
void LuaFunction::function (const char * arg1, const char * arg2) {
  int functionTable = lua_gettop(L) + 1;
  lua_getfield(L, LUA_REGISTRYINDEX, LUA_FUNCTION_REGISTRY);
  if (!lua_istable(L, -1)) {
    return;
  }
  lua_pushlightuserdata(L, this);
  lua_gettable(L, functionTable);
  lua_pushstring(L, arg1);lua_pushstring(L, arg2);
  lua_call(L,2, 0);
};
