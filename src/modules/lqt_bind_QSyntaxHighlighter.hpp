#include "lqt_common.hpp"
#include <QSyntaxHighlighter>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QSyntaxHighlighter > : public QSyntaxHighlighter {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__tr (lua_State *L);
  static int __LuaWrapCall__delete (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__new (lua_State *L);
  static int __LuaWrapCall__setDocument (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__trUtf8 (lua_State *L);
  static int __LuaWrapCall__metaObject (lua_State *L);
  static int __LuaWrapCall__rehighlight (lua_State *L);
  static int __LuaWrapCall__document (lua_State *L);
  bool eventFilter (QObject * arg1, QEvent * arg2);
protected:
  void highlightBlock (const QString& arg1);
public:
  const QMetaObject * metaObject () const;
  bool event (QEvent * arg1);
protected:
  void disconnectNotify (const char * arg1);
public:
protected:
  void connectNotify (const char * arg1);
public:
protected:
  void timerEvent (QTimerEvent * arg1);
public:
protected:
  void childEvent (QChildEvent * arg1);
public:
protected:
  void customEvent (QEvent * arg1);
public:
  ~LuaBinder< QSyntaxHighlighter > ();
  LuaBinder< QSyntaxHighlighter > (lua_State *l, QObject * arg1):QSyntaxHighlighter(arg1), L(l) {}
  LuaBinder< QSyntaxHighlighter > (lua_State *l, QTextDocument * arg1):QSyntaxHighlighter(arg1), L(l) {}
  LuaBinder< QSyntaxHighlighter > (lua_State *l, QTextEdit * arg1):QSyntaxHighlighter(arg1), L(l) {}
};

extern "C" int luaopen_QSyntaxHighlighter (lua_State *L);
