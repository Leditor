#include "lqt_bind_QLayoutItem.hpp"

int LuaBinder< QLayoutItem >::__LuaWrapCall__invalidate (lua_State *L) {
  QLayoutItem *& __lua__obj = *static_cast<QLayoutItem**>(lqtL_checkudata(L, 1, "QLayoutItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QLayoutItem::invalidate();
  return 0;
}
int LuaBinder< QLayoutItem >::__LuaWrapCall__controlTypes (lua_State *L) {
  QLayoutItem *& __lua__obj = *static_cast<QLayoutItem**>(lqtL_checkudata(L, 1, "QLayoutItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<QSizePolicy::ControlType> ret = __lua__obj->QLayoutItem::controlTypes();
  lqtL_passudata(L, new QFlags<QSizePolicy::ControlType>(ret), "QFlags<QSizePolicy::ControlType>*");
  return 1;
}
int LuaBinder< QLayoutItem >::__LuaWrapCall__heightForWidth (lua_State *L) {
  QLayoutItem *& __lua__obj = *static_cast<QLayoutItem**>(lqtL_checkudata(L, 1, "QLayoutItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int ret = __lua__obj->QLayoutItem::heightForWidth(arg1);
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QLayoutItem >::__LuaWrapCall__spacerItem (lua_State *L) {
  QLayoutItem *& __lua__obj = *static_cast<QLayoutItem**>(lqtL_checkudata(L, 1, "QLayoutItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QSpacerItem * ret = __lua__obj->QLayoutItem::spacerItem();
  lqtL_pushudata(L, ret, "QSpacerItem*");
  return 1;
}
int LuaBinder< QLayoutItem >::__LuaWrapCall__delete (lua_State *L) {
  QLayoutItem *& __lua__obj = *static_cast<QLayoutItem**>(lqtL_checkudata(L, 1, "QLayoutItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  delete __lua__obj;
  __lua__obj = 0;
  return 0;
}
int LuaBinder< QLayoutItem >::__LuaWrapCall__layout (lua_State *L) {
  QLayoutItem *& __lua__obj = *static_cast<QLayoutItem**>(lqtL_checkudata(L, 1, "QLayoutItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QLayout * ret = __lua__obj->QLayoutItem::layout();
  lqtL_pushudata(L, ret, "QLayout*");
  return 1;
}
int LuaBinder< QLayoutItem >::__LuaWrapCall__setAlignment (lua_State *L) {
  QLayoutItem *& __lua__obj = *static_cast<QLayoutItem**>(lqtL_checkudata(L, 1, "QLayoutItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<Qt::AlignmentFlag> arg1 = **static_cast<QFlags<Qt::AlignmentFlag>**>(lqtL_checkudata(L, 2, "QFlags<Qt::AlignmentFlag>*"));
  __lua__obj->QLayoutItem::setAlignment(arg1);
  return 0;
}
int LuaBinder< QLayoutItem >::__LuaWrapCall__minimumHeightForWidth (lua_State *L) {
  QLayoutItem *& __lua__obj = *static_cast<QLayoutItem**>(lqtL_checkudata(L, 1, "QLayoutItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int ret = __lua__obj->QLayoutItem::minimumHeightForWidth(arg1);
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QLayoutItem >::__LuaWrapCall__alignment (lua_State *L) {
  QLayoutItem *& __lua__obj = *static_cast<QLayoutItem**>(lqtL_checkudata(L, 1, "QLayoutItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<Qt::AlignmentFlag> ret = __lua__obj->QLayoutItem::alignment();
  lqtL_passudata(L, new QFlags<Qt::AlignmentFlag>(ret), "QFlags<Qt::AlignmentFlag>*");
  return 1;
}
int LuaBinder< QLayoutItem >::__LuaWrapCall__widget (lua_State *L) {
  QLayoutItem *& __lua__obj = *static_cast<QLayoutItem**>(lqtL_checkudata(L, 1, "QLayoutItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * ret = __lua__obj->QLayoutItem::widget();
  lqtL_pushudata(L, ret, "QWidget*");
  return 1;
}
int LuaBinder< QLayoutItem >::__LuaWrapCall__hasHeightForWidth (lua_State *L) {
  QLayoutItem *& __lua__obj = *static_cast<QLayoutItem**>(lqtL_checkudata(L, 1, "QLayoutItem*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QLayoutItem::hasHeightForWidth();
  lua_pushboolean(L, ret);
  return 1;
}
bool LuaBinder< QLayoutItem >::hasHeightForWidth () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayoutItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "hasHeightForWidth");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QLayoutItem::hasHeightForWidth();
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
QLayout * LuaBinder< QLayoutItem >::layout () {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayoutItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "layout");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QLayoutItem::layout();
  }
  QLayout * ret = *static_cast<QLayout**>(lqtL_checkudata(L, -1, "QLayout*"));
  lua_settop(L, oldtop);
  return ret;
}
QSpacerItem * LuaBinder< QLayoutItem >::spacerItem () {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayoutItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "spacerItem");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QLayoutItem::spacerItem();
  }
  QSpacerItem * ret = *static_cast<QSpacerItem**>(lqtL_checkudata(L, -1, "QSpacerItem*"));
  lua_settop(L, oldtop);
  return ret;
}
QSize LuaBinder< QLayoutItem >::sizeHint () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayoutItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "sizeHint");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
      }
  QSize ret = **static_cast<QSize**>(lqtL_checkudata(L, -1, "QSize*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QLayoutItem >::setGeometry (const QRect& arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayoutItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "setGeometry");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QRect*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
      }
  lua_settop(L, oldtop);
}
QSize LuaBinder< QLayoutItem >::minimumSize () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayoutItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "minimumSize");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
      }
  QSize ret = **static_cast<QSize**>(lqtL_checkudata(L, -1, "QSize*"));
  lua_settop(L, oldtop);
  return ret;
}
int LuaBinder< QLayoutItem >::heightForWidth (int arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayoutItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "heightForWidth");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushinteger(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QLayoutItem::heightForWidth(arg1);
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
QWidget * LuaBinder< QLayoutItem >::widget () {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayoutItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "widget");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QLayoutItem::widget();
  }
  QWidget * ret = *static_cast<QWidget**>(lqtL_checkudata(L, -1, "QWidget*"));
  lua_settop(L, oldtop);
  return ret;
}
bool LuaBinder< QLayoutItem >::isEmpty () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayoutItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "isEmpty");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
      }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QLayoutItem >::invalidate () {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayoutItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "invalidate");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QLayoutItem::invalidate();
  }
  lua_settop(L, oldtop);
}
QFlags<Qt::Orientation> LuaBinder< QLayoutItem >::expandingDirections () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayoutItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "expandingDirections");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
      }
  QFlags<Qt::Orientation> ret = **static_cast<QFlags<Qt::Orientation>**>(lqtL_checkudata(L, -1, "QFlags<Qt::Orientation>*"));
  lua_settop(L, oldtop);
  return ret;
}
QRect LuaBinder< QLayoutItem >::geometry () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayoutItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "geometry");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
      }
  QRect ret = **static_cast<QRect**>(lqtL_checkudata(L, -1, "QRect*"));
  lua_settop(L, oldtop);
  return ret;
}
int LuaBinder< QLayoutItem >::minimumHeightForWidth (int arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayoutItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "minimumHeightForWidth");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushinteger(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QLayoutItem::minimumHeightForWidth(arg1);
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
QSize LuaBinder< QLayoutItem >::maximumSize () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayoutItem*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "maximumSize");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
      }
  QSize ret = **static_cast<QSize**>(lqtL_checkudata(L, -1, "QSize*"));
  lua_settop(L, oldtop);
  return ret;
}
LuaBinder< QLayoutItem >::  ~LuaBinder< QLayoutItem > () {
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLayoutItem*");
  lua_getfield(L, -1, "~QLayoutItem");

  if (lua_isfunction(L, -1)) {
    lua_insert(L, -2);
    lua_pcall(L, 1, 1, 0);
  } else {
  }
  lua_settop(L, oldtop);
}
int luaopen_QLayoutItem (lua_State *L) {
  if (luaL_newmetatable(L, "QLayoutItem*")) {
    lua_pushcfunction(L, LuaBinder< QLayoutItem >::__LuaWrapCall__invalidate);
    lua_setfield(L, -2, "invalidate");
    lua_pushcfunction(L, LuaBinder< QLayoutItem >::__LuaWrapCall__controlTypes);
    lua_setfield(L, -2, "controlTypes");
    lua_pushcfunction(L, LuaBinder< QLayoutItem >::__LuaWrapCall__heightForWidth);
    lua_setfield(L, -2, "heightForWidth");
    lua_pushcfunction(L, LuaBinder< QLayoutItem >::__LuaWrapCall__spacerItem);
    lua_setfield(L, -2, "spacerItem");
    lua_pushcfunction(L, LuaBinder< QLayoutItem >::__LuaWrapCall__delete);
    lua_setfield(L, -2, "delete");
    lua_pushcfunction(L, LuaBinder< QLayoutItem >::__LuaWrapCall__layout);
    lua_setfield(L, -2, "layout");
    lua_pushcfunction(L, LuaBinder< QLayoutItem >::__LuaWrapCall__setAlignment);
    lua_setfield(L, -2, "setAlignment");
    lua_pushcfunction(L, LuaBinder< QLayoutItem >::__LuaWrapCall__minimumHeightForWidth);
    lua_setfield(L, -2, "minimumHeightForWidth");
    lua_pushcfunction(L, LuaBinder< QLayoutItem >::__LuaWrapCall__alignment);
    lua_setfield(L, -2, "alignment");
    lua_pushcfunction(L, LuaBinder< QLayoutItem >::__LuaWrapCall__widget);
    lua_setfield(L, -2, "widget");
    lua_pushcfunction(L, LuaBinder< QLayoutItem >::__LuaWrapCall__hasHeightForWidth);
    lua_setfield(L, -2, "hasHeightForWidth");
    lua_newtable(L);
    lua_setfield(L, -2, "__base");
    lua_pushcfunction(L, lqtL_newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, lqtL_index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, lqtL_gc);
    lua_setfield(L, -2, "__gc");
    lua_pushstring(L, "QLayoutItem");
    lua_setfield(L, -2, "__qtype");
		lua_setglobal(L, "QLayoutItem");
  } else {
		lua_pop(L, 1);
	}
  return 0;
}
