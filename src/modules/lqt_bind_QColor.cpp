#include "lqt_bind_QColor.hpp"

int LuaBinder< QColor >::__LuaWrapCall__cyan (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QColor::cyan();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__fromCmyk (lua_State *L) {
  int arg1 = lua_tointeger(L, 1);
  int arg2 = lua_tointeger(L, 2);
  int arg3 = lua_tointeger(L, 3);
  int arg4 = lua_tointeger(L, 4);
  int arg5 = lua_isnumber(L, 5)?lua_tointeger(L, 5):static_cast< int >(255);
  QColor ret = QColor::fromCmyk(arg1, arg2, arg3, arg4, arg5);
  lqtL_passudata(L, new QColor(ret), "QColor*");
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__cyanF (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double ret = __lua__obj->QColor::cyanF();
  lua_pushnumber(L, ret);
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__redF (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double ret = __lua__obj->QColor::redF();
  lua_pushnumber(L, ret);
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__setCmyk (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int arg2 = lua_tointeger(L, 3);
  int arg3 = lua_tointeger(L, 4);
  int arg4 = lua_tointeger(L, 5);
  int arg5 = lua_isnumber(L, 6)?lua_tointeger(L, 6):static_cast< int >(255);
  __lua__obj->QColor::setCmyk(arg1, arg2, arg3, arg4, arg5);
  return 0;
}
int LuaBinder< QColor >::__LuaWrapCall__isValid (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QColor::isValid();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__black (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QColor::black();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__getCmyk (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int * arg1 = static_cast<int *>(lua_touserdata(L, 2));
  int * arg2 = static_cast<int *>(lua_touserdata(L, 3));
  int * arg3 = static_cast<int *>(lua_touserdata(L, 4));
  int * arg4 = static_cast<int *>(lua_touserdata(L, 5));
  int * arg5 = lua_isnumber(L, 6)?static_cast<int *>(lua_touserdata(L, 6)):static_cast< int * >(0);
  __lua__obj->QColor::getCmyk(arg1, arg2, arg3, arg4, arg5);
  return 0;
}
int LuaBinder< QColor >::__LuaWrapCall__yellow (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QColor::yellow();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__setGreen (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QColor::setGreen(arg1);
  return 0;
}
int LuaBinder< QColor >::__LuaWrapCall__setAlpha (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QColor::setAlpha(arg1);
  return 0;
}
int LuaBinder< QColor >::__LuaWrapCall__convertTo (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QColor::Spec arg1 = static_cast<QColor::Spec>(lqtL_toenum(L, 2, "QColor::Spec"));
  QColor ret = __lua__obj->QColor::convertTo(arg1);
  lqtL_passudata(L, new QColor(ret), "QColor*");
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__setHsv (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int arg2 = lua_tointeger(L, 3);
  int arg3 = lua_tointeger(L, 4);
  int arg4 = lua_isnumber(L, 5)?lua_tointeger(L, 5):static_cast< int >(255);
  __lua__obj->QColor::setHsv(arg1, arg2, arg3, arg4);
  return 0;
}
int LuaBinder< QColor >::__LuaWrapCall__toCmyk (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QColor ret = __lua__obj->QColor::toCmyk();
  lqtL_passudata(L, new QColor(ret), "QColor*");
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__hueF (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double ret = __lua__obj->QColor::hueF();
  lua_pushnumber(L, ret);
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__fromCmykF (lua_State *L) {
  double arg1 = lua_tonumber(L, 1);
  double arg2 = lua_tonumber(L, 2);
  double arg3 = lua_tonumber(L, 3);
  double arg4 = lua_tonumber(L, 4);
  double arg5 = lua_isnumber(L, 5)?lua_tonumber(L, 5):static_cast< double >(1.0e+0);
  QColor ret = QColor::fromCmykF(arg1, arg2, arg3, arg4, arg5);
  lqtL_passudata(L, new QColor(ret), "QColor*");
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__darker (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_isnumber(L, 2)?lua_tointeger(L, 2):static_cast< int >(200);
  QColor ret = __lua__obj->QColor::darker(arg1);
  lqtL_passudata(L, new QColor(ret), "QColor*");
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__setRgba (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  unsigned int arg1 = lua_tointeger(L, 2);
  __lua__obj->QColor::setRgba(arg1);
  return 0;
}
int LuaBinder< QColor >::__LuaWrapCall__rgb (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  unsigned int ret = __lua__obj->QColor::rgb();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__setGreenF (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  __lua__obj->QColor::setGreenF(arg1);
  return 0;
}
int LuaBinder< QColor >::__LuaWrapCall__green (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QColor::green();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__valueF (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double ret = __lua__obj->QColor::valueF();
  lua_pushnumber(L, ret);
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__setNamedColor (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QColor::setNamedColor(arg1);
  return 0;
}
int LuaBinder< QColor >::__LuaWrapCall__fromHsvF (lua_State *L) {
  double arg1 = lua_tonumber(L, 1);
  double arg2 = lua_tonumber(L, 2);
  double arg3 = lua_tonumber(L, 3);
  double arg4 = lua_isnumber(L, 4)?lua_tonumber(L, 4):static_cast< double >(1.0e+0);
  QColor ret = QColor::fromHsvF(arg1, arg2, arg3, arg4);
  lqtL_passudata(L, new QColor(ret), "QColor*");
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__fromRgbF (lua_State *L) {
  double arg1 = lua_tonumber(L, 1);
  double arg2 = lua_tonumber(L, 2);
  double arg3 = lua_tonumber(L, 3);
  double arg4 = lua_isnumber(L, 4)?lua_tonumber(L, 4):static_cast< double >(1.0e+0);
  QColor ret = QColor::fromRgbF(arg1, arg2, arg3, arg4);
  lqtL_passudata(L, new QColor(ret), "QColor*");
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__setRed (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QColor::setRed(arg1);
  return 0;
}
int LuaBinder< QColor >::__LuaWrapCall__value (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QColor::value();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__lighter (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_isnumber(L, 2)?lua_tointeger(L, 2):static_cast< int >(150);
  QColor ret = __lua__obj->QColor::lighter(arg1);
  lqtL_passudata(L, new QColor(ret), "QColor*");
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__rgba (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  unsigned int ret = __lua__obj->QColor::rgba();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__magentaF (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double ret = __lua__obj->QColor::magentaF();
  lua_pushnumber(L, ret);
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__getCmykF (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double * arg1 = static_cast<double *>(lua_touserdata(L, 2));
  double * arg2 = static_cast<double *>(lua_touserdata(L, 3));
  double * arg3 = static_cast<double *>(lua_touserdata(L, 4));
  double * arg4 = static_cast<double *>(lua_touserdata(L, 5));
  double * arg5 = lua_isnumber(L, 6)?static_cast<double *>(lua_touserdata(L, 6)):static_cast< double * >(0);
  __lua__obj->QColor::getCmykF(arg1, arg2, arg3, arg4, arg5);
  return 0;
}
int LuaBinder< QColor >::__LuaWrapCall__setBlueF (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  __lua__obj->QColor::setBlueF(arg1);
  return 0;
}
int LuaBinder< QColor >::__LuaWrapCall__colorNames (lua_State *L) {
  QStringList ret = QColor::colorNames();
  lqtL_passudata(L, new QStringList(ret), "QStringList*");
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__getRgb (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int * arg1 = static_cast<int *>(lua_touserdata(L, 2));
  int * arg2 = static_cast<int *>(lua_touserdata(L, 3));
  int * arg3 = static_cast<int *>(lua_touserdata(L, 4));
  int * arg4 = lua_isnumber(L, 5)?static_cast<int *>(lua_touserdata(L, 5)):static_cast< int * >(0);
  __lua__obj->QColor::getRgb(arg1, arg2, arg3, arg4);
  return 0;
}
int LuaBinder< QColor >::__LuaWrapCall__name (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QColor::name();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__setRgb__OverloadedVersion__1 (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int arg2 = lua_tointeger(L, 3);
  int arg3 = lua_tointeger(L, 4);
  int arg4 = lua_isnumber(L, 5)?lua_tointeger(L, 5):static_cast< int >(255);
  __lua__obj->QColor::setRgb(arg1, arg2, arg3, arg4);
  return 0;
}
int LuaBinder< QColor >::__LuaWrapCall__setRgb__OverloadedVersion__2 (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  unsigned int arg1 = lua_tointeger(L, 2);
  __lua__obj->QColor::setRgb(arg1);
  return 0;
}
int LuaBinder< QColor >::__LuaWrapCall__setRgb (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QColor*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1f49c40;
  } else {
    score[1] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1f4a160;
  } else {
    score[1] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1f4a510;
  } else {
    score[1] -= premium*premium;
  }
  if (lua_isnumber(L, 5)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x1f4a940;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QColor*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1f4f600;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__setRgb__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__setRgb__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__setRgb matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QColor >::__LuaWrapCall__setAlphaF (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  __lua__obj->QColor::setAlphaF(arg1);
  return 0;
}
int LuaBinder< QColor >::__LuaWrapCall__setCmykF (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  double arg2 = lua_tonumber(L, 3);
  double arg3 = lua_tonumber(L, 4);
  double arg4 = lua_tonumber(L, 5);
  double arg5 = lua_isnumber(L, 6)?lua_tonumber(L, 6):static_cast< double >(1.0e+0);
  __lua__obj->QColor::setCmykF(arg1, arg2, arg3, arg4, arg5);
  return 0;
}
int LuaBinder< QColor >::__LuaWrapCall__setBlue (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QColor::setBlue(arg1);
  return 0;
}
int LuaBinder< QColor >::__LuaWrapCall__greenF (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double ret = __lua__obj->QColor::greenF();
  lua_pushnumber(L, ret);
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__toHsv (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QColor ret = __lua__obj->QColor::toHsv();
  lqtL_passudata(L, new QColor(ret), "QColor*");
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__alphaF (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double ret = __lua__obj->QColor::alphaF();
  lua_pushnumber(L, ret);
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__toRgb (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QColor ret = __lua__obj->QColor::toRgb();
  lqtL_passudata(L, new QColor(ret), "QColor*");
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__new__OverloadedVersion__1 (lua_State *L) {
  QColor * ret = new LuaBinder< QColor >(L);
  lqtL_passudata(L, ret, "QColor*");
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__new__OverloadedVersion__2 (lua_State *L) {
  Qt::GlobalColor arg1 = static_cast<Qt::GlobalColor>(lqtL_toenum(L, 1, "Qt::GlobalColor"));
  QColor * ret = new LuaBinder< QColor >(L, arg1);
  lqtL_passudata(L, ret, "QColor*");
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__new__OverloadedVersion__3 (lua_State *L) {
  int arg1 = lua_tointeger(L, 1);
  int arg2 = lua_tointeger(L, 2);
  int arg3 = lua_tointeger(L, 3);
  int arg4 = lua_isnumber(L, 4)?lua_tointeger(L, 4):static_cast< int >(255);
  QColor * ret = new LuaBinder< QColor >(L, arg1, arg2, arg3, arg4);
  lqtL_passudata(L, ret, "QColor*");
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__new__OverloadedVersion__4 (lua_State *L) {
  unsigned int arg1 = lua_tointeger(L, 1);
  QColor * ret = new LuaBinder< QColor >(L, arg1);
  lqtL_passudata(L, ret, "QColor*");
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__new__OverloadedVersion__5 (lua_State *L) {
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 1), lua_objlen(L, 1));
  QColor * ret = new LuaBinder< QColor >(L, arg1);
  lqtL_passudata(L, ret, "QColor*");
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__new__OverloadedVersion__6 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  QColor * ret = new LuaBinder< QColor >(L, arg1);
  lqtL_passudata(L, ret, "QColor*");
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__new__OverloadedVersion__7 (lua_State *L) {
  const QColor& arg1 = **static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
  QColor * ret = new LuaBinder< QColor >(L, arg1);
  lqtL_passudata(L, ret, "QColor*");
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__new__OverloadedVersion__8 (lua_State *L) {
  QColor::Spec arg1 = static_cast<QColor::Spec>(lqtL_toenum(L, 1, "QColor::Spec"));
  QColor * ret = new LuaBinder< QColor >(L, arg1);
  lqtL_passudata(L, ret, "QColor*");
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__new (lua_State *L) {
  int score[9];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[2] = 0;
  if (lqtL_isenum(L, 1, "Qt::GlobalColor")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1f38060;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  if (lua_isnumber(L, 1)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1f38ab0;
  } else {
    score[3] -= premium*premium;
  }
  if (lua_isnumber(L, 2)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1f38fc0;
  } else {
    score[3] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1f39370;
  } else {
    score[3] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[3] += premium;
  } else if (true) {
    score[3] += premium-1; // table: 0x1f397a0;
  } else {
    score[3] -= premium*premium;
  }
  score[4] = 0;
  if (lua_isnumber(L, 1)) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x1f3a050;
  } else {
    score[4] -= premium*premium;
  }
  score[5] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[5] += premium;
  } else if (false) {
    score[5] += premium-1; // table: 0x1f3aac0;
  } else {
    score[5] -= premium*premium;
  }
  score[6] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x1f3b4e0;
  } else {
    score[6] -= premium*premium;
  }
  score[7] = 0;
  if (lqtL_testudata(L, 1, "QColor*")) {
    score[7] += premium;
  } else if (false) {
    score[7] += premium-1; // table: 0x1f3bf20;
  } else {
    score[7] -= premium*premium;
  }
  score[8] = 0;
  if (lqtL_isenum(L, 1, "QColor::Spec")) {
    score[8] += premium;
  } else if (false) {
    score[8] += premium-1; // table: 0x1f3c970;
  } else {
    score[8] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=8;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__new__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__new__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__new__OverloadedVersion__3(L); break;
    case 4: return __LuaWrapCall__new__OverloadedVersion__4(L); break;
    case 5: return __LuaWrapCall__new__OverloadedVersion__5(L); break;
    case 6: return __LuaWrapCall__new__OverloadedVersion__6(L); break;
    case 7: return __LuaWrapCall__new__OverloadedVersion__7(L); break;
    case 8: return __LuaWrapCall__new__OverloadedVersion__8(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__new matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QColor >::__LuaWrapCall__saturation (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QColor::saturation();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__alpha (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QColor::alpha();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__getHsv (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int * arg1 = static_cast<int *>(lua_touserdata(L, 2));
  int * arg2 = static_cast<int *>(lua_touserdata(L, 3));
  int * arg3 = static_cast<int *>(lua_touserdata(L, 4));
  int * arg4 = lua_isnumber(L, 5)?static_cast<int *>(lua_touserdata(L, 5)):static_cast< int * >(0);
  __lua__obj->QColor::getHsv(arg1, arg2, arg3, arg4);
  return 0;
}
int LuaBinder< QColor >::__LuaWrapCall__setRgbF (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  double arg2 = lua_tonumber(L, 3);
  double arg3 = lua_tonumber(L, 4);
  double arg4 = lua_isnumber(L, 5)?lua_tonumber(L, 5):static_cast< double >(1.0e+0);
  __lua__obj->QColor::setRgbF(arg1, arg2, arg3, arg4);
  return 0;
}
int LuaBinder< QColor >::__LuaWrapCall__magenta (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QColor::magenta();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__light (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_isnumber(L, 2)?lua_tointeger(L, 2):static_cast< int >(150);
  QColor ret = __lua__obj->QColor::light(arg1);
  lqtL_passudata(L, new QColor(ret), "QColor*");
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__hue (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QColor::hue();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__setHsvF (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  double arg2 = lua_tonumber(L, 3);
  double arg3 = lua_tonumber(L, 4);
  double arg4 = lua_isnumber(L, 5)?lua_tonumber(L, 5):static_cast< double >(1.0e+0);
  __lua__obj->QColor::setHsvF(arg1, arg2, arg3, arg4);
  return 0;
}
int LuaBinder< QColor >::__LuaWrapCall__blue (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QColor::blue();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__yellowF (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double ret = __lua__obj->QColor::yellowF();
  lua_pushnumber(L, ret);
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__setRedF (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  __lua__obj->QColor::setRedF(arg1);
  return 0;
}
int LuaBinder< QColor >::__LuaWrapCall__spec (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QColor::Spec ret = __lua__obj->QColor::spec();
  lqtL_pushenum(L, ret, "QColor::Spec");
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__saturationF (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double ret = __lua__obj->QColor::saturationF();
  lua_pushnumber(L, ret);
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__dark (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_isnumber(L, 2)?lua_tointeger(L, 2):static_cast< int >(200);
  QColor ret = __lua__obj->QColor::dark(arg1);
  lqtL_passudata(L, new QColor(ret), "QColor*");
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__blackF (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double ret = __lua__obj->QColor::blackF();
  lua_pushnumber(L, ret);
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__blueF (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double ret = __lua__obj->QColor::blueF();
  lua_pushnumber(L, ret);
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__red (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QColor::red();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__fromRgb__OverloadedVersion__1 (lua_State *L) {
  unsigned int arg1 = lua_tointeger(L, 1);
  QColor ret = QColor::fromRgb(arg1);
  lqtL_passudata(L, new QColor(ret), "QColor*");
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__fromRgb__OverloadedVersion__2 (lua_State *L) {
  int arg1 = lua_tointeger(L, 1);
  int arg2 = lua_tointeger(L, 2);
  int arg3 = lua_tointeger(L, 3);
  int arg4 = lua_isnumber(L, 4)?lua_tointeger(L, 4):static_cast< int >(255);
  QColor ret = QColor::fromRgb(arg1, arg2, arg3, arg4);
  lqtL_passudata(L, new QColor(ret), "QColor*");
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__fromRgb (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if (lua_isnumber(L, 1)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1f642e0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if (lua_isnumber(L, 1)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1f65760;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 2)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1f65c70;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1f66020;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x1f66450;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__fromRgb__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__fromRgb__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__fromRgb matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QColor >::__LuaWrapCall__fromRgba (lua_State *L) {
  unsigned int arg1 = lua_tointeger(L, 1);
  QColor ret = QColor::fromRgba(arg1);
  lqtL_passudata(L, new QColor(ret), "QColor*");
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__fromHsv (lua_State *L) {
  int arg1 = lua_tointeger(L, 1);
  int arg2 = lua_tointeger(L, 2);
  int arg3 = lua_tointeger(L, 3);
  int arg4 = lua_isnumber(L, 4)?lua_tointeger(L, 4):static_cast< int >(255);
  QColor ret = QColor::fromHsv(arg1, arg2, arg3, arg4);
  lqtL_passudata(L, new QColor(ret), "QColor*");
  return 1;
}
int LuaBinder< QColor >::__LuaWrapCall__getRgbF (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double * arg1 = static_cast<double *>(lua_touserdata(L, 2));
  double * arg2 = static_cast<double *>(lua_touserdata(L, 3));
  double * arg3 = static_cast<double *>(lua_touserdata(L, 4));
  double * arg4 = lua_isnumber(L, 5)?static_cast<double *>(lua_touserdata(L, 5)):static_cast< double * >(0);
  __lua__obj->QColor::getRgbF(arg1, arg2, arg3, arg4);
  return 0;
}
int LuaBinder< QColor >::__LuaWrapCall__getHsvF (lua_State *L) {
  QColor *& __lua__obj = *static_cast<QColor**>(lqtL_checkudata(L, 1, "QColor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double * arg1 = static_cast<double *>(lua_touserdata(L, 2));
  double * arg2 = static_cast<double *>(lua_touserdata(L, 3));
  double * arg3 = static_cast<double *>(lua_touserdata(L, 4));
  double * arg4 = lua_isnumber(L, 5)?static_cast<double *>(lua_touserdata(L, 5)):static_cast< double * >(0);
  __lua__obj->QColor::getHsvF(arg1, arg2, arg3, arg4);
  return 0;
}
int LuaBinder< QColor >::lqt_pushenum_Spec (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "Invalid");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "Invalid");
  lua_pushstring(L, "Rgb");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "Rgb");
  lua_pushstring(L, "Hsv");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "Hsv");
  lua_pushstring(L, "Cmyk");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "Cmyk");
  lua_pushcfunction(L, LuaBinder< QColor >::lqt_pushenum_Spec_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QColor::Spec");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QColor >::lqt_pushenum_Spec_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QColor::Spec>*) + sizeof(QFlags<QColor::Spec>));
  QFlags<QColor::Spec> *fl = static_cast<QFlags<QColor::Spec>*>( static_cast<void*>(&static_cast<QFlags<QColor::Spec>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QColor::Spec>(lqtL_toenum(L, i, "QColor::Spec"));
	}
	if (luaL_newmetatable(L, "QFlags<QColor::Spec>*")) {
		lua_pushstring(L, "QFlags<QColor::Spec>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int luaopen_QColor (lua_State *L) {
  if (luaL_newmetatable(L, "QColor*")) {
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__cyan);
    lua_setfield(L, -2, "cyan");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__fromCmyk);
    lua_setfield(L, -2, "fromCmyk");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__cyanF);
    lua_setfield(L, -2, "cyanF");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__redF);
    lua_setfield(L, -2, "redF");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__setCmyk);
    lua_setfield(L, -2, "setCmyk");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__isValid);
    lua_setfield(L, -2, "isValid");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__black);
    lua_setfield(L, -2, "black");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__getCmyk);
    lua_setfield(L, -2, "getCmyk");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__yellow);
    lua_setfield(L, -2, "yellow");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__setGreen);
    lua_setfield(L, -2, "setGreen");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__setAlpha);
    lua_setfield(L, -2, "setAlpha");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__convertTo);
    lua_setfield(L, -2, "convertTo");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__setHsv);
    lua_setfield(L, -2, "setHsv");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__toCmyk);
    lua_setfield(L, -2, "toCmyk");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__hueF);
    lua_setfield(L, -2, "hueF");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__fromCmykF);
    lua_setfield(L, -2, "fromCmykF");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__darker);
    lua_setfield(L, -2, "darker");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__setRgba);
    lua_setfield(L, -2, "setRgba");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__rgb);
    lua_setfield(L, -2, "rgb");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__setGreenF);
    lua_setfield(L, -2, "setGreenF");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__green);
    lua_setfield(L, -2, "green");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__valueF);
    lua_setfield(L, -2, "valueF");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__setNamedColor);
    lua_setfield(L, -2, "setNamedColor");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__fromHsvF);
    lua_setfield(L, -2, "fromHsvF");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__fromRgbF);
    lua_setfield(L, -2, "fromRgbF");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__setRed);
    lua_setfield(L, -2, "setRed");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__value);
    lua_setfield(L, -2, "value");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__lighter);
    lua_setfield(L, -2, "lighter");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__rgba);
    lua_setfield(L, -2, "rgba");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__magentaF);
    lua_setfield(L, -2, "magentaF");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__getCmykF);
    lua_setfield(L, -2, "getCmykF");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__setBlueF);
    lua_setfield(L, -2, "setBlueF");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__colorNames);
    lua_setfield(L, -2, "colorNames");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__getRgb);
    lua_setfield(L, -2, "getRgb");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__name);
    lua_setfield(L, -2, "name");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__setRgb);
    lua_setfield(L, -2, "setRgb");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__setAlphaF);
    lua_setfield(L, -2, "setAlphaF");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__setCmykF);
    lua_setfield(L, -2, "setCmykF");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__setBlue);
    lua_setfield(L, -2, "setBlue");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__greenF);
    lua_setfield(L, -2, "greenF");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__toHsv);
    lua_setfield(L, -2, "toHsv");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__alphaF);
    lua_setfield(L, -2, "alphaF");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__toRgb);
    lua_setfield(L, -2, "toRgb");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__new);
    lua_setfield(L, -2, "new");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__saturation);
    lua_setfield(L, -2, "saturation");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__alpha);
    lua_setfield(L, -2, "alpha");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__getHsv);
    lua_setfield(L, -2, "getHsv");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__setRgbF);
    lua_setfield(L, -2, "setRgbF");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__magenta);
    lua_setfield(L, -2, "magenta");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__light);
    lua_setfield(L, -2, "light");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__hue);
    lua_setfield(L, -2, "hue");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__setHsvF);
    lua_setfield(L, -2, "setHsvF");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__blue);
    lua_setfield(L, -2, "blue");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__yellowF);
    lua_setfield(L, -2, "yellowF");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__setRedF);
    lua_setfield(L, -2, "setRedF");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__spec);
    lua_setfield(L, -2, "spec");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__saturationF);
    lua_setfield(L, -2, "saturationF");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__dark);
    lua_setfield(L, -2, "dark");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__blackF);
    lua_setfield(L, -2, "blackF");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__blueF);
    lua_setfield(L, -2, "blueF");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__red);
    lua_setfield(L, -2, "red");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__fromRgb);
    lua_setfield(L, -2, "fromRgb");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__fromRgba);
    lua_setfield(L, -2, "fromRgba");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__fromHsv);
    lua_setfield(L, -2, "fromHsv");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__getRgbF);
    lua_setfield(L, -2, "getRgbF");
    lua_pushcfunction(L, LuaBinder< QColor >::__LuaWrapCall__getHsvF);
    lua_setfield(L, -2, "getHsvF");
    LuaBinder< QColor >::lqt_pushenum_Spec(L);
    lua_setfield(L, -2, "Spec");
    lua_newtable(L);
    lua_setfield(L, -2, "__base");
    lua_pushcfunction(L, lqtL_newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, lqtL_index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, lqtL_gc);
    lua_setfield(L, -2, "__gc");
    lua_pushstring(L, "QColor");
    lua_setfield(L, -2, "__qtype");
		lua_setglobal(L, "QColor");
  } else {
		lua_pop(L, 1);
	}
  return 0;
}
