#include "lqt_common.hpp"
#include <QTextCursor>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QTextCursor > : public QTextCursor {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__insertBlock__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__insertBlock__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__insertBlock__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__insertBlock (lua_State *L);
  static int __LuaWrapCall__insertFragment (lua_State *L);
  static int __LuaWrapCall__clearSelection (lua_State *L);
  static int __LuaWrapCall__insertImage__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__insertImage__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__insertImage__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__insertImage (lua_State *L);
  static int __LuaWrapCall__insertFrame (lua_State *L);
  static int __LuaWrapCall__setPosition (lua_State *L);
  static int __LuaWrapCall__atEnd (lua_State *L);
  static int __LuaWrapCall__insertHtml (lua_State *L);
  static int __LuaWrapCall__block (lua_State *L);
  static int __LuaWrapCall__atBlockStart (lua_State *L);
  static int __LuaWrapCall__insertTable__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__insertTable__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__insertTable (lua_State *L);
  static int __LuaWrapCall__isCopyOf (lua_State *L);
  static int __LuaWrapCall__selectedText (lua_State *L);
  static int __LuaWrapCall__endEditBlock (lua_State *L);
  static int __LuaWrapCall__blockFormat (lua_State *L);
  static int __LuaWrapCall__blockNumber (lua_State *L);
  static int __LuaWrapCall__position (lua_State *L);
  static int __LuaWrapCall__delete (lua_State *L);
  static int __LuaWrapCall__deletePreviousChar (lua_State *L);
  static int __LuaWrapCall__currentTable (lua_State *L);
  static int __LuaWrapCall__insertText__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__insertText__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__insertText (lua_State *L);
  static int __LuaWrapCall__hasComplexSelection (lua_State *L);
  static int __LuaWrapCall__movePosition (lua_State *L);
  static int __LuaWrapCall__columnNumber (lua_State *L);
  static int __LuaWrapCall__selectionStart (lua_State *L);
  static int __LuaWrapCall__select (lua_State *L);
  static int __LuaWrapCall__anchor (lua_State *L);
  static int __LuaWrapCall__joinPreviousEditBlock (lua_State *L);
  static int __LuaWrapCall__selection (lua_State *L);
  static int __LuaWrapCall__hasSelection (lua_State *L);
  static int __LuaWrapCall__isNull (lua_State *L);
  static int __LuaWrapCall__mergeCharFormat (lua_State *L);
  static int __LuaWrapCall__removeSelectedText (lua_State *L);
  static int __LuaWrapCall__createList__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__createList__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__createList (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__4 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__5 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__6 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__7 (lua_State *L);
  static int __LuaWrapCall__new (lua_State *L);
  static int __LuaWrapCall__setBlockCharFormat (lua_State *L);
  static int __LuaWrapCall__charFormat (lua_State *L);
  static int __LuaWrapCall__currentList (lua_State *L);
  static int __LuaWrapCall__currentFrame (lua_State *L);
  static int __LuaWrapCall__selectionEnd (lua_State *L);
  static int __LuaWrapCall__mergeBlockCharFormat (lua_State *L);
  static int __LuaWrapCall__atStart (lua_State *L);
  static int __LuaWrapCall__insertList__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__insertList__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__insertList (lua_State *L);
  static int __LuaWrapCall__setCharFormat (lua_State *L);
  static int __LuaWrapCall__atBlockEnd (lua_State *L);
  static int __LuaWrapCall__setBlockFormat (lua_State *L);
  static int __LuaWrapCall__blockCharFormat (lua_State *L);
  static int __LuaWrapCall__selectedTableCells (lua_State *L);
  static int __LuaWrapCall__deleteChar (lua_State *L);
  static int __LuaWrapCall__mergeBlockFormat (lua_State *L);
  static int __LuaWrapCall__beginEditBlock (lua_State *L);
  static int lqt_pushenum_MoveMode (lua_State *L);
  static int lqt_pushenum_MoveMode_QFLAGS_CREATOR (lua_State *L);
  static int lqt_pushenum_MoveOperation (lua_State *L);
  static int lqt_pushenum_MoveOperation_QFLAGS_CREATOR (lua_State *L);
  static int lqt_pushenum_SelectionType (lua_State *L);
  static int lqt_pushenum_SelectionType_QFLAGS_CREATOR (lua_State *L);
  LuaBinder< QTextCursor > (lua_State *l):QTextCursor(), L(l) {}
  LuaBinder< QTextCursor > (lua_State *l, QTextDocument * arg1):QTextCursor(arg1), L(l) {}
  LuaBinder< QTextCursor > (lua_State *l, QTextDocumentPrivate * arg1, int arg2):QTextCursor(arg1, arg2), L(l) {}
  LuaBinder< QTextCursor > (lua_State *l, QTextFrame * arg1):QTextCursor(arg1), L(l) {}
  LuaBinder< QTextCursor > (lua_State *l, const QTextBlock& arg1):QTextCursor(arg1), L(l) {}
  LuaBinder< QTextCursor > (lua_State *l, QTextCursorPrivate * arg1):QTextCursor(arg1), L(l) {}
  LuaBinder< QTextCursor > (lua_State *l, const QTextCursor& arg1):QTextCursor(arg1), L(l) {}
};

extern "C" int luaopen_QTextCursor (lua_State *L);
