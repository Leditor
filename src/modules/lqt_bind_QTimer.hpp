#include "lqt_common.hpp"
#include <QTimer>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QTimer > : public QTimer {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__tr (lua_State *L);
  static int __LuaWrapCall__delete (lua_State *L);
  static int __LuaWrapCall__new (lua_State *L);
  static int __LuaWrapCall__singleShot (lua_State *L);
  static int __LuaWrapCall__isActive (lua_State *L);
  static int __LuaWrapCall__timerId (lua_State *L);
  static int __LuaWrapCall__start__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__start__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__start (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__trUtf8 (lua_State *L);
  static int __LuaWrapCall__stop (lua_State *L);
  static int __LuaWrapCall__setInterval (lua_State *L);
  static int __LuaWrapCall__interval (lua_State *L);
  static int __LuaWrapCall__setSingleShot (lua_State *L);
  static int __LuaWrapCall__metaObject (lua_State *L);
  static int __LuaWrapCall__isSingleShot (lua_State *L);
  bool event (QEvent * arg1);
protected:
  void customEvent (QEvent * arg1);
public:
  bool eventFilter (QObject * arg1, QEvent * arg2);
protected:
  void timerEvent (QTimerEvent * arg1);
public:
protected:
  void childEvent (QChildEvent * arg1);
public:
protected:
  void disconnectNotify (const char * arg1);
public:
  const QMetaObject * metaObject () const;
protected:
  void connectNotify (const char * arg1);
public:
  ~LuaBinder< QTimer > ();
  LuaBinder< QTimer > (lua_State *l, QObject * arg1):QTimer(arg1), L(l) {}
};

extern "C" int luaopen_QTimer (lua_State *L);
