#include "lqt_common.hpp"
#include <QFileDialog>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QFileDialog > : public QFileDialog {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__confirmOverwrite (lua_State *L);
  static int __LuaWrapCall__selectFilter (lua_State *L);
  static int __LuaWrapCall__saveState (lua_State *L);
  static int __LuaWrapCall__resolveSymlinks (lua_State *L);
  static int __LuaWrapCall__setAcceptMode (lua_State *L);
  static int __LuaWrapCall__setSidebarUrls (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__trUtf8 (lua_State *L);
  static int __LuaWrapCall__filters (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__tr (lua_State *L);
  static int __LuaWrapCall__sidebarUrls (lua_State *L);
  static int __LuaWrapCall__setConfirmOverwrite (lua_State *L);
  static int __LuaWrapCall__setViewMode (lua_State *L);
  static int __LuaWrapCall__acceptMode (lua_State *L);
  static int __LuaWrapCall__setHistory (lua_State *L);
  static int __LuaWrapCall__fileMode (lua_State *L);
  static int __LuaWrapCall__setDefaultSuffix (lua_State *L);
  static int __LuaWrapCall__getOpenFileName (lua_State *L);
  static int __LuaWrapCall__setReadOnly (lua_State *L);
  static int __LuaWrapCall__setLabelText (lua_State *L);
  static int __LuaWrapCall__selectFile (lua_State *L);
  static int __LuaWrapCall__setFilters (lua_State *L);
  static int __LuaWrapCall__selectedFiles (lua_State *L);
  static int __LuaWrapCall__viewMode (lua_State *L);
  static int __LuaWrapCall__setFileMode (lua_State *L);
  static int __LuaWrapCall__getExistingDirectory (lua_State *L);
  static int __LuaWrapCall__delete (lua_State *L);
  static int __LuaWrapCall__metaObject (lua_State *L);
  static int __LuaWrapCall__getSaveFileName (lua_State *L);
  static int __LuaWrapCall__setResolveSymlinks (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__new (lua_State *L);
  static int __LuaWrapCall__selectedFilter (lua_State *L);
  static int __LuaWrapCall__directory (lua_State *L);
  static int __LuaWrapCall__isReadOnly (lua_State *L);
  static int __LuaWrapCall__defaultSuffix (lua_State *L);
  static int __LuaWrapCall__setFilter (lua_State *L);
  static int __LuaWrapCall__setDirectory__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__setDirectory__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__setDirectory (lua_State *L);
  static int __LuaWrapCall__setItemDelegate (lua_State *L);
  static int __LuaWrapCall__restoreState (lua_State *L);
  static int __LuaWrapCall__itemDelegate (lua_State *L);
  static int __LuaWrapCall__history (lua_State *L);
  static int __LuaWrapCall__setIconProvider (lua_State *L);
  static int __LuaWrapCall__iconProvider (lua_State *L);
  static int __LuaWrapCall__proxyModel (lua_State *L);
  static int __LuaWrapCall__labelText (lua_State *L);
  static int __LuaWrapCall__setProxyModel (lua_State *L);
  static int __LuaWrapCall__getOpenFileNames (lua_State *L);
protected:
  void styleChange (QStyle& arg1);
public:
protected:
  void focusInEvent (QFocusEvent * arg1);
public:
protected:
  void keyPressEvent (QKeyEvent * arg1);
public:
  const QMetaObject * metaObject () const;
  int devType () const;
  void setVisible (bool arg1);
protected:
  void keyReleaseEvent (QKeyEvent * arg1);
public:
  QPaintEngine * paintEngine () const;
protected:
  void dragLeaveEvent (QDragLeaveEvent * arg1);
public:
protected:
  void mousePressEvent (QMouseEvent * arg1);
public:
protected:
  void dragEnterEvent (QDragEnterEvent * arg1);
public:
protected:
  void closeEvent (QCloseEvent * arg1);
public:
protected:
  void enterEvent (QEvent * arg1);
public:
protected:
  void accept ();
public:
protected:
  void tabletEvent (QTabletEvent * arg1);
public:
protected:
  void dragMoveEvent (QDragMoveEvent * arg1);
public:
protected:
  void changeEvent (QEvent * arg1);
public:
protected:
  void moveEvent (QMoveEvent * arg1);
public:
protected:
  void dropEvent (QDropEvent * arg1);
public:
  QSize sizeHint () const;
  QVariant inputMethodQuery (Qt::InputMethodQuery arg1) const;
protected:
  void showEvent (QShowEvent * arg1);
public:
protected:
  void windowActivationChange (bool arg1);
public:
protected:
  void paintEvent (QPaintEvent * arg1);
public:
protected:
  void contextMenuEvent (QContextMenuEvent * arg1);
public:
protected:
  void mouseDoubleClickEvent (QMouseEvent * arg1);
public:
protected:
  void enabledChange (bool arg1);
public:
protected:
  void disconnectNotify (const char * arg1);
public:
  int heightForWidth (int arg1) const;
protected:
  void fontChange (const QFont& arg1);
public:
protected:
  void actionEvent (QActionEvent * arg1);
public:
protected:
  void timerEvent (QTimerEvent * arg1);
public:
protected:
  void mouseMoveEvent (QMouseEvent * arg1);
public:
protected:
  int metric (QPaintDevice::PaintDeviceMetric arg1) const;
public:
protected:
  void childEvent (QChildEvent * arg1);
public:
protected:
  void connectNotify (const char * arg1);
public:
protected:
  void mouseReleaseEvent (QMouseEvent * arg1);
public:
protected:
  void focusOutEvent (QFocusEvent * arg1);
public:
protected:
  bool eventFilter (QObject * arg1, QEvent * arg2);
public:
protected:
  void inputMethodEvent (QInputMethodEvent * arg1);
public:
protected:
  void wheelEvent (QWheelEvent * arg1);
public:
protected:
  void resizeEvent (QResizeEvent * arg1);
public:
protected:
  void paletteChange (const QPalette& arg1);
public:
protected:
  void languageChange ();
public:
protected:
  bool event (QEvent * arg1);
public:
protected:
  void hideEvent (QHideEvent * arg1);
public:
protected:
  bool focusNextPrevChild (bool arg1);
public:
protected:
  void done (int arg1);
public:
  QSize minimumSizeHint () const;
protected:
  void leaveEvent (QEvent * arg1);
public:
protected:
  void customEvent (QEvent * arg1);
public:
  void reject ();
  ~LuaBinder< QFileDialog > ();
  static int lqt_pushenum_ViewMode (lua_State *L);
  static int lqt_pushenum_ViewMode_QFLAGS_CREATOR (lua_State *L);
  static int lqt_pushenum_FileMode (lua_State *L);
  static int lqt_pushenum_FileMode_QFLAGS_CREATOR (lua_State *L);
  static int lqt_pushenum_AcceptMode (lua_State *L);
  static int lqt_pushenum_AcceptMode_QFLAGS_CREATOR (lua_State *L);
  static int lqt_pushenum_DialogLabel (lua_State *L);
  static int lqt_pushenum_DialogLabel_QFLAGS_CREATOR (lua_State *L);
  static int lqt_pushenum_Option (lua_State *L);
  static int lqt_pushenum_Option_QFLAGS_CREATOR (lua_State *L);
  LuaBinder< QFileDialog > (lua_State *l, QWidget * arg1, QFlags<Qt::WindowType> arg2):QFileDialog(arg1, arg2), L(l) {}
  LuaBinder< QFileDialog > (lua_State *l, QWidget * arg1, const QString& arg2, const QString& arg3, const QString& arg4):QFileDialog(arg1, arg2, arg3, arg4), L(l) {}
};

extern "C" int luaopen_QFileDialog (lua_State *L);
