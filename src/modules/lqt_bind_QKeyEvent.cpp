#include "lqt_bind_QKeyEvent.hpp"

int LuaBinder< QKeyEvent >::__LuaWrapCall__delete (lua_State *L) {
  QKeyEvent *& __lua__obj = *static_cast<QKeyEvent**>(lqtL_checkudata(L, 1, "QKeyEvent*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  delete __lua__obj;
  __lua__obj = 0;
  return 0;
}
int LuaBinder< QKeyEvent >::__LuaWrapCall__createExtendedKeyEvent (lua_State *L) {
  QEvent::Type arg1 = static_cast<QEvent::Type>(lqtL_toenum(L, 1, "QEvent::Type"));
  int arg2 = lua_tointeger(L, 2);
  QFlags<Qt::KeyboardModifier> arg3 = **static_cast<QFlags<Qt::KeyboardModifier>**>(lqtL_checkudata(L, 3, "QFlags<Qt::KeyboardModifier>*"));
  unsigned int arg4 = lua_tointeger(L, 4);
  unsigned int arg5 = lua_tointeger(L, 5);
  unsigned int arg6 = lua_tointeger(L, 6);
  const QString& arg7 = (lua_type(L, 7)==LUA_TSTRING)?QString::fromAscii(lua_tostring(L, 7), lua_objlen(L, 7)):QString();
  bool arg8 = lua_isboolean(L, 8)?(bool)lua_toboolean(L, 8):false;
  short unsigned int arg9 = lua_isnumber(L, 9)?lua_tointeger(L, 9):static_cast< short unsigned int >(1);
  QKeyEvent * ret = QKeyEvent::createExtendedKeyEvent(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9);
  lqtL_pushudata(L, ret, "QKeyEvent*");
  return 1;
}
int LuaBinder< QKeyEvent >::__LuaWrapCall__text (lua_State *L) {
  QKeyEvent *& __lua__obj = *static_cast<QKeyEvent**>(lqtL_checkudata(L, 1, "QKeyEvent*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QKeyEvent::text();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QKeyEvent >::__LuaWrapCall__modifiers (lua_State *L) {
  QKeyEvent *& __lua__obj = *static_cast<QKeyEvent**>(lqtL_checkudata(L, 1, "QKeyEvent*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<Qt::KeyboardModifier> ret = __lua__obj->QKeyEvent::modifiers();
  lqtL_passudata(L, new QFlags<Qt::KeyboardModifier>(ret), "QFlags<Qt::KeyboardModifier>*");
  return 1;
}
int LuaBinder< QKeyEvent >::__LuaWrapCall__hasExtendedInfo (lua_State *L) {
  QKeyEvent *& __lua__obj = *static_cast<QKeyEvent**>(lqtL_checkudata(L, 1, "QKeyEvent*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QKeyEvent::hasExtendedInfo();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QKeyEvent >::__LuaWrapCall__nativeVirtualKey (lua_State *L) {
  QKeyEvent *& __lua__obj = *static_cast<QKeyEvent**>(lqtL_checkudata(L, 1, "QKeyEvent*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  unsigned int ret = __lua__obj->QKeyEvent::nativeVirtualKey();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QKeyEvent >::__LuaWrapCall__count (lua_State *L) {
  QKeyEvent *& __lua__obj = *static_cast<QKeyEvent**>(lqtL_checkudata(L, 1, "QKeyEvent*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QKeyEvent::count();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QKeyEvent >::__LuaWrapCall__isAutoRepeat (lua_State *L) {
  QKeyEvent *& __lua__obj = *static_cast<QKeyEvent**>(lqtL_checkudata(L, 1, "QKeyEvent*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QKeyEvent::isAutoRepeat();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QKeyEvent >::__LuaWrapCall__nativeModifiers (lua_State *L) {
  QKeyEvent *& __lua__obj = *static_cast<QKeyEvent**>(lqtL_checkudata(L, 1, "QKeyEvent*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  unsigned int ret = __lua__obj->QKeyEvent::nativeModifiers();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QKeyEvent >::__LuaWrapCall__matches (lua_State *L) {
  QKeyEvent *& __lua__obj = *static_cast<QKeyEvent**>(lqtL_checkudata(L, 1, "QKeyEvent*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QKeySequence::StandardKey arg1 = static_cast<QKeySequence::StandardKey>(lqtL_toenum(L, 2, "QKeySequence::StandardKey"));
  bool ret = __lua__obj->QKeyEvent::matches(arg1);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QKeyEvent >::__LuaWrapCall__key (lua_State *L) {
  QKeyEvent *& __lua__obj = *static_cast<QKeyEvent**>(lqtL_checkudata(L, 1, "QKeyEvent*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QKeyEvent::key();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QKeyEvent >::__LuaWrapCall__nativeScanCode (lua_State *L) {
  QKeyEvent *& __lua__obj = *static_cast<QKeyEvent**>(lqtL_checkudata(L, 1, "QKeyEvent*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  unsigned int ret = __lua__obj->QKeyEvent::nativeScanCode();
  lua_pushinteger(L, ret);
  return 1;
}
LuaBinder< QKeyEvent >::  ~LuaBinder< QKeyEvent > () {
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QKeyEvent*");
  lua_getfield(L, -1, "~QKeyEvent");

  if (lua_isfunction(L, -1)) {
    lua_insert(L, -2);
    lua_pcall(L, 1, 1, 0);
  } else {
  }
  lua_settop(L, oldtop);
}
int luaopen_QKeyEvent (lua_State *L) {
  if (luaL_newmetatable(L, "QKeyEvent*")) {
    lua_pushcfunction(L, LuaBinder< QKeyEvent >::__LuaWrapCall__delete);
    lua_setfield(L, -2, "delete");
    lua_pushcfunction(L, LuaBinder< QKeyEvent >::__LuaWrapCall__createExtendedKeyEvent);
    lua_setfield(L, -2, "createExtendedKeyEvent");
    lua_pushcfunction(L, LuaBinder< QKeyEvent >::__LuaWrapCall__text);
    lua_setfield(L, -2, "text");
    lua_pushcfunction(L, LuaBinder< QKeyEvent >::__LuaWrapCall__modifiers);
    lua_setfield(L, -2, "modifiers");
    lua_pushcfunction(L, LuaBinder< QKeyEvent >::__LuaWrapCall__hasExtendedInfo);
    lua_setfield(L, -2, "hasExtendedInfo");
    lua_pushcfunction(L, LuaBinder< QKeyEvent >::__LuaWrapCall__nativeVirtualKey);
    lua_setfield(L, -2, "nativeVirtualKey");
    lua_pushcfunction(L, LuaBinder< QKeyEvent >::__LuaWrapCall__count);
    lua_setfield(L, -2, "count");
    lua_pushcfunction(L, LuaBinder< QKeyEvent >::__LuaWrapCall__isAutoRepeat);
    lua_setfield(L, -2, "isAutoRepeat");
    lua_pushcfunction(L, LuaBinder< QKeyEvent >::__LuaWrapCall__nativeModifiers);
    lua_setfield(L, -2, "nativeModifiers");
    lua_pushcfunction(L, LuaBinder< QKeyEvent >::__LuaWrapCall__matches);
    lua_setfield(L, -2, "matches");
    lua_pushcfunction(L, LuaBinder< QKeyEvent >::__LuaWrapCall__key);
    lua_setfield(L, -2, "key");
    lua_pushcfunction(L, LuaBinder< QKeyEvent >::__LuaWrapCall__nativeScanCode);
    lua_setfield(L, -2, "nativeScanCode");
    lua_newtable(L);
    lua_pushboolean(L, 1);
    lua_setfield(L, -2, "QInputEvent*");
    lua_pushboolean(L, 0);
    lua_setfield(L, -2, "QEvent*");
    lua_setfield(L, -2, "__base");
    lua_pushcfunction(L, lqtL_newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, lqtL_index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, lqtL_gc);
    lua_setfield(L, -2, "__gc");
    lua_pushstring(L, "QKeyEvent");
    lua_setfield(L, -2, "__qtype");
		lua_setglobal(L, "QKeyEvent");
  } else {
		lua_pop(L, 1);
	}
  return 0;
}
