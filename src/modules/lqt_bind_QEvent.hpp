#include "lqt_common.hpp"
#include <QEvent>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QEvent > : public QEvent {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__delete (lua_State *L);
  static int __LuaWrapCall__type (lua_State *L);
  static int __LuaWrapCall__setAccepted (lua_State *L);
  static int __LuaWrapCall__accept (lua_State *L);
  static int __LuaWrapCall__isAccepted (lua_State *L);
  static int __LuaWrapCall__spontaneous (lua_State *L);
  static int __LuaWrapCall__ignore (lua_State *L);
  ~LuaBinder< QEvent > ();
  static int lqt_pushenum_Type (lua_State *L);
  static int lqt_pushenum_Type_QFLAGS_CREATOR (lua_State *L);
  LuaBinder< QEvent > (lua_State *l, QEvent::Type arg1):QEvent(arg1), L(l) {}
};

extern "C" int luaopen_QEvent (lua_State *L);
