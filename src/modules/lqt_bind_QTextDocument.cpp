#include "lqt_bind_QTextDocument.hpp"

int LuaBinder< QTextDocument >::__LuaWrapCall__objectForFormat (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QTextFormat& arg1 = **static_cast<QTextFormat**>(lqtL_checkudata(L, 2, "QTextFormat*"));
  QTextObject * ret = __lua__obj->QTextDocument::objectForFormat(arg1);
  lqtL_pushudata(L, ret, "QTextObject*");
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__defaultFont (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFont ret = __lua__obj->QTextDocument::defaultFont();
  lqtL_passudata(L, new QFont(ret), "QFont*");
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__isRedoAvailable (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QTextDocument::isRedoAvailable();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__useDesignMetrics (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QTextDocument::useDesignMetrics();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__print (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QPrinter * arg1 = *static_cast<QPrinter**>(lqtL_checkudata(L, 2, "QPrinter*"));
  __lua__obj->QTextDocument::print(arg1);
  return 0;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__delete (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  delete __lua__obj;
  __lua__obj = 0;
  return 0;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__setUseDesignMetrics (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QTextDocument::setUseDesignMetrics(arg1);
  return 0;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QTextDocument::trUtf8(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QTextDocument::trUtf8(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__trUtf8 (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0xe892c0;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x73af00;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x672570;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xe59900;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x625050;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__trUtf8__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__trUtf8__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__trUtf8 matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__object (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  QTextObject * ret = __lua__obj->QTextDocument::object(arg1);
  lqtL_pushudata(L, ret, "QTextObject*");
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__setUndoRedoEnabled (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QTextDocument::setUndoRedoEnabled(arg1);
  return 0;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__size (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QSizeF ret = __lua__obj->QTextDocument::size();
  lqtL_passudata(L, new QSizeF(ret), "QSizeF*");
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QTextDocument::tr(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QTextDocument::tr(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__tr (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x7e8b80;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0xe79e10;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x7338d0;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x71f3d0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xef4310;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__tr__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__tr__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__tr matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__rootFrame (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextFrame * ret = __lua__obj->QTextDocument::rootFrame();
  lqtL_pushudata(L, ret, "QTextFrame*");
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__addResource (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  const QUrl& arg2 = **static_cast<QUrl**>(lqtL_checkudata(L, 3, "QUrl*"));
  const QVariant& arg3 = **static_cast<QVariant**>(lqtL_checkudata(L, 4, "QVariant*"));
  __lua__obj->QTextDocument::addResource(arg1, arg2, arg3);
  return 0;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__setPlainText (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QTextDocument::setPlainText(arg1);
  return 0;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__setDefaultStyleSheet (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QTextDocument::setDefaultStyleSheet(arg1);
  return 0;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__isEmpty (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QTextDocument::isEmpty();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__find__OverloadedVersion__1 (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  int arg2 = lua_isnumber(L, 3)?lua_tointeger(L, 3):static_cast< int >(0);
  QFlags<QTextDocument::FindFlag> arg3 = lqtL_testudata(L, 4, "QFlags<QTextDocument::FindFlag>*")?**static_cast<QFlags<QTextDocument::FindFlag>**>(lqtL_checkudata(L, 4, "QFlags<QTextDocument::FindFlag>*")):static_cast< QFlags<QTextDocument::FindFlag> >(0);
  QTextCursor ret = __lua__obj->QTextDocument::find(arg1, arg2, arg3);
  lqtL_passudata(L, new QTextCursor(ret), "QTextCursor*");
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__find__OverloadedVersion__2 (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  const QTextCursor& arg2 = **static_cast<QTextCursor**>(lqtL_checkudata(L, 3, "QTextCursor*"));
  QFlags<QTextDocument::FindFlag> arg3 = lqtL_testudata(L, 4, "QFlags<QTextDocument::FindFlag>*")?**static_cast<QFlags<QTextDocument::FindFlag>**>(lqtL_checkudata(L, 4, "QFlags<QTextDocument::FindFlag>*")):static_cast< QFlags<QTextDocument::FindFlag> >(0);
  QTextCursor ret = __lua__obj->QTextDocument::find(arg1, arg2, arg3);
  lqtL_passudata(L, new QTextCursor(ret), "QTextCursor*");
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__find__OverloadedVersion__3 (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QRegExp& arg1 = **static_cast<QRegExp**>(lqtL_checkudata(L, 2, "QRegExp*"));
  int arg2 = lua_isnumber(L, 3)?lua_tointeger(L, 3):static_cast< int >(0);
  QFlags<QTextDocument::FindFlag> arg3 = lqtL_testudata(L, 4, "QFlags<QTextDocument::FindFlag>*")?**static_cast<QFlags<QTextDocument::FindFlag>**>(lqtL_checkudata(L, 4, "QFlags<QTextDocument::FindFlag>*")):static_cast< QFlags<QTextDocument::FindFlag> >(0);
  QTextCursor ret = __lua__obj->QTextDocument::find(arg1, arg2, arg3);
  lqtL_passudata(L, new QTextCursor(ret), "QTextCursor*");
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__find__OverloadedVersion__4 (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QRegExp& arg1 = **static_cast<QRegExp**>(lqtL_checkudata(L, 2, "QRegExp*"));
  const QTextCursor& arg2 = **static_cast<QTextCursor**>(lqtL_checkudata(L, 3, "QTextCursor*"));
  QFlags<QTextDocument::FindFlag> arg3 = lqtL_testudata(L, 4, "QFlags<QTextDocument::FindFlag>*")?**static_cast<QFlags<QTextDocument::FindFlag>**>(lqtL_checkudata(L, 4, "QFlags<QTextDocument::FindFlag>*")):static_cast< QFlags<QTextDocument::FindFlag> >(0);
  QTextCursor ret = __lua__obj->QTextDocument::find(arg1, arg2, arg3);
  lqtL_passudata(L, new QTextCursor(ret), "QTextCursor*");
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__find (lua_State *L) {
  int score[5];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QTextDocument*")?premium:-premium*premium;
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0xd884e0;
  } else {
    score[1] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0xd9f120;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_testudata(L, 4, "QFlags<QTextDocument::FindFlag>*")) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0xd32120;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QTextDocument*")?premium:-premium*premium;
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xc5bbe0;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_testudata(L, 3, "QTextCursor*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xe93790;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_testudata(L, 4, "QFlags<QTextDocument::FindFlag>*")) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0xb1f880;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  score[3] += lqtL_testudata(L, 1, "QTextDocument*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QRegExp*")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x88b400;
  } else {
    score[3] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[3] += premium;
  } else if (true) {
    score[3] += premium-1; // table: 0x81f230;
  } else {
    score[3] -= premium*premium;
  }
  if (lqtL_testudata(L, 4, "QFlags<QTextDocument::FindFlag>*")) {
    score[3] += premium;
  } else if (true) {
    score[3] += premium-1; // table: 0x793930;
  } else {
    score[3] -= premium*premium;
  }
  score[4] = 0;
  score[4] += lqtL_testudata(L, 1, "QTextDocument*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QRegExp*")) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x741940;
  } else {
    score[4] -= premium*premium;
  }
  if (lqtL_testudata(L, 3, "QTextCursor*")) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x6f66a0;
  } else {
    score[4] -= premium*premium;
  }
  if (lqtL_testudata(L, 4, "QFlags<QTextDocument::FindFlag>*")) {
    score[4] += premium;
  } else if (true) {
    score[4] += premium-1; // table: 0x618ee0;
  } else {
    score[4] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=4;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__find__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__find__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__find__OverloadedVersion__3(L); break;
    case 4: return __LuaWrapCall__find__OverloadedVersion__4(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__find matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__isUndoRedoEnabled (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QTextDocument::isUndoRedoEnabled();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__begin (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextBlock ret = __lua__obj->QTextDocument::begin();
  lqtL_passudata(L, new QTextBlock(ret), "QTextBlock*");
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__isUndoAvailable (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QTextDocument::isUndoAvailable();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__adjustSize (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QTextDocument::adjustSize();
  return 0;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__markContentsDirty (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int arg2 = lua_tointeger(L, 3);
  __lua__obj->QTextDocument::markContentsDirty(arg1, arg2);
  return 0;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__clone (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QObject * arg1 = lqtL_testudata(L, 2, "QObject*")?*static_cast<QObject**>(lqtL_checkudata(L, 2, "QObject*")):static_cast< QObject * >(0);
  QTextDocument * ret = __lua__obj->QTextDocument::clone(arg1);
  lqtL_pushudata(L, ret, "QTextDocument*");
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__defaultStyleSheet (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QTextDocument::defaultStyleSheet();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__toPlainText (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QTextDocument::toPlainText();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__setModified (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = lua_isboolean(L, 2)?(bool)lua_toboolean(L, 2):true;
  __lua__obj->QTextDocument::setModified(arg1);
  return 0;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__maximumBlockCount (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QTextDocument::maximumBlockCount();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__appendUndoItem (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QAbstractUndoItem * arg1 = *static_cast<QAbstractUndoItem**>(lqtL_checkudata(L, 2, "QAbstractUndoItem*"));
  __lua__obj->QTextDocument::appendUndoItem(arg1);
  return 0;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__pageCount (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QTextDocument::pageCount();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__documentLayout (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QAbstractTextDocumentLayout * ret = __lua__obj->QTextDocument::documentLayout();
  lqtL_pushudata(L, ret, "QAbstractTextDocumentLayout*");
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__allFormats (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QVector<QTextFormat> ret = __lua__obj->QTextDocument::allFormats();
  lqtL_passudata(L, new QVector<QTextFormat>(ret), "QVector<QTextFormat>*");
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__setMaximumBlockCount (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QTextDocument::setMaximumBlockCount(arg1);
  return 0;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__setDefaultTextOption (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QTextOption& arg1 = **static_cast<QTextOption**>(lqtL_checkudata(L, 2, "QTextOption*"));
  __lua__obj->QTextDocument::setDefaultTextOption(arg1);
  return 0;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__defaultTextOption (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextOption ret = __lua__obj->QTextDocument::defaultTextOption();
  lqtL_passudata(L, new QTextOption(ret), "QTextOption*");
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__isModified (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QTextDocument::isModified();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__metaInformation (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextDocument::MetaInformation arg1 = static_cast<QTextDocument::MetaInformation>(lqtL_toenum(L, 2, "QTextDocument::MetaInformation"));
  QString ret = __lua__obj->QTextDocument::metaInformation(arg1);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__resource (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  const QUrl& arg2 = **static_cast<QUrl**>(lqtL_checkudata(L, 3, "QUrl*"));
  QVariant ret = __lua__obj->QTextDocument::resource(arg1, arg2);
  lqtL_passudata(L, new QVariant(ret), "QVariant*");
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__end (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextBlock ret = __lua__obj->QTextDocument::end();
  lqtL_passudata(L, new QTextBlock(ret), "QTextBlock*");
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__setDefaultFont (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QFont& arg1 = **static_cast<QFont**>(lqtL_checkudata(L, 2, "QFont*"));
  __lua__obj->QTextDocument::setDefaultFont(arg1);
  return 0;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__toHtml (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QByteArray& arg1 = (lua_type(L, 2)==LUA_TSTRING)?QByteArray(lua_tostring(L, 2), lua_objlen(L, 2)):QByteArray();
  QString ret = __lua__obj->QTextDocument::toHtml(arg1);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__metaObject (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QMetaObject * ret = __lua__obj->QTextDocument::metaObject();
  lqtL_pushudata(L, ret, "QMetaObject*");
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__new__OverloadedVersion__1 (lua_State *L) {
  QObject * arg1 = lqtL_testudata(L, 1, "QObject*")?*static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*")):static_cast< QObject * >(0);
  QTextDocument * ret = new LuaBinder< QTextDocument >(L, arg1);
  lqtL_passudata(L, ret, "QTextDocument*");
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__new__OverloadedVersion__2 (lua_State *L) {
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 1), lua_objlen(L, 1));
  QObject * arg2 = lqtL_testudata(L, 2, "QObject*")?*static_cast<QObject**>(lqtL_checkudata(L, 2, "QObject*")):static_cast< QObject * >(0);
  QTextDocument * ret = new LuaBinder< QTextDocument >(L, arg1, arg2);
  lqtL_passudata(L, ret, "QTextDocument*");
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__new (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if (lqtL_testudata(L, 1, "QObject*")) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x569070;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x580d30;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_testudata(L, 2, "QObject*")) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x51f8c0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__new__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__new__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__new matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__clear (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QTextDocument::clear();
  return 0;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__setDocumentLayout (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QAbstractTextDocumentLayout * arg1 = *static_cast<QAbstractTextDocumentLayout**>(lqtL_checkudata(L, 2, "QAbstractTextDocumentLayout*"));
  __lua__obj->QTextDocument::setDocumentLayout(arg1);
  return 0;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__redo__OverloadedVersion__1 (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextCursor * arg1 = *static_cast<QTextCursor**>(lqtL_checkudata(L, 2, "QTextCursor*"));
  __lua__obj->QTextDocument::redo(arg1);
  return 0;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__redo__OverloadedVersion__2 (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QTextDocument::redo();
  return 0;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__redo (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QTextDocument*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QTextCursor*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x9e94c0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QTextDocument*")?premium:-premium*premium;
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__redo__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__redo__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__redo matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__findBlock (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  QTextBlock ret = __lua__obj->QTextDocument::findBlock(arg1);
  lqtL_passudata(L, new QTextBlock(ret), "QTextBlock*");
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__setTextWidth (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  __lua__obj->QTextDocument::setTextWidth(arg1);
  return 0;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__textWidth (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double ret = __lua__obj->QTextDocument::textWidth();
  lua_pushnumber(L, ret);
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__idealWidth (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double ret = __lua__obj->QTextDocument::idealWidth();
  lua_pushnumber(L, ret);
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__setHtml (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QTextDocument::setHtml(arg1);
  return 0;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__blockCount (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QTextDocument::blockCount();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__setMetaInformation (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextDocument::MetaInformation arg1 = static_cast<QTextDocument::MetaInformation>(lqtL_toenum(L, 2, "QTextDocument::MetaInformation"));
  const QString& arg2 = QString::fromAscii(lua_tostring(L, 3), lua_objlen(L, 3));
  __lua__obj->QTextDocument::setMetaInformation(arg1, arg2);
  return 0;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__frameAt (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  QTextFrame * ret = __lua__obj->QTextDocument::frameAt(arg1);
  lqtL_pushudata(L, ret, "QTextFrame*");
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__drawContents (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QPainter * arg1 = *static_cast<QPainter**>(lqtL_checkudata(L, 2, "QPainter*"));
  const QRectF& arg2 = lqtL_testudata(L, 3, "QRectF*")?**static_cast<QRectF**>(lqtL_checkudata(L, 3, "QRectF*")):QRectF();
  __lua__obj->QTextDocument::drawContents(arg1, arg2);
  return 0;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__undo__OverloadedVersion__1 (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextCursor * arg1 = *static_cast<QTextCursor**>(lqtL_checkudata(L, 2, "QTextCursor*"));
  __lua__obj->QTextDocument::undo(arg1);
  return 0;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__undo__OverloadedVersion__2 (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QTextDocument::undo();
  return 0;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__undo (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QTextDocument*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QTextCursor*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0xd42890;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QTextDocument*")?premium:-premium*premium;
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__undo__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__undo__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__undo matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__docHandle (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextDocumentPrivate * ret = __lua__obj->QTextDocument::docHandle();
  lqtL_pushudata(L, ret, "QTextDocumentPrivate*");
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__pageSize (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QSizeF ret = __lua__obj->QTextDocument::pageSize();
  lqtL_passudata(L, new QSizeF(ret), "QSizeF*");
  return 1;
}
int LuaBinder< QTextDocument >::__LuaWrapCall__setPageSize (lua_State *L) {
  QTextDocument *& __lua__obj = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QSizeF& arg1 = **static_cast<QSizeF**>(lqtL_checkudata(L, 2, "QSizeF*"));
  __lua__obj->QTextDocument::setPageSize(arg1);
  return 0;
}
bool LuaBinder< QTextDocument >::eventFilter (QObject * arg1, QEvent * arg2) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextDocument*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "eventFilter");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QObject*");
  lqtL_pushudata(L, arg2, "QEvent*");
  if (lua_isfunction(L, -2-2)) {
    lua_pcall(L, 2+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QObject::eventFilter(arg1, arg2);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
const QMetaObject * LuaBinder< QTextDocument >::metaObject () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextDocument*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "metaObject");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QTextDocument::metaObject();
  }
  const QMetaObject * ret = *static_cast<QMetaObject**>(lqtL_checkudata(L, -1, "QMetaObject*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QTextDocument >::connectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextDocument*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "connectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::connectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QTextDocument >::event (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextDocument*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "event");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QObject::event(arg1);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QTextDocument >::clear () {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextDocument*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "clear");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QTextDocument::clear();
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QTextDocument >::timerEvent (QTimerEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextDocument*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "timerEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QTimerEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::timerEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QTextDocument >::disconnectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextDocument*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "disconnectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::disconnectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
QVariant LuaBinder< QTextDocument >::loadResource (int arg1, const QUrl& arg2) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextDocument*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "loadResource");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushinteger(L, arg1);
  lqtL_pushudata(L, &(arg2), "QUrl*");
  if (lua_isfunction(L, -2-2)) {
    lua_pcall(L, 2+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QTextDocument::loadResource(arg1, arg2);
  }
  QVariant ret = **static_cast<QVariant**>(lqtL_checkudata(L, -1, "QVariant*"));
  lua_settop(L, oldtop);
  return ret;
}
QTextObject * LuaBinder< QTextDocument >::createObject (const QTextFormat& arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextDocument*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "createObject");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QTextFormat*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QTextDocument::createObject(arg1);
  }
  QTextObject * ret = *static_cast<QTextObject**>(lqtL_checkudata(L, -1, "QTextObject*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QTextDocument >::childEvent (QChildEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextDocument*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "childEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QChildEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::childEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QTextDocument >::customEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextDocument*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "customEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::customEvent(arg1);
  }
  lua_settop(L, oldtop);
}
LuaBinder< QTextDocument >::  ~LuaBinder< QTextDocument > () {
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextDocument*");
  lua_getfield(L, -1, "~QTextDocument");

  if (lua_isfunction(L, -1)) {
    lua_insert(L, -2);
    lua_pcall(L, 1, 1, 0);
  } else {
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QTextDocument >::lqt_pushenum_MetaInformation (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "DocumentTitle");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "DocumentTitle");
  lua_pushcfunction(L, LuaBinder< QTextDocument >::lqt_pushenum_MetaInformation_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QTextDocument::MetaInformation");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QTextDocument >::lqt_pushenum_MetaInformation_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QTextDocument::MetaInformation>*) + sizeof(QFlags<QTextDocument::MetaInformation>));
  QFlags<QTextDocument::MetaInformation> *fl = static_cast<QFlags<QTextDocument::MetaInformation>*>( static_cast<void*>(&static_cast<QFlags<QTextDocument::MetaInformation>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QTextDocument::MetaInformation>(lqtL_toenum(L, i, "QTextDocument::MetaInformation"));
	}
	if (luaL_newmetatable(L, "QFlags<QTextDocument::MetaInformation>*")) {
		lua_pushstring(L, "QFlags<QTextDocument::MetaInformation>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinder< QTextDocument >::lqt_pushenum_FindFlag (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "FindBackward");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "FindBackward");
  lua_pushstring(L, "FindCaseSensitively");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "FindCaseSensitively");
  lua_pushstring(L, "FindWholeWords");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "FindWholeWords");
  lua_pushcfunction(L, LuaBinder< QTextDocument >::lqt_pushenum_FindFlag_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QTextDocument::FindFlag");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QTextDocument >::lqt_pushenum_FindFlag_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QTextDocument::FindFlag>*) + sizeof(QFlags<QTextDocument::FindFlag>));
  QFlags<QTextDocument::FindFlag> *fl = static_cast<QFlags<QTextDocument::FindFlag>*>( static_cast<void*>(&static_cast<QFlags<QTextDocument::FindFlag>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QTextDocument::FindFlag>(lqtL_toenum(L, i, "QTextDocument::FindFlag"));
	}
	if (luaL_newmetatable(L, "QFlags<QTextDocument::FindFlag>*")) {
		lua_pushstring(L, "QFlags<QTextDocument::FindFlag>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinder< QTextDocument >::lqt_pushenum_ResourceType (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "HtmlResource");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "HtmlResource");
  lua_pushstring(L, "ImageResource");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "ImageResource");
  lua_pushstring(L, "StyleSheetResource");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "StyleSheetResource");
  lua_pushstring(L, "UserResource");
  lua_rawseti(L, enum_table, 100);
  lua_pushinteger(L, 100);
  lua_setfield(L, enum_table, "UserResource");
  lua_pushcfunction(L, LuaBinder< QTextDocument >::lqt_pushenum_ResourceType_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QTextDocument::ResourceType");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QTextDocument >::lqt_pushenum_ResourceType_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QTextDocument::ResourceType>*) + sizeof(QFlags<QTextDocument::ResourceType>));
  QFlags<QTextDocument::ResourceType> *fl = static_cast<QFlags<QTextDocument::ResourceType>*>( static_cast<void*>(&static_cast<QFlags<QTextDocument::ResourceType>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QTextDocument::ResourceType>(lqtL_toenum(L, i, "QTextDocument::ResourceType"));
	}
	if (luaL_newmetatable(L, "QFlags<QTextDocument::ResourceType>*")) {
		lua_pushstring(L, "QFlags<QTextDocument::ResourceType>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int luaopen_QTextDocument (lua_State *L) {
  if (luaL_newmetatable(L, "QTextDocument*")) {
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__objectForFormat);
    lua_setfield(L, -2, "objectForFormat");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__defaultFont);
    lua_setfield(L, -2, "defaultFont");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__isRedoAvailable);
    lua_setfield(L, -2, "isRedoAvailable");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__useDesignMetrics);
    lua_setfield(L, -2, "useDesignMetrics");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__print);
    lua_setfield(L, -2, "print");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__delete);
    lua_setfield(L, -2, "delete");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__setUseDesignMetrics);
    lua_setfield(L, -2, "setUseDesignMetrics");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__trUtf8);
    lua_setfield(L, -2, "trUtf8");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__object);
    lua_setfield(L, -2, "object");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__setUndoRedoEnabled);
    lua_setfield(L, -2, "setUndoRedoEnabled");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__size);
    lua_setfield(L, -2, "size");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__tr);
    lua_setfield(L, -2, "tr");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__rootFrame);
    lua_setfield(L, -2, "rootFrame");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__addResource);
    lua_setfield(L, -2, "addResource");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__setPlainText);
    lua_setfield(L, -2, "setPlainText");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__setDefaultStyleSheet);
    lua_setfield(L, -2, "setDefaultStyleSheet");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__isEmpty);
    lua_setfield(L, -2, "isEmpty");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__find);
    lua_setfield(L, -2, "find");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__isUndoRedoEnabled);
    lua_setfield(L, -2, "isUndoRedoEnabled");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__begin);
    lua_setfield(L, -2, "begin");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__isUndoAvailable);
    lua_setfield(L, -2, "isUndoAvailable");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__adjustSize);
    lua_setfield(L, -2, "adjustSize");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__markContentsDirty);
    lua_setfield(L, -2, "markContentsDirty");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__clone);
    lua_setfield(L, -2, "clone");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__defaultStyleSheet);
    lua_setfield(L, -2, "defaultStyleSheet");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__toPlainText);
    lua_setfield(L, -2, "toPlainText");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__setModified);
    lua_setfield(L, -2, "setModified");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__maximumBlockCount);
    lua_setfield(L, -2, "maximumBlockCount");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__appendUndoItem);
    lua_setfield(L, -2, "appendUndoItem");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__pageCount);
    lua_setfield(L, -2, "pageCount");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__documentLayout);
    lua_setfield(L, -2, "documentLayout");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__allFormats);
    lua_setfield(L, -2, "allFormats");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__setMaximumBlockCount);
    lua_setfield(L, -2, "setMaximumBlockCount");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__setDefaultTextOption);
    lua_setfield(L, -2, "setDefaultTextOption");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__defaultTextOption);
    lua_setfield(L, -2, "defaultTextOption");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__isModified);
    lua_setfield(L, -2, "isModified");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__metaInformation);
    lua_setfield(L, -2, "metaInformation");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__resource);
    lua_setfield(L, -2, "resource");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__end);
    lua_setfield(L, -2, "end");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__setDefaultFont);
    lua_setfield(L, -2, "setDefaultFont");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__toHtml);
    lua_setfield(L, -2, "toHtml");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__metaObject);
    lua_setfield(L, -2, "metaObject");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__new);
    lua_setfield(L, -2, "new");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__clear);
    lua_setfield(L, -2, "clear");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__setDocumentLayout);
    lua_setfield(L, -2, "setDocumentLayout");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__redo);
    lua_setfield(L, -2, "redo");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__findBlock);
    lua_setfield(L, -2, "findBlock");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__setTextWidth);
    lua_setfield(L, -2, "setTextWidth");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__textWidth);
    lua_setfield(L, -2, "textWidth");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__idealWidth);
    lua_setfield(L, -2, "idealWidth");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__setHtml);
    lua_setfield(L, -2, "setHtml");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__blockCount);
    lua_setfield(L, -2, "blockCount");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__setMetaInformation);
    lua_setfield(L, -2, "setMetaInformation");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__frameAt);
    lua_setfield(L, -2, "frameAt");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__drawContents);
    lua_setfield(L, -2, "drawContents");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__undo);
    lua_setfield(L, -2, "undo");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__docHandle);
    lua_setfield(L, -2, "docHandle");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__pageSize);
    lua_setfield(L, -2, "pageSize");
    lua_pushcfunction(L, LuaBinder< QTextDocument >::__LuaWrapCall__setPageSize);
    lua_setfield(L, -2, "setPageSize");
    LuaBinder< QTextDocument >::lqt_pushenum_MetaInformation(L);
    lua_setfield(L, -2, "MetaInformation");
    LuaBinder< QTextDocument >::lqt_pushenum_FindFlag(L);
    lua_setfield(L, -2, "FindFlag");
    LuaBinder< QTextDocument >::lqt_pushenum_ResourceType(L);
    lua_setfield(L, -2, "ResourceType");
    lua_newtable(L);
    lua_pushboolean(L, 1);
    lua_setfield(L, -2, "QObject*");
    lua_setfield(L, -2, "__base");
    lua_pushcfunction(L, lqtL_newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, lqtL_index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, lqtL_gc);
    lua_setfield(L, -2, "__gc");
    lua_pushstring(L, "QTextDocument");
    lua_setfield(L, -2, "__qtype");
		lua_setglobal(L, "QTextDocument");
  } else {
		lua_pop(L, 1);
	}
  return 0;
}
