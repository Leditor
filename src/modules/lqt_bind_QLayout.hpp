#include "lqt_common.hpp"
#include <QLayout>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QLayout > : public QLayout {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__indexOf (lua_State *L);
  static int __LuaWrapCall__parentWidget (lua_State *L);
  static int __LuaWrapCall__minimumSize (lua_State *L);
  static int __LuaWrapCall__update (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__trUtf8 (lua_State *L);
  static int __LuaWrapCall__totalMaximumSize (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__tr (lua_State *L);
  static int __LuaWrapCall__isEmpty (lua_State *L);
  static int __LuaWrapCall__setAlignment__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__setAlignment__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__setAlignment (lua_State *L);
  static int __LuaWrapCall__expandingDirections (lua_State *L);
  static int __LuaWrapCall__addWidget (lua_State *L);
  static int __LuaWrapCall__removeWidget (lua_State *L);
  static int __LuaWrapCall__activate (lua_State *L);
  static int __LuaWrapCall__setMargin (lua_State *L);
  static int __LuaWrapCall__delete (lua_State *L);
  static int __LuaWrapCall__totalMinimumSize (lua_State *L);
  static int __LuaWrapCall__layout (lua_State *L);
  static int __LuaWrapCall__margin (lua_State *L);
  static int __LuaWrapCall__menuBar (lua_State *L);
  static int __LuaWrapCall__geometry (lua_State *L);
  static int __LuaWrapCall__spacing (lua_State *L);
  static int __LuaWrapCall__sizeConstraint (lua_State *L);
  static int __LuaWrapCall__setContentsMargins (lua_State *L);
  static int __LuaWrapCall__metaObject (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__new (lua_State *L);
  static int __LuaWrapCall__contentsRect (lua_State *L);
  static int __LuaWrapCall__getContentsMargins (lua_State *L);
  static int __LuaWrapCall__invalidate (lua_State *L);
  static int __LuaWrapCall__maximumSize (lua_State *L);
  static int __LuaWrapCall__setMenuBar (lua_State *L);
  static int __LuaWrapCall__totalSizeHint (lua_State *L);
  static int __LuaWrapCall__setSpacing (lua_State *L);
  static int __LuaWrapCall__closestAcceptableSize (lua_State *L);
  static int __LuaWrapCall__setSizeConstraint (lua_State *L);
  static int __LuaWrapCall__isEnabled (lua_State *L);
  static int __LuaWrapCall__removeItem (lua_State *L);
  static int __LuaWrapCall__setEnabled (lua_State *L);
  static int __LuaWrapCall__totalHeightForWidth (lua_State *L);
  QLayout * layout ();
  QLayoutItem * takeAt (int arg1);
  const QMetaObject * metaObject () const;
  QLayoutItem * itemAt (int arg1) const;
  int heightForWidth (int arg1) const;
  int minimumHeightForWidth (int arg1) const;
  void invalidate ();
  QWidget * widget ();
  bool hasHeightForWidth () const;
protected:
  void childEvent (QChildEvent * arg1);
public:
  QSize maximumSize () const;
  int indexOf (QWidget * arg1) const;
  void addItem (QLayoutItem * arg1);
  bool eventFilter (QObject * arg1, QEvent * arg2);
  QSpacerItem * spacerItem ();
  QSize sizeHint () const;
  QSize minimumSize () const;
  bool isEmpty () const;
protected:
  void connectNotify (const char * arg1);
public:
  bool event (QEvent * arg1);
protected:
  void timerEvent (QTimerEvent * arg1);
public:
  int count () const;
protected:
  void disconnectNotify (const char * arg1);
public:
  QFlags<Qt::Orientation> expandingDirections () const;
  QRect geometry () const;
  void setGeometry (const QRect& arg1);
protected:
  void customEvent (QEvent * arg1);
public:
  ~LuaBinder< QLayout > ();
  static int lqt_pushenum_SizeConstraint (lua_State *L);
  static int lqt_pushenum_SizeConstraint_QFLAGS_CREATOR (lua_State *L);
  LuaBinder< QLayout > (lua_State *l, QWidget * arg1):QLayout(arg1), L(l) {}
  LuaBinder< QLayout > (lua_State *l):QLayout(), L(l) {}
};

extern "C" int luaopen_QLayout (lua_State *L);
