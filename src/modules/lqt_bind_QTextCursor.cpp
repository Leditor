#include "lqt_bind_QTextCursor.hpp"

int LuaBinder< QTextCursor >::__LuaWrapCall__insertBlock__OverloadedVersion__1 (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QTextCursor::insertBlock();
  return 0;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__insertBlock__OverloadedVersion__2 (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QTextBlockFormat& arg1 = **static_cast<QTextBlockFormat**>(lqtL_checkudata(L, 2, "QTextBlockFormat*"));
  __lua__obj->QTextCursor::insertBlock(arg1);
  return 0;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__insertBlock__OverloadedVersion__3 (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QTextBlockFormat& arg1 = **static_cast<QTextBlockFormat**>(lqtL_checkudata(L, 2, "QTextBlockFormat*"));
  const QTextCharFormat& arg2 = **static_cast<QTextCharFormat**>(lqtL_checkudata(L, 3, "QTextCharFormat*"));
  __lua__obj->QTextCursor::insertBlock(arg1, arg2);
  return 0;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__insertBlock (lua_State *L) {
  int score[4];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QTextCursor*")?premium:-premium*premium;
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QTextCursor*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QTextBlockFormat*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xe9ae20;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  score[3] += lqtL_testudata(L, 1, "QTextCursor*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QTextBlockFormat*")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0xe844e0;
  } else {
    score[3] -= premium*premium;
  }
  if (lqtL_testudata(L, 3, "QTextCharFormat*")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0xe89160;
  } else {
    score[3] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=3;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__insertBlock__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__insertBlock__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__insertBlock__OverloadedVersion__3(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__insertBlock matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__insertFragment (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QTextDocumentFragment& arg1 = **static_cast<QTextDocumentFragment**>(lqtL_checkudata(L, 2, "QTextDocumentFragment*"));
  __lua__obj->QTextCursor::insertFragment(arg1);
  return 0;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__clearSelection (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QTextCursor::clearSelection();
  return 0;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__insertImage__OverloadedVersion__1 (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QTextImageFormat& arg1 = **static_cast<QTextImageFormat**>(lqtL_checkudata(L, 2, "QTextImageFormat*"));
  QTextFrameFormat::Position arg2 = static_cast<QTextFrameFormat::Position>(lqtL_toenum(L, 3, "QTextFrameFormat::Position"));
  __lua__obj->QTextCursor::insertImage(arg1, arg2);
  return 0;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__insertImage__OverloadedVersion__2 (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QTextImageFormat& arg1 = **static_cast<QTextImageFormat**>(lqtL_checkudata(L, 2, "QTextImageFormat*"));
  __lua__obj->QTextCursor::insertImage(arg1);
  return 0;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__insertImage__OverloadedVersion__3 (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QTextCursor::insertImage(arg1);
  return 0;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__insertImage (lua_State *L) {
  int score[4];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QTextCursor*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QTextImageFormat*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0xd7de10;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_isenum(L, 3, "QTextFrameFormat::Position")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0xd7f820;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QTextCursor*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QTextImageFormat*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xd3f710;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  score[3] += lqtL_testudata(L, 1, "QTextCursor*")?premium:-premium*premium;
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0xd529d0;
  } else {
    score[3] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=3;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__insertImage__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__insertImage__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__insertImage__OverloadedVersion__3(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__insertImage matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__insertFrame (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QTextFrameFormat& arg1 = **static_cast<QTextFrameFormat**>(lqtL_checkudata(L, 2, "QTextFrameFormat*"));
  QTextFrame * ret = __lua__obj->QTextCursor::insertFrame(arg1);
  lqtL_pushudata(L, ret, "QTextFrame*");
  return 1;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__setPosition (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  QTextCursor::MoveMode arg2 = lqtL_isenum(L, 3, "QTextCursor::MoveMode")?static_cast<QTextCursor::MoveMode>(lqtL_toenum(L, 3, "QTextCursor::MoveMode")):QTextCursor::MoveAnchor;
  __lua__obj->QTextCursor::setPosition(arg1, arg2);
  return 0;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__atEnd (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QTextCursor::atEnd();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__insertHtml (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QTextCursor::insertHtml(arg1);
  return 0;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__block (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextBlock ret = __lua__obj->QTextCursor::block();
  lqtL_passudata(L, new QTextBlock(ret), "QTextBlock*");
  return 1;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__atBlockStart (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QTextCursor::atBlockStart();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__insertTable__OverloadedVersion__1 (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int arg2 = lua_tointeger(L, 3);
  const QTextTableFormat& arg3 = **static_cast<QTextTableFormat**>(lqtL_checkudata(L, 4, "QTextTableFormat*"));
  QTextTable * ret = __lua__obj->QTextCursor::insertTable(arg1, arg2, arg3);
  lqtL_pushudata(L, ret, "QTextTable*");
  return 1;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__insertTable__OverloadedVersion__2 (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int arg2 = lua_tointeger(L, 3);
  QTextTable * ret = __lua__obj->QTextCursor::insertTable(arg1, arg2);
  lqtL_pushudata(L, ret, "QTextTable*");
  return 1;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__insertTable (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QTextCursor*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0xe60510;
  } else {
    score[1] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x968060;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_testudata(L, 4, "QTextTableFormat*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x96f710;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QTextCursor*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x965a70;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xda4850;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__insertTable__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__insertTable__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__insertTable matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__isCopyOf (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QTextCursor& arg1 = **static_cast<QTextCursor**>(lqtL_checkudata(L, 2, "QTextCursor*"));
  bool ret = __lua__obj->QTextCursor::isCopyOf(arg1);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__selectedText (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QTextCursor::selectedText();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__endEditBlock (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QTextCursor::endEditBlock();
  return 0;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__blockFormat (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextBlockFormat ret = __lua__obj->QTextCursor::blockFormat();
  lqtL_passudata(L, new QTextBlockFormat(ret), "QTextBlockFormat*");
  return 1;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__blockNumber (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QTextCursor::blockNumber();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__position (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QTextCursor::position();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__delete (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  delete __lua__obj;
  __lua__obj = 0;
  return 0;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__deletePreviousChar (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QTextCursor::deletePreviousChar();
  return 0;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__currentTable (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextTable * ret = __lua__obj->QTextCursor::currentTable();
  lqtL_pushudata(L, ret, "QTextTable*");
  return 1;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__insertText__OverloadedVersion__1 (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QTextCursor::insertText(arg1);
  return 0;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__insertText__OverloadedVersion__2 (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  const QTextCharFormat& arg2 = **static_cast<QTextCharFormat**>(lqtL_checkudata(L, 3, "QTextCharFormat*"));
  __lua__obj->QTextCursor::insertText(arg1, arg2);
  return 0;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__insertText (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QTextCursor*")?premium:-premium*premium;
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0xa238b0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QTextCursor*")?premium:-premium*premium;
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xc82810;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_testudata(L, 3, "QTextCharFormat*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xa4c2c0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__insertText__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__insertText__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__insertText matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__hasComplexSelection (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QTextCursor::hasComplexSelection();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__movePosition (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextCursor::MoveOperation arg1 = static_cast<QTextCursor::MoveOperation>(lqtL_toenum(L, 2, "QTextCursor::MoveOperation"));
  QTextCursor::MoveMode arg2 = lqtL_isenum(L, 3, "QTextCursor::MoveMode")?static_cast<QTextCursor::MoveMode>(lqtL_toenum(L, 3, "QTextCursor::MoveMode")):QTextCursor::MoveAnchor;
  int arg3 = lua_isnumber(L, 4)?lua_tointeger(L, 4):static_cast< int >(1);
  bool ret = __lua__obj->QTextCursor::movePosition(arg1, arg2, arg3);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__columnNumber (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QTextCursor::columnNumber();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__selectionStart (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QTextCursor::selectionStart();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__select (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextCursor::SelectionType arg1 = static_cast<QTextCursor::SelectionType>(lqtL_toenum(L, 2, "QTextCursor::SelectionType"));
  __lua__obj->QTextCursor::select(arg1);
  return 0;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__anchor (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QTextCursor::anchor();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__joinPreviousEditBlock (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QTextCursor::joinPreviousEditBlock();
  return 0;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__selection (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextDocumentFragment ret = __lua__obj->QTextCursor::selection();
  lqtL_passudata(L, new QTextDocumentFragment(ret), "QTextDocumentFragment*");
  return 1;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__hasSelection (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QTextCursor::hasSelection();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__isNull (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QTextCursor::isNull();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__mergeCharFormat (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QTextCharFormat& arg1 = **static_cast<QTextCharFormat**>(lqtL_checkudata(L, 2, "QTextCharFormat*"));
  __lua__obj->QTextCursor::mergeCharFormat(arg1);
  return 0;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__removeSelectedText (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QTextCursor::removeSelectedText();
  return 0;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__createList__OverloadedVersion__1 (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QTextListFormat& arg1 = **static_cast<QTextListFormat**>(lqtL_checkudata(L, 2, "QTextListFormat*"));
  QTextList * ret = __lua__obj->QTextCursor::createList(arg1);
  lqtL_pushudata(L, ret, "QTextList*");
  return 1;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__createList__OverloadedVersion__2 (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextListFormat::Style arg1 = static_cast<QTextListFormat::Style>(lqtL_toenum(L, 2, "QTextListFormat::Style"));
  QTextList * ret = __lua__obj->QTextCursor::createList(arg1);
  lqtL_pushudata(L, ret, "QTextList*");
  return 1;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__createList (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QTextCursor*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QTextListFormat*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0xe48ce0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QTextCursor*")?premium:-premium*premium;
  if (lqtL_isenum(L, 2, "QTextListFormat::Style")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xe49280;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__createList__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__createList__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__createList matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__new__OverloadedVersion__1 (lua_State *L) {
  QTextCursor * ret = new LuaBinder< QTextCursor >(L);
  lqtL_passudata(L, ret, "QTextCursor*");
  return 1;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__new__OverloadedVersion__2 (lua_State *L) {
  QTextDocument * arg1 = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
  QTextCursor * ret = new LuaBinder< QTextCursor >(L, arg1);
  lqtL_passudata(L, ret, "QTextCursor*");
  return 1;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__new__OverloadedVersion__3 (lua_State *L) {
  QTextDocumentPrivate * arg1 = *static_cast<QTextDocumentPrivate**>(lqtL_checkudata(L, 1, "QTextDocumentPrivate*"));
  int arg2 = lua_tointeger(L, 2);
  QTextCursor * ret = new LuaBinder< QTextCursor >(L, arg1, arg2);
  lqtL_passudata(L, ret, "QTextCursor*");
  return 1;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__new__OverloadedVersion__4 (lua_State *L) {
  QTextFrame * arg1 = *static_cast<QTextFrame**>(lqtL_checkudata(L, 1, "QTextFrame*"));
  QTextCursor * ret = new LuaBinder< QTextCursor >(L, arg1);
  lqtL_passudata(L, ret, "QTextCursor*");
  return 1;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__new__OverloadedVersion__5 (lua_State *L) {
  const QTextBlock& arg1 = **static_cast<QTextBlock**>(lqtL_checkudata(L, 1, "QTextBlock*"));
  QTextCursor * ret = new LuaBinder< QTextCursor >(L, arg1);
  lqtL_passudata(L, ret, "QTextCursor*");
  return 1;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__new__OverloadedVersion__6 (lua_State *L) {
  QTextCursorPrivate * arg1 = *static_cast<QTextCursorPrivate**>(lqtL_checkudata(L, 1, "QTextCursorPrivate*"));
  QTextCursor * ret = new LuaBinder< QTextCursor >(L, arg1);
  lqtL_passudata(L, ret, "QTextCursor*");
  return 1;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__new__OverloadedVersion__7 (lua_State *L) {
  const QTextCursor& arg1 = **static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
  QTextCursor * ret = new LuaBinder< QTextCursor >(L, arg1);
  lqtL_passudata(L, ret, "QTextCursor*");
  return 1;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__new (lua_State *L) {
  int score[8];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[2] = 0;
  if (lqtL_testudata(L, 1, "QTextDocument*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x817440;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  if (lqtL_testudata(L, 1, "QTextDocumentPrivate*")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x8cbb70;
  } else {
    score[3] -= premium*premium;
  }
  if (lua_isnumber(L, 2)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x54c880;
  } else {
    score[3] -= premium*premium;
  }
  score[4] = 0;
  if (lqtL_testudata(L, 1, "QTextFrame*")) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x8d43d0;
  } else {
    score[4] -= premium*premium;
  }
  score[5] = 0;
  if (lqtL_testudata(L, 1, "QTextBlock*")) {
    score[5] += premium;
  } else if (false) {
    score[5] += premium-1; // table: 0x899340;
  } else {
    score[5] -= premium*premium;
  }
  score[6] = 0;
  if (lqtL_testudata(L, 1, "QTextCursorPrivate*")) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0xa175f0;
  } else {
    score[6] -= premium*premium;
  }
  score[7] = 0;
  if (lqtL_testudata(L, 1, "QTextCursor*")) {
    score[7] += premium;
  } else if (false) {
    score[7] += premium-1; // table: 0xa03eb0;
  } else {
    score[7] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=7;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__new__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__new__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__new__OverloadedVersion__3(L); break;
    case 4: return __LuaWrapCall__new__OverloadedVersion__4(L); break;
    case 5: return __LuaWrapCall__new__OverloadedVersion__5(L); break;
    case 6: return __LuaWrapCall__new__OverloadedVersion__6(L); break;
    case 7: return __LuaWrapCall__new__OverloadedVersion__7(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__new matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__setBlockCharFormat (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QTextCharFormat& arg1 = **static_cast<QTextCharFormat**>(lqtL_checkudata(L, 2, "QTextCharFormat*"));
  __lua__obj->QTextCursor::setBlockCharFormat(arg1);
  return 0;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__charFormat (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextCharFormat ret = __lua__obj->QTextCursor::charFormat();
  lqtL_passudata(L, new QTextCharFormat(ret), "QTextCharFormat*");
  return 1;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__currentList (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextList * ret = __lua__obj->QTextCursor::currentList();
  lqtL_pushudata(L, ret, "QTextList*");
  return 1;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__currentFrame (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextFrame * ret = __lua__obj->QTextCursor::currentFrame();
  lqtL_pushudata(L, ret, "QTextFrame*");
  return 1;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__selectionEnd (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QTextCursor::selectionEnd();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__mergeBlockCharFormat (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QTextCharFormat& arg1 = **static_cast<QTextCharFormat**>(lqtL_checkudata(L, 2, "QTextCharFormat*"));
  __lua__obj->QTextCursor::mergeBlockCharFormat(arg1);
  return 0;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__atStart (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QTextCursor::atStart();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__insertList__OverloadedVersion__1 (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QTextListFormat& arg1 = **static_cast<QTextListFormat**>(lqtL_checkudata(L, 2, "QTextListFormat*"));
  QTextList * ret = __lua__obj->QTextCursor::insertList(arg1);
  lqtL_pushudata(L, ret, "QTextList*");
  return 1;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__insertList__OverloadedVersion__2 (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextListFormat::Style arg1 = static_cast<QTextListFormat::Style>(lqtL_toenum(L, 2, "QTextListFormat::Style"));
  QTextList * ret = __lua__obj->QTextCursor::insertList(arg1);
  lqtL_pushudata(L, ret, "QTextList*");
  return 1;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__insertList (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QTextCursor*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QTextListFormat*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0xe9db60;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QTextCursor*")?premium:-premium*premium;
  if (lqtL_isenum(L, 2, "QTextListFormat::Style")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xe6c660;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__insertList__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__insertList__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__insertList matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__setCharFormat (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QTextCharFormat& arg1 = **static_cast<QTextCharFormat**>(lqtL_checkudata(L, 2, "QTextCharFormat*"));
  __lua__obj->QTextCursor::setCharFormat(arg1);
  return 0;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__atBlockEnd (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QTextCursor::atBlockEnd();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__setBlockFormat (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QTextBlockFormat& arg1 = **static_cast<QTextBlockFormat**>(lqtL_checkudata(L, 2, "QTextBlockFormat*"));
  __lua__obj->QTextCursor::setBlockFormat(arg1);
  return 0;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__blockCharFormat (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextCharFormat ret = __lua__obj->QTextCursor::blockCharFormat();
  lqtL_passudata(L, new QTextCharFormat(ret), "QTextCharFormat*");
  return 1;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__selectedTableCells (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int * arg1 = static_cast<int *>(lua_touserdata(L, 2));
  int * arg2 = static_cast<int *>(lua_touserdata(L, 3));
  int * arg3 = static_cast<int *>(lua_touserdata(L, 4));
  int * arg4 = static_cast<int *>(lua_touserdata(L, 5));
  __lua__obj->QTextCursor::selectedTableCells(arg1, arg2, arg3, arg4);
  return 0;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__deleteChar (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QTextCursor::deleteChar();
  return 0;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__mergeBlockFormat (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QTextBlockFormat& arg1 = **static_cast<QTextBlockFormat**>(lqtL_checkudata(L, 2, "QTextBlockFormat*"));
  __lua__obj->QTextCursor::mergeBlockFormat(arg1);
  return 0;
}
int LuaBinder< QTextCursor >::__LuaWrapCall__beginEditBlock (lua_State *L) {
  QTextCursor *& __lua__obj = *static_cast<QTextCursor**>(lqtL_checkudata(L, 1, "QTextCursor*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QTextCursor::beginEditBlock();
  return 0;
}
int LuaBinder< QTextCursor >::lqt_pushenum_MoveMode (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "MoveAnchor");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "MoveAnchor");
  lua_pushstring(L, "KeepAnchor");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "KeepAnchor");
  lua_pushcfunction(L, LuaBinder< QTextCursor >::lqt_pushenum_MoveMode_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QTextCursor::MoveMode");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QTextCursor >::lqt_pushenum_MoveMode_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QTextCursor::MoveMode>*) + sizeof(QFlags<QTextCursor::MoveMode>));
  QFlags<QTextCursor::MoveMode> *fl = static_cast<QFlags<QTextCursor::MoveMode>*>( static_cast<void*>(&static_cast<QFlags<QTextCursor::MoveMode>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QTextCursor::MoveMode>(lqtL_toenum(L, i, "QTextCursor::MoveMode"));
	}
	if (luaL_newmetatable(L, "QFlags<QTextCursor::MoveMode>*")) {
		lua_pushstring(L, "QFlags<QTextCursor::MoveMode>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinder< QTextCursor >::lqt_pushenum_MoveOperation (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "NoMove");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "NoMove");
  lua_pushstring(L, "Start");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "Start");
  lua_pushstring(L, "Up");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "Up");
  lua_pushstring(L, "StartOfLine");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "StartOfLine");
  lua_pushstring(L, "StartOfBlock");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "StartOfBlock");
  lua_pushstring(L, "StartOfWord");
  lua_rawseti(L, enum_table, 5);
  lua_pushinteger(L, 5);
  lua_setfield(L, enum_table, "StartOfWord");
  lua_pushstring(L, "PreviousBlock");
  lua_rawseti(L, enum_table, 6);
  lua_pushinteger(L, 6);
  lua_setfield(L, enum_table, "PreviousBlock");
  lua_pushstring(L, "PreviousCharacter");
  lua_rawseti(L, enum_table, 7);
  lua_pushinteger(L, 7);
  lua_setfield(L, enum_table, "PreviousCharacter");
  lua_pushstring(L, "PreviousWord");
  lua_rawseti(L, enum_table, 8);
  lua_pushinteger(L, 8);
  lua_setfield(L, enum_table, "PreviousWord");
  lua_pushstring(L, "Left");
  lua_rawseti(L, enum_table, 9);
  lua_pushinteger(L, 9);
  lua_setfield(L, enum_table, "Left");
  lua_pushstring(L, "WordLeft");
  lua_rawseti(L, enum_table, 10);
  lua_pushinteger(L, 10);
  lua_setfield(L, enum_table, "WordLeft");
  lua_pushstring(L, "End");
  lua_rawseti(L, enum_table, 11);
  lua_pushinteger(L, 11);
  lua_setfield(L, enum_table, "End");
  lua_pushstring(L, "Down");
  lua_rawseti(L, enum_table, 12);
  lua_pushinteger(L, 12);
  lua_setfield(L, enum_table, "Down");
  lua_pushstring(L, "EndOfLine");
  lua_rawseti(L, enum_table, 13);
  lua_pushinteger(L, 13);
  lua_setfield(L, enum_table, "EndOfLine");
  lua_pushstring(L, "EndOfWord");
  lua_rawseti(L, enum_table, 14);
  lua_pushinteger(L, 14);
  lua_setfield(L, enum_table, "EndOfWord");
  lua_pushstring(L, "EndOfBlock");
  lua_rawseti(L, enum_table, 15);
  lua_pushinteger(L, 15);
  lua_setfield(L, enum_table, "EndOfBlock");
  lua_pushstring(L, "NextBlock");
  lua_rawseti(L, enum_table, 16);
  lua_pushinteger(L, 16);
  lua_setfield(L, enum_table, "NextBlock");
  lua_pushstring(L, "NextCharacter");
  lua_rawseti(L, enum_table, 17);
  lua_pushinteger(L, 17);
  lua_setfield(L, enum_table, "NextCharacter");
  lua_pushstring(L, "NextWord");
  lua_rawseti(L, enum_table, 18);
  lua_pushinteger(L, 18);
  lua_setfield(L, enum_table, "NextWord");
  lua_pushstring(L, "Right");
  lua_rawseti(L, enum_table, 19);
  lua_pushinteger(L, 19);
  lua_setfield(L, enum_table, "Right");
  lua_pushstring(L, "WordRight");
  lua_rawseti(L, enum_table, 20);
  lua_pushinteger(L, 20);
  lua_setfield(L, enum_table, "WordRight");
  lua_pushcfunction(L, LuaBinder< QTextCursor >::lqt_pushenum_MoveOperation_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QTextCursor::MoveOperation");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QTextCursor >::lqt_pushenum_MoveOperation_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QTextCursor::MoveOperation>*) + sizeof(QFlags<QTextCursor::MoveOperation>));
  QFlags<QTextCursor::MoveOperation> *fl = static_cast<QFlags<QTextCursor::MoveOperation>*>( static_cast<void*>(&static_cast<QFlags<QTextCursor::MoveOperation>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QTextCursor::MoveOperation>(lqtL_toenum(L, i, "QTextCursor::MoveOperation"));
	}
	if (luaL_newmetatable(L, "QFlags<QTextCursor::MoveOperation>*")) {
		lua_pushstring(L, "QFlags<QTextCursor::MoveOperation>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinder< QTextCursor >::lqt_pushenum_SelectionType (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "WordUnderCursor");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "WordUnderCursor");
  lua_pushstring(L, "LineUnderCursor");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "LineUnderCursor");
  lua_pushstring(L, "BlockUnderCursor");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "BlockUnderCursor");
  lua_pushstring(L, "Document");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "Document");
  lua_pushcfunction(L, LuaBinder< QTextCursor >::lqt_pushenum_SelectionType_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QTextCursor::SelectionType");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QTextCursor >::lqt_pushenum_SelectionType_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QTextCursor::SelectionType>*) + sizeof(QFlags<QTextCursor::SelectionType>));
  QFlags<QTextCursor::SelectionType> *fl = static_cast<QFlags<QTextCursor::SelectionType>*>( static_cast<void*>(&static_cast<QFlags<QTextCursor::SelectionType>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QTextCursor::SelectionType>(lqtL_toenum(L, i, "QTextCursor::SelectionType"));
	}
	if (luaL_newmetatable(L, "QFlags<QTextCursor::SelectionType>*")) {
		lua_pushstring(L, "QFlags<QTextCursor::SelectionType>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int luaopen_QTextCursor (lua_State *L) {
  if (luaL_newmetatable(L, "QTextCursor*")) {
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__insertBlock);
    lua_setfield(L, -2, "insertBlock");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__insertFragment);
    lua_setfield(L, -2, "insertFragment");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__clearSelection);
    lua_setfield(L, -2, "clearSelection");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__insertImage);
    lua_setfield(L, -2, "insertImage");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__insertFrame);
    lua_setfield(L, -2, "insertFrame");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__setPosition);
    lua_setfield(L, -2, "setPosition");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__atEnd);
    lua_setfield(L, -2, "atEnd");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__insertHtml);
    lua_setfield(L, -2, "insertHtml");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__block);
    lua_setfield(L, -2, "block");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__atBlockStart);
    lua_setfield(L, -2, "atBlockStart");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__insertTable);
    lua_setfield(L, -2, "insertTable");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__isCopyOf);
    lua_setfield(L, -2, "isCopyOf");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__selectedText);
    lua_setfield(L, -2, "selectedText");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__endEditBlock);
    lua_setfield(L, -2, "endEditBlock");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__blockFormat);
    lua_setfield(L, -2, "blockFormat");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__blockNumber);
    lua_setfield(L, -2, "blockNumber");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__position);
    lua_setfield(L, -2, "position");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__delete);
    lua_setfield(L, -2, "delete");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__deletePreviousChar);
    lua_setfield(L, -2, "deletePreviousChar");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__currentTable);
    lua_setfield(L, -2, "currentTable");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__insertText);
    lua_setfield(L, -2, "insertText");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__hasComplexSelection);
    lua_setfield(L, -2, "hasComplexSelection");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__movePosition);
    lua_setfield(L, -2, "movePosition");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__columnNumber);
    lua_setfield(L, -2, "columnNumber");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__selectionStart);
    lua_setfield(L, -2, "selectionStart");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__select);
    lua_setfield(L, -2, "select");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__anchor);
    lua_setfield(L, -2, "anchor");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__joinPreviousEditBlock);
    lua_setfield(L, -2, "joinPreviousEditBlock");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__selection);
    lua_setfield(L, -2, "selection");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__hasSelection);
    lua_setfield(L, -2, "hasSelection");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__isNull);
    lua_setfield(L, -2, "isNull");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__mergeCharFormat);
    lua_setfield(L, -2, "mergeCharFormat");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__removeSelectedText);
    lua_setfield(L, -2, "removeSelectedText");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__createList);
    lua_setfield(L, -2, "createList");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__new);
    lua_setfield(L, -2, "new");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__setBlockCharFormat);
    lua_setfield(L, -2, "setBlockCharFormat");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__charFormat);
    lua_setfield(L, -2, "charFormat");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__currentList);
    lua_setfield(L, -2, "currentList");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__currentFrame);
    lua_setfield(L, -2, "currentFrame");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__selectionEnd);
    lua_setfield(L, -2, "selectionEnd");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__mergeBlockCharFormat);
    lua_setfield(L, -2, "mergeBlockCharFormat");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__atStart);
    lua_setfield(L, -2, "atStart");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__insertList);
    lua_setfield(L, -2, "insertList");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__setCharFormat);
    lua_setfield(L, -2, "setCharFormat");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__atBlockEnd);
    lua_setfield(L, -2, "atBlockEnd");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__setBlockFormat);
    lua_setfield(L, -2, "setBlockFormat");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__blockCharFormat);
    lua_setfield(L, -2, "blockCharFormat");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__selectedTableCells);
    lua_setfield(L, -2, "selectedTableCells");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__deleteChar);
    lua_setfield(L, -2, "deleteChar");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__mergeBlockFormat);
    lua_setfield(L, -2, "mergeBlockFormat");
    lua_pushcfunction(L, LuaBinder< QTextCursor >::__LuaWrapCall__beginEditBlock);
    lua_setfield(L, -2, "beginEditBlock");
    LuaBinder< QTextCursor >::lqt_pushenum_MoveMode(L);
    lua_setfield(L, -2, "MoveMode");
    LuaBinder< QTextCursor >::lqt_pushenum_MoveOperation(L);
    lua_setfield(L, -2, "MoveOperation");
    LuaBinder< QTextCursor >::lqt_pushenum_SelectionType(L);
    lua_setfield(L, -2, "SelectionType");
    lua_newtable(L);
    lua_setfield(L, -2, "__base");
    lua_pushcfunction(L, lqtL_newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, lqtL_index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, lqtL_gc);
    lua_setfield(L, -2, "__gc");
    lua_pushstring(L, "QTextCursor");
    lua_setfield(L, -2, "__qtype");
		lua_setglobal(L, "QTextCursor");
  } else {
		lua_pop(L, 1);
	}
  return 0;
}
