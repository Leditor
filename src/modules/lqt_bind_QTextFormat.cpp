#include "lqt_bind_QTextFormat.hpp"

int LuaBinder< QTextFormat >::__LuaWrapCall__colorProperty (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  QColor ret = __lua__obj->QTextFormat::colorProperty(arg1);
  lqtL_passudata(L, new QColor(ret), "QColor*");
  return 1;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__clearBackground (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QTextFormat::clearBackground();
  return 0;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__lengthVectorProperty (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  QVector<QTextLength> ret = __lua__obj->QTextFormat::lengthVectorProperty(arg1);
  lqtL_passudata(L, new QVector<QTextLength>(ret), "QVector<QTextLength>*");
  return 1;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__setForeground (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QBrush& arg1 = **static_cast<QBrush**>(lqtL_checkudata(L, 2, "QBrush*"));
  __lua__obj->QTextFormat::setForeground(arg1);
  return 0;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__background (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QBrush ret = __lua__obj->QTextFormat::background();
  lqtL_passudata(L, new QBrush(ret), "QBrush*");
  return 1;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__setProperty__OverloadedVersion__1 (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  const QVariant& arg2 = **static_cast<QVariant**>(lqtL_checkudata(L, 3, "QVariant*"));
  __lua__obj->QTextFormat::setProperty(arg1, arg2);
  return 0;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__setProperty__OverloadedVersion__2 (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  const QVector<QTextLength>& arg2 = **static_cast<QVector<QTextLength>**>(lqtL_checkudata(L, 3, "QVector<QTextLength>*"));
  __lua__obj->QTextFormat::setProperty(arg1, arg2);
  return 0;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__setProperty (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QTextFormat*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0xc73950;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_testudata(L, 3, "QVariant*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0xc70bf0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QTextFormat*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xb97e50;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_testudata(L, 3, "QVector<QTextLength>*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xb8ee80;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__setProperty__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__setProperty__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__setProperty matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__isValid (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QTextFormat::isValid();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__isImageFormat (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QTextFormat::isImageFormat();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__toListFormat (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextListFormat ret = __lua__obj->QTextFormat::toListFormat();
  lqtL_passudata(L, new QTextListFormat(ret), "QTextListFormat*");
  return 1;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__lengthProperty (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  QTextLength ret = __lua__obj->QTextFormat::lengthProperty(arg1);
  lqtL_passudata(L, new QTextLength(ret), "QTextLength*");
  return 1;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__setObjectType (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QTextFormat::setObjectType(arg1);
  return 0;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__merge (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QTextFormat& arg1 = **static_cast<QTextFormat**>(lqtL_checkudata(L, 2, "QTextFormat*"));
  __lua__obj->QTextFormat::merge(arg1);
  return 0;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__delete (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  delete __lua__obj;
  __lua__obj = 0;
  return 0;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__new__OverloadedVersion__1 (lua_State *L) {
  QTextFormat * ret = new LuaBinder< QTextFormat >(L);
  lqtL_passudata(L, ret, "QTextFormat*");
  return 1;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__new__OverloadedVersion__2 (lua_State *L) {
  int arg1 = lua_tointeger(L, 1);
  QTextFormat * ret = new LuaBinder< QTextFormat >(L, arg1);
  lqtL_passudata(L, ret, "QTextFormat*");
  return 1;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__new__OverloadedVersion__3 (lua_State *L) {
  const QTextFormat& arg1 = **static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
  QTextFormat * ret = new LuaBinder< QTextFormat >(L, arg1);
  lqtL_passudata(L, ret, "QTextFormat*");
  return 1;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__new (lua_State *L) {
  int score[4];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[2] = 0;
  if (lua_isnumber(L, 1)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xcf3420;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  if (lqtL_testudata(L, 1, "QTextFormat*")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0xcc5750;
  } else {
    score[3] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=3;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__new__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__new__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__new__OverloadedVersion__3(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__new matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__toBlockFormat (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextBlockFormat ret = __lua__obj->QTextFormat::toBlockFormat();
  lqtL_passudata(L, new QTextBlockFormat(ret), "QTextBlockFormat*");
  return 1;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__toCharFormat (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextCharFormat ret = __lua__obj->QTextFormat::toCharFormat();
  lqtL_passudata(L, new QTextCharFormat(ret), "QTextCharFormat*");
  return 1;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__boolProperty (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  bool ret = __lua__obj->QTextFormat::boolProperty(arg1);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__isFrameFormat (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QTextFormat::isFrameFormat();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__isBlockFormat (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QTextFormat::isBlockFormat();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__toTableFormat (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextTableFormat ret = __lua__obj->QTextFormat::toTableFormat();
  lqtL_passudata(L, new QTextTableFormat(ret), "QTextTableFormat*");
  return 1;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__penProperty (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  QPen ret = __lua__obj->QTextFormat::penProperty(arg1);
  lqtL_passudata(L, new QPen(ret), "QPen*");
  return 1;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__stringProperty (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  QString ret = __lua__obj->QTextFormat::stringProperty(arg1);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__isListFormat (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QTextFormat::isListFormat();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__setLayoutDirection (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::LayoutDirection arg1 = static_cast<Qt::LayoutDirection>(lqtL_toenum(L, 2, "Qt::LayoutDirection"));
  __lua__obj->QTextFormat::setLayoutDirection(arg1);
  return 0;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__layoutDirection (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::LayoutDirection ret = __lua__obj->QTextFormat::layoutDirection();
  lqtL_pushenum(L, ret, "Qt::LayoutDirection");
  return 1;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__property (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  QVariant ret = __lua__obj->QTextFormat::property(arg1);
  lqtL_passudata(L, new QVariant(ret), "QVariant*");
  return 1;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__properties (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QMap<int,QVariant> ret = __lua__obj->QTextFormat::properties();
  lqtL_passudata(L, new QMap<int,QVariant>(ret), "QMap<int,QVariant>*");
  return 1;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__hasProperty (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  bool ret = __lua__obj->QTextFormat::hasProperty(arg1);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__propertyCount (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QTextFormat::propertyCount();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__type (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QTextFormat::type();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__clearProperty (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QTextFormat::clearProperty(arg1);
  return 0;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__setBackground (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QBrush& arg1 = **static_cast<QBrush**>(lqtL_checkudata(L, 2, "QBrush*"));
  __lua__obj->QTextFormat::setBackground(arg1);
  return 0;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__setObjectIndex (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QTextFormat::setObjectIndex(arg1);
  return 0;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__isCharFormat (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QTextFormat::isCharFormat();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__intProperty (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int ret = __lua__obj->QTextFormat::intProperty(arg1);
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__isTableFormat (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QTextFormat::isTableFormat();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__toImageFormat (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextImageFormat ret = __lua__obj->QTextFormat::toImageFormat();
  lqtL_passudata(L, new QTextImageFormat(ret), "QTextImageFormat*");
  return 1;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__toFrameFormat (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextFrameFormat ret = __lua__obj->QTextFormat::toFrameFormat();
  lqtL_passudata(L, new QTextFrameFormat(ret), "QTextFrameFormat*");
  return 1;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__brushProperty (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  QBrush ret = __lua__obj->QTextFormat::brushProperty(arg1);
  lqtL_passudata(L, new QBrush(ret), "QBrush*");
  return 1;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__clearForeground (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QTextFormat::clearForeground();
  return 0;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__foreground (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QBrush ret = __lua__obj->QTextFormat::foreground();
  lqtL_passudata(L, new QBrush(ret), "QBrush*");
  return 1;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__objectIndex (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QTextFormat::objectIndex();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__objectType (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QTextFormat::objectType();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QTextFormat >::__LuaWrapCall__doubleProperty (lua_State *L) {
  QTextFormat *& __lua__obj = *static_cast<QTextFormat**>(lqtL_checkudata(L, 1, "QTextFormat*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  double ret = __lua__obj->QTextFormat::doubleProperty(arg1);
  lua_pushnumber(L, ret);
  return 1;
}
int LuaBinder< QTextFormat >::lqt_pushenum_FormatType (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "InvalidFormat");
  lua_rawseti(L, enum_table, -1);
  lua_pushinteger(L, -1);
  lua_setfield(L, enum_table, "InvalidFormat");
  lua_pushstring(L, "BlockFormat");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "BlockFormat");
  lua_pushstring(L, "CharFormat");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "CharFormat");
  lua_pushstring(L, "ListFormat");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "ListFormat");
  lua_pushstring(L, "TableFormat");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "TableFormat");
  lua_pushstring(L, "FrameFormat");
  lua_rawseti(L, enum_table, 5);
  lua_pushinteger(L, 5);
  lua_setfield(L, enum_table, "FrameFormat");
  lua_pushstring(L, "UserFormat");
  lua_rawseti(L, enum_table, 100);
  lua_pushinteger(L, 100);
  lua_setfield(L, enum_table, "UserFormat");
  lua_pushcfunction(L, LuaBinder< QTextFormat >::lqt_pushenum_FormatType_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QTextFormat::FormatType");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QTextFormat >::lqt_pushenum_FormatType_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QTextFormat::FormatType>*) + sizeof(QFlags<QTextFormat::FormatType>));
  QFlags<QTextFormat::FormatType> *fl = static_cast<QFlags<QTextFormat::FormatType>*>( static_cast<void*>(&static_cast<QFlags<QTextFormat::FormatType>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QTextFormat::FormatType>(lqtL_toenum(L, i, "QTextFormat::FormatType"));
	}
	if (luaL_newmetatable(L, "QFlags<QTextFormat::FormatType>*")) {
		lua_pushstring(L, "QFlags<QTextFormat::FormatType>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinder< QTextFormat >::lqt_pushenum_Property (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "ObjectIndex");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "ObjectIndex");
  lua_pushstring(L, "CssFloat");
  lua_rawseti(L, enum_table, 2048);
  lua_pushinteger(L, 2048);
  lua_setfield(L, enum_table, "CssFloat");
  lua_pushstring(L, "LayoutDirection");
  lua_rawseti(L, enum_table, 2049);
  lua_pushinteger(L, 2049);
  lua_setfield(L, enum_table, "LayoutDirection");
  lua_pushstring(L, "OutlinePen");
  lua_rawseti(L, enum_table, 2064);
  lua_pushinteger(L, 2064);
  lua_setfield(L, enum_table, "OutlinePen");
  lua_pushstring(L, "BackgroundBrush");
  lua_rawseti(L, enum_table, 2080);
  lua_pushinteger(L, 2080);
  lua_setfield(L, enum_table, "BackgroundBrush");
  lua_pushstring(L, "ForegroundBrush");
  lua_rawseti(L, enum_table, 2081);
  lua_pushinteger(L, 2081);
  lua_setfield(L, enum_table, "ForegroundBrush");
  lua_pushstring(L, "BlockAlignment");
  lua_rawseti(L, enum_table, 4112);
  lua_pushinteger(L, 4112);
  lua_setfield(L, enum_table, "BlockAlignment");
  lua_pushstring(L, "BlockTopMargin");
  lua_rawseti(L, enum_table, 4144);
  lua_pushinteger(L, 4144);
  lua_setfield(L, enum_table, "BlockTopMargin");
  lua_pushstring(L, "BlockBottomMargin");
  lua_rawseti(L, enum_table, 4145);
  lua_pushinteger(L, 4145);
  lua_setfield(L, enum_table, "BlockBottomMargin");
  lua_pushstring(L, "BlockLeftMargin");
  lua_rawseti(L, enum_table, 4146);
  lua_pushinteger(L, 4146);
  lua_setfield(L, enum_table, "BlockLeftMargin");
  lua_pushstring(L, "BlockRightMargin");
  lua_rawseti(L, enum_table, 4147);
  lua_pushinteger(L, 4147);
  lua_setfield(L, enum_table, "BlockRightMargin");
  lua_pushstring(L, "TextIndent");
  lua_rawseti(L, enum_table, 4148);
  lua_pushinteger(L, 4148);
  lua_setfield(L, enum_table, "TextIndent");
  lua_pushstring(L, "BlockIndent");
  lua_rawseti(L, enum_table, 4160);
  lua_pushinteger(L, 4160);
  lua_setfield(L, enum_table, "BlockIndent");
  lua_pushstring(L, "BlockNonBreakableLines");
  lua_rawseti(L, enum_table, 4176);
  lua_pushinteger(L, 4176);
  lua_setfield(L, enum_table, "BlockNonBreakableLines");
  lua_pushstring(L, "BlockTrailingHorizontalRulerWidth");
  lua_rawseti(L, enum_table, 4192);
  lua_pushinteger(L, 4192);
  lua_setfield(L, enum_table, "BlockTrailingHorizontalRulerWidth");
  lua_pushstring(L, "FontFamily");
  lua_rawseti(L, enum_table, 8192);
  lua_pushinteger(L, 8192);
  lua_setfield(L, enum_table, "FontFamily");
  lua_pushstring(L, "FontPointSize");
  lua_rawseti(L, enum_table, 8193);
  lua_pushinteger(L, 8193);
  lua_setfield(L, enum_table, "FontPointSize");
  lua_pushstring(L, "FontSizeAdjustment");
  lua_rawseti(L, enum_table, 8194);
  lua_pushinteger(L, 8194);
  lua_setfield(L, enum_table, "FontSizeAdjustment");
  lua_pushstring(L, "FontSizeIncrement");
  lua_rawseti(L, enum_table, 8194);
  lua_pushinteger(L, 8194);
  lua_setfield(L, enum_table, "FontSizeIncrement");
  lua_pushstring(L, "FontWeight");
  lua_rawseti(L, enum_table, 8195);
  lua_pushinteger(L, 8195);
  lua_setfield(L, enum_table, "FontWeight");
  lua_pushstring(L, "FontItalic");
  lua_rawseti(L, enum_table, 8196);
  lua_pushinteger(L, 8196);
  lua_setfield(L, enum_table, "FontItalic");
  lua_pushstring(L, "FontUnderline");
  lua_rawseti(L, enum_table, 8197);
  lua_pushinteger(L, 8197);
  lua_setfield(L, enum_table, "FontUnderline");
  lua_pushstring(L, "FontOverline");
  lua_rawseti(L, enum_table, 8198);
  lua_pushinteger(L, 8198);
  lua_setfield(L, enum_table, "FontOverline");
  lua_pushstring(L, "FontStrikeOut");
  lua_rawseti(L, enum_table, 8199);
  lua_pushinteger(L, 8199);
  lua_setfield(L, enum_table, "FontStrikeOut");
  lua_pushstring(L, "FontFixedPitch");
  lua_rawseti(L, enum_table, 8200);
  lua_pushinteger(L, 8200);
  lua_setfield(L, enum_table, "FontFixedPitch");
  lua_pushstring(L, "FontPixelSize");
  lua_rawseti(L, enum_table, 8201);
  lua_pushinteger(L, 8201);
  lua_setfield(L, enum_table, "FontPixelSize");
  lua_pushstring(L, "TextUnderlineColor");
  lua_rawseti(L, enum_table, 8208);
  lua_pushinteger(L, 8208);
  lua_setfield(L, enum_table, "TextUnderlineColor");
  lua_pushstring(L, "TextVerticalAlignment");
  lua_rawseti(L, enum_table, 8225);
  lua_pushinteger(L, 8225);
  lua_setfield(L, enum_table, "TextVerticalAlignment");
  lua_pushstring(L, "TextOutline");
  lua_rawseti(L, enum_table, 8226);
  lua_pushinteger(L, 8226);
  lua_setfield(L, enum_table, "TextOutline");
  lua_pushstring(L, "TextUnderlineStyle");
  lua_rawseti(L, enum_table, 8227);
  lua_pushinteger(L, 8227);
  lua_setfield(L, enum_table, "TextUnderlineStyle");
  lua_pushstring(L, "TextToolTip");
  lua_rawseti(L, enum_table, 8228);
  lua_pushinteger(L, 8228);
  lua_setfield(L, enum_table, "TextToolTip");
  lua_pushstring(L, "IsAnchor");
  lua_rawseti(L, enum_table, 8240);
  lua_pushinteger(L, 8240);
  lua_setfield(L, enum_table, "IsAnchor");
  lua_pushstring(L, "AnchorHref");
  lua_rawseti(L, enum_table, 8241);
  lua_pushinteger(L, 8241);
  lua_setfield(L, enum_table, "AnchorHref");
  lua_pushstring(L, "AnchorName");
  lua_rawseti(L, enum_table, 8242);
  lua_pushinteger(L, 8242);
  lua_setfield(L, enum_table, "AnchorName");
  lua_pushstring(L, "ObjectType");
  lua_rawseti(L, enum_table, 12032);
  lua_pushinteger(L, 12032);
  lua_setfield(L, enum_table, "ObjectType");
  lua_pushstring(L, "ListStyle");
  lua_rawseti(L, enum_table, 12288);
  lua_pushinteger(L, 12288);
  lua_setfield(L, enum_table, "ListStyle");
  lua_pushstring(L, "ListIndent");
  lua_rawseti(L, enum_table, 12289);
  lua_pushinteger(L, 12289);
  lua_setfield(L, enum_table, "ListIndent");
  lua_pushstring(L, "FrameBorder");
  lua_rawseti(L, enum_table, 16384);
  lua_pushinteger(L, 16384);
  lua_setfield(L, enum_table, "FrameBorder");
  lua_pushstring(L, "FrameMargin");
  lua_rawseti(L, enum_table, 16385);
  lua_pushinteger(L, 16385);
  lua_setfield(L, enum_table, "FrameMargin");
  lua_pushstring(L, "FramePadding");
  lua_rawseti(L, enum_table, 16386);
  lua_pushinteger(L, 16386);
  lua_setfield(L, enum_table, "FramePadding");
  lua_pushstring(L, "FrameWidth");
  lua_rawseti(L, enum_table, 16387);
  lua_pushinteger(L, 16387);
  lua_setfield(L, enum_table, "FrameWidth");
  lua_pushstring(L, "FrameHeight");
  lua_rawseti(L, enum_table, 16388);
  lua_pushinteger(L, 16388);
  lua_setfield(L, enum_table, "FrameHeight");
  lua_pushstring(L, "FrameTopMargin");
  lua_rawseti(L, enum_table, 16389);
  lua_pushinteger(L, 16389);
  lua_setfield(L, enum_table, "FrameTopMargin");
  lua_pushstring(L, "FrameBottomMargin");
  lua_rawseti(L, enum_table, 16390);
  lua_pushinteger(L, 16390);
  lua_setfield(L, enum_table, "FrameBottomMargin");
  lua_pushstring(L, "FrameLeftMargin");
  lua_rawseti(L, enum_table, 16391);
  lua_pushinteger(L, 16391);
  lua_setfield(L, enum_table, "FrameLeftMargin");
  lua_pushstring(L, "FrameRightMargin");
  lua_rawseti(L, enum_table, 16392);
  lua_pushinteger(L, 16392);
  lua_setfield(L, enum_table, "FrameRightMargin");
  lua_pushstring(L, "FrameBorderBrush");
  lua_rawseti(L, enum_table, 16393);
  lua_pushinteger(L, 16393);
  lua_setfield(L, enum_table, "FrameBorderBrush");
  lua_pushstring(L, "FrameBorderStyle");
  lua_rawseti(L, enum_table, 16400);
  lua_pushinteger(L, 16400);
  lua_setfield(L, enum_table, "FrameBorderStyle");
  lua_pushstring(L, "TableColumns");
  lua_rawseti(L, enum_table, 16640);
  lua_pushinteger(L, 16640);
  lua_setfield(L, enum_table, "TableColumns");
  lua_pushstring(L, "TableColumnWidthConstraints");
  lua_rawseti(L, enum_table, 16641);
  lua_pushinteger(L, 16641);
  lua_setfield(L, enum_table, "TableColumnWidthConstraints");
  lua_pushstring(L, "TableCellSpacing");
  lua_rawseti(L, enum_table, 16642);
  lua_pushinteger(L, 16642);
  lua_setfield(L, enum_table, "TableCellSpacing");
  lua_pushstring(L, "TableCellPadding");
  lua_rawseti(L, enum_table, 16643);
  lua_pushinteger(L, 16643);
  lua_setfield(L, enum_table, "TableCellPadding");
  lua_pushstring(L, "TableHeaderRowCount");
  lua_rawseti(L, enum_table, 16644);
  lua_pushinteger(L, 16644);
  lua_setfield(L, enum_table, "TableHeaderRowCount");
  lua_pushstring(L, "TableCellRowSpan");
  lua_rawseti(L, enum_table, 18448);
  lua_pushinteger(L, 18448);
  lua_setfield(L, enum_table, "TableCellRowSpan");
  lua_pushstring(L, "TableCellColumnSpan");
  lua_rawseti(L, enum_table, 18449);
  lua_pushinteger(L, 18449);
  lua_setfield(L, enum_table, "TableCellColumnSpan");
  lua_pushstring(L, "ImageName");
  lua_rawseti(L, enum_table, 20480);
  lua_pushinteger(L, 20480);
  lua_setfield(L, enum_table, "ImageName");
  lua_pushstring(L, "ImageWidth");
  lua_rawseti(L, enum_table, 20496);
  lua_pushinteger(L, 20496);
  lua_setfield(L, enum_table, "ImageWidth");
  lua_pushstring(L, "ImageHeight");
  lua_rawseti(L, enum_table, 20497);
  lua_pushinteger(L, 20497);
  lua_setfield(L, enum_table, "ImageHeight");
  lua_pushstring(L, "FullWidthSelection");
  lua_rawseti(L, enum_table, 24576);
  lua_pushinteger(L, 24576);
  lua_setfield(L, enum_table, "FullWidthSelection");
  lua_pushstring(L, "PageBreakPolicy");
  lua_rawseti(L, enum_table, 28672);
  lua_pushinteger(L, 28672);
  lua_setfield(L, enum_table, "PageBreakPolicy");
  lua_pushstring(L, "UserProperty");
  lua_rawseti(L, enum_table, 1048576);
  lua_pushinteger(L, 1048576);
  lua_setfield(L, enum_table, "UserProperty");
  lua_pushcfunction(L, LuaBinder< QTextFormat >::lqt_pushenum_Property_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QTextFormat::Property");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QTextFormat >::lqt_pushenum_Property_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QTextFormat::Property>*) + sizeof(QFlags<QTextFormat::Property>));
  QFlags<QTextFormat::Property> *fl = static_cast<QFlags<QTextFormat::Property>*>( static_cast<void*>(&static_cast<QFlags<QTextFormat::Property>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QTextFormat::Property>(lqtL_toenum(L, i, "QTextFormat::Property"));
	}
	if (luaL_newmetatable(L, "QFlags<QTextFormat::Property>*")) {
		lua_pushstring(L, "QFlags<QTextFormat::Property>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinder< QTextFormat >::lqt_pushenum_ObjectTypes (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "NoObject");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "NoObject");
  lua_pushstring(L, "ImageObject");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "ImageObject");
  lua_pushstring(L, "TableObject");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "TableObject");
  lua_pushstring(L, "UserObject");
  lua_rawseti(L, enum_table, 4096);
  lua_pushinteger(L, 4096);
  lua_setfield(L, enum_table, "UserObject");
  lua_pushcfunction(L, LuaBinder< QTextFormat >::lqt_pushenum_ObjectTypes_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QTextFormat::ObjectTypes");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QTextFormat >::lqt_pushenum_ObjectTypes_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QTextFormat::ObjectTypes>*) + sizeof(QFlags<QTextFormat::ObjectTypes>));
  QFlags<QTextFormat::ObjectTypes> *fl = static_cast<QFlags<QTextFormat::ObjectTypes>*>( static_cast<void*>(&static_cast<QFlags<QTextFormat::ObjectTypes>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QTextFormat::ObjectTypes>(lqtL_toenum(L, i, "QTextFormat::ObjectTypes"));
	}
	if (luaL_newmetatable(L, "QFlags<QTextFormat::ObjectTypes>*")) {
		lua_pushstring(L, "QFlags<QTextFormat::ObjectTypes>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinder< QTextFormat >::lqt_pushenum_PageBreakFlag (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "PageBreak_Auto");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "PageBreak_Auto");
  lua_pushstring(L, "PageBreak_AlwaysBefore");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "PageBreak_AlwaysBefore");
  lua_pushstring(L, "PageBreak_AlwaysAfter");
  lua_rawseti(L, enum_table, 16);
  lua_pushinteger(L, 16);
  lua_setfield(L, enum_table, "PageBreak_AlwaysAfter");
  lua_pushcfunction(L, LuaBinder< QTextFormat >::lqt_pushenum_PageBreakFlag_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QTextFormat::PageBreakFlag");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QTextFormat >::lqt_pushenum_PageBreakFlag_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QTextFormat::PageBreakFlag>*) + sizeof(QFlags<QTextFormat::PageBreakFlag>));
  QFlags<QTextFormat::PageBreakFlag> *fl = static_cast<QFlags<QTextFormat::PageBreakFlag>*>( static_cast<void*>(&static_cast<QFlags<QTextFormat::PageBreakFlag>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QTextFormat::PageBreakFlag>(lqtL_toenum(L, i, "QTextFormat::PageBreakFlag"));
	}
	if (luaL_newmetatable(L, "QFlags<QTextFormat::PageBreakFlag>*")) {
		lua_pushstring(L, "QFlags<QTextFormat::PageBreakFlag>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int luaopen_QTextFormat (lua_State *L) {
  if (luaL_newmetatable(L, "QTextFormat*")) {
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__colorProperty);
    lua_setfield(L, -2, "colorProperty");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__clearBackground);
    lua_setfield(L, -2, "clearBackground");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__lengthVectorProperty);
    lua_setfield(L, -2, "lengthVectorProperty");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__setForeground);
    lua_setfield(L, -2, "setForeground");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__background);
    lua_setfield(L, -2, "background");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__setProperty);
    lua_setfield(L, -2, "setProperty");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__isValid);
    lua_setfield(L, -2, "isValid");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__isImageFormat);
    lua_setfield(L, -2, "isImageFormat");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__toListFormat);
    lua_setfield(L, -2, "toListFormat");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__lengthProperty);
    lua_setfield(L, -2, "lengthProperty");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__setObjectType);
    lua_setfield(L, -2, "setObjectType");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__merge);
    lua_setfield(L, -2, "merge");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__delete);
    lua_setfield(L, -2, "delete");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__new);
    lua_setfield(L, -2, "new");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__toBlockFormat);
    lua_setfield(L, -2, "toBlockFormat");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__toCharFormat);
    lua_setfield(L, -2, "toCharFormat");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__boolProperty);
    lua_setfield(L, -2, "boolProperty");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__isFrameFormat);
    lua_setfield(L, -2, "isFrameFormat");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__isBlockFormat);
    lua_setfield(L, -2, "isBlockFormat");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__toTableFormat);
    lua_setfield(L, -2, "toTableFormat");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__penProperty);
    lua_setfield(L, -2, "penProperty");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__stringProperty);
    lua_setfield(L, -2, "stringProperty");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__isListFormat);
    lua_setfield(L, -2, "isListFormat");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__setLayoutDirection);
    lua_setfield(L, -2, "setLayoutDirection");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__layoutDirection);
    lua_setfield(L, -2, "layoutDirection");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__property);
    lua_setfield(L, -2, "property");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__properties);
    lua_setfield(L, -2, "properties");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__hasProperty);
    lua_setfield(L, -2, "hasProperty");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__propertyCount);
    lua_setfield(L, -2, "propertyCount");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__type);
    lua_setfield(L, -2, "type");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__clearProperty);
    lua_setfield(L, -2, "clearProperty");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__setBackground);
    lua_setfield(L, -2, "setBackground");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__setObjectIndex);
    lua_setfield(L, -2, "setObjectIndex");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__isCharFormat);
    lua_setfield(L, -2, "isCharFormat");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__intProperty);
    lua_setfield(L, -2, "intProperty");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__isTableFormat);
    lua_setfield(L, -2, "isTableFormat");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__toImageFormat);
    lua_setfield(L, -2, "toImageFormat");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__toFrameFormat);
    lua_setfield(L, -2, "toFrameFormat");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__brushProperty);
    lua_setfield(L, -2, "brushProperty");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__clearForeground);
    lua_setfield(L, -2, "clearForeground");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__foreground);
    lua_setfield(L, -2, "foreground");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__objectIndex);
    lua_setfield(L, -2, "objectIndex");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__objectType);
    lua_setfield(L, -2, "objectType");
    lua_pushcfunction(L, LuaBinder< QTextFormat >::__LuaWrapCall__doubleProperty);
    lua_setfield(L, -2, "doubleProperty");
    LuaBinder< QTextFormat >::lqt_pushenum_FormatType(L);
    lua_setfield(L, -2, "FormatType");
    LuaBinder< QTextFormat >::lqt_pushenum_Property(L);
    lua_setfield(L, -2, "Property");
    LuaBinder< QTextFormat >::lqt_pushenum_ObjectTypes(L);
    lua_setfield(L, -2, "ObjectTypes");
    LuaBinder< QTextFormat >::lqt_pushenum_PageBreakFlag(L);
    lua_setfield(L, -2, "PageBreakFlag");
    lua_newtable(L);
    lua_setfield(L, -2, "__base");
    lua_pushcfunction(L, lqtL_newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, lqtL_index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, lqtL_gc);
    lua_setfield(L, -2, "__gc");
    lua_pushstring(L, "QTextFormat");
    lua_setfield(L, -2, "__qtype");
		lua_setglobal(L, "QTextFormat");
  } else {
		lua_pop(L, 1);
	}
  return 0;
}
