#include "lqt_common.hpp"
#include <QFrame>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QFrame > : public QFrame {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__lineWidth (lua_State *L);
  static int __LuaWrapCall__setLineWidth (lua_State *L);
  static int __LuaWrapCall__sizeHint (lua_State *L);
  static int __LuaWrapCall__setFrameStyle (lua_State *L);
  static int __LuaWrapCall__setMidLineWidth (lua_State *L);
  static int __LuaWrapCall__frameStyle (lua_State *L);
  static int __LuaWrapCall__metaObject (lua_State *L);
  static int __LuaWrapCall__new (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__tr (lua_State *L);
  static int __LuaWrapCall__delete (lua_State *L);
  static int __LuaWrapCall__frameShadow (lua_State *L);
  static int __LuaWrapCall__frameShape (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__trUtf8 (lua_State *L);
  static int __LuaWrapCall__setFrameShadow (lua_State *L);
  static int __LuaWrapCall__frameWidth (lua_State *L);
  static int __LuaWrapCall__midLineWidth (lua_State *L);
  static int __LuaWrapCall__setFrameShape (lua_State *L);
  static int __LuaWrapCall__frameRect (lua_State *L);
  static int __LuaWrapCall__setFrameRect (lua_State *L);
protected:
  void styleChange (QStyle& arg1);
public:
protected:
  void focusInEvent (QFocusEvent * arg1);
public:
protected:
  void keyPressEvent (QKeyEvent * arg1);
public:
  const QMetaObject * metaObject () const;
  int devType () const;
  void setVisible (bool arg1);
  QVariant inputMethodQuery (Qt::InputMethodQuery arg1) const;
  QPaintEngine * paintEngine () const;
protected:
  void dragLeaveEvent (QDragLeaveEvent * arg1);
public:
protected:
  void mousePressEvent (QMouseEvent * arg1);
public:
  QSize sizeHint () const;
protected:
  void moveEvent (QMoveEvent * arg1);
public:
protected:
  void tabletEvent (QTabletEvent * arg1);
public:
protected:
  void enterEvent (QEvent * arg1);
public:
protected:
  void closeEvent (QCloseEvent * arg1);
public:
  QSize minimumSizeHint () const;
protected:
  void dropEvent (QDropEvent * arg1);
public:
protected:
  void actionEvent (QActionEvent * arg1);
public:
protected:
  void showEvent (QShowEvent * arg1);
public:
protected:
  void dragEnterEvent (QDragEnterEvent * arg1);
public:
protected:
  void paintEvent (QPaintEvent * arg1);
public:
protected:
  void dragMoveEvent (QDragMoveEvent * arg1);
public:
protected:
  void mouseDoubleClickEvent (QMouseEvent * arg1);
public:
protected:
  void enabledChange (bool arg1);
public:
protected:
  void contextMenuEvent (QContextMenuEvent * arg1);
public:
  int heightForWidth (int arg1) const;
protected:
  void fontChange (const QFont& arg1);
public:
protected:
  bool focusNextPrevChild (bool arg1);
public:
protected:
  void timerEvent (QTimerEvent * arg1);
public:
protected:
  void mouseMoveEvent (QMouseEvent * arg1);
public:
protected:
  void focusOutEvent (QFocusEvent * arg1);
public:
protected:
  void childEvent (QChildEvent * arg1);
public:
protected:
  void connectNotify (const char * arg1);
public:
protected:
  void mouseReleaseEvent (QMouseEvent * arg1);
public:
protected:
  int metric (QPaintDevice::PaintDeviceMetric arg1) const;
public:
  bool eventFilter (QObject * arg1, QEvent * arg2);
protected:
  void inputMethodEvent (QInputMethodEvent * arg1);
public:
protected:
  void wheelEvent (QWheelEvent * arg1);
public:
protected:
  void resizeEvent (QResizeEvent * arg1);
public:
protected:
  void paletteChange (const QPalette& arg1);
public:
protected:
  void languageChange ();
public:
protected:
  bool event (QEvent * arg1);
public:
protected:
  void hideEvent (QHideEvent * arg1);
public:
protected:
  void disconnectNotify (const char * arg1);
public:
protected:
  void keyReleaseEvent (QKeyEvent * arg1);
public:
protected:
  void windowActivationChange (bool arg1);
public:
protected:
  void leaveEvent (QEvent * arg1);
public:
protected:
  void changeEvent (QEvent * arg1);
public:
protected:
  void customEvent (QEvent * arg1);
public:
  ~LuaBinder< QFrame > ();
  static int lqt_pushenum_Shape (lua_State *L);
  static int lqt_pushenum_Shape_QFLAGS_CREATOR (lua_State *L);
  static int lqt_pushenum_Shadow (lua_State *L);
  static int lqt_pushenum_Shadow_QFLAGS_CREATOR (lua_State *L);
  static int lqt_pushenum_StyleMask (lua_State *L);
  static int lqt_pushenum_StyleMask_QFLAGS_CREATOR (lua_State *L);
  LuaBinder< QFrame > (lua_State *l, QWidget * arg1, QFlags<Qt::WindowType> arg2):QFrame(arg1, arg2), L(l) {}
};

extern "C" int luaopen_QFrame (lua_State *L);
