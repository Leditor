#include "lqt_bind_QFileDialog.hpp"

int LuaBinder< QFileDialog >::__LuaWrapCall__confirmOverwrite (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QFileDialog::confirmOverwrite();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__selectFilter (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QFileDialog::selectFilter(arg1);
  return 0;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__saveState (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QByteArray ret = __lua__obj->QFileDialog::saveState();
  lua_pushlstring(L, ret.data(), ret.size());
  return 1;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__resolveSymlinks (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QFileDialog::resolveSymlinks();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__setAcceptMode (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFileDialog::AcceptMode arg1 = static_cast<QFileDialog::AcceptMode>(lqtL_toenum(L, 2, "QFileDialog::AcceptMode"));
  __lua__obj->QFileDialog::setAcceptMode(arg1);
  return 0;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__setSidebarUrls (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QList<QUrl>& arg1 = **static_cast<QList<QUrl>**>(lqtL_checkudata(L, 2, "QList<QUrl>*"));
  __lua__obj->QFileDialog::setSidebarUrls(arg1);
  return 0;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QFileDialog::trUtf8(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QFileDialog::trUtf8(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__trUtf8 (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0xea51b0;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0xea4ce0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xea6fd0;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xea6700;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xea7460;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__trUtf8__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__trUtf8__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__trUtf8 matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__filters (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QStringList ret = __lua__obj->QFileDialog::filters();
  lqtL_passudata(L, new QStringList(ret), "QStringList*");
  return 1;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QFileDialog::tr(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QFileDialog::tr(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__tr (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0xea4410;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0xea4110;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xea5ec0;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xea63b0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xea6760;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__tr__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__tr__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__tr matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__sidebarUrls (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QList<QUrl> ret = __lua__obj->QFileDialog::sidebarUrls();
  lqtL_passudata(L, new QList<QUrl>(ret), "QList<QUrl>*");
  return 1;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__setConfirmOverwrite (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QFileDialog::setConfirmOverwrite(arg1);
  return 0;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__setViewMode (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFileDialog::ViewMode arg1 = static_cast<QFileDialog::ViewMode>(lqtL_toenum(L, 2, "QFileDialog::ViewMode"));
  __lua__obj->QFileDialog::setViewMode(arg1);
  return 0;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__acceptMode (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFileDialog::AcceptMode ret = __lua__obj->QFileDialog::acceptMode();
  lqtL_pushenum(L, ret, "QFileDialog::AcceptMode");
  return 1;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__setHistory (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QStringList& arg1 = **static_cast<QStringList**>(lqtL_checkudata(L, 2, "QStringList*"));
  __lua__obj->QFileDialog::setHistory(arg1);
  return 0;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__fileMode (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFileDialog::FileMode ret = __lua__obj->QFileDialog::fileMode();
  lqtL_pushenum(L, ret, "QFileDialog::FileMode");
  return 1;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__setDefaultSuffix (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QFileDialog::setDefaultSuffix(arg1);
  return 0;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__getOpenFileName (lua_State *L) {
  QWidget * arg1 = lqtL_testudata(L, 1, "QWidget*")?*static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*")):static_cast< QWidget * >(0);
  const QString& arg2 = (lua_type(L, 2)==LUA_TSTRING)?QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2)):QString();
  const QString& arg3 = (lua_type(L, 3)==LUA_TSTRING)?QString::fromAscii(lua_tostring(L, 3), lua_objlen(L, 3)):QString();
  const QString& arg4 = (lua_type(L, 4)==LUA_TSTRING)?QString::fromAscii(lua_tostring(L, 4), lua_objlen(L, 4)):QString();
  QString * arg5 = (lua_type(L, 5)==LUA_TSTRING)?static_cast<QString *>(lua_touserdata(L, 5)):static_cast< QString * >(0);
  QFlags<QFileDialog::Option> arg6 = lqtL_testudata(L, 6, "QFlags<QFileDialog::Option>*")?**static_cast<QFlags<QFileDialog::Option>**>(lqtL_checkudata(L, 6, "QFlags<QFileDialog::Option>*")):static_cast< QFlags<QFileDialog::Option> >(0);
  QString ret = QFileDialog::getOpenFileName(arg1, arg2, arg3, arg4, arg5, arg6);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__setReadOnly (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QFileDialog::setReadOnly(arg1);
  return 0;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__setLabelText (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFileDialog::DialogLabel arg1 = static_cast<QFileDialog::DialogLabel>(lqtL_toenum(L, 2, "QFileDialog::DialogLabel"));
  const QString& arg2 = QString::fromAscii(lua_tostring(L, 3), lua_objlen(L, 3));
  __lua__obj->QFileDialog::setLabelText(arg1, arg2);
  return 0;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__selectFile (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QFileDialog::selectFile(arg1);
  return 0;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__setFilters (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QStringList& arg1 = **static_cast<QStringList**>(lqtL_checkudata(L, 2, "QStringList*"));
  __lua__obj->QFileDialog::setFilters(arg1);
  return 0;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__selectedFiles (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QStringList ret = __lua__obj->QFileDialog::selectedFiles();
  lqtL_passudata(L, new QStringList(ret), "QStringList*");
  return 1;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__viewMode (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFileDialog::ViewMode ret = __lua__obj->QFileDialog::viewMode();
  lqtL_pushenum(L, ret, "QFileDialog::ViewMode");
  return 1;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__setFileMode (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFileDialog::FileMode arg1 = static_cast<QFileDialog::FileMode>(lqtL_toenum(L, 2, "QFileDialog::FileMode"));
  __lua__obj->QFileDialog::setFileMode(arg1);
  return 0;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__getExistingDirectory (lua_State *L) {
  QWidget * arg1 = lqtL_testudata(L, 1, "QWidget*")?*static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*")):static_cast< QWidget * >(0);
  const QString& arg2 = (lua_type(L, 2)==LUA_TSTRING)?QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2)):QString();
  const QString& arg3 = (lua_type(L, 3)==LUA_TSTRING)?QString::fromAscii(lua_tostring(L, 3), lua_objlen(L, 3)):QString();
  QFlags<QFileDialog::Option> arg4 = lqtL_testudata(L, 4, "QFlags<QFileDialog::Option>*")?**static_cast<QFlags<QFileDialog::Option>**>(lqtL_checkudata(L, 4, "QFlags<QFileDialog::Option>*")):ShowDirsOnly;
  QString ret = QFileDialog::getExistingDirectory(arg1, arg2, arg3, arg4);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__delete (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  delete __lua__obj;
  __lua__obj = 0;
  return 0;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__metaObject (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QMetaObject * ret = __lua__obj->QFileDialog::metaObject();
  lqtL_pushudata(L, ret, "QMetaObject*");
  return 1;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__getSaveFileName (lua_State *L) {
  QWidget * arg1 = lqtL_testudata(L, 1, "QWidget*")?*static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*")):static_cast< QWidget * >(0);
  const QString& arg2 = (lua_type(L, 2)==LUA_TSTRING)?QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2)):QString();
  const QString& arg3 = (lua_type(L, 3)==LUA_TSTRING)?QString::fromAscii(lua_tostring(L, 3), lua_objlen(L, 3)):QString();
  const QString& arg4 = (lua_type(L, 4)==LUA_TSTRING)?QString::fromAscii(lua_tostring(L, 4), lua_objlen(L, 4)):QString();
  QString * arg5 = (lua_type(L, 5)==LUA_TSTRING)?static_cast<QString *>(lua_touserdata(L, 5)):static_cast< QString * >(0);
  QFlags<QFileDialog::Option> arg6 = lqtL_testudata(L, 6, "QFlags<QFileDialog::Option>*")?**static_cast<QFlags<QFileDialog::Option>**>(lqtL_checkudata(L, 6, "QFlags<QFileDialog::Option>*")):static_cast< QFlags<QFileDialog::Option> >(0);
  QString ret = QFileDialog::getSaveFileName(arg1, arg2, arg3, arg4, arg5, arg6);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__setResolveSymlinks (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QFileDialog::setResolveSymlinks(arg1);
  return 0;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__new__OverloadedVersion__1 (lua_State *L) {
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
  QFlags<Qt::WindowType> arg2 = **static_cast<QFlags<Qt::WindowType>**>(lqtL_checkudata(L, 2, "QFlags<Qt::WindowType>*"));
  QFileDialog * ret = new LuaBinder< QFileDialog >(L, arg1, arg2);
  lqtL_passudata(L, ret, "QFileDialog*");
  return 1;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__new__OverloadedVersion__2 (lua_State *L) {
  QWidget * arg1 = lqtL_testudata(L, 1, "QWidget*")?*static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*")):static_cast< QWidget * >(0);
  const QString& arg2 = (lua_type(L, 2)==LUA_TSTRING)?QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2)):QString();
  const QString& arg3 = (lua_type(L, 3)==LUA_TSTRING)?QString::fromAscii(lua_tostring(L, 3), lua_objlen(L, 3)):QString();
  const QString& arg4 = (lua_type(L, 4)==LUA_TSTRING)?QString::fromAscii(lua_tostring(L, 4), lua_objlen(L, 4)):QString();
  QFileDialog * ret = new LuaBinder< QFileDialog >(L, arg1, arg2, arg3, arg4);
  lqtL_passudata(L, ret, "QFileDialog*");
  return 1;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__new (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if (lqtL_testudata(L, 1, "QWidget*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0xea9180;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_testudata(L, 2, "QFlags<Qt::WindowType>*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0xea96e0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if (lqtL_testudata(L, 1, "QWidget*")) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0xeaa0a0;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0xeaa5b0;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 3)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0xeaa550;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 4)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0xeaaef0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__new__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__new__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__new matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__selectedFilter (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QFileDialog::selectedFilter();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__directory (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QDir ret = __lua__obj->QFileDialog::directory();
  lqtL_passudata(L, new QDir(ret), "QDir*");
  return 1;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__isReadOnly (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QFileDialog::isReadOnly();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__defaultSuffix (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QFileDialog::defaultSuffix();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__setFilter (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QFileDialog::setFilter(arg1);
  return 0;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__setDirectory__OverloadedVersion__1 (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QFileDialog::setDirectory(arg1);
  return 0;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__setDirectory__OverloadedVersion__2 (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QDir& arg1 = **static_cast<QDir**>(lqtL_checkudata(L, 2, "QDir*"));
  __lua__obj->QFileDialog::setDirectory(arg1);
  return 0;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__setDirectory (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QFileDialog*")?premium:-premium*premium;
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0xeabf00;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QFileDialog*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QDir*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xeac980;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__setDirectory__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__setDirectory__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__setDirectory matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__setItemDelegate (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QAbstractItemDelegate * arg1 = *static_cast<QAbstractItemDelegate**>(lqtL_checkudata(L, 2, "QAbstractItemDelegate*"));
  __lua__obj->QFileDialog::setItemDelegate(arg1);
  return 0;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__restoreState (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QByteArray& arg1 = QByteArray(lua_tostring(L, 2), lua_objlen(L, 2));
  bool ret = __lua__obj->QFileDialog::restoreState(arg1);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__itemDelegate (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QAbstractItemDelegate * ret = __lua__obj->QFileDialog::itemDelegate();
  lqtL_pushudata(L, ret, "QAbstractItemDelegate*");
  return 1;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__history (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QStringList ret = __lua__obj->QFileDialog::history();
  lqtL_passudata(L, new QStringList(ret), "QStringList*");
  return 1;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__setIconProvider (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFileIconProvider * arg1 = *static_cast<QFileIconProvider**>(lqtL_checkudata(L, 2, "QFileIconProvider*"));
  __lua__obj->QFileDialog::setIconProvider(arg1);
  return 0;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__iconProvider (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFileIconProvider * ret = __lua__obj->QFileDialog::iconProvider();
  lqtL_pushudata(L, ret, "QFileIconProvider*");
  return 1;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__proxyModel (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QAbstractProxyModel * ret = __lua__obj->QFileDialog::proxyModel();
  lqtL_pushudata(L, ret, "QAbstractProxyModel*");
  return 1;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__labelText (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFileDialog::DialogLabel arg1 = static_cast<QFileDialog::DialogLabel>(lqtL_toenum(L, 2, "QFileDialog::DialogLabel"));
  QString ret = __lua__obj->QFileDialog::labelText(arg1);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__setProxyModel (lua_State *L) {
  QFileDialog *& __lua__obj = *static_cast<QFileDialog**>(lqtL_checkudata(L, 1, "QFileDialog*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QAbstractProxyModel * arg1 = *static_cast<QAbstractProxyModel**>(lqtL_checkudata(L, 2, "QAbstractProxyModel*"));
  __lua__obj->QFileDialog::setProxyModel(arg1);
  return 0;
}
int LuaBinder< QFileDialog >::__LuaWrapCall__getOpenFileNames (lua_State *L) {
  QWidget * arg1 = lqtL_testudata(L, 1, "QWidget*")?*static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*")):static_cast< QWidget * >(0);
  const QString& arg2 = (lua_type(L, 2)==LUA_TSTRING)?QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2)):QString();
  const QString& arg3 = (lua_type(L, 3)==LUA_TSTRING)?QString::fromAscii(lua_tostring(L, 3), lua_objlen(L, 3)):QString();
  const QString& arg4 = (lua_type(L, 4)==LUA_TSTRING)?QString::fromAscii(lua_tostring(L, 4), lua_objlen(L, 4)):QString();
  QString * arg5 = (lua_type(L, 5)==LUA_TSTRING)?static_cast<QString *>(lua_touserdata(L, 5)):static_cast< QString * >(0);
  QFlags<QFileDialog::Option> arg6 = lqtL_testudata(L, 6, "QFlags<QFileDialog::Option>*")?**static_cast<QFlags<QFileDialog::Option>**>(lqtL_checkudata(L, 6, "QFlags<QFileDialog::Option>*")):static_cast< QFlags<QFileDialog::Option> >(0);
  QStringList ret = QFileDialog::getOpenFileNames(arg1, arg2, arg3, arg4, arg5, arg6);
  lqtL_passudata(L, new QStringList(ret), "QStringList*");
  return 1;
}
void LuaBinder< QFileDialog >::styleChange (QStyle& arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "styleChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QStyle*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::styleChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QFileDialog >::focusInEvent (QFocusEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "focusInEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QFocusEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::focusInEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QFileDialog >::keyPressEvent (QKeyEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "keyPressEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QKeyEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QDialog::keyPressEvent(arg1);
  }
  lua_settop(L, oldtop);
}
const QMetaObject * LuaBinder< QFileDialog >::metaObject () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "metaObject");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QFileDialog::metaObject();
  }
  const QMetaObject * ret = *static_cast<QMetaObject**>(lqtL_checkudata(L, -1, "QMetaObject*"));
  lua_settop(L, oldtop);
  return ret;
}
int LuaBinder< QFileDialog >::devType () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "devType");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::devType();
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QFileDialog >::setVisible (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "setVisible");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QDialog::setVisible(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QFileDialog >::keyReleaseEvent (QKeyEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "keyReleaseEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QKeyEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::keyReleaseEvent(arg1);
  }
  lua_settop(L, oldtop);
}
QPaintEngine * LuaBinder< QFileDialog >::paintEngine () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "paintEngine");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::paintEngine();
  }
  QPaintEngine * ret = *static_cast<QPaintEngine**>(lqtL_checkudata(L, -1, "QPaintEngine*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QFileDialog >::dragLeaveEvent (QDragLeaveEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dragLeaveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDragLeaveEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::dragLeaveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QFileDialog >::mousePressEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mousePressEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::mousePressEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QFileDialog >::dragEnterEvent (QDragEnterEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dragEnterEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDragEnterEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::dragEnterEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QFileDialog >::closeEvent (QCloseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "closeEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QCloseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QDialog::closeEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QFileDialog >::enterEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "enterEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::enterEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QFileDialog >::accept () {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "accept");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QFileDialog::accept();
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QFileDialog >::tabletEvent (QTabletEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "tabletEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QTabletEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::tabletEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QFileDialog >::dragMoveEvent (QDragMoveEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dragMoveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDragMoveEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::dragMoveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QFileDialog >::changeEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "changeEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QFileDialog::changeEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QFileDialog >::moveEvent (QMoveEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "moveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMoveEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::moveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QFileDialog >::dropEvent (QDropEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dropEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDropEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::dropEvent(arg1);
  }
  lua_settop(L, oldtop);
}
QSize LuaBinder< QFileDialog >::sizeHint () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "sizeHint");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QDialog::sizeHint();
  }
  QSize ret = **static_cast<QSize**>(lqtL_checkudata(L, -1, "QSize*"));
  lua_settop(L, oldtop);
  return ret;
}
QVariant LuaBinder< QFileDialog >::inputMethodQuery (Qt::InputMethodQuery arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "inputMethodQuery");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushenum(L, arg1, "Qt::InputMethodQuery");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::inputMethodQuery(arg1);
  }
  QVariant ret = **static_cast<QVariant**>(lqtL_checkudata(L, -1, "QVariant*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QFileDialog >::showEvent (QShowEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "showEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QShowEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QDialog::showEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QFileDialog >::windowActivationChange (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "windowActivationChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::windowActivationChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QFileDialog >::paintEvent (QPaintEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "paintEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QPaintEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::paintEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QFileDialog >::contextMenuEvent (QContextMenuEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "contextMenuEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QContextMenuEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QDialog::contextMenuEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QFileDialog >::mouseDoubleClickEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mouseDoubleClickEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::mouseDoubleClickEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QFileDialog >::enabledChange (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "enabledChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::enabledChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QFileDialog >::disconnectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "disconnectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::disconnectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QFileDialog >::heightForWidth (int arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "heightForWidth");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushinteger(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::heightForWidth(arg1);
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QFileDialog >::fontChange (const QFont& arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "fontChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QFont*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::fontChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QFileDialog >::actionEvent (QActionEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "actionEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QActionEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::actionEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QFileDialog >::timerEvent (QTimerEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "timerEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QTimerEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::timerEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QFileDialog >::mouseMoveEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mouseMoveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::mouseMoveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QFileDialog >::metric (QPaintDevice::PaintDeviceMetric arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "metric");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushenum(L, arg1, "QPaintDevice::PaintDeviceMetric");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::metric(arg1);
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QFileDialog >::childEvent (QChildEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "childEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QChildEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::childEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QFileDialog >::connectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "connectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::connectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QFileDialog >::mouseReleaseEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mouseReleaseEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::mouseReleaseEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QFileDialog >::focusOutEvent (QFocusEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "focusOutEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QFocusEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::focusOutEvent(arg1);
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QFileDialog >::eventFilter (QObject * arg1, QEvent * arg2) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "eventFilter");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QObject*");
  lqtL_pushudata(L, arg2, "QEvent*");
  if (lua_isfunction(L, -2-2)) {
    lua_pcall(L, 2+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QDialog::eventFilter(arg1, arg2);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QFileDialog >::inputMethodEvent (QInputMethodEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "inputMethodEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QInputMethodEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::inputMethodEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QFileDialog >::wheelEvent (QWheelEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "wheelEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QWheelEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::wheelEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QFileDialog >::resizeEvent (QResizeEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "resizeEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QResizeEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QDialog::resizeEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QFileDialog >::paletteChange (const QPalette& arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "paletteChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QPalette*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::paletteChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QFileDialog >::languageChange () {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "languageChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::languageChange();
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QFileDialog >::event (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "event");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::event(arg1);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QFileDialog >::hideEvent (QHideEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "hideEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QHideEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::hideEvent(arg1);
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QFileDialog >::focusNextPrevChild (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "focusNextPrevChild");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::focusNextPrevChild(arg1);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QFileDialog >::done (int arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "done");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushinteger(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QFileDialog::done(arg1);
  }
  lua_settop(L, oldtop);
}
QSize LuaBinder< QFileDialog >::minimumSizeHint () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "minimumSizeHint");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QDialog::minimumSizeHint();
  }
  QSize ret = **static_cast<QSize**>(lqtL_checkudata(L, -1, "QSize*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QFileDialog >::leaveEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "leaveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::leaveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QFileDialog >::customEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "customEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::customEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QFileDialog >::reject () {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "reject");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QDialog::reject();
  }
  lua_settop(L, oldtop);
}
LuaBinder< QFileDialog >::  ~LuaBinder< QFileDialog > () {
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QFileDialog*");
  lua_getfield(L, -1, "~QFileDialog");

  if (lua_isfunction(L, -1)) {
    lua_insert(L, -2);
    lua_pcall(L, 1, 1, 0);
  } else {
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QFileDialog >::lqt_pushenum_ViewMode (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "Detail");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "Detail");
  lua_pushstring(L, "List");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "List");
  lua_pushcfunction(L, LuaBinder< QFileDialog >::lqt_pushenum_ViewMode_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QFileDialog::ViewMode");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QFileDialog >::lqt_pushenum_ViewMode_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QFileDialog::ViewMode>*) + sizeof(QFlags<QFileDialog::ViewMode>));
  QFlags<QFileDialog::ViewMode> *fl = static_cast<QFlags<QFileDialog::ViewMode>*>( static_cast<void*>(&static_cast<QFlags<QFileDialog::ViewMode>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QFileDialog::ViewMode>(lqtL_toenum(L, i, "QFileDialog::ViewMode"));
	}
	if (luaL_newmetatable(L, "QFlags<QFileDialog::ViewMode>*")) {
		lua_pushstring(L, "QFlags<QFileDialog::ViewMode>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinder< QFileDialog >::lqt_pushenum_FileMode (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "AnyFile");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "AnyFile");
  lua_pushstring(L, "ExistingFile");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "ExistingFile");
  lua_pushstring(L, "Directory");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "Directory");
  lua_pushstring(L, "ExistingFiles");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "ExistingFiles");
  lua_pushstring(L, "DirectoryOnly");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "DirectoryOnly");
  lua_pushcfunction(L, LuaBinder< QFileDialog >::lqt_pushenum_FileMode_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QFileDialog::FileMode");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QFileDialog >::lqt_pushenum_FileMode_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QFileDialog::FileMode>*) + sizeof(QFlags<QFileDialog::FileMode>));
  QFlags<QFileDialog::FileMode> *fl = static_cast<QFlags<QFileDialog::FileMode>*>( static_cast<void*>(&static_cast<QFlags<QFileDialog::FileMode>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QFileDialog::FileMode>(lqtL_toenum(L, i, "QFileDialog::FileMode"));
	}
	if (luaL_newmetatable(L, "QFlags<QFileDialog::FileMode>*")) {
		lua_pushstring(L, "QFlags<QFileDialog::FileMode>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinder< QFileDialog >::lqt_pushenum_AcceptMode (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "AcceptOpen");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "AcceptOpen");
  lua_pushstring(L, "AcceptSave");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "AcceptSave");
  lua_pushcfunction(L, LuaBinder< QFileDialog >::lqt_pushenum_AcceptMode_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QFileDialog::AcceptMode");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QFileDialog >::lqt_pushenum_AcceptMode_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QFileDialog::AcceptMode>*) + sizeof(QFlags<QFileDialog::AcceptMode>));
  QFlags<QFileDialog::AcceptMode> *fl = static_cast<QFlags<QFileDialog::AcceptMode>*>( static_cast<void*>(&static_cast<QFlags<QFileDialog::AcceptMode>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QFileDialog::AcceptMode>(lqtL_toenum(L, i, "QFileDialog::AcceptMode"));
	}
	if (luaL_newmetatable(L, "QFlags<QFileDialog::AcceptMode>*")) {
		lua_pushstring(L, "QFlags<QFileDialog::AcceptMode>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinder< QFileDialog >::lqt_pushenum_DialogLabel (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "LookIn");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "LookIn");
  lua_pushstring(L, "FileName");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "FileName");
  lua_pushstring(L, "FileType");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "FileType");
  lua_pushstring(L, "Accept");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "Accept");
  lua_pushstring(L, "Reject");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "Reject");
  lua_pushcfunction(L, LuaBinder< QFileDialog >::lqt_pushenum_DialogLabel_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QFileDialog::DialogLabel");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QFileDialog >::lqt_pushenum_DialogLabel_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QFileDialog::DialogLabel>*) + sizeof(QFlags<QFileDialog::DialogLabel>));
  QFlags<QFileDialog::DialogLabel> *fl = static_cast<QFlags<QFileDialog::DialogLabel>*>( static_cast<void*>(&static_cast<QFlags<QFileDialog::DialogLabel>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QFileDialog::DialogLabel>(lqtL_toenum(L, i, "QFileDialog::DialogLabel"));
	}
	if (luaL_newmetatable(L, "QFlags<QFileDialog::DialogLabel>*")) {
		lua_pushstring(L, "QFlags<QFileDialog::DialogLabel>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinder< QFileDialog >::lqt_pushenum_Option (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "ShowDirsOnly");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "ShowDirsOnly");
  lua_pushstring(L, "DontResolveSymlinks");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "DontResolveSymlinks");
  lua_pushstring(L, "DontConfirmOverwrite");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "DontConfirmOverwrite");
  lua_pushstring(L, "DontUseSheet");
  lua_rawseti(L, enum_table, 8);
  lua_pushinteger(L, 8);
  lua_setfield(L, enum_table, "DontUseSheet");
  lua_pushstring(L, "DontUseNativeDialog");
  lua_rawseti(L, enum_table, 16);
  lua_pushinteger(L, 16);
  lua_setfield(L, enum_table, "DontUseNativeDialog");
  lua_pushcfunction(L, LuaBinder< QFileDialog >::lqt_pushenum_Option_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QFileDialog::Option");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QFileDialog >::lqt_pushenum_Option_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QFileDialog::Option>*) + sizeof(QFlags<QFileDialog::Option>));
  QFlags<QFileDialog::Option> *fl = static_cast<QFlags<QFileDialog::Option>*>( static_cast<void*>(&static_cast<QFlags<QFileDialog::Option>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QFileDialog::Option>(lqtL_toenum(L, i, "QFileDialog::Option"));
	}
	if (luaL_newmetatable(L, "QFlags<QFileDialog::Option>*")) {
		lua_pushstring(L, "QFlags<QFileDialog::Option>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int luaopen_QFileDialog (lua_State *L) {
  if (luaL_newmetatable(L, "QFileDialog*")) {
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__confirmOverwrite);
    lua_setfield(L, -2, "confirmOverwrite");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__selectFilter);
    lua_setfield(L, -2, "selectFilter");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__saveState);
    lua_setfield(L, -2, "saveState");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__resolveSymlinks);
    lua_setfield(L, -2, "resolveSymlinks");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__setAcceptMode);
    lua_setfield(L, -2, "setAcceptMode");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__setSidebarUrls);
    lua_setfield(L, -2, "setSidebarUrls");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__trUtf8);
    lua_setfield(L, -2, "trUtf8");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__filters);
    lua_setfield(L, -2, "filters");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__tr);
    lua_setfield(L, -2, "tr");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__sidebarUrls);
    lua_setfield(L, -2, "sidebarUrls");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__setConfirmOverwrite);
    lua_setfield(L, -2, "setConfirmOverwrite");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__setViewMode);
    lua_setfield(L, -2, "setViewMode");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__acceptMode);
    lua_setfield(L, -2, "acceptMode");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__setHistory);
    lua_setfield(L, -2, "setHistory");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__fileMode);
    lua_setfield(L, -2, "fileMode");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__setDefaultSuffix);
    lua_setfield(L, -2, "setDefaultSuffix");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__getOpenFileName);
    lua_setfield(L, -2, "getOpenFileName");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__setReadOnly);
    lua_setfield(L, -2, "setReadOnly");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__setLabelText);
    lua_setfield(L, -2, "setLabelText");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__selectFile);
    lua_setfield(L, -2, "selectFile");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__setFilters);
    lua_setfield(L, -2, "setFilters");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__selectedFiles);
    lua_setfield(L, -2, "selectedFiles");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__viewMode);
    lua_setfield(L, -2, "viewMode");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__setFileMode);
    lua_setfield(L, -2, "setFileMode");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__getExistingDirectory);
    lua_setfield(L, -2, "getExistingDirectory");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__delete);
    lua_setfield(L, -2, "delete");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__metaObject);
    lua_setfield(L, -2, "metaObject");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__getSaveFileName);
    lua_setfield(L, -2, "getSaveFileName");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__setResolveSymlinks);
    lua_setfield(L, -2, "setResolveSymlinks");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__new);
    lua_setfield(L, -2, "new");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__selectedFilter);
    lua_setfield(L, -2, "selectedFilter");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__directory);
    lua_setfield(L, -2, "directory");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__isReadOnly);
    lua_setfield(L, -2, "isReadOnly");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__defaultSuffix);
    lua_setfield(L, -2, "defaultSuffix");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__setFilter);
    lua_setfield(L, -2, "setFilter");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__setDirectory);
    lua_setfield(L, -2, "setDirectory");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__setItemDelegate);
    lua_setfield(L, -2, "setItemDelegate");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__restoreState);
    lua_setfield(L, -2, "restoreState");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__itemDelegate);
    lua_setfield(L, -2, "itemDelegate");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__history);
    lua_setfield(L, -2, "history");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__setIconProvider);
    lua_setfield(L, -2, "setIconProvider");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__iconProvider);
    lua_setfield(L, -2, "iconProvider");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__proxyModel);
    lua_setfield(L, -2, "proxyModel");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__labelText);
    lua_setfield(L, -2, "labelText");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__setProxyModel);
    lua_setfield(L, -2, "setProxyModel");
    lua_pushcfunction(L, LuaBinder< QFileDialog >::__LuaWrapCall__getOpenFileNames);
    lua_setfield(L, -2, "getOpenFileNames");
    LuaBinder< QFileDialog >::lqt_pushenum_ViewMode(L);
    lua_setfield(L, -2, "ViewMode");
    LuaBinder< QFileDialog >::lqt_pushenum_FileMode(L);
    lua_setfield(L, -2, "FileMode");
    LuaBinder< QFileDialog >::lqt_pushenum_AcceptMode(L);
    lua_setfield(L, -2, "AcceptMode");
    LuaBinder< QFileDialog >::lqt_pushenum_DialogLabel(L);
    lua_setfield(L, -2, "DialogLabel");
    LuaBinder< QFileDialog >::lqt_pushenum_Option(L);
    lua_setfield(L, -2, "Option");
    lua_newtable(L);
    lua_pushboolean(L, 1);
    lua_setfield(L, -2, "QDialog*");
    lua_pushboolean(L, 0);
    lua_setfield(L, -2, "QObject*");
    lua_pushboolean(L, 0);
    lua_setfield(L, -2, "QPaintDevice*");
    lua_pushboolean(L, 0);
    lua_setfield(L, -2, "QWidget*");
    lua_setfield(L, -2, "__base");
    lua_pushcfunction(L, lqtL_newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, lqtL_index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, lqtL_gc);
    lua_setfield(L, -2, "__gc");
    lua_pushstring(L, "QFileDialog");
    lua_setfield(L, -2, "__qtype");
		lua_setglobal(L, "QFileDialog");
  } else {
		lua_pop(L, 1);
	}
  return 0;
}
