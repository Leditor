#include "lqt_common.hpp"
#include <QTextBlockUserData>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QTextBlockUserData > : public QTextBlockUserData {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__delete (lua_State *L);
  ~LuaBinder< QTextBlockUserData > ();
};

extern "C" int luaopen_QTextBlockUserData (lua_State *L);
