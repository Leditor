#include "lqt_bind_QLineEdit.hpp"

int LuaBinder< QLineEdit >::__LuaWrapCall__setValidator (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QValidator * arg1 = *static_cast<QValidator**>(lqtL_checkudata(L, 2, "QValidator*"));
  __lua__obj->QLineEdit::setValidator(arg1);
  return 0;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__inputMask (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QLineEdit::inputMask();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__createStandardContextMenu (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QMenu * ret = __lua__obj->QLineEdit::createStandardContextMenu();
  lqtL_pushudata(L, ret, "QMenu*");
  return 1;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__sizeHint (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QSize ret = __lua__obj->QLineEdit::sizeHint();
  lqtL_passudata(L, new QSize(ret), "QSize*");
  return 1;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__validator (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QValidator * ret = __lua__obj->QLineEdit::validator();
  lqtL_pushudata(L, ret, "QValidator*");
  return 1;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__cursorWordBackward (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QLineEdit::cursorWordBackward(arg1);
  return 0;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__cut (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QLineEdit::cut();
  return 0;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__hasSelectedText (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QLineEdit::hasSelectedText();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__copy (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QLineEdit::copy();
  return 0;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__home (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QLineEdit::home(arg1);
  return 0;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QLineEdit::trUtf8(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QLineEdit::trUtf8(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__trUtf8 (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0xcbcfe0;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0xcc96b0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xb3cdd0;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xb907f0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xb26f00;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__trUtf8__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__trUtf8__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__trUtf8 matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__cursorWordForward (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QLineEdit::cursorWordForward(arg1);
  return 0;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QLineEdit::tr(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QLineEdit::tr(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__tr (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0xd57b30;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0xd5b120;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xcb9fe0;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xc5ca50;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x6b0720;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__tr__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__tr__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__tr matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__cursorPositionAt (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPoint& arg1 = **static_cast<QPoint**>(lqtL_checkudata(L, 2, "QPoint*"));
  int ret = __lua__obj->QLineEdit::cursorPositionAt(arg1);
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__isReadOnly (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QLineEdit::isReadOnly();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__insert (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QLineEdit::insert(arg1);
  return 0;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__setMaxLength (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QLineEdit::setMaxLength(arg1);
  return 0;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__text (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QLineEdit::text();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__event (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QEvent * arg1 = *static_cast<QEvent**>(lqtL_checkudata(L, 2, "QEvent*"));
  bool ret = __lua__obj->QLineEdit::event(arg1);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__isModified (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QLineEdit::isModified();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__hasFrame (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QLineEdit::hasFrame();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__setCursorPosition (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QLineEdit::setCursorPosition(arg1);
  return 0;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__echoMode (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QLineEdit::EchoMode ret = __lua__obj->QLineEdit::echoMode();
  lqtL_pushenum(L, ret, "QLineEdit::EchoMode");
  return 1;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__isUndoAvailable (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QLineEdit::isUndoAvailable();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__cursorBackward (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  int arg2 = lua_isnumber(L, 3)?lua_tointeger(L, 3):static_cast< int >(1);
  __lua__obj->QLineEdit::cursorBackward(arg1, arg2);
  return 0;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__setSelection (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int arg2 = lua_tointeger(L, 3);
  __lua__obj->QLineEdit::setSelection(arg1, arg2);
  return 0;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__setReadOnly (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QLineEdit::setReadOnly(arg1);
  return 0;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__setModified (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QLineEdit::setModified(arg1);
  return 0;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__setDragEnabled (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QLineEdit::setDragEnabled(arg1);
  return 0;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__cursorPosition (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QLineEdit::cursorPosition();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__isRedoAvailable (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QLineEdit::isRedoAvailable();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__del (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QLineEdit::del();
  return 0;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__displayText (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QLineEdit::displayText();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__setText (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QLineEdit::setText(arg1);
  return 0;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__minimumSizeHint (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QSize ret = __lua__obj->QLineEdit::minimumSizeHint();
  lqtL_passudata(L, new QSize(ret), "QSize*");
  return 1;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__hasAcceptableInput (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QLineEdit::hasAcceptableInput();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__selectionStart (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QLineEdit::selectionStart();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__backspace (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QLineEdit::backspace();
  return 0;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__setEchoMode (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QLineEdit::EchoMode arg1 = static_cast<QLineEdit::EchoMode>(lqtL_toenum(L, 2, "QLineEdit::EchoMode"));
  __lua__obj->QLineEdit::setEchoMode(arg1);
  return 0;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__end (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QLineEdit::end(arg1);
  return 0;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__selectedText (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QLineEdit::selectedText();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__setCompleter (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QCompleter * arg1 = *static_cast<QCompleter**>(lqtL_checkudata(L, 2, "QCompleter*"));
  __lua__obj->QLineEdit::setCompleter(arg1);
  return 0;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__metaObject (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QMetaObject * ret = __lua__obj->QLineEdit::metaObject();
  lqtL_pushudata(L, ret, "QMetaObject*");
  return 1;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__new__OverloadedVersion__1 (lua_State *L) {
  QWidget * arg1 = lqtL_testudata(L, 1, "QWidget*")?*static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*")):static_cast< QWidget * >(0);
  QLineEdit * ret = new LuaBinder< QLineEdit >(L, arg1);
  lqtL_passudata(L, ret, "QLineEdit*");
  return 1;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__new__OverloadedVersion__2 (lua_State *L) {
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 1), lua_objlen(L, 1));
  QWidget * arg2 = lqtL_testudata(L, 2, "QWidget*")?*static_cast<QWidget**>(lqtL_checkudata(L, 2, "QWidget*")):static_cast< QWidget * >(0);
  QLineEdit * ret = new LuaBinder< QLineEdit >(L, arg1, arg2);
  lqtL_passudata(L, ret, "QLineEdit*");
  return 1;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__new (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if (lqtL_testudata(L, 1, "QWidget*")) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x81b8f0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x775c60;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_testudata(L, 2, "QWidget*")) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x7e63a0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__new__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__new__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__new matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__inputMethodQuery (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::InputMethodQuery arg1 = static_cast<Qt::InputMethodQuery>(lqtL_toenum(L, 2, "Qt::InputMethodQuery"));
  QVariant ret = __lua__obj->QLineEdit::inputMethodQuery(arg1);
  lqtL_passudata(L, new QVariant(ret), "QVariant*");
  return 1;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__delete (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  delete __lua__obj;
  __lua__obj = 0;
  return 0;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__redo (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QLineEdit::redo();
  return 0;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__deselect (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QLineEdit::deselect();
  return 0;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__completer (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QCompleter * ret = __lua__obj->QLineEdit::completer();
  lqtL_pushudata(L, ret, "QCompleter*");
  return 1;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__setInputMask (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QLineEdit::setInputMask(arg1);
  return 0;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__clear (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QLineEdit::clear();
  return 0;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__maxLength (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QLineEdit::maxLength();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__setFrame (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QLineEdit::setFrame(arg1);
  return 0;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__selectAll (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QLineEdit::selectAll();
  return 0;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__alignment (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<Qt::AlignmentFlag> ret = __lua__obj->QLineEdit::alignment();
  lqtL_passudata(L, new QFlags<Qt::AlignmentFlag>(ret), "QFlags<Qt::AlignmentFlag>*");
  return 1;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__cursorForward (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  int arg2 = lua_isnumber(L, 3)?lua_tointeger(L, 3):static_cast< int >(1);
  __lua__obj->QLineEdit::cursorForward(arg1, arg2);
  return 0;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__undo (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QLineEdit::undo();
  return 0;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__paste (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QLineEdit::paste();
  return 0;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__dragEnabled (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QLineEdit::dragEnabled();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QLineEdit >::__LuaWrapCall__setAlignment (lua_State *L) {
  QLineEdit *& __lua__obj = *static_cast<QLineEdit**>(lqtL_checkudata(L, 1, "QLineEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<Qt::AlignmentFlag> arg1 = **static_cast<QFlags<Qt::AlignmentFlag>**>(lqtL_checkudata(L, 2, "QFlags<Qt::AlignmentFlag>*"));
  __lua__obj->QLineEdit::setAlignment(arg1);
  return 0;
}
void LuaBinder< QLineEdit >::styleChange (QStyle& arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "styleChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QStyle*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::styleChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QLineEdit >::mouseReleaseEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mouseReleaseEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QLineEdit::mouseReleaseEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QLineEdit >::keyPressEvent (QKeyEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "keyPressEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QKeyEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QLineEdit::keyPressEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QLineEdit >::contextMenuEvent (QContextMenuEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "contextMenuEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QContextMenuEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QLineEdit::contextMenuEvent(arg1);
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QLineEdit >::devType () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "devType");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::devType();
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QLineEdit >::setVisible (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "setVisible");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::setVisible(arg1);
  }
  lua_settop(L, oldtop);
}
QVariant LuaBinder< QLineEdit >::inputMethodQuery (Qt::InputMethodQuery arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "inputMethodQuery");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushenum(L, arg1, "Qt::InputMethodQuery");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QLineEdit::inputMethodQuery(arg1);
  }
  QVariant ret = **static_cast<QVariant**>(lqtL_checkudata(L, -1, "QVariant*"));
  lua_settop(L, oldtop);
  return ret;
}
QPaintEngine * LuaBinder< QLineEdit >::paintEngine () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "paintEngine");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::paintEngine();
  }
  QPaintEngine * ret = *static_cast<QPaintEngine**>(lqtL_checkudata(L, -1, "QPaintEngine*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QLineEdit >::dragLeaveEvent (QDragLeaveEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dragLeaveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDragLeaveEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QLineEdit::dragLeaveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QLineEdit >::mousePressEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mousePressEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QLineEdit::mousePressEvent(arg1);
  }
  lua_settop(L, oldtop);
}
QSize LuaBinder< QLineEdit >::sizeHint () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "sizeHint");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QLineEdit::sizeHint();
  }
  QSize ret = **static_cast<QSize**>(lqtL_checkudata(L, -1, "QSize*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QLineEdit >::changeEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "changeEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QLineEdit::changeEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QLineEdit >::tabletEvent (QTabletEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "tabletEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QTabletEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::tabletEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QLineEdit >::enterEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "enterEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::enterEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QLineEdit >::closeEvent (QCloseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "closeEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QCloseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::closeEvent(arg1);
  }
  lua_settop(L, oldtop);
}
QSize LuaBinder< QLineEdit >::minimumSizeHint () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "minimumSizeHint");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QLineEdit::minimumSizeHint();
  }
  QSize ret = **static_cast<QSize**>(lqtL_checkudata(L, -1, "QSize*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QLineEdit >::dragEnterEvent (QDragEnterEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dragEnterEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDragEnterEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QLineEdit::dragEnterEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QLineEdit >::actionEvent (QActionEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "actionEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QActionEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::actionEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QLineEdit >::showEvent (QShowEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "showEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QShowEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::showEvent(arg1);
  }
  lua_settop(L, oldtop);
}
const QMetaObject * LuaBinder< QLineEdit >::metaObject () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "metaObject");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QLineEdit::metaObject();
  }
  const QMetaObject * ret = *static_cast<QMetaObject**>(lqtL_checkudata(L, -1, "QMetaObject*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QLineEdit >::paintEvent (QPaintEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "paintEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QPaintEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QLineEdit::paintEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QLineEdit >::dragMoveEvent (QDragMoveEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dragMoveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDragMoveEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QLineEdit::dragMoveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QLineEdit >::focusInEvent (QFocusEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "focusInEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QFocusEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QLineEdit::focusInEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QLineEdit >::enabledChange (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "enabledChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::enabledChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QLineEdit >::windowActivationChange (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "windowActivationChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::windowActivationChange(arg1);
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QLineEdit >::heightForWidth (int arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "heightForWidth");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushinteger(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::heightForWidth(arg1);
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QLineEdit >::mouseDoubleClickEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mouseDoubleClickEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QLineEdit::mouseDoubleClickEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QLineEdit >::fontChange (const QFont& arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "fontChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QFont*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::fontChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QLineEdit >::timerEvent (QTimerEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "timerEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QTimerEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::timerEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QLineEdit >::mouseMoveEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mouseMoveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QLineEdit::mouseMoveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QLineEdit >::disconnectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "disconnectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::disconnectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QLineEdit >::childEvent (QChildEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "childEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QChildEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::childEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QLineEdit >::focusOutEvent (QFocusEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "focusOutEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QFocusEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QLineEdit::focusOutEvent(arg1);
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QLineEdit >::metric (QPaintDevice::PaintDeviceMetric arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "metric");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushenum(L, arg1, "QPaintDevice::PaintDeviceMetric");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::metric(arg1);
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
bool LuaBinder< QLineEdit >::focusNextPrevChild (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "focusNextPrevChild");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::focusNextPrevChild(arg1);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
bool LuaBinder< QLineEdit >::eventFilter (QObject * arg1, QEvent * arg2) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "eventFilter");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QObject*");
  lqtL_pushudata(L, arg2, "QEvent*");
  if (lua_isfunction(L, -2-2)) {
    lua_pcall(L, 2+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QObject::eventFilter(arg1, arg2);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QLineEdit >::inputMethodEvent (QInputMethodEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "inputMethodEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QInputMethodEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QLineEdit::inputMethodEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QLineEdit >::wheelEvent (QWheelEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "wheelEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QWheelEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::wheelEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QLineEdit >::resizeEvent (QResizeEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "resizeEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QResizeEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::resizeEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QLineEdit >::paletteChange (const QPalette& arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "paletteChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QPalette*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::paletteChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QLineEdit >::languageChange () {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "languageChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::languageChange();
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QLineEdit >::event (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "event");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QLineEdit::event(arg1);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QLineEdit >::hideEvent (QHideEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "hideEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QHideEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::hideEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QLineEdit >::connectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "connectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::connectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QLineEdit >::moveEvent (QMoveEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "moveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMoveEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::moveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QLineEdit >::keyReleaseEvent (QKeyEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "keyReleaseEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QKeyEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::keyReleaseEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QLineEdit >::leaveEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "leaveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::leaveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QLineEdit >::dropEvent (QDropEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dropEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDropEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QLineEdit::dropEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QLineEdit >::customEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "customEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::customEvent(arg1);
  }
  lua_settop(L, oldtop);
}
LuaBinder< QLineEdit >::  ~LuaBinder< QLineEdit > () {
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QLineEdit*");
  lua_getfield(L, -1, "~QLineEdit");

  if (lua_isfunction(L, -1)) {
    lua_insert(L, -2);
    lua_pcall(L, 1, 1, 0);
  } else {
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QLineEdit >::lqt_pushenum_EchoMode (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "Normal");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "Normal");
  lua_pushstring(L, "NoEcho");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "NoEcho");
  lua_pushstring(L, "Password");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "Password");
  lua_pushstring(L, "PasswordEchoOnEdit");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "PasswordEchoOnEdit");
  lua_pushcfunction(L, LuaBinder< QLineEdit >::lqt_pushenum_EchoMode_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QLineEdit::EchoMode");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QLineEdit >::lqt_pushenum_EchoMode_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QLineEdit::EchoMode>*) + sizeof(QFlags<QLineEdit::EchoMode>));
  QFlags<QLineEdit::EchoMode> *fl = static_cast<QFlags<QLineEdit::EchoMode>*>( static_cast<void*>(&static_cast<QFlags<QLineEdit::EchoMode>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QLineEdit::EchoMode>(lqtL_toenum(L, i, "QLineEdit::EchoMode"));
	}
	if (luaL_newmetatable(L, "QFlags<QLineEdit::EchoMode>*")) {
		lua_pushstring(L, "QFlags<QLineEdit::EchoMode>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int luaopen_QLineEdit (lua_State *L) {
  if (luaL_newmetatable(L, "QLineEdit*")) {
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__setValidator);
    lua_setfield(L, -2, "setValidator");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__inputMask);
    lua_setfield(L, -2, "inputMask");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__createStandardContextMenu);
    lua_setfield(L, -2, "createStandardContextMenu");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__sizeHint);
    lua_setfield(L, -2, "sizeHint");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__validator);
    lua_setfield(L, -2, "validator");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__cursorWordBackward);
    lua_setfield(L, -2, "cursorWordBackward");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__cut);
    lua_setfield(L, -2, "cut");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__hasSelectedText);
    lua_setfield(L, -2, "hasSelectedText");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__copy);
    lua_setfield(L, -2, "copy");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__home);
    lua_setfield(L, -2, "home");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__trUtf8);
    lua_setfield(L, -2, "trUtf8");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__cursorWordForward);
    lua_setfield(L, -2, "cursorWordForward");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__tr);
    lua_setfield(L, -2, "tr");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__cursorPositionAt);
    lua_setfield(L, -2, "cursorPositionAt");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__isReadOnly);
    lua_setfield(L, -2, "isReadOnly");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__insert);
    lua_setfield(L, -2, "insert");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__setMaxLength);
    lua_setfield(L, -2, "setMaxLength");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__text);
    lua_setfield(L, -2, "text");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__event);
    lua_setfield(L, -2, "event");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__isModified);
    lua_setfield(L, -2, "isModified");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__hasFrame);
    lua_setfield(L, -2, "hasFrame");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__setCursorPosition);
    lua_setfield(L, -2, "setCursorPosition");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__echoMode);
    lua_setfield(L, -2, "echoMode");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__isUndoAvailable);
    lua_setfield(L, -2, "isUndoAvailable");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__cursorBackward);
    lua_setfield(L, -2, "cursorBackward");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__setSelection);
    lua_setfield(L, -2, "setSelection");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__setReadOnly);
    lua_setfield(L, -2, "setReadOnly");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__setModified);
    lua_setfield(L, -2, "setModified");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__setDragEnabled);
    lua_setfield(L, -2, "setDragEnabled");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__cursorPosition);
    lua_setfield(L, -2, "cursorPosition");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__isRedoAvailable);
    lua_setfield(L, -2, "isRedoAvailable");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__del);
    lua_setfield(L, -2, "del");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__displayText);
    lua_setfield(L, -2, "displayText");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__setText);
    lua_setfield(L, -2, "setText");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__minimumSizeHint);
    lua_setfield(L, -2, "minimumSizeHint");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__hasAcceptableInput);
    lua_setfield(L, -2, "hasAcceptableInput");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__selectionStart);
    lua_setfield(L, -2, "selectionStart");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__backspace);
    lua_setfield(L, -2, "backspace");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__setEchoMode);
    lua_setfield(L, -2, "setEchoMode");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__end);
    lua_setfield(L, -2, "end");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__selectedText);
    lua_setfield(L, -2, "selectedText");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__setCompleter);
    lua_setfield(L, -2, "setCompleter");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__metaObject);
    lua_setfield(L, -2, "metaObject");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__new);
    lua_setfield(L, -2, "new");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__inputMethodQuery);
    lua_setfield(L, -2, "inputMethodQuery");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__delete);
    lua_setfield(L, -2, "delete");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__redo);
    lua_setfield(L, -2, "redo");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__deselect);
    lua_setfield(L, -2, "deselect");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__completer);
    lua_setfield(L, -2, "completer");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__setInputMask);
    lua_setfield(L, -2, "setInputMask");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__clear);
    lua_setfield(L, -2, "clear");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__maxLength);
    lua_setfield(L, -2, "maxLength");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__setFrame);
    lua_setfield(L, -2, "setFrame");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__selectAll);
    lua_setfield(L, -2, "selectAll");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__alignment);
    lua_setfield(L, -2, "alignment");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__cursorForward);
    lua_setfield(L, -2, "cursorForward");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__undo);
    lua_setfield(L, -2, "undo");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__paste);
    lua_setfield(L, -2, "paste");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__dragEnabled);
    lua_setfield(L, -2, "dragEnabled");
    lua_pushcfunction(L, LuaBinder< QLineEdit >::__LuaWrapCall__setAlignment);
    lua_setfield(L, -2, "setAlignment");
    LuaBinder< QLineEdit >::lqt_pushenum_EchoMode(L);
    lua_setfield(L, -2, "EchoMode");
    lua_newtable(L);
    lua_pushboolean(L, 1);
    lua_setfield(L, -2, "QWidget*");
    lua_pushboolean(L, 0);
    lua_setfield(L, -2, "QObject*");
    lua_pushboolean(L, 0);
    lua_setfield(L, -2, "QPaintDevice*");
    lua_setfield(L, -2, "__base");
    lua_pushcfunction(L, lqtL_newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, lqtL_index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, lqtL_gc);
    lua_setfield(L, -2, "__gc");
    lua_pushstring(L, "QLineEdit");
    lua_setfield(L, -2, "__qtype");
		lua_setglobal(L, "QLineEdit");
  } else {
		lua_pop(L, 1);
	}
  return 0;
}
