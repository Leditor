#include "lqt_common.hpp"
#include <QColor>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QColor > : public QColor {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__cyan (lua_State *L);
  static int __LuaWrapCall__fromCmyk (lua_State *L);
  static int __LuaWrapCall__cyanF (lua_State *L);
  static int __LuaWrapCall__redF (lua_State *L);
  static int __LuaWrapCall__setCmyk (lua_State *L);
  static int __LuaWrapCall__isValid (lua_State *L);
  static int __LuaWrapCall__black (lua_State *L);
  static int __LuaWrapCall__getCmyk (lua_State *L);
  static int __LuaWrapCall__yellow (lua_State *L);
  static int __LuaWrapCall__setGreen (lua_State *L);
  static int __LuaWrapCall__setAlpha (lua_State *L);
  static int __LuaWrapCall__convertTo (lua_State *L);
  static int __LuaWrapCall__setHsv (lua_State *L);
  static int __LuaWrapCall__toCmyk (lua_State *L);
  static int __LuaWrapCall__hueF (lua_State *L);
  static int __LuaWrapCall__fromCmykF (lua_State *L);
  static int __LuaWrapCall__darker (lua_State *L);
  static int __LuaWrapCall__setRgba (lua_State *L);
  static int __LuaWrapCall__rgb (lua_State *L);
  static int __LuaWrapCall__setGreenF (lua_State *L);
  static int __LuaWrapCall__green (lua_State *L);
  static int __LuaWrapCall__valueF (lua_State *L);
  static int __LuaWrapCall__setNamedColor (lua_State *L);
  static int __LuaWrapCall__fromHsvF (lua_State *L);
  static int __LuaWrapCall__fromRgbF (lua_State *L);
  static int __LuaWrapCall__setRed (lua_State *L);
  static int __LuaWrapCall__value (lua_State *L);
  static int __LuaWrapCall__lighter (lua_State *L);
  static int __LuaWrapCall__rgba (lua_State *L);
  static int __LuaWrapCall__magentaF (lua_State *L);
  static int __LuaWrapCall__getCmykF (lua_State *L);
  static int __LuaWrapCall__setBlueF (lua_State *L);
  static int __LuaWrapCall__colorNames (lua_State *L);
  static int __LuaWrapCall__getRgb (lua_State *L);
  static int __LuaWrapCall__name (lua_State *L);
  static int __LuaWrapCall__setRgb__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__setRgb__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__setRgb (lua_State *L);
  static int __LuaWrapCall__setAlphaF (lua_State *L);
  static int __LuaWrapCall__setCmykF (lua_State *L);
  static int __LuaWrapCall__setBlue (lua_State *L);
  static int __LuaWrapCall__greenF (lua_State *L);
  static int __LuaWrapCall__toHsv (lua_State *L);
  static int __LuaWrapCall__alphaF (lua_State *L);
  static int __LuaWrapCall__toRgb (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__4 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__5 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__6 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__7 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__8 (lua_State *L);
  static int __LuaWrapCall__new (lua_State *L);
  static int __LuaWrapCall__saturation (lua_State *L);
  static int __LuaWrapCall__alpha (lua_State *L);
  static int __LuaWrapCall__getHsv (lua_State *L);
  static int __LuaWrapCall__setRgbF (lua_State *L);
  static int __LuaWrapCall__magenta (lua_State *L);
  static int __LuaWrapCall__light (lua_State *L);
  static int __LuaWrapCall__hue (lua_State *L);
  static int __LuaWrapCall__setHsvF (lua_State *L);
  static int __LuaWrapCall__blue (lua_State *L);
  static int __LuaWrapCall__yellowF (lua_State *L);
  static int __LuaWrapCall__setRedF (lua_State *L);
  static int __LuaWrapCall__spec (lua_State *L);
  static int __LuaWrapCall__saturationF (lua_State *L);
  static int __LuaWrapCall__dark (lua_State *L);
  static int __LuaWrapCall__blackF (lua_State *L);
  static int __LuaWrapCall__blueF (lua_State *L);
  static int __LuaWrapCall__red (lua_State *L);
  static int __LuaWrapCall__fromRgb__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__fromRgb__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__fromRgb (lua_State *L);
  static int __LuaWrapCall__fromRgba (lua_State *L);
  static int __LuaWrapCall__fromHsv (lua_State *L);
  static int __LuaWrapCall__getRgbF (lua_State *L);
  static int __LuaWrapCall__getHsvF (lua_State *L);
  static int lqt_pushenum_Spec (lua_State *L);
  static int lqt_pushenum_Spec_QFLAGS_CREATOR (lua_State *L);
  LuaBinder< QColor > (lua_State *l):QColor(), L(l) {}
  LuaBinder< QColor > (lua_State *l, Qt::GlobalColor arg1):QColor(arg1), L(l) {}
  LuaBinder< QColor > (lua_State *l, int arg1, int arg2, int arg3, int arg4):QColor(arg1, arg2, arg3, arg4), L(l) {}
  LuaBinder< QColor > (lua_State *l, unsigned int arg1):QColor(arg1), L(l) {}
  LuaBinder< QColor > (lua_State *l, const QString& arg1):QColor(arg1), L(l) {}
  LuaBinder< QColor > (lua_State *l, const char * arg1):QColor(arg1), L(l) {}
  LuaBinder< QColor > (lua_State *l, const QColor& arg1):QColor(arg1), L(l) {}
  LuaBinder< QColor > (lua_State *l, QColor::Spec arg1):QColor(arg1), L(l) {}
};

extern "C" int luaopen_QColor (lua_State *L);
