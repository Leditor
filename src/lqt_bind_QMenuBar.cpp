#include "lqt_bind_QMenuBar.hpp"

int LuaBinder< QMenuBar >::__LuaWrapCall__setCornerWidget (lua_State *L) {
  QMenuBar *& __lua__obj = *static_cast<QMenuBar**>(lqtL_checkudata(L, 1, "QMenuBar*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 2, "QWidget*"));
  Qt::Corner arg2 = lqtL_isenum(L, 3, "Qt::Corner")?static_cast<Qt::Corner>(lqtL_toenum(L, 3, "Qt::Corner")):Qt::TopRightCorner;
  __lua__obj->QMenuBar::setCornerWidget(arg1, arg2);
  return 0;
}
int LuaBinder< QMenuBar >::__LuaWrapCall__sizeHint (lua_State *L) {
  QMenuBar *& __lua__obj = *static_cast<QMenuBar**>(lqtL_checkudata(L, 1, "QMenuBar*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QSize ret = __lua__obj->QMenuBar::sizeHint();
  lqtL_passudata(L, new QSize(ret), "QSize*");
  return 1;
}
int LuaBinder< QMenuBar >::__LuaWrapCall__addSeparator (lua_State *L) {
  QMenuBar *& __lua__obj = *static_cast<QMenuBar**>(lqtL_checkudata(L, 1, "QMenuBar*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QAction * ret = __lua__obj->QMenuBar::addSeparator();
  lqtL_pushudata(L, ret, "QAction*");
  return 1;
}
int LuaBinder< QMenuBar >::__LuaWrapCall__minimumSizeHint (lua_State *L) {
  QMenuBar *& __lua__obj = *static_cast<QMenuBar**>(lqtL_checkudata(L, 1, "QMenuBar*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QSize ret = __lua__obj->QMenuBar::minimumSizeHint();
  lqtL_passudata(L, new QSize(ret), "QSize*");
  return 1;
}
int LuaBinder< QMenuBar >::__LuaWrapCall__isDefaultUp (lua_State *L) {
  QMenuBar *& __lua__obj = *static_cast<QMenuBar**>(lqtL_checkudata(L, 1, "QMenuBar*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QMenuBar::isDefaultUp();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QMenuBar >::__LuaWrapCall__heightForWidth (lua_State *L) {
  QMenuBar *& __lua__obj = *static_cast<QMenuBar**>(lqtL_checkudata(L, 1, "QMenuBar*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int ret = __lua__obj->QMenuBar::heightForWidth(arg1);
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QMenuBar >::__LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QMenuBar::trUtf8(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QMenuBar >::__LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QMenuBar::trUtf8(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QMenuBar >::__LuaWrapCall__trUtf8 (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1066350;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x1065eb0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1068140;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1067870;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x10685d0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__trUtf8__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__trUtf8__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__trUtf8 matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QMenuBar >::__LuaWrapCall__actionAt (lua_State *L) {
  QMenuBar *& __lua__obj = *static_cast<QMenuBar**>(lqtL_checkudata(L, 1, "QMenuBar*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPoint& arg1 = **static_cast<QPoint**>(lqtL_checkudata(L, 2, "QPoint*"));
  QAction * ret = __lua__obj->QMenuBar::actionAt(arg1);
  lqtL_pushudata(L, ret, "QAction*");
  return 1;
}
int LuaBinder< QMenuBar >::__LuaWrapCall__metaObject (lua_State *L) {
  QMenuBar *& __lua__obj = *static_cast<QMenuBar**>(lqtL_checkudata(L, 1, "QMenuBar*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QMetaObject * ret = __lua__obj->QMenuBar::metaObject();
  lqtL_pushudata(L, ret, "QMetaObject*");
  return 1;
}
int LuaBinder< QMenuBar >::__LuaWrapCall__insertMenu (lua_State *L) {
  QMenuBar *& __lua__obj = *static_cast<QMenuBar**>(lqtL_checkudata(L, 1, "QMenuBar*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QAction * arg1 = *static_cast<QAction**>(lqtL_checkudata(L, 2, "QAction*"));
  QMenu * arg2 = *static_cast<QMenu**>(lqtL_checkudata(L, 3, "QMenu*"));
  QAction * ret = __lua__obj->QMenuBar::insertMenu(arg1, arg2);
  lqtL_pushudata(L, ret, "QAction*");
  return 1;
}
int LuaBinder< QMenuBar >::__LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QMenuBar::tr(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QMenuBar >::__LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QMenuBar::tr(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QMenuBar >::__LuaWrapCall__tr (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x10655f0;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x1065350;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1067030;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1067520;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x10678d0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__tr__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__tr__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__tr matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QMenuBar >::__LuaWrapCall__delete (lua_State *L) {
  QMenuBar *& __lua__obj = *static_cast<QMenuBar**>(lqtL_checkudata(L, 1, "QMenuBar*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  delete __lua__obj;
  __lua__obj = 0;
  return 0;
}
int LuaBinder< QMenuBar >::__LuaWrapCall__cornerWidget (lua_State *L) {
  QMenuBar *& __lua__obj = *static_cast<QMenuBar**>(lqtL_checkudata(L, 1, "QMenuBar*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::Corner arg1 = lqtL_isenum(L, 2, "Qt::Corner")?static_cast<Qt::Corner>(lqtL_toenum(L, 2, "Qt::Corner")):Qt::TopRightCorner;
  QWidget * ret = __lua__obj->QMenuBar::cornerWidget(arg1);
  lqtL_pushudata(L, ret, "QWidget*");
  return 1;
}
int LuaBinder< QMenuBar >::__LuaWrapCall__new (lua_State *L) {
  QWidget * arg1 = lqtL_testudata(L, 1, "QWidget*")?*static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*")):static_cast< QWidget * >(0);
  QMenuBar * ret = new LuaBinder< QMenuBar >(L, arg1);
  lqtL_passudata(L, ret, "QMenuBar*");
  return 1;
}
int LuaBinder< QMenuBar >::__LuaWrapCall__setVisible (lua_State *L) {
  QMenuBar *& __lua__obj = *static_cast<QMenuBar**>(lqtL_checkudata(L, 1, "QMenuBar*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QMenuBar::setVisible(arg1);
  return 0;
}
int LuaBinder< QMenuBar >::__LuaWrapCall__clear (lua_State *L) {
  QMenuBar *& __lua__obj = *static_cast<QMenuBar**>(lqtL_checkudata(L, 1, "QMenuBar*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QMenuBar::clear();
  return 0;
}
int LuaBinder< QMenuBar >::__LuaWrapCall__insertSeparator (lua_State *L) {
  QMenuBar *& __lua__obj = *static_cast<QMenuBar**>(lqtL_checkudata(L, 1, "QMenuBar*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QAction * arg1 = *static_cast<QAction**>(lqtL_checkudata(L, 2, "QAction*"));
  QAction * ret = __lua__obj->QMenuBar::insertSeparator(arg1);
  lqtL_pushudata(L, ret, "QAction*");
  return 1;
}
int LuaBinder< QMenuBar >::__LuaWrapCall__addMenu__OverloadedVersion__1 (lua_State *L) {
  QMenuBar *& __lua__obj = *static_cast<QMenuBar**>(lqtL_checkudata(L, 1, "QMenuBar*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QMenu * arg1 = *static_cast<QMenu**>(lqtL_checkudata(L, 2, "QMenu*"));
  QAction * ret = __lua__obj->QMenuBar::addMenu(arg1);
  lqtL_pushudata(L, ret, "QAction*");
  return 1;
}
int LuaBinder< QMenuBar >::__LuaWrapCall__addMenu__OverloadedVersion__2 (lua_State *L) {
  QMenuBar *& __lua__obj = *static_cast<QMenuBar**>(lqtL_checkudata(L, 1, "QMenuBar*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  QMenu * ret = __lua__obj->QMenuBar::addMenu(arg1);
  lqtL_pushudata(L, ret, "QMenu*");
  return 1;
}
int LuaBinder< QMenuBar >::__LuaWrapCall__addMenu__OverloadedVersion__3 (lua_State *L) {
  QMenuBar *& __lua__obj = *static_cast<QMenuBar**>(lqtL_checkudata(L, 1, "QMenuBar*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QIcon& arg1 = **static_cast<QIcon**>(lqtL_checkudata(L, 2, "QIcon*"));
  const QString& arg2 = QString::fromAscii(lua_tostring(L, 3), lua_objlen(L, 3));
  QMenu * ret = __lua__obj->QMenuBar::addMenu(arg1, arg2);
  lqtL_pushudata(L, ret, "QMenu*");
  return 1;
}
int LuaBinder< QMenuBar >::__LuaWrapCall__addMenu (lua_State *L) {
  int score[4];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QMenuBar*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QMenu*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x106cf00;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QMenuBar*")?premium:-premium*premium;
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x106d940;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  score[3] += lqtL_testudata(L, 1, "QMenuBar*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QIcon*")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x106e3a0;
  } else {
    score[3] -= premium*premium;
  }
  if ((lua_type(L, 3)==LUA_TSTRING)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x106de70;
  } else {
    score[3] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=3;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__addMenu__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__addMenu__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__addMenu__OverloadedVersion__3(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__addMenu matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QMenuBar >::__LuaWrapCall__setDefaultUp (lua_State *L) {
  QMenuBar *& __lua__obj = *static_cast<QMenuBar**>(lqtL_checkudata(L, 1, "QMenuBar*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QMenuBar::setDefaultUp(arg1);
  return 0;
}
int LuaBinder< QMenuBar >::__LuaWrapCall__addAction__OverloadedVersion__1 (lua_State *L) {
  QMenuBar *& __lua__obj = *static_cast<QMenuBar**>(lqtL_checkudata(L, 1, "QMenuBar*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  QAction * ret = __lua__obj->QMenuBar::addAction(arg1);
  lqtL_pushudata(L, ret, "QAction*");
  return 1;
}
int LuaBinder< QMenuBar >::__LuaWrapCall__addAction__OverloadedVersion__2 (lua_State *L) {
  QMenuBar *& __lua__obj = *static_cast<QMenuBar**>(lqtL_checkudata(L, 1, "QMenuBar*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  const QObject * arg2 = *static_cast<QObject**>(lqtL_checkudata(L, 3, "QObject*"));
  const char * arg3 = lua_tostring(L, 4);
  QAction * ret = __lua__obj->QMenuBar::addAction(arg1, arg2, arg3);
  lqtL_pushudata(L, ret, "QAction*");
  return 1;
}
int LuaBinder< QMenuBar >::__LuaWrapCall__addAction (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QMenuBar*")?premium:-premium*premium;
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x106b2b0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QMenuBar*")?premium:-premium*premium;
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x106bd10;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_testudata(L, 3, "QObject*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x106b7d0;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 4)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x106c1c0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__addAction__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__addAction__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__addAction matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QMenuBar >::__LuaWrapCall__setActiveAction (lua_State *L) {
  QMenuBar *& __lua__obj = *static_cast<QMenuBar**>(lqtL_checkudata(L, 1, "QMenuBar*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QAction * arg1 = *static_cast<QAction**>(lqtL_checkudata(L, 2, "QAction*"));
  __lua__obj->QMenuBar::setActiveAction(arg1);
  return 0;
}
int LuaBinder< QMenuBar >::__LuaWrapCall__actionGeometry (lua_State *L) {
  QMenuBar *& __lua__obj = *static_cast<QMenuBar**>(lqtL_checkudata(L, 1, "QMenuBar*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QAction * arg1 = *static_cast<QAction**>(lqtL_checkudata(L, 2, "QAction*"));
  QRect ret = __lua__obj->QMenuBar::actionGeometry(arg1);
  lqtL_passudata(L, new QRect(ret), "QRect*");
  return 1;
}
int LuaBinder< QMenuBar >::__LuaWrapCall__activeAction (lua_State *L) {
  QMenuBar *& __lua__obj = *static_cast<QMenuBar**>(lqtL_checkudata(L, 1, "QMenuBar*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QAction * ret = __lua__obj->QMenuBar::activeAction();
  lqtL_pushudata(L, ret, "QAction*");
  return 1;
}
void LuaBinder< QMenuBar >::styleChange (QStyle& arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "styleChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QStyle*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::styleChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMenuBar >::focusInEvent (QFocusEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "focusInEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QFocusEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QMenuBar::focusInEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMenuBar >::keyPressEvent (QKeyEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "keyPressEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QKeyEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QMenuBar::keyPressEvent(arg1);
  }
  lua_settop(L, oldtop);
}
const QMetaObject * LuaBinder< QMenuBar >::metaObject () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "metaObject");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QMenuBar::metaObject();
  }
  const QMetaObject * ret = *static_cast<QMetaObject**>(lqtL_checkudata(L, -1, "QMetaObject*"));
  lua_settop(L, oldtop);
  return ret;
}
int LuaBinder< QMenuBar >::devType () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "devType");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::devType();
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QMenuBar >::setVisible (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "setVisible");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QMenuBar::setVisible(arg1);
  }
  lua_settop(L, oldtop);
}
QVariant LuaBinder< QMenuBar >::inputMethodQuery (Qt::InputMethodQuery arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "inputMethodQuery");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushenum(L, arg1, "Qt::InputMethodQuery");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::inputMethodQuery(arg1);
  }
  QVariant ret = **static_cast<QVariant**>(lqtL_checkudata(L, -1, "QVariant*"));
  lua_settop(L, oldtop);
  return ret;
}
QPaintEngine * LuaBinder< QMenuBar >::paintEngine () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "paintEngine");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::paintEngine();
  }
  QPaintEngine * ret = *static_cast<QPaintEngine**>(lqtL_checkudata(L, -1, "QPaintEngine*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QMenuBar >::dragLeaveEvent (QDragLeaveEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dragLeaveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDragLeaveEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::dragLeaveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMenuBar >::mousePressEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mousePressEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QMenuBar::mousePressEvent(arg1);
  }
  lua_settop(L, oldtop);
}
QSize LuaBinder< QMenuBar >::sizeHint () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "sizeHint");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QMenuBar::sizeHint();
  }
  QSize ret = **static_cast<QSize**>(lqtL_checkudata(L, -1, "QSize*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QMenuBar >::changeEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "changeEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QMenuBar::changeEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMenuBar >::tabletEvent (QTabletEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "tabletEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QTabletEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::tabletEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMenuBar >::customEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "customEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::customEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMenuBar >::dropEvent (QDropEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dropEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDropEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::dropEvent(arg1);
  }
  lua_settop(L, oldtop);
}
QSize LuaBinder< QMenuBar >::minimumSizeHint () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "minimumSizeHint");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QMenuBar::minimumSizeHint();
  }
  QSize ret = **static_cast<QSize**>(lqtL_checkudata(L, -1, "QSize*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QMenuBar >::windowActivationChange (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "windowActivationChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::windowActivationChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMenuBar >::keyReleaseEvent (QKeyEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "keyReleaseEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QKeyEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::keyReleaseEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMenuBar >::showEvent (QShowEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "showEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QShowEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::showEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMenuBar >::mouseDoubleClickEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mouseDoubleClickEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::mouseDoubleClickEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMenuBar >::paintEvent (QPaintEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "paintEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QPaintEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QMenuBar::paintEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMenuBar >::dragMoveEvent (QDragMoveEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dragMoveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDragMoveEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::dragMoveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMenuBar >::contextMenuEvent (QContextMenuEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "contextMenuEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QContextMenuEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::contextMenuEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMenuBar >::enabledChange (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "enabledChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::enabledChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMenuBar >::mouseReleaseEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mouseReleaseEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QMenuBar::mouseReleaseEvent(arg1);
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QMenuBar >::heightForWidth (int arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "heightForWidth");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushinteger(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QMenuBar::heightForWidth(arg1);
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QMenuBar >::fontChange (const QFont& arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "fontChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QFont*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::fontChange(arg1);
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QMenuBar >::focusNextPrevChild (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "focusNextPrevChild");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::focusNextPrevChild(arg1);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QMenuBar >::disconnectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "disconnectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::disconnectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMenuBar >::mouseMoveEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mouseMoveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QMenuBar::mouseMoveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMenuBar >::timerEvent (QTimerEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "timerEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QTimerEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::timerEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMenuBar >::childEvent (QChildEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "childEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QChildEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::childEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMenuBar >::focusOutEvent (QFocusEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "focusOutEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QFocusEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QMenuBar::focusOutEvent(arg1);
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QMenuBar >::metric (QPaintDevice::PaintDeviceMetric arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "metric");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushenum(L, arg1, "QPaintDevice::PaintDeviceMetric");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::metric(arg1);
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QMenuBar >::connectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "connectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::connectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QMenuBar >::eventFilter (QObject * arg1, QEvent * arg2) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "eventFilter");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QObject*");
  lqtL_pushudata(L, arg2, "QEvent*");
  if (lua_isfunction(L, -2-2)) {
    lua_pcall(L, 2+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QMenuBar::eventFilter(arg1, arg2);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QMenuBar >::inputMethodEvent (QInputMethodEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "inputMethodEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QInputMethodEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::inputMethodEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMenuBar >::wheelEvent (QWheelEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "wheelEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QWheelEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::wheelEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMenuBar >::actionEvent (QActionEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "actionEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QActionEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QMenuBar::actionEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMenuBar >::paletteChange (const QPalette& arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "paletteChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QPalette*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::paletteChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMenuBar >::languageChange () {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "languageChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::languageChange();
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QMenuBar >::event (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "event");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QMenuBar::event(arg1);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QMenuBar >::hideEvent (QHideEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "hideEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QHideEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::hideEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMenuBar >::dragEnterEvent (QDragEnterEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dragEnterEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDragEnterEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::dragEnterEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMenuBar >::moveEvent (QMoveEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "moveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMoveEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::moveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMenuBar >::closeEvent (QCloseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "closeEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QCloseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::closeEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMenuBar >::leaveEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "leaveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QMenuBar::leaveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMenuBar >::resizeEvent (QResizeEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "resizeEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QResizeEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QMenuBar::resizeEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QMenuBar >::enterEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "enterEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::enterEvent(arg1);
  }
  lua_settop(L, oldtop);
}
LuaBinder< QMenuBar >::  ~LuaBinder< QMenuBar > () {
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QMenuBar*");
  lua_getfield(L, -1, "~QMenuBar");

  if (lua_isfunction(L, -1)) {
    lua_insert(L, -2);
    lua_pcall(L, 1, 1, 0);
  } else {
  }
  lua_settop(L, oldtop);
}
int luaopen_QMenuBar (lua_State *L) {
  if (luaL_newmetatable(L, "QMenuBar*")) {
    lua_pushcfunction(L, LuaBinder< QMenuBar >::__LuaWrapCall__setCornerWidget);
    lua_setfield(L, -2, "setCornerWidget");
    lua_pushcfunction(L, LuaBinder< QMenuBar >::__LuaWrapCall__sizeHint);
    lua_setfield(L, -2, "sizeHint");
    lua_pushcfunction(L, LuaBinder< QMenuBar >::__LuaWrapCall__addSeparator);
    lua_setfield(L, -2, "addSeparator");
    lua_pushcfunction(L, LuaBinder< QMenuBar >::__LuaWrapCall__minimumSizeHint);
    lua_setfield(L, -2, "minimumSizeHint");
    lua_pushcfunction(L, LuaBinder< QMenuBar >::__LuaWrapCall__isDefaultUp);
    lua_setfield(L, -2, "isDefaultUp");
    lua_pushcfunction(L, LuaBinder< QMenuBar >::__LuaWrapCall__heightForWidth);
    lua_setfield(L, -2, "heightForWidth");
    lua_pushcfunction(L, LuaBinder< QMenuBar >::__LuaWrapCall__trUtf8);
    lua_setfield(L, -2, "trUtf8");
    lua_pushcfunction(L, LuaBinder< QMenuBar >::__LuaWrapCall__actionAt);
    lua_setfield(L, -2, "actionAt");
    lua_pushcfunction(L, LuaBinder< QMenuBar >::__LuaWrapCall__metaObject);
    lua_setfield(L, -2, "metaObject");
    lua_pushcfunction(L, LuaBinder< QMenuBar >::__LuaWrapCall__insertMenu);
    lua_setfield(L, -2, "insertMenu");
    lua_pushcfunction(L, LuaBinder< QMenuBar >::__LuaWrapCall__tr);
    lua_setfield(L, -2, "tr");
    lua_pushcfunction(L, LuaBinder< QMenuBar >::__LuaWrapCall__delete);
    lua_setfield(L, -2, "delete");
    lua_pushcfunction(L, LuaBinder< QMenuBar >::__LuaWrapCall__cornerWidget);
    lua_setfield(L, -2, "cornerWidget");
    lua_pushcfunction(L, LuaBinder< QMenuBar >::__LuaWrapCall__new);
    lua_setfield(L, -2, "new");
    lua_pushcfunction(L, LuaBinder< QMenuBar >::__LuaWrapCall__setVisible);
    lua_setfield(L, -2, "setVisible");
    lua_pushcfunction(L, LuaBinder< QMenuBar >::__LuaWrapCall__clear);
    lua_setfield(L, -2, "clear");
    lua_pushcfunction(L, LuaBinder< QMenuBar >::__LuaWrapCall__insertSeparator);
    lua_setfield(L, -2, "insertSeparator");
    lua_pushcfunction(L, LuaBinder< QMenuBar >::__LuaWrapCall__addMenu);
    lua_setfield(L, -2, "addMenu");
    lua_pushcfunction(L, LuaBinder< QMenuBar >::__LuaWrapCall__setDefaultUp);
    lua_setfield(L, -2, "setDefaultUp");
    lua_pushcfunction(L, LuaBinder< QMenuBar >::__LuaWrapCall__addAction);
    lua_setfield(L, -2, "addAction");
    lua_pushcfunction(L, LuaBinder< QMenuBar >::__LuaWrapCall__setActiveAction);
    lua_setfield(L, -2, "setActiveAction");
    lua_pushcfunction(L, LuaBinder< QMenuBar >::__LuaWrapCall__actionGeometry);
    lua_setfield(L, -2, "actionGeometry");
    lua_pushcfunction(L, LuaBinder< QMenuBar >::__LuaWrapCall__activeAction);
    lua_setfield(L, -2, "activeAction");
    lua_newtable(L);
    lua_pushboolean(L, 1);
    lua_setfield(L, -2, "QWidget*");
    lua_pushboolean(L, 0);
    lua_setfield(L, -2, "QObject*");
    lua_pushboolean(L, 0);
    lua_setfield(L, -2, "QPaintDevice*");
    lua_setfield(L, -2, "__base");
    lua_pushcfunction(L, lqtL_newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, lqtL_index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, lqtL_gc);
    lua_setfield(L, -2, "__gc");
    lua_pushstring(L, "QMenuBar");
    lua_setfield(L, -2, "__qtype");
		lua_setglobal(L, "QMenuBar");
  } else {
		lua_pop(L, 1);
	}
  return 0;
}
