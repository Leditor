#include "lqt_bind_QPaintDevice.hpp"

int LuaBinder< QPaintDevice >::__LuaWrapCall__delete (lua_State *L) {
  QPaintDevice *& __lua__obj = *static_cast<QPaintDevice**>(lqtL_checkudata(L, 1, "QPaintDevice*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  delete __lua__obj;
  __lua__obj = 0;
  return 0;
}
int LuaBinder< QPaintDevice >::__LuaWrapCall__logicalDpiX (lua_State *L) {
  QPaintDevice *& __lua__obj = *static_cast<QPaintDevice**>(lqtL_checkudata(L, 1, "QPaintDevice*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QPaintDevice::logicalDpiX();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QPaintDevice >::__LuaWrapCall__width (lua_State *L) {
  QPaintDevice *& __lua__obj = *static_cast<QPaintDevice**>(lqtL_checkudata(L, 1, "QPaintDevice*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QPaintDevice::width();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QPaintDevice >::__LuaWrapCall__widthMM (lua_State *L) {
  QPaintDevice *& __lua__obj = *static_cast<QPaintDevice**>(lqtL_checkudata(L, 1, "QPaintDevice*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QPaintDevice::widthMM();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QPaintDevice >::__LuaWrapCall__height (lua_State *L) {
  QPaintDevice *& __lua__obj = *static_cast<QPaintDevice**>(lqtL_checkudata(L, 1, "QPaintDevice*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QPaintDevice::height();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QPaintDevice >::__LuaWrapCall__numColors (lua_State *L) {
  QPaintDevice *& __lua__obj = *static_cast<QPaintDevice**>(lqtL_checkudata(L, 1, "QPaintDevice*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QPaintDevice::numColors();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QPaintDevice >::__LuaWrapCall__physicalDpiX (lua_State *L) {
  QPaintDevice *& __lua__obj = *static_cast<QPaintDevice**>(lqtL_checkudata(L, 1, "QPaintDevice*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QPaintDevice::physicalDpiX();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QPaintDevice >::__LuaWrapCall__physicalDpiY (lua_State *L) {
  QPaintDevice *& __lua__obj = *static_cast<QPaintDevice**>(lqtL_checkudata(L, 1, "QPaintDevice*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QPaintDevice::physicalDpiY();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QPaintDevice >::__LuaWrapCall__devType (lua_State *L) {
  QPaintDevice *& __lua__obj = *static_cast<QPaintDevice**>(lqtL_checkudata(L, 1, "QPaintDevice*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QPaintDevice::devType();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QPaintDevice >::__LuaWrapCall__logicalDpiY (lua_State *L) {
  QPaintDevice *& __lua__obj = *static_cast<QPaintDevice**>(lqtL_checkudata(L, 1, "QPaintDevice*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QPaintDevice::logicalDpiY();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QPaintDevice >::__LuaWrapCall__paintingActive (lua_State *L) {
  QPaintDevice *& __lua__obj = *static_cast<QPaintDevice**>(lqtL_checkudata(L, 1, "QPaintDevice*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QPaintDevice::paintingActive();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QPaintDevice >::__LuaWrapCall__depth (lua_State *L) {
  QPaintDevice *& __lua__obj = *static_cast<QPaintDevice**>(lqtL_checkudata(L, 1, "QPaintDevice*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QPaintDevice::depth();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QPaintDevice >::__LuaWrapCall__heightMM (lua_State *L) {
  QPaintDevice *& __lua__obj = *static_cast<QPaintDevice**>(lqtL_checkudata(L, 1, "QPaintDevice*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QPaintDevice::heightMM();
  lua_pushinteger(L, ret);
  return 1;
}
QPaintEngine * LuaBinder< QPaintDevice >::paintEngine () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QPaintDevice*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "paintEngine");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
      }
  QPaintEngine * ret = *static_cast<QPaintEngine**>(lqtL_checkudata(L, -1, "QPaintEngine*"));
  lua_settop(L, oldtop);
  return ret;
}
int LuaBinder< QPaintDevice >::metric (QPaintDevice::PaintDeviceMetric arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QPaintDevice*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "metric");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushenum(L, arg1, "QPaintDevice::PaintDeviceMetric");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QPaintDevice::metric(arg1);
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
int LuaBinder< QPaintDevice >::devType () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QPaintDevice*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "devType");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QPaintDevice::devType();
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
LuaBinder< QPaintDevice >::  ~LuaBinder< QPaintDevice > () {
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QPaintDevice*");
  lua_getfield(L, -1, "~QPaintDevice");

  if (lua_isfunction(L, -1)) {
    lua_insert(L, -2);
    lua_pcall(L, 1, 1, 0);
  } else {
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QPaintDevice >::lqt_pushenum_PaintDeviceMetric (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "PdmWidth");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "PdmWidth");
  lua_pushstring(L, "PdmHeight");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "PdmHeight");
  lua_pushstring(L, "PdmWidthMM");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "PdmWidthMM");
  lua_pushstring(L, "PdmHeightMM");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "PdmHeightMM");
  lua_pushstring(L, "PdmNumColors");
  lua_rawseti(L, enum_table, 5);
  lua_pushinteger(L, 5);
  lua_setfield(L, enum_table, "PdmNumColors");
  lua_pushstring(L, "PdmDepth");
  lua_rawseti(L, enum_table, 6);
  lua_pushinteger(L, 6);
  lua_setfield(L, enum_table, "PdmDepth");
  lua_pushstring(L, "PdmDpiX");
  lua_rawseti(L, enum_table, 7);
  lua_pushinteger(L, 7);
  lua_setfield(L, enum_table, "PdmDpiX");
  lua_pushstring(L, "PdmDpiY");
  lua_rawseti(L, enum_table, 8);
  lua_pushinteger(L, 8);
  lua_setfield(L, enum_table, "PdmDpiY");
  lua_pushstring(L, "PdmPhysicalDpiX");
  lua_rawseti(L, enum_table, 9);
  lua_pushinteger(L, 9);
  lua_setfield(L, enum_table, "PdmPhysicalDpiX");
  lua_pushstring(L, "PdmPhysicalDpiY");
  lua_rawseti(L, enum_table, 10);
  lua_pushinteger(L, 10);
  lua_setfield(L, enum_table, "PdmPhysicalDpiY");
  lua_pushcfunction(L, LuaBinder< QPaintDevice >::lqt_pushenum_PaintDeviceMetric_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QPaintDevice::PaintDeviceMetric");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QPaintDevice >::lqt_pushenum_PaintDeviceMetric_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QPaintDevice::PaintDeviceMetric>*) + sizeof(QFlags<QPaintDevice::PaintDeviceMetric>));
  QFlags<QPaintDevice::PaintDeviceMetric> *fl = static_cast<QFlags<QPaintDevice::PaintDeviceMetric>*>( static_cast<void*>(&static_cast<QFlags<QPaintDevice::PaintDeviceMetric>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QPaintDevice::PaintDeviceMetric>(lqtL_toenum(L, i, "QPaintDevice::PaintDeviceMetric"));
	}
	if (luaL_newmetatable(L, "QFlags<QPaintDevice::PaintDeviceMetric>*")) {
		lua_pushstring(L, "QFlags<QPaintDevice::PaintDeviceMetric>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int luaopen_QPaintDevice (lua_State *L) {
  if (luaL_newmetatable(L, "QPaintDevice*")) {
    lua_pushcfunction(L, LuaBinder< QPaintDevice >::__LuaWrapCall__delete);
    lua_setfield(L, -2, "delete");
    lua_pushcfunction(L, LuaBinder< QPaintDevice >::__LuaWrapCall__logicalDpiX);
    lua_setfield(L, -2, "logicalDpiX");
    lua_pushcfunction(L, LuaBinder< QPaintDevice >::__LuaWrapCall__width);
    lua_setfield(L, -2, "width");
    lua_pushcfunction(L, LuaBinder< QPaintDevice >::__LuaWrapCall__widthMM);
    lua_setfield(L, -2, "widthMM");
    lua_pushcfunction(L, LuaBinder< QPaintDevice >::__LuaWrapCall__height);
    lua_setfield(L, -2, "height");
    lua_pushcfunction(L, LuaBinder< QPaintDevice >::__LuaWrapCall__numColors);
    lua_setfield(L, -2, "numColors");
    lua_pushcfunction(L, LuaBinder< QPaintDevice >::__LuaWrapCall__physicalDpiX);
    lua_setfield(L, -2, "physicalDpiX");
    lua_pushcfunction(L, LuaBinder< QPaintDevice >::__LuaWrapCall__physicalDpiY);
    lua_setfield(L, -2, "physicalDpiY");
    lua_pushcfunction(L, LuaBinder< QPaintDevice >::__LuaWrapCall__devType);
    lua_setfield(L, -2, "devType");
    lua_pushcfunction(L, LuaBinder< QPaintDevice >::__LuaWrapCall__logicalDpiY);
    lua_setfield(L, -2, "logicalDpiY");
    lua_pushcfunction(L, LuaBinder< QPaintDevice >::__LuaWrapCall__paintingActive);
    lua_setfield(L, -2, "paintingActive");
    lua_pushcfunction(L, LuaBinder< QPaintDevice >::__LuaWrapCall__depth);
    lua_setfield(L, -2, "depth");
    lua_pushcfunction(L, LuaBinder< QPaintDevice >::__LuaWrapCall__heightMM);
    lua_setfield(L, -2, "heightMM");
    LuaBinder< QPaintDevice >::lqt_pushenum_PaintDeviceMetric(L);
    lua_setfield(L, -2, "PaintDeviceMetric");
    lua_newtable(L);
    lua_setfield(L, -2, "__base");
    lua_pushcfunction(L, lqtL_newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, lqtL_index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, lqtL_gc);
    lua_setfield(L, -2, "__gc");
    lua_pushstring(L, "QPaintDevice");
    lua_setfield(L, -2, "__qtype");
		lua_setglobal(L, "QPaintDevice");
  } else {
		lua_pop(L, 1);
	}
  return 0;
}
