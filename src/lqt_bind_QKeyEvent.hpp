#include "lqt_common.hpp"
#include <QKeyEvent>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QKeyEvent > : public QKeyEvent {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__delete (lua_State *L);
  static int __LuaWrapCall__createExtendedKeyEvent (lua_State *L);
  static int __LuaWrapCall__text (lua_State *L);
  static int __LuaWrapCall__modifiers (lua_State *L);
  static int __LuaWrapCall__hasExtendedInfo (lua_State *L);
  static int __LuaWrapCall__nativeVirtualKey (lua_State *L);
  static int __LuaWrapCall__count (lua_State *L);
  static int __LuaWrapCall__isAutoRepeat (lua_State *L);
  static int __LuaWrapCall__nativeModifiers (lua_State *L);
  static int __LuaWrapCall__matches (lua_State *L);
  static int __LuaWrapCall__key (lua_State *L);
  static int __LuaWrapCall__nativeScanCode (lua_State *L);
  ~LuaBinder< QKeyEvent > ();
  LuaBinder< QKeyEvent > (lua_State *l, QEvent::Type arg1, int arg2, QFlags<Qt::KeyboardModifier> arg3, const QString& arg4, bool arg5, short unsigned int arg6):QKeyEvent(arg1, arg2, arg3, arg4, arg5, arg6), L(l) {}
};

extern "C" int luaopen_QKeyEvent (lua_State *L);
