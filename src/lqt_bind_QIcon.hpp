#include "lqt_common.hpp"
#include <QIcon>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QIcon > : public QIcon {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__paint__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__paint__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__paint (lua_State *L);
  static int __LuaWrapCall__serialNumber (lua_State *L);
  static int __LuaWrapCall__addFile (lua_State *L);
  static int __LuaWrapCall__detach (lua_State *L);
  static int __LuaWrapCall__cacheKey (lua_State *L);
  static int __LuaWrapCall__actualSize (lua_State *L);
  static int __LuaWrapCall__delete (lua_State *L);
  static int __LuaWrapCall__isNull (lua_State *L);
  static int __LuaWrapCall__addPixmap (lua_State *L);
  static int __LuaWrapCall__isDetached (lua_State *L);
  static int __LuaWrapCall__pixmap__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__pixmap__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__pixmap__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__pixmap (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__4 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__5 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__6 (lua_State *L);
  static int __LuaWrapCall__new (lua_State *L);
  static int lqt_pushenum_Mode (lua_State *L);
  static int lqt_pushenum_Mode_QFLAGS_CREATOR (lua_State *L);
  static int lqt_pushenum_State (lua_State *L);
  static int lqt_pushenum_State_QFLAGS_CREATOR (lua_State *L);
  LuaBinder< QIcon > (lua_State *l):QIcon(), L(l) {}
  LuaBinder< QIcon > (lua_State *l, const QPixmap& arg1):QIcon(arg1), L(l) {}
  LuaBinder< QIcon > (lua_State *l, const QIcon& arg1):QIcon(arg1), L(l) {}
  LuaBinder< QIcon > (lua_State *l, const QString& arg1):QIcon(arg1), L(l) {}
  LuaBinder< QIcon > (lua_State *l, QIconEngine * arg1):QIcon(arg1), L(l) {}
  LuaBinder< QIcon > (lua_State *l, QIconEngineV2 * arg1):QIcon(arg1), L(l) {}
};

extern "C" int luaopen_QIcon (lua_State *L);
