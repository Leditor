#include "lqt_bind_QGraphicsView.hpp"

int LuaBinder< QGraphicsView >::__LuaWrapCall__render (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QPainter * arg1 = *static_cast<QPainter**>(lqtL_checkudata(L, 2, "QPainter*"));
  const QRectF& arg2 = lqtL_testudata(L, 3, "QRectF*")?**static_cast<QRectF**>(lqtL_checkudata(L, 3, "QRectF*")):QRectF();
  const QRect& arg3 = lqtL_testudata(L, 4, "QRect*")?**static_cast<QRect**>(lqtL_checkudata(L, 4, "QRect*")):QRect();
  Qt::AspectRatioMode arg4 = lqtL_isenum(L, 5, "Qt::AspectRatioMode")?static_cast<Qt::AspectRatioMode>(lqtL_toenum(L, 5, "Qt::AspectRatioMode")):Qt::KeepAspectRatio;
  __lua__obj->QGraphicsView::render(arg1, arg2, arg3, arg4);
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__setOptimizationFlag (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QGraphicsView::OptimizationFlag arg1 = static_cast<QGraphicsView::OptimizationFlag>(lqtL_toenum(L, 2, "QGraphicsView::OptimizationFlag"));
  bool arg2 = lua_isboolean(L, 3)?(bool)lua_toboolean(L, 3):true;
  __lua__obj->QGraphicsView::setOptimizationFlag(arg1, arg2);
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__sizeHint (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QSize ret = __lua__obj->QGraphicsView::sizeHint();
  lqtL_passudata(L, new QSize(ret), "QSize*");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__setMatrix (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QMatrix& arg1 = **static_cast<QMatrix**>(lqtL_checkudata(L, 2, "QMatrix*"));
  bool arg2 = lua_isboolean(L, 3)?(bool)lua_toboolean(L, 3):false;
  __lua__obj->QGraphicsView::setMatrix(arg1, arg2);
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__setInteractive (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QGraphicsView::setInteractive(arg1);
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__sceneRect (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QRectF ret = __lua__obj->QGraphicsView::sceneRect();
  lqtL_passudata(L, new QRectF(ret), "QRectF*");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__viewportTransform (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTransform ret = __lua__obj->QGraphicsView::viewportTransform();
  lqtL_passudata(L, new QTransform(ret), "QTransform*");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__setOptimizationFlags (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<QGraphicsView::OptimizationFlag> arg1 = **static_cast<QFlags<QGraphicsView::OptimizationFlag>**>(lqtL_checkudata(L, 2, "QFlags<QGraphicsView::OptimizationFlag>*"));
  __lua__obj->QGraphicsView::setOptimizationFlags(arg1);
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__setTransformationAnchor (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QGraphicsView::ViewportAnchor arg1 = static_cast<QGraphicsView::ViewportAnchor>(lqtL_toenum(L, 2, "QGraphicsView::ViewportAnchor"));
  __lua__obj->QGraphicsView::setTransformationAnchor(arg1);
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QGraphicsView::trUtf8(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QGraphicsView::trUtf8(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__trUtf8 (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x11608a0;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x11603f0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1162690;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1161db0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1162b10;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__trUtf8__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__trUtf8__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__trUtf8 matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__optimizationFlags (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<QGraphicsView::OptimizationFlag> ret = __lua__obj->QGraphicsView::optimizationFlags();
  lqtL_passudata(L, new QFlags<QGraphicsView::OptimizationFlag>(ret), "QFlags<QGraphicsView::OptimizationFlag>*");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__rubberBandSelectionMode (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::ItemSelectionMode ret = __lua__obj->QGraphicsView::rubberBandSelectionMode();
  lqtL_pushenum(L, ret, "Qt::ItemSelectionMode");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QGraphicsView::tr(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QGraphicsView::tr(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__tr (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x115fb30;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x115f890;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1161570;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1161a60;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1161e10;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__tr__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__tr__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__tr matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__renderHints (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<QPainter::RenderHint> ret = __lua__obj->QGraphicsView::renderHints();
  lqtL_passudata(L, new QFlags<QPainter::RenderHint>(ret), "QFlags<QPainter::RenderHint>*");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__setCacheMode (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<QGraphicsView::CacheModeFlag> arg1 = **static_cast<QFlags<QGraphicsView::CacheModeFlag>**>(lqtL_checkudata(L, 2, "QFlags<QGraphicsView::CacheModeFlag>*"));
  __lua__obj->QGraphicsView::setCacheMode(arg1);
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__delete (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  delete __lua__obj;
  __lua__obj = 0;
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__new__OverloadedVersion__2 (lua_State *L) {
  QWidget * arg1 = lqtL_testudata(L, 1, "QWidget*")?*static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*")):static_cast< QWidget * >(0);
  QGraphicsView * ret = new LuaBinder< QGraphicsView >(L, arg1);
  lqtL_passudata(L, ret, "QGraphicsView*");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__new__OverloadedVersion__3 (lua_State *L) {
  QGraphicsScene * arg1 = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
  QWidget * arg2 = lqtL_testudata(L, 2, "QWidget*")?*static_cast<QWidget**>(lqtL_checkudata(L, 2, "QWidget*")):static_cast< QWidget * >(0);
  QGraphicsView * ret = new LuaBinder< QGraphicsView >(L, arg1, arg2);
  lqtL_passudata(L, ret, "QGraphicsView*");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__new (lua_State *L) {
  int score[4];
  const int premium = 11+lua_gettop(L);
  score[2] = 0;
  if (lqtL_testudata(L, 1, "QWidget*")) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x1164780;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  if (lqtL_testudata(L, 1, "QGraphicsScene*")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x11651f0;
  } else {
    score[3] -= premium*premium;
  }
  if (lqtL_testudata(L, 2, "QWidget*")) {
    score[3] += premium;
  } else if (true) {
    score[3] += premium-1; // table: 0x1164c90;
  } else {
    score[3] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=3;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 2: return __LuaWrapCall__new__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__new__OverloadedVersion__3(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__new matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__setRenderHints (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<QPainter::RenderHint> arg1 = **static_cast<QFlags<QPainter::RenderHint>**>(lqtL_checkudata(L, 2, "QFlags<QPainter::RenderHint>*"));
  __lua__obj->QGraphicsView::setRenderHints(arg1);
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__updateScene (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QList<QRectF>& arg1 = **static_cast<QList<QRectF>**>(lqtL_checkudata(L, 2, "QList<QRectF>*"));
  __lua__obj->QGraphicsView::updateScene(arg1);
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__setRenderHint (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QPainter::RenderHint arg1 = static_cast<QPainter::RenderHint>(lqtL_toenum(L, 2, "QPainter::RenderHint"));
  bool arg2 = lua_isboolean(L, 3)?(bool)lua_toboolean(L, 3):true;
  __lua__obj->QGraphicsView::setRenderHint(arg1, arg2);
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__items__OverloadedVersion__1 (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QList<QGraphicsItem*> ret = __lua__obj->QGraphicsView::items();
  lqtL_passudata(L, new QList<QGraphicsItem*>(ret), "QList<QGraphicsItem*>*");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__items__OverloadedVersion__2 (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPoint& arg1 = **static_cast<QPoint**>(lqtL_checkudata(L, 2, "QPoint*"));
  QList<QGraphicsItem*> ret = __lua__obj->QGraphicsView::items(arg1);
  lqtL_passudata(L, new QList<QGraphicsItem*>(ret), "QList<QGraphicsItem*>*");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__items__OverloadedVersion__3 (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int arg2 = lua_tointeger(L, 3);
  QList<QGraphicsItem*> ret = __lua__obj->QGraphicsView::items(arg1, arg2);
  lqtL_passudata(L, new QList<QGraphicsItem*>(ret), "QList<QGraphicsItem*>*");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__items__OverloadedVersion__4 (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QRect& arg1 = **static_cast<QRect**>(lqtL_checkudata(L, 2, "QRect*"));
  Qt::ItemSelectionMode arg2 = lqtL_isenum(L, 3, "Qt::ItemSelectionMode")?static_cast<Qt::ItemSelectionMode>(lqtL_toenum(L, 3, "Qt::ItemSelectionMode")):Qt::IntersectsItemShape;
  QList<QGraphicsItem*> ret = __lua__obj->QGraphicsView::items(arg1, arg2);
  lqtL_passudata(L, new QList<QGraphicsItem*>(ret), "QList<QGraphicsItem*>*");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__items__OverloadedVersion__5 (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int arg2 = lua_tointeger(L, 3);
  int arg3 = lua_tointeger(L, 4);
  int arg4 = lua_tointeger(L, 5);
  Qt::ItemSelectionMode arg5 = lqtL_isenum(L, 6, "Qt::ItemSelectionMode")?static_cast<Qt::ItemSelectionMode>(lqtL_toenum(L, 6, "Qt::ItemSelectionMode")):Qt::IntersectsItemShape;
  QList<QGraphicsItem*> ret = __lua__obj->QGraphicsView::items(arg1, arg2, arg3, arg4, arg5);
  lqtL_passudata(L, new QList<QGraphicsItem*>(ret), "QList<QGraphicsItem*>*");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__items__OverloadedVersion__6 (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPolygon& arg1 = **static_cast<QPolygon**>(lqtL_checkudata(L, 2, "QPolygon*"));
  Qt::ItemSelectionMode arg2 = lqtL_isenum(L, 3, "Qt::ItemSelectionMode")?static_cast<Qt::ItemSelectionMode>(lqtL_toenum(L, 3, "Qt::ItemSelectionMode")):Qt::IntersectsItemShape;
  QList<QGraphicsItem*> ret = __lua__obj->QGraphicsView::items(arg1, arg2);
  lqtL_passudata(L, new QList<QGraphicsItem*>(ret), "QList<QGraphicsItem*>*");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__items__OverloadedVersion__7 (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPainterPath& arg1 = **static_cast<QPainterPath**>(lqtL_checkudata(L, 2, "QPainterPath*"));
  Qt::ItemSelectionMode arg2 = lqtL_isenum(L, 3, "Qt::ItemSelectionMode")?static_cast<Qt::ItemSelectionMode>(lqtL_toenum(L, 3, "Qt::ItemSelectionMode")):Qt::IntersectsItemShape;
  QList<QGraphicsItem*> ret = __lua__obj->QGraphicsView::items(arg1, arg2);
  lqtL_passudata(L, new QList<QGraphicsItem*>(ret), "QList<QGraphicsItem*>*");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__items (lua_State *L) {
  int score[8];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QGraphicsView*")?premium:-premium*premium;
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QGraphicsView*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPoint*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x118c240;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  score[3] += lqtL_testudata(L, 1, "QGraphicsView*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x118cc70;
  } else {
    score[3] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x118d180;
  } else {
    score[3] -= premium*premium;
  }
  score[4] = 0;
  score[4] += lqtL_testudata(L, 1, "QGraphicsView*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QRect*")) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x118dad0;
  } else {
    score[4] -= premium*premium;
  }
  if (lqtL_isenum(L, 3, "Qt::ItemSelectionMode")) {
    score[4] += premium;
  } else if (true) {
    score[4] += premium-1; // table: 0x118d530;
  } else {
    score[4] -= premium*premium;
  }
  score[5] = 0;
  score[5] += lqtL_testudata(L, 1, "QGraphicsView*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[5] += premium;
  } else if (false) {
    score[5] += premium-1; // table: 0x118e980;
  } else {
    score[5] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[5] += premium;
  } else if (false) {
    score[5] += premium-1; // table: 0x118eea0;
  } else {
    score[5] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[5] += premium;
  } else if (false) {
    score[5] += premium-1; // table: 0x118f250;
  } else {
    score[5] -= premium*premium;
  }
  if (lua_isnumber(L, 5)) {
    score[5] += premium;
  } else if (false) {
    score[5] += premium-1; // table: 0x118f1f0;
  } else {
    score[5] -= premium*premium;
  }
  if (lqtL_isenum(L, 6, "Qt::ItemSelectionMode")) {
    score[5] += premium;
  } else if (true) {
    score[5] += premium-1; // table: 0x118fa30;
  } else {
    score[5] -= premium*premium;
  }
  score[6] = 0;
  score[6] += lqtL_testudata(L, 1, "QGraphicsView*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPolygon*")) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x1190390;
  } else {
    score[6] -= premium*premium;
  }
  if (lqtL_isenum(L, 3, "Qt::ItemSelectionMode")) {
    score[6] += premium;
  } else if (true) {
    score[6] += premium-1; // table: 0x118f620;
  } else {
    score[6] -= premium*premium;
  }
  score[7] = 0;
  score[7] += lqtL_testudata(L, 1, "QGraphicsView*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPainterPath*")) {
    score[7] += premium;
  } else if (false) {
    score[7] += premium-1; // table: 0x1191250;
  } else {
    score[7] -= premium*premium;
  }
  if (lqtL_isenum(L, 3, "Qt::ItemSelectionMode")) {
    score[7] += premium;
  } else if (true) {
    score[7] += premium-1; // table: 0x1190cd0;
  } else {
    score[7] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=7;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__items__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__items__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__items__OverloadedVersion__3(L); break;
    case 4: return __LuaWrapCall__items__OverloadedVersion__4(L); break;
    case 5: return __LuaWrapCall__items__OverloadedVersion__5(L); break;
    case 6: return __LuaWrapCall__items__OverloadedVersion__6(L); break;
    case 7: return __LuaWrapCall__items__OverloadedVersion__7(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__items matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__invalidateScene (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QRectF& arg1 = lqtL_testudata(L, 2, "QRectF*")?**static_cast<QRectF**>(lqtL_checkudata(L, 2, "QRectF*")):QRectF();
  QFlags<QGraphicsScene::SceneLayer> arg2 = lqtL_testudata(L, 3, "QFlags<QGraphicsScene::SceneLayer>*")?**static_cast<QFlags<QGraphicsScene::SceneLayer>**>(lqtL_checkudata(L, 3, "QFlags<QGraphicsScene::SceneLayer>*")):QGraphicsScene::AllLayers;
  __lua__obj->QGraphicsView::invalidateScene(arg1, arg2);
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__matrix (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QMatrix ret = __lua__obj->QGraphicsView::matrix();
  lqtL_passudata(L, new QMatrix(ret), "QMatrix*");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__transformationAnchor (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QGraphicsView::ViewportAnchor ret = __lua__obj->QGraphicsView::transformationAnchor();
  lqtL_pushenum(L, ret, "QGraphicsView::ViewportAnchor");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__centerOn__OverloadedVersion__1 (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPointF& arg1 = **static_cast<QPointF**>(lqtL_checkudata(L, 2, "QPointF*"));
  __lua__obj->QGraphicsView::centerOn(arg1);
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__centerOn__OverloadedVersion__2 (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  double arg2 = lua_tonumber(L, 3);
  __lua__obj->QGraphicsView::centerOn(arg1, arg2);
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__centerOn__OverloadedVersion__3 (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QGraphicsItem * arg1 = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 2, "QGraphicsItem*"));
  __lua__obj->QGraphicsView::centerOn(arg1);
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__centerOn (lua_State *L) {
  int score[4];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QGraphicsView*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPointF*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1180590;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QGraphicsView*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1181040;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1180af0;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  score[3] += lqtL_testudata(L, 1, "QGraphicsView*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QGraphicsItem*")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1181e90;
  } else {
    score[3] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=3;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__centerOn__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__centerOn__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__centerOn__OverloadedVersion__3(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__centerOn matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__rotate (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  __lua__obj->QGraphicsView::rotate(arg1);
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__setScene (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QGraphicsScene * arg1 = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 2, "QGraphicsScene*"));
  __lua__obj->QGraphicsView::setScene(arg1);
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__transform (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTransform ret = __lua__obj->QGraphicsView::transform();
  lqtL_passudata(L, new QTransform(ret), "QTransform*");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__resetMatrix (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QGraphicsView::resetMatrix();
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__setRubberBandSelectionMode (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::ItemSelectionMode arg1 = static_cast<Qt::ItemSelectionMode>(lqtL_toenum(L, 2, "Qt::ItemSelectionMode"));
  __lua__obj->QGraphicsView::setRubberBandSelectionMode(arg1);
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__setTransform (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QTransform& arg1 = **static_cast<QTransform**>(lqtL_checkudata(L, 2, "QTransform*"));
  bool arg2 = lua_isboolean(L, 3)?(bool)lua_toboolean(L, 3):false;
  __lua__obj->QGraphicsView::setTransform(arg1, arg2);
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__setSceneRect__OverloadedVersion__1 (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QRectF& arg1 = **static_cast<QRectF**>(lqtL_checkudata(L, 2, "QRectF*"));
  __lua__obj->QGraphicsView::setSceneRect(arg1);
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__setSceneRect__OverloadedVersion__2 (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  double arg2 = lua_tonumber(L, 3);
  double arg3 = lua_tonumber(L, 4);
  double arg4 = lua_tonumber(L, 5);
  __lua__obj->QGraphicsView::setSceneRect(arg1, arg2, arg3, arg4);
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__setSceneRect (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QGraphicsView*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QRectF*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1176810;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QGraphicsView*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1177290;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1176d20;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x11777a0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 5)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x11780c0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__setSceneRect__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__setSceneRect__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__setSceneRect matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__scale (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  double arg2 = lua_tonumber(L, 3);
  __lua__obj->QGraphicsView::scale(arg1, arg2);
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__resizeAnchor (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QGraphicsView::ViewportAnchor ret = __lua__obj->QGraphicsView::resizeAnchor();
  lqtL_pushenum(L, ret, "QGraphicsView::ViewportAnchor");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__dragMode (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QGraphicsView::DragMode ret = __lua__obj->QGraphicsView::dragMode();
  lqtL_pushenum(L, ret, "QGraphicsView::DragMode");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__shear (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  double arg2 = lua_tonumber(L, 3);
  __lua__obj->QGraphicsView::shear(arg1, arg2);
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__fitInView__OverloadedVersion__1 (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QRectF& arg1 = **static_cast<QRectF**>(lqtL_checkudata(L, 2, "QRectF*"));
  Qt::AspectRatioMode arg2 = lqtL_isenum(L, 3, "Qt::AspectRatioMode")?static_cast<Qt::AspectRatioMode>(lqtL_toenum(L, 3, "Qt::AspectRatioMode")):Qt::IgnoreAspectRatio;
  __lua__obj->QGraphicsView::fitInView(arg1, arg2);
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__fitInView__OverloadedVersion__2 (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  double arg2 = lua_tonumber(L, 3);
  double arg3 = lua_tonumber(L, 4);
  double arg4 = lua_tonumber(L, 5);
  Qt::AspectRatioMode arg5 = lqtL_isenum(L, 6, "Qt::AspectRatioMode")?static_cast<Qt::AspectRatioMode>(lqtL_toenum(L, 6, "Qt::AspectRatioMode")):Qt::IgnoreAspectRatio;
  __lua__obj->QGraphicsView::fitInView(arg1, arg2, arg3, arg4, arg5);
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__fitInView__OverloadedVersion__3 (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QGraphicsItem * arg1 = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 2, "QGraphicsItem*"));
  Qt::AspectRatioMode arg2 = lqtL_isenum(L, 3, "Qt::AspectRatioMode")?static_cast<Qt::AspectRatioMode>(lqtL_toenum(L, 3, "Qt::AspectRatioMode")):Qt::IgnoreAspectRatio;
  __lua__obj->QGraphicsView::fitInView(arg1, arg2);
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__fitInView (lua_State *L) {
  int score[4];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QGraphicsView*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QRectF*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1186c60;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_isenum(L, 3, "Qt::AspectRatioMode")) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x1186200;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QGraphicsView*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1187c00;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x11878d0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x11884d0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 5)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1188470;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_isenum(L, 6, "Qt::AspectRatioMode")) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x1188ca0;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  score[3] += lqtL_testudata(L, 1, "QGraphicsView*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QGraphicsItem*")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x11895d0;
  } else {
    score[3] -= premium*premium;
  }
  if (lqtL_isenum(L, 3, "Qt::AspectRatioMode")) {
    score[3] += premium;
  } else if (true) {
    score[3] += premium-1; // table: 0x11888a0;
  } else {
    score[3] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=3;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__fitInView__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__fitInView__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__fitInView__OverloadedVersion__3(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__fitInView matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__isInteractive (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QGraphicsView::isInteractive();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__setResizeAnchor (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QGraphicsView::ViewportAnchor arg1 = static_cast<QGraphicsView::ViewportAnchor>(lqtL_toenum(L, 2, "QGraphicsView::ViewportAnchor"));
  __lua__obj->QGraphicsView::setResizeAnchor(arg1);
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__translate (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  double arg2 = lua_tonumber(L, 3);
  __lua__obj->QGraphicsView::translate(arg1, arg2);
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__ensureVisible__OverloadedVersion__1 (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QRectF& arg1 = **static_cast<QRectF**>(lqtL_checkudata(L, 2, "QRectF*"));
  int arg2 = lua_isnumber(L, 3)?lua_tointeger(L, 3):static_cast< int >(50);
  int arg3 = lua_isnumber(L, 4)?lua_tointeger(L, 4):static_cast< int >(50);
  __lua__obj->QGraphicsView::ensureVisible(arg1, arg2, arg3);
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__ensureVisible__OverloadedVersion__2 (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  double arg2 = lua_tonumber(L, 3);
  double arg3 = lua_tonumber(L, 4);
  double arg4 = lua_tonumber(L, 5);
  int arg5 = lua_isnumber(L, 6)?lua_tointeger(L, 6):static_cast< int >(50);
  int arg6 = lua_isnumber(L, 7)?lua_tointeger(L, 7):static_cast< int >(50);
  __lua__obj->QGraphicsView::ensureVisible(arg1, arg2, arg3, arg4, arg5, arg6);
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__ensureVisible__OverloadedVersion__3 (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QGraphicsItem * arg1 = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 2, "QGraphicsItem*"));
  int arg2 = lua_isnumber(L, 3)?lua_tointeger(L, 3):static_cast< int >(50);
  int arg3 = lua_isnumber(L, 4)?lua_tointeger(L, 4):static_cast< int >(50);
  __lua__obj->QGraphicsView::ensureVisible(arg1, arg2, arg3);
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__ensureVisible (lua_State *L) {
  int score[4];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QGraphicsView*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QRectF*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x11828e0;
  } else {
    score[1] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x11823a0;
  } else {
    score[1] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x1182d90;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QGraphicsView*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1183b60;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1183160;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1184010;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 5)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1184870;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 6)) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x1184c30;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 7)) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x11847c0;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  score[3] += lqtL_testudata(L, 1, "QGraphicsView*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QGraphicsItem*")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1185980;
  } else {
    score[3] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[3] += premium;
  } else if (true) {
    score[3] += premium-1; // table: 0x1185650;
  } else {
    score[3] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[3] += premium;
  } else if (true) {
    score[3] += premium-1; // table: 0x1185e30;
  } else {
    score[3] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=3;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__ensureVisible__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__ensureVisible__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__ensureVisible__OverloadedVersion__3(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__ensureVisible matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__updateSceneRect (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QRectF& arg1 = **static_cast<QRectF**>(lqtL_checkudata(L, 2, "QRectF*"));
  __lua__obj->QGraphicsView::updateSceneRect(arg1);
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__backgroundBrush (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QBrush ret = __lua__obj->QGraphicsView::backgroundBrush();
  lqtL_passudata(L, new QBrush(ret), "QBrush*");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__cacheMode (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<QGraphicsView::CacheModeFlag> ret = __lua__obj->QGraphicsView::cacheMode();
  lqtL_passudata(L, new QFlags<QGraphicsView::CacheModeFlag>(ret), "QFlags<QGraphicsView::CacheModeFlag>*");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__inputMethodQuery (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::InputMethodQuery arg1 = static_cast<Qt::InputMethodQuery>(lqtL_toenum(L, 2, "Qt::InputMethodQuery"));
  QVariant ret = __lua__obj->QGraphicsView::inputMethodQuery(arg1);
  lqtL_passudata(L, new QVariant(ret), "QVariant*");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__setViewportUpdateMode (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QGraphicsView::ViewportUpdateMode arg1 = static_cast<QGraphicsView::ViewportUpdateMode>(lqtL_toenum(L, 2, "QGraphicsView::ViewportUpdateMode"));
  __lua__obj->QGraphicsView::setViewportUpdateMode(arg1);
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__mapToScene__OverloadedVersion__1 (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPoint& arg1 = **static_cast<QPoint**>(lqtL_checkudata(L, 2, "QPoint*"));
  QPointF ret = __lua__obj->QGraphicsView::mapToScene(arg1);
  lqtL_passudata(L, new QPointF(ret), "QPointF*");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__mapToScene__OverloadedVersion__2 (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QRect& arg1 = **static_cast<QRect**>(lqtL_checkudata(L, 2, "QRect*"));
  QPolygonF ret = __lua__obj->QGraphicsView::mapToScene(arg1);
  lqtL_passudata(L, new QPolygonF(ret), "QPolygonF*");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__mapToScene__OverloadedVersion__3 (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPolygon& arg1 = **static_cast<QPolygon**>(lqtL_checkudata(L, 2, "QPolygon*"));
  QPolygonF ret = __lua__obj->QGraphicsView::mapToScene(arg1);
  lqtL_passudata(L, new QPolygonF(ret), "QPolygonF*");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__mapToScene__OverloadedVersion__4 (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPainterPath& arg1 = **static_cast<QPainterPath**>(lqtL_checkudata(L, 2, "QPainterPath*"));
  QPainterPath ret = __lua__obj->QGraphicsView::mapToScene(arg1);
  lqtL_passudata(L, new QPainterPath(ret), "QPainterPath*");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__mapToScene__OverloadedVersion__5 (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int arg2 = lua_tointeger(L, 3);
  QPointF ret = __lua__obj->QGraphicsView::mapToScene(arg1, arg2);
  lqtL_passudata(L, new QPointF(ret), "QPointF*");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__mapToScene__OverloadedVersion__6 (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int arg2 = lua_tointeger(L, 3);
  int arg3 = lua_tointeger(L, 4);
  int arg4 = lua_tointeger(L, 5);
  QPolygonF ret = __lua__obj->QGraphicsView::mapToScene(arg1, arg2, arg3, arg4);
  lqtL_passudata(L, new QPolygonF(ret), "QPolygonF*");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__mapToScene (lua_State *L) {
  int score[7];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QGraphicsView*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPoint*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x11939e0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QGraphicsView*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QRect*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x11944c0;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  score[3] += lqtL_testudata(L, 1, "QGraphicsView*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPolygon*")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1194f60;
  } else {
    score[3] -= premium*premium;
  }
  score[4] = 0;
  score[4] += lqtL_testudata(L, 1, "QGraphicsView*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPainterPath*")) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x11959e0;
  } else {
    score[4] -= premium*premium;
  }
  score[5] = 0;
  score[5] += lqtL_testudata(L, 1, "QGraphicsView*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[5] += premium;
  } else if (false) {
    score[5] += premium-1; // table: 0x1198e90;
  } else {
    score[5] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[5] += premium;
  } else if (false) {
    score[5] += premium-1; // table: 0x11993c0;
  } else {
    score[5] -= premium*premium;
  }
  score[6] = 0;
  score[6] += lqtL_testudata(L, 1, "QGraphicsView*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x1199cd0;
  } else {
    score[6] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x119a200;
  } else {
    score[6] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x119a5b0;
  } else {
    score[6] -= premium*premium;
  }
  if (lua_isnumber(L, 5)) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x119a550;
  } else {
    score[6] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=6;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__mapToScene__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__mapToScene__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__mapToScene__OverloadedVersion__3(L); break;
    case 4: return __LuaWrapCall__mapToScene__OverloadedVersion__4(L); break;
    case 5: return __LuaWrapCall__mapToScene__OverloadedVersion__5(L); break;
    case 6: return __LuaWrapCall__mapToScene__OverloadedVersion__6(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__mapToScene matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__foregroundBrush (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QBrush ret = __lua__obj->QGraphicsView::foregroundBrush();
  lqtL_passudata(L, new QBrush(ret), "QBrush*");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__resetTransform (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QGraphicsView::resetTransform();
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__scene (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QGraphicsScene * ret = __lua__obj->QGraphicsView::scene();
  lqtL_pushudata(L, ret, "QGraphicsScene*");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__metaObject (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QMetaObject * ret = __lua__obj->QGraphicsView::metaObject();
  lqtL_pushudata(L, ret, "QMetaObject*");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__resetCachedContent (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QGraphicsView::resetCachedContent();
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__setForegroundBrush (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QBrush& arg1 = **static_cast<QBrush**>(lqtL_checkudata(L, 2, "QBrush*"));
  __lua__obj->QGraphicsView::setForegroundBrush(arg1);
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__itemAt__OverloadedVersion__1 (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPoint& arg1 = **static_cast<QPoint**>(lqtL_checkudata(L, 2, "QPoint*"));
  QGraphicsItem * ret = __lua__obj->QGraphicsView::itemAt(arg1);
  lqtL_pushudata(L, ret, "QGraphicsItem*");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__itemAt__OverloadedVersion__2 (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int arg2 = lua_tointeger(L, 3);
  QGraphicsItem * ret = __lua__obj->QGraphicsView::itemAt(arg1, arg2);
  lqtL_pushudata(L, ret, "QGraphicsItem*");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__itemAt (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QGraphicsView*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPoint*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1192110;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QGraphicsView*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1192b40;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1193050;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__itemAt__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__itemAt__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__itemAt matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__alignment (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<Qt::AlignmentFlag> ret = __lua__obj->QGraphicsView::alignment();
  lqtL_passudata(L, new QFlags<Qt::AlignmentFlag>(ret), "QFlags<Qt::AlignmentFlag>*");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__setDragMode (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QGraphicsView::DragMode arg1 = static_cast<QGraphicsView::DragMode>(lqtL_toenum(L, 2, "QGraphicsView::DragMode"));
  __lua__obj->QGraphicsView::setDragMode(arg1);
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__viewportUpdateMode (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QGraphicsView::ViewportUpdateMode ret = __lua__obj->QGraphicsView::viewportUpdateMode();
  lqtL_pushenum(L, ret, "QGraphicsView::ViewportUpdateMode");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__setBackgroundBrush (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QBrush& arg1 = **static_cast<QBrush**>(lqtL_checkudata(L, 2, "QBrush*"));
  __lua__obj->QGraphicsView::setBackgroundBrush(arg1);
  return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__mapFromScene__OverloadedVersion__1 (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPointF& arg1 = **static_cast<QPointF**>(lqtL_checkudata(L, 2, "QPointF*"));
  QPoint ret = __lua__obj->QGraphicsView::mapFromScene(arg1);
  lqtL_passudata(L, new QPoint(ret), "QPoint*");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__mapFromScene__OverloadedVersion__2 (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QRectF& arg1 = **static_cast<QRectF**>(lqtL_checkudata(L, 2, "QRectF*"));
  QPolygon ret = __lua__obj->QGraphicsView::mapFromScene(arg1);
  lqtL_passudata(L, new QPolygon(ret), "QPolygon*");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__mapFromScene__OverloadedVersion__3 (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPolygonF& arg1 = **static_cast<QPolygonF**>(lqtL_checkudata(L, 2, "QPolygonF*"));
  QPolygon ret = __lua__obj->QGraphicsView::mapFromScene(arg1);
  lqtL_passudata(L, new QPolygon(ret), "QPolygon*");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__mapFromScene__OverloadedVersion__4 (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPainterPath& arg1 = **static_cast<QPainterPath**>(lqtL_checkudata(L, 2, "QPainterPath*"));
  QPainterPath ret = __lua__obj->QGraphicsView::mapFromScene(arg1);
  lqtL_passudata(L, new QPainterPath(ret), "QPainterPath*");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__mapFromScene__OverloadedVersion__5 (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  double arg2 = lua_tonumber(L, 3);
  QPoint ret = __lua__obj->QGraphicsView::mapFromScene(arg1, arg2);
  lqtL_passudata(L, new QPoint(ret), "QPoint*");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__mapFromScene__OverloadedVersion__6 (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  double arg2 = lua_tonumber(L, 3);
  double arg3 = lua_tonumber(L, 4);
  double arg4 = lua_tonumber(L, 5);
  QPolygon ret = __lua__obj->QGraphicsView::mapFromScene(arg1, arg2, arg3, arg4);
  lqtL_passudata(L, new QPolygon(ret), "QPolygon*");
  return 1;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__mapFromScene (lua_State *L) {
  int score[7];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QGraphicsView*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPointF*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x11964d0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QGraphicsView*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QRectF*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1196f20;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  score[3] += lqtL_testudata(L, 1, "QGraphicsView*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPolygonF*")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x11979d0;
  } else {
    score[3] -= premium*premium;
  }
  score[4] = 0;
  score[4] += lqtL_testudata(L, 1, "QGraphicsView*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPainterPath*")) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x1198450;
  } else {
    score[4] -= premium*premium;
  }
  score[5] = 0;
  score[5] += lqtL_testudata(L, 1, "QGraphicsView*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[5] += premium;
  } else if (false) {
    score[5] += premium-1; // table: 0x119b2b0;
  } else {
    score[5] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[5] += premium;
  } else if (false) {
    score[5] += premium-1; // table: 0x119afb0;
  } else {
    score[5] -= premium*premium;
  }
  score[6] = 0;
  score[6] += lqtL_testudata(L, 1, "QGraphicsView*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x119c110;
  } else {
    score[6] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x119bb90;
  } else {
    score[6] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x119c9e0;
  } else {
    score[6] -= premium*premium;
  }
  if (lua_isnumber(L, 5)) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x119c980;
  } else {
    score[6] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=6;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__mapFromScene__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__mapFromScene__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__mapFromScene__OverloadedVersion__3(L); break;
    case 4: return __LuaWrapCall__mapFromScene__OverloadedVersion__4(L); break;
    case 5: return __LuaWrapCall__mapFromScene__OverloadedVersion__5(L); break;
    case 6: return __LuaWrapCall__mapFromScene__OverloadedVersion__6(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__mapFromScene matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QGraphicsView >::__LuaWrapCall__setAlignment (lua_State *L) {
  QGraphicsView *& __lua__obj = *static_cast<QGraphicsView**>(lqtL_checkudata(L, 1, "QGraphicsView*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<Qt::AlignmentFlag> arg1 = **static_cast<QFlags<Qt::AlignmentFlag>**>(lqtL_checkudata(L, 2, "QFlags<Qt::AlignmentFlag>*"));
  __lua__obj->QGraphicsView::setAlignment(arg1);
  return 0;
}
void LuaBinder< QGraphicsView >::styleChange (QStyle& arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "styleChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QStyle*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::styleChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsView >::mouseReleaseEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mouseReleaseEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsView::mouseReleaseEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsView >::keyPressEvent (QKeyEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "keyPressEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QKeyEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsView::keyPressEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsView >::contextMenuEvent (QContextMenuEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "contextMenuEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QContextMenuEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsView::contextMenuEvent(arg1);
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QGraphicsView >::devType () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "devType");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::devType();
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QGraphicsView >::drawBackground (QPainter * arg1, const QRectF& arg2) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "drawBackground");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QPainter*");
  lqtL_pushudata(L, &(arg2), "QRectF*");
  if (lua_isfunction(L, -2-2)) {
    lua_pcall(L, 2+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsView::drawBackground(arg1, arg2);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsView >::setVisible (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "setVisible");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::setVisible(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsView >::keyReleaseEvent (QKeyEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "keyReleaseEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QKeyEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsView::keyReleaseEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsView >::scrollContentsBy (int arg1, int arg2) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "scrollContentsBy");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushinteger(L, arg1);
  lua_pushinteger(L, arg2);
  if (lua_isfunction(L, -2-2)) {
    lua_pcall(L, 2+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsView::scrollContentsBy(arg1, arg2);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsView >::dragLeaveEvent (QDragLeaveEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dragLeaveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDragLeaveEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsView::dragLeaveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsView >::mousePressEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mousePressEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsView::mousePressEvent(arg1);
  }
  lua_settop(L, oldtop);
}
QSize LuaBinder< QGraphicsView >::sizeHint () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "sizeHint");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QGraphicsView::sizeHint();
  }
  QSize ret = **static_cast<QSize**>(lqtL_checkudata(L, -1, "QSize*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QGraphicsView >::customEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "customEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::customEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsView >::closeEvent (QCloseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "closeEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QCloseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::closeEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsView >::wheelEvent (QWheelEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "wheelEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QWheelEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsView::wheelEvent(arg1);
  }
  lua_settop(L, oldtop);
}
QPaintEngine * LuaBinder< QGraphicsView >::paintEngine () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "paintEngine");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::paintEngine();
  }
  QPaintEngine * ret = *static_cast<QPaintEngine**>(lqtL_checkudata(L, -1, "QPaintEngine*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QGraphicsView >::tabletEvent (QTabletEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "tabletEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QTabletEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::tabletEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsView >::focusInEvent (QFocusEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "focusInEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QFocusEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsView::focusInEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsView >::dragMoveEvent (QDragMoveEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dragMoveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDragMoveEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsView::dragMoveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsView >::moveEvent (QMoveEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "moveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMoveEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::moveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsView >::dropEvent (QDropEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dropEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDropEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsView::dropEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsView >::fontChange (const QFont& arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "fontChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QFont*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::fontChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsView >::timerEvent (QTimerEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "timerEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QTimerEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::timerEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsView >::showEvent (QShowEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "showEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QShowEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsView::showEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsView >::connectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "connectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::connectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsView >::paintEvent (QPaintEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "paintEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QPaintEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsView::paintEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsView >::childEvent (QChildEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "childEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QChildEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::childEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsView >::windowActivationChange (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "windowActivationChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::windowActivationChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsView >::enabledChange (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "enabledChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::enabledChange(arg1);
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QGraphicsView >::metric (QPaintDevice::PaintDeviceMetric arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "metric");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushenum(L, arg1, "QPaintDevice::PaintDeviceMetric");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::metric(arg1);
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
int LuaBinder< QGraphicsView >::heightForWidth (int arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "heightForWidth");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushinteger(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::heightForWidth(arg1);
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QGraphicsView >::mouseDoubleClickEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mouseDoubleClickEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsView::mouseDoubleClickEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsView >::actionEvent (QActionEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "actionEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QActionEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::actionEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsView >::disconnectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "disconnectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::disconnectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsView >::mouseMoveEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mouseMoveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsView::mouseMoveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsView >::changeEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "changeEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QFrame::changeEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsView >::drawForeground (QPainter * arg1, const QRectF& arg2) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "drawForeground");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QPainter*");
  lqtL_pushudata(L, &(arg2), "QRectF*");
  if (lua_isfunction(L, -2-2)) {
    lua_pcall(L, 2+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsView::drawForeground(arg1, arg2);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsView >::focusOutEvent (QFocusEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "focusOutEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QFocusEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsView::focusOutEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsView >::drawItems (QPainter * arg1, int arg2, QGraphicsItem * * arg3, const QStyleOptionGraphicsItem * arg4) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "drawItems");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QPainter*");
  lua_pushinteger(L, arg2);
  lqtL_pushudata(L, arg3, "QGraphicsItem **");
  lqtL_pushudata(L, arg4, "QStyleOptionGraphicsItem*");
  if (lua_isfunction(L, -4-2)) {
    lua_pcall(L, 4+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsView::drawItems(arg1, arg2, arg3, arg4);
  }
  lua_settop(L, oldtop);
}
QSize LuaBinder< QGraphicsView >::minimumSizeHint () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "minimumSizeHint");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QAbstractScrollArea::minimumSizeHint();
  }
  QSize ret = **static_cast<QSize**>(lqtL_checkudata(L, -1, "QSize*"));
  lua_settop(L, oldtop);
  return ret;
}
bool LuaBinder< QGraphicsView >::eventFilter (QObject * arg1, QEvent * arg2) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "eventFilter");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QObject*");
  lqtL_pushudata(L, arg2, "QEvent*");
  if (lua_isfunction(L, -2-2)) {
    lua_pcall(L, 2+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QObject::eventFilter(arg1, arg2);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QGraphicsView >::inputMethodEvent (QInputMethodEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "inputMethodEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QInputMethodEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsView::inputMethodEvent(arg1);
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QGraphicsView >::viewportEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "viewportEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QGraphicsView::viewportEvent(arg1);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QGraphicsView >::resizeEvent (QResizeEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "resizeEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QResizeEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsView::resizeEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsView >::paletteChange (const QPalette& arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "paletteChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QPalette*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::paletteChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsView >::languageChange () {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "languageChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::languageChange();
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QGraphicsView >::event (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "event");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QGraphicsView::event(arg1);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QGraphicsView >::hideEvent (QHideEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "hideEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QHideEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::hideEvent(arg1);
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QGraphicsView >::focusNextPrevChild (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "focusNextPrevChild");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::focusNextPrevChild(arg1);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
const QMetaObject * LuaBinder< QGraphicsView >::metaObject () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "metaObject");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QGraphicsView::metaObject();
  }
  const QMetaObject * ret = *static_cast<QMetaObject**>(lqtL_checkudata(L, -1, "QMetaObject*"));
  lua_settop(L, oldtop);
  return ret;
}
QVariant LuaBinder< QGraphicsView >::inputMethodQuery (Qt::InputMethodQuery arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "inputMethodQuery");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushenum(L, arg1, "Qt::InputMethodQuery");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QGraphicsView::inputMethodQuery(arg1);
  }
  QVariant ret = **static_cast<QVariant**>(lqtL_checkudata(L, -1, "QVariant*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QGraphicsView >::leaveEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "leaveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::leaveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsView >::dragEnterEvent (QDragEnterEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dragEnterEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDragEnterEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsView::dragEnterEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsView >::enterEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "enterEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::enterEvent(arg1);
  }
  lua_settop(L, oldtop);
}
LuaBinder< QGraphicsView >::  ~LuaBinder< QGraphicsView > () {
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsView*");
  lua_getfield(L, -1, "~QGraphicsView");

  if (lua_isfunction(L, -1)) {
    lua_insert(L, -2);
    lua_pcall(L, 1, 1, 0);
  } else {
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QGraphicsView >::lqt_pushenum_ViewportAnchor (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "NoAnchor");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "NoAnchor");
  lua_pushstring(L, "AnchorViewCenter");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "AnchorViewCenter");
  lua_pushstring(L, "AnchorUnderMouse");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "AnchorUnderMouse");
  lua_pushcfunction(L, LuaBinder< QGraphicsView >::lqt_pushenum_ViewportAnchor_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QGraphicsView::ViewportAnchor");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QGraphicsView >::lqt_pushenum_ViewportAnchor_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QGraphicsView::ViewportAnchor>*) + sizeof(QFlags<QGraphicsView::ViewportAnchor>));
  QFlags<QGraphicsView::ViewportAnchor> *fl = static_cast<QFlags<QGraphicsView::ViewportAnchor>*>( static_cast<void*>(&static_cast<QFlags<QGraphicsView::ViewportAnchor>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QGraphicsView::ViewportAnchor>(lqtL_toenum(L, i, "QGraphicsView::ViewportAnchor"));
	}
	if (luaL_newmetatable(L, "QFlags<QGraphicsView::ViewportAnchor>*")) {
		lua_pushstring(L, "QFlags<QGraphicsView::ViewportAnchor>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinder< QGraphicsView >::lqt_pushenum_CacheModeFlag (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "CacheNone");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "CacheNone");
  lua_pushstring(L, "CacheBackground");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "CacheBackground");
  lua_pushcfunction(L, LuaBinder< QGraphicsView >::lqt_pushenum_CacheModeFlag_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QGraphicsView::CacheModeFlag");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QGraphicsView >::lqt_pushenum_CacheModeFlag_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QGraphicsView::CacheModeFlag>*) + sizeof(QFlags<QGraphicsView::CacheModeFlag>));
  QFlags<QGraphicsView::CacheModeFlag> *fl = static_cast<QFlags<QGraphicsView::CacheModeFlag>*>( static_cast<void*>(&static_cast<QFlags<QGraphicsView::CacheModeFlag>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QGraphicsView::CacheModeFlag>(lqtL_toenum(L, i, "QGraphicsView::CacheModeFlag"));
	}
	if (luaL_newmetatable(L, "QFlags<QGraphicsView::CacheModeFlag>*")) {
		lua_pushstring(L, "QFlags<QGraphicsView::CacheModeFlag>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinder< QGraphicsView >::lqt_pushenum_DragMode (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "NoDrag");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "NoDrag");
  lua_pushstring(L, "ScrollHandDrag");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "ScrollHandDrag");
  lua_pushstring(L, "RubberBandDrag");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "RubberBandDrag");
  lua_pushcfunction(L, LuaBinder< QGraphicsView >::lqt_pushenum_DragMode_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QGraphicsView::DragMode");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QGraphicsView >::lqt_pushenum_DragMode_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QGraphicsView::DragMode>*) + sizeof(QFlags<QGraphicsView::DragMode>));
  QFlags<QGraphicsView::DragMode> *fl = static_cast<QFlags<QGraphicsView::DragMode>*>( static_cast<void*>(&static_cast<QFlags<QGraphicsView::DragMode>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QGraphicsView::DragMode>(lqtL_toenum(L, i, "QGraphicsView::DragMode"));
	}
	if (luaL_newmetatable(L, "QFlags<QGraphicsView::DragMode>*")) {
		lua_pushstring(L, "QFlags<QGraphicsView::DragMode>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinder< QGraphicsView >::lqt_pushenum_ViewportUpdateMode (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "FullViewportUpdate");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "FullViewportUpdate");
  lua_pushstring(L, "MinimalViewportUpdate");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "MinimalViewportUpdate");
  lua_pushstring(L, "SmartViewportUpdate");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "SmartViewportUpdate");
  lua_pushstring(L, "NoViewportUpdate");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "NoViewportUpdate");
  lua_pushcfunction(L, LuaBinder< QGraphicsView >::lqt_pushenum_ViewportUpdateMode_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QGraphicsView::ViewportUpdateMode");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QGraphicsView >::lqt_pushenum_ViewportUpdateMode_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QGraphicsView::ViewportUpdateMode>*) + sizeof(QFlags<QGraphicsView::ViewportUpdateMode>));
  QFlags<QGraphicsView::ViewportUpdateMode> *fl = static_cast<QFlags<QGraphicsView::ViewportUpdateMode>*>( static_cast<void*>(&static_cast<QFlags<QGraphicsView::ViewportUpdateMode>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QGraphicsView::ViewportUpdateMode>(lqtL_toenum(L, i, "QGraphicsView::ViewportUpdateMode"));
	}
	if (luaL_newmetatable(L, "QFlags<QGraphicsView::ViewportUpdateMode>*")) {
		lua_pushstring(L, "QFlags<QGraphicsView::ViewportUpdateMode>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinder< QGraphicsView >::lqt_pushenum_OptimizationFlag (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "DontClipPainter");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "DontClipPainter");
  lua_pushstring(L, "DontSavePainterState");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "DontSavePainterState");
  lua_pushstring(L, "DontAdjustForAntialiasing");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "DontAdjustForAntialiasing");
  lua_pushcfunction(L, LuaBinder< QGraphicsView >::lqt_pushenum_OptimizationFlag_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QGraphicsView::OptimizationFlag");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QGraphicsView >::lqt_pushenum_OptimizationFlag_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QGraphicsView::OptimizationFlag>*) + sizeof(QFlags<QGraphicsView::OptimizationFlag>));
  QFlags<QGraphicsView::OptimizationFlag> *fl = static_cast<QFlags<QGraphicsView::OptimizationFlag>*>( static_cast<void*>(&static_cast<QFlags<QGraphicsView::OptimizationFlag>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QGraphicsView::OptimizationFlag>(lqtL_toenum(L, i, "QGraphicsView::OptimizationFlag"));
	}
	if (luaL_newmetatable(L, "QFlags<QGraphicsView::OptimizationFlag>*")) {
		lua_pushstring(L, "QFlags<QGraphicsView::OptimizationFlag>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int luaopen_QGraphicsView (lua_State *L) {
  if (luaL_newmetatable(L, "QGraphicsView*")) {
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__render);
    lua_setfield(L, -2, "render");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__setOptimizationFlag);
    lua_setfield(L, -2, "setOptimizationFlag");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__sizeHint);
    lua_setfield(L, -2, "sizeHint");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__setMatrix);
    lua_setfield(L, -2, "setMatrix");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__setInteractive);
    lua_setfield(L, -2, "setInteractive");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__sceneRect);
    lua_setfield(L, -2, "sceneRect");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__viewportTransform);
    lua_setfield(L, -2, "viewportTransform");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__setOptimizationFlags);
    lua_setfield(L, -2, "setOptimizationFlags");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__setTransformationAnchor);
    lua_setfield(L, -2, "setTransformationAnchor");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__trUtf8);
    lua_setfield(L, -2, "trUtf8");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__optimizationFlags);
    lua_setfield(L, -2, "optimizationFlags");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__rubberBandSelectionMode);
    lua_setfield(L, -2, "rubberBandSelectionMode");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__tr);
    lua_setfield(L, -2, "tr");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__renderHints);
    lua_setfield(L, -2, "renderHints");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__setCacheMode);
    lua_setfield(L, -2, "setCacheMode");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__delete);
    lua_setfield(L, -2, "delete");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__new);
    lua_setfield(L, -2, "new");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__setRenderHints);
    lua_setfield(L, -2, "setRenderHints");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__updateScene);
    lua_setfield(L, -2, "updateScene");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__setRenderHint);
    lua_setfield(L, -2, "setRenderHint");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__items);
    lua_setfield(L, -2, "items");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__invalidateScene);
    lua_setfield(L, -2, "invalidateScene");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__matrix);
    lua_setfield(L, -2, "matrix");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__transformationAnchor);
    lua_setfield(L, -2, "transformationAnchor");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__centerOn);
    lua_setfield(L, -2, "centerOn");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__rotate);
    lua_setfield(L, -2, "rotate");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__setScene);
    lua_setfield(L, -2, "setScene");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__transform);
    lua_setfield(L, -2, "transform");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__resetMatrix);
    lua_setfield(L, -2, "resetMatrix");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__setRubberBandSelectionMode);
    lua_setfield(L, -2, "setRubberBandSelectionMode");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__setTransform);
    lua_setfield(L, -2, "setTransform");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__setSceneRect);
    lua_setfield(L, -2, "setSceneRect");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__scale);
    lua_setfield(L, -2, "scale");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__resizeAnchor);
    lua_setfield(L, -2, "resizeAnchor");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__dragMode);
    lua_setfield(L, -2, "dragMode");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__shear);
    lua_setfield(L, -2, "shear");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__fitInView);
    lua_setfield(L, -2, "fitInView");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__isInteractive);
    lua_setfield(L, -2, "isInteractive");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__setResizeAnchor);
    lua_setfield(L, -2, "setResizeAnchor");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__translate);
    lua_setfield(L, -2, "translate");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__ensureVisible);
    lua_setfield(L, -2, "ensureVisible");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__updateSceneRect);
    lua_setfield(L, -2, "updateSceneRect");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__backgroundBrush);
    lua_setfield(L, -2, "backgroundBrush");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__cacheMode);
    lua_setfield(L, -2, "cacheMode");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__inputMethodQuery);
    lua_setfield(L, -2, "inputMethodQuery");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__setViewportUpdateMode);
    lua_setfield(L, -2, "setViewportUpdateMode");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__mapToScene);
    lua_setfield(L, -2, "mapToScene");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__foregroundBrush);
    lua_setfield(L, -2, "foregroundBrush");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__resetTransform);
    lua_setfield(L, -2, "resetTransform");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__scene);
    lua_setfield(L, -2, "scene");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__metaObject);
    lua_setfield(L, -2, "metaObject");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__resetCachedContent);
    lua_setfield(L, -2, "resetCachedContent");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__setForegroundBrush);
    lua_setfield(L, -2, "setForegroundBrush");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__itemAt);
    lua_setfield(L, -2, "itemAt");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__alignment);
    lua_setfield(L, -2, "alignment");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__setDragMode);
    lua_setfield(L, -2, "setDragMode");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__viewportUpdateMode);
    lua_setfield(L, -2, "viewportUpdateMode");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__setBackgroundBrush);
    lua_setfield(L, -2, "setBackgroundBrush");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__mapFromScene);
    lua_setfield(L, -2, "mapFromScene");
    lua_pushcfunction(L, LuaBinder< QGraphicsView >::__LuaWrapCall__setAlignment);
    lua_setfield(L, -2, "setAlignment");
    LuaBinder< QGraphicsView >::lqt_pushenum_ViewportAnchor(L);
    lua_setfield(L, -2, "ViewportAnchor");
    LuaBinder< QGraphicsView >::lqt_pushenum_CacheModeFlag(L);
    lua_setfield(L, -2, "CacheModeFlag");
    LuaBinder< QGraphicsView >::lqt_pushenum_DragMode(L);
    lua_setfield(L, -2, "DragMode");
    LuaBinder< QGraphicsView >::lqt_pushenum_ViewportUpdateMode(L);
    lua_setfield(L, -2, "ViewportUpdateMode");
    LuaBinder< QGraphicsView >::lqt_pushenum_OptimizationFlag(L);
    lua_setfield(L, -2, "OptimizationFlag");
    lua_newtable(L);
    lua_pushboolean(L, 1);
    lua_setfield(L, -2, "QAbstractScrollArea*");
    lua_pushboolean(L, 0);
    lua_setfield(L, -2, "QObject*");
    lua_pushboolean(L, 0);
    lua_setfield(L, -2, "QPaintDevice*");
    lua_pushboolean(L, 0);
    lua_setfield(L, -2, "QWidget*");
    lua_pushboolean(L, 0);
    lua_setfield(L, -2, "QFrame*");
    lua_setfield(L, -2, "__base");
    lua_pushcfunction(L, lqtL_newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, lqtL_index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, lqtL_gc);
    lua_setfield(L, -2, "__gc");
    lua_pushstring(L, "QGraphicsView");
    lua_setfield(L, -2, "__qtype");
		lua_setglobal(L, "QGraphicsView");
  } else {
		lua_pop(L, 1);
	}
  return 0;
}
