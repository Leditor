#include "lqt_bind_QSyntaxHighlighter.hpp"

int LuaBinder< QSyntaxHighlighter >::__LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QSyntaxHighlighter::tr(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QSyntaxHighlighter >::__LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QSyntaxHighlighter::tr(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QSyntaxHighlighter >::__LuaWrapCall__tr (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0xa86b00;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0xeae5f0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x8ae060;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x979540;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xc46500;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__tr__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__tr__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__tr matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QSyntaxHighlighter >::__LuaWrapCall__delete (lua_State *L) {
  QSyntaxHighlighter *& __lua__obj = *static_cast<QSyntaxHighlighter**>(lqtL_checkudata(L, 1, "QSyntaxHighlighter*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  delete __lua__obj;
  __lua__obj = 0;
  return 0;
}
int LuaBinder< QSyntaxHighlighter >::__LuaWrapCall__new__OverloadedVersion__1 (lua_State *L) {
  QObject * arg1 = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
  QSyntaxHighlighter * ret = new LuaBinder< QSyntaxHighlighter >(L, arg1);
  lqtL_passudata(L, ret, "QSyntaxHighlighter*");
  return 1;
}
int LuaBinder< QSyntaxHighlighter >::__LuaWrapCall__new__OverloadedVersion__2 (lua_State *L) {
  QTextDocument * arg1 = *static_cast<QTextDocument**>(lqtL_checkudata(L, 1, "QTextDocument*"));
  QSyntaxHighlighter * ret = new LuaBinder< QSyntaxHighlighter >(L, arg1);
  lqtL_passudata(L, ret, "QSyntaxHighlighter*");
  return 1;
}
int LuaBinder< QSyntaxHighlighter >::__LuaWrapCall__new__OverloadedVersion__3 (lua_State *L) {
  QTextEdit * arg1 = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
  QSyntaxHighlighter * ret = new LuaBinder< QSyntaxHighlighter >(L, arg1);
  lqtL_passudata(L, ret, "QSyntaxHighlighter*");
  return 1;
}
int LuaBinder< QSyntaxHighlighter >::__LuaWrapCall__new (lua_State *L) {
  int score[4];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if (lqtL_testudata(L, 1, "QObject*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0xe4af40;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if (lqtL_testudata(L, 1, "QTextDocument*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x796ff0;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  if (lqtL_testudata(L, 1, "QTextEdit*")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0xd2be50;
  } else {
    score[3] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=3;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__new__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__new__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__new__OverloadedVersion__3(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__new matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QSyntaxHighlighter >::__LuaWrapCall__setDocument (lua_State *L) {
  QSyntaxHighlighter *& __lua__obj = *static_cast<QSyntaxHighlighter**>(lqtL_checkudata(L, 1, "QSyntaxHighlighter*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextDocument * arg1 = *static_cast<QTextDocument**>(lqtL_checkudata(L, 2, "QTextDocument*"));
  __lua__obj->QSyntaxHighlighter::setDocument(arg1);
  return 0;
}
int LuaBinder< QSyntaxHighlighter >::__LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QSyntaxHighlighter::trUtf8(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QSyntaxHighlighter >::__LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QSyntaxHighlighter::trUtf8(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QSyntaxHighlighter >::__LuaWrapCall__trUtf8 (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x9ed820;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0xee36c0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x947210;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x94b260;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x9355a0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__trUtf8__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__trUtf8__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__trUtf8 matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QSyntaxHighlighter >::__LuaWrapCall__metaObject (lua_State *L) {
  QSyntaxHighlighter *& __lua__obj = *static_cast<QSyntaxHighlighter**>(lqtL_checkudata(L, 1, "QSyntaxHighlighter*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QMetaObject * ret = __lua__obj->QSyntaxHighlighter::metaObject();
  lqtL_pushudata(L, ret, "QMetaObject*");
  return 1;
}
int LuaBinder< QSyntaxHighlighter >::__LuaWrapCall__rehighlight (lua_State *L) {
  QSyntaxHighlighter *& __lua__obj = *static_cast<QSyntaxHighlighter**>(lqtL_checkudata(L, 1, "QSyntaxHighlighter*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QSyntaxHighlighter::rehighlight();
  return 0;
}
int LuaBinder< QSyntaxHighlighter >::__LuaWrapCall__document (lua_State *L) {
  QSyntaxHighlighter *& __lua__obj = *static_cast<QSyntaxHighlighter**>(lqtL_checkudata(L, 1, "QSyntaxHighlighter*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextDocument * ret = __lua__obj->QSyntaxHighlighter::document();
  lqtL_pushudata(L, ret, "QTextDocument*");
  return 1;
}
bool LuaBinder< QSyntaxHighlighter >::eventFilter (QObject * arg1, QEvent * arg2) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QSyntaxHighlighter*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "eventFilter");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QObject*");
  lqtL_pushudata(L, arg2, "QEvent*");
  if (lua_isfunction(L, -2-2)) {
    lua_pcall(L, 2+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QObject::eventFilter(arg1, arg2);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QSyntaxHighlighter >::highlightBlock (const QString& arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QSyntaxHighlighter*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "highlightBlock");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushlstring(L, arg1.toAscii().data(), arg1.toAscii().size());
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
      }
  lua_settop(L, oldtop);
}
const QMetaObject * LuaBinder< QSyntaxHighlighter >::metaObject () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QSyntaxHighlighter*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "metaObject");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QSyntaxHighlighter::metaObject();
  }
  const QMetaObject * ret = *static_cast<QMetaObject**>(lqtL_checkudata(L, -1, "QMetaObject*"));
  lua_settop(L, oldtop);
  return ret;
}
bool LuaBinder< QSyntaxHighlighter >::event (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QSyntaxHighlighter*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "event");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QObject::event(arg1);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QSyntaxHighlighter >::disconnectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QSyntaxHighlighter*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "disconnectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::disconnectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QSyntaxHighlighter >::connectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QSyntaxHighlighter*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "connectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::connectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QSyntaxHighlighter >::timerEvent (QTimerEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QSyntaxHighlighter*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "timerEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QTimerEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::timerEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QSyntaxHighlighter >::childEvent (QChildEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QSyntaxHighlighter*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "childEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QChildEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::childEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QSyntaxHighlighter >::customEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QSyntaxHighlighter*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "customEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::customEvent(arg1);
  }
  lua_settop(L, oldtop);
}
LuaBinder< QSyntaxHighlighter >::  ~LuaBinder< QSyntaxHighlighter > () {
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QSyntaxHighlighter*");
  lua_getfield(L, -1, "~QSyntaxHighlighter");

  if (lua_isfunction(L, -1)) {
    lua_insert(L, -2);
    lua_pcall(L, 1, 1, 0);
  } else {
  }
  lua_settop(L, oldtop);
}
int luaopen_QSyntaxHighlighter (lua_State *L) {
  if (luaL_newmetatable(L, "QSyntaxHighlighter*")) {
    lua_pushcfunction(L, LuaBinder< QSyntaxHighlighter >::__LuaWrapCall__tr);
    lua_setfield(L, -2, "tr");
    lua_pushcfunction(L, LuaBinder< QSyntaxHighlighter >::__LuaWrapCall__delete);
    lua_setfield(L, -2, "delete");
    lua_pushcfunction(L, LuaBinder< QSyntaxHighlighter >::__LuaWrapCall__new);
    lua_setfield(L, -2, "new");
    lua_pushcfunction(L, LuaBinder< QSyntaxHighlighter >::__LuaWrapCall__setDocument);
    lua_setfield(L, -2, "setDocument");
    lua_pushcfunction(L, LuaBinder< QSyntaxHighlighter >::__LuaWrapCall__trUtf8);
    lua_setfield(L, -2, "trUtf8");
    lua_pushcfunction(L, LuaBinder< QSyntaxHighlighter >::__LuaWrapCall__metaObject);
    lua_setfield(L, -2, "metaObject");
    lua_pushcfunction(L, LuaBinder< QSyntaxHighlighter >::__LuaWrapCall__rehighlight);
    lua_setfield(L, -2, "rehighlight");
    lua_pushcfunction(L, LuaBinder< QSyntaxHighlighter >::__LuaWrapCall__document);
    lua_setfield(L, -2, "document");
    lua_newtable(L);
    lua_pushboolean(L, 1);
    lua_setfield(L, -2, "QObject*");
    lua_setfield(L, -2, "__base");
    lua_pushcfunction(L, lqtL_newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, lqtL_index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, lqtL_gc);
    lua_setfield(L, -2, "__gc");
    lua_pushstring(L, "QSyntaxHighlighter");
    lua_setfield(L, -2, "__qtype");
		lua_setglobal(L, "QSyntaxHighlighter");
  } else {
		lua_pop(L, 1);
	}
  return 0;
}
