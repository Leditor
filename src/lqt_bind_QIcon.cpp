#include "lqt_bind_QIcon.hpp"

int LuaBinder< QIcon >::__LuaWrapCall__paint__OverloadedVersion__1 (lua_State *L) {
  QIcon *& __lua__obj = *static_cast<QIcon**>(lqtL_checkudata(L, 1, "QIcon*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QPainter * arg1 = *static_cast<QPainter**>(lqtL_checkudata(L, 2, "QPainter*"));
  const QRect& arg2 = **static_cast<QRect**>(lqtL_checkudata(L, 3, "QRect*"));
  QFlags<Qt::AlignmentFlag> arg3 = lqtL_testudata(L, 4, "QFlags<Qt::AlignmentFlag>*")?**static_cast<QFlags<Qt::AlignmentFlag>**>(lqtL_checkudata(L, 4, "QFlags<Qt::AlignmentFlag>*")):Qt::AlignCenter;
  QIcon::Mode arg4 = lqtL_isenum(L, 5, "QIcon::Mode")?static_cast<QIcon::Mode>(lqtL_toenum(L, 5, "QIcon::Mode")):QIcon::Normal;
  QIcon::State arg5 = lqtL_isenum(L, 6, "QIcon::State")?static_cast<QIcon::State>(lqtL_toenum(L, 6, "QIcon::State")):QIcon::Off;
  __lua__obj->QIcon::paint(arg1, arg2, arg3, arg4, arg5);
  return 0;
}
int LuaBinder< QIcon >::__LuaWrapCall__paint__OverloadedVersion__2 (lua_State *L) {
  QIcon *& __lua__obj = *static_cast<QIcon**>(lqtL_checkudata(L, 1, "QIcon*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QPainter * arg1 = *static_cast<QPainter**>(lqtL_checkudata(L, 2, "QPainter*"));
  int arg2 = lua_tointeger(L, 3);
  int arg3 = lua_tointeger(L, 4);
  int arg4 = lua_tointeger(L, 5);
  int arg5 = lua_tointeger(L, 6);
  QFlags<Qt::AlignmentFlag> arg6 = lqtL_testudata(L, 7, "QFlags<Qt::AlignmentFlag>*")?**static_cast<QFlags<Qt::AlignmentFlag>**>(lqtL_checkudata(L, 7, "QFlags<Qt::AlignmentFlag>*")):Qt::AlignCenter;
  QIcon::Mode arg7 = lqtL_isenum(L, 8, "QIcon::Mode")?static_cast<QIcon::Mode>(lqtL_toenum(L, 8, "QIcon::Mode")):QIcon::Normal;
  QIcon::State arg8 = lqtL_isenum(L, 9, "QIcon::State")?static_cast<QIcon::State>(lqtL_toenum(L, 9, "QIcon::State")):QIcon::Off;
  __lua__obj->QIcon::paint(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
  return 0;
}
int LuaBinder< QIcon >::__LuaWrapCall__paint (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QIcon*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPainter*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x13b2170;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_testudata(L, 3, "QRect*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x13b1770;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_testudata(L, 4, "QFlags<Qt::AlignmentFlag>*")) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x13b2660;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_isenum(L, 5, "QIcon::Mode")) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x13b2a20;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_isenum(L, 6, "QIcon::State")) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x13b3310;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QIcon*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPainter*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x13b3c70;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x13b41c0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x13b4570;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 5)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x13b4510;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 6)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x13b4d20;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_testudata(L, 7, "QFlags<Qt::AlignmentFlag>*")) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x13b48f0;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_isenum(L, 8, "QIcon::Mode")) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x13b5520;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_isenum(L, 9, "QIcon::State")) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x13b58d0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__paint__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__paint__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__paint matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QIcon >::__LuaWrapCall__serialNumber (lua_State *L) {
  QIcon *& __lua__obj = *static_cast<QIcon**>(lqtL_checkudata(L, 1, "QIcon*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QIcon::serialNumber();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QIcon >::__LuaWrapCall__addFile (lua_State *L) {
  QIcon *& __lua__obj = *static_cast<QIcon**>(lqtL_checkudata(L, 1, "QIcon*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  const QSize& arg2 = lqtL_testudata(L, 3, "QSize*")?**static_cast<QSize**>(lqtL_checkudata(L, 3, "QSize*")):QSize();
  QIcon::Mode arg3 = lqtL_isenum(L, 4, "QIcon::Mode")?static_cast<QIcon::Mode>(lqtL_toenum(L, 4, "QIcon::Mode")):QIcon::Normal;
  QIcon::State arg4 = lqtL_isenum(L, 5, "QIcon::State")?static_cast<QIcon::State>(lqtL_toenum(L, 5, "QIcon::State")):QIcon::Off;
  __lua__obj->QIcon::addFile(arg1, arg2, arg3, arg4);
  return 0;
}
int LuaBinder< QIcon >::__LuaWrapCall__detach (lua_State *L) {
  QIcon *& __lua__obj = *static_cast<QIcon**>(lqtL_checkudata(L, 1, "QIcon*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QIcon::detach();
  return 0;
}
int LuaBinder< QIcon >::__LuaWrapCall__cacheKey (lua_State *L) {
  QIcon *& __lua__obj = *static_cast<QIcon**>(lqtL_checkudata(L, 1, "QIcon*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  long long int ret = __lua__obj->QIcon::cacheKey();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QIcon >::__LuaWrapCall__actualSize (lua_State *L) {
  QIcon *& __lua__obj = *static_cast<QIcon**>(lqtL_checkudata(L, 1, "QIcon*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QSize& arg1 = **static_cast<QSize**>(lqtL_checkudata(L, 2, "QSize*"));
  QIcon::Mode arg2 = lqtL_isenum(L, 3, "QIcon::Mode")?static_cast<QIcon::Mode>(lqtL_toenum(L, 3, "QIcon::Mode")):QIcon::Normal;
  QIcon::State arg3 = lqtL_isenum(L, 4, "QIcon::State")?static_cast<QIcon::State>(lqtL_toenum(L, 4, "QIcon::State")):QIcon::Off;
  QSize ret = __lua__obj->QIcon::actualSize(arg1, arg2, arg3);
  lqtL_passudata(L, new QSize(ret), "QSize*");
  return 1;
}
int LuaBinder< QIcon >::__LuaWrapCall__delete (lua_State *L) {
  QIcon *& __lua__obj = *static_cast<QIcon**>(lqtL_checkudata(L, 1, "QIcon*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  delete __lua__obj;
  __lua__obj = 0;
  return 0;
}
int LuaBinder< QIcon >::__LuaWrapCall__isNull (lua_State *L) {
  QIcon *& __lua__obj = *static_cast<QIcon**>(lqtL_checkudata(L, 1, "QIcon*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QIcon::isNull();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QIcon >::__LuaWrapCall__addPixmap (lua_State *L) {
  QIcon *& __lua__obj = *static_cast<QIcon**>(lqtL_checkudata(L, 1, "QIcon*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPixmap& arg1 = **static_cast<QPixmap**>(lqtL_checkudata(L, 2, "QPixmap*"));
  QIcon::Mode arg2 = lqtL_isenum(L, 3, "QIcon::Mode")?static_cast<QIcon::Mode>(lqtL_toenum(L, 3, "QIcon::Mode")):QIcon::Normal;
  QIcon::State arg3 = lqtL_isenum(L, 4, "QIcon::State")?static_cast<QIcon::State>(lqtL_toenum(L, 4, "QIcon::State")):QIcon::Off;
  __lua__obj->QIcon::addPixmap(arg1, arg2, arg3);
  return 0;
}
int LuaBinder< QIcon >::__LuaWrapCall__isDetached (lua_State *L) {
  QIcon *& __lua__obj = *static_cast<QIcon**>(lqtL_checkudata(L, 1, "QIcon*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QIcon::isDetached();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QIcon >::__LuaWrapCall__pixmap__OverloadedVersion__1 (lua_State *L) {
  QIcon *& __lua__obj = *static_cast<QIcon**>(lqtL_checkudata(L, 1, "QIcon*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QSize& arg1 = **static_cast<QSize**>(lqtL_checkudata(L, 2, "QSize*"));
  QIcon::Mode arg2 = lqtL_isenum(L, 3, "QIcon::Mode")?static_cast<QIcon::Mode>(lqtL_toenum(L, 3, "QIcon::Mode")):QIcon::Normal;
  QIcon::State arg3 = lqtL_isenum(L, 4, "QIcon::State")?static_cast<QIcon::State>(lqtL_toenum(L, 4, "QIcon::State")):QIcon::Off;
  QPixmap ret = __lua__obj->QIcon::pixmap(arg1, arg2, arg3);
  lqtL_passudata(L, new QPixmap(ret), "QPixmap*");
  return 1;
}
int LuaBinder< QIcon >::__LuaWrapCall__pixmap__OverloadedVersion__2 (lua_State *L) {
  QIcon *& __lua__obj = *static_cast<QIcon**>(lqtL_checkudata(L, 1, "QIcon*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int arg2 = lua_tointeger(L, 3);
  QIcon::Mode arg3 = lqtL_isenum(L, 4, "QIcon::Mode")?static_cast<QIcon::Mode>(lqtL_toenum(L, 4, "QIcon::Mode")):QIcon::Normal;
  QIcon::State arg4 = lqtL_isenum(L, 5, "QIcon::State")?static_cast<QIcon::State>(lqtL_toenum(L, 5, "QIcon::State")):QIcon::Off;
  QPixmap ret = __lua__obj->QIcon::pixmap(arg1, arg2, arg3, arg4);
  lqtL_passudata(L, new QPixmap(ret), "QPixmap*");
  return 1;
}
int LuaBinder< QIcon >::__LuaWrapCall__pixmap__OverloadedVersion__3 (lua_State *L) {
  QIcon *& __lua__obj = *static_cast<QIcon**>(lqtL_checkudata(L, 1, "QIcon*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  QIcon::Mode arg2 = lqtL_isenum(L, 3, "QIcon::Mode")?static_cast<QIcon::Mode>(lqtL_toenum(L, 3, "QIcon::Mode")):QIcon::Normal;
  QIcon::State arg3 = lqtL_isenum(L, 4, "QIcon::State")?static_cast<QIcon::State>(lqtL_toenum(L, 4, "QIcon::State")):QIcon::Off;
  QPixmap ret = __lua__obj->QIcon::pixmap(arg1, arg2, arg3);
  lqtL_passudata(L, new QPixmap(ret), "QPixmap*");
  return 1;
}
int LuaBinder< QIcon >::__LuaWrapCall__pixmap (lua_State *L) {
  int score[4];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QIcon*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QSize*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x13ad280;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_isenum(L, 3, "QIcon::Mode")) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x13accb0;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_isenum(L, 4, "QIcon::State")) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x13ad730;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QIcon*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x13ae5b0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x13aeac0;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_isenum(L, 4, "QIcon::Mode")) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x13aea60;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_isenum(L, 5, "QIcon::State")) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x13af2c0;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  score[3] += lqtL_testudata(L, 1, "QIcon*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x13afbc0;
  } else {
    score[3] -= premium*premium;
  }
  if (lqtL_isenum(L, 3, "QIcon::Mode")) {
    score[3] += premium;
  } else if (true) {
    score[3] += premium-1; // table: 0x13af8b0;
  } else {
    score[3] -= premium*premium;
  }
  if (lqtL_isenum(L, 4, "QIcon::State")) {
    score[3] += premium;
  } else if (true) {
    score[3] += premium-1; // table: 0x13b00d0;
  } else {
    score[3] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=3;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__pixmap__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__pixmap__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__pixmap__OverloadedVersion__3(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__pixmap matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QIcon >::__LuaWrapCall__new__OverloadedVersion__1 (lua_State *L) {
  QIcon * ret = new LuaBinder< QIcon >(L);
  lqtL_passudata(L, ret, "QIcon*");
  return 1;
}
int LuaBinder< QIcon >::__LuaWrapCall__new__OverloadedVersion__2 (lua_State *L) {
  const QPixmap& arg1 = **static_cast<QPixmap**>(lqtL_checkudata(L, 1, "QPixmap*"));
  QIcon * ret = new LuaBinder< QIcon >(L, arg1);
  lqtL_passudata(L, ret, "QIcon*");
  return 1;
}
int LuaBinder< QIcon >::__LuaWrapCall__new__OverloadedVersion__3 (lua_State *L) {
  const QIcon& arg1 = **static_cast<QIcon**>(lqtL_checkudata(L, 1, "QIcon*"));
  QIcon * ret = new LuaBinder< QIcon >(L, arg1);
  lqtL_passudata(L, ret, "QIcon*");
  return 1;
}
int LuaBinder< QIcon >::__LuaWrapCall__new__OverloadedVersion__4 (lua_State *L) {
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 1), lua_objlen(L, 1));
  QIcon * ret = new LuaBinder< QIcon >(L, arg1);
  lqtL_passudata(L, ret, "QIcon*");
  return 1;
}
int LuaBinder< QIcon >::__LuaWrapCall__new__OverloadedVersion__5 (lua_State *L) {
  QIconEngine * arg1 = *static_cast<QIconEngine**>(lqtL_checkudata(L, 1, "QIconEngine*"));
  QIcon * ret = new LuaBinder< QIcon >(L, arg1);
  lqtL_passudata(L, ret, "QIcon*");
  return 1;
}
int LuaBinder< QIcon >::__LuaWrapCall__new__OverloadedVersion__6 (lua_State *L) {
  QIconEngineV2 * arg1 = *static_cast<QIconEngineV2**>(lqtL_checkudata(L, 1, "QIconEngineV2*"));
  QIcon * ret = new LuaBinder< QIcon >(L, arg1);
  lqtL_passudata(L, ret, "QIcon*");
  return 1;
}
int LuaBinder< QIcon >::__LuaWrapCall__new (lua_State *L) {
  int score[7];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[2] = 0;
  if (lqtL_testudata(L, 1, "QPixmap*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x13a8710;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  if (lqtL_testudata(L, 1, "QIcon*")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x13a90f0;
  } else {
    score[3] -= premium*premium;
  }
  score[4] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x13a9b40;
  } else {
    score[4] -= premium*premium;
  }
  score[5] = 0;
  if (lqtL_testudata(L, 1, "QIconEngine*")) {
    score[5] += premium;
  } else if (false) {
    score[5] += premium-1; // table: 0x13aa5c0;
  } else {
    score[5] -= premium*premium;
  }
  score[6] = 0;
  if (lqtL_testudata(L, 1, "QIconEngineV2*")) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x13ab0c0;
  } else {
    score[6] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=6;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__new__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__new__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__new__OverloadedVersion__3(L); break;
    case 4: return __LuaWrapCall__new__OverloadedVersion__4(L); break;
    case 5: return __LuaWrapCall__new__OverloadedVersion__5(L); break;
    case 6: return __LuaWrapCall__new__OverloadedVersion__6(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__new matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QIcon >::lqt_pushenum_Mode (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "Normal");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "Normal");
  lua_pushstring(L, "Disabled");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "Disabled");
  lua_pushstring(L, "Active");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "Active");
  lua_pushstring(L, "Selected");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "Selected");
  lua_pushcfunction(L, LuaBinder< QIcon >::lqt_pushenum_Mode_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QIcon::Mode");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QIcon >::lqt_pushenum_Mode_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QIcon::Mode>*) + sizeof(QFlags<QIcon::Mode>));
  QFlags<QIcon::Mode> *fl = static_cast<QFlags<QIcon::Mode>*>( static_cast<void*>(&static_cast<QFlags<QIcon::Mode>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QIcon::Mode>(lqtL_toenum(L, i, "QIcon::Mode"));
	}
	if (luaL_newmetatable(L, "QFlags<QIcon::Mode>*")) {
		lua_pushstring(L, "QFlags<QIcon::Mode>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinder< QIcon >::lqt_pushenum_State (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "On");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "On");
  lua_pushstring(L, "Off");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "Off");
  lua_pushcfunction(L, LuaBinder< QIcon >::lqt_pushenum_State_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QIcon::State");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QIcon >::lqt_pushenum_State_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QIcon::State>*) + sizeof(QFlags<QIcon::State>));
  QFlags<QIcon::State> *fl = static_cast<QFlags<QIcon::State>*>( static_cast<void*>(&static_cast<QFlags<QIcon::State>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QIcon::State>(lqtL_toenum(L, i, "QIcon::State"));
	}
	if (luaL_newmetatable(L, "QFlags<QIcon::State>*")) {
		lua_pushstring(L, "QFlags<QIcon::State>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int luaopen_QIcon (lua_State *L) {
  if (luaL_newmetatable(L, "QIcon*")) {
    lua_pushcfunction(L, LuaBinder< QIcon >::__LuaWrapCall__paint);
    lua_setfield(L, -2, "paint");
    lua_pushcfunction(L, LuaBinder< QIcon >::__LuaWrapCall__serialNumber);
    lua_setfield(L, -2, "serialNumber");
    lua_pushcfunction(L, LuaBinder< QIcon >::__LuaWrapCall__addFile);
    lua_setfield(L, -2, "addFile");
    lua_pushcfunction(L, LuaBinder< QIcon >::__LuaWrapCall__detach);
    lua_setfield(L, -2, "detach");
    lua_pushcfunction(L, LuaBinder< QIcon >::__LuaWrapCall__cacheKey);
    lua_setfield(L, -2, "cacheKey");
    lua_pushcfunction(L, LuaBinder< QIcon >::__LuaWrapCall__actualSize);
    lua_setfield(L, -2, "actualSize");
    lua_pushcfunction(L, LuaBinder< QIcon >::__LuaWrapCall__delete);
    lua_setfield(L, -2, "delete");
    lua_pushcfunction(L, LuaBinder< QIcon >::__LuaWrapCall__isNull);
    lua_setfield(L, -2, "isNull");
    lua_pushcfunction(L, LuaBinder< QIcon >::__LuaWrapCall__addPixmap);
    lua_setfield(L, -2, "addPixmap");
    lua_pushcfunction(L, LuaBinder< QIcon >::__LuaWrapCall__isDetached);
    lua_setfield(L, -2, "isDetached");
    lua_pushcfunction(L, LuaBinder< QIcon >::__LuaWrapCall__pixmap);
    lua_setfield(L, -2, "pixmap");
    lua_pushcfunction(L, LuaBinder< QIcon >::__LuaWrapCall__new);
    lua_setfield(L, -2, "new");
    LuaBinder< QIcon >::lqt_pushenum_Mode(L);
    lua_setfield(L, -2, "Mode");
    LuaBinder< QIcon >::lqt_pushenum_State(L);
    lua_setfield(L, -2, "State");
    lua_newtable(L);
    lua_setfield(L, -2, "__base");
    lua_pushcfunction(L, lqtL_newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, lqtL_index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, lqtL_gc);
    lua_setfield(L, -2, "__gc");
    lua_pushstring(L, "QIcon");
    lua_setfield(L, -2, "__qtype");
		lua_setglobal(L, "QIcon");
  } else {
		lua_pop(L, 1);
	}
  return 0;
}
