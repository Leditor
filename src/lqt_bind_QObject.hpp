#include "lqt_common.hpp"
#include <QObject>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QObject > : public QObject {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__parent (lua_State *L);
  static int __LuaWrapCall__signalsBlocked (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__trUtf8 (lua_State *L);
  static int __LuaWrapCall__killTimer (lua_State *L);
  static int __LuaWrapCall__dumpObjectTree (lua_State *L);
  static int __LuaWrapCall__dumpObjectInfo (lua_State *L);
  static int __LuaWrapCall__event (lua_State *L);
  static int __LuaWrapCall__installEventFilter (lua_State *L);
  static int __LuaWrapCall__inherits (lua_State *L);
  static int __LuaWrapCall__registerUserData (lua_State *L);
  static int __LuaWrapCall__dynamicPropertyNames (lua_State *L);
  static int __LuaWrapCall__connect__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__connect__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__connect (lua_State *L);
  static int __LuaWrapCall__setObjectName (lua_State *L);
  static int __LuaWrapCall__isWidgetType (lua_State *L);
  static int __LuaWrapCall__property (lua_State *L);
  static int __LuaWrapCall__children (lua_State *L);
  static int __LuaWrapCall__new (lua_State *L);
  static int __LuaWrapCall__delete (lua_State *L);
  static int __LuaWrapCall__setUserData (lua_State *L);
  static int __LuaWrapCall__eventFilter (lua_State *L);
  static int __LuaWrapCall__userData (lua_State *L);
  static int __LuaWrapCall__startTimer (lua_State *L);
  static int __LuaWrapCall__moveToThread (lua_State *L);
  static int __LuaWrapCall__thread (lua_State *L);
  static int __LuaWrapCall__blockSignals (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__tr (lua_State *L);
  static int __LuaWrapCall__setParent (lua_State *L);
  static int __LuaWrapCall__disconnect__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__disconnect__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__disconnect__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__disconnect (lua_State *L);
  static int __LuaWrapCall__removeEventFilter (lua_State *L);
  static int __LuaWrapCall__deleteLater (lua_State *L);
  static int __LuaWrapCall__metaObject (lua_State *L);
  static int __LuaWrapCall__setProperty (lua_State *L);
  static int __LuaWrapCall__objectName (lua_State *L);
  bool event (QEvent * arg1);
protected:
  void disconnectNotify (const char * arg1);
public:
  bool eventFilter (QObject * arg1, QEvent * arg2);
protected:
  void timerEvent (QTimerEvent * arg1);
public:
protected:
  void connectNotify (const char * arg1);
public:
protected:
  void childEvent (QChildEvent * arg1);
public:
  const QMetaObject * metaObject () const;
protected:
  void customEvent (QEvent * arg1);
public:
  ~LuaBinder< QObject > ();
  LuaBinder< QObject > (lua_State *l, QObject * arg1):QObject(arg1), L(l) {}
};

extern "C" int luaopen_QObject (lua_State *L);
