#include <iostream>
#include <QDebug>

#include "lqt_common.hpp"
#include "lqt_function.hpp"
#include "lqt_bind_QAbstractScrollArea.hpp"
#include "lqt_bind_QAction.hpp"
#include "lqt_bind_QApplication.hpp"
#include "lqt_bind_QCoreApplication.hpp"
#include "lqt_bind_QColor.hpp"
#include "lqt_bind_QEvent.hpp"
#include "lqt_bind_QFileDialog.hpp"
#include "lqt_bind_QFrame.hpp"
#include "lqt_bind_QFont.hpp"
#include "lqt_bind_QIcon.hpp"
#include "lqt_bind_QInputEvent.hpp"
#include "lqt_bind_QKeyEvent.hpp"
#include "lqt_bind_QKeySequence.hpp"
#include "lqt_bind_QLabel.hpp"
#include "lqt_bind_QLineEdit.hpp"
#include "lqt_bind_QMainWindow.hpp"
#include "lqt_bind_QMessageBox.hpp"
#include "lqt_bind_QMenu.hpp"
#include "lqt_bind_QMenuBar.hpp"
#include "lqt_bind_QObject.hpp"
#include "lqt_bind_QPaintDevice.hpp"
#include "lqt_bind_QShortcut.hpp"
#include "lqt_bind_QSize.hpp"
#include "lqt_bind_QSyntaxHighlighter.hpp"
#include "lqt_bind_QTextBlockUserData.hpp"
#include "lqt_bind_QTextCharFormat.hpp"
#include "lqt_bind_QTextCursor.hpp"
#include "lqt_bind_QTextDocument.hpp"
#include "lqt_bind_QTextEdit.hpp"
#include "lqt_bind_QToolBar.hpp"
#include "lqt_bind_QWidget.hpp"
#include "lqt_bind_QVBoxLayout.hpp"
#include "lqt_bind_QBoxLayout.hpp"
#include "lqt_bind_QLayoutItem.hpp"
#include "lqt_bind_QLayout.hpp"
#include "lqt_bind_Qt.hpp"
#include "lqt_bind_QGraphicsView.hpp"
#include "lqt_bind_QGraphicsScene.hpp"
#include "lqt_bind_QGraphicsItem.hpp"

static int qt_connect (lua_State *L) {
	QObject::connect(
			static_cast<QObject*>(lqtL_toudata(L, 1, "QObject*")),
			lua_tostring(L, 2),
			static_cast<QObject*>(lqtL_toudata(L, 3, "QObject*")),
			lua_tostring(L, 4));
	return 0;
}
static int qt_slot (lua_State *L) {
	lua_pushstring(L, "1");
	lua_pushvalue(L, 1);
	lua_concat(L, 2);
	return 1;
}

static int qt_signal (lua_State *L) {
	lua_pushstring(L, "2");
	lua_pushvalue(L, 1);
	lua_concat(L, 2);
	return 1;
}

static int retriever (lua_State *L) {
	lua_pushvalue(L, lua_upvalueindex(1));
	return 1;
}
static int derive (lua_State *L) {
	if (!lua_isuserdata(L, 1) || !lua_getmetatable(L, 1)) {
		lua_pushnil(L);
		lua_pushstring(L, "no userdata or no metatable given");
		return 2;
	}
	lua_getfield(L, -1, "__qtype");
	if (!lua_isstring(L, -1)) {
		lua_pushnil(L);
		lua_pushstring(L, "not a Qt type");
		return 2;
	}
	lua_insert(L, -2);
	lua_newtable(L);
	lua_insert(L, -3);
	lua_settable(L, -3);
	lua_newtable(L);
	lua_insert(L, -2);
	lua_setfield(L, -2, "__base");
	lua_pushcfunction(L, lqtL_index);
	lua_setfield(L, -2, "__index");
	lua_pushcfunction(L, lqtL_newindex);
	lua_setfield(L, -2, "__newindex");
	lua_setmetatable(L, 1);
	return 0;
}

static int slot_from_function (lua_State *L) {
	lua_pushvalue(L, 1);
	LuaFunction *f = new LuaFunction(L);
	f = 0;
	lua_pushstring(L, "1function()");
	return 2;
}

void push_relevant (lua_State *L) {
	lua_pushcfunction(L, derive);
	lua_setglobal(L, "derive");

	lua_pushcfunction(L, slot_from_function);
	lua_setglobal(L, "slot_from_function");

	lua_pushcfunction(L, qt_signal);
	lua_setglobal(L, "signal");

	lua_pushcfunction(L, qt_slot);
	lua_setglobal(L, "slot");

	lua_pushcfunction(L, qt_connect);
	lua_setglobal(L, "connect");

}


int main(int argc, char **argv) {
	int status = 0;
	//QApplication *app = new QApplication(argc, argv);

	lua_State *L = lua_open();
	luaL_openlibs(L);

	push_relevant(L);

	luaopen_QGraphicsView(L);
	luaopen_QGraphicsItem(L);
	luaopen_QGraphicsScene(L);
	luaopen_QAbstractScrollArea(L);
	luaopen_QAction(L);
	luaopen_QLayout(L);
	luaopen_QVBoxLayout(L);
	luaopen_QBoxLayout(L);
	luaopen_QApplication(L);
	luaopen_QCoreApplication(L);
	luaopen_QColor(L);
	luaopen_QEvent(L);
	luaopen_QFrame(L);
	luaopen_QFileDialog(L);
	luaopen_QFont(L);
	luaopen_QIcon(L);
	luaopen_QInputEvent(L);
	luaopen_QKeyEvent(L);
	luaopen_QKeySequence(L);
	luaopen_QLabel(L);
	luaopen_QLineEdit(L);
	luaopen_QMainWindow(L);
	luaopen_QMessageBox(L);
	luaopen_QMenu(L);
	luaopen_QMenuBar(L);
	luaopen_QObject(L);
	luaopen_QPaintDevice(L);
	luaopen_QShortcut(L);
	luaopen_QSize(L);
	luaopen_QSyntaxHighlighter(L);
	luaopen_QToolBar(L);
	luaopen_QTextBlockUserData(L);
	luaopen_QTextCursor(L);
	luaopen_QTextCharFormat(L);
	luaopen_QTextDocument(L);
	luaopen_QTextEdit(L);
	luaopen_QWidget(L);
	luaopen_Qt(L);

	luaL_getmetatable(L, "QAbstractScrollArea*");
	lua_setglobal(L, "QAbstractScrollArea");
	luaL_getmetatable(L, "QLayout*");
	lua_setglobal(L, "QLayout");
	luaL_getmetatable(L, "QVBoxLayout*");
	lua_setglobal(L, "QVBoxLayout");
	luaL_getmetatable(L, "QBoxLayout*");
	lua_setglobal(L, "QBoxLayout");
	luaL_getmetatable(L, "QApplication*");
	lua_setglobal(L, "QApplication");
	luaL_getmetatable(L, "QCoreApplication*");
	lua_setglobal(L, "QCoreApplication");
	luaL_getmetatable(L, "QEvent*");
	lua_setglobal(L, "QEvent");
	luaL_getmetatable(L, "QFrame*");
	lua_setglobal(L, "QFrame");
	luaL_getmetatable(L, "QFont*");
	lua_setglobal(L, "QFont");
	luaL_getmetatable(L, "QInputEvent*");
	lua_setglobal(L, "QInputEvent");
	luaL_getmetatable(L, "QKeyEvent*");
	lua_setglobal(L, "QKeyEvent");
	luaL_getmetatable(L, "QKeySequence*");
	lua_setglobal(L, "QKeySequence");
	luaL_getmetatable(L, "QLabel*");
	lua_setglobal(L, "QLabel");
	luaL_getmetatable(L, "QLineEdit*");
	lua_setglobal(L, "QLineEdit");
	luaL_getmetatable(L, "QMainWindow*");
	lua_setglobal(L, "QMainWindow");
	luaL_getmetatable(L, "QObject*");
	lua_setglobal(L, "QObject");
	luaL_getmetatable(L, "QPaintDevice*");
	lua_setglobal(L, "QPaintDevice");
	luaL_getmetatable(L, "QShortcut*");
	lua_setglobal(L, "QShortcut");
	luaL_getmetatable(L, "QTextCursor*");
	lua_setglobal(L, "QTextCursor");
	luaL_getmetatable(L, "QTextDocument*");
	lua_setglobal(L, "QTextDocument");
	luaL_getmetatable(L, "QTextEdit*");
	lua_setglobal(L, "QTextEdit");
	luaL_getmetatable(L, "QWidget*");
	lua_setglobal(L, "QWidget");

	if (luaL_dofile(L, "editor.lua")) {
    std::cerr << lua_tostring(L, 1) << std::endl;
		status = -1;
	}

	//if (status==0) status = app->exec();

	lua_close(L);

	return status;
}
