#include "lqt_bind_QTextEdit.hpp"

int LuaBinder< QTextEdit >::__LuaWrapCall__lineWrapColumnOrWidth (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QTextEdit::lineWrapColumnOrWidth();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__lineWrapMode (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextEdit::LineWrapMode ret = __lua__obj->QTextEdit::lineWrapMode();
  lqtL_pushenum(L, ret, "QTextEdit::LineWrapMode");
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__ensureCursorVisible (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QTextEdit::ensureCursorVisible();
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__cut (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QTextEdit::cut();
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__selectAll (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QTextEdit::selectAll();
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__copy (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QTextEdit::copy();
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__fontFamily (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QTextEdit::fontFamily();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__insertHtml (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QTextEdit::insertHtml(arg1);
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__fontWeight (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QTextEdit::fontWeight();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__setLineWrapMode (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextEdit::LineWrapMode arg1 = static_cast<QTextEdit::LineWrapMode>(lqtL_toenum(L, 2, "QTextEdit::LineWrapMode"));
  __lua__obj->QTextEdit::setLineWrapMode(arg1);
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__setAlignment (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<Qt::AlignmentFlag> arg1 = **static_cast<QFlags<Qt::AlignmentFlag>**>(lqtL_checkudata(L, 2, "QFlags<Qt::AlignmentFlag>*"));
  __lua__obj->QTextEdit::setAlignment(arg1);
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__isUndoRedoEnabled (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QTextEdit::isUndoRedoEnabled();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__setReadOnly (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QTextEdit::setReadOnly(arg1);
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__document (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextDocument * ret = __lua__obj->QTextEdit::document();
  lqtL_pushudata(L, ret, "QTextDocument*");
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__setDocumentTitle (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QTextEdit::setDocumentTitle(arg1);
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__documentTitle (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QTextEdit::documentTitle();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__setTextCursor (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QTextCursor& arg1 = **static_cast<QTextCursor**>(lqtL_checkudata(L, 2, "QTextCursor*"));
  __lua__obj->QTextEdit::setTextCursor(arg1);
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__setFontPointSize (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  __lua__obj->QTextEdit::setFontPointSize(arg1);
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__tabChangesFocus (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QTextEdit::tabChangesFocus();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__setWordWrapMode (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextOption::WrapMode arg1 = static_cast<QTextOption::WrapMode>(lqtL_toenum(L, 2, "QTextOption::WrapMode"));
  __lua__obj->QTextEdit::setWordWrapMode(arg1);
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__metaObject (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QMetaObject * ret = __lua__obj->QTextEdit::metaObject();
  lqtL_pushudata(L, ret, "QMetaObject*");
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__cursorWidth (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QTextEdit::cursorWidth();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__setDocument (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextDocument * arg1 = *static_cast<QTextDocument**>(lqtL_checkudata(L, 2, "QTextDocument*"));
  __lua__obj->QTextEdit::setDocument(arg1);
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__delete (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  delete __lua__obj;
  __lua__obj = 0;
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__isReadOnly (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QTextEdit::isReadOnly();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__textCursor (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextCursor ret = __lua__obj->QTextEdit::textCursor();
  lqtL_passudata(L, new QTextCursor(ret), "QTextCursor*");
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__setTextInteractionFlags (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<Qt::TextInteractionFlag> arg1 = **static_cast<QFlags<Qt::TextInteractionFlag>**>(lqtL_checkudata(L, 2, "QFlags<Qt::TextInteractionFlag>*"));
  __lua__obj->QTextEdit::setTextInteractionFlags(arg1);
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__zoomIn (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_isnumber(L, 2)?lua_tointeger(L, 2):static_cast< int >(1);
  __lua__obj->QTextEdit::zoomIn(arg1);
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__undo (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QTextEdit::undo();
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__paste (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QTextEdit::paste();
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__fontUnderline (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QTextEdit::fontUnderline();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__textColor (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QColor ret = __lua__obj->QTextEdit::textColor();
  lqtL_passudata(L, new QColor(ret), "QColor*");
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__setFontFamily (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QTextEdit::setFontFamily(arg1);
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__createStandardContextMenu (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QMenu * ret = __lua__obj->QTextEdit::createStandardContextMenu();
  lqtL_pushudata(L, ret, "QMenu*");
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__setFontItalic (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QTextEdit::setFontItalic(arg1);
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__print (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QPrinter * arg1 = *static_cast<QPrinter**>(lqtL_checkudata(L, 2, "QPrinter*"));
  __lua__obj->QTextEdit::print(arg1);
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__cursorForPosition (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPoint& arg1 = **static_cast<QPoint**>(lqtL_checkudata(L, 2, "QPoint*"));
  QTextCursor ret = __lua__obj->QTextEdit::cursorForPosition(arg1);
  lqtL_passudata(L, new QTextCursor(ret), "QTextCursor*");
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__autoFormatting (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<QTextEdit::AutoFormattingFlag> ret = __lua__obj->QTextEdit::autoFormatting();
  lqtL_passudata(L, new QFlags<QTextEdit::AutoFormattingFlag>(ret), "QFlags<QTextEdit::AutoFormattingFlag>*");
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__fontItalic (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QTextEdit::fontItalic();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QTextEdit::trUtf8(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QTextEdit::trUtf8(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__trUtf8 (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0xe88350;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0xa79270;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x531820;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x62b190;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xa17370;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__trUtf8__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__trUtf8__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__trUtf8 matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__setTabStopWidth (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QTextEdit::setTabStopWidth(arg1);
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__setUndoRedoEnabled (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QTextEdit::setUndoRedoEnabled(arg1);
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__currentCharFormat (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextCharFormat ret = __lua__obj->QTextEdit::currentCharFormat();
  lqtL_passudata(L, new QTextCharFormat(ret), "QTextCharFormat*");
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__find (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  QFlags<QTextDocument::FindFlag> arg2 = lqtL_testudata(L, 3, "QFlags<QTextDocument::FindFlag>*")?**static_cast<QFlags<QTextDocument::FindFlag>**>(lqtL_checkudata(L, 3, "QFlags<QTextDocument::FindFlag>*")):static_cast< QFlags<QTextDocument::FindFlag> >(0);
  bool ret = __lua__obj->QTextEdit::find(arg1, arg2);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__textInteractionFlags (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<Qt::TextInteractionFlag> ret = __lua__obj->QTextEdit::textInteractionFlags();
  lqtL_passudata(L, new QFlags<Qt::TextInteractionFlag>(ret), "QFlags<Qt::TextInteractionFlag>*");
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__setCurrentFont (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QFont& arg1 = **static_cast<QFont**>(lqtL_checkudata(L, 2, "QFont*"));
  __lua__obj->QTextEdit::setCurrentFont(arg1);
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__mergeCurrentCharFormat (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QTextCharFormat& arg1 = **static_cast<QTextCharFormat**>(lqtL_checkudata(L, 2, "QTextCharFormat*"));
  __lua__obj->QTextEdit::mergeCurrentCharFormat(arg1);
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__scrollToAnchor (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QTextEdit::scrollToAnchor(arg1);
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__append (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QTextEdit::append(arg1);
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__tabStopWidth (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QTextEdit::tabStopWidth();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__loadResource (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  const QUrl& arg2 = **static_cast<QUrl**>(lqtL_checkudata(L, 3, "QUrl*"));
  QVariant ret = __lua__obj->QTextEdit::loadResource(arg1, arg2);
  lqtL_passudata(L, new QVariant(ret), "QVariant*");
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__setLineWrapColumnOrWidth (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QTextEdit::setLineWrapColumnOrWidth(arg1);
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__clear (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QTextEdit::clear();
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__setTabChangesFocus (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QTextEdit::setTabChangesFocus(arg1);
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__redo (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QTextEdit::redo();
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__extraSelections (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QList<QTextEdit::ExtraSelection> ret = __lua__obj->QTextEdit::extraSelections();
  lqtL_passudata(L, new QList<QTextEdit::ExtraSelection>(ret), "QList<QTextEdit::ExtraSelection>*");
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__acceptRichText (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QTextEdit::acceptRichText();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__toPlainText (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QTextEdit::toPlainText();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__setExtraSelections (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QList<QTextEdit::ExtraSelection>& arg1 = **static_cast<QList<QTextEdit::ExtraSelection>**>(lqtL_checkudata(L, 2, "QList<QTextEdit::ExtraSelection>*"));
  __lua__obj->QTextEdit::setExtraSelections(arg1);
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__currentFont (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFont ret = __lua__obj->QTextEdit::currentFont();
  lqtL_passudata(L, new QFont(ret), "QFont*");
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__setAcceptRichText (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QTextEdit::setAcceptRichText(arg1);
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__wordWrapMode (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextOption::WrapMode ret = __lua__obj->QTextEdit::wordWrapMode();
  lqtL_pushenum(L, ret, "QTextOption::WrapMode");
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__setAutoFormatting (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<QTextEdit::AutoFormattingFlag> arg1 = **static_cast<QFlags<QTextEdit::AutoFormattingFlag>**>(lqtL_checkudata(L, 2, "QFlags<QTextEdit::AutoFormattingFlag>*"));
  __lua__obj->QTextEdit::setAutoFormatting(arg1);
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__setCurrentCharFormat (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QTextCharFormat& arg1 = **static_cast<QTextCharFormat**>(lqtL_checkudata(L, 2, "QTextCharFormat*"));
  __lua__obj->QTextEdit::setCurrentCharFormat(arg1);
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__setPlainText (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QTextEdit::setPlainText(arg1);
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__setTextColor (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QColor& arg1 = **static_cast<QColor**>(lqtL_checkudata(L, 2, "QColor*"));
  __lua__obj->QTextEdit::setTextColor(arg1);
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__fontPointSize (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double ret = __lua__obj->QTextEdit::fontPointSize();
  lua_pushnumber(L, ret);
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__new__OverloadedVersion__1 (lua_State *L) {
  QWidget * arg1 = lqtL_testudata(L, 1, "QWidget*")?*static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*")):static_cast< QWidget * >(0);
  QTextEdit * ret = new LuaBinder< QTextEdit >(L, arg1);
  lqtL_passudata(L, ret, "QTextEdit*");
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__new__OverloadedVersion__2 (lua_State *L) {
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 1), lua_objlen(L, 1));
  QWidget * arg2 = lqtL_testudata(L, 2, "QWidget*")?*static_cast<QWidget**>(lqtL_checkudata(L, 2, "QWidget*")):static_cast< QWidget * >(0);
  QTextEdit * ret = new LuaBinder< QTextEdit >(L, arg1, arg2);
  lqtL_passudata(L, ret, "QTextEdit*");
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__new (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if (lqtL_testudata(L, 1, "QWidget*")) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0xef3cc0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x9f7440;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_testudata(L, 2, "QWidget*")) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0xd5ab60;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__new__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__new__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__new matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__setFontUnderline (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QTextEdit::setFontUnderline(arg1);
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__canPaste (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QTextEdit::canPaste();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__anchorAt (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPoint& arg1 = **static_cast<QPoint**>(lqtL_checkudata(L, 2, "QPoint*"));
  QString ret = __lua__obj->QTextEdit::anchorAt(arg1);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__toHtml (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QTextEdit::toHtml();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QTextEdit::tr(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QTextEdit::tr(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__tr (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0xc706b0;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0xedb230;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x9f2600;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x9e80c0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0xd6d4e0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__tr__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__tr__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__tr matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__setFontWeight (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QTextEdit::setFontWeight(arg1);
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__zoomOut (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_isnumber(L, 2)?lua_tointeger(L, 2):static_cast< int >(1);
  __lua__obj->QTextEdit::zoomOut(arg1);
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__setHtml (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QTextEdit::setHtml(arg1);
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__setCursorWidth (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QTextEdit::setCursorWidth(arg1);
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__overwriteMode (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QTextEdit::overwriteMode();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__alignment (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<Qt::AlignmentFlag> ret = __lua__obj->QTextEdit::alignment();
  lqtL_passudata(L, new QFlags<Qt::AlignmentFlag>(ret), "QFlags<Qt::AlignmentFlag>*");
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__setText (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QTextEdit::setText(arg1);
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__setOverwriteMode (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QTextEdit::setOverwriteMode(arg1);
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__cursorRect__OverloadedVersion__1 (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QTextCursor& arg1 = **static_cast<QTextCursor**>(lqtL_checkudata(L, 2, "QTextCursor*"));
  QRect ret = __lua__obj->QTextEdit::cursorRect(arg1);
  lqtL_passudata(L, new QRect(ret), "QRect*");
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__cursorRect__OverloadedVersion__2 (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QRect ret = __lua__obj->QTextEdit::cursorRect();
  lqtL_passudata(L, new QRect(ret), "QRect*");
  return 1;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__cursorRect (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QTextEdit*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QTextCursor*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x9dd0b0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QTextEdit*")?premium:-premium*premium;
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__cursorRect__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__cursorRect__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__cursorRect matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__insertPlainText (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QTextEdit::insertPlainText(arg1);
  return 0;
}
int LuaBinder< QTextEdit >::__LuaWrapCall__moveCursor (lua_State *L) {
  QTextEdit *& __lua__obj = *static_cast<QTextEdit**>(lqtL_checkudata(L, 1, "QTextEdit*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QTextCursor::MoveOperation arg1 = static_cast<QTextCursor::MoveOperation>(lqtL_toenum(L, 2, "QTextCursor::MoveOperation"));
  QTextCursor::MoveMode arg2 = lqtL_isenum(L, 3, "QTextCursor::MoveMode")?static_cast<QTextCursor::MoveMode>(lqtL_toenum(L, 3, "QTextCursor::MoveMode")):QTextCursor::QTextCursor::MoveAnchor;
  __lua__obj->QTextEdit::moveCursor(arg1, arg2);
  return 0;
}
void LuaBinder< QTextEdit >::styleChange (QStyle& arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "styleChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QStyle*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::styleChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QTextEdit >::focusInEvent (QFocusEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "focusInEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QFocusEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QTextEdit::focusInEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QTextEdit >::keyPressEvent (QKeyEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "keyPressEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QKeyEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QTextEdit::keyPressEvent(arg1);
  }
  lua_settop(L, oldtop);
}
const QMetaObject * LuaBinder< QTextEdit >::metaObject () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "metaObject");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QTextEdit::metaObject();
  }
  const QMetaObject * ret = *static_cast<QMetaObject**>(lqtL_checkudata(L, -1, "QMetaObject*"));
  lua_settop(L, oldtop);
  return ret;
}
int LuaBinder< QTextEdit >::devType () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "devType");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::devType();
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QTextEdit >::setVisible (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "setVisible");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::setVisible(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QTextEdit >::keyReleaseEvent (QKeyEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "keyReleaseEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QKeyEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QTextEdit::keyReleaseEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QTextEdit >::scrollContentsBy (int arg1, int arg2) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "scrollContentsBy");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushinteger(L, arg1);
  lua_pushinteger(L, arg2);
  if (lua_isfunction(L, -2-2)) {
    lua_pcall(L, 2+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QTextEdit::scrollContentsBy(arg1, arg2);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QTextEdit >::dragLeaveEvent (QDragLeaveEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dragLeaveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDragLeaveEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QTextEdit::dragLeaveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QTextEdit >::mousePressEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mousePressEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QTextEdit::mousePressEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QTextEdit >::customEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "customEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::customEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QTextEdit >::dragMoveEvent (QDragMoveEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dragMoveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDragMoveEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QTextEdit::dragMoveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
QSize LuaBinder< QTextEdit >::sizeHint () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "sizeHint");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QAbstractScrollArea::sizeHint();
  }
  QSize ret = **static_cast<QSize**>(lqtL_checkudata(L, -1, "QSize*"));
  lua_settop(L, oldtop);
  return ret;
}
bool LuaBinder< QTextEdit >::canInsertFromMimeData (const QMimeData * arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "canInsertFromMimeData");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMimeData*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QTextEdit::canInsertFromMimeData(arg1);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QTextEdit >::changeEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "changeEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QTextEdit::changeEvent(arg1);
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QTextEdit >::viewportEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "viewportEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QAbstractScrollArea::viewportEvent(arg1);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QTextEdit >::mouseReleaseEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mouseReleaseEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QTextEdit::mouseReleaseEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QTextEdit >::tabletEvent (QTabletEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "tabletEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QTabletEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::tabletEvent(arg1);
  }
  lua_settop(L, oldtop);
}
QPaintEngine * LuaBinder< QTextEdit >::paintEngine () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "paintEngine");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::paintEngine();
  }
  QPaintEngine * ret = *static_cast<QPaintEngine**>(lqtL_checkudata(L, -1, "QPaintEngine*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QTextEdit >::closeEvent (QCloseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "closeEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QCloseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::closeEvent(arg1);
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QTextEdit >::focusNextPrevChild (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "focusNextPrevChild");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QTextEdit::focusNextPrevChild(arg1);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QTextEdit >::dropEvent (QDropEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dropEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDropEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QTextEdit::dropEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QTextEdit >::fontChange (const QFont& arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "fontChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QFont*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::fontChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QTextEdit >::insertFromMimeData (const QMimeData * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "insertFromMimeData");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMimeData*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QTextEdit::insertFromMimeData(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QTextEdit >::showEvent (QShowEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "showEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QShowEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QTextEdit::showEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QTextEdit >::connectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "connectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::connectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QTextEdit >::paintEvent (QPaintEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "paintEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QPaintEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QTextEdit::paintEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QTextEdit >::windowActivationChange (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "windowActivationChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::windowActivationChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QTextEdit >::disconnectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "disconnectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::disconnectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
QMimeData * LuaBinder< QTextEdit >::createMimeDataFromSelection () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "createMimeDataFromSelection");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QTextEdit::createMimeDataFromSelection();
  }
  QMimeData * ret = *static_cast<QMimeData**>(lqtL_checkudata(L, -1, "QMimeData*"));
  lua_settop(L, oldtop);
  return ret;
}
int LuaBinder< QTextEdit >::metric (QPaintDevice::PaintDeviceMetric arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "metric");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushenum(L, arg1, "QPaintDevice::PaintDeviceMetric");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::metric(arg1);
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
int LuaBinder< QTextEdit >::heightForWidth (int arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "heightForWidth");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushinteger(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::heightForWidth(arg1);
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QTextEdit >::mouseDoubleClickEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mouseDoubleClickEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QTextEdit::mouseDoubleClickEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QTextEdit >::enabledChange (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "enabledChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::enabledChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QTextEdit >::timerEvent (QTimerEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "timerEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QTimerEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QTextEdit::timerEvent(arg1);
  }
  lua_settop(L, oldtop);
}
QVariant LuaBinder< QTextEdit >::loadResource (int arg1, const QUrl& arg2) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "loadResource");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushinteger(L, arg1);
  lqtL_pushudata(L, &(arg2), "QUrl*");
  if (lua_isfunction(L, -2-2)) {
    lua_pcall(L, 2+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QTextEdit::loadResource(arg1, arg2);
  }
  QVariant ret = **static_cast<QVariant**>(lqtL_checkudata(L, -1, "QVariant*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QTextEdit >::actionEvent (QActionEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "actionEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QActionEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::actionEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QTextEdit >::childEvent (QChildEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "childEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QChildEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::childEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QTextEdit >::focusOutEvent (QFocusEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "focusOutEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QFocusEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QTextEdit::focusOutEvent(arg1);
  }
  lua_settop(L, oldtop);
}
QSize LuaBinder< QTextEdit >::minimumSizeHint () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "minimumSizeHint");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QAbstractScrollArea::minimumSizeHint();
  }
  QSize ret = **static_cast<QSize**>(lqtL_checkudata(L, -1, "QSize*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QTextEdit >::moveEvent (QMoveEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "moveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMoveEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::moveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QTextEdit >::eventFilter (QObject * arg1, QEvent * arg2) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "eventFilter");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QObject*");
  lqtL_pushudata(L, arg2, "QEvent*");
  if (lua_isfunction(L, -2-2)) {
    lua_pcall(L, 2+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QObject::eventFilter(arg1, arg2);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QTextEdit >::inputMethodEvent (QInputMethodEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "inputMethodEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QInputMethodEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QTextEdit::inputMethodEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QTextEdit >::wheelEvent (QWheelEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "wheelEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QWheelEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QTextEdit::wheelEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QTextEdit >::resizeEvent (QResizeEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "resizeEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QResizeEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QTextEdit::resizeEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QTextEdit >::paletteChange (const QPalette& arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "paletteChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QPalette*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::paletteChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QTextEdit >::languageChange () {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "languageChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::languageChange();
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QTextEdit >::event (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "event");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QTextEdit::event(arg1);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QTextEdit >::hideEvent (QHideEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "hideEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QHideEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::hideEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QTextEdit >::contextMenuEvent (QContextMenuEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "contextMenuEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QContextMenuEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QTextEdit::contextMenuEvent(arg1);
  }
  lua_settop(L, oldtop);
}
QVariant LuaBinder< QTextEdit >::inputMethodQuery (Qt::InputMethodQuery arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "inputMethodQuery");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushenum(L, arg1, "Qt::InputMethodQuery");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QTextEdit::inputMethodQuery(arg1);
  }
  QVariant ret = **static_cast<QVariant**>(lqtL_checkudata(L, -1, "QVariant*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QTextEdit >::mouseMoveEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mouseMoveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QTextEdit::mouseMoveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QTextEdit >::leaveEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "leaveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::leaveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QTextEdit >::dragEnterEvent (QDragEnterEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dragEnterEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDragEnterEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QTextEdit::dragEnterEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QTextEdit >::enterEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "enterEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::enterEvent(arg1);
  }
  lua_settop(L, oldtop);
}
LuaBinder< QTextEdit >::  ~LuaBinder< QTextEdit > () {
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextEdit*");
  lua_getfield(L, -1, "~QTextEdit");

  if (lua_isfunction(L, -1)) {
    lua_insert(L, -2);
    lua_pcall(L, 1, 1, 0);
  } else {
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QTextEdit >::lqt_pushenum_LineWrapMode (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "NoWrap");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "NoWrap");
  lua_pushstring(L, "WidgetWidth");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "WidgetWidth");
  lua_pushstring(L, "FixedPixelWidth");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "FixedPixelWidth");
  lua_pushstring(L, "FixedColumnWidth");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "FixedColumnWidth");
  lua_pushcfunction(L, LuaBinder< QTextEdit >::lqt_pushenum_LineWrapMode_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QTextEdit::LineWrapMode");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QTextEdit >::lqt_pushenum_LineWrapMode_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QTextEdit::LineWrapMode>*) + sizeof(QFlags<QTextEdit::LineWrapMode>));
  QFlags<QTextEdit::LineWrapMode> *fl = static_cast<QFlags<QTextEdit::LineWrapMode>*>( static_cast<void*>(&static_cast<QFlags<QTextEdit::LineWrapMode>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QTextEdit::LineWrapMode>(lqtL_toenum(L, i, "QTextEdit::LineWrapMode"));
	}
	if (luaL_newmetatable(L, "QFlags<QTextEdit::LineWrapMode>*")) {
		lua_pushstring(L, "QFlags<QTextEdit::LineWrapMode>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinder< QTextEdit >::lqt_pushenum_AutoFormattingFlag (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "AutoNone");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "AutoNone");
  lua_pushstring(L, "AutoBulletList");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "AutoBulletList");
  lua_pushstring(L, "AutoAll");
  lua_rawseti(L, enum_table, -1);
  lua_pushinteger(L, -1);
  lua_setfield(L, enum_table, "AutoAll");
  lua_pushcfunction(L, LuaBinder< QTextEdit >::lqt_pushenum_AutoFormattingFlag_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QTextEdit::AutoFormattingFlag");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QTextEdit >::lqt_pushenum_AutoFormattingFlag_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QTextEdit::AutoFormattingFlag>*) + sizeof(QFlags<QTextEdit::AutoFormattingFlag>));
  QFlags<QTextEdit::AutoFormattingFlag> *fl = static_cast<QFlags<QTextEdit::AutoFormattingFlag>*>( static_cast<void*>(&static_cast<QFlags<QTextEdit::AutoFormattingFlag>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QTextEdit::AutoFormattingFlag>(lqtL_toenum(L, i, "QTextEdit::AutoFormattingFlag"));
	}
	if (luaL_newmetatable(L, "QFlags<QTextEdit::AutoFormattingFlag>*")) {
		lua_pushstring(L, "QFlags<QTextEdit::AutoFormattingFlag>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int luaopen_QTextEdit (lua_State *L) {
  if (luaL_newmetatable(L, "QTextEdit*")) {
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__lineWrapColumnOrWidth);
    lua_setfield(L, -2, "lineWrapColumnOrWidth");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__lineWrapMode);
    lua_setfield(L, -2, "lineWrapMode");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__ensureCursorVisible);
    lua_setfield(L, -2, "ensureCursorVisible");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__cut);
    lua_setfield(L, -2, "cut");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__selectAll);
    lua_setfield(L, -2, "selectAll");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__copy);
    lua_setfield(L, -2, "copy");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__fontFamily);
    lua_setfield(L, -2, "fontFamily");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__insertHtml);
    lua_setfield(L, -2, "insertHtml");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__fontWeight);
    lua_setfield(L, -2, "fontWeight");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__setLineWrapMode);
    lua_setfield(L, -2, "setLineWrapMode");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__setAlignment);
    lua_setfield(L, -2, "setAlignment");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__isUndoRedoEnabled);
    lua_setfield(L, -2, "isUndoRedoEnabled");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__setReadOnly);
    lua_setfield(L, -2, "setReadOnly");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__document);
    lua_setfield(L, -2, "document");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__setDocumentTitle);
    lua_setfield(L, -2, "setDocumentTitle");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__documentTitle);
    lua_setfield(L, -2, "documentTitle");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__setTextCursor);
    lua_setfield(L, -2, "setTextCursor");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__setFontPointSize);
    lua_setfield(L, -2, "setFontPointSize");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__tabChangesFocus);
    lua_setfield(L, -2, "tabChangesFocus");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__setWordWrapMode);
    lua_setfield(L, -2, "setWordWrapMode");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__metaObject);
    lua_setfield(L, -2, "metaObject");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__cursorWidth);
    lua_setfield(L, -2, "cursorWidth");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__setDocument);
    lua_setfield(L, -2, "setDocument");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__delete);
    lua_setfield(L, -2, "delete");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__isReadOnly);
    lua_setfield(L, -2, "isReadOnly");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__textCursor);
    lua_setfield(L, -2, "textCursor");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__setTextInteractionFlags);
    lua_setfield(L, -2, "setTextInteractionFlags");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__zoomIn);
    lua_setfield(L, -2, "zoomIn");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__undo);
    lua_setfield(L, -2, "undo");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__paste);
    lua_setfield(L, -2, "paste");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__fontUnderline);
    lua_setfield(L, -2, "fontUnderline");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__textColor);
    lua_setfield(L, -2, "textColor");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__setFontFamily);
    lua_setfield(L, -2, "setFontFamily");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__createStandardContextMenu);
    lua_setfield(L, -2, "createStandardContextMenu");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__setFontItalic);
    lua_setfield(L, -2, "setFontItalic");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__print);
    lua_setfield(L, -2, "print");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__cursorForPosition);
    lua_setfield(L, -2, "cursorForPosition");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__autoFormatting);
    lua_setfield(L, -2, "autoFormatting");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__fontItalic);
    lua_setfield(L, -2, "fontItalic");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__trUtf8);
    lua_setfield(L, -2, "trUtf8");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__setTabStopWidth);
    lua_setfield(L, -2, "setTabStopWidth");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__setUndoRedoEnabled);
    lua_setfield(L, -2, "setUndoRedoEnabled");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__currentCharFormat);
    lua_setfield(L, -2, "currentCharFormat");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__find);
    lua_setfield(L, -2, "find");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__textInteractionFlags);
    lua_setfield(L, -2, "textInteractionFlags");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__setCurrentFont);
    lua_setfield(L, -2, "setCurrentFont");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__mergeCurrentCharFormat);
    lua_setfield(L, -2, "mergeCurrentCharFormat");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__scrollToAnchor);
    lua_setfield(L, -2, "scrollToAnchor");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__append);
    lua_setfield(L, -2, "append");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__tabStopWidth);
    lua_setfield(L, -2, "tabStopWidth");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__loadResource);
    lua_setfield(L, -2, "loadResource");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__setLineWrapColumnOrWidth);
    lua_setfield(L, -2, "setLineWrapColumnOrWidth");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__clear);
    lua_setfield(L, -2, "clear");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__setTabChangesFocus);
    lua_setfield(L, -2, "setTabChangesFocus");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__redo);
    lua_setfield(L, -2, "redo");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__extraSelections);
    lua_setfield(L, -2, "extraSelections");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__acceptRichText);
    lua_setfield(L, -2, "acceptRichText");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__toPlainText);
    lua_setfield(L, -2, "toPlainText");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__setExtraSelections);
    lua_setfield(L, -2, "setExtraSelections");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__currentFont);
    lua_setfield(L, -2, "currentFont");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__setAcceptRichText);
    lua_setfield(L, -2, "setAcceptRichText");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__wordWrapMode);
    lua_setfield(L, -2, "wordWrapMode");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__setAutoFormatting);
    lua_setfield(L, -2, "setAutoFormatting");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__setCurrentCharFormat);
    lua_setfield(L, -2, "setCurrentCharFormat");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__setPlainText);
    lua_setfield(L, -2, "setPlainText");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__setTextColor);
    lua_setfield(L, -2, "setTextColor");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__fontPointSize);
    lua_setfield(L, -2, "fontPointSize");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__new);
    lua_setfield(L, -2, "new");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__setFontUnderline);
    lua_setfield(L, -2, "setFontUnderline");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__canPaste);
    lua_setfield(L, -2, "canPaste");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__anchorAt);
    lua_setfield(L, -2, "anchorAt");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__toHtml);
    lua_setfield(L, -2, "toHtml");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__tr);
    lua_setfield(L, -2, "tr");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__setFontWeight);
    lua_setfield(L, -2, "setFontWeight");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__zoomOut);
    lua_setfield(L, -2, "zoomOut");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__setHtml);
    lua_setfield(L, -2, "setHtml");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__setCursorWidth);
    lua_setfield(L, -2, "setCursorWidth");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__overwriteMode);
    lua_setfield(L, -2, "overwriteMode");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__alignment);
    lua_setfield(L, -2, "alignment");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__setText);
    lua_setfield(L, -2, "setText");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__setOverwriteMode);
    lua_setfield(L, -2, "setOverwriteMode");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__cursorRect);
    lua_setfield(L, -2, "cursorRect");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__insertPlainText);
    lua_setfield(L, -2, "insertPlainText");
    lua_pushcfunction(L, LuaBinder< QTextEdit >::__LuaWrapCall__moveCursor);
    lua_setfield(L, -2, "moveCursor");
    LuaBinder< QTextEdit >::lqt_pushenum_LineWrapMode(L);
    lua_setfield(L, -2, "LineWrapMode");
    LuaBinder< QTextEdit >::lqt_pushenum_AutoFormattingFlag(L);
    lua_setfield(L, -2, "AutoFormattingFlag");
    lua_newtable(L);
    lua_pushboolean(L, 1);
    lua_setfield(L, -2, "QAbstractScrollArea*");
    lua_pushboolean(L, 0);
    lua_setfield(L, -2, "QObject*");
    lua_pushboolean(L, 0);
    lua_setfield(L, -2, "QPaintDevice*");
    lua_pushboolean(L, 0);
    lua_setfield(L, -2, "QWidget*");
    lua_pushboolean(L, 0);
    lua_setfield(L, -2, "QFrame*");
    lua_setfield(L, -2, "__base");
    lua_pushcfunction(L, lqtL_newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, lqtL_index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, lqtL_gc);
    lua_setfield(L, -2, "__gc");
    lua_pushstring(L, "QTextEdit");
    lua_setfield(L, -2, "__qtype");
		lua_setglobal(L, "QTextEdit");
  } else {
		lua_pop(L, 1);
	}
  return 0;
}
