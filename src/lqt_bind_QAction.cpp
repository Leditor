#include "lqt_bind_QAction.hpp"

int LuaBinder< QAction >::__LuaWrapCall__parentWidget (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * ret = __lua__obj->QAction::parentWidget();
  lqtL_pushudata(L, ret, "QWidget*");
  return 1;
}
int LuaBinder< QAction >::__LuaWrapCall__setChecked (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QAction::setChecked(arg1);
  return 0;
}
int LuaBinder< QAction >::__LuaWrapCall__statusTip (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QAction::statusTip();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QAction >::__LuaWrapCall__setVisible (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QAction::setVisible(arg1);
  return 0;
}
int LuaBinder< QAction >::__LuaWrapCall__setIcon (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QIcon& arg1 = **static_cast<QIcon**>(lqtL_checkudata(L, 2, "QIcon*"));
  __lua__obj->QAction::setIcon(arg1);
  return 0;
}
int LuaBinder< QAction >::__LuaWrapCall__isSeparator (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QAction::isSeparator();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QAction >::__LuaWrapCall__toggle (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QAction::toggle();
  return 0;
}
int LuaBinder< QAction >::__LuaWrapCall__icon (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QIcon ret = __lua__obj->QAction::icon();
  lqtL_passudata(L, new QIcon(ret), "QIcon*");
  return 1;
}
int LuaBinder< QAction >::__LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QAction::tr(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QAction >::__LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QAction::tr(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QAction >::__LuaWrapCall__tr (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x143c9d0;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x143c740;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x143e400;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x143e8f0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x143eca0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__tr__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__tr__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__tr matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QAction >::__LuaWrapCall__isVisible (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QAction::isVisible();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QAction >::__LuaWrapCall__whatsThis (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QAction::whatsThis();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QAction >::__LuaWrapCall__toolTip (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QAction::toolTip();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QAction >::__LuaWrapCall__setIconText (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QAction::setIconText(arg1);
  return 0;
}
int LuaBinder< QAction >::__LuaWrapCall__shortcut (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QKeySequence ret = __lua__obj->QAction::shortcut();
  lqtL_passudata(L, new QKeySequence(ret), "QKeySequence*");
  return 1;
}
int LuaBinder< QAction >::__LuaWrapCall__iconText (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QAction::iconText();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QAction >::__LuaWrapCall__shortcutContext (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::ShortcutContext ret = __lua__obj->QAction::shortcutContext();
  lqtL_pushenum(L, ret, "Qt::ShortcutContext");
  return 1;
}
int LuaBinder< QAction >::__LuaWrapCall__setToolTip (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QAction::setToolTip(arg1);
  return 0;
}
int LuaBinder< QAction >::__LuaWrapCall__setMenu (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QMenu * arg1 = *static_cast<QMenu**>(lqtL_checkudata(L, 2, "QMenu*"));
  __lua__obj->QAction::setMenu(arg1);
  return 0;
}
int LuaBinder< QAction >::__LuaWrapCall__showStatusText (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * arg1 = lqtL_testudata(L, 2, "QWidget*")?*static_cast<QWidget**>(lqtL_checkudata(L, 2, "QWidget*")):static_cast< QWidget * >(0);
  bool ret = __lua__obj->QAction::showStatusText(arg1);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QAction >::__LuaWrapCall__setAutoRepeat (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QAction::setAutoRepeat(arg1);
  return 0;
}
int LuaBinder< QAction >::__LuaWrapCall__setCheckable (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QAction::setCheckable(arg1);
  return 0;
}
int LuaBinder< QAction >::__LuaWrapCall__setSeparator (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QAction::setSeparator(arg1);
  return 0;
}
int LuaBinder< QAction >::__LuaWrapCall__activate (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QAction::ActionEvent arg1 = static_cast<QAction::ActionEvent>(lqtL_toenum(L, 2, "QAction::ActionEvent"));
  __lua__obj->QAction::activate(arg1);
  return 0;
}
int LuaBinder< QAction >::__LuaWrapCall__trigger (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QAction::trigger();
  return 0;
}
int LuaBinder< QAction >::__LuaWrapCall__shortcuts (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QList<QKeySequence> ret = __lua__obj->QAction::shortcuts();
  lqtL_passudata(L, new QList<QKeySequence>(ret), "QList<QKeySequence>*");
  return 1;
}
int LuaBinder< QAction >::__LuaWrapCall__associatedWidgets (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QList<QWidget*> ret = __lua__obj->QAction::associatedWidgets();
  lqtL_passudata(L, new QList<QWidget*>(ret), "QList<QWidget*>*");
  return 1;
}
int LuaBinder< QAction >::__LuaWrapCall__setText (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QAction::setText(arg1);
  return 0;
}
int LuaBinder< QAction >::__LuaWrapCall__data (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QVariant ret = __lua__obj->QAction::data();
  lqtL_passudata(L, new QVariant(ret), "QVariant*");
  return 1;
}
int LuaBinder< QAction >::__LuaWrapCall__isCheckable (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QAction::isCheckable();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QAction >::__LuaWrapCall__setStatusTip (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QAction::setStatusTip(arg1);
  return 0;
}
int LuaBinder< QAction >::__LuaWrapCall__setDisabled (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QAction::setDisabled(arg1);
  return 0;
}
int LuaBinder< QAction >::__LuaWrapCall__setShortcutContext (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::ShortcutContext arg1 = static_cast<Qt::ShortcutContext>(lqtL_toenum(L, 2, "Qt::ShortcutContext"));
  __lua__obj->QAction::setShortcutContext(arg1);
  return 0;
}
int LuaBinder< QAction >::__LuaWrapCall__setActionGroup (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QActionGroup * arg1 = *static_cast<QActionGroup**>(lqtL_checkudata(L, 2, "QActionGroup*"));
  __lua__obj->QAction::setActionGroup(arg1);
  return 0;
}
int LuaBinder< QAction >::__LuaWrapCall__autoRepeat (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QAction::autoRepeat();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QAction >::__LuaWrapCall__setFont (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QFont& arg1 = **static_cast<QFont**>(lqtL_checkudata(L, 2, "QFont*"));
  __lua__obj->QAction::setFont(arg1);
  return 0;
}
int LuaBinder< QAction >::__LuaWrapCall__metaObject (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QMetaObject * ret = __lua__obj->QAction::metaObject();
  lqtL_pushudata(L, ret, "QMetaObject*");
  return 1;
}
int LuaBinder< QAction >::__LuaWrapCall__new__OverloadedVersion__1 (lua_State *L) {
  QObject * arg1 = *static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*"));
  QAction * ret = new LuaBinder< QAction >(L, arg1);
  lqtL_passudata(L, ret, "QAction*");
  return 1;
}
int LuaBinder< QAction >::__LuaWrapCall__new__OverloadedVersion__2 (lua_State *L) {
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 1), lua_objlen(L, 1));
  QObject * arg2 = *static_cast<QObject**>(lqtL_checkudata(L, 2, "QObject*"));
  QAction * ret = new LuaBinder< QAction >(L, arg1, arg2);
  lqtL_passudata(L, ret, "QAction*");
  return 1;
}
int LuaBinder< QAction >::__LuaWrapCall__new__OverloadedVersion__3 (lua_State *L) {
  const QIcon& arg1 = **static_cast<QIcon**>(lqtL_checkudata(L, 1, "QIcon*"));
  const QString& arg2 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  QObject * arg3 = *static_cast<QObject**>(lqtL_checkudata(L, 3, "QObject*"));
  QAction * ret = new LuaBinder< QAction >(L, arg1, arg2, arg3);
  lqtL_passudata(L, ret, "QAction*");
  return 1;
}
int LuaBinder< QAction >::__LuaWrapCall__new (lua_State *L) {
  int score[4];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if (lqtL_testudata(L, 1, "QObject*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x14423d0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1442df0;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_testudata(L, 2, "QObject*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x14428e0;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  if (lqtL_testudata(L, 1, "QIcon*")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1443c50;
  } else {
    score[3] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x14436c0;
  } else {
    score[3] -= premium*premium;
  }
  if (lqtL_testudata(L, 3, "QObject*")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1444100;
  } else {
    score[3] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=3;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__new__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__new__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__new__OverloadedVersion__3(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__new matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QAction >::__LuaWrapCall__hover (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QAction::hover();
  return 0;
}
int LuaBinder< QAction >::__LuaWrapCall__delete (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  delete __lua__obj;
  __lua__obj = 0;
  return 0;
}
int LuaBinder< QAction >::__LuaWrapCall__font (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFont ret = __lua__obj->QAction::font();
  lqtL_passudata(L, new QFont(ret), "QFont*");
  return 1;
}
int LuaBinder< QAction >::__LuaWrapCall__actionGroup (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QActionGroup * ret = __lua__obj->QAction::actionGroup();
  lqtL_pushudata(L, ret, "QActionGroup*");
  return 1;
}
int LuaBinder< QAction >::__LuaWrapCall__setShortcut (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QKeySequence& arg1 = **static_cast<QKeySequence**>(lqtL_checkudata(L, 2, "QKeySequence*"));
  __lua__obj->QAction::setShortcut(arg1);
  return 0;
}
int LuaBinder< QAction >::__LuaWrapCall__setShortcuts__OverloadedVersion__1 (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QList<QKeySequence>& arg1 = **static_cast<QList<QKeySequence>**>(lqtL_checkudata(L, 2, "QList<QKeySequence>*"));
  __lua__obj->QAction::setShortcuts(arg1);
  return 0;
}
int LuaBinder< QAction >::__LuaWrapCall__setShortcuts__OverloadedVersion__2 (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QKeySequence::StandardKey arg1 = static_cast<QKeySequence::StandardKey>(lqtL_toenum(L, 2, "QKeySequence::StandardKey"));
  __lua__obj->QAction::setShortcuts(arg1);
  return 0;
}
int LuaBinder< QAction >::__LuaWrapCall__setShortcuts (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QAction*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QList<QKeySequence>*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1450510;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QAction*")?premium:-premium*premium;
  if (lqtL_isenum(L, 2, "QKeySequence::StandardKey")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1451050;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__setShortcuts__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__setShortcuts__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__setShortcuts matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QAction >::__LuaWrapCall__isChecked (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QAction::isChecked();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QAction >::__LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QAction::trUtf8(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QAction >::__LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QAction::trUtf8(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QAction >::__LuaWrapCall__trUtf8 (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x143d730;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x143d290;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x143f510;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x143ec40;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x143f9a0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__trUtf8__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__trUtf8__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__trUtf8 matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QAction >::__LuaWrapCall__setMenuRole (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QAction::MenuRole arg1 = static_cast<QAction::MenuRole>(lqtL_toenum(L, 2, "QAction::MenuRole"));
  __lua__obj->QAction::setMenuRole(arg1);
  return 0;
}
int LuaBinder< QAction >::__LuaWrapCall__menuRole (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QAction::MenuRole ret = __lua__obj->QAction::menuRole();
  lqtL_pushenum(L, ret, "QAction::MenuRole");
  return 1;
}
int LuaBinder< QAction >::__LuaWrapCall__menu (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QMenu * ret = __lua__obj->QAction::menu();
  lqtL_pushudata(L, ret, "QMenu*");
  return 1;
}
int LuaBinder< QAction >::__LuaWrapCall__setWhatsThis (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QAction::setWhatsThis(arg1);
  return 0;
}
int LuaBinder< QAction >::__LuaWrapCall__isEnabled (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QAction::isEnabled();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QAction >::__LuaWrapCall__setData (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QVariant& arg1 = **static_cast<QVariant**>(lqtL_checkudata(L, 2, "QVariant*"));
  __lua__obj->QAction::setData(arg1);
  return 0;
}
int LuaBinder< QAction >::__LuaWrapCall__setEnabled (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QAction::setEnabled(arg1);
  return 0;
}
int LuaBinder< QAction >::__LuaWrapCall__text (lua_State *L) {
  QAction *& __lua__obj = *static_cast<QAction**>(lqtL_checkudata(L, 1, "QAction*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QAction::text();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
bool LuaBinder< QAction >::event (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAction*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "event");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QAction::event(arg1);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QAction >::connectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAction*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "connectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::connectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QAction >::eventFilter (QObject * arg1, QEvent * arg2) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAction*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "eventFilter");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QObject*");
  lqtL_pushudata(L, arg2, "QEvent*");
  if (lua_isfunction(L, -2-2)) {
    lua_pcall(L, 2+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QObject::eventFilter(arg1, arg2);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QAction >::disconnectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAction*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "disconnectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::disconnectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QAction >::childEvent (QChildEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAction*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "childEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QChildEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::childEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QAction >::timerEvent (QTimerEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAction*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "timerEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QTimerEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::timerEvent(arg1);
  }
  lua_settop(L, oldtop);
}
const QMetaObject * LuaBinder< QAction >::metaObject () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAction*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "metaObject");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QAction::metaObject();
  }
  const QMetaObject * ret = *static_cast<QMetaObject**>(lqtL_checkudata(L, -1, "QMetaObject*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QAction >::customEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAction*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "customEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::customEvent(arg1);
  }
  lua_settop(L, oldtop);
}
LuaBinder< QAction >::  ~LuaBinder< QAction > () {
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QAction*");
  lua_getfield(L, -1, "~QAction");

  if (lua_isfunction(L, -1)) {
    lua_insert(L, -2);
    lua_pcall(L, 1, 1, 0);
  } else {
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QAction >::lqt_pushenum_MenuRole (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "NoRole");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "NoRole");
  lua_pushstring(L, "TextHeuristicRole");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "TextHeuristicRole");
  lua_pushstring(L, "ApplicationSpecificRole");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "ApplicationSpecificRole");
  lua_pushstring(L, "AboutQtRole");
  lua_rawseti(L, enum_table, 3);
  lua_pushinteger(L, 3);
  lua_setfield(L, enum_table, "AboutQtRole");
  lua_pushstring(L, "AboutRole");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "AboutRole");
  lua_pushstring(L, "PreferencesRole");
  lua_rawseti(L, enum_table, 5);
  lua_pushinteger(L, 5);
  lua_setfield(L, enum_table, "PreferencesRole");
  lua_pushstring(L, "QuitRole");
  lua_rawseti(L, enum_table, 6);
  lua_pushinteger(L, 6);
  lua_setfield(L, enum_table, "QuitRole");
  lua_pushcfunction(L, LuaBinder< QAction >::lqt_pushenum_MenuRole_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QAction::MenuRole");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QAction >::lqt_pushenum_MenuRole_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QAction::MenuRole>*) + sizeof(QFlags<QAction::MenuRole>));
  QFlags<QAction::MenuRole> *fl = static_cast<QFlags<QAction::MenuRole>*>( static_cast<void*>(&static_cast<QFlags<QAction::MenuRole>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QAction::MenuRole>(lqtL_toenum(L, i, "QAction::MenuRole"));
	}
	if (luaL_newmetatable(L, "QFlags<QAction::MenuRole>*")) {
		lua_pushstring(L, "QFlags<QAction::MenuRole>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinder< QAction >::lqt_pushenum_ActionEvent (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "Trigger");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "Trigger");
  lua_pushstring(L, "Hover");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "Hover");
  lua_pushcfunction(L, LuaBinder< QAction >::lqt_pushenum_ActionEvent_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QAction::ActionEvent");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QAction >::lqt_pushenum_ActionEvent_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QAction::ActionEvent>*) + sizeof(QFlags<QAction::ActionEvent>));
  QFlags<QAction::ActionEvent> *fl = static_cast<QFlags<QAction::ActionEvent>*>( static_cast<void*>(&static_cast<QFlags<QAction::ActionEvent>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QAction::ActionEvent>(lqtL_toenum(L, i, "QAction::ActionEvent"));
	}
	if (luaL_newmetatable(L, "QFlags<QAction::ActionEvent>*")) {
		lua_pushstring(L, "QFlags<QAction::ActionEvent>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int luaopen_QAction (lua_State *L) {
  if (luaL_newmetatable(L, "QAction*")) {
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__parentWidget);
    lua_setfield(L, -2, "parentWidget");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__setChecked);
    lua_setfield(L, -2, "setChecked");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__statusTip);
    lua_setfield(L, -2, "statusTip");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__setVisible);
    lua_setfield(L, -2, "setVisible");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__setIcon);
    lua_setfield(L, -2, "setIcon");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__isSeparator);
    lua_setfield(L, -2, "isSeparator");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__toggle);
    lua_setfield(L, -2, "toggle");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__icon);
    lua_setfield(L, -2, "icon");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__tr);
    lua_setfield(L, -2, "tr");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__isVisible);
    lua_setfield(L, -2, "isVisible");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__whatsThis);
    lua_setfield(L, -2, "whatsThis");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__toolTip);
    lua_setfield(L, -2, "toolTip");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__setIconText);
    lua_setfield(L, -2, "setIconText");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__shortcut);
    lua_setfield(L, -2, "shortcut");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__iconText);
    lua_setfield(L, -2, "iconText");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__shortcutContext);
    lua_setfield(L, -2, "shortcutContext");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__setToolTip);
    lua_setfield(L, -2, "setToolTip");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__setMenu);
    lua_setfield(L, -2, "setMenu");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__showStatusText);
    lua_setfield(L, -2, "showStatusText");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__setAutoRepeat);
    lua_setfield(L, -2, "setAutoRepeat");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__setCheckable);
    lua_setfield(L, -2, "setCheckable");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__setSeparator);
    lua_setfield(L, -2, "setSeparator");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__activate);
    lua_setfield(L, -2, "activate");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__trigger);
    lua_setfield(L, -2, "trigger");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__shortcuts);
    lua_setfield(L, -2, "shortcuts");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__associatedWidgets);
    lua_setfield(L, -2, "associatedWidgets");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__setText);
    lua_setfield(L, -2, "setText");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__data);
    lua_setfield(L, -2, "data");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__isCheckable);
    lua_setfield(L, -2, "isCheckable");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__setStatusTip);
    lua_setfield(L, -2, "setStatusTip");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__setDisabled);
    lua_setfield(L, -2, "setDisabled");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__setShortcutContext);
    lua_setfield(L, -2, "setShortcutContext");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__setActionGroup);
    lua_setfield(L, -2, "setActionGroup");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__autoRepeat);
    lua_setfield(L, -2, "autoRepeat");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__setFont);
    lua_setfield(L, -2, "setFont");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__metaObject);
    lua_setfield(L, -2, "metaObject");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__new);
    lua_setfield(L, -2, "new");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__hover);
    lua_setfield(L, -2, "hover");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__delete);
    lua_setfield(L, -2, "delete");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__font);
    lua_setfield(L, -2, "font");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__actionGroup);
    lua_setfield(L, -2, "actionGroup");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__setShortcut);
    lua_setfield(L, -2, "setShortcut");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__setShortcuts);
    lua_setfield(L, -2, "setShortcuts");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__isChecked);
    lua_setfield(L, -2, "isChecked");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__trUtf8);
    lua_setfield(L, -2, "trUtf8");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__setMenuRole);
    lua_setfield(L, -2, "setMenuRole");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__menuRole);
    lua_setfield(L, -2, "menuRole");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__menu);
    lua_setfield(L, -2, "menu");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__setWhatsThis);
    lua_setfield(L, -2, "setWhatsThis");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__isEnabled);
    lua_setfield(L, -2, "isEnabled");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__setData);
    lua_setfield(L, -2, "setData");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__setEnabled);
    lua_setfield(L, -2, "setEnabled");
    lua_pushcfunction(L, LuaBinder< QAction >::__LuaWrapCall__text);
    lua_setfield(L, -2, "text");
    LuaBinder< QAction >::lqt_pushenum_MenuRole(L);
    lua_setfield(L, -2, "MenuRole");
    LuaBinder< QAction >::lqt_pushenum_ActionEvent(L);
    lua_setfield(L, -2, "ActionEvent");
    lua_newtable(L);
    lua_pushboolean(L, 1);
    lua_setfield(L, -2, "QObject*");
    lua_setfield(L, -2, "__base");
    lua_pushcfunction(L, lqtL_newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, lqtL_index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, lqtL_gc);
    lua_setfield(L, -2, "__gc");
    lua_pushstring(L, "QAction");
    lua_setfield(L, -2, "__qtype");
		lua_setglobal(L, "QAction");
  } else {
		lua_pop(L, 1);
	}
  return 0;
}
