#include "lqt_bind_QSize.hpp"

int LuaBinder< QSize >::__LuaWrapCall__isValid (lua_State *L) {
  QSize *& __lua__obj = *static_cast<QSize**>(lqtL_checkudata(L, 1, "QSize*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QSize::isValid();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QSize >::__LuaWrapCall__boundedTo (lua_State *L) {
  QSize *& __lua__obj = *static_cast<QSize**>(lqtL_checkudata(L, 1, "QSize*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QSize& arg1 = **static_cast<QSize**>(lqtL_checkudata(L, 2, "QSize*"));
  QSize ret = __lua__obj->QSize::boundedTo(arg1);
  lqtL_passudata(L, new QSize(ret), "QSize*");
  return 1;
}
int LuaBinder< QSize >::__LuaWrapCall__scale__OverloadedVersion__1 (lua_State *L) {
  QSize *& __lua__obj = *static_cast<QSize**>(lqtL_checkudata(L, 1, "QSize*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int arg2 = lua_tointeger(L, 3);
  Qt::AspectRatioMode arg3 = static_cast<Qt::AspectRatioMode>(lqtL_toenum(L, 4, "Qt::AspectRatioMode"));
  __lua__obj->QSize::scale(arg1, arg2, arg3);
  return 0;
}
int LuaBinder< QSize >::__LuaWrapCall__scale__OverloadedVersion__2 (lua_State *L) {
  QSize *& __lua__obj = *static_cast<QSize**>(lqtL_checkudata(L, 1, "QSize*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QSize& arg1 = **static_cast<QSize**>(lqtL_checkudata(L, 2, "QSize*"));
  Qt::AspectRatioMode arg2 = static_cast<Qt::AspectRatioMode>(lqtL_toenum(L, 3, "Qt::AspectRatioMode"));
  __lua__obj->QSize::scale(arg1, arg2);
  return 0;
}
int LuaBinder< QSize >::__LuaWrapCall__scale (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QSize*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1ee7fb0;
  } else {
    score[1] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1ee79e0;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_isenum(L, 4, "Qt::AspectRatioMode")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1ee8460;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QSize*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QSize*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1ee91c0;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_isenum(L, 3, "Qt::AspectRatioMode")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1ee8820;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__scale__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__scale__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__scale matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QSize >::__LuaWrapCall__setHeight (lua_State *L) {
  QSize *& __lua__obj = *static_cast<QSize**>(lqtL_checkudata(L, 1, "QSize*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QSize::setHeight(arg1);
  return 0;
}
int LuaBinder< QSize >::__LuaWrapCall__rheight (lua_State *L) {
  QSize *& __lua__obj = *static_cast<QSize**>(lqtL_checkudata(L, 1, "QSize*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int& ret = __lua__obj->QSize::rheight();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QSize >::__LuaWrapCall__width (lua_State *L) {
  QSize *& __lua__obj = *static_cast<QSize**>(lqtL_checkudata(L, 1, "QSize*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QSize::width();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QSize >::__LuaWrapCall__new__OverloadedVersion__2 (lua_State *L) {
  QSize * ret = new LuaBinder< QSize >(L);
  lqtL_passudata(L, ret, "QSize*");
  return 1;
}
int LuaBinder< QSize >::__LuaWrapCall__new__OverloadedVersion__3 (lua_State *L) {
  int arg1 = lua_tointeger(L, 1);
  int arg2 = lua_tointeger(L, 2);
  QSize * ret = new LuaBinder< QSize >(L, arg1, arg2);
  lqtL_passudata(L, ret, "QSize*");
  return 1;
}
int LuaBinder< QSize >::__LuaWrapCall__new (lua_State *L) {
  int score[4];
  const int premium = 11+lua_gettop(L);
  score[2] = 0;
  score[3] = 0;
  if (lua_isnumber(L, 1)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1ee34a0;
  } else {
    score[3] -= premium*premium;
  }
  if (lua_isnumber(L, 2)) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1ee39d0;
  } else {
    score[3] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=3;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 2: return __LuaWrapCall__new__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__new__OverloadedVersion__3(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__new matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QSize >::__LuaWrapCall__rwidth (lua_State *L) {
  QSize *& __lua__obj = *static_cast<QSize**>(lqtL_checkudata(L, 1, "QSize*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int& ret = __lua__obj->QSize::rwidth();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QSize >::__LuaWrapCall__setWidth (lua_State *L) {
  QSize *& __lua__obj = *static_cast<QSize**>(lqtL_checkudata(L, 1, "QSize*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QSize::setWidth(arg1);
  return 0;
}
int LuaBinder< QSize >::__LuaWrapCall__isNull (lua_State *L) {
  QSize *& __lua__obj = *static_cast<QSize**>(lqtL_checkudata(L, 1, "QSize*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QSize::isNull();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QSize >::__LuaWrapCall__height (lua_State *L) {
  QSize *& __lua__obj = *static_cast<QSize**>(lqtL_checkudata(L, 1, "QSize*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QSize::height();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QSize >::__LuaWrapCall__expandedTo (lua_State *L) {
  QSize *& __lua__obj = *static_cast<QSize**>(lqtL_checkudata(L, 1, "QSize*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QSize& arg1 = **static_cast<QSize**>(lqtL_checkudata(L, 2, "QSize*"));
  QSize ret = __lua__obj->QSize::expandedTo(arg1);
  lqtL_passudata(L, new QSize(ret), "QSize*");
  return 1;
}
int LuaBinder< QSize >::__LuaWrapCall__isEmpty (lua_State *L) {
  QSize *& __lua__obj = *static_cast<QSize**>(lqtL_checkudata(L, 1, "QSize*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QSize::isEmpty();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QSize >::__LuaWrapCall__transpose (lua_State *L) {
  QSize *& __lua__obj = *static_cast<QSize**>(lqtL_checkudata(L, 1, "QSize*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QSize::transpose();
  return 0;
}
int luaopen_QSize (lua_State *L) {
  if (luaL_newmetatable(L, "QSize*")) {
    lua_pushcfunction(L, LuaBinder< QSize >::__LuaWrapCall__isValid);
    lua_setfield(L, -2, "isValid");
    lua_pushcfunction(L, LuaBinder< QSize >::__LuaWrapCall__boundedTo);
    lua_setfield(L, -2, "boundedTo");
    lua_pushcfunction(L, LuaBinder< QSize >::__LuaWrapCall__scale);
    lua_setfield(L, -2, "scale");
    lua_pushcfunction(L, LuaBinder< QSize >::__LuaWrapCall__setHeight);
    lua_setfield(L, -2, "setHeight");
    lua_pushcfunction(L, LuaBinder< QSize >::__LuaWrapCall__rheight);
    lua_setfield(L, -2, "rheight");
    lua_pushcfunction(L, LuaBinder< QSize >::__LuaWrapCall__width);
    lua_setfield(L, -2, "width");
    lua_pushcfunction(L, LuaBinder< QSize >::__LuaWrapCall__new);
    lua_setfield(L, -2, "new");
    lua_pushcfunction(L, LuaBinder< QSize >::__LuaWrapCall__rwidth);
    lua_setfield(L, -2, "rwidth");
    lua_pushcfunction(L, LuaBinder< QSize >::__LuaWrapCall__setWidth);
    lua_setfield(L, -2, "setWidth");
    lua_pushcfunction(L, LuaBinder< QSize >::__LuaWrapCall__isNull);
    lua_setfield(L, -2, "isNull");
    lua_pushcfunction(L, LuaBinder< QSize >::__LuaWrapCall__height);
    lua_setfield(L, -2, "height");
    lua_pushcfunction(L, LuaBinder< QSize >::__LuaWrapCall__expandedTo);
    lua_setfield(L, -2, "expandedTo");
    lua_pushcfunction(L, LuaBinder< QSize >::__LuaWrapCall__isEmpty);
    lua_setfield(L, -2, "isEmpty");
    lua_pushcfunction(L, LuaBinder< QSize >::__LuaWrapCall__transpose);
    lua_setfield(L, -2, "transpose");
    lua_newtable(L);
    lua_setfield(L, -2, "__base");
    lua_pushcfunction(L, lqtL_newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, lqtL_index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, lqtL_gc);
    lua_setfield(L, -2, "__gc");
    lua_pushstring(L, "QSize");
    lua_setfield(L, -2, "__qtype");
		lua_setglobal(L, "QSize");
  } else {
		lua_pop(L, 1);
	}
  return 0;
}
