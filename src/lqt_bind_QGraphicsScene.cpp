#include "lqt_bind_QGraphicsScene.hpp"

int LuaBinder< QGraphicsScene >::__LuaWrapCall__render (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QPainter * arg1 = *static_cast<QPainter**>(lqtL_checkudata(L, 2, "QPainter*"));
  const QRectF& arg2 = lqtL_testudata(L, 3, "QRectF*")?**static_cast<QRectF**>(lqtL_checkudata(L, 3, "QRectF*")):QRectF();
  const QRectF& arg3 = lqtL_testudata(L, 4, "QRectF*")?**static_cast<QRectF**>(lqtL_checkudata(L, 4, "QRectF*")):QRectF();
  Qt::AspectRatioMode arg4 = lqtL_isenum(L, 5, "Qt::AspectRatioMode")?static_cast<Qt::AspectRatioMode>(lqtL_toenum(L, 5, "Qt::AspectRatioMode")):Qt::KeepAspectRatio;
  __lua__obj->QGraphicsScene::render(arg1, arg2, arg3, arg4);
  return 0;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__addPixmap (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPixmap& arg1 = **static_cast<QPixmap**>(lqtL_checkudata(L, 2, "QPixmap*"));
  QGraphicsPixmapItem * ret = __lua__obj->QGraphicsScene::addPixmap(arg1);
  lqtL_pushudata(L, ret, "QGraphicsPixmapItem*");
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__focusItem (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QGraphicsItem * ret = __lua__obj->QGraphicsScene::focusItem();
  lqtL_pushudata(L, ret, "QGraphicsItem*");
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__clearSelection (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QGraphicsScene::clearSelection();
  return 0;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__update__OverloadedVersion__1 (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  double arg2 = lua_tonumber(L, 3);
  double arg3 = lua_tonumber(L, 4);
  double arg4 = lua_tonumber(L, 5);
  __lua__obj->QGraphicsScene::update(arg1, arg2, arg3, arg4);
  return 0;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__update__OverloadedVersion__2 (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QRectF& arg1 = lqtL_testudata(L, 2, "QRectF*")?**static_cast<QRectF**>(lqtL_checkudata(L, 2, "QRectF*")):QRectF();
  __lua__obj->QGraphicsScene::update(arg1);
  return 0;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__update (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QGraphicsScene*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1142160;
  } else {
    score[1] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1142670;
  } else {
    score[1] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1142a20;
  } else {
    score[1] -= premium*premium;
  }
  if (lua_isnumber(L, 5)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x11429c0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QGraphicsScene*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QRectF*")) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x1145180;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__update__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__update__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__update matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__sceneRect (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QRectF ret = __lua__obj->QGraphicsScene::sceneRect();
  lqtL_passudata(L, new QRectF(ret), "QRectF*");
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__addLine__OverloadedVersion__1 (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QLineF& arg1 = **static_cast<QLineF**>(lqtL_checkudata(L, 2, "QLineF*"));
  const QPen& arg2 = lqtL_testudata(L, 3, "QPen*")?**static_cast<QPen**>(lqtL_checkudata(L, 3, "QPen*")):QPen();
  QGraphicsLineItem * ret = __lua__obj->QGraphicsScene::addLine(arg1, arg2);
  lqtL_pushudata(L, ret, "QGraphicsLineItem*");
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__addLine__OverloadedVersion__2 (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  double arg2 = lua_tonumber(L, 3);
  double arg3 = lua_tonumber(L, 4);
  double arg4 = lua_tonumber(L, 5);
  const QPen& arg5 = lqtL_testudata(L, 6, "QPen*")?**static_cast<QPen**>(lqtL_checkudata(L, 6, "QPen*")):QPen();
  QGraphicsLineItem * ret = __lua__obj->QGraphicsScene::addLine(arg1, arg2, arg3, arg4, arg5);
  lqtL_pushudata(L, ret, "QGraphicsLineItem*");
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__addLine (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QGraphicsScene*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QLineF*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x112db90;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_testudata(L, 3, "QPen*")) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x112d800;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QGraphicsScene*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1136d20;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1136a10;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1137200;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 5)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1137ac0;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_testudata(L, 6, "QPen*")) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x1137eb0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__addLine__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__addLine__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__addLine matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__itemsBoundingRect (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QRectF ret = __lua__obj->QGraphicsScene::itemsBoundingRect();
  lqtL_passudata(L, new QRectF(ret), "QRectF*");
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QGraphicsScene::trUtf8(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QGraphicsScene::trUtf8(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__trUtf8 (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1110070;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x110fbc0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1111e60;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1111580;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x11122e0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__trUtf8__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__trUtf8__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__trUtf8 matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__addRect__OverloadedVersion__1 (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QRectF& arg1 = **static_cast<QRectF**>(lqtL_checkudata(L, 2, "QRectF*"));
  const QPen& arg2 = lqtL_testudata(L, 3, "QPen*")?**static_cast<QPen**>(lqtL_checkudata(L, 3, "QPen*")):QPen();
  const QBrush& arg3 = lqtL_testudata(L, 4, "QBrush*")?**static_cast<QBrush**>(lqtL_checkudata(L, 4, "QBrush*")):QBrush();
  QGraphicsRectItem * ret = __lua__obj->QGraphicsScene::addRect(arg1, arg2, arg3);
  lqtL_pushudata(L, ret, "QGraphicsRectItem*");
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__addRect__OverloadedVersion__2 (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  double arg2 = lua_tonumber(L, 3);
  double arg3 = lua_tonumber(L, 4);
  double arg4 = lua_tonumber(L, 5);
  const QPen& arg5 = lqtL_testudata(L, 6, "QPen*")?**static_cast<QPen**>(lqtL_checkudata(L, 6, "QPen*")):QPen();
  const QBrush& arg6 = lqtL_testudata(L, 7, "QBrush*")?**static_cast<QBrush**>(lqtL_checkudata(L, 7, "QBrush*")):QBrush();
  QGraphicsRectItem * ret = __lua__obj->QGraphicsScene::addRect(arg1, arg2, arg3, arg4, arg5, arg6);
  lqtL_pushudata(L, ret, "QGraphicsRectItem*");
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__addRect (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QGraphicsScene*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QRectF*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1131d30;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_testudata(L, 3, "QPen*")) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x1131310;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_testudata(L, 4, "QBrush*")) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x1132280;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QGraphicsScene*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1138830;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1138d60;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1139110;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 5)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x11390b0;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_testudata(L, 6, "QPen*")) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x11398e0;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_testudata(L, 7, "QBrush*")) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x1139490;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__addRect__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__addRect__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__addRect matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__advance (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QGraphicsScene::advance();
  return 0;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__selectionArea (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QPainterPath ret = __lua__obj->QGraphicsScene::selectionArea();
  lqtL_passudata(L, new QPainterPath(ret), "QPainterPath*");
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__setSelectionArea__OverloadedVersion__1 (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPainterPath& arg1 = **static_cast<QPainterPath**>(lqtL_checkudata(L, 2, "QPainterPath*"));
  __lua__obj->QGraphicsScene::setSelectionArea(arg1);
  return 0;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__setSelectionArea__OverloadedVersion__2 (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPainterPath& arg1 = **static_cast<QPainterPath**>(lqtL_checkudata(L, 2, "QPainterPath*"));
  Qt::ItemSelectionMode arg2 = static_cast<Qt::ItemSelectionMode>(lqtL_toenum(L, 3, "Qt::ItemSelectionMode"));
  __lua__obj->QGraphicsScene::setSelectionArea(arg1, arg2);
  return 0;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__setSelectionArea (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QGraphicsScene*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPainterPath*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x11285f0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QGraphicsScene*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPainterPath*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1129060;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_isenum(L, 3, "Qt::ItemSelectionMode")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x11295b0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__setSelectionArea__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__setSelectionArea__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__setSelectionArea matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__setItemIndexMethod (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QGraphicsScene::ItemIndexMethod arg1 = static_cast<QGraphicsScene::ItemIndexMethod>(lqtL_toenum(L, 2, "QGraphicsScene::ItemIndexMethod"));
  __lua__obj->QGraphicsScene::setItemIndexMethod(arg1);
  return 0;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__hasFocus (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QGraphicsScene::hasFocus();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__bspTreeDepth (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QGraphicsScene::bspTreeDepth();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__collidingItems (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QGraphicsItem * arg1 = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 2, "QGraphicsItem*"));
  Qt::ItemSelectionMode arg2 = lqtL_isenum(L, 3, "Qt::ItemSelectionMode")?static_cast<Qt::ItemSelectionMode>(lqtL_toenum(L, 3, "Qt::ItemSelectionMode")):Qt::IntersectsItemShape;
  QList<QGraphicsItem*> ret = __lua__obj->QGraphicsScene::collidingItems(arg1, arg2);
  lqtL_passudata(L, new QList<QGraphicsItem*>(ret), "QList<QGraphicsItem*>*");
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__height (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double ret = __lua__obj->QGraphicsScene::height();
  lua_pushnumber(L, ret);
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__addEllipse__OverloadedVersion__1 (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QRectF& arg1 = **static_cast<QRectF**>(lqtL_checkudata(L, 2, "QRectF*"));
  const QPen& arg2 = lqtL_testudata(L, 3, "QPen*")?**static_cast<QPen**>(lqtL_checkudata(L, 3, "QPen*")):QPen();
  const QBrush& arg3 = lqtL_testudata(L, 4, "QBrush*")?**static_cast<QBrush**>(lqtL_checkudata(L, 4, "QBrush*")):QBrush();
  QGraphicsEllipseItem * ret = __lua__obj->QGraphicsScene::addEllipse(arg1, arg2, arg3);
  lqtL_pushudata(L, ret, "QGraphicsEllipseItem*");
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__addEllipse__OverloadedVersion__2 (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  double arg2 = lua_tonumber(L, 3);
  double arg3 = lua_tonumber(L, 4);
  double arg4 = lua_tonumber(L, 5);
  const QPen& arg5 = lqtL_testudata(L, 6, "QPen*")?**static_cast<QPen**>(lqtL_checkudata(L, 6, "QPen*")):QPen();
  const QBrush& arg6 = lqtL_testudata(L, 7, "QBrush*")?**static_cast<QBrush**>(lqtL_checkudata(L, 7, "QBrush*")):QBrush();
  QGraphicsEllipseItem * ret = __lua__obj->QGraphicsScene::addEllipse(arg1, arg2, arg3, arg4, arg5, arg6);
  lqtL_pushudata(L, ret, "QGraphicsEllipseItem*");
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__addEllipse (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QGraphicsScene*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QRectF*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x112c720;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_testudata(L, 3, "QPen*")) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x112c160;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_testudata(L, 4, "QBrush*")) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x112cc80;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QGraphicsScene*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1134f50;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1135470;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1135820;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 5)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x11357c0;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_testudata(L, 6, "QPen*")) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x1135ff0;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_testudata(L, 7, "QBrush*")) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x1135ba0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__addEllipse__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__addEllipse__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__addEllipse matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__addSimpleText (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  const QFont& arg2 = lqtL_testudata(L, 3, "QFont*")?**static_cast<QFont**>(lqtL_checkudata(L, 3, "QFont*")):QFont();
  QGraphicsSimpleTextItem * ret = __lua__obj->QGraphicsScene::addSimpleText(arg1, arg2);
  lqtL_pushudata(L, ret, "QGraphicsSimpleTextItem*");
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__setSceneRect__OverloadedVersion__1 (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QRectF& arg1 = **static_cast<QRectF**>(lqtL_checkudata(L, 2, "QRectF*"));
  __lua__obj->QGraphicsScene::setSceneRect(arg1);
  return 0;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__setSceneRect__OverloadedVersion__2 (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  double arg2 = lua_tonumber(L, 3);
  double arg3 = lua_tonumber(L, 4);
  double arg4 = lua_tonumber(L, 5);
  __lua__obj->QGraphicsScene::setSceneRect(arg1, arg2, arg3, arg4);
  return 0;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__setSceneRect (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QGraphicsScene*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QRectF*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1118e20;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QGraphicsScene*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1119870;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1119d90;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x111a140;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 5)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x111a0e0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__setSceneRect__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__setSceneRect__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__setSceneRect matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__destroyItemGroup (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QGraphicsItemGroup * arg1 = *static_cast<QGraphicsItemGroup**>(lqtL_checkudata(L, 2, "QGraphicsItemGroup*"));
  __lua__obj->QGraphicsScene::destroyItemGroup(arg1);
  return 0;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__createItemGroup (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QList<QGraphicsItem*>& arg1 = **static_cast<QList<QGraphicsItem*>**>(lqtL_checkudata(L, 2, "QList<QGraphicsItem*>*"));
  QGraphicsItemGroup * ret = __lua__obj->QGraphicsScene::createItemGroup(arg1);
  lqtL_pushudata(L, ret, "QGraphicsItemGroup*");
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__invalidate__OverloadedVersion__1 (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  double arg2 = lua_tonumber(L, 3);
  double arg3 = lua_tonumber(L, 4);
  double arg4 = lua_tonumber(L, 5);
  QFlags<QGraphicsScene::SceneLayer> arg5 = lqtL_testudata(L, 6, "QFlags<QGraphicsScene::SceneLayer>*")?**static_cast<QFlags<QGraphicsScene::SceneLayer>**>(lqtL_checkudata(L, 6, "QFlags<QGraphicsScene::SceneLayer>*")):AllLayers;
  __lua__obj->QGraphicsScene::invalidate(arg1, arg2, arg3, arg4, arg5);
  return 0;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__invalidate__OverloadedVersion__2 (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QRectF& arg1 = lqtL_testudata(L, 2, "QRectF*")?**static_cast<QRectF**>(lqtL_checkudata(L, 2, "QRectF*")):QRectF();
  QFlags<QGraphicsScene::SceneLayer> arg2 = lqtL_testudata(L, 3, "QFlags<QGraphicsScene::SceneLayer>*")?**static_cast<QFlags<QGraphicsScene::SceneLayer>**>(lqtL_checkudata(L, 3, "QFlags<QGraphicsScene::SceneLayer>*")):AllLayers;
  __lua__obj->QGraphicsScene::invalidate(arg1, arg2);
  return 0;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__invalidate (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QGraphicsScene*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1143740;
  } else {
    score[1] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1143c60;
  } else {
    score[1] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1144010;
  } else {
    score[1] -= premium*premium;
  }
  if (lua_isnumber(L, 5)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1143fb0;
  } else {
    score[1] -= premium*premium;
  }
  if (lqtL_testudata(L, 6, "QFlags<QGraphicsScene::SceneLayer>*")) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x11447e0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QGraphicsScene*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QRectF*")) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x1145be0;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_testudata(L, 3, "QFlags<QGraphicsScene::SceneLayer>*")) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x1145690;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__invalidate__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__invalidate__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__invalidate matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__addItem (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QGraphicsItem * arg1 = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 2, "QGraphicsItem*"));
  __lua__obj->QGraphicsScene::addItem(arg1);
  return 0;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__setFocusItem (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QGraphicsItem * arg1 = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 2, "QGraphicsItem*"));
  Qt::FocusReason arg2 = lqtL_isenum(L, 3, "Qt::FocusReason")?static_cast<Qt::FocusReason>(lqtL_toenum(L, 3, "Qt::FocusReason")):Qt::OtherFocusReason;
  __lua__obj->QGraphicsScene::setFocusItem(arg1, arg2);
  return 0;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__setFocus (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::FocusReason arg1 = lqtL_isenum(L, 2, "Qt::FocusReason")?static_cast<Qt::FocusReason>(lqtL_toenum(L, 2, "Qt::FocusReason")):Qt::OtherFocusReason;
  __lua__obj->QGraphicsScene::setFocus(arg1);
  return 0;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__items__OverloadedVersion__1 (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QList<QGraphicsItem*> ret = __lua__obj->QGraphicsScene::items();
  lqtL_passudata(L, new QList<QGraphicsItem*>(ret), "QList<QGraphicsItem*>*");
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__items__OverloadedVersion__2 (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPointF& arg1 = **static_cast<QPointF**>(lqtL_checkudata(L, 2, "QPointF*"));
  QList<QGraphicsItem*> ret = __lua__obj->QGraphicsScene::items(arg1);
  lqtL_passudata(L, new QList<QGraphicsItem*>(ret), "QList<QGraphicsItem*>*");
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__items__OverloadedVersion__3 (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QRectF& arg1 = **static_cast<QRectF**>(lqtL_checkudata(L, 2, "QRectF*"));
  Qt::ItemSelectionMode arg2 = lqtL_isenum(L, 3, "Qt::ItemSelectionMode")?static_cast<Qt::ItemSelectionMode>(lqtL_toenum(L, 3, "Qt::ItemSelectionMode")):Qt::IntersectsItemShape;
  QList<QGraphicsItem*> ret = __lua__obj->QGraphicsScene::items(arg1, arg2);
  lqtL_passudata(L, new QList<QGraphicsItem*>(ret), "QList<QGraphicsItem*>*");
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__items__OverloadedVersion__4 (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPolygonF& arg1 = **static_cast<QPolygonF**>(lqtL_checkudata(L, 2, "QPolygonF*"));
  Qt::ItemSelectionMode arg2 = lqtL_isenum(L, 3, "Qt::ItemSelectionMode")?static_cast<Qt::ItemSelectionMode>(lqtL_toenum(L, 3, "Qt::ItemSelectionMode")):Qt::IntersectsItemShape;
  QList<QGraphicsItem*> ret = __lua__obj->QGraphicsScene::items(arg1, arg2);
  lqtL_passudata(L, new QList<QGraphicsItem*>(ret), "QList<QGraphicsItem*>*");
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__items__OverloadedVersion__5 (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPainterPath& arg1 = **static_cast<QPainterPath**>(lqtL_checkudata(L, 2, "QPainterPath*"));
  Qt::ItemSelectionMode arg2 = lqtL_isenum(L, 3, "Qt::ItemSelectionMode")?static_cast<Qt::ItemSelectionMode>(lqtL_toenum(L, 3, "Qt::ItemSelectionMode")):Qt::IntersectsItemShape;
  QList<QGraphicsItem*> ret = __lua__obj->QGraphicsScene::items(arg1, arg2);
  lqtL_passudata(L, new QList<QGraphicsItem*>(ret), "QList<QGraphicsItem*>*");
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__items__OverloadedVersion__6 (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  double arg2 = lua_tonumber(L, 3);
  double arg3 = lua_tonumber(L, 4);
  double arg4 = lua_tonumber(L, 5);
  Qt::ItemSelectionMode arg5 = lqtL_isenum(L, 6, "Qt::ItemSelectionMode")?static_cast<Qt::ItemSelectionMode>(lqtL_toenum(L, 6, "Qt::ItemSelectionMode")):Qt::IntersectsItemShape;
  QList<QGraphicsItem*> ret = __lua__obj->QGraphicsScene::items(arg1, arg2, arg3, arg4, arg5);
  lqtL_passudata(L, new QList<QGraphicsItem*>(ret), "QList<QGraphicsItem*>*");
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__items (lua_State *L) {
  int score[7];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QGraphicsScene*")?premium:-premium*premium;
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QGraphicsScene*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPointF*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x111fb00;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  score[3] += lqtL_testudata(L, 1, "QGraphicsScene*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QRectF*")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x11205c0;
  } else {
    score[3] -= premium*premium;
  }
  if (lqtL_isenum(L, 3, "Qt::ItemSelectionMode")) {
    score[3] += premium;
  } else if (true) {
    score[3] += premium-1; // table: 0x1120020;
  } else {
    score[3] -= premium*premium;
  }
  score[4] = 0;
  score[4] += lqtL_testudata(L, 1, "QGraphicsScene*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPolygonF*")) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x1121550;
  } else {
    score[4] -= premium*premium;
  }
  if (lqtL_isenum(L, 3, "Qt::ItemSelectionMode")) {
    score[4] += premium;
  } else if (true) {
    score[4] += premium-1; // table: 0x11211f0;
  } else {
    score[4] -= premium*premium;
  }
  score[5] = 0;
  score[5] += lqtL_testudata(L, 1, "QGraphicsScene*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPainterPath*")) {
    score[5] += premium;
  } else if (false) {
    score[5] += premium-1; // table: 0x1122470;
  } else {
    score[5] -= premium*premium;
  }
  if (lqtL_isenum(L, 3, "Qt::ItemSelectionMode")) {
    score[5] += premium;
  } else if (true) {
    score[5] += premium-1; // table: 0x1121ef0;
  } else {
    score[5] -= premium*premium;
  }
  score[6] = 0;
  score[6] += lqtL_testudata(L, 1, "QGraphicsScene*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x1124e50;
  } else {
    score[6] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x1125380;
  } else {
    score[6] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x1125730;
  } else {
    score[6] -= premium*premium;
  }
  if (lua_isnumber(L, 5)) {
    score[6] += premium;
  } else if (false) {
    score[6] += premium-1; // table: 0x11256d0;
  } else {
    score[6] -= premium*premium;
  }
  if (lqtL_isenum(L, 6, "Qt::ItemSelectionMode")) {
    score[6] += premium;
  } else if (true) {
    score[6] += premium-1; // table: 0x1125f10;
  } else {
    score[6] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=6;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__items__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__items__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__items__OverloadedVersion__3(L); break;
    case 4: return __LuaWrapCall__items__OverloadedVersion__4(L); break;
    case 5: return __LuaWrapCall__items__OverloadedVersion__5(L); break;
    case 6: return __LuaWrapCall__items__OverloadedVersion__6(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__items matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__addText (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  const QFont& arg2 = lqtL_testudata(L, 3, "QFont*")?**static_cast<QFont**>(lqtL_checkudata(L, 3, "QFont*")):QFont();
  QGraphicsTextItem * ret = __lua__obj->QGraphicsScene::addText(arg1, arg2);
  lqtL_pushudata(L, ret, "QGraphicsTextItem*");
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__metaObject (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QMetaObject * ret = __lua__obj->QGraphicsScene::metaObject();
  lqtL_pushudata(L, ret, "QMetaObject*");
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__itemIndexMethod (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QGraphicsScene::ItemIndexMethod ret = __lua__obj->QGraphicsScene::itemIndexMethod();
  lqtL_pushenum(L, ret, "QGraphicsScene::ItemIndexMethod");
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__backgroundBrush (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QBrush ret = __lua__obj->QGraphicsScene::backgroundBrush();
  lqtL_passudata(L, new QBrush(ret), "QBrush*");
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__new__OverloadedVersion__2 (lua_State *L) {
  QObject * arg1 = lqtL_testudata(L, 1, "QObject*")?*static_cast<QObject**>(lqtL_checkudata(L, 1, "QObject*")):static_cast< QObject * >(0);
  QGraphicsScene * ret = new LuaBinder< QGraphicsScene >(L, arg1);
  lqtL_passudata(L, ret, "QGraphicsScene*");
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__new__OverloadedVersion__3 (lua_State *L) {
  const QRectF& arg1 = **static_cast<QRectF**>(lqtL_checkudata(L, 1, "QRectF*"));
  QObject * arg2 = lqtL_testudata(L, 2, "QObject*")?*static_cast<QObject**>(lqtL_checkudata(L, 2, "QObject*")):static_cast< QObject * >(0);
  QGraphicsScene * ret = new LuaBinder< QGraphicsScene >(L, arg1, arg2);
  lqtL_passudata(L, ret, "QGraphicsScene*");
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__new__OverloadedVersion__4 (lua_State *L) {
  double arg1 = lua_tonumber(L, 1);
  double arg2 = lua_tonumber(L, 2);
  double arg3 = lua_tonumber(L, 3);
  double arg4 = lua_tonumber(L, 4);
  QObject * arg5 = lqtL_testudata(L, 5, "QObject*")?*static_cast<QObject**>(lqtL_checkudata(L, 5, "QObject*")):static_cast< QObject * >(0);
  QGraphicsScene * ret = new LuaBinder< QGraphicsScene >(L, arg1, arg2, arg3, arg4, arg5);
  lqtL_passudata(L, ret, "QGraphicsScene*");
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__new (lua_State *L) {
  int score[5];
  const int premium = 11+lua_gettop(L);
  score[2] = 0;
  if (lqtL_testudata(L, 1, "QObject*")) {
    score[2] += premium;
  } else if (true) {
    score[2] += premium-1; // table: 0x1113f50;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  if (lqtL_testudata(L, 1, "QRectF*")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x11149c0;
  } else {
    score[3] -= premium*premium;
  }
  if (lqtL_testudata(L, 2, "QObject*")) {
    score[3] += premium;
  } else if (true) {
    score[3] += premium-1; // table: 0x1114460;
  } else {
    score[3] -= premium*premium;
  }
  score[4] = 0;
  if (lua_isnumber(L, 1)) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x1115870;
  } else {
    score[4] -= premium*premium;
  }
  if (lua_isnumber(L, 2)) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x1115d90;
  } else {
    score[4] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x1115d30;
  } else {
    score[4] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x1116580;
  } else {
    score[4] -= premium*premium;
  }
  if (lqtL_testudata(L, 5, "QObject*")) {
    score[4] += premium;
  } else if (true) {
    score[4] += premium-1; // table: 0x1116940;
  } else {
    score[4] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=4;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 2: return __LuaWrapCall__new__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__new__OverloadedVersion__3(L); break;
    case 4: return __LuaWrapCall__new__OverloadedVersion__4(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__new matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__inputMethodQuery (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::InputMethodQuery arg1 = static_cast<Qt::InputMethodQuery>(lqtL_toenum(L, 2, "Qt::InputMethodQuery"));
  QVariant ret = __lua__obj->QGraphicsScene::inputMethodQuery(arg1);
  lqtL_passudata(L, new QVariant(ret), "QVariant*");
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__delete (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  delete __lua__obj;
  __lua__obj = 0;
  return 0;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__addPath (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPainterPath& arg1 = **static_cast<QPainterPath**>(lqtL_checkudata(L, 2, "QPainterPath*"));
  const QPen& arg2 = lqtL_testudata(L, 3, "QPen*")?**static_cast<QPen**>(lqtL_checkudata(L, 3, "QPen*")):QPen();
  const QBrush& arg3 = lqtL_testudata(L, 4, "QBrush*")?**static_cast<QBrush**>(lqtL_checkudata(L, 4, "QBrush*")):QBrush();
  QGraphicsPathItem * ret = __lua__obj->QGraphicsScene::addPath(arg1, arg2, arg3);
  lqtL_pushudata(L, ret, "QGraphicsPathItem*");
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__foregroundBrush (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QBrush ret = __lua__obj->QGraphicsScene::foregroundBrush();
  lqtL_passudata(L, new QBrush(ret), "QBrush*");
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__selectedItems (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QList<QGraphicsItem*> ret = __lua__obj->QGraphicsScene::selectedItems();
  lqtL_passudata(L, new QList<QGraphicsItem*>(ret), "QList<QGraphicsItem*>*");
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__mouseGrabberItem (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QGraphicsItem * ret = __lua__obj->QGraphicsScene::mouseGrabberItem();
  lqtL_pushudata(L, ret, "QGraphicsItem*");
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__setBspTreeDepth (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QGraphicsScene::setBspTreeDepth(arg1);
  return 0;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__width (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double ret = __lua__obj->QGraphicsScene::width();
  lua_pushnumber(L, ret);
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__setForegroundBrush (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QBrush& arg1 = **static_cast<QBrush**>(lqtL_checkudata(L, 2, "QBrush*"));
  __lua__obj->QGraphicsScene::setForegroundBrush(arg1);
  return 0;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__itemAt__OverloadedVersion__1 (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPointF& arg1 = **static_cast<QPointF**>(lqtL_checkudata(L, 2, "QPointF*"));
  QGraphicsItem * ret = __lua__obj->QGraphicsScene::itemAt(arg1);
  lqtL_pushudata(L, ret, "QGraphicsItem*");
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__itemAt__OverloadedVersion__2 (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  double arg2 = lua_tonumber(L, 3);
  QGraphicsItem * ret = __lua__obj->QGraphicsScene::itemAt(arg1, arg2);
  lqtL_pushudata(L, ret, "QGraphicsItem*");
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__itemAt (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QGraphicsScene*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPointF*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1124350;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QGraphicsScene*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1126840;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1126d50;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__itemAt__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__itemAt__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__itemAt matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__setBackgroundBrush (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QBrush& arg1 = **static_cast<QBrush**>(lqtL_checkudata(L, 2, "QBrush*"));
  __lua__obj->QGraphicsScene::setBackgroundBrush(arg1);
  return 0;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QGraphicsScene::tr(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QGraphicsScene::tr(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__tr (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x110f2d0;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x110f030;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1110d40;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1111230;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x11115e0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__tr__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__tr__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__tr matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__views (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QList<QGraphicsView*> ret = __lua__obj->QGraphicsScene::views();
  lqtL_passudata(L, new QList<QGraphicsView*>(ret), "QList<QGraphicsView*>*");
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__removeItem (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QGraphicsItem * arg1 = *static_cast<QGraphicsItem**>(lqtL_checkudata(L, 2, "QGraphicsItem*"));
  __lua__obj->QGraphicsScene::removeItem(arg1);
  return 0;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__addPolygon (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPolygonF& arg1 = **static_cast<QPolygonF**>(lqtL_checkudata(L, 2, "QPolygonF*"));
  const QPen& arg2 = lqtL_testudata(L, 3, "QPen*")?**static_cast<QPen**>(lqtL_checkudata(L, 3, "QPen*")):QPen();
  const QBrush& arg3 = lqtL_testudata(L, 4, "QBrush*")?**static_cast<QBrush**>(lqtL_checkudata(L, 4, "QBrush*")):QBrush();
  QGraphicsPolygonItem * ret = __lua__obj->QGraphicsScene::addPolygon(arg1, arg2, arg3);
  lqtL_pushudata(L, ret, "QGraphicsPolygonItem*");
  return 1;
}
int LuaBinder< QGraphicsScene >::__LuaWrapCall__clearFocus (lua_State *L) {
  QGraphicsScene *& __lua__obj = *static_cast<QGraphicsScene**>(lqtL_checkudata(L, 1, "QGraphicsScene*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QGraphicsScene::clearFocus();
  return 0;
}
void LuaBinder< QGraphicsScene >::focusInEvent (QFocusEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsScene*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "focusInEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QFocusEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsScene::focusInEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsScene >::keyPressEvent (QKeyEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsScene*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "keyPressEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QKeyEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsScene::keyPressEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsScene >::contextMenuEvent (QGraphicsSceneContextMenuEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsScene*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "contextMenuEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QGraphicsSceneContextMenuEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsScene::contextMenuEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsScene >::focusOutEvent (QFocusEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsScene*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "focusOutEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QFocusEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsScene::focusOutEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsScene >::drawBackground (QPainter * arg1, const QRectF& arg2) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsScene*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "drawBackground");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QPainter*");
  lqtL_pushudata(L, &(arg2), "QRectF*");
  if (lua_isfunction(L, -2-2)) {
    lua_pcall(L, 2+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsScene::drawBackground(arg1, arg2);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsScene >::mouseDoubleClickEvent (QGraphicsSceneMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsScene*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mouseDoubleClickEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QGraphicsSceneMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsScene::mouseDoubleClickEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsScene >::childEvent (QChildEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsScene*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "childEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QChildEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::childEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsScene >::disconnectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsScene*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "disconnectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::disconnectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
QVariant LuaBinder< QGraphicsScene >::inputMethodQuery (Qt::InputMethodQuery arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsScene*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "inputMethodQuery");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushenum(L, arg1, "Qt::InputMethodQuery");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QGraphicsScene::inputMethodQuery(arg1);
  }
  QVariant ret = **static_cast<QVariant**>(lqtL_checkudata(L, -1, "QVariant*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QGraphicsScene >::connectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsScene*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "connectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::connectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsScene >::drawForeground (QPainter * arg1, const QRectF& arg2) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsScene*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "drawForeground");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QPainter*");
  lqtL_pushudata(L, &(arg2), "QRectF*");
  if (lua_isfunction(L, -2-2)) {
    lua_pcall(L, 2+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsScene::drawForeground(arg1, arg2);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsScene >::dragLeaveEvent (QGraphicsSceneDragDropEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsScene*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dragLeaveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QGraphicsSceneDragDropEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsScene::dragLeaveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsScene >::mousePressEvent (QGraphicsSceneMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsScene*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mousePressEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QGraphicsSceneMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsScene::mousePressEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsScene >::timerEvent (QTimerEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsScene*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "timerEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QTimerEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::timerEvent(arg1);
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QGraphicsScene >::eventFilter (QObject * arg1, QEvent * arg2) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsScene*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "eventFilter");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QObject*");
  lqtL_pushudata(L, arg2, "QEvent*");
  if (lua_isfunction(L, -2-2)) {
    lua_pcall(L, 2+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QObject::eventFilter(arg1, arg2);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QGraphicsScene >::inputMethodEvent (QInputMethodEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsScene*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "inputMethodEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QInputMethodEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsScene::inputMethodEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsScene >::dragMoveEvent (QGraphicsSceneDragDropEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsScene*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dragMoveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QGraphicsSceneDragDropEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsScene::dragMoveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsScene >::drawItems (QPainter * arg1, int arg2, QGraphicsItem * * arg3, const QStyleOptionGraphicsItem * arg4, QWidget * arg5) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsScene*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "drawItems");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QPainter*");
  lua_pushinteger(L, arg2);
  lqtL_pushudata(L, arg3, "QGraphicsItem **");
  lqtL_pushudata(L, arg4, "QStyleOptionGraphicsItem*");
  lqtL_pushudata(L, arg5, "QWidget*");
  if (lua_isfunction(L, -5-2)) {
    lua_pcall(L, 5+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsScene::drawItems(arg1, arg2, arg3, arg4, arg5);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsScene >::wheelEvent (QGraphicsSceneWheelEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsScene*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "wheelEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QGraphicsSceneWheelEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsScene::wheelEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsScene >::mouseReleaseEvent (QGraphicsSceneMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsScene*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mouseReleaseEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QGraphicsSceneMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsScene::mouseReleaseEvent(arg1);
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QGraphicsScene >::event (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsScene*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "event");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QGraphicsScene::event(arg1);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QGraphicsScene >::dragEnterEvent (QGraphicsSceneDragDropEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsScene*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dragEnterEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QGraphicsSceneDragDropEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsScene::dragEnterEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsScene >::mouseMoveEvent (QGraphicsSceneMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsScene*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mouseMoveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QGraphicsSceneMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsScene::mouseMoveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsScene >::helpEvent (QGraphicsSceneHelpEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsScene*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "helpEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QGraphicsSceneHelpEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsScene::helpEvent(arg1);
  }
  lua_settop(L, oldtop);
}
const QMetaObject * LuaBinder< QGraphicsScene >::metaObject () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsScene*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "metaObject");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QGraphicsScene::metaObject();
  }
  const QMetaObject * ret = *static_cast<QMetaObject**>(lqtL_checkudata(L, -1, "QMetaObject*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QGraphicsScene >::keyReleaseEvent (QKeyEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsScene*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "keyReleaseEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QKeyEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsScene::keyReleaseEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsScene >::dropEvent (QGraphicsSceneDragDropEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsScene*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dropEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QGraphicsSceneDragDropEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QGraphicsScene::dropEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QGraphicsScene >::customEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsScene*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "customEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::customEvent(arg1);
  }
  lua_settop(L, oldtop);
}
LuaBinder< QGraphicsScene >::  ~LuaBinder< QGraphicsScene > () {
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QGraphicsScene*");
  lua_getfield(L, -1, "~QGraphicsScene");

  if (lua_isfunction(L, -1)) {
    lua_insert(L, -2);
    lua_pcall(L, 1, 1, 0);
  } else {
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QGraphicsScene >::lqt_pushenum_ItemIndexMethod (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "BspTreeIndex");
  lua_rawseti(L, enum_table, 0);
  lua_pushinteger(L, 0);
  lua_setfield(L, enum_table, "BspTreeIndex");
  lua_pushstring(L, "NoIndex");
  lua_rawseti(L, enum_table, -1);
  lua_pushinteger(L, -1);
  lua_setfield(L, enum_table, "NoIndex");
  lua_pushcfunction(L, LuaBinder< QGraphicsScene >::lqt_pushenum_ItemIndexMethod_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QGraphicsScene::ItemIndexMethod");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QGraphicsScene >::lqt_pushenum_ItemIndexMethod_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QGraphicsScene::ItemIndexMethod>*) + sizeof(QFlags<QGraphicsScene::ItemIndexMethod>));
  QFlags<QGraphicsScene::ItemIndexMethod> *fl = static_cast<QFlags<QGraphicsScene::ItemIndexMethod>*>( static_cast<void*>(&static_cast<QFlags<QGraphicsScene::ItemIndexMethod>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QGraphicsScene::ItemIndexMethod>(lqtL_toenum(L, i, "QGraphicsScene::ItemIndexMethod"));
	}
	if (luaL_newmetatable(L, "QFlags<QGraphicsScene::ItemIndexMethod>*")) {
		lua_pushstring(L, "QFlags<QGraphicsScene::ItemIndexMethod>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int LuaBinder< QGraphicsScene >::lqt_pushenum_SceneLayer (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "ItemLayer");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "ItemLayer");
  lua_pushstring(L, "BackgroundLayer");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "BackgroundLayer");
  lua_pushstring(L, "ForegroundLayer");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "ForegroundLayer");
  lua_pushstring(L, "AllLayers");
  lua_rawseti(L, enum_table, 65535);
  lua_pushinteger(L, 65535);
  lua_setfield(L, enum_table, "AllLayers");
  lua_pushcfunction(L, LuaBinder< QGraphicsScene >::lqt_pushenum_SceneLayer_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QGraphicsScene::SceneLayer");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QGraphicsScene >::lqt_pushenum_SceneLayer_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QGraphicsScene::SceneLayer>*) + sizeof(QFlags<QGraphicsScene::SceneLayer>));
  QFlags<QGraphicsScene::SceneLayer> *fl = static_cast<QFlags<QGraphicsScene::SceneLayer>*>( static_cast<void*>(&static_cast<QFlags<QGraphicsScene::SceneLayer>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QGraphicsScene::SceneLayer>(lqtL_toenum(L, i, "QGraphicsScene::SceneLayer"));
	}
	if (luaL_newmetatable(L, "QFlags<QGraphicsScene::SceneLayer>*")) {
		lua_pushstring(L, "QFlags<QGraphicsScene::SceneLayer>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int luaopen_QGraphicsScene (lua_State *L) {
  if (luaL_newmetatable(L, "QGraphicsScene*")) {
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__render);
    lua_setfield(L, -2, "render");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__addPixmap);
    lua_setfield(L, -2, "addPixmap");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__focusItem);
    lua_setfield(L, -2, "focusItem");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__clearSelection);
    lua_setfield(L, -2, "clearSelection");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__update);
    lua_setfield(L, -2, "update");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__sceneRect);
    lua_setfield(L, -2, "sceneRect");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__addLine);
    lua_setfield(L, -2, "addLine");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__itemsBoundingRect);
    lua_setfield(L, -2, "itemsBoundingRect");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__trUtf8);
    lua_setfield(L, -2, "trUtf8");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__addRect);
    lua_setfield(L, -2, "addRect");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__advance);
    lua_setfield(L, -2, "advance");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__selectionArea);
    lua_setfield(L, -2, "selectionArea");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__setSelectionArea);
    lua_setfield(L, -2, "setSelectionArea");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__setItemIndexMethod);
    lua_setfield(L, -2, "setItemIndexMethod");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__hasFocus);
    lua_setfield(L, -2, "hasFocus");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__bspTreeDepth);
    lua_setfield(L, -2, "bspTreeDepth");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__collidingItems);
    lua_setfield(L, -2, "collidingItems");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__height);
    lua_setfield(L, -2, "height");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__addEllipse);
    lua_setfield(L, -2, "addEllipse");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__addSimpleText);
    lua_setfield(L, -2, "addSimpleText");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__setSceneRect);
    lua_setfield(L, -2, "setSceneRect");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__destroyItemGroup);
    lua_setfield(L, -2, "destroyItemGroup");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__createItemGroup);
    lua_setfield(L, -2, "createItemGroup");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__invalidate);
    lua_setfield(L, -2, "invalidate");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__addItem);
    lua_setfield(L, -2, "addItem");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__setFocusItem);
    lua_setfield(L, -2, "setFocusItem");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__setFocus);
    lua_setfield(L, -2, "setFocus");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__items);
    lua_setfield(L, -2, "items");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__addText);
    lua_setfield(L, -2, "addText");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__metaObject);
    lua_setfield(L, -2, "metaObject");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__itemIndexMethod);
    lua_setfield(L, -2, "itemIndexMethod");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__backgroundBrush);
    lua_setfield(L, -2, "backgroundBrush");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__new);
    lua_setfield(L, -2, "new");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__inputMethodQuery);
    lua_setfield(L, -2, "inputMethodQuery");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__delete);
    lua_setfield(L, -2, "delete");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__addPath);
    lua_setfield(L, -2, "addPath");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__foregroundBrush);
    lua_setfield(L, -2, "foregroundBrush");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__selectedItems);
    lua_setfield(L, -2, "selectedItems");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__mouseGrabberItem);
    lua_setfield(L, -2, "mouseGrabberItem");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__setBspTreeDepth);
    lua_setfield(L, -2, "setBspTreeDepth");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__width);
    lua_setfield(L, -2, "width");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__setForegroundBrush);
    lua_setfield(L, -2, "setForegroundBrush");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__itemAt);
    lua_setfield(L, -2, "itemAt");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__setBackgroundBrush);
    lua_setfield(L, -2, "setBackgroundBrush");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__tr);
    lua_setfield(L, -2, "tr");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__views);
    lua_setfield(L, -2, "views");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__removeItem);
    lua_setfield(L, -2, "removeItem");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__addPolygon);
    lua_setfield(L, -2, "addPolygon");
    lua_pushcfunction(L, LuaBinder< QGraphicsScene >::__LuaWrapCall__clearFocus);
    lua_setfield(L, -2, "clearFocus");
    LuaBinder< QGraphicsScene >::lqt_pushenum_ItemIndexMethod(L);
    lua_setfield(L, -2, "ItemIndexMethod");
    LuaBinder< QGraphicsScene >::lqt_pushenum_SceneLayer(L);
    lua_setfield(L, -2, "SceneLayer");
    lua_newtable(L);
    lua_pushboolean(L, 1);
    lua_setfield(L, -2, "QObject*");
    lua_setfield(L, -2, "__base");
    lua_pushcfunction(L, lqtL_newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, lqtL_index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, lqtL_gc);
    lua_setfield(L, -2, "__gc");
    lua_pushstring(L, "QGraphicsScene");
    lua_setfield(L, -2, "__qtype");
		lua_setglobal(L, "QGraphicsScene");
  } else {
		lua_pop(L, 1);
	}
  return 0;
}
