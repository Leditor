#include "lqt_common.hpp"
#include <QMenu>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QMenu > : public QMenu {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__sizeHint (lua_State *L);
  static int __LuaWrapCall__defaultAction (lua_State *L);
  static int __LuaWrapCall__isTearOffMenuVisible (lua_State *L);
  static int __LuaWrapCall__isTearOffEnabled (lua_State *L);
  static int __LuaWrapCall__setIcon (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__trUtf8 (lua_State *L);
  static int __LuaWrapCall__actionAt (lua_State *L);
  static int __LuaWrapCall__icon (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__tr (lua_State *L);
  static int __LuaWrapCall__isEmpty (lua_State *L);
  static int __LuaWrapCall__popup (lua_State *L);
  static int __LuaWrapCall__title (lua_State *L);
  static int __LuaWrapCall__setDefaultAction (lua_State *L);
  static int __LuaWrapCall__setNoReplayFor (lua_State *L);
  static int __LuaWrapCall__setSeparatorsCollapsible (lua_State *L);
  static int __LuaWrapCall__addAction__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__addAction__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__addAction__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__addAction__OverloadedVersion__4 (lua_State *L);
  static int __LuaWrapCall__addAction (lua_State *L);
  static int __LuaWrapCall__insertMenu (lua_State *L);
  static int __LuaWrapCall__clear (lua_State *L);
  static int __LuaWrapCall__delete (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__new (lua_State *L);
  static int __LuaWrapCall__actionGeometry (lua_State *L);
  static int __LuaWrapCall__addSeparator (lua_State *L);
  static int __LuaWrapCall__insertSeparator (lua_State *L);
  static int __LuaWrapCall__metaObject (lua_State *L);
  static int __LuaWrapCall__hideTearOffMenu (lua_State *L);
  static int __LuaWrapCall__addMenu__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__addMenu__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__addMenu__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__addMenu (lua_State *L);
  static int __LuaWrapCall__setTearOffEnabled (lua_State *L);
  static int __LuaWrapCall__separatorsCollapsible (lua_State *L);
  static int __LuaWrapCall__setActiveAction (lua_State *L);
  static int __LuaWrapCall__menuAction (lua_State *L);
  static int __LuaWrapCall__exec__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__exec__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__exec__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__exec (lua_State *L);
  static int __LuaWrapCall__setTitle (lua_State *L);
  static int __LuaWrapCall__activeAction (lua_State *L);
protected:
  void styleChange (QStyle& arg1);
public:
protected:
  void focusInEvent (QFocusEvent * arg1);
public:
protected:
  void keyPressEvent (QKeyEvent * arg1);
public:
  const QMetaObject * metaObject () const;
  int devType () const;
  void setVisible (bool arg1);
protected:
  bool focusNextPrevChild (bool arg1);
public:
  QPaintEngine * paintEngine () const;
protected:
  void dragLeaveEvent (QDragLeaveEvent * arg1);
public:
protected:
  void mousePressEvent (QMouseEvent * arg1);
public:
  QSize sizeHint () const;
protected:
  void changeEvent (QEvent * arg1);
public:
protected:
  void tabletEvent (QTabletEvent * arg1);
public:
protected:
  void customEvent (QEvent * arg1);
public:
protected:
  void dropEvent (QDropEvent * arg1);
public:
  QSize minimumSizeHint () const;
protected:
  void windowActivationChange (bool arg1);
public:
protected:
  void keyReleaseEvent (QKeyEvent * arg1);
public:
protected:
  void showEvent (QShowEvent * arg1);
public:
protected:
  void mouseDoubleClickEvent (QMouseEvent * arg1);
public:
protected:
  void paintEvent (QPaintEvent * arg1);
public:
protected:
  void dragMoveEvent (QDragMoveEvent * arg1);
public:
protected:
  void contextMenuEvent (QContextMenuEvent * arg1);
public:
protected:
  void enabledChange (bool arg1);
public:
protected:
  void resizeEvent (QResizeEvent * arg1);
public:
  int heightForWidth (int arg1) const;
protected:
  void fontChange (const QFont& arg1);
public:
protected:
  void mouseReleaseEvent (QMouseEvent * arg1);
public:
protected:
  void timerEvent (QTimerEvent * arg1);
public:
protected:
  void mouseMoveEvent (QMouseEvent * arg1);
public:
protected:
  void disconnectNotify (const char * arg1);
public:
protected:
  void childEvent (QChildEvent * arg1);
public:
protected:
  void focusOutEvent (QFocusEvent * arg1);
public:
protected:
  int metric (QPaintDevice::PaintDeviceMetric arg1) const;
public:
protected:
  void connectNotify (const char * arg1);
public:
  bool eventFilter (QObject * arg1, QEvent * arg2);
protected:
  void inputMethodEvent (QInputMethodEvent * arg1);
public:
protected:
  void wheelEvent (QWheelEvent * arg1);
public:
protected:
  void actionEvent (QActionEvent * arg1);
public:
protected:
  void paletteChange (const QPalette& arg1);
public:
protected:
  void languageChange ();
public:
protected:
  bool event (QEvent * arg1);
public:
protected:
  void hideEvent (QHideEvent * arg1);
public:
protected:
  void dragEnterEvent (QDragEnterEvent * arg1);
public:
protected:
  void moveEvent (QMoveEvent * arg1);
public:
protected:
  void closeEvent (QCloseEvent * arg1);
public:
protected:
  void leaveEvent (QEvent * arg1);
public:
  QVariant inputMethodQuery (Qt::InputMethodQuery arg1) const;
protected:
  void enterEvent (QEvent * arg1);
public:
  ~LuaBinder< QMenu > ();
  LuaBinder< QMenu > (lua_State *l, QWidget * arg1):QMenu(arg1), L(l) {}
  LuaBinder< QMenu > (lua_State *l, const QString& arg1, QWidget * arg2):QMenu(arg1, arg2), L(l) {}
};

extern "C" int luaopen_QMenu (lua_State *L);
