#include "lqt_common.hpp"
#include <QVBoxLayout>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QVBoxLayout > : public QVBoxLayout {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__tr (lua_State *L);
  static int __LuaWrapCall__delete (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__trUtf8 (lua_State *L);
  static int __LuaWrapCall__metaObject (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__new (lua_State *L);
  QLayout * layout ();
  QLayoutItem * takeAt (int arg1);
  void setGeometry (const QRect& arg1);
  QSize minimumSize () const;
  int heightForWidth (int arg1) const;
  void invalidate ();
protected:
  void timerEvent (QTimerEvent * arg1);
public:
protected:
  void childEvent (QChildEvent * arg1);
public:
  const QMetaObject * metaObject () const;
  int minimumHeightForWidth (int arg1) const;
protected:
  void connectNotify (const char * arg1);
public:
  bool hasHeightForWidth () const;
  void addItem (QLayoutItem * arg1);
  bool eventFilter (QObject * arg1, QEvent * arg2);
  QSpacerItem * spacerItem ();
  QSize sizeHint () const;
  QRect geometry () const;
  bool isEmpty () const;
protected:
  void disconnectNotify (const char * arg1);
public:
  bool event (QEvent * arg1);
  QWidget * widget ();
  int count () const;
  int indexOf (QWidget * arg1) const;
  QFlags<Qt::Orientation> expandingDirections () const;
  QLayoutItem * itemAt (int arg1) const;
  QSize maximumSize () const;
protected:
  void customEvent (QEvent * arg1);
public:
  ~LuaBinder< QVBoxLayout > ();
  LuaBinder< QVBoxLayout > (lua_State *l):QVBoxLayout(), L(l) {}
  LuaBinder< QVBoxLayout > (lua_State *l, QWidget * arg1):QVBoxLayout(arg1), L(l) {}
};

extern "C" int luaopen_QVBoxLayout (lua_State *L);
