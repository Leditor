#include "lqt_common.hpp"
#include <QAbstractScrollArea>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QAbstractScrollArea > : public QAbstractScrollArea {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__setHorizontalScrollBarPolicy (lua_State *L);
  static int __LuaWrapCall__setCornerWidget (lua_State *L);
  static int __LuaWrapCall__sizeHint (lua_State *L);
  static int __LuaWrapCall__setHorizontalScrollBar (lua_State *L);
  static int __LuaWrapCall__minimumSizeHint (lua_State *L);
  static int __LuaWrapCall__horizontalScrollBar (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__trUtf8 (lua_State *L);
  static int __LuaWrapCall__maximumViewportSize (lua_State *L);
  static int __LuaWrapCall__setVerticalScrollBarPolicy (lua_State *L);
  static int __LuaWrapCall__new (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__tr (lua_State *L);
  static int __LuaWrapCall__scrollBarWidgets (lua_State *L);
  static int __LuaWrapCall__cornerWidget (lua_State *L);
  static int __LuaWrapCall__verticalScrollBar (lua_State *L);
  static int __LuaWrapCall__addScrollBarWidget (lua_State *L);
  static int __LuaWrapCall__setViewport (lua_State *L);
  static int __LuaWrapCall__delete (lua_State *L);
  static int __LuaWrapCall__metaObject (lua_State *L);
  static int __LuaWrapCall__viewport (lua_State *L);
  static int __LuaWrapCall__verticalScrollBarPolicy (lua_State *L);
  static int __LuaWrapCall__horizontalScrollBarPolicy (lua_State *L);
  static int __LuaWrapCall__setVerticalScrollBar (lua_State *L);
protected:
  void styleChange (QStyle& arg1);
public:
protected:
  void focusInEvent (QFocusEvent * arg1);
public:
protected:
  void keyPressEvent (QKeyEvent * arg1);
public:
  const QMetaObject * metaObject () const;
  int devType () const;
  void setVisible (bool arg1);
protected:
  bool focusNextPrevChild (bool arg1);
public:
  QPaintEngine * paintEngine () const;
protected:
  void dragLeaveEvent (QDragLeaveEvent * arg1);
public:
protected:
  void mousePressEvent (QMouseEvent * arg1);
public:
protected:
  void dragMoveEvent (QDragMoveEvent * arg1);
public:
protected:
  void closeEvent (QCloseEvent * arg1);
public:
protected:
  void enterEvent (QEvent * arg1);
public:
protected:
  void tabletEvent (QTabletEvent * arg1);
public:
protected:
  void dropEvent (QDropEvent * arg1);
public:
protected:
  void moveEvent (QMoveEvent * arg1);
public:
  QVariant inputMethodQuery (Qt::InputMethodQuery arg1) const;
  QSize minimumSizeHint () const;
protected:
  void changeEvent (QEvent * arg1);
public:
protected:
  void contextMenuEvent (QContextMenuEvent * arg1);
public:
protected:
  void showEvent (QShowEvent * arg1);
public:
protected:
  void windowActivationChange (bool arg1);
public:
protected:
  void paintEvent (QPaintEvent * arg1);
public:
protected:
  void actionEvent (QActionEvent * arg1);
public:
protected:
  void keyReleaseEvent (QKeyEvent * arg1);
public:
protected:
  void enabledChange (bool arg1);
public:
protected:
  void mouseReleaseEvent (QMouseEvent * arg1);
public:
  int heightForWidth (int arg1) const;
protected:
  void mouseDoubleClickEvent (QMouseEvent * arg1);
public:
protected:
  int metric (QPaintDevice::PaintDeviceMetric arg1) const;
public:
protected:
  void disconnectNotify (const char * arg1);
public:
protected:
  void mouseMoveEvent (QMouseEvent * arg1);
public:
protected:
  void focusOutEvent (QFocusEvent * arg1);
public:
protected:
  void childEvent (QChildEvent * arg1);
public:
protected:
  void connectNotify (const char * arg1);
public:
protected:
  void timerEvent (QTimerEvent * arg1);
public:
protected:
  void fontChange (const QFont& arg1);
public:
  bool eventFilter (QObject * arg1, QEvent * arg2);
protected:
  void inputMethodEvent (QInputMethodEvent * arg1);
public:
protected:
  void wheelEvent (QWheelEvent * arg1);
public:
protected:
  void resizeEvent (QResizeEvent * arg1);
public:
protected:
  void paletteChange (const QPalette& arg1);
public:
protected:
  void languageChange ();
public:
protected:
  bool event (QEvent * arg1);
public:
protected:
  void hideEvent (QHideEvent * arg1);
public:
protected:
  void dragEnterEvent (QDragEnterEvent * arg1);
public:
protected:
  void scrollContentsBy (int arg1, int arg2);
public:
protected:
  bool viewportEvent (QEvent * arg1);
public:
protected:
  void leaveEvent (QEvent * arg1);
public:
  QSize sizeHint () const;
protected:
  void customEvent (QEvent * arg1);
public:
  ~LuaBinder< QAbstractScrollArea > ();
  LuaBinder< QAbstractScrollArea > (lua_State *l, QWidget * arg1):QAbstractScrollArea(arg1), L(l) {}
};

extern "C" int luaopen_QAbstractScrollArea (lua_State *L);
