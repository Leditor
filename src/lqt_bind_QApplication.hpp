#include "lqt_common.hpp"
#include <QApplication>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QApplication > : public QApplication {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__setWheelScrollLines (lua_State *L);
  static int __LuaWrapCall__styleSheet (lua_State *L);
  static int __LuaWrapCall__colorSpec (lua_State *L);
  static int __LuaWrapCall__doubleClickInterval (lua_State *L);
  static int __LuaWrapCall__activePopupWidget (lua_State *L);
  static int __LuaWrapCall__setStyle__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__setStyle__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__setStyle (lua_State *L);
  static int __LuaWrapCall__keyboardInputDirection (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__tr (lua_State *L);
  static int __LuaWrapCall__sessionId (lua_State *L);
  static int __LuaWrapCall__palette__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__palette__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__palette__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__palette (lua_State *L);
  static int __LuaWrapCall__setStartDragDistance (lua_State *L);
  static int __LuaWrapCall__style (lua_State *L);
  static int __LuaWrapCall__setGlobalStrut (lua_State *L);
  static int __LuaWrapCall__fontMetrics (lua_State *L);
  static int __LuaWrapCall__setQuitOnLastWindowClosed (lua_State *L);
  static int __LuaWrapCall__keyboardInputInterval (lua_State *L);
  static int __LuaWrapCall__beep (lua_State *L);
  static int __LuaWrapCall__aboutQt (lua_State *L);
  static int __LuaWrapCall__startDragTime (lua_State *L);
  static int __LuaWrapCall__metaObject (lua_State *L);
  static int __LuaWrapCall__delete (lua_State *L);
  static int __LuaWrapCall__mouseButtons (lua_State *L);
  static int __LuaWrapCall__keyboardModifiers (lua_State *L);
  static int __LuaWrapCall__setOverrideCursor (lua_State *L);
  static int __LuaWrapCall__focusWidget (lua_State *L);
  static int __LuaWrapCall__overrideCursor (lua_State *L);
  static int __LuaWrapCall__exec (lua_State *L);
  static int __LuaWrapCall__setEffectEnabled (lua_State *L);
  static int __LuaWrapCall__saveState (lua_State *L);
  static int __LuaWrapCall__setDesktopSettingsAware (lua_State *L);
  static int __LuaWrapCall__sessionKey (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__trUtf8 (lua_State *L);
  static int __LuaWrapCall__topLevelWidgets (lua_State *L);
  static int __LuaWrapCall__desktop (lua_State *L);
  static int __LuaWrapCall__quitOnLastWindowClosed (lua_State *L);
  static int __LuaWrapCall__setPalette (lua_State *L);
  static int __LuaWrapCall__isLeftToRight (lua_State *L);
  static int __LuaWrapCall__layoutDirection (lua_State *L);
  static int __LuaWrapCall__keyboardInputLocale (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__4 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__5 (lua_State *L);
  static int __LuaWrapCall__new (lua_State *L);
  static int __LuaWrapCall__wheelScrollLines (lua_State *L);
  static int __LuaWrapCall__type (lua_State *L);
  static int __LuaWrapCall__syncX (lua_State *L);
  static int __LuaWrapCall__setDoubleClickInterval (lua_State *L);
  static int __LuaWrapCall__setColorSpec (lua_State *L);
  static int __LuaWrapCall__windowIcon (lua_State *L);
  static int __LuaWrapCall__restoreOverrideCursor (lua_State *L);
  static int __LuaWrapCall__setKeyboardInputInterval (lua_State *L);
  static int __LuaWrapCall__setActiveWindow (lua_State *L);
  static int __LuaWrapCall__cursorFlashTime (lua_State *L);
  static int __LuaWrapCall__globalStrut (lua_State *L);
  static int __LuaWrapCall__font__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__font__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__font__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__font (lua_State *L);
  static int __LuaWrapCall__desktopSettingsAware (lua_State *L);
  static int __LuaWrapCall__topLevelAt__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__topLevelAt__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__topLevelAt (lua_State *L);
  static int __LuaWrapCall__setCursorFlashTime (lua_State *L);
  static int __LuaWrapCall__setInputContext (lua_State *L);
  static int __LuaWrapCall__startDragDistance (lua_State *L);
  static int __LuaWrapCall__clipboard (lua_State *L);
  static int __LuaWrapCall__activeWindow (lua_State *L);
  static int __LuaWrapCall__changeOverrideCursor (lua_State *L);
  static int __LuaWrapCall__isSessionRestored (lua_State *L);
  static int __LuaWrapCall__activeModalWidget (lua_State *L);
  static int __LuaWrapCall__isEffectEnabled (lua_State *L);
  static int __LuaWrapCall__inputContext (lua_State *L);
  static int __LuaWrapCall__widgetAt__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__widgetAt__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__widgetAt (lua_State *L);
  static int __LuaWrapCall__setFont (lua_State *L);
  static int __LuaWrapCall__setStartDragTime (lua_State *L);
  static int __LuaWrapCall__allWidgets (lua_State *L);
  static int __LuaWrapCall__setWindowIcon (lua_State *L);
  static int __LuaWrapCall__commitData (lua_State *L);
  static int __LuaWrapCall__isRightToLeft (lua_State *L);
  static int __LuaWrapCall__notify (lua_State *L);
  static int __LuaWrapCall__setLayoutDirection (lua_State *L);
  static int __LuaWrapCall__closeAllWindows (lua_State *L);
  static int __LuaWrapCall__alert (lua_State *L);
  static int __LuaWrapCall__setStyleSheet (lua_State *L);
  void commitData (QSessionManager& arg1);
  bool notify (QObject * arg1, QEvent * arg2);
  bool eventFilter (QObject * arg1, QEvent * arg2);
  const QMetaObject * metaObject () const;
protected:
  bool event (QEvent * arg1);
public:
protected:
  void connectNotify (const char * arg1);
public:
  void saveState (QSessionManager& arg1);
protected:
  void timerEvent (QTimerEvent * arg1);
public:
protected:
  bool compressEvent (QEvent * arg1, QObject * arg2, QPostEventList * arg3);
public:
protected:
  void disconnectNotify (const char * arg1);
public:
protected:
  void childEvent (QChildEvent * arg1);
public:
protected:
  void customEvent (QEvent * arg1);
public:
  ~LuaBinder< QApplication > ();
  static int lqt_pushenum_Type (lua_State *L);
  static int lqt_pushenum_Type_QFLAGS_CREATOR (lua_State *L);
  static int lqt_pushenum_ColorSpec (lua_State *L);
  static int lqt_pushenum_ColorSpec_QFLAGS_CREATOR (lua_State *L);
  LuaBinder< QApplication > (lua_State *l, int& arg1, char * * arg2, int arg3):QApplication(arg1, arg2, arg3), L(l) {}
  LuaBinder< QApplication > (lua_State *l, int& arg1, char * * arg2, bool arg3, int arg4):QApplication(arg1, arg2, arg3, arg4), L(l) {}
  LuaBinder< QApplication > (lua_State *l, int& arg1, char * * arg2, QApplication::Type arg3, int arg4):QApplication(arg1, arg2, arg3, arg4), L(l) {}
  LuaBinder< QApplication > (lua_State *l, _XDisplay * arg1, long unsigned int arg2, long unsigned int arg3, int arg4):QApplication(arg1, arg2, arg3, arg4), L(l) {}
  LuaBinder< QApplication > (lua_State *l, _XDisplay * arg1, int& arg2, char * * arg3, long unsigned int arg4, long unsigned int arg5, int arg6):QApplication(arg1, arg2, arg3, arg4, arg5, arg6), L(l) {}
};

extern "C" int luaopen_QApplication (lua_State *L);
