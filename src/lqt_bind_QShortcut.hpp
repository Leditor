#include "lqt_common.hpp"
#include <QShortcut>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QShortcut > : public QShortcut {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__parentWidget (lua_State *L);
  static int __LuaWrapCall__setContext (lua_State *L);
  static int __LuaWrapCall__key (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__trUtf8 (lua_State *L);
  static int __LuaWrapCall__context (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__new (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__tr (lua_State *L);
  static int __LuaWrapCall__delete (lua_State *L);
  static int __LuaWrapCall__setKey (lua_State *L);
  static int __LuaWrapCall__whatsThis (lua_State *L);
  static int __LuaWrapCall__autoRepeat (lua_State *L);
  static int __LuaWrapCall__id (lua_State *L);
  static int __LuaWrapCall__setWhatsThis (lua_State *L);
  static int __LuaWrapCall__isEnabled (lua_State *L);
  static int __LuaWrapCall__setEnabled (lua_State *L);
  static int __LuaWrapCall__setAutoRepeat (lua_State *L);
  static int __LuaWrapCall__metaObject (lua_State *L);
protected:
  bool event (QEvent * arg1);
public:
protected:
  void connectNotify (const char * arg1);
public:
  bool eventFilter (QObject * arg1, QEvent * arg2);
protected:
  void disconnectNotify (const char * arg1);
public:
protected:
  void childEvent (QChildEvent * arg1);
public:
protected:
  void timerEvent (QTimerEvent * arg1);
public:
  const QMetaObject * metaObject () const;
protected:
  void customEvent (QEvent * arg1);
public:
  ~LuaBinder< QShortcut > ();
  LuaBinder< QShortcut > (lua_State *l, QWidget * arg1):QShortcut(arg1), L(l) {}
  LuaBinder< QShortcut > (lua_State *l, const QKeySequence& arg1, QWidget * arg2, const char * arg3, const char * arg4, Qt::ShortcutContext arg5):QShortcut(arg1, arg2, arg3, arg4, arg5), L(l) {}
};

extern "C" int luaopen_QShortcut (lua_State *L);
