#include "lqt_common.hpp"
#include <QKeySequence>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QKeySequence > : public QKeySequence {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__fromString (lua_State *L);
  static int __LuaWrapCall__isDetached (lua_State *L);
  static int __LuaWrapCall__toString (lua_State *L);
  static int __LuaWrapCall__delete (lua_State *L);
  static int __LuaWrapCall__count (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__4 (lua_State *L);
  static int __LuaWrapCall__new__OverloadedVersion__5 (lua_State *L);
  static int __LuaWrapCall__new (lua_State *L);
  static int __LuaWrapCall__isEmpty (lua_State *L);
  static int __LuaWrapCall__matches (lua_State *L);
  static int __LuaWrapCall__mnemonic (lua_State *L);
  static int __LuaWrapCall__keyBindings (lua_State *L);
  static int lqt_pushenum_StandardKey (lua_State *L);
  static int lqt_pushenum_StandardKey_QFLAGS_CREATOR (lua_State *L);
  static int lqt_pushenum_SequenceMatch (lua_State *L);
  static int lqt_pushenum_SequenceMatch_QFLAGS_CREATOR (lua_State *L);
  static int lqt_pushenum_SequenceFormat (lua_State *L);
  static int lqt_pushenum_SequenceFormat_QFLAGS_CREATOR (lua_State *L);
  LuaBinder< QKeySequence > (lua_State *l):QKeySequence(), L(l) {}
  LuaBinder< QKeySequence > (lua_State *l, const QString& arg1):QKeySequence(arg1), L(l) {}
  LuaBinder< QKeySequence > (lua_State *l, int arg1, int arg2, int arg3, int arg4):QKeySequence(arg1, arg2, arg3, arg4), L(l) {}
  LuaBinder< QKeySequence > (lua_State *l, const QKeySequence& arg1):QKeySequence(arg1), L(l) {}
  LuaBinder< QKeySequence > (lua_State *l, QKeySequence::StandardKey arg1):QKeySequence(arg1), L(l) {}
};

extern "C" int luaopen_QKeySequence (lua_State *L);
