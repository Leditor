#include "lqt_common.hpp"
#include <QLayoutItem>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QLayoutItem > : public QLayoutItem {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__invalidate (lua_State *L);
  static int __LuaWrapCall__controlTypes (lua_State *L);
  static int __LuaWrapCall__heightForWidth (lua_State *L);
  static int __LuaWrapCall__spacerItem (lua_State *L);
  static int __LuaWrapCall__delete (lua_State *L);
  static int __LuaWrapCall__layout (lua_State *L);
  static int __LuaWrapCall__setAlignment (lua_State *L);
  static int __LuaWrapCall__minimumHeightForWidth (lua_State *L);
  static int __LuaWrapCall__alignment (lua_State *L);
  static int __LuaWrapCall__widget (lua_State *L);
  static int __LuaWrapCall__hasHeightForWidth (lua_State *L);
  bool hasHeightForWidth () const;
  QLayout * layout ();
  QSpacerItem * spacerItem ();
  QSize sizeHint () const;
  void setGeometry (const QRect& arg1);
  QSize minimumSize () const;
  int heightForWidth (int arg1) const;
  QWidget * widget ();
  bool isEmpty () const;
  void invalidate ();
  QFlags<Qt::Orientation> expandingDirections () const;
  QRect geometry () const;
  int minimumHeightForWidth (int arg1) const;
  QSize maximumSize () const;
  ~LuaBinder< QLayoutItem > ();
  LuaBinder< QLayoutItem > (lua_State *l, QFlags<Qt::AlignmentFlag> arg1):QLayoutItem(arg1), L(l) {}
};

extern "C" int luaopen_QLayoutItem (lua_State *L);
