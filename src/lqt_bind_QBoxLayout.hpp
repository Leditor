#include "lqt_common.hpp"
#include <QBoxLayout>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QBoxLayout > : public QBoxLayout {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__insertLayout (lua_State *L);
  static int __LuaWrapCall__takeAt (lua_State *L);
  static int __LuaWrapCall__delete (lua_State *L);
  static int __LuaWrapCall__sizeHint (lua_State *L);
  static int __LuaWrapCall__setSpacing (lua_State *L);
  static int __LuaWrapCall__minimumSize (lua_State *L);
  static int __LuaWrapCall__invalidate (lua_State *L);
  static int __LuaWrapCall__direction (lua_State *L);
  static int __LuaWrapCall__spacing (lua_State *L);
  static int __LuaWrapCall__addSpacing (lua_State *L);
  static int __LuaWrapCall__insertWidget (lua_State *L);
  static int __LuaWrapCall__addStretch (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__trUtf8 (lua_State *L);
  static int __LuaWrapCall__insertStretch (lua_State *L);
  static int __LuaWrapCall__metaObject (lua_State *L);
  static int __LuaWrapCall__new (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__tr (lua_State *L);
  static int __LuaWrapCall__addStrut (lua_State *L);
  static int __LuaWrapCall__minimumHeightForWidth (lua_State *L);
  static int __LuaWrapCall__setDirection (lua_State *L);
  static int __LuaWrapCall__heightForWidth (lua_State *L);
  static int __LuaWrapCall__addItem (lua_State *L);
  static int __LuaWrapCall__setStretchFactor__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__setStretchFactor__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__setStretchFactor (lua_State *L);
  static int __LuaWrapCall__expandingDirections (lua_State *L);
  static int __LuaWrapCall__insertSpacing (lua_State *L);
  static int __LuaWrapCall__addLayout (lua_State *L);
  static int __LuaWrapCall__count (lua_State *L);
  static int __LuaWrapCall__addWidget (lua_State *L);
  static int __LuaWrapCall__maximumSize (lua_State *L);
  static int __LuaWrapCall__setGeometry (lua_State *L);
  static int __LuaWrapCall__itemAt (lua_State *L);
  static int __LuaWrapCall__hasHeightForWidth (lua_State *L);
  QLayout * layout ();
  QLayoutItem * takeAt (int arg1);
  void setGeometry (const QRect& arg1);
  QLayoutItem * itemAt (int arg1) const;
  int heightForWidth (int arg1) const;
  QSize minimumSize () const;
  void invalidate ();
protected:
  void connectNotify (const char * arg1);
public:
protected:
  void timerEvent (QTimerEvent * arg1);
public:
  int minimumHeightForWidth (int arg1) const;
  QSize maximumSize () const;
  bool hasHeightForWidth () const;
  void addItem (QLayoutItem * arg1);
  bool eventFilter (QObject * arg1, QEvent * arg2);
  QSpacerItem * spacerItem ();
  QSize sizeHint () const;
  int indexOf (QWidget * arg1) const;
  bool isEmpty () const;
protected:
  void disconnectNotify (const char * arg1);
public:
  bool event (QEvent * arg1);
  QWidget * widget ();
  int count () const;
  QRect geometry () const;
  QFlags<Qt::Orientation> expandingDirections () const;
protected:
  void childEvent (QChildEvent * arg1);
public:
  const QMetaObject * metaObject () const;
protected:
  void customEvent (QEvent * arg1);
public:
  ~LuaBinder< QBoxLayout > ();
  static int lqt_pushenum_Direction (lua_State *L);
  static int lqt_pushenum_Direction_QFLAGS_CREATOR (lua_State *L);
  LuaBinder< QBoxLayout > (lua_State *l, QBoxLayout::Direction arg1, QWidget * arg2):QBoxLayout(arg1, arg2), L(l) {}
};

extern "C" int luaopen_QBoxLayout (lua_State *L);
