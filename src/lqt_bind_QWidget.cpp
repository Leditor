#include "lqt_bind_QWidget.hpp"

int LuaBinder< QWidget >::__LuaWrapCall__windowFlags (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<Qt::WindowType> ret = __lua__obj->QWidget::windowFlags();
  lqtL_passudata(L, new QFlags<Qt::WindowType>(ret), "QFlags<Qt::WindowType>*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__setShown (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QWidget::setShown(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__minimumSize (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QSize ret = __lua__obj->QWidget::minimumSize();
  lqtL_passudata(L, new QSize(ret), "QSize*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__showMinimized (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QWidget::showMinimized();
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__statusTip (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QWidget::statusTip();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__styleSheet (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QWidget::styleSheet();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__childrenRegion (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QRegion ret = __lua__obj->QWidget::childrenRegion();
  lqtL_passudata(L, new QRegion(ret), "QRegion*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__mapTo (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 2, "QWidget*"));
  const QPoint& arg2 = **static_cast<QPoint**>(lqtL_checkudata(L, 3, "QPoint*"));
  QPoint ret = __lua__obj->QWidget::mapTo(arg1, arg2);
  lqtL_passudata(L, new QPoint(ret), "QPoint*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__setStyle (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QStyle * arg1 = *static_cast<QStyle**>(lqtL_checkudata(L, 2, "QStyle*"));
  __lua__obj->QWidget::setStyle(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__size (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QSize ret = __lua__obj->QWidget::size();
  lqtL_passudata(L, new QSize(ret), "QSize*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__tr__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QWidget::tr(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__tr__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QWidget::tr(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__tr (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1f92f50;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x1f92cc0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1f94970;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1f94e60;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1f95210;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__tr__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__tr__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__tr matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__isVisible (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QWidget::isVisible();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__whatsThis (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QWidget::whatsThis();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__layout (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QLayout * ret = __lua__obj->QWidget::layout();
  lqtL_pushudata(L, ret, "QLayout*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__palette (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPalette& ret = __lua__obj->QWidget::palette();
  lqtL_pushudata(L, &(ret), "QPalette*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__font (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QFont& ret = __lua__obj->QWidget::font();
  lqtL_pushudata(L, &(ret), "QFont*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__adjustSize (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QWidget::adjustSize();
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setAcceptDrops (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QWidget::setAcceptDrops(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__unsetLocale (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QWidget::unsetLocale();
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__pos (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QPoint ret = __lua__obj->QWidget::pos();
  lqtL_passudata(L, new QPoint(ret), "QPoint*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__setGeometry__OverloadedVersion__1 (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int arg2 = lua_tointeger(L, 3);
  int arg3 = lua_tointeger(L, 4);
  int arg4 = lua_tointeger(L, 5);
  __lua__obj->QWidget::setGeometry(arg1, arg2, arg3, arg4);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setGeometry__OverloadedVersion__2 (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QRect& arg1 = **static_cast<QRect**>(lqtL_checkudata(L, 2, "QRect*"));
  __lua__obj->QWidget::setGeometry(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setGeometry (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QWidget*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1ff59b0;
  } else {
    score[1] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1ff5e90;
  } else {
    score[1] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1ff6260;
  } else {
    score[1] -= premium*premium;
  }
  if (lua_isnumber(L, 5)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1ff6200;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QWidget*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QRect*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1ff6f10;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__setGeometry__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__setGeometry__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__setGeometry matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__windowIconText (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QWidget::windowIconText();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__isRightToLeft (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QWidget::isRightToLeft();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__mapFrom (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 2, "QWidget*"));
  const QPoint& arg2 = **static_cast<QPoint**>(lqtL_checkudata(L, 3, "QPoint*"));
  QPoint ret = __lua__obj->QWidget::mapFrom(arg1, arg2);
  lqtL_passudata(L, new QPoint(ret), "QPoint*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__inputContext (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QInputContext * ret = __lua__obj->QWidget::inputContext();
  lqtL_pushudata(L, ret, "QInputContext*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__windowState (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<Qt::WindowState> ret = __lua__obj->QWidget::windowState();
  lqtL_passudata(L, new QFlags<Qt::WindowState>(ret), "QFlags<Qt::WindowState>*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__minimumWidth (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QWidget::minimumWidth();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__addAction (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QAction * arg1 = *static_cast<QAction**>(lqtL_checkudata(L, 2, "QAction*"));
  __lua__obj->QWidget::addAction(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__isHidden (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QWidget::isHidden();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__delete (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  delete __lua__obj;
  __lua__obj = 0;
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setWindowTitle (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QWidget::setWindowTitle(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setStyleSheet (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QWidget::setStyleSheet(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__releaseShortcut (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QWidget::releaseShortcut(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setWhatsThis (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QWidget::setWhatsThis(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__isEnabled (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QWidget::isEnabled();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__addActions (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QList<QAction*> arg1 = **static_cast<QList<QAction*>**>(lqtL_checkudata(L, 2, "QList<QAction*>*"));
  __lua__obj->QWidget::addActions(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setWindowModified (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QWidget::setWindowModified(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setWindowIconText (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QWidget::setWindowIconText(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setWindowRole (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QWidget::setWindowRole(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__scroll__OverloadedVersion__1 (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int arg2 = lua_tointeger(L, 3);
  __lua__obj->QWidget::scroll(arg1, arg2);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__scroll__OverloadedVersion__2 (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int arg2 = lua_tointeger(L, 3);
  const QRect& arg3 = **static_cast<QRect**>(lqtL_checkudata(L, 4, "QRect*"));
  __lua__obj->QWidget::scroll(arg1, arg2, arg3);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__scroll (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QWidget*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x20081d0;
  } else {
    score[1] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x20086f0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QWidget*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x2008fd0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x20094e0;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_testudata(L, 4, "QRect*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x2009890;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__scroll__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__scroll__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__scroll matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__cursor (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QCursor ret = __lua__obj->QWidget::cursor();
  lqtL_passudata(L, new QCursor(ret), "QCursor*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__setFixedHeight (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QWidget::setFixedHeight(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setShortcutAutoRepeat (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  bool arg2 = lua_isboolean(L, 3)?(bool)lua_toboolean(L, 3):true;
  __lua__obj->QWidget::setShortcutAutoRepeat(arg1, arg2);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__trUtf8__OverloadedVersion__1 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = (lua_type(L, 2)==LUA_TSTRING)?lua_tostring(L, 2):static_cast< const char * >(0);
  QString ret = QWidget::trUtf8(arg1, arg2);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__trUtf8__OverloadedVersion__2 (lua_State *L) {
  const char * arg1 = lua_tostring(L, 1);
  const char * arg2 = lua_tostring(L, 2);
  int arg3 = lua_tointeger(L, 3);
  QString ret = QWidget::trUtf8(arg1, arg2, arg3);
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__trUtf8 (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1f93c90;
  } else {
    score[1] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[1] += premium;
  } else if (true) {
    score[1] += premium-1; // table: 0x1f937f0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  if ((lua_type(L, 1)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1f95a80;
  } else {
    score[2] -= premium*premium;
  }
  if ((lua_type(L, 2)==LUA_TSTRING)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1f951b0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1f95f10;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__trUtf8__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__trUtf8__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__trUtf8 matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__foregroundRole (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QPalette::ColorRole ret = __lua__obj->QWidget::foregroundRole();
  lqtL_pushenum(L, ret, "QPalette::ColorRole");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__mapToGlobal (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPoint& arg1 = **static_cast<QPoint**>(lqtL_checkudata(L, 2, "QPoint*"));
  QPoint ret = __lua__obj->QWidget::mapToGlobal(arg1);
  lqtL_passudata(L, new QPoint(ret), "QPoint*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__releaseMouse (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QWidget::releaseMouse();
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__isEnabledTo (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 2, "QWidget*"));
  bool ret = __lua__obj->QWidget::isEnabledTo(arg1);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__isLeftToRight (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QWidget::isLeftToRight();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__contextMenuPolicy (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::ContextMenuPolicy ret = __lua__obj->QWidget::contextMenuPolicy();
  lqtL_pushenum(L, ret, "Qt::ContextMenuPolicy");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__updatesEnabled (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QWidget::updatesEnabled();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__isAncestorOf (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 2, "QWidget*"));
  bool ret = __lua__obj->QWidget::isAncestorOf(arg1);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__normalGeometry (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QRect ret = __lua__obj->QWidget::normalGeometry();
  lqtL_passudata(L, new QRect(ret), "QRect*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__nextInFocusChain (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * ret = __lua__obj->QWidget::nextInFocusChain();
  lqtL_pushudata(L, ret, "QWidget*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__sizePolicy (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QSizePolicy ret = __lua__obj->QWidget::sizePolicy();
  lqtL_passudata(L, new QSizePolicy(ret), "QSizePolicy*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__overrideWindowState (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<Qt::WindowState> arg1 = **static_cast<QFlags<Qt::WindowState>**>(lqtL_checkudata(L, 2, "QFlags<Qt::WindowState>*"));
  __lua__obj->QWidget::overrideWindowState(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setMinimumSize__OverloadedVersion__1 (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QSize& arg1 = **static_cast<QSize**>(lqtL_checkudata(L, 2, "QSize*"));
  __lua__obj->QWidget::setMinimumSize(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setMinimumSize__OverloadedVersion__2 (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int arg2 = lua_tointeger(L, 3);
  __lua__obj->QWidget::setMinimumSize(arg1, arg2);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setMinimumSize (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QWidget*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QSize*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1fab1d0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QWidget*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1fabbf0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1fab6e0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__setMinimumSize__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__setMinimumSize__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__setMinimumSize matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__testAttribute (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::WidgetAttribute arg1 = static_cast<Qt::WidgetAttribute>(lqtL_toenum(L, 2, "Qt::WidgetAttribute"));
  bool ret = __lua__obj->QWidget::testAttribute(arg1);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__stackUnder (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 2, "QWidget*"));
  __lua__obj->QWidget::stackUnder(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__grabShortcut (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QKeySequence& arg1 = **static_cast<QKeySequence**>(lqtL_checkudata(L, 2, "QKeySequence*"));
  Qt::ShortcutContext arg2 = lqtL_isenum(L, 3, "Qt::ShortcutContext")?static_cast<Qt::ShortcutContext>(lqtL_toenum(L, 3, "Qt::ShortcutContext")):Qt::WindowShortcut;
  int ret = __lua__obj->QWidget::grabShortcut(arg1, arg2);
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__setMaximumSize__OverloadedVersion__1 (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QSize& arg1 = **static_cast<QSize**>(lqtL_checkudata(L, 2, "QSize*"));
  __lua__obj->QWidget::setMaximumSize(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setMaximumSize__OverloadedVersion__2 (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int arg2 = lua_tointeger(L, 3);
  __lua__obj->QWidget::setMaximumSize(arg1, arg2);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setMaximumSize (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QWidget*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QSize*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1facbb0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QWidget*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1fad5d0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1fad0c0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__setMaximumSize__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__setMaximumSize__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__setMaximumSize matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setBackgroundRole (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QPalette::ColorRole arg1 = static_cast<QPalette::ColorRole>(lqtL_toenum(L, 2, "QPalette::ColorRole"));
  __lua__obj->QWidget::setBackgroundRole(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__locale (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QLocale ret = __lua__obj->QWidget::locale();
  lqtL_passudata(L, new QLocale(ret), "QLocale*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__mouseGrabber (lua_State *L) {
  QWidget * ret = QWidget::mouseGrabber();
  lqtL_pushudata(L, ret, "QWidget*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__frameSize (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QSize ret = __lua__obj->QWidget::frameSize();
  lqtL_passudata(L, new QSize(ret), "QSize*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__setInputContext (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QInputContext * arg1 = *static_cast<QInputContext**>(lqtL_checkudata(L, 2, "QInputContext*"));
  __lua__obj->QWidget::setInputContext(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__repaint__OverloadedVersion__1 (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QWidget::repaint();
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__repaint__OverloadedVersion__2 (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int arg2 = lua_tointeger(L, 3);
  int arg3 = lua_tointeger(L, 4);
  int arg4 = lua_tointeger(L, 5);
  __lua__obj->QWidget::repaint(arg1, arg2, arg3, arg4);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__repaint__OverloadedVersion__3 (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QRect& arg1 = **static_cast<QRect**>(lqtL_checkudata(L, 2, "QRect*"));
  __lua__obj->QWidget::repaint(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__repaint__OverloadedVersion__4 (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QRegion& arg1 = **static_cast<QRegion**>(lqtL_checkudata(L, 2, "QRegion*"));
  __lua__obj->QWidget::repaint(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__repaint (lua_State *L) {
  int score[5];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QWidget*")?premium:-premium*premium;
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QWidget*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1fe97e0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1fe9cf0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1fea0a0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 5)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1fea040;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  score[3] += lqtL_testudata(L, 1, "QWidget*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QRect*")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1fead40;
  } else {
    score[3] -= premium*premium;
  }
  score[4] = 0;
  score[4] += lqtL_testudata(L, 1, "QWidget*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QRegion*")) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x1feb6b0;
  } else {
    score[4] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=4;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__repaint__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__repaint__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__repaint__OverloadedVersion__3(L); break;
    case 4: return __LuaWrapCall__repaint__OverloadedVersion__4(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__repaint matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__unsetCursor (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QWidget::unsetCursor();
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__winId (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  long unsigned int ret = __lua__obj->QWidget::winId();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__isWindowModified (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QWidget::isWindowModified();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__acceptDrops (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QWidget::acceptDrops();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__setFont (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QFont& arg1 = **static_cast<QFont**>(lqtL_checkudata(L, 2, "QFont*"));
  __lua__obj->QWidget::setFont(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setWindowIcon (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QIcon& arg1 = **static_cast<QIcon**>(lqtL_checkudata(L, 2, "QIcon*"));
  __lua__obj->QWidget::setWindowIcon(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setParent__OverloadedVersion__1 (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 2, "QWidget*"));
  __lua__obj->QWidget::setParent(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setParent__OverloadedVersion__2 (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 2, "QWidget*"));
  QFlags<Qt::WindowType> arg2 = **static_cast<QFlags<Qt::WindowType>**>(lqtL_checkudata(L, 3, "QFlags<Qt::WindowType>*"));
  __lua__obj->QWidget::setParent(arg1, arg2);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setParent (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QWidget*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QWidget*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x2006920;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QWidget*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QWidget*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x2007390;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_testudata(L, 3, "QFlags<Qt::WindowType>*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x2006e30;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__setParent__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__setParent__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__setParent matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__frameGeometry (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QRect ret = __lua__obj->QWidget::frameGeometry();
  lqtL_passudata(L, new QRect(ret), "QRect*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__setLayoutDirection (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::LayoutDirection arg1 = static_cast<Qt::LayoutDirection>(lqtL_toenum(L, 2, "Qt::LayoutDirection"));
  __lua__obj->QWidget::setLayoutDirection(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setContentsMargins (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int arg2 = lua_tointeger(L, 3);
  int arg3 = lua_tointeger(L, 4);
  int arg4 = lua_tointeger(L, 5);
  __lua__obj->QWidget::setContentsMargins(arg1, arg2, arg3, arg4);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__new (lua_State *L) {
  QWidget * arg1 = lqtL_testudata(L, 1, "QWidget*")?*static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*")):static_cast< QWidget * >(0);
  QFlags<Qt::WindowType> arg2 = lqtL_testudata(L, 2, "QFlags<Qt::WindowType>*")?**static_cast<QFlags<Qt::WindowType>**>(lqtL_checkudata(L, 2, "QFlags<Qt::WindowType>*")):static_cast< QFlags<Qt::WindowType> >(0);
  QWidget * ret = new LuaBinder< QWidget >(L, arg1, arg2);
  lqtL_passudata(L, ret, "QWidget*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__mapToParent (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPoint& arg1 = **static_cast<QPoint**>(lqtL_checkudata(L, 2, "QPoint*"));
  QPoint ret = __lua__obj->QWidget::mapToParent(arg1);
  lqtL_passudata(L, new QPoint(ret), "QPoint*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__render (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QPaintDevice * arg1 = *static_cast<QPaintDevice**>(lqtL_checkudata(L, 2, "QPaintDevice*"));
  const QPoint& arg2 = lqtL_testudata(L, 3, "QPoint*")?**static_cast<QPoint**>(lqtL_checkudata(L, 3, "QPoint*")):QPoint();
  const QRegion& arg3 = lqtL_testudata(L, 4, "QRegion*")?**static_cast<QRegion**>(lqtL_checkudata(L, 4, "QRegion*")):QRegion();
  QFlags<QWidget::RenderFlag> arg4 = lqtL_testudata(L, 5, "QFlags<QWidget::RenderFlag>*")?**static_cast<QFlags<QWidget::RenderFlag>**>(lqtL_checkudata(L, 5, "QFlags<QWidget::RenderFlag>*")):QFlags<QWidget::RenderFlag>(QFlag(3));
  __lua__obj->QWidget::render(arg1, arg2, arg3, arg4);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__baseSize (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QSize ret = __lua__obj->QWidget::baseSize();
  lqtL_passudata(L, new QSize(ret), "QSize*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__windowType (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::WindowType ret = __lua__obj->QWidget::windowType();
  lqtL_pushenum(L, ret, "Qt::WindowType");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__setBaseSize__OverloadedVersion__1 (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QSize& arg1 = **static_cast<QSize**>(lqtL_checkudata(L, 2, "QSize*"));
  __lua__obj->QWidget::setBaseSize(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setBaseSize__OverloadedVersion__2 (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int arg2 = lua_tointeger(L, 3);
  __lua__obj->QWidget::setBaseSize(arg1, arg2);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setBaseSize (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QWidget*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QSize*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1fb38e0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QWidget*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1fb4300;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1fb3df0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__setBaseSize__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__setBaseSize__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__setBaseSize matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__find (lua_State *L) {
  long unsigned int arg1 = lua_tointeger(L, 1);
  QWidget * ret = QWidget::find(arg1);
  lqtL_pushudata(L, ret, "QWidget*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__mask (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QRegion ret = __lua__obj->QWidget::mask();
  lqtL_passudata(L, new QRegion(ret), "QRegion*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__isModal (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QWidget::isModal();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__maximumWidth (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QWidget::maximumWidth();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__windowModality (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::WindowModality ret = __lua__obj->QWidget::windowModality();
  lqtL_pushenum(L, ret, "Qt::WindowModality");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__setVisible (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QWidget::setVisible(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setEnabled (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QWidget::setEnabled(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__topLevelWidget (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * ret = __lua__obj->QWidget::topLevelWidget();
  lqtL_pushudata(L, ret, "QWidget*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__resize__OverloadedVersion__1 (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int arg2 = lua_tointeger(L, 3);
  __lua__obj->QWidget::resize(arg1, arg2);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__resize__OverloadedVersion__2 (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QSize& arg1 = **static_cast<QSize**>(lqtL_checkudata(L, 2, "QSize*"));
  __lua__obj->QWidget::resize(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__resize (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QWidget*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1ff41e0;
  } else {
    score[1] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1ff46f0;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QWidget*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QSize*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1ff4fd0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__resize__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__resize__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__resize matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__hasMouseTracking (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QWidget::hasMouseTracking();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__actions (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QList<QAction*> ret = __lua__obj->QWidget::actions();
  lqtL_passudata(L, new QList<QAction*>(ret), "QList<QAction*>*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__isEnabledToTLW (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QWidget::isEnabledToTLW();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__setFixedSize__OverloadedVersion__1 (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QSize& arg1 = **static_cast<QSize**>(lqtL_checkudata(L, 2, "QSize*"));
  __lua__obj->QWidget::setFixedSize(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setFixedSize__OverloadedVersion__2 (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int arg2 = lua_tointeger(L, 3);
  __lua__obj->QWidget::setFixedSize(arg1, arg2);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setFixedSize (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QWidget*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QSize*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1fb5280;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QWidget*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1fb5c00;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1fb6110;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__setFixedSize__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__setFixedSize__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__setFixedSize matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__backgroundRole (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QPalette::ColorRole ret = __lua__obj->QWidget::backgroundRole();
  lqtL_pushenum(L, ret, "QPalette::ColorRole");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__setContextMenuPolicy (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::ContextMenuPolicy arg1 = static_cast<Qt::ContextMenuPolicy>(lqtL_toenum(L, 2, "Qt::ContextMenuPolicy"));
  __lua__obj->QWidget::setContextMenuPolicy(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__createWinId (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QWidget::createWinId();
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__windowSurface (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWindowSurface * ret = __lua__obj->QWidget::windowSurface();
  lqtL_pushudata(L, ret, "QWindowSurface*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__maximumSize (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QSize ret = __lua__obj->QWidget::maximumSize();
  lqtL_passudata(L, new QSize(ret), "QSize*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__isMaximized (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QWidget::isMaximized();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__releaseKeyboard (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QWidget::releaseKeyboard();
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__windowOpacity (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double ret = __lua__obj->QWidget::windowOpacity();
  lua_pushnumber(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__isVisibleTo (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 2, "QWidget*"));
  bool ret = __lua__obj->QWidget::isVisibleTo(arg1);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__style (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QStyle * ret = __lua__obj->QWidget::style();
  lqtL_pushudata(L, ret, "QStyle*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__contentsRect (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QRect ret = __lua__obj->QWidget::contentsRect();
  lqtL_passudata(L, new QRect(ret), "QRect*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__clearFocus (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QWidget::clearFocus();
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__insertActions (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QAction * arg1 = *static_cast<QAction**>(lqtL_checkudata(L, 2, "QAction*"));
  QList<QAction*> arg2 = **static_cast<QList<QAction*>**>(lqtL_checkudata(L, 3, "QList<QAction*>*"));
  __lua__obj->QWidget::insertActions(arg1, arg2);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setShortcutEnabled (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  bool arg2 = lua_isboolean(L, 3)?(bool)lua_toboolean(L, 3):true;
  __lua__obj->QWidget::setShortcutEnabled(arg1, arg2);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__childrenRect (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QRect ret = __lua__obj->QWidget::childrenRect();
  lqtL_passudata(L, new QRect(ret), "QRect*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__hide (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QWidget::hide();
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__grabKeyboard (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QWidget::grabKeyboard();
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setWindowState (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<Qt::WindowState> arg1 = **static_cast<QFlags<Qt::WindowState>**>(lqtL_checkudata(L, 2, "QFlags<Qt::WindowState>*"));
  __lua__obj->QWidget::setWindowState(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setMaximumHeight (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QWidget::setMaximumHeight(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__clearMask (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QWidget::clearMask();
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setFocusPolicy (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::FocusPolicy arg1 = static_cast<Qt::FocusPolicy>(lqtL_toenum(L, 2, "Qt::FocusPolicy"));
  __lua__obj->QWidget::setFocusPolicy(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__hasFocus (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QWidget::hasFocus();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__removeAction (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QAction * arg1 = *static_cast<QAction**>(lqtL_checkudata(L, 2, "QAction*"));
  __lua__obj->QWidget::removeAction(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setStatusTip (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QWidget::setStatusTip(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setFocus__OverloadedVersion__1 (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QWidget::setFocus();
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setFocus__OverloadedVersion__2 (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::FocusReason arg1 = static_cast<Qt::FocusReason>(lqtL_toenum(L, 2, "Qt::FocusReason"));
  __lua__obj->QWidget::setFocus(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setFocus (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QWidget*")?premium:-premium*premium;
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QWidget*")?premium:-premium*premium;
  if (lqtL_isenum(L, 2, "Qt::FocusReason")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1fd8cf0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__setFocus__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__setFocus__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__setFocus matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__fontMetrics (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFontMetrics ret = __lua__obj->QWidget::fontMetrics();
  lqtL_passudata(L, new QFontMetrics(ret), "QFontMetrics*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__setTabOrder (lua_State *L) {
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
  QWidget * arg2 = *static_cast<QWidget**>(lqtL_checkudata(L, 2, "QWidget*"));
  QWidget::setTabOrder(arg1, arg2);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__windowRole (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QWidget::windowRole();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__underMouse (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QWidget::underMouse();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__metaObject (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QMetaObject * ret = __lua__obj->QWidget::metaObject();
  lqtL_pushudata(L, ret, "QMetaObject*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__sizeHint (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QSize ret = __lua__obj->QWidget::sizeHint();
  lqtL_passudata(L, new QSize(ret), "QSize*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__inputMethodQuery (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::InputMethodQuery arg1 = static_cast<Qt::InputMethodQuery>(lqtL_toenum(L, 2, "Qt::InputMethodQuery"));
  QVariant ret = __lua__obj->QWidget::inputMethodQuery(arg1);
  lqtL_passudata(L, new QVariant(ret), "QVariant*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__grabMouse__OverloadedVersion__1 (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QWidget::grabMouse();
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__grabMouse__OverloadedVersion__2 (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QCursor& arg1 = **static_cast<QCursor**>(lqtL_checkudata(L, 2, "QCursor*"));
  __lua__obj->QWidget::grabMouse(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__grabMouse (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QWidget*")?premium:-premium*premium;
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QWidget*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QCursor*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1fde970;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__grabMouse__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__grabMouse__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__grabMouse matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setMaximumWidth (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QWidget::setMaximumWidth(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__updateGeometry (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QWidget::updateGeometry();
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setMinimumWidth (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QWidget::setMinimumWidth(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setToolTip (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QString& arg1 = QString::fromAscii(lua_tostring(L, 2), lua_objlen(L, 2));
  __lua__obj->QWidget::setToolTip(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setSizeIncrement__OverloadedVersion__1 (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QSize& arg1 = **static_cast<QSize**>(lqtL_checkudata(L, 2, "QSize*"));
  __lua__obj->QWidget::setSizeIncrement(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setSizeIncrement__OverloadedVersion__2 (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int arg2 = lua_tointeger(L, 3);
  __lua__obj->QWidget::setSizeIncrement(arg1, arg2);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setSizeIncrement (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QWidget*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QSize*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1fb1860;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QWidget*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1fb22c0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1fb27f0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__setSizeIncrement__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__setSizeIncrement__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__setSizeIncrement matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setFocusProxy (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * arg1 = *static_cast<QWidget**>(lqtL_checkudata(L, 2, "QWidget*"));
  __lua__obj->QWidget::setFocusProxy(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__show (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QWidget::show();
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__y (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QWidget::y();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__focusWidget (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * ret = __lua__obj->QWidget::focusWidget();
  lqtL_pushudata(L, ret, "QWidget*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__focusPolicy (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::FocusPolicy ret = __lua__obj->QWidget::focusPolicy();
  lqtL_pushenum(L, ret, "Qt::FocusPolicy");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__focusProxy (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * ret = __lua__obj->QWidget::focusProxy();
  lqtL_pushudata(L, ret, "QWidget*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__childAt__OverloadedVersion__1 (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int arg2 = lua_tointeger(L, 3);
  QWidget * ret = __lua__obj->QWidget::childAt(arg1, arg2);
  lqtL_pushudata(L, ret, "QWidget*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__childAt__OverloadedVersion__2 (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPoint& arg1 = **static_cast<QPoint**>(lqtL_checkudata(L, 2, "QPoint*"));
  QWidget * ret = __lua__obj->QWidget::childAt(arg1);
  lqtL_pushudata(L, ret, "QWidget*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__childAt (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QWidget*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x2013b60;
  } else {
    score[1] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x2014090;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QWidget*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPoint*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x2014980;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__childAt__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__childAt__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__childAt matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__geometry (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QRect& ret = __lua__obj->QWidget::geometry();
  lqtL_pushudata(L, &(ret), "QRect*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__setLayout (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QLayout * arg1 = *static_cast<QLayout**>(lqtL_checkudata(L, 2, "QLayout*"));
  __lua__obj->QWidget::setLayout(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setAutoFillBackground (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QWidget::setAutoFillBackground(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setFixedWidth (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QWidget::setFixedWidth(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__close (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QWidget::close();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__parentWidget (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * ret = __lua__obj->QWidget::parentWidget();
  lqtL_pushudata(L, ret, "QWidget*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__isTopLevel (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QWidget::isTopLevel();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__setWindowSurface (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWindowSurface * arg1 = *static_cast<QWindowSurface**>(lqtL_checkudata(L, 2, "QWindowSurface*"));
  __lua__obj->QWidget::setWindowSurface(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__update__OverloadedVersion__1 (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QWidget::update();
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__update__OverloadedVersion__2 (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int arg2 = lua_tointeger(L, 3);
  int arg3 = lua_tointeger(L, 4);
  int arg4 = lua_tointeger(L, 5);
  __lua__obj->QWidget::update(arg1, arg2, arg3, arg4);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__update__OverloadedVersion__3 (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QRect& arg1 = **static_cast<QRect**>(lqtL_checkudata(L, 2, "QRect*"));
  __lua__obj->QWidget::update(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__update__OverloadedVersion__4 (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QRegion& arg1 = **static_cast<QRegion**>(lqtL_checkudata(L, 2, "QRegion*"));
  __lua__obj->QWidget::update(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__update (lua_State *L) {
  int score[5];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QWidget*")?premium:-premium*premium;
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QWidget*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1fe6fa0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1fe74b0;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 4)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1fe7860;
  } else {
    score[2] -= premium*premium;
  }
  if (lua_isnumber(L, 5)) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1fe7800;
  } else {
    score[2] -= premium*premium;
  }
  score[3] = 0;
  score[3] += lqtL_testudata(L, 1, "QWidget*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QRect*")) {
    score[3] += premium;
  } else if (false) {
    score[3] += premium-1; // table: 0x1fe8500;
  } else {
    score[3] -= premium*premium;
  }
  score[4] = 0;
  score[4] += lqtL_testudata(L, 1, "QWidget*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QRegion*")) {
    score[4] += premium;
  } else if (false) {
    score[4] += premium-1; // table: 0x1fe8e70;
  } else {
    score[4] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=4;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__update__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__update__OverloadedVersion__2(L); break;
    case 3: return __LuaWrapCall__update__OverloadedVersion__3(L); break;
    case 4: return __LuaWrapCall__update__OverloadedVersion__4(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__update matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__isWindow (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QWidget::isWindow();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__internalWinId (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  long unsigned int ret = __lua__obj->QWidget::internalWinId();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__setCursor (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QCursor& arg1 = **static_cast<QCursor**>(lqtL_checkudata(L, 2, "QCursor*"));
  __lua__obj->QWidget::setCursor(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__keyboardGrabber (lua_State *L) {
  QWidget * ret = QWidget::keyboardGrabber();
  lqtL_pushudata(L, ret, "QWidget*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__x (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QWidget::x();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__ensurePolished (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QWidget::ensurePolished();
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setMouseTracking (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QWidget::setMouseTracking(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setAttribute (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::WidgetAttribute arg1 = static_cast<Qt::WidgetAttribute>(lqtL_toenum(L, 2, "Qt::WidgetAttribute"));
  bool arg2 = lua_isboolean(L, 3)?(bool)lua_toboolean(L, 3):true;
  __lua__obj->QWidget::setAttribute(arg1, arg2);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__window (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QWidget * ret = __lua__obj->QWidget::window();
  lqtL_pushudata(L, ret, "QWidget*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__setForegroundRole (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QPalette::ColorRole arg1 = static_cast<QPalette::ColorRole>(lqtL_toenum(L, 2, "QPalette::ColorRole"));
  __lua__obj->QWidget::setForegroundRole(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setDisabled (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QWidget::setDisabled(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__mapFromParent (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPoint& arg1 = **static_cast<QPoint**>(lqtL_checkudata(L, 2, "QPoint*"));
  QPoint ret = __lua__obj->QWidget::mapFromParent(arg1);
  lqtL_passudata(L, new QPoint(ret), "QPoint*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__setHidden (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QWidget::setHidden(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setPalette (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPalette& arg1 = **static_cast<QPalette**>(lqtL_checkudata(L, 2, "QPalette*"));
  __lua__obj->QWidget::setPalette(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setUpdatesEnabled (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool arg1 = (bool)lua_toboolean(L, 2);
  __lua__obj->QWidget::setUpdatesEnabled(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__mapFromGlobal (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPoint& arg1 = **static_cast<QPoint**>(lqtL_checkudata(L, 2, "QPoint*"));
  QPoint ret = __lua__obj->QWidget::mapFromGlobal(arg1);
  lqtL_passudata(L, new QPoint(ret), "QPoint*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__isFullScreen (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QWidget::isFullScreen();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__layoutDirection (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::LayoutDirection ret = __lua__obj->QWidget::layoutDirection();
  lqtL_pushenum(L, ret, "Qt::LayoutDirection");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__rect (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QRect ret = __lua__obj->QWidget::rect();
  lqtL_passudata(L, new QRect(ret), "QRect*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__activateWindow (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QWidget::activateWindow();
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__showMaximized (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QWidget::showMaximized();
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__height (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QWidget::height();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__showFullScreen (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QWidget::showFullScreen();
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__minimumHeight (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QWidget::minimumHeight();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__insertAction (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QAction * arg1 = *static_cast<QAction**>(lqtL_checkudata(L, 2, "QAction*"));
  QAction * arg2 = *static_cast<QAction**>(lqtL_checkudata(L, 3, "QAction*"));
  __lua__obj->QWidget::insertAction(arg1, arg2);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__devType (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QWidget::devType();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__setMask__OverloadedVersion__1 (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QBitmap& arg1 = **static_cast<QBitmap**>(lqtL_checkudata(L, 2, "QBitmap*"));
  __lua__obj->QWidget::setMask(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setMask__OverloadedVersion__2 (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QRegion& arg1 = **static_cast<QRegion**>(lqtL_checkudata(L, 2, "QRegion*"));
  __lua__obj->QWidget::setMask(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setMask (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QWidget*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QBitmap*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1fc5530;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QWidget*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QRegion*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1fc5ea0;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__setMask__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__setMask__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__setMask matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setWindowOpacity (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  double arg1 = lua_tonumber(L, 2);
  __lua__obj->QWidget::setWindowOpacity(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__move__OverloadedVersion__1 (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int arg2 = lua_tointeger(L, 3);
  __lua__obj->QWidget::move(arg1, arg2);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__move__OverloadedVersion__2 (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QPoint& arg1 = **static_cast<QPoint**>(lqtL_checkudata(L, 2, "QPoint*"));
  __lua__obj->QWidget::move(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__move (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QWidget*")?premium:-premium*premium;
  if (lua_isnumber(L, 2)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1ff2a20;
  } else {
    score[1] -= premium*premium;
  }
  if (lua_isnumber(L, 3)) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1ff2f40;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QWidget*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QPoint*")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1ff3820;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__move__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__move__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__move matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__restoreGeometry (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QByteArray& arg1 = QByteArray(lua_tostring(L, 2), lua_objlen(L, 2));
  bool ret = __lua__obj->QWidget::restoreGeometry(arg1);
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__maximumHeight (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QWidget::maximumHeight();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__setWindowFlags (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<Qt::WindowType> arg1 = **static_cast<QFlags<Qt::WindowType>**>(lqtL_checkudata(L, 2, "QFlags<Qt::WindowType>*"));
  __lua__obj->QWidget::setWindowFlags(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__paintEngine (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QPaintEngine * ret = __lua__obj->QWidget::paintEngine();
  lqtL_pushudata(L, ret, "QPaintEngine*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__isMinimized (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QWidget::isMinimized();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__setMinimumHeight (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  __lua__obj->QWidget::setMinimumHeight(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setSizePolicy__OverloadedVersion__1 (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QSizePolicy arg1 = **static_cast<QSizePolicy**>(lqtL_checkudata(L, 2, "QSizePolicy*"));
  __lua__obj->QWidget::setSizePolicy(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setSizePolicy__OverloadedVersion__2 (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QSizePolicy::Policy arg1 = static_cast<QSizePolicy::Policy>(lqtL_toenum(L, 2, "QSizePolicy::Policy"));
  QSizePolicy::Policy arg2 = static_cast<QSizePolicy::Policy>(lqtL_toenum(L, 3, "QSizePolicy::Policy"));
  __lua__obj->QWidget::setSizePolicy(arg1, arg2);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setSizePolicy (lua_State *L) {
  int score[3];
  const int premium = 11+lua_gettop(L);
  score[1] = 0;
  score[1] += lqtL_testudata(L, 1, "QWidget*")?premium:-premium*premium;
  if (lqtL_testudata(L, 2, "QSizePolicy*")) {
    score[1] += premium;
  } else if (false) {
    score[1] += premium-1; // table: 0x1fff390;
  } else {
    score[1] -= premium*premium;
  }
  score[2] = 0;
  score[2] += lqtL_testudata(L, 1, "QWidget*")?premium:-premium*premium;
  if (lqtL_isenum(L, 2, "QSizePolicy::Policy")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1fffe00;
  } else {
    score[2] -= premium*premium;
  }
  if (lqtL_isenum(L, 3, "QSizePolicy::Policy")) {
    score[2] += premium;
  } else if (false) {
    score[2] += premium-1; // table: 0x1fffa70;
  } else {
    score[2] -= premium*premium;
  }
  int best = 1;
  for (int i=1;i<=2;i++) {
    if (score[best] < score[i]) { best = i; }
  }
  switch (best) {
    case 1: return __LuaWrapCall__setSizePolicy__OverloadedVersion__1(L); break;
    case 2: return __LuaWrapCall__setSizePolicy__OverloadedVersion__2(L); break;
	}
	lua_pushstring(L, "no overload of function __LuaWrapCall__setSizePolicy matching arguments");
	lua_error(L);
	return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__heightForWidth (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int arg1 = lua_tointeger(L, 2);
  int ret = __lua__obj->QWidget::heightForWidth(arg1);
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__visibleRegion (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QRegion ret = __lua__obj->QWidget::visibleRegion();
  lqtL_passudata(L, new QRegion(ret), "QRegion*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__getContentsMargins (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int * arg1 = static_cast<int *>(lua_touserdata(L, 2));
  int * arg2 = static_cast<int *>(lua_touserdata(L, 3));
  int * arg3 = static_cast<int *>(lua_touserdata(L, 4));
  int * arg4 = static_cast<int *>(lua_touserdata(L, 5));
  __lua__obj->QWidget::getContentsMargins(arg1, arg2, arg3, arg4);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__setWindowModality (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  Qt::WindowModality arg1 = static_cast<Qt::WindowModality>(lqtL_toenum(L, 2, "Qt::WindowModality"));
  __lua__obj->QWidget::setWindowModality(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__lower (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QWidget::lower();
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__windowTitle (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QWidget::windowTitle();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__sizeIncrement (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QSize ret = __lua__obj->QWidget::sizeIncrement();
  lqtL_passudata(L, new QSize(ret), "QSize*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__showNormal (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QWidget::showNormal();
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__unsetLayoutDirection (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QWidget::unsetLayoutDirection();
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__autoFillBackground (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QWidget::autoFillBackground();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__isActiveWindow (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  bool ret = __lua__obj->QWidget::isActiveWindow();
  lua_pushboolean(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__minimumSizeHint (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QSize ret = __lua__obj->QWidget::minimumSizeHint();
  lqtL_passudata(L, new QSize(ret), "QSize*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__width (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  int ret = __lua__obj->QWidget::width();
  lua_pushinteger(L, ret);
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__setLocale (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  const QLocale& arg1 = **static_cast<QLocale**>(lqtL_checkudata(L, 2, "QLocale*"));
  __lua__obj->QWidget::setLocale(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__saveGeometry (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QByteArray ret = __lua__obj->QWidget::saveGeometry();
  lua_pushlstring(L, ret.data(), ret.size());
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__overrideWindowFlags (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFlags<Qt::WindowType> arg1 = **static_cast<QFlags<Qt::WindowType>**>(lqtL_checkudata(L, 2, "QFlags<Qt::WindowType>*"));
  __lua__obj->QWidget::overrideWindowFlags(arg1);
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__toolTip (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QString ret = __lua__obj->QWidget::toolTip();
  lua_pushlstring(L, ret.toAscii().data(), ret.toAscii().size());
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__raise (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  __lua__obj->QWidget::raise();
  return 0;
}
int LuaBinder< QWidget >::__LuaWrapCall__windowIcon (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QIcon ret = __lua__obj->QWidget::windowIcon();
  lqtL_passudata(L, new QIcon(ret), "QIcon*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__fontInfo (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  QFontInfo ret = __lua__obj->QWidget::fontInfo();
  lqtL_passudata(L, new QFontInfo(ret), "QFontInfo*");
  return 1;
}
int LuaBinder< QWidget >::__LuaWrapCall__handle (lua_State *L) {
  QWidget *& __lua__obj = *static_cast<QWidget**>(lqtL_checkudata(L, 1, "QWidget*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  long unsigned int ret = __lua__obj->QWidget::handle();
  lua_pushinteger(L, ret);
  return 1;
}
void LuaBinder< QWidget >::styleChange (QStyle& arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "styleChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QStyle*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::styleChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QWidget >::focusInEvent (QFocusEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "focusInEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QFocusEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::focusInEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QWidget >::keyPressEvent (QKeyEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "keyPressEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QKeyEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::keyPressEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QWidget >::contextMenuEvent (QContextMenuEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "contextMenuEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QContextMenuEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::contextMenuEvent(arg1);
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QWidget >::devType () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "devType");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::devType();
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QWidget >::setVisible (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "setVisible");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::setVisible(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QWidget >::keyReleaseEvent (QKeyEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "keyReleaseEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QKeyEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::keyReleaseEvent(arg1);
  }
  lua_settop(L, oldtop);
}
QPaintEngine * LuaBinder< QWidget >::paintEngine () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "paintEngine");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::paintEngine();
  }
  QPaintEngine * ret = *static_cast<QPaintEngine**>(lqtL_checkudata(L, -1, "QPaintEngine*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QWidget >::dragLeaveEvent (QDragLeaveEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dragLeaveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDragLeaveEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::dragLeaveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QWidget >::mousePressEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mousePressEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::mousePressEvent(arg1);
  }
  lua_settop(L, oldtop);
}
QSize LuaBinder< QWidget >::sizeHint () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "sizeHint");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::sizeHint();
  }
  QSize ret = **static_cast<QSize**>(lqtL_checkudata(L, -1, "QSize*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QWidget >::moveEvent (QMoveEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "moveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMoveEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::moveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QWidget >::tabletEvent (QTabletEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "tabletEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QTabletEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::tabletEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QWidget >::connectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "connectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::connectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
QVariant LuaBinder< QWidget >::inputMethodQuery (Qt::InputMethodQuery arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "inputMethodQuery");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushenum(L, arg1, "Qt::InputMethodQuery");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::inputMethodQuery(arg1);
  }
  QVariant ret = **static_cast<QVariant**>(lqtL_checkudata(L, -1, "QVariant*"));
  lua_settop(L, oldtop);
  return ret;
}
QSize LuaBinder< QWidget >::minimumSizeHint () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "minimumSizeHint");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::minimumSizeHint();
  }
  QSize ret = **static_cast<QSize**>(lqtL_checkudata(L, -1, "QSize*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QWidget >::timerEvent (QTimerEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "timerEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QTimerEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::timerEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QWidget >::dropEvent (QDropEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dropEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDropEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::dropEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QWidget >::showEvent (QShowEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "showEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QShowEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::showEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QWidget >::changeEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "changeEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::changeEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QWidget >::paintEvent (QPaintEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "paintEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QPaintEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::paintEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QWidget >::customEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "customEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::customEvent(arg1);
  }
  lua_settop(L, oldtop);
}
const QMetaObject * LuaBinder< QWidget >::metaObject () const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "metaObject");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::metaObject();
  }
  const QMetaObject * ret = *static_cast<QMetaObject**>(lqtL_checkudata(L, -1, "QMetaObject*"));
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QWidget >::enabledChange (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "enabledChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::enabledChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QWidget >::windowActivationChange (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "windowActivationChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::windowActivationChange(arg1);
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QWidget >::heightForWidth (int arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "heightForWidth");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushinteger(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::heightForWidth(arg1);
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QWidget >::mouseDoubleClickEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mouseDoubleClickEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::mouseDoubleClickEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QWidget >::fontChange (const QFont& arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "fontChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QFont*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::fontChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QWidget >::disconnectNotify (const char * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "disconnectNotify");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushstring(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::disconnectNotify(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QWidget >::mouseMoveEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mouseMoveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::mouseMoveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QWidget >::focusNextPrevChild (bool arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "focusNextPrevChild");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lua_pushboolean(L, arg1);
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::focusNextPrevChild(arg1);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QWidget >::childEvent (QChildEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "childEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QChildEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QObject::childEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QWidget >::focusOutEvent (QFocusEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "focusOutEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QFocusEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::focusOutEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QWidget >::mouseReleaseEvent (QMouseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "mouseReleaseEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QMouseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::mouseReleaseEvent(arg1);
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QWidget >::metric (QPaintDevice::PaintDeviceMetric arg1) const {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "metric");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushenum(L, arg1, "QPaintDevice::PaintDeviceMetric");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::metric(arg1);
  }
  int ret = lua_tointeger(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
bool LuaBinder< QWidget >::eventFilter (QObject * arg1, QEvent * arg2) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "eventFilter");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QObject*");
  lqtL_pushudata(L, arg2, "QEvent*");
  if (lua_isfunction(L, -2-2)) {
    lua_pcall(L, 2+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QObject::eventFilter(arg1, arg2);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QWidget >::inputMethodEvent (QInputMethodEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "inputMethodEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QInputMethodEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::inputMethodEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QWidget >::wheelEvent (QWheelEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "wheelEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QWheelEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::wheelEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QWidget >::resizeEvent (QResizeEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "resizeEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QResizeEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::resizeEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QWidget >::paletteChange (const QPalette& arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "paletteChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, &(arg1), "QPalette*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::paletteChange(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QWidget >::languageChange () {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "languageChange");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  if (lua_isfunction(L, -0-2)) {
    lua_pcall(L, 0+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::languageChange();
  }
  lua_settop(L, oldtop);
}
bool LuaBinder< QWidget >::event (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "event");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    return this->QWidget::event(arg1);
  }
  bool ret = (bool)lua_toboolean(L, -1);
  lua_settop(L, oldtop);
  return ret;
}
void LuaBinder< QWidget >::hideEvent (QHideEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "hideEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QHideEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::hideEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QWidget >::dragMoveEvent (QDragMoveEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dragMoveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDragMoveEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::dragMoveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QWidget >::dragEnterEvent (QDragEnterEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "dragEnterEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QDragEnterEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::dragEnterEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QWidget >::actionEvent (QActionEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "actionEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QActionEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::actionEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QWidget >::leaveEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "leaveEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::leaveEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QWidget >::closeEvent (QCloseEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "closeEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QCloseEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::closeEvent(arg1);
  }
  lua_settop(L, oldtop);
}
void LuaBinder< QWidget >::enterEvent (QEvent * arg1) {
  bool absorbed = false;
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
	if (lua_getmetatable(L, -1)) {
		lua_getfield(L, -1, "enterEvent");
		lua_remove(L, -2);
	} else {
		lua_pushnil(L);
	}
	lua_insert(L, -2);
  lqtL_pushudata(L, arg1, "QEvent*");
  if (lua_isfunction(L, -1-2)) {
    lua_pcall(L, 1+1, 2, 0);
		absorbed = (bool)lua_toboolean(L, -1) || (bool)lua_toboolean(L, -2);
		lua_pop(L, 1);
  }
  if (!absorbed) {
    lua_settop(L, oldtop);
    this->QWidget::enterEvent(arg1);
  }
  lua_settop(L, oldtop);
}
LuaBinder< QWidget >::  ~LuaBinder< QWidget > () {
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QWidget*");
  lua_getfield(L, -1, "~QWidget");

  if (lua_isfunction(L, -1)) {
    lua_insert(L, -2);
    lua_pcall(L, 1, 1, 0);
  } else {
  }
  lua_settop(L, oldtop);
}
int LuaBinder< QWidget >::lqt_pushenum_RenderFlag (lua_State *L) {
  int enum_table = 0;
  lua_getfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  if (!lua_istable(L, -1)) {
    lua_pop(L, 1);
    lua_newtable(L);
    lua_pushvalue(L, -1);
    lua_setfield(L, LUA_REGISTRYINDEX, LQT_ENUMS);
  }
  lua_newtable(L);
  enum_table = lua_gettop(L);
  lua_pushstring(L, "DrawWindowBackground");
  lua_rawseti(L, enum_table, 1);
  lua_pushinteger(L, 1);
  lua_setfield(L, enum_table, "DrawWindowBackground");
  lua_pushstring(L, "DrawChildren");
  lua_rawseti(L, enum_table, 2);
  lua_pushinteger(L, 2);
  lua_setfield(L, enum_table, "DrawChildren");
  lua_pushstring(L, "IgnoreMask");
  lua_rawseti(L, enum_table, 4);
  lua_pushinteger(L, 4);
  lua_setfield(L, enum_table, "IgnoreMask");
  lua_pushcfunction(L, LuaBinder< QWidget >::lqt_pushenum_RenderFlag_QFLAGS_CREATOR);
  lua_setfield(L, enum_table, "QFlags");
  lua_pushvalue(L, -1);
  lua_setfield(L, -3, "QWidget::RenderFlag");
  lua_remove(L, -2);
  return 1;
}
int LuaBinder< QWidget >::lqt_pushenum_RenderFlag_QFLAGS_CREATOR (lua_State *L) {
	int argn = lua_gettop(L);
	int i = 0;
  void *p  = lua_newuserdata(L, sizeof(QFlags<QWidget::RenderFlag>*) + sizeof(QFlags<QWidget::RenderFlag>));
  QFlags<QWidget::RenderFlag> *fl = static_cast<QFlags<QWidget::RenderFlag>*>( static_cast<void*>(&static_cast<QFlags<QWidget::RenderFlag>**>(p)[1]) );
	*(void**)p = fl;
	for (i=1;i<=argn;i++) {
    *fl |= static_cast<QWidget::RenderFlag>(lqtL_toenum(L, i, "QWidget::RenderFlag"));
	}
	if (luaL_newmetatable(L, "QFlags<QWidget::RenderFlag>*")) {
		lua_pushstring(L, "QFlags<QWidget::RenderFlag>*");
		lua_setfield(L, -2, "__qtype");
	}
	lua_setmetatable(L, -2);
	return 1;
}
int luaopen_QWidget (lua_State *L) {
  if (luaL_newmetatable(L, "QWidget*")) {
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__windowFlags);
    lua_setfield(L, -2, "windowFlags");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setShown);
    lua_setfield(L, -2, "setShown");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__minimumSize);
    lua_setfield(L, -2, "minimumSize");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__showMinimized);
    lua_setfield(L, -2, "showMinimized");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__statusTip);
    lua_setfield(L, -2, "statusTip");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__styleSheet);
    lua_setfield(L, -2, "styleSheet");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__childrenRegion);
    lua_setfield(L, -2, "childrenRegion");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__mapTo);
    lua_setfield(L, -2, "mapTo");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setStyle);
    lua_setfield(L, -2, "setStyle");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__size);
    lua_setfield(L, -2, "size");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__tr);
    lua_setfield(L, -2, "tr");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__isVisible);
    lua_setfield(L, -2, "isVisible");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__whatsThis);
    lua_setfield(L, -2, "whatsThis");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__layout);
    lua_setfield(L, -2, "layout");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__palette);
    lua_setfield(L, -2, "palette");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__font);
    lua_setfield(L, -2, "font");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__adjustSize);
    lua_setfield(L, -2, "adjustSize");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setAcceptDrops);
    lua_setfield(L, -2, "setAcceptDrops");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__unsetLocale);
    lua_setfield(L, -2, "unsetLocale");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__pos);
    lua_setfield(L, -2, "pos");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setGeometry);
    lua_setfield(L, -2, "setGeometry");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__windowIconText);
    lua_setfield(L, -2, "windowIconText");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__isRightToLeft);
    lua_setfield(L, -2, "isRightToLeft");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__mapFrom);
    lua_setfield(L, -2, "mapFrom");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__inputContext);
    lua_setfield(L, -2, "inputContext");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__windowState);
    lua_setfield(L, -2, "windowState");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__minimumWidth);
    lua_setfield(L, -2, "minimumWidth");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__addAction);
    lua_setfield(L, -2, "addAction");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__isHidden);
    lua_setfield(L, -2, "isHidden");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__delete);
    lua_setfield(L, -2, "delete");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setWindowTitle);
    lua_setfield(L, -2, "setWindowTitle");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setStyleSheet);
    lua_setfield(L, -2, "setStyleSheet");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__releaseShortcut);
    lua_setfield(L, -2, "releaseShortcut");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setWhatsThis);
    lua_setfield(L, -2, "setWhatsThis");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__isEnabled);
    lua_setfield(L, -2, "isEnabled");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__addActions);
    lua_setfield(L, -2, "addActions");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setWindowModified);
    lua_setfield(L, -2, "setWindowModified");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setWindowIconText);
    lua_setfield(L, -2, "setWindowIconText");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setWindowRole);
    lua_setfield(L, -2, "setWindowRole");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__scroll);
    lua_setfield(L, -2, "scroll");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__cursor);
    lua_setfield(L, -2, "cursor");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setFixedHeight);
    lua_setfield(L, -2, "setFixedHeight");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setShortcutAutoRepeat);
    lua_setfield(L, -2, "setShortcutAutoRepeat");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__trUtf8);
    lua_setfield(L, -2, "trUtf8");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__foregroundRole);
    lua_setfield(L, -2, "foregroundRole");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__mapToGlobal);
    lua_setfield(L, -2, "mapToGlobal");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__releaseMouse);
    lua_setfield(L, -2, "releaseMouse");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__isEnabledTo);
    lua_setfield(L, -2, "isEnabledTo");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__isLeftToRight);
    lua_setfield(L, -2, "isLeftToRight");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__contextMenuPolicy);
    lua_setfield(L, -2, "contextMenuPolicy");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__updatesEnabled);
    lua_setfield(L, -2, "updatesEnabled");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__isAncestorOf);
    lua_setfield(L, -2, "isAncestorOf");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__normalGeometry);
    lua_setfield(L, -2, "normalGeometry");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__nextInFocusChain);
    lua_setfield(L, -2, "nextInFocusChain");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__sizePolicy);
    lua_setfield(L, -2, "sizePolicy");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__overrideWindowState);
    lua_setfield(L, -2, "overrideWindowState");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setMinimumSize);
    lua_setfield(L, -2, "setMinimumSize");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__testAttribute);
    lua_setfield(L, -2, "testAttribute");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__stackUnder);
    lua_setfield(L, -2, "stackUnder");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__grabShortcut);
    lua_setfield(L, -2, "grabShortcut");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setMaximumSize);
    lua_setfield(L, -2, "setMaximumSize");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setBackgroundRole);
    lua_setfield(L, -2, "setBackgroundRole");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__locale);
    lua_setfield(L, -2, "locale");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__mouseGrabber);
    lua_setfield(L, -2, "mouseGrabber");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__frameSize);
    lua_setfield(L, -2, "frameSize");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setInputContext);
    lua_setfield(L, -2, "setInputContext");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__repaint);
    lua_setfield(L, -2, "repaint");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__unsetCursor);
    lua_setfield(L, -2, "unsetCursor");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__winId);
    lua_setfield(L, -2, "winId");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__isWindowModified);
    lua_setfield(L, -2, "isWindowModified");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__acceptDrops);
    lua_setfield(L, -2, "acceptDrops");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setFont);
    lua_setfield(L, -2, "setFont");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setWindowIcon);
    lua_setfield(L, -2, "setWindowIcon");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setParent);
    lua_setfield(L, -2, "setParent");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__frameGeometry);
    lua_setfield(L, -2, "frameGeometry");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setLayoutDirection);
    lua_setfield(L, -2, "setLayoutDirection");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setContentsMargins);
    lua_setfield(L, -2, "setContentsMargins");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__new);
    lua_setfield(L, -2, "new");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__mapToParent);
    lua_setfield(L, -2, "mapToParent");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__render);
    lua_setfield(L, -2, "render");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__baseSize);
    lua_setfield(L, -2, "baseSize");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__windowType);
    lua_setfield(L, -2, "windowType");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setBaseSize);
    lua_setfield(L, -2, "setBaseSize");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__find);
    lua_setfield(L, -2, "find");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__mask);
    lua_setfield(L, -2, "mask");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__isModal);
    lua_setfield(L, -2, "isModal");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__maximumWidth);
    lua_setfield(L, -2, "maximumWidth");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__windowModality);
    lua_setfield(L, -2, "windowModality");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setVisible);
    lua_setfield(L, -2, "setVisible");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setEnabled);
    lua_setfield(L, -2, "setEnabled");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__topLevelWidget);
    lua_setfield(L, -2, "topLevelWidget");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__resize);
    lua_setfield(L, -2, "resize");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__hasMouseTracking);
    lua_setfield(L, -2, "hasMouseTracking");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__actions);
    lua_setfield(L, -2, "actions");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__isEnabledToTLW);
    lua_setfield(L, -2, "isEnabledToTLW");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setFixedSize);
    lua_setfield(L, -2, "setFixedSize");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__backgroundRole);
    lua_setfield(L, -2, "backgroundRole");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setContextMenuPolicy);
    lua_setfield(L, -2, "setContextMenuPolicy");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__createWinId);
    lua_setfield(L, -2, "createWinId");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__windowSurface);
    lua_setfield(L, -2, "windowSurface");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__maximumSize);
    lua_setfield(L, -2, "maximumSize");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__isMaximized);
    lua_setfield(L, -2, "isMaximized");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__releaseKeyboard);
    lua_setfield(L, -2, "releaseKeyboard");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__windowOpacity);
    lua_setfield(L, -2, "windowOpacity");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__isVisibleTo);
    lua_setfield(L, -2, "isVisibleTo");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__style);
    lua_setfield(L, -2, "style");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__contentsRect);
    lua_setfield(L, -2, "contentsRect");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__clearFocus);
    lua_setfield(L, -2, "clearFocus");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__insertActions);
    lua_setfield(L, -2, "insertActions");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setShortcutEnabled);
    lua_setfield(L, -2, "setShortcutEnabled");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__childrenRect);
    lua_setfield(L, -2, "childrenRect");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__hide);
    lua_setfield(L, -2, "hide");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__grabKeyboard);
    lua_setfield(L, -2, "grabKeyboard");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setWindowState);
    lua_setfield(L, -2, "setWindowState");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setMaximumHeight);
    lua_setfield(L, -2, "setMaximumHeight");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__clearMask);
    lua_setfield(L, -2, "clearMask");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setFocusPolicy);
    lua_setfield(L, -2, "setFocusPolicy");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__hasFocus);
    lua_setfield(L, -2, "hasFocus");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__removeAction);
    lua_setfield(L, -2, "removeAction");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setStatusTip);
    lua_setfield(L, -2, "setStatusTip");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setFocus);
    lua_setfield(L, -2, "setFocus");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__fontMetrics);
    lua_setfield(L, -2, "fontMetrics");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setTabOrder);
    lua_setfield(L, -2, "setTabOrder");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__windowRole);
    lua_setfield(L, -2, "windowRole");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__underMouse);
    lua_setfield(L, -2, "underMouse");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__metaObject);
    lua_setfield(L, -2, "metaObject");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__sizeHint);
    lua_setfield(L, -2, "sizeHint");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__inputMethodQuery);
    lua_setfield(L, -2, "inputMethodQuery");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__grabMouse);
    lua_setfield(L, -2, "grabMouse");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setMaximumWidth);
    lua_setfield(L, -2, "setMaximumWidth");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__updateGeometry);
    lua_setfield(L, -2, "updateGeometry");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setMinimumWidth);
    lua_setfield(L, -2, "setMinimumWidth");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setToolTip);
    lua_setfield(L, -2, "setToolTip");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setSizeIncrement);
    lua_setfield(L, -2, "setSizeIncrement");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setFocusProxy);
    lua_setfield(L, -2, "setFocusProxy");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__show);
    lua_setfield(L, -2, "show");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__y);
    lua_setfield(L, -2, "y");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__focusWidget);
    lua_setfield(L, -2, "focusWidget");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__focusPolicy);
    lua_setfield(L, -2, "focusPolicy");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__focusProxy);
    lua_setfield(L, -2, "focusProxy");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__childAt);
    lua_setfield(L, -2, "childAt");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__geometry);
    lua_setfield(L, -2, "geometry");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setLayout);
    lua_setfield(L, -2, "setLayout");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setAutoFillBackground);
    lua_setfield(L, -2, "setAutoFillBackground");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setFixedWidth);
    lua_setfield(L, -2, "setFixedWidth");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__close);
    lua_setfield(L, -2, "close");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__parentWidget);
    lua_setfield(L, -2, "parentWidget");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__isTopLevel);
    lua_setfield(L, -2, "isTopLevel");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setWindowSurface);
    lua_setfield(L, -2, "setWindowSurface");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__update);
    lua_setfield(L, -2, "update");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__isWindow);
    lua_setfield(L, -2, "isWindow");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__internalWinId);
    lua_setfield(L, -2, "internalWinId");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setCursor);
    lua_setfield(L, -2, "setCursor");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__keyboardGrabber);
    lua_setfield(L, -2, "keyboardGrabber");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__x);
    lua_setfield(L, -2, "x");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__ensurePolished);
    lua_setfield(L, -2, "ensurePolished");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setMouseTracking);
    lua_setfield(L, -2, "setMouseTracking");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setAttribute);
    lua_setfield(L, -2, "setAttribute");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__window);
    lua_setfield(L, -2, "window");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setForegroundRole);
    lua_setfield(L, -2, "setForegroundRole");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setDisabled);
    lua_setfield(L, -2, "setDisabled");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__mapFromParent);
    lua_setfield(L, -2, "mapFromParent");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setHidden);
    lua_setfield(L, -2, "setHidden");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setPalette);
    lua_setfield(L, -2, "setPalette");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setUpdatesEnabled);
    lua_setfield(L, -2, "setUpdatesEnabled");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__mapFromGlobal);
    lua_setfield(L, -2, "mapFromGlobal");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__isFullScreen);
    lua_setfield(L, -2, "isFullScreen");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__layoutDirection);
    lua_setfield(L, -2, "layoutDirection");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__rect);
    lua_setfield(L, -2, "rect");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__activateWindow);
    lua_setfield(L, -2, "activateWindow");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__showMaximized);
    lua_setfield(L, -2, "showMaximized");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__height);
    lua_setfield(L, -2, "height");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__showFullScreen);
    lua_setfield(L, -2, "showFullScreen");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__minimumHeight);
    lua_setfield(L, -2, "minimumHeight");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__insertAction);
    lua_setfield(L, -2, "insertAction");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__devType);
    lua_setfield(L, -2, "devType");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setMask);
    lua_setfield(L, -2, "setMask");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setWindowOpacity);
    lua_setfield(L, -2, "setWindowOpacity");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__move);
    lua_setfield(L, -2, "move");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__restoreGeometry);
    lua_setfield(L, -2, "restoreGeometry");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__maximumHeight);
    lua_setfield(L, -2, "maximumHeight");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setWindowFlags);
    lua_setfield(L, -2, "setWindowFlags");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__paintEngine);
    lua_setfield(L, -2, "paintEngine");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__isMinimized);
    lua_setfield(L, -2, "isMinimized");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setMinimumHeight);
    lua_setfield(L, -2, "setMinimumHeight");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setSizePolicy);
    lua_setfield(L, -2, "setSizePolicy");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__heightForWidth);
    lua_setfield(L, -2, "heightForWidth");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__visibleRegion);
    lua_setfield(L, -2, "visibleRegion");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__getContentsMargins);
    lua_setfield(L, -2, "getContentsMargins");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setWindowModality);
    lua_setfield(L, -2, "setWindowModality");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__lower);
    lua_setfield(L, -2, "lower");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__windowTitle);
    lua_setfield(L, -2, "windowTitle");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__sizeIncrement);
    lua_setfield(L, -2, "sizeIncrement");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__showNormal);
    lua_setfield(L, -2, "showNormal");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__unsetLayoutDirection);
    lua_setfield(L, -2, "unsetLayoutDirection");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__autoFillBackground);
    lua_setfield(L, -2, "autoFillBackground");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__isActiveWindow);
    lua_setfield(L, -2, "isActiveWindow");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__minimumSizeHint);
    lua_setfield(L, -2, "minimumSizeHint");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__width);
    lua_setfield(L, -2, "width");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__setLocale);
    lua_setfield(L, -2, "setLocale");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__saveGeometry);
    lua_setfield(L, -2, "saveGeometry");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__overrideWindowFlags);
    lua_setfield(L, -2, "overrideWindowFlags");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__toolTip);
    lua_setfield(L, -2, "toolTip");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__raise);
    lua_setfield(L, -2, "raise");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__windowIcon);
    lua_setfield(L, -2, "windowIcon");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__fontInfo);
    lua_setfield(L, -2, "fontInfo");
    lua_pushcfunction(L, LuaBinder< QWidget >::__LuaWrapCall__handle);
    lua_setfield(L, -2, "handle");
    LuaBinder< QWidget >::lqt_pushenum_RenderFlag(L);
    lua_setfield(L, -2, "RenderFlag");
    lua_newtable(L);
    lua_pushboolean(L, 1);
    lua_setfield(L, -2, "QObject*");
    lua_pushboolean(L, 1);
    lua_setfield(L, -2, "QPaintDevice*");
    lua_setfield(L, -2, "__base");
    lua_pushcfunction(L, lqtL_newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, lqtL_index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, lqtL_gc);
    lua_setfield(L, -2, "__gc");
    lua_pushstring(L, "QWidget");
    lua_setfield(L, -2, "__qtype");
		lua_setglobal(L, "QWidget");
  } else {
		lua_pop(L, 1);
	}
  return 0;
}
