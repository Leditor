#include "lqt_common.hpp"
#include <QGraphicsItem>
#include <QtCore>
#include <QtGui>

template <> class LuaBinder< QGraphicsItem > : public QGraphicsItem {
  private:
  lua_State *L;
  public:
  static int __LuaWrapCall__scenePos (lua_State *L);
  static int __LuaWrapCall__setCursor (lua_State *L);
  static int __LuaWrapCall__setPos__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__setPos__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__setPos (lua_State *L);
  static int __LuaWrapCall__isVisible (lua_State *L);
  static int __LuaWrapCall__setAcceptsHoverEvents (lua_State *L);
  static int __LuaWrapCall__hasCursor (lua_State *L);
  static int __LuaWrapCall__topLevelItem (lua_State *L);
  static int __LuaWrapCall__y (lua_State *L);
  static int __LuaWrapCall__x (lua_State *L);
  static int __LuaWrapCall__setAcceptDrops (lua_State *L);
  static int __LuaWrapCall__clearFocus (lua_State *L);
  static int __LuaWrapCall__deviceTransform (lua_State *L);
  static int __LuaWrapCall__shape (lua_State *L);
  static int __LuaWrapCall__pos (lua_State *L);
  static int __LuaWrapCall__moveBy (lua_State *L);
  static int __LuaWrapCall__scale (lua_State *L);
  static int __LuaWrapCall__isSelected (lua_State *L);
  static int __LuaWrapCall__setParentItem (lua_State *L);
  static int __LuaWrapCall__hasFocus (lua_State *L);
  static int __LuaWrapCall__setFocus (lua_State *L);
  static int __LuaWrapCall__setFlag (lua_State *L);
  static int __LuaWrapCall__translate (lua_State *L);
  static int __LuaWrapCall__acceptedMouseButtons (lua_State *L);
  static int __LuaWrapCall__opaqueArea (lua_State *L);
  static int __LuaWrapCall__sceneTransform (lua_State *L);
  static int __LuaWrapCall__resetTransform (lua_State *L);
  static int __LuaWrapCall__setToolTip (lua_State *L);
  static int __LuaWrapCall__parentItem (lua_State *L);
  static int __LuaWrapCall__collidesWithItem (lua_State *L);
  static int __LuaWrapCall__show (lua_State *L);
  static int __LuaWrapCall__isEnabled (lua_State *L);
  static int __LuaWrapCall__setFlags (lua_State *L);
  static int __LuaWrapCall__setGroup (lua_State *L);
  static int __LuaWrapCall__transform (lua_State *L);
  static int __LuaWrapCall__mapToItem__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__mapToItem__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__mapToItem__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__mapToItem__OverloadedVersion__4 (lua_State *L);
  static int __LuaWrapCall__mapToItem__OverloadedVersion__5 (lua_State *L);
  static int __LuaWrapCall__mapToItem__OverloadedVersion__6 (lua_State *L);
  static int __LuaWrapCall__mapToItem (lua_State *L);
  static int __LuaWrapCall__ensureVisible__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__ensureVisible__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__ensureVisible (lua_State *L);
  static int __LuaWrapCall__update__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__update__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__update (lua_State *L);
  static int __LuaWrapCall__isObscured__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__isObscured__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__isObscured__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__isObscured (lua_State *L);
  static int __LuaWrapCall__setAcceptedMouseButtons (lua_State *L);
  static int __LuaWrapCall__matrix (lua_State *L);
  static int __LuaWrapCall__advance (lua_State *L);
  static int __LuaWrapCall__toolTip (lua_State *L);
  static int __LuaWrapCall__delete (lua_State *L);
  static int __LuaWrapCall__sceneBoundingRect (lua_State *L);
  static int __LuaWrapCall__flags (lua_State *L);
  static int __LuaWrapCall__isAncestorOf (lua_State *L);
  static int __LuaWrapCall__collidesWithPath (lua_State *L);
  static int __LuaWrapCall__new (lua_State *L);
  static int __LuaWrapCall__scene (lua_State *L);
  static int __LuaWrapCall__rotate (lua_State *L);
  static int __LuaWrapCall__setSelected (lua_State *L);
  static int __LuaWrapCall__hide (lua_State *L);
  static int __LuaWrapCall__installSceneEventFilter (lua_State *L);
  static int __LuaWrapCall__contains (lua_State *L);
  static int __LuaWrapCall__sceneMatrix (lua_State *L);
  static int __LuaWrapCall__setTransform (lua_State *L);
  static int __LuaWrapCall__group (lua_State *L);
  static int __LuaWrapCall__mapFromScene__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__mapFromScene__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__mapFromScene__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__mapFromScene__OverloadedVersion__4 (lua_State *L);
  static int __LuaWrapCall__mapFromScene__OverloadedVersion__5 (lua_State *L);
  static int __LuaWrapCall__mapFromScene__OverloadedVersion__6 (lua_State *L);
  static int __LuaWrapCall__mapFromScene (lua_State *L);
  static int __LuaWrapCall__acceptsHoverEvents (lua_State *L);
  static int __LuaWrapCall__setZValue (lua_State *L);
  static int __LuaWrapCall__setMatrix (lua_State *L);
  static int __LuaWrapCall__setVisible (lua_State *L);
  static int __LuaWrapCall__handlesChildEvents (lua_State *L);
  static int __LuaWrapCall__mapFromItem__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__mapFromItem__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__mapFromItem__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__mapFromItem__OverloadedVersion__4 (lua_State *L);
  static int __LuaWrapCall__mapFromItem__OverloadedVersion__5 (lua_State *L);
  static int __LuaWrapCall__mapFromItem__OverloadedVersion__6 (lua_State *L);
  static int __LuaWrapCall__mapFromItem (lua_State *L);
  static int __LuaWrapCall__childrenBoundingRect (lua_State *L);
  static int __LuaWrapCall__isObscuredBy (lua_State *L);
  static int __LuaWrapCall__children (lua_State *L);
  static int __LuaWrapCall__unsetCursor (lua_State *L);
  static int __LuaWrapCall__shear (lua_State *L);
  static int __LuaWrapCall__type (lua_State *L);
  static int __LuaWrapCall__mapToScene__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__mapToScene__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__mapToScene__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__mapToScene__OverloadedVersion__4 (lua_State *L);
  static int __LuaWrapCall__mapToScene__OverloadedVersion__5 (lua_State *L);
  static int __LuaWrapCall__mapToScene__OverloadedVersion__6 (lua_State *L);
  static int __LuaWrapCall__mapToScene (lua_State *L);
  static int __LuaWrapCall__collidingItems (lua_State *L);
  static int __LuaWrapCall__setEnabled (lua_State *L);
  static int __LuaWrapCall__acceptDrops (lua_State *L);
  static int __LuaWrapCall__zValue (lua_State *L);
  static int __LuaWrapCall__resetMatrix (lua_State *L);
  static int __LuaWrapCall__cursor (lua_State *L);
  static int __LuaWrapCall__mapFromParent__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__mapFromParent__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__mapFromParent__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__mapFromParent__OverloadedVersion__4 (lua_State *L);
  static int __LuaWrapCall__mapFromParent__OverloadedVersion__5 (lua_State *L);
  static int __LuaWrapCall__mapFromParent__OverloadedVersion__6 (lua_State *L);
  static int __LuaWrapCall__mapFromParent (lua_State *L);
  static int __LuaWrapCall__mapToParent__OverloadedVersion__1 (lua_State *L);
  static int __LuaWrapCall__mapToParent__OverloadedVersion__2 (lua_State *L);
  static int __LuaWrapCall__mapToParent__OverloadedVersion__3 (lua_State *L);
  static int __LuaWrapCall__mapToParent__OverloadedVersion__4 (lua_State *L);
  static int __LuaWrapCall__mapToParent__OverloadedVersion__5 (lua_State *L);
  static int __LuaWrapCall__mapToParent__OverloadedVersion__6 (lua_State *L);
  static int __LuaWrapCall__mapToParent (lua_State *L);
  static int __LuaWrapCall__setHandlesChildEvents (lua_State *L);
  static int __LuaWrapCall__removeSceneEventFilter (lua_State *L);
  static int __LuaWrapCall__data (lua_State *L);
  static int __LuaWrapCall__setData (lua_State *L);
  void advance (int arg1);
protected:
  void focusInEvent (QFocusEvent * arg1);
public:
protected:
  void keyPressEvent (QKeyEvent * arg1);
public:
protected:
  void contextMenuEvent (QGraphicsSceneContextMenuEvent * arg1);
public:
protected:
  void keyReleaseEvent (QKeyEvent * arg1);
public:
protected:
  void dragLeaveEvent (QGraphicsSceneDragDropEvent * arg1);
public:
protected:
  void mousePressEvent (QGraphicsSceneMouseEvent * arg1);
public:
  bool collidesWithPath (const QPainterPath& arg1, Qt::ItemSelectionMode arg2 = Qt::IntersectsItemShape) const;
protected:
  void hoverMoveEvent (QGraphicsSceneHoverEvent * arg1);
public:
protected:
  void dragMoveEvent (QGraphicsSceneDragDropEvent * arg1);
public:
  bool isObscuredBy (const QGraphicsItem * arg1) const;
protected:
  void hoverLeaveEvent (QGraphicsSceneHoverEvent * arg1);
public:
protected:
  bool sceneEvent (QEvent * arg1);
public:
protected:
  bool sceneEventFilter (QGraphicsItem * arg1, QEvent * arg2);
public:
  QRectF boundingRect () const;
protected:
  QVariant itemChange (QGraphicsItem::GraphicsItemChange arg1, const QVariant& arg2);
public:
protected:
  void mouseDoubleClickEvent (QGraphicsSceneMouseEvent * arg1);
public:
protected:
  void mouseMoveEvent (QGraphicsSceneMouseEvent * arg1);
public:
  QPainterPath opaqueArea () const;
protected:
  void focusOutEvent (QFocusEvent * arg1);
public:
protected:
  QVariant extension (const QVariant& arg1) const;
public:
protected:
  void setExtension (QGraphicsItem::Extension arg1, const QVariant& arg2);
public:
protected:
  void inputMethodEvent (QInputMethodEvent * arg1);
public:
  int type () const;
protected:
  bool supportsExtension (QGraphicsItem::Extension arg1) const;
public:
protected:
  QVariant inputMethodQuery (Qt::InputMethodQuery arg1) const;
public:
protected:
  void wheelEvent (QGraphicsSceneWheelEvent * arg1);
public:
  bool contains (const QPointF& arg1) const;
protected:
  void mouseReleaseEvent (QGraphicsSceneMouseEvent * arg1);
public:
  QPainterPath shape () const;
  void paint (QPainter * arg1, const QStyleOptionGraphicsItem * arg2, QWidget * arg3 = 0);
  bool collidesWithItem (const QGraphicsItem * arg1, Qt::ItemSelectionMode arg2 = Qt::IntersectsItemShape) const;
protected:
  void hoverEnterEvent (QGraphicsSceneHoverEvent * arg1);
public:
protected:
  void dragEnterEvent (QGraphicsSceneDragDropEvent * arg1);
public:
protected:
  void dropEvent (QGraphicsSceneDragDropEvent * arg1);
public:
  ~LuaBinder< QGraphicsItem > ();
  static int lqt_pushenum_GraphicsItemFlag (lua_State *L);
  static int lqt_pushenum_GraphicsItemFlag_QFLAGS_CREATOR (lua_State *L);
  static int lqt_pushenum_GraphicsItemChange (lua_State *L);
  static int lqt_pushenum_GraphicsItemChange_QFLAGS_CREATOR (lua_State *L);
  static int lqt_pushenum_Extension (lua_State *L);
  static int lqt_pushenum_Extension_QFLAGS_CREATOR (lua_State *L);
  LuaBinder< QGraphicsItem > (lua_State *l, QGraphicsItem * arg1, QGraphicsScene * arg2):QGraphicsItem(arg1, arg2), L(l) {}
};

extern "C" int luaopen_QGraphicsItem (lua_State *L);
