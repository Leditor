#include "lqt_bind_QTextBlockUserData.hpp"

int LuaBinder< QTextBlockUserData >::__LuaWrapCall__delete (lua_State *L) {
  QTextBlockUserData *& __lua__obj = *static_cast<QTextBlockUserData**>(lqtL_checkudata(L, 1, "QTextBlockUserData*"));
	if (__lua__obj==0) {
		lua_pushstring(L, "trying to reference deleted pointer");
		lua_error(L);
		return 0;
	}
  delete __lua__obj;
  __lua__obj = 0;
  return 0;
}
LuaBinder< QTextBlockUserData >::  ~LuaBinder< QTextBlockUserData > () {
  int oldtop = lua_gettop(L);
  lqtL_pushudata(L, this, "QTextBlockUserData*");
  lua_getfield(L, -1, "~QTextBlockUserData");

  if (lua_isfunction(L, -1)) {
    lua_insert(L, -2);
    lua_pcall(L, 1, 1, 0);
  } else {
  }
  lua_settop(L, oldtop);
}
int luaopen_QTextBlockUserData (lua_State *L) {
  if (luaL_newmetatable(L, "QTextBlockUserData*")) {
    lua_pushcfunction(L, LuaBinder< QTextBlockUserData >::__LuaWrapCall__delete);
    lua_setfield(L, -2, "delete");
    lua_newtable(L);
    lua_setfield(L, -2, "__base");
    lua_pushcfunction(L, lqtL_newindex);
    lua_setfield(L, -2, "__newindex");
    lua_pushcfunction(L, lqtL_index);
    lua_setfield(L, -2, "__index");
    lua_pushcfunction(L, lqtL_gc);
    lua_setfield(L, -2, "__gc");
    lua_pushstring(L, "QTextBlockUserData");
    lua_setfield(L, -2, "__qtype");
		lua_setglobal(L, "QTextBlockUserData");
  } else {
		lua_pop(L, 1);
	}
  return 0;
}
